Consbench - benchmarks for Cons-like build tools
================================================

Consbench_ is meant to become a collection of benchmarks for Cons-like
build tools. Currently it consists of just one benchmark: "prog6000",
a program consisting of > 6000 C++ source files, partitioned into ~140
libraries linked together to form the application.

Benchmarks can be used to measure different aspects of a
build tool. Some things of obvious interest are:

* timing a full build, to see the general "overhead" of the build tool

* timing an up-to-date check, when nothing has to be built

* if the build tool can build in parallel, see how it scales to many
  cores (e.g. "make -j24")

prog6000
--------

This benchmark is meant to be a realistic big application. The realism
comes from the fact that the filenames and #include structure is taken
directly from a real application: the Chromium web browser. The
application is (almost) a nonsense application, but retains the
following features from Chromium:

* the names and directory layout of the source and include files

* the sizes of the files (in lines and bytes)

* the #include structure of the application

Since all the "logic" of the original application is gone, the
resulting application is very portable (but doesn't do much).  The
application is described by a JSON file. From that file, it is easy to
generate "build descriptions" for different build tools. Currently
there are such generators for: Make, CMake_, Cons_, SCons_ and Jcons.
The CMake files can of course be used with the different "generators"
available there: Make, Ninja, etc..

Step by step
------------

::

    $ cd prog6000
    $ TOP=$(pwd)
    $
    $ rake gen-construct
    $
    $ time cons .
    $
    $ mkdir -p build/cmake
    $ cd build/cmake
    $ cmake $TOP
    $ time make -j24
    $ 
    $ cd $TOP
    $ time scons -j24
    $ 
    $ time jcons -j24
    $


License
-------

Consbench_ is released under the GNU General Public License version 3.
For details see the file `<COPYING.txt>`_ in the same directory as this file.

The source files, have their names and size from the Chromium
browser. But the content is totally different.

.. _Consbench:          https://bitbucket.org/holmberg556/consbench
.. _CMake:              http://www.cmake.org
.. _Cons:               http://www.gnu.org/software/cons/
.. _SCons:              http://www.scons.org

