#!/usr/bin/ruby
#----------------------------------------------------------------------
# change-each-file.rb
#----------------------------------------------------------------------
# Copyright 2011-2012 Johan Holmberg.
#----------------------------------------------------------------------
# This file is part of "consbench".
#
# "consbench" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "consbench" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "consbench".  If not, see <http://www.gnu.org/licenses/>.
#----------------------------------------------------------------------

def my_system(cmd)
    puts "+ #{cmd}"
    ok = system(cmd)
    if ! ok
        raise "command failed: '#{cmd}'"
    end
end

def my_system_err(cmd)
    puts "+ #{cmd}"
    ok = system(cmd)
    if ! ok
        puts "warning: error in command #{cmd}"
    end
end

#----------------------------------------------------------------------

build_variants = {
    "rcons.py" => {
        "cmd" => "rcons.py -jNNN",
        "dir" => "build/jcons-python",
    },
    "scons" => {
        "cmd" => "scons -jNNN -s",
        "dir" => "build/scons",
    },
    "rcons" => {
        "cmd" => "rcons-ruby -jNNN -q",
        "dir" => "build/jcons-ruby",
    },
    "cmake" => {
        "cmd" => "cd build/cmake && make -jNNN",
        "dir" => "build/cmake",
    },
    "cons" => {
        "cmd" => "cons .",
        "dir" => ".",
    },
}

$opt_start = 0
$opt_parallel = 4

def show_usage
    puts "Usage: change-each-file [--start=N] [--parallel=N] CMD"
end

while ARGV.size > 0 && ARGV[0] =~ /^--/
    arg = ARGV.shift
    case arg
    when /^--start=(\d+)/
        $opt_start = Integer($1)
    when /^--parallel=(\d+)/
        $opt_parallel = Integer($1)
    else
        puts "ERROR: unknown option: #{arg}"
        exit 1
    end
end

if ARGV.size == 0
    show_usage
    exit(1)
end

command = ARGV[0]
if ! build_variants[command]
    puts "ERRROR: unknown command: '#{command}'"
    exit(1)
end

all_files = File.readlines("etc/sources.txt").map(&:chomp)
all_files = all_files.drop($opt_start)

ii = 0

for file in all_files
    src_file = "src/" + file

    sleep 1

    puts "=" * 40 + " " + file
    File.open(src_file, "a") do |f|
        f.puts "/* a dummy line */"
    end
    t = Time.now
    time = t.strftime("%Y-%m-%d_%H:%M:%S")
    
    sleep 1

    my_system "echo #{time} --- #{file} >> processed-files"

    cmd = build_variants[command]["cmd"]
    dir = build_variants[command]["dir"]

    used_cmd = cmd.sub(/NNN/, $opt_parallel.to_s)

    my_system used_cmd
    my_system "#{dir}/src/prog6000 using-src-files #{file} . > list.using"
    my_system "#{dir}/src/prog6000 newer-src-files #{time} > list.newer"

    my_system "wc -l list.using list.newer"
    my_system_err "diff list.using list.newer"

    ii += 1
end
