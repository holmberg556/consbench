/* CONSBENCH file begin */
#ifndef MAIN_LIBAPP_BASE_CPP
#define MAIN_LIBAPP_BASE_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, app_gfx_gl_main_libapp_base_cpp, "app/gfx/gl/main_libapp_base.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(app_gfx_gl_main_libapp_base_cpp, "app/gfx/gl/main_libapp_base.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_ui_base_animation_animation_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_animation_animation_container_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_animation_linear_animation_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_animation_multi_animation_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_animation_slide_animation_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_animation_throb_animation_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_animation_tween_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_clipboard_clipboard_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_clipboard_clipboard_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_clipboard_scoped_clipboard_writer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_dragdrop_gtk_dnd_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_gtk_event_synthesis_gtk_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_gtk_gtk_signal_registrar_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_keycodes_keyboard_code_conversion_gtk_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_keycodes_keyboard_code_conversion_x_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_l10n_l10n_font_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_l10n_l10n_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_l10n_l10n_util_posix_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_models_button_menu_item_model_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_models_menu_model_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_models_simple_menu_model_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_models_table_model_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_resource_data_pack_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_resource_resource_bundle_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_resource_resource_bundle_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_resource_resource_bundle_posix_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_text_text_elider_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_theme_provider_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_x_active_window_watcher_x_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_x_x11_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_app_gfx_gl_gl_context_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_app_gfx_gl_gl_context_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_app_gfx_gl_gl_context_osmesa_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_app_gfx_gl_gl_context_stub_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_app_gfx_gl_gl_implementation_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_app_gfx_gl_gl_implementation_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_app_gfx_gl_gl_interface_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_app_sql_connection_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_app_sql_meta_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_app_sql_statement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_app_sql_transaction_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_app_surface_transport_dib_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_app_gl_bindings_autogen_gl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_app_gl_bindings_autogen_mock_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_app_gl_bindings_autogen_osmesa_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_app_gfx_gl_gl_context_egl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_app_gl_bindings_autogen_egl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_app_gl_bindings_autogen_glx_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_models_tree_model_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_system_monitor_system_monitor_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_system_monitor_system_monitor_posix_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_ui_base_paths_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_ui_base_switches_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_app_app_paths_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_app_app_switches_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_app_gfx_gl_main_libapp_base_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_ui_base_animation_animation_cpp(it);
  PUBLIC_ui_base_animation_animation_container_cpp(it);
  PUBLIC_ui_base_animation_linear_animation_cpp(it);
  PUBLIC_ui_base_animation_multi_animation_cpp(it);
  PUBLIC_ui_base_animation_slide_animation_cpp(it);
  PUBLIC_ui_base_animation_throb_animation_cpp(it);
  PUBLIC_ui_base_animation_tween_cpp(it);
  PUBLIC_ui_base_clipboard_clipboard_cpp(it);
  PUBLIC_ui_base_clipboard_clipboard_linux_cpp(it);
  PUBLIC_ui_base_clipboard_scoped_clipboard_writer_cpp(it);
  PUBLIC_ui_base_dragdrop_gtk_dnd_util_cpp(it);
  PUBLIC_ui_base_gtk_event_synthesis_gtk_cpp(it);
  PUBLIC_ui_base_gtk_gtk_signal_registrar_cpp(it);
  PUBLIC_ui_base_keycodes_keyboard_code_conversion_gtk_cpp(it);
  PUBLIC_ui_base_keycodes_keyboard_code_conversion_x_cpp(it);
  PUBLIC_ui_base_l10n_l10n_font_util_cpp(it);
  PUBLIC_ui_base_l10n_l10n_util_cpp(it);
  PUBLIC_ui_base_l10n_l10n_util_posix_cpp(it);
  PUBLIC_ui_base_models_button_menu_item_model_cpp(it);
  PUBLIC_ui_base_models_menu_model_cpp(it);
  PUBLIC_ui_base_models_simple_menu_model_cpp(it);
  PUBLIC_ui_base_models_table_model_cpp(it);
  PUBLIC_ui_base_resource_data_pack_cpp(it);
  PUBLIC_ui_base_resource_resource_bundle_cpp(it);
  PUBLIC_ui_base_resource_resource_bundle_linux_cpp(it);
  PUBLIC_ui_base_resource_resource_bundle_posix_cpp(it);
  PUBLIC_ui_base_text_text_elider_cpp(it);
  PUBLIC_ui_base_theme_provider_cpp(it);
  PUBLIC_ui_base_x_active_window_watcher_x_cpp(it);
  PUBLIC_ui_base_x_x11_util_cpp(it);
  PUBLIC_app_gfx_gl_gl_context_cpp(it);
  PUBLIC_app_gfx_gl_gl_context_linux_cpp(it);
  PUBLIC_app_gfx_gl_gl_context_osmesa_cpp(it);
  PUBLIC_app_gfx_gl_gl_context_stub_cpp(it);
  PUBLIC_app_gfx_gl_gl_implementation_cpp(it);
  PUBLIC_app_gfx_gl_gl_implementation_linux_cpp(it);
  PUBLIC_app_gfx_gl_gl_interface_cpp(it);
  PUBLIC_app_sql_connection_cpp(it);
  PUBLIC_app_sql_meta_table_cpp(it);
  PUBLIC_app_sql_statement_cpp(it);
  PUBLIC_app_sql_transaction_cpp(it);
  PUBLIC_app_surface_transport_dib_linux_cpp(it);
  PUBLIC_out_Debug_obj_gen_app_gl_bindings_autogen_gl_cpp(it);
  PUBLIC_out_Debug_obj_gen_app_gl_bindings_autogen_mock_cpp(it);
  PUBLIC_out_Debug_obj_gen_app_gl_bindings_autogen_osmesa_cpp(it);
  PUBLIC_app_gfx_gl_gl_context_egl_cpp(it);
  PUBLIC_out_Debug_obj_gen_app_gl_bindings_autogen_egl_cpp(it);
  PUBLIC_out_Debug_obj_gen_app_gl_bindings_autogen_glx_cpp(it);
  PUBLIC_ui_base_models_tree_model_cpp(it);
  PUBLIC_ui_base_system_monitor_system_monitor_cpp(it);
  PUBLIC_ui_base_system_monitor_system_monitor_posix_cpp(it);
  PUBLIC_ui_base_ui_base_paths_cpp(it);
  PUBLIC_ui_base_ui_base_switches_cpp(it);
  PUBLIC_app_app_paths_cpp(it);
  PUBLIC_app_app_switches_cpp(it);
}

#endif

