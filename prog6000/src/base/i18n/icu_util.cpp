/* CONSBENCH file begin */
#ifndef ICU_UTIL_CC_238
#define ICU_UTIL_CC_238

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, base_i18n_icu_util_cpp, "base/i18n/icu_util.cpp");

/* CONSBENCH includes begin */
#include "base/i18n/icu_util.h"
#include "build/build_config.h"
#include <string>
#include "base/file_path.h"
#include "base/file_util.h"
#include "base/logging.h"
#include "base/path_service.h"
#include "base/string_util.h"
#include "base/sys_string_conversions.h"
#include "unicode/putil.h"
#include "unicode/udata.h"
#include "base/mac/foundation_util.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(base_i18n_icu_util_cpp, "base/i18n/icu_util.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(base_i18n_icu_util_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include "base/i18n/icu_util.h" */
/*-no-  */
/*-no- #include "build/build_config.h" */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- #include <windows.h> */
/*-no- #endif */
/*-no-  */
/*-no- #include <string> */
/*-no-  */
/*-no- #include "base/file_path.h" */
/*-no- #include "base/file_util.h" */
/*-no- #include "base/logging.h" */
/*-no- #include "base/path_service.h" */
/*-no- #include "base/string_util.h" */
/*-no- #include "base/sys_string_conversions.h" */
/*-no- #include "unicode/putil.h" */
/*-no- #include "unicode/udata.h" */
/*-no-  */
/*-no- #if defined(OS_MACOSX) */
/*-no- #include "base/mac/foundation_util.h" */
/*-no- #endif */
/*-no-  */
/*-no- #define ICU_UTIL_DATA_FILE   0 */
/*-no- #define ICU_UTIL_DATA_SHARED 1 */
/*-no- #define ICU_UTIL_DATA_STATIC 2 */
/*-no-  */
/*-no- #ifndef ICU_UTIL_DATA_IMPL */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- #define ICU_UTIL_DATA_IMPL ICU_UTIL_DATA_SHARED */
/*-no- #else */
/*-no- #define ICU_UTIL_DATA_IMPL ICU_UTIL_DATA_STATIC */
/*-no- #endif */
/*-no-  */
/*-no- #endif  // ICU_UTIL_DATA_IMPL */
/*-no-  */
/*-no- #if ICU_UTIL_DATA_IMPL == ICU_UTIL_DATA_FILE */
/*-no- #define ICU_UTIL_DATA_FILE_NAME "icudt" U_ICU_VERSION_SHORT "l.dat" */
/*-no- #elif ICU_UTIL_DATA_IMPL == ICU_UTIL_DATA_SHARED */
/*-no- #define ICU_UTIL_DATA_SYMBOL "icudt" U_ICU_VERSION_SHORT "_dat" */
/*-no- #if defined(OS_WIN) */
/*-no- #define ICU_UTIL_DATA_SHARED_MODULE_NAME "icudt" U_ICU_VERSION_SHORT ".dll" */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- .................... */
/*-no-  */
/*-no- ................... */
/*-no- #ifndef NDEBUG */
/*-no- ............................................................................ */
/*-no- ............................................................................ */
/*-no- ................................... */
/*-no- .................................. */
/*-no- ....................... */
/*-no- ..................... */
/*-no- #endif */
/*-no-  */
/*-no- #if (ICU_UTIL_DATA_IMPL == ICU_UTIL_DATA_SHARED) */
/*-no- ........................................................................ */
/*-no- ..................... */
/*-no- ................................................. */
/*-no- ...................................................................... */
/*-no-  */
/*-no- .......................................................... */
/*-no- ................ */
/*-no- ........................................................................ */
/*-no- ................. */
/*-no- ... */
/*-no-  */
/*-no- .............................................................. */
/*-no- .............. */
/*-no- ........................................................... */
/*-no- ................................................... */
/*-no- ................. */
/*-no- ... */
/*-no-  */
/*-no- ................................ */
/*-no- ........................................................... */
/*-no- ............................. */
/*-no- #elif (ICU_UTIL_DATA_IMPL == ICU_UTIL_DATA_STATIC) */
/*-no- ...................................... */
/*-no- .............. */
/*-no- #elif (ICU_UTIL_DATA_IMPL == ICU_UTIL_DATA_FILE) */
/*-no- #if !defined(OS_MACOSX) */
/*-no- .................................................................. */
/*-no- ........................................................................ */
/*-no- ..................................... */
/*-no- ..................... */
/*-no- ............................................................. */
/*-no- .................. */
/*-no- ................................................ */
/*-no- .......................................... */
/*-no- .......................................................... */
/*-no- ................................ */
/*-no- ................................................. */
/*-no- ............................. */
/*-no- #else */
/*-no- ............................................................................. */
/*-no- ........................................................................... */
/*-no- .............................................................................. */
/*-no- ........................ */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ................. */
/*-no- ................................................. */
/*-no- ............................... */
/*-no- ............................................................ */
/*-no- ........................ */
/*-no- .............................................................................. */
/*-no- ............................ */
/*-no- ...................................................................... */
/*-no- ................... */
/*-no- ..... */
/*-no- ............................................. */
/*-no- .......................................................... */
/*-no- ................... */
/*-no- ..... */
/*-no- ... */
/*-no- ................................ */
/*-no- .................................................................... */
/*-no- ............................. */
/*-no- #endif  // OS check */
/*-no- #endif */
/*-no- . */
/*-no-  */
/*-no- ........................ */

#endif
/* CONSBENCH file end */
