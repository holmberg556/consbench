/* CONSBENCH file begin */
#ifndef MAIN_LIBBASE_I18N_CPP
#define MAIN_LIBBASE_I18N_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, base_i18n_main_libbase_i18n_cpp, "base/i18n/main_libbase_i18n.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(base_i18n_main_libbase_i18n_cpp, "base/i18n/main_libbase_i18n.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_base_i18n_bidi_line_iterator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_i18n_break_iterator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_i18n_char_iterator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_i18n_file_util_icu_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_i18n_icu_encoding_detection_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_i18n_icu_string_conversions_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_i18n_icu_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_i18n_number_formatting_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_i18n_rtl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_i18n_time_formatting_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_base_i18n_main_libbase_i18n_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_base_i18n_bidi_line_iterator_cpp(it);
  PUBLIC_base_i18n_break_iterator_cpp(it);
  PUBLIC_base_i18n_char_iterator_cpp(it);
  PUBLIC_base_i18n_file_util_icu_cpp(it);
  PUBLIC_base_i18n_icu_encoding_detection_cpp(it);
  PUBLIC_base_i18n_icu_string_conversions_cpp(it);
  PUBLIC_base_i18n_icu_util_cpp(it);
  PUBLIC_base_i18n_number_formatting_cpp(it);
  PUBLIC_base_i18n_rtl_cpp(it);
  PUBLIC_base_i18n_time_formatting_cpp(it);
}

#endif

