/* CONSBENCH file begin */
#ifndef MAIN_LIBDYNAMIC_ANNOTATIONS_CPP
#define MAIN_LIBDYNAMIC_ANNOTATIONS_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, base_third_party_dynamic_annotations_main_libdynamic_annotations_cpp, "base/third_party/dynamic_annotations/main_libdynamic_annotations.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(base_third_party_dynamic_annotations_main_libdynamic_annotations_cpp, "base/third_party/dynamic_annotations/main_libdynamic_annotations.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_base_third_party_dynamic_annotations_dynamic_annotations_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_base_third_party_dynamic_annotations_main_libdynamic_annotations_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_base_third_party_dynamic_annotations_dynamic_annotations_cpp(it);
}

#endif

