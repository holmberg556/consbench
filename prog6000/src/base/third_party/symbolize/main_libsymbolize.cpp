/* CONSBENCH file begin */
#ifndef MAIN_LIBSYMBOLIZE_CPP
#define MAIN_LIBSYMBOLIZE_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, base_third_party_symbolize_main_libsymbolize_cpp, "base/third_party/symbolize/main_libsymbolize.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(base_third_party_symbolize_main_libsymbolize_cpp, "base/third_party/symbolize/main_libsymbolize.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_base_third_party_symbolize_symbolize_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_third_party_symbolize_demangle_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_base_third_party_symbolize_main_libsymbolize_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_base_third_party_symbolize_symbolize_cpp(it);
  PUBLIC_base_third_party_symbolize_demangle_cpp(it);
}

#endif

