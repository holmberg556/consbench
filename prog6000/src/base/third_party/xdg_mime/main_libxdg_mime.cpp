/* CONSBENCH file begin */
#ifndef MAIN_LIBXDG_MIME_CPP
#define MAIN_LIBXDG_MIME_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, base_third_party_xdg_mime_main_libxdg_mime_cpp, "base/third_party/xdg_mime/main_libxdg_mime.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(base_third_party_xdg_mime_main_libxdg_mime_cpp, "base/third_party/xdg_mime/main_libxdg_mime.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_base_third_party_xdg_mime_xdgmime_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_third_party_xdg_mime_xdgmimealias_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_third_party_xdg_mime_xdgmimecache_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_third_party_xdg_mime_xdgmimeglob_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_third_party_xdg_mime_xdgmimeicon_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_third_party_xdg_mime_xdgmimeint_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_third_party_xdg_mime_xdgmimemagic_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_base_third_party_xdg_mime_xdgmimeparent_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_base_third_party_xdg_mime_main_libxdg_mime_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_base_third_party_xdg_mime_xdgmime_cpp(it);
  PUBLIC_base_third_party_xdg_mime_xdgmimealias_cpp(it);
  PUBLIC_base_third_party_xdg_mime_xdgmimecache_cpp(it);
  PUBLIC_base_third_party_xdg_mime_xdgmimeglob_cpp(it);
  PUBLIC_base_third_party_xdg_mime_xdgmimeicon_cpp(it);
  PUBLIC_base_third_party_xdg_mime_xdgmimeint_cpp(it);
  PUBLIC_base_third_party_xdg_mime_xdgmimemagic_cpp(it);
  PUBLIC_base_third_party_xdg_mime_xdgmimeparent_cpp(it);
}

#endif

