/* CONSBENCH file begin */
#ifndef BOOKMARK_UTILS_CC_1683
#define BOOKMARK_UTILS_CC_1683

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_browser_bookmarks_bookmark_utils_cpp, "chrome/browser/bookmarks/bookmark_utils.cpp");

/* CONSBENCH includes begin */
#include "chrome/browser/bookmarks/bookmark_utils.h"
#include <utility>
#include "base/basictypes.h"
#include "base/file_path.h"
#include "base/string16.h"
#include "base/string_number_conversions.h"
#include "base/time.h"
#include "base/utf_string_conversions.h"
#include "chrome/browser/bookmarks/bookmark_model.h"
#include "chrome/browser/bookmarks/bookmark_node_data.h"
#include "chrome/browser/bookmarks/bookmark_pasteboard_helper_mac.h"
#include "chrome/browser/browser_list.h"
#include "chrome/browser/browser_window.h"
#include "chrome/browser/history/query_parser.h"
#include "chrome/browser/platform_util.h"
#include "chrome/browser/prefs/pref_service.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/common/notification_service.h"
#include "chrome/common/pref_names.h"
#include "content/browser/tab_contents/page_navigator.h"
#include "content/browser/tab_contents/tab_contents.h"
#include "grit/app_strings.h"
#include "grit/chromium_strings.h"
#include "grit/generated_resources.h"
#include "net/base/net_util.h"
#include "ui/base/dragdrop/drag_drop_types.h"
#include "ui/base/l10n/l10n_util.h"
#include "ui/base/models/tree_node_iterator.h"
#include "ui/base/dragdrop/os_exchange_data.h"
#include "views/drag_utils.h"
#include "views/events/event.h"
#include "views/widget/native_widget.h"
#include "views/widget/widget.h"
#include "chrome/browser/ui/gtk/custom_drag.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_browser_bookmarks_bookmark_utils_cpp, "chrome/browser/bookmarks/bookmark_utils.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(chrome_browser_bookmarks_bookmark_utils_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include "chrome/browser/bookmarks/bookmark_utils.h" */
/*-no-  */
/*-no- #include <utility> */
/*-no-  */
/*-no- #include "base/basictypes.h" */
/*-no- #include "base/file_path.h" */
/*-no- #include "base/string16.h" */
/*-no- #include "base/string_number_conversions.h" */
/*-no- #include "base/time.h" */
/*-no- #include "base/utf_string_conversions.h" */
/*-no- #include "chrome/browser/bookmarks/bookmark_model.h" */
/*-no- #include "chrome/browser/bookmarks/bookmark_node_data.h" */
/*-no- #if defined(OS_MACOSX) */
/*-no- #include "chrome/browser/bookmarks/bookmark_pasteboard_helper_mac.h" */
/*-no- #endif */
/*-no- #include "chrome/browser/browser_list.h" */
/*-no- #include "chrome/browser/browser_window.h" */
/*-no- #include "chrome/browser/history/query_parser.h" */
/*-no- #include "chrome/browser/platform_util.h" */
/*-no- #include "chrome/browser/prefs/pref_service.h" */
/*-no- #include "chrome/browser/profiles/profile.h" */
/*-no- #include "chrome/browser/ui/browser.h" */
/*-no- #include "chrome/common/notification_service.h" */
/*-no- #include "chrome/common/pref_names.h" */
/*-no- #include "content/browser/tab_contents/page_navigator.h" */
/*-no- #include "content/browser/tab_contents/tab_contents.h" */
/*-no- #include "grit/app_strings.h" */
/*-no- #include "grit/chromium_strings.h" */
/*-no- #include "grit/generated_resources.h" */
/*-no- #include "net/base/net_util.h" */
/*-no- #include "ui/base/dragdrop/drag_drop_types.h" */
/*-no- #include "ui/base/l10n/l10n_util.h" */
/*-no- #include "ui/base/models/tree_node_iterator.h" */
/*-no-  */
/*-no- #if defined(TOOLKIT_VIEWS) */
/*-no- #include "ui/base/dragdrop/os_exchange_data.h" */
/*-no- #include "views/drag_utils.h" */
/*-no- #include "views/events/event.h" */
/*-no- #include "views/widget/native_widget.h" */
/*-no- #include "views/widget/widget.h" */
/*-no- #elif defined(TOOLKIT_GTK) */
/*-no- #include "chrome/browser/ui/gtk/custom_drag.h" */
/*-no- #endif */
/*-no-  */
/*-no- ................. */
/*-no-  */
/*-no- ........... */
/*-no-  */
/*-no- ............................................................................... */
/*-no- ............................................................................... */
/*-no- ............................................ */
/*-no- ...................................................... */
/*-no- ........ */
/*-no- .................................................... */
/*-no- .......................... */
/*-no- ......................... */
/*-no-  */
/*-no- ...................................... */
/*-no- ................. */
/*-no- ................................. */
/*-no- ... */
/*-no-  */
/*-no- ............................................... */
/*-no-  */
/*-no- ....................................... */
/*-no- ............................................ */
/*-no- ......................................................... */
/*-no- ......................................................... */
/*-no- .................... */
/*-no- .......................................................... */
/*-no- ........................................................ */
/*-no- .......................................... */
/*-no- ..................................................... */
/*-no- ....................................... */
/*-no- ..... */
/*-no- ..................................................................... */
/*-no- ... */
/*-no-  */
/*-no- ......... */
/*-no- .................... */
/*-no- .................... */
/*-no-  */
/*-no- .................................................... */
/*-no- .. */
/*-no-  */
/*-no- ................................................ */
/*-no- .................................................................... */
/*-no- ...................................................... */
/*-no- ................................................. */
/*-no- ....................... */
/*-no- ....................................................................... */
/*-no- .......... */
/*-no- ............................................................ */
/*-no- ..................................................................... */
/*-no- .................................................................... */
/*-no- ....................................................................... */
/*-no- ....................................................................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- .................................................................. */
/*-no- .................................................. */
/*-no- ................. */
/*-no- ................................................... */
/*-no- .................................................. */
/*-no- ........................ */
/*-no- ............... */
/*-no- ........ */
/*-no- .......................................... */
/*-no- ... */
/*-no- ................ */
/*-no- . */
/*-no-  */
/*-no- .......................................................................... */
/*-no- .............................................................................. */
/*-no- .......................................................................... */
/*-no- ........................................................................ */
/*-no- .............................................................................. */
/*-no- ............... */
/*-no- .......................................... */
/*-no- ........................................................... */
/*-no- ........................................... */
/*-no- .................................... */
/*-no- ....................... */
/*-no- ...................................... */
/*-no- .................... */
/*-no- ....................................... */
/*-no- ........ */
/*-no- ........................................ */
/*-no- .............................................................. */
/*-no- ......................................................... */
/*-no- ....................... */
/*-no- ......................... */
/*-no- ................................................................................ */
/*-no- ............................................................... */
/*-no- .......................................................... */
/*-no- ........................ */
/*-no- ......................................................................... */
/*-no- ........................................... */
/*-no- ....................................... */
/*-no- ................................... */
/*-no- .................................................................... */
/*-no- ..... */
/*-no- .......... */
/*-no- ....................................... */
/*-no- ..................................................... */
/*-no- .................................................................... */
/*-no- .............................. */
/*-no- ..... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ............................................ */
/*-no- ................................................................... */
/*-no- ........................... */
/*-no- ........................................... */
/*-no- ..................................................... */
/*-no- ................................................................... */
/*-no- ................ */
/*-no-  */
/*-no- ................................................ */
/*-no- ....................................... */
/*-no- ............................................. */
/*-no- ............................................................... */
/*-no- ............................................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ........................................................................... */
/*-no- ............................................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ........................... */
/*-no- ....................................................... */
/*-no- ....................................................................... */
/*-no- ............................................. */
/*-no- .............................................. */
/*-no- ................... */
/*-no- ... */
/*-no- .............. */
/*-no- . */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ........................................................................ */
/*-no- ....................................................... */
/*-no- ................................................................. */
/*-no- ............................................................. */
/*-no- ........ */
/*-no- ................................... */
/*-no- ......................................................... */
/*-no- ................................... */
/*-no- ........................................................................... */
/*-no- ...................................................... */
/*-no- ............................................................................... */
/*-no- .......................................................................... */
/*-no- . */
/*-no-  */
/*-no- ............... */
/*-no-  */
/*-no- .......................... */
/*-no-  */
/*-no- ................................... */
/*-no-  */
/*-no- ................................................................... */
/*-no- .................................................... */
/*-no- .................. */
/*-no- ............. */
/*-no- ................................................ */
/*-no- ........................................ */
/*-no- ................................................ */
/*-no- ........................................ */
/*-no- ................................................ */
/*-no- ........................................ */
/*-no- ...................................... */
/*-no- . */
/*-no-  */
/*-no- ..................................................... */
/*-no- ....................... */
/*-no- ........................................................................ */
/*-no- ........................................ */
/*-no- ... */
/*-no- ..................................................................... */
/*-no- . */
/*-no-  */
/*-no- #if defined(TOOLKIT_VIEWS) */
/*-no- ........................................... */
/*-no- .............................................................. */
/*-no- ....................................................... */
/*-no- ..................................................... */
/*-no- ...................................... */
/*-no- ..................................................... */
/*-no- ........................................................ */
/*-no- ........................................ */
/*-no-  */
/*-no- ......................................................................... */
/*-no- ........................................ */
/*-no-  */
/*-no- ................................... */
/*-no- ................................................ */
/*-no- ........................................ */
/*-no- ... */
/*-no- ............................................. */
/*-no- .......................................................... */
/*-no- ................................................................... */
/*-no- . */
/*-no- #endif  // defined(TOOLKIT_VIEWS) */
/*-no-  */
/*-no- ......................................... */
/*-no- ..................................................... */
/*-no- ........................................................ */
/*-no- .................................... */
/*-no- ..................................................... */
/*-no- .................................... */
/*-no- .......................................................... */
/*-no- ............................... */
/*-no- ................................. */
/*-no- ............................................ */
/*-no- ......................................................... */
/*-no- .......................................................... */
/*-no- ................................................................ */
/*-no- ....... */
/*-no- .......................................... */
/*-no- ..... */
/*-no- ........................................ */
/*-no- ... */
/*-no- ............................................................ */
/*-no- .............................................................................. */
/*-no- ...................................... */
/*-no- . */
/*-no-  */
/*-no- .......................................... */
/*-no- ...................................................... */
/*-no- ......................................................... */
/*-no- ..................................... */
/*-no- .................................. */
/*-no- ................. */
/*-no- ................. */
/*-no- ... */
/*-no-  */
/*-no- ....................... */
/*-no- ................. */
/*-no-  */
/*-no- .................................... */
/*-no- .................................................................... */
/*-no- ............................................... */
/*-no- ............................................................................. */
/*-no- ............................. */
/*-no- .......................................... */
/*-no- ........................................................... */
/*-no- ................................................... */
/*-no- ............................................................................... */
/*-no- ..................... */
/*-no-  */
/*-no- .............................................................. */
/*-no- ......................................... */
/*-no- ..................... */
/*-no- ..... */
/*-no- ................ */
/*-no- ... */
/*-no- .......................................... */
/*-no- .............. */
/*-no- . */
/*-no-  */
/*-no- ............................................ */
/*-no- .............................................................................. */
/*-no- .................................................. */
/*-no- ............................................. */
/*-no- ....................................... */
/*-no- ................. */
/*-no- ........... */
/*-no- ... */
/*-no- .............................................. */
/*-no- ........................................................................... */
/*-no- . */
/*-no-  */
/*-no-  */
/*-no- .................... */
/*-no- .................................... */
/*-no- ................................................................. */
/*-no- .......................................... */
/*-no- ......................... */
/*-no-  */
/*-no- #if defined(TOOLKIT_VIEWS) */
/*-no- ............................. */
/*-no- .......................... */
/*-no- .................................... */
/*-no- .................................. */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ....................................................... */
/*-no- ........................................................ */
/*-no-  */
/*-no- ...................................... */
/*-no- .............................................................. */
/*-no- ...................... */
/*-no- ........................................................ */
/*-no- ..................................................................... */
/*-no- ...................................... */
/*-no- ... */
/*-no-  */
/*-no- .............................................................. */
/*-no- #elif defined(OS_MACOSX) */
/*-no- ........................................................................... */
/*-no- ....................................................... */
/*-no- ........................................................ */
/*-no- .................................................................. */
/*-no- .............................................................. */
/*-no- #elif defined(TOOLKIT_GTK) */
/*-no- .......................................... */
/*-no- #endif */
/*-no- . */
/*-no-  */
/*-no- ...................................... */
/*-no- .............................. */
/*-no- ...................................... */
/*-no- ........................................................... */
/*-no- ......................................................... */
/*-no- .................................... */
/*-no- ........... */
/*-no-  */
/*-no- .................................................. */
/*-no- ................... */
/*-no- ...................... */
/*-no- ............................................................................... */
/*-no- ......................................................... */
/*-no- .................................. */
/*-no- ............ */
/*-no- .............................................. */
/*-no- .................................................. */
/*-no- ...................................... */
/*-no- ....... */
/*-no- .................................................... */
/*-no- ..... */
/*-no- ... */
/*-no-  */
/*-no- .......................... */
/*-no- ........................................... */
/*-no- ........................................................................ */
/*-no- . */
/*-no-  */
/*-no- ...................................... */
/*-no- .............................. */
/*-no- ...................................... */
/*-no- ...................................... */
/*-no- ......................................................... */
/*-no- ......................................... */
/*-no- ........................ */
/*-no- .................................................................. */
/*-no- . */
/*-no-  */
/*-no- .......................................... */
/*-no- ................................................................... */
/*-no- ......................................... */
/*-no- .................... */
/*-no- ........... */
/*-no-  */
/*-no- ................................................. */
/*-no-  */
/*-no- ..................... */
/*-no- ............................................... */
/*-no- .......................................... */
/*-no- ................................................................... */
/*-no- ..... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ............................................. */
/*-no- ................................................... */
/*-no- .................................... */
/*-no- .............. */
/*-no- ........... */
/*-no-  */
/*-no- ................................. */
/*-no- ......................................... */
/*-no- ........... */
/*-no-  */
/*-no- .................. */
/*-no- .................................... */
/*-no- .................................... */
/*-no- .................................................... */
/*-no- . */
/*-no-  */
/*-no- ...................................................... */
/*-no- ............ */
/*-no- ................. */
/*-no- ........................................................ */
/*-no- . */
/*-no-  */
/*-no- ......................................... */
/*-no- ....................... */
/*-no- .............................................................. */
/*-no- .......... */
/*-no- .......................................................................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ............................................................... */
/*-no- ......................... */
/*-no- ....................... */
/*-no- ......................................... */
/*-no- ........................................................................ */
/*-no- ............................... */
/*-no- ................................................. */
/*-no- .............................................................................. */
/*-no- ........................... */
/*-no- ................................ */
/*-no- .............. */
/*-no- ...................................................... */
/*-no- ................................................................ */
/*-no- .................................................... */
/*-no- ........................................................... */
/*-no- .................................. */
/*-no- .......................................... */
/*-no- ............................. */
/*-no- ......... */
/*-no- ....... */
/*-no- ............................................................................... */
/*-no- ................................... */
/*-no- ... */
/*-no-  */
/*-no- ................................. */
/*-no- .............................................................. */
/*-no- ........................................................................ */
/*-no- ...................... */
/*-no- ................................................... */
/*-no- ..... */
/*-no-  */
/*-no- ................................... */
/*-no- ............................................................................... */
/*-no- ........................................... */
/*-no- ..... */
/*-no- ... */
/*-no- ............... */
/*-no- . */
/*-no-  */
/*-no- ...................................................... */
/*-no- .............................................. */
/*-no- ........................................................................... */
/*-no- ........................................................................ */
/*-no- ............................... */
/*-no- ............................................... */
/*-no- ......................... */
/*-no- .................................................................. */
/*-no- .............................................................. */
/*-no- ............................................... */
/*-no- ..................................................................... */
/*-no- ............................................. */
/*-no- ..................................... */
/*-no- ............................ */
/*-no- ....... */
/*-no- ..... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ........................ */
/*-no- .................. */
/*-no- . */
/*-no-  */
/*-no- ............................ */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ............................................. */
/*-no- . */
/*-no-  */
/*-no- ..................................................... */
/*-no- ..................................................... */
/*-no- ................................................. */
/*-no- ............................................................. */
/*-no- .......................................................................... */
/*-no- .............................. */
/*-no- ..................... */
/*-no- ............................................................. */
/*-no- .................... */
/*-no- ........... */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ............................... */
/*-no- ............................................... */
/*-no- ............................................................................. */
/*-no- ............................. */
/*-no- ..................................... */
/*-no- ............... */
/*-no- ..... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ...................................................... */
/*-no- .................................................. */
/*-no- ............................................................ */
/*-no- .............................. */
/*-no- ..................... */
/*-no- ............................................................. */
/*-no- .................... */
/*-no- ................. */
/*-no-  */
/*-no- .............................................................................. */
/*-no- . */
/*-no-  */
/*-no- .............................................................. */
/*-no- ........................................................................... */
/*-no- ..................................................... */
/*-no- ........................... */
/*-no- ............................................................. */
/*-no- .............................................................................. */
/*-no- ....................................................................... */
/*-no- ....................................................................... */
/*-no- ...................................................... */
/*-no- ........................................................................ */
/*-no- ........................................... */
/*-no- ..... */
/*-no- ..................................................... */
/*-no- .......... */
/*-no- ................. */
/*-no- ................ */
/*-no- ... */
/*-no-  */
/*-no- .............. */
/*-no- . */
/*-no-  */
/*-no- ..................................................................... */
/*-no- ........................................................................... */
/*-no- ..................................................... */
/*-no- ............................................................. */
/*-no- ................................................................ */
/*-no- ..................................................................... */
/*-no- ... */
/*-no-  */
/*-no- ................................................... */
/*-no- ............... */
/*-no-  */
/*-no- ..................... */
/*-no- ................................. */
/*-no- ................................... */
/*-no-  */
/*-no- .............. */
/*-no- . */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ............................................................................... */
/*-no- ..................................................... */
/*-no- ............................................................. */
/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ... */
/*-no-  */
/*-no- ................................................... */
/*-no- ............... */
/*-no-  */
/*-no- ...................................... */
/*-no- ............................................................... */
/*-no- ..................... */
/*-no- ................................. */
/*-no- ................................... */
/*-no-  */
/*-no- .............. */
/*-no- . */
/*-no-  */
/*-no- .............................. */
/*-no- .......................................... */
/*-no- ........................................... */
/*-no- ....................................................................... */
/*-no-  */
/*-no- ............................................................................. */
/*-no- .......................................................... */
/*-no- ....................................... */
/*-no-  */
/*-no- ......................................... */
/*-no- .................................. */
/*-no- ......................................... */
/*-no- ............................................................. */
/*-no- ............. */
/*-no- ........................................ */
/*-no- . */
/*-no-  */
/*-no- ............................................ */
/*-no- ............................................................. */
/*-no- . */
/*-no-  */
/*-no- ........................................................ */
/*-no- ........................................ */
/*-no- ................................................ */
/*-no- ................................ */
/*-no- .................................... */
/*-no- . */
/*-no-  */
/*-no- ......................................... */
/*-no- .................................................... */
/*-no- .................................................. */
/*-no- .................................... */
/*-no- .......................................................................... */
/*-no- .............................................. */
/*-no- ........................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ......................................... */
/*-no- ............................... */
/*-no- ...................................................... */
/*-no- ................. */
/*-no- ........................................... */
/*-no-  */
/*-no- ......................................................... */
/*-no- ............................... */
/*-no-  */
/*-no- .............. */
/*-no- .......................................................... */
/*-no- ........................................................... */
/*-no- ........................ */
/*-no- .................................................... */
/*-no- ..................... */
/*-no- .............................................. */
/*-no- ....... */
/*-no- ............ */
/*-no- ............................................ */
/*-no- ..... */
/*-no- ... */
/*-no-  */
/*-no- ..................... */
/*-no- . */
/*-no-  */
/*-no- ............................................ */
/*-no- ............... */
/*-no-  */
/*-no- ..................... */
/*-no- ................ */
/*-no-  */
/*-no- ................................................... */
/*-no- ....................................... */
/*-no- .................. */
/*-no- ... */
/*-no- ............... */
/*-no- . */
/*-no-  */
/*-no- .............................. */

#endif
/* CONSBENCH file end */
