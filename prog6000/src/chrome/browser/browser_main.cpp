/* CONSBENCH file begin */
#ifndef BROWSER_MAIN_CC_1697
#define BROWSER_MAIN_CC_1697

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_browser_browser_main_cpp, "chrome/browser/browser_main.cpp");

/* CONSBENCH includes begin */
#include "chrome/browser/browser_main.h"
#include <algorithm>
#include <string>
#include <vector>
#include "base/allocator/allocator_shim.h"
#include "base/at_exit.h"
#include "base/command_line.h"
#include "base/debug/trace_event.h"
#include "base/file_path.h"
#include "base/file_util.h"
#include "base/mac/scoped_nsautorelease_pool.h"
#include "base/metrics/field_trial.h"
#include "base/metrics/histogram.h"
#include "base/path_service.h"
#include "base/process_util.h"
#include "base/string_number_conversions.h"
#include "base/string_piece.h"
#include "base/string_split.h"
#include "base/string_util.h"
#include "base/sys_string_conversions.h"
#include "base/threading/platform_thread.h"
#include "base/threading/thread_restrictions.h"
#include "base/time.h"
#include "base/utf_string_conversions.h"
#include "base/values.h"
#include "build/build_config.h"
#include "chrome/browser/about_flags.h"
#include "chrome/browser/browser_main_win.h"
#include "chrome/browser/defaults.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/browser_process_impl.h"
#include "chrome/browser/browser_shutdown.h"
#include "chrome/browser/extensions/extension_protocols.h"
#include "chrome/browser/extensions/extension_service.h"
#include "chrome/browser/extensions/extensions_startup.h"
#include "chrome/browser/first_run/first_run.h"
#include "chrome/browser/gpu_data_manager.h"
#include "chrome/browser/jankometer.h"
#include "chrome/browser/metrics/histogram_synchronizer.h"
#include "chrome/browser/metrics/metrics_log.h"
#include "chrome/browser/metrics/metrics_service.h"
#include "chrome/browser/metrics/thread_watcher.h"
#include "chrome/browser/net/blob_url_request_job_factory.h"
#include "chrome/browser/net/chrome_dns_cert_provenance_checker.h"
#include "chrome/browser/net/chrome_dns_cert_provenance_checker_factory.h"
#include "chrome/browser/net/file_system_url_request_job_factory.h"
#include "chrome/browser/net/metadata_url_request.h"
#include "chrome/browser/net/predictor_api.h"
#include "chrome/browser/net/sdch_dictionary_fetcher.h"
#include "chrome/browser/net/websocket_experiment/websocket_experiment_runner.h"
#include "chrome/browser/prefs/browser_prefs.h"
#include "chrome/browser/prefs/pref_service.h"
#include "chrome/browser/prefs/pref_value_store.h"
#include "chrome/browser/prerender/prerender_field_trial.h"
#include "chrome/browser/printing/cloud_print/cloud_print_proxy_service.h"
#include "chrome/browser/printing/print_dialog_cloud.h"
#include "chrome/browser/process_singleton.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/profiles/profile_manager.h"
#include "chrome/browser/search_engines/search_engine_type.h"
#include "chrome/browser/search_engines/template_url.h"
#include "chrome/browser/search_engines/template_url_model.h"
#include "chrome/browser/service/service_process_control.h"
#include "chrome/browser/service/service_process_control_manager.h"
#include "chrome/browser/shell_integration.h"
#include "chrome/browser/translate/translate_manager.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/browser_init.h"
#include "chrome/browser/ui/webui/chrome_url_data_manager_backend.h"
#include "chrome/common/chrome_constants.h"
#include "chrome/common/chrome_paths.h"
#include "chrome/common/chrome_switches.h"
#include "chrome/common/env_vars.h"
#include "chrome/common/gfx_resource_provider.h"
#include "chrome/common/hi_res_timer_manager.h"
#include "chrome/common/json_pref_store.h"
#include "chrome/common/jstemplate_builder.h"
#include "chrome/common/logging_chrome.h"
#include "chrome/common/main_function_params.h"
#include "chrome/common/net/net_resource_provider.h"
#include "chrome/common/pref_names.h"
#include "chrome/common/profiling.h"
#include "chrome/common/result_codes.h"
#include "chrome/installer/util/google_update_settings.h"
#include "content/browser/browser_thread.h"
#include "content/browser/plugin_service.h"
#include "content/browser/renderer_host/resource_dispatcher_host.h"
#include "content/common/child_process.h"
#include "grit/app_locale_settings.h"
#include "grit/chromium_strings.h"
#include "grit/generated_resources.h"
#include "net/base/cookie_monster.h"
#include "net/base/net_module.h"
#include "net/base/network_change_notifier.h"
#include "net/http/http_network_layer.h"
#include "net/http/http_stream_factory.h"
#include "net/socket/client_socket_pool_base.h"
#include "net/socket/client_socket_pool_manager.h"
#include "net/socket/tcp_client_socket.h"
#include "net/spdy/spdy_session.h"
#include "net/spdy/spdy_session_pool.h"
#include "net/url_request/url_request.h"
#include "net/url_request/url_request_throttler_manager.h"
#include "ui/base/l10n/l10n_util.h"
#include "ui/base/resource/resource_bundle.h"
#include "ui/base/system_monitor/system_monitor.h"
#include "ui/gfx/gfx_module.h"
#include "base/linux_util.h"
#include "chrome/app/breakpad_linux.h"
#include <dbus/dbus-glib.h>
#include "chrome/browser/browser_main_gtk.h"
#include "chrome/browser/ui/gtk/gtk_util.h"
#include "chrome/browser/chromeos/boot_times_loader.h"
#include "chrome/browser/oom_priority_manager.h"
#include "app/win/scoped_com_initializer.h"
#include "base/win/windows_version.h"
#include "chrome/browser/browser_trial.h"
#include "chrome/browser/metrics/user_metrics.h"
#include "chrome/browser/net/url_fixer_upper.h"
#include "chrome/browser/rlz/rlz.h"
#include "chrome/browser/ui/views/user_data_dir_dialog.h"
#include "chrome/common/sandbox_policy.h"
#include "chrome/installer/util/helper.h"
#include "chrome/installer/util/install_util.h"
#include "chrome/installer/util/shell_util.h"
#include "net/base/net_util.h"
#include "net/base/sdch_manager.h"
#include "printing/printed_document.h"
#include "sandbox/src/sandbox.h"
#include "ui/base/l10n/l10n_util_win.h"
#include "ui/gfx/platform_font_win.h"
#include "chrome/browser/cocoa/install_from_dmg.h"
#include "chrome/browser/ui/views/chrome_views_delegate.h"
#include "views/focus/accelerator_handler.h"
#include "views/widget/widget_gtk.h"
#include "chrome/browser/chromeos/cros/cros_library.h"
#include "chrome/browser/chromeos/cros/screen_lock_library.h"
#include "chrome/browser/chromeos/customization_document.h"
#include "chrome/browser/chromeos/external_metrics.h"
#include "chrome/browser/chromeos/login/authenticator.h"
#include "chrome/browser/chromeos/login/login_utils.h"
#include "chrome/browser/chromeos/login/ownership_service.h"
#include "chrome/browser/chromeos/login/screen_locker.h"
#include "chrome/browser/chromeos/login/user_manager.h"
#include "chrome/browser/chromeos/metrics_cros_settings_provider.h"
#include "chrome/browser/ui/views/browser_dialogs.h"
#include "ui/gfx/gtk_util.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_browser_browser_main_cpp, "chrome/browser/browser_main.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(chrome_browser_browser_main_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include "chrome/browser/browser_main.h" */
/*-no-  */
/*-no- #include <algorithm> */
/*-no- #include <string> */
/*-no- #include <vector> */
/*-no-  */
/*-no- #include "base/allocator/allocator_shim.h" */
/*-no- #include "base/at_exit.h" */
/*-no- #include "base/command_line.h" */
/*-no- #include "base/debug/trace_event.h" */
/*-no- #include "base/file_path.h" */
/*-no- #include "base/file_util.h" */
/*-no- #include "base/mac/scoped_nsautorelease_pool.h" */
/*-no- #include "base/metrics/field_trial.h" */
/*-no- #include "base/metrics/histogram.h" */
/*-no- #include "base/path_service.h" */
/*-no- #include "base/process_util.h" */
/*-no- #include "base/string_number_conversions.h" */
/*-no- #include "base/string_piece.h" */
/*-no- #include "base/string_split.h" */
/*-no- #include "base/string_util.h" */
/*-no- #include "base/sys_string_conversions.h" */
/*-no- #include "base/threading/platform_thread.h" */
/*-no- #include "base/threading/thread_restrictions.h" */
/*-no- #include "base/time.h" */
/*-no- #include "base/utf_string_conversions.h" */
/*-no- #include "base/values.h" */
/*-no- #include "build/build_config.h" */
/*-no- #include "chrome/browser/about_flags.h" */
/*-no- #include "chrome/browser/browser_main_win.h" */
/*-no- #include "chrome/browser/defaults.h" */
/*-no- #include "chrome/browser/browser_process.h" */
/*-no- #include "chrome/browser/browser_process_impl.h" */
/*-no- #include "chrome/browser/browser_shutdown.h" */
/*-no- #include "chrome/browser/extensions/extension_protocols.h" */
/*-no- #include "chrome/browser/extensions/extension_service.h" */
/*-no- #include "chrome/browser/extensions/extensions_startup.h" */
/*-no- #include "chrome/browser/first_run/first_run.h" */
/*-no- #include "chrome/browser/gpu_data_manager.h" */
/*-no- #include "chrome/browser/jankometer.h" */
/*-no- #include "chrome/browser/metrics/histogram_synchronizer.h" */
/*-no- #include "chrome/browser/metrics/metrics_log.h" */
/*-no- #include "chrome/browser/metrics/metrics_service.h" */
/*-no- #include "chrome/browser/metrics/thread_watcher.h" */
/*-no- #include "chrome/browser/net/blob_url_request_job_factory.h" */
/*-no- #include "chrome/browser/net/chrome_dns_cert_provenance_checker.h" */
/*-no- #include "chrome/browser/net/chrome_dns_cert_provenance_checker_factory.h" */
/*-no- #include "chrome/browser/net/file_system_url_request_job_factory.h" */
/*-no- #include "chrome/browser/net/metadata_url_request.h" */
/*-no- #include "chrome/browser/net/predictor_api.h" */
/*-no- #include "chrome/browser/net/sdch_dictionary_fetcher.h" */
/*-no- #include "chrome/browser/net/websocket_experiment/websocket_experiment_runner.h" */
/*-no- #include "chrome/browser/prefs/browser_prefs.h" */
/*-no- #include "chrome/browser/prefs/pref_service.h" */
/*-no- #include "chrome/browser/prefs/pref_value_store.h" */
/*-no- #include "chrome/browser/prerender/prerender_field_trial.h" */
/*-no- #include "chrome/browser/printing/cloud_print/cloud_print_proxy_service.h" */
/*-no- #include "chrome/browser/printing/print_dialog_cloud.h" */
/*-no- #include "chrome/browser/process_singleton.h" */
/*-no- #include "chrome/browser/profiles/profile.h" */
/*-no- #include "chrome/browser/profiles/profile_manager.h" */
/*-no- #include "chrome/browser/search_engines/search_engine_type.h" */
/*-no- #include "chrome/browser/search_engines/template_url.h" */
/*-no- #include "chrome/browser/search_engines/template_url_model.h" */
/*-no- #include "chrome/browser/service/service_process_control.h" */
/*-no- #include "chrome/browser/service/service_process_control_manager.h" */
/*-no- #include "chrome/browser/shell_integration.h" */
/*-no- #include "chrome/browser/translate/translate_manager.h" */
/*-no- #include "chrome/browser/ui/browser.h" */
/*-no- #include "chrome/browser/ui/browser_init.h" */
/*-no- #include "chrome/browser/ui/webui/chrome_url_data_manager_backend.h" */
/*-no- #include "chrome/common/chrome_constants.h" */
/*-no- #include "chrome/common/chrome_paths.h" */
/*-no- #include "chrome/common/chrome_switches.h" */
/*-no- #include "chrome/common/env_vars.h" */
/*-no- #include "chrome/common/gfx_resource_provider.h" */
/*-no- #include "chrome/common/hi_res_timer_manager.h" */
/*-no- #include "chrome/common/json_pref_store.h" */
/*-no- #include "chrome/common/jstemplate_builder.h" */
/*-no- #include "chrome/common/logging_chrome.h" */
/*-no- #include "chrome/common/main_function_params.h" */
/*-no- #include "chrome/common/net/net_resource_provider.h" */
/*-no- #include "chrome/common/pref_names.h" */
/*-no- #include "chrome/common/profiling.h" */
/*-no- #include "chrome/common/result_codes.h" */
/*-no- #include "chrome/installer/util/google_update_settings.h" */
/*-no- #include "content/browser/browser_thread.h" */
/*-no- #include "content/browser/plugin_service.h" */
/*-no- #include "content/browser/renderer_host/resource_dispatcher_host.h" */
/*-no- #include "content/common/child_process.h" */
/*-no- #include "grit/app_locale_settings.h" */
/*-no- #include "grit/chromium_strings.h" */
/*-no- #include "grit/generated_resources.h" */
/*-no- #include "net/base/cookie_monster.h" */
/*-no- #include "net/base/net_module.h" */
/*-no- #include "net/base/network_change_notifier.h" */
/*-no- #include "net/http/http_network_layer.h" */
/*-no- #include "net/http/http_stream_factory.h" */
/*-no- #include "net/socket/client_socket_pool_base.h" */
/*-no- #include "net/socket/client_socket_pool_manager.h" */
/*-no- #include "net/socket/tcp_client_socket.h" */
/*-no- #include "net/spdy/spdy_session.h" */
/*-no- #include "net/spdy/spdy_session_pool.h" */
/*-no- #include "net/url_request/url_request.h" */
/*-no- #include "net/url_request/url_request_throttler_manager.h" */
/*-no- #include "ui/base/l10n/l10n_util.h" */
/*-no- #include "ui/base/resource/resource_bundle.h" */
/*-no- #include "ui/base/system_monitor/system_monitor.h" */
/*-no- #include "ui/gfx/gfx_module.h" */
/*-no-  */
/*-no- #if defined(USE_LINUX_BREAKPAD) */
/*-no- #include "base/linux_util.h" */
/*-no- #include "chrome/app/breakpad_linux.h" */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(OS_POSIX) && !defined(OS_MACOSX) */
/*-no- #include <dbus/dbus-glib.h> */
/*-no- #include "chrome/browser/browser_main_gtk.h" */
/*-no- #include "chrome/browser/ui/gtk/gtk_util.h" */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- #include "chrome/browser/chromeos/boot_times_loader.h" */
/*-no- #include "chrome/browser/oom_priority_manager.h" */
/*-no- #endif */
/*-no-  */
/*-no- ......................................................................... */
/*-no- ........................................................................... */
/*-no- ........................................................................ */
/*-no- ........................................................................... */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- #include <commctrl.h> */
/*-no- #include <shellapi.h> */
/*-no- #include <windows.h> */
/*-no-  */
/*-no- #include "app/win/scoped_com_initializer.h" */
/*-no- #include "base/win/windows_version.h" */
/*-no- #include "chrome/browser/browser_trial.h" */
/*-no- #include "chrome/browser/metrics/user_metrics.h" */
/*-no- #include "chrome/browser/net/url_fixer_upper.h" */
/*-no- #include "chrome/browser/rlz/rlz.h" */
/*-no- #include "chrome/browser/ui/views/user_data_dir_dialog.h" */
/*-no- #include "chrome/common/sandbox_policy.h" */
/*-no- #include "chrome/installer/util/helper.h" */
/*-no- #include "chrome/installer/util/install_util.h" */
/*-no- #include "chrome/installer/util/shell_util.h" */
/*-no- #include "net/base/net_util.h" */
/*-no- #include "net/base/sdch_manager.h" */
/*-no- #include "printing/printed_document.h" */
/*-no- #include "sandbox/src/sandbox.h" */
/*-no- #include "ui/base/l10n/l10n_util_win.h" */
/*-no- #include "ui/gfx/platform_font_win.h" */
/*-no- #endif  // defined(OS_WIN) */
/*-no-  */
/*-no- #if defined(OS_MACOSX) */
/*-no- #include <Security/Security.h> */
/*-no- #include "chrome/browser/cocoa/install_from_dmg.h" */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(TOOLKIT_VIEWS) */
/*-no- #include "chrome/browser/ui/views/chrome_views_delegate.h" */
/*-no- #include "views/focus/accelerator_handler.h" */
/*-no- #if defined(TOOLKIT_USES_GTK) */
/*-no- #include "views/widget/widget_gtk.h" */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- #include "chrome/browser/chromeos/cros/cros_library.h" */
/*-no- #include "chrome/browser/chromeos/cros/screen_lock_library.h" */
/*-no- #include "chrome/browser/chromeos/customization_document.h" */
/*-no- #include "chrome/browser/chromeos/external_metrics.h" */
/*-no- #include "chrome/browser/chromeos/login/authenticator.h" */
/*-no- #include "chrome/browser/chromeos/login/login_utils.h" */
/*-no- #include "chrome/browser/chromeos/login/ownership_service.h" */
/*-no- #include "chrome/browser/chromeos/login/screen_locker.h" */
/*-no- #include "chrome/browser/chromeos/login/user_manager.h" */
/*-no- #include "chrome/browser/chromeos/metrics_cros_settings_provider.h" */
/*-no- #include "chrome/browser/ui/views/browser_dialogs.h" */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(TOOLKIT_USES_GTK) */
/*-no- #include "ui/gfx/gtk_util.h" */
/*-no- #endif */
/*-no-  */
/*-no- ................................................................................ */
/*-no-  */
/*-no- ........................................................................ */
/*-no- .............................. */
/*-no- ...................................................... */
/*-no- . */
/*-no-  */
/*-no- ....................................... */
/*-no- . */
/*-no-  */
/*-no- ................................................................................ */
/*-no-  */
/*-no- .............................................. */
/*-no- ........................... */
/*-no-  */
/*-no- ..................................................................... */
/*-no- ........................................... */
/*-no-  */
/*-no- .................. */
/*-no-  */
/*-no- .................................................................... */
/*-no- .......................................... */
/*-no- ....................................................................... */
/*-no- ............................................... */
/*-no- ...................................................................... */
/*-no- .............................................. */
/*-no- ............................................. */
/*-no- ..................................................................... */
/*-no- ................................................ */
/*-no- ...................................... */
/*-no- ........................................................ */
/*-no- ............................................................. */
/*-no- ... */
/*-no-  */
/*-no- .................................................................... */
/*-no- ........................................ */
/*-no-  */
/*-no- ............................ */
/*-no- . */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ........................................... */
/*-no- .......................................................... */
/*-no- .................................. */
/*-no- ......................... */
/*-no- ............................ */
/*-no- ............................... */
/*-no- .................................................................. */
/*-no- ................... */
/*-no- ................................ */
/*-no- ............................ */
/*-no- . */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ............................................................................ */
/*-no- ............................................................................... */
/*-no- ................................................................................ */
/*-no- ................................................................................ */
/*-no- .............. */
/*-no- ............................................... */
/*-no- ............................................................ */
/*-no- ........................................................................... */
/*-no-  */
/*-no- .................................................................... */
/*-no- ................................................ */
/*-no- ........................... */
/*-no- ............................................................................ */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ................................................................................ */
/*-no- ...................................................................... */
/*-no- ............................................................................... */
/*-no- ........................................................... */
/*-no-  */
/*-no- .................................................................. */
/*-no- ........................................................................ */
/*-no- .................................................................. */
/*-no- ........................................................................ */
/*-no- .................................................................. */
/*-no- ........................................................................ */
/*-no- .................................................................. */
/*-no- ........................................................................ */
/*-no-  */
/*-no- ......................................................... */
/*-no-  */
/*-no- ......................................... */
/*-no- ............................................................... */
/*-no- ................................................ */
/*-no- ............................................................... */
/*-no- ................................................ */
/*-no- ............................................................... */
/*-no- ................................................ */
/*-no- ............................................................... */
/*-no- ................................................ */
/*-no- ............................................................... */
/*-no- .......... */
/*-no- ................. */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ............................................................................... */
/*-no- ........................................................................... */
/*-no- .......................................................................... */
/*-no- .................................................................. */
/*-no- .................................................. */
/*-no- ...................................................................... */
/*-no- .................................................. */
/*-no- .................................................................... */
/*-no-  */
/*-no- .................................................................... */
/*-no- ....................................................... */
/*-no- ........................................................................ */
/*-no- ........................................... */
/*-no- .......................................................................... */
/*-no-  */
/*-no- .............................. */
/*-no- ......................................................... */
/*-no- ................................................................... */
/*-no- ............................... */
/*-no- .......................................................... */
/*-no- ................................................................... */
/*-no- ............................... */
/*-no- .......................................................... */
/*-no- ................................................................... */
/*-no-  */
/*-no- ................................................................ */
/*-no-  */
/*-no- ................................................ */
/*-no- ............................................................. */
/*-no- ........................................................ */
/*-no- .............................................................. */
/*-no- ........................................................ */
/*-no- .............................................................. */
/*-no- ........................................................ */
/*-no- .............................................................. */
/*-no- .......... */
/*-no- ................. */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ..................................................... */
/*-no- ..................................................................... */
/*-no- .................... */
/*-no- ...................................................................... */
/*-no-  */
/*-no- .................................................................... */
/*-no- ......................................................... */
/*-no- ............................................................................. */
/*-no- ................................................ */
/*-no-  */
/*-no- ......................................................................... */
/*-no- ................................................................................ */
/*-no- ................................................................................ */
/*-no- .......................................................................... */
/*-no- ............................................................................... */
/*-no-  */
/*-no- .............................................................................. */
/*-no- ...................................................................... */
/*-no- .................................. */
/*-no- .................................. */
/*-no- ................................................................. */
/*-no- ....................................................................... */
/*-no- .................................. */
/*-no- ................................................................. */
/*-no- ....................................................................... */
/*-no-  */
/*-no- ............................................................................ */
/*-no-  */
/*-no- .............................................................. */
/*-no- ....................................................................... */
/*-no- ..................................................................... */
/*-no- ....................................................................... */
/*-no- ..................................................................... */
/*-no- ....................................................................... */
/*-no- .......... */
/*-no- ................. */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ............................................................... */
/*-no- ............................................................................... */
/*-no- ................................................................. */
/*-no- ............................................................................. */
/*-no- ................................................................ */
/*-no- .......................................................................... */
/*-no- ............................................ */
/*-no- ......................................... */
/*-no- ............................. */
/*-no- ............................................................ */
/*-no- ........................... */
/*-no- ...................................................................... */
/*-no- ................................................. */
/*-no- .......... */
/*-no- ........................................................... */
/*-no- ............................ */
/*-no- ........................................................... */
/*-no-  */
/*-no- ...................................................................... */
/*-no- .......................................... */
/*-no- ............................. */
/*-no- ....................................................................... */
/*-no-  */
/*-no- ............................................ */
/*-no- .................................................. */
/*-no-  */
/*-no- ........................................... */
/*-no- ................................................................................ */
/*-no-  */
/*-no- ................................... */
/*-no- .................................... */
/*-no- ........................... */
/*-no- .................................................... */
/*-no- ........................................... */
/*-no- ........................... */
/*-no- ............................................... */
/*-no- ............ */
/*-no- ............................ */
/*-no- ..... */
/*-no- ... */
/*-no-  */
/*-no- ................................. */
/*-no- ............................................................. */
/*-no- .......................................................................... */
/*-no- .......................................................................... */
/*-no- .............................................................................. */
/*-no- .............................................................................. */
/*-no-  */
/*-no- ................................................................... */
/*-no- ................... */
/*-no- ........................................ */
/*-no- ........................... */
/*-no- ..................................................................... */
/*-no-  */
/*-no- ............................................ */
/*-no- ............................................ */
/*-no- .................................................. */
/*-no- .................................................. */
/*-no-  */
/*-no- ............................................................................. */
/*-no- .................. */
/*-no- ................................................................ */
/*-no- ................................................. */
/*-no- ................ */
/*-no- .................. */
/*-no- .......................................................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- .................................................. */
/*-no- ......................................................................... */
/*-no- ............................................... */
/*-no- ........... */
/*-no- ... */
/*-no-  */
/*-no- ..................................................... */
/*-no- ............................................................................ */
/*-no-  */
/*-no- .................................................................... */
/*-no- ........................................ */
/*-no- ........................... */
/*-no- ......................................................................... */
/*-no-  */
/*-no- ................................................................ */
/*-no- .................................................................. */
/*-no-  */
/*-no- ................................. */
/*-no- .................................. */
/*-no- ............................................... */
/*-no- . */
/*-no-  */
/*-no-  */
/*-no- .............................................................................. */
/*-no- ....................................................................... */
/*-no- ........................................................ */
/*-no- ...................................................... */
/*-no- ............................................................................ */
/*-no- ............................................................................... */
/*-no- .............. */
/*-no- ............................................. */
/*-no- ............................................... */
/*-no- ............................................................................... */
/*-no- ............... */
/*-no- .......... */
/*-no- ........................................................................ */
/*-no- ...................... */
/*-no- .......................................................................... */
/*-no- ..................................................................... */
/*-no- .......................................... */
/*-no- .................................................. */
/*-no- ........................................................................... */
/*-no- ..................... */
/*-no- ....................................................................... */
/*-no- ................................................... */
/*-no- ...................................................... */
/*-no- ........................................... */
/*-no- ............................................................................... */
/*-no- .................................................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ................................................................................ */
/*-no-  */
/*-no- ............................................... */
/*-no- ............................ */
/*-no-  */
/*-no- .................................................................. */
/*-no-  */
/*-no- ............................................................................... */
/*-no- ............................................... */
/*-no- .............................................................. */
/*-no- ....................................................................... */
/*-no-  */
/*-no- ......................... */
/*-no-  */
/*-no- ............................. */
/*-no- ...................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................... */
/*-no- ............................................ */
/*-no- ............................................. */
/*-no- ................................................... */
/*-no-  */
/*-no- .............................................................................. */
/*-no- ......................................................... */
/*-no- ................................................................ */
/*-no- . */
/*-no-  */
/*-no- ................................................................................ */
/*-no- ........................................................................... */
/*-no-  */
/*-no- ........... */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ........................... */
/*-no- ............................................................ */
/*-no- ........................................ */
/*-no- ............................................................. */
/*-no- .................. */
/*-no- ... */
/*-no-  */
/*-no- ......................................................................... */
/*-no- ............................................................ */
/*-no- ............................ */
/*-no- ..................... */
/*-no- ... */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ............................................................................... */
/*-no- ............................................................................... */
/*-no- ............................................... */
/*-no- ......................................................... */
/*-no- ........................................................ */
/*-no- #endif */
/*-no- . */
/*-no-  */
/*-no- ........................................................ */
/*-no- ....................................................... */
/*-no-  */
/*-no- ........................................................... */
/*-no- ............................................. */
/*-no- ................................................ */
/*-no-  */
/*-no- #if defined(TOOLKIT_VIEWS) */
/*-no- ................................................ */
/*-no- ......................................................... */
/*-no- #elif defined(USE_X11) */
/*-no- ......................................... */
/*-no- #elif defined(OS_POSIX) */
/*-no- ..................................... */
/*-no- #endif */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ............................................................................. */
/*-no- .............................................................. */
/*-no- #endif */
/*-no-  */
/*-no- ..................................................... */
/*-no- . */
/*-no-  */
/*-no- .................................................. */
/*-no- ............................................................ */
/*-no- ............................................................... */
/*-no- .................................... */
/*-no- ....................... */
/*-no- ........................................ */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- #if defined(USE_LINUX_BREAKPAD) */
/*-no- ........................................ */
/*-no- ........ */
/*-no- .................................. */
/*-no-  */
/*-no- ...................... */
/*-no- ........................................................................ */
/*-no- ... */
/*-no-  */
/*-no- ............................................... */
/*-no- .. */
/*-no- #endif  // USE_LINUX_BREAKPAD */
/*-no-  */
/*-no- ....................................................................... */
/*-no- .................................................................... */
/*-no- ............................................................................. */
/*-no- .................................................................. */
/*-no- ........................................... */
/*-no- ... */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ................................................................ */
/*-no-  */
/*-no- .......................................................... */
/*-no- ................................................ */
/*-no- ....................................................................... */
/*-no-  */
/*-no- ........................................................................... */
/*-no- .............. */
/*-no- .............................................................. */
/*-no- ................................................. */
/*-no- ................ */
/*-no- ............................................................. */
/*-no- ... */
/*-no-  */
/*-no- .................................................................... */
/*-no- ........................................................................... */
/*-no-  */
/*-no- ........................................................................... */
/*-no- . */
/*-no-  */
/*-no- ................................................................. */
/*-no- ............................................................................... */
/*-no- ...................... */
/*-no- ...................................................... */
/*-no- ....................... */
/*-no- ......................... */
/*-no- ..................................... */
/*-no- .......................... */
/*-no- ....................... */
/*-no- ............................................................................ */
/*-no- ...................................................... */
/*-no- ............................. */
/*-no- . */
/*-no-  */
/*-no- ........................................................... */
/*-no- ......................................................................... */
/*-no- ...................................................... */
/*-no- ............................ */
/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ........................ */
/*-no- .............................................................. */
/*-no- ...................... */
/*-no-  */
/*-no- ..................................................................... */
/*-no- .................................................................. */
/*-no- ............................. */
/*-no- ....................................................................... */
/*-no- ............................................................................. */
/*-no- .................... */
/*-no- ............................................................................ */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ...................................................................... */
/*-no- ................................................................. */
/*-no- ................................................. */
/*-no- #endif  // defined(OS_CHROMEOS) */
/*-no- #if !defined(OS_CHROMEOS) */
/*-no- ................................................................... */
/*-no- ...................................................... */
/*-no- #endif  // !defined(OS_CHROMEOS) */
/*-no-  */
/*-no- ..................... */
/*-no- #if defined(OS_WIN) */
/*-no- ........................................................................... */
/*-no- ...................................................................... */
/*-no- ................................................. */
/*-no- .............................................. */
/*-no- .............................. */
/*-no- ........................................................... */
/*-no- ....................................................... */
/*-no- ........................................................ */
/*-no- ..... */
/*-no- #endif  // defined(OS_WIN) */
/*-no- ... */
/*-no-  */
/*-no- .......................................................................... */
/*-no- ............................................................................. */
/*-no- ......................................... */
/*-no- ............................................................................. */
/*-no- .......................................................................... */
/*-no- ............................................................................ */
/*-no- ......................................................................... */
/*-no- .......................................... */
/*-no- ................................. */
/*-no- ................................................................ */
/*-no- ............................. */
/*-no- ......................................................................... */
/*-no- ............................................... */
/*-no- .................................................................... */
/*-no- ..................................................................... */
/*-no- .......................................................... */
/*-no- ............................................................................. */
/*-no- ........................... */
/*-no- .................................. */
/*-no- .................................................................. */
/*-no- ... */
/*-no-  */
/*-no- ..................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................................. */
/*-no- .................................................................... */
/*-no- ................................................................... */
/*-no- ....................................................................... */
/*-no- #if defined(OS_WIN) */
/*-no- ............................................ */
/*-no- ................................................ */
/*-no- ........................ */
/*-no- ................................................. */
/*-no- ............................................................... */
/*-no- ....................................................... */
/*-no- ............................................................. */
/*-no- ........................................................................ */
/*-no- ...................................................................... */
/*-no- .............................................................................. */
/*-no- ............................................................................ */
/*-no- ........................ */
/*-no- ..... */
/*-no- ... */
/*-no- #endif */
/*-no- . */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ....................................................... */
/*-no- ......................................................................... */
/*-no- ................................................................... */
/*-no- #if defined(OS_WIN) */
/*-no- ............................................................ */
/*-no- ............................................ */
/*-no- #elif defined(ARCH_CPU_64_BITS) */
/*-no- ........................................... */
/*-no- #endif  // defined(OS_WIN) */
/*-no-  */
/*-no- ................................................................. */
/*-no-  */
/*-no- ....................................................................... */
/*-no- ..................................................................... */
/*-no- ............................................................................... */
/*-no- ............................................................. */
/*-no- .................................. */
/*-no- .......... */
/*-no- ..................................................................... */
/*-no- ...................................................................... */
/*-no- ........................... */
/*-no- #if defined(GOOGLE_CHROME_BUILD) */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ............................................................................. */
/*-no- #else */
/*-no- ............................................................................ */
/*-no- #endif  // #if defined(OS_CHROMEOS) */
/*-no- ........................................... */
/*-no- .................. */
/*-no- ....................... */
/*-no- ............................................... */
/*-no- ............................................. */
/*-no- ..... */
/*-no- #endif */
/*-no- ... */
/*-no-  */
/*-no- ................. */
/*-no- . */
/*-no-  */
/*-no- ........................................................................ */
/*-no- .......................................................................... */
/*-no- ....................... */
/*-no- ............................................................ */
/*-no- ....................................................... */
/*-no- ............................................................................. */
/*-no- ..................... */
/*-no- .............. */
/*-no- ................... */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- ....................................................................... */
/*-no- ......................................................................... */
/*-no- ...................... */
/*-no- ............................................................. */
/*-no- ....................................................................... */
/*-no- ..................... */
/*-no- .............................................................................. */
/*-no- ............................................................................. */
/*-no- ........................................................................... */
/*-no- ......................................... */
/*-no- ............................................ */
/*-no- ... */
/*-no-  */
/*-no- ................................... */
/*-no- ............................................................................. */
/*-no- .................................................................... */
/*-no- ............................................................................ */
/*-no- .................................................................... */
/*-no- ............................................................ */
/*-no- ............................................................. */
/*-no- ......................................................... */
/*-no- .......................................................... */
/*-no- ... */
/*-no- #else */
/*-no- ............................................................... */
/*-no- .................................................................. */
/*-no- ............................................................. */
/*-no- .................................................. */
/*-no- #endif */
/*-no-  */
/*-no- .............. */
/*-no- . */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no-  */
/*-no- ...................... */
/*-no- ..................................... */
/*-no- ................................... */
/*-no- . */
/*-no-  */
/*-no- .......................... */
/*-no- .................... */
/*-no- ........................................................................ */
/*-no- .................................... */
/*-no- ....................... */
/*-no- . */
/*-no-  */
/*-no- #elif defined(OS_CHROMEOS) */
/*-no- ............................................................... */
/*-no- ................................................................. */
/*-no- ....................................................................... */
/*-no- ....................................................................... */
/*-no- ...................................................... */
/*-no- .......................... */
/*-no- ............................... */
/*-no- ........................................................ */
/*-no- .............................................................. */
/*-no- .................................................... */
/*-no- ........... */
/*-no- ... */
/*-no- .................................. */
/*-no-  */
/*-no- ............................................................................. */
/*-no- .................................. */
/*-no- ............................................................................. */
/*-no- .................................................... */
/*-no- ............................. */
/*-no- ................................................................. */
/*-no- ............................................. */
/*-no- ........... */
/*-no- ..................................................................... */
/*-no- ............................................. */
/*-no- . */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(TOOLKIT_USES_GTK) */
/*-no- ................................................... */
/*-no- .................................................... */
/*-no- ................................................ */
/*-no- ............................................... */
/*-no- .................. */
/*-no- ............................. */
/*-no- ............... */
/*-no- ............................. */
/*-no-  */
/*-no- ................................................... */
/*-no- ....................................................... */
/*-no- ............................ */
/*-no- .............................................................................. */
/*-no- ............................................................................... */
/*-no- ................................................ */
/*-no- ................................ */
/*-no- ................... */
/*-no- .................................................................. */
/*-no- ..................... */
/*-no- ..... */
/*-no- ...................................................................... */
/*-no- ............................................................. */
/*-no- ............................................ */
/*-no- ............................................................................ */
/*-no- ............................. */
/*-no- ................................................................ */
/*-no- .................................................. */
/*-no- ............................................... */
/*-no- ................................................. */
/*-no- ................................................................. */
/*-no- ..................................................................... */
/*-no- .......... */
/*-no- #ifdef NDEBUG */
/*-no- ................................................ */
/*-no- #else */
/*-no- ................................................ */
/*-no- #endif */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ................................... */
/*-no- ....................................................................... */
/*-no- ............................................................................. */
/*-no- ....................................................... */
/*-no- ..................................... */
/*-no- ........................................................................ */
/*-no- .................................................................... */
/*-no- ..................................................................... */
/*-no- ........................................................................ */
/*-no- ....................................................................... */
/*-no- ..................................... */
/*-no- ............................ */
/*-no- ... */
/*-no- . */
/*-no- #endif */
/*-no-  */
/*-no- .............................................................. */
/*-no- ................................................................... */
/*-no- ...................................................................... */
/*-no- ....................................... */
/*-no-  */
/*-no- #if defined(TOOLKIT_USES_GTK) */
/*-no- ...................................................................... */
/*-no- .................................................................. */
/*-no- .................................................................... */
/*-no- ..................................................................... */
/*-no- ..................................................................... */
/*-no- ................................................................... */
/*-no- ........... */
/*-no- ............................ */
/*-no- ........................ */
/*-no- ................................................................ */
/*-no- ................................................................. */
/*-no- ............................................................. */
/*-no- ........................................................... */
/*-no- ........................... */
/*-no- ................ */
/*-no- ....................................................................... */
/*-no- ....................................................................... */
/*-no- ............. */
/*-no- ....................... */
/*-no- ........................................................ */
/*-no- ........................ */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(TOOLKIT_GTK) */
/*-no- .......................................................................... */
/*-no- ............................... */
/*-no- ........................... */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(TOOLKIT_VIEWS) */
/*-no- .......................................................................... */
/*-no- .............................. */
/*-no- ............................................ */
/*-no- ................................................................... */
/*-no-  */
/*-no- #if defined(TOOLKIT_USES_GTK) */
/*-no- ................................................................. */
/*-no- ..................................................................... */
/*-no- ......................................... */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- ............................................................. */
/*-no- ............................................................................. */
/*-no-  */
/*-no- ............................. */
/*-no- .............................. */
/*-no- ................................. */
/*-no- ................................... */
/*-no- ................................ */
/*-no- #endif */
/*-no- . */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no-  */
/*-no- ............................................................. */
/*-no- ........................................................ */
/*-no- ........................................................ */
/*-no- ........ */
/*-no- .................................................................. */
/*-no- ............................................................................ */
/*-no- .............................................. */
/*-no- .................................................................. */
/*-no- ................. */
/*-no- ................. */
/*-no- ...................... */
/*-no- ....................... */
/*-no- ... */
/*-no-  */
/*-no- ............................................................ */
/*-no- .............................................................. */
/*-no- ................ */
/*-no- ... */
/*-no-  */
/*-no- .................................................. */
/*-no- .................................................. */
/*-no- ............................................................................. */
/*-no- .............................................. */
/*-no- ........................................................ */
/*-no- ........................................................ */
/*-no- ........................................................... */
/*-no- ................................................................. */
/*-no- ................ */
/*-no- ... */
/*-no-  */
/*-no- ........................................................ */
/*-no- .. */
/*-no-  */
/*-no- ................................................................................ */
/*-no- ............................................................... */
/*-no- .............................. */
/*-no- ........................................................................ */
/*-no- .......................... */
/*-no- ................................................ */
/*-no- ........................................ */
/*-no- ......................... */
/*-no- ........................................................................... */
/*-no- ....................................................................... */
/*-no- .......................... */
/*-no- .......................................... */
/*-no- .................................................... */
/*-no- ................................... */
/*-no- .......................... */
/*-no- ....................................................... */
/*-no- ...................................................... */
/*-no- ...................................... */
/*-no- ....... */
/*-no- ..... */
/*-no- ................................................. */
/*-no- ................................................................... */
/*-no- ................................................................ */
/*-no- ............................................................. */
/*-no- .................. */
/*-no- ...................................................................... */
/*-no- ........................................................................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- #else */
/*-no-  */
/*-no- ................................................................................ */
/*-no- .............................................................................. */
/*-no- . */
/*-no-  */
/*-no- #endif  // defined(OS_CHROMEOS) */
/*-no-  */
/*-no- #if defined(OS_MACOSX) */
/*-no- .......................................................... */
/*-no- ......................................................................... */
/*-no- ............... */
/*-no- . */
/*-no- #endif */
/*-no-  */
/*-no- ............... */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ............................................................................... */
/*-no- .......................................... */
/*-no- ............................................ */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- #define DLLEXPORT __declspec(dllexport) */
/*-no-  */
/*-no- .......................................................................... */
/*-no- ............ */
/*-no- ......................................................................... */
/*-no- . */
/*-no-  */
/*-no- .......................................................................... */
/*-no- ........................................................................ */
/*-no- ............................................................................ */
/*-no- ............................................................... */
/*-no- ................................... */
/*-no- ............................................................. */
/*-no- . */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(USE_LINUX_BREAKPAD) */
/*-no- ................................................................ */
/*-no- .............................................................................. */
/*-no- ..................................................... */
/*-no- ............................................................................... */
/*-no- ......................................................................... */
/*-no- ............................................................. */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ......................... */
/*-no- ................................................................ */
/*-no- ........................ */
/*-no- ........................................................... */
/*-no- #else */
/*-no- ............................................................ */
/*-no- ................................................................... */
/*-no- ................................... */
/*-no- ......................... */
/*-no- ............................................................... */
/*-no- ......................................................................... */
/*-no- ........................................................... */
/*-no- #endif  // #if defined(OS_CHROMEOS) */
/*-no- .......................... */
/*-no- . */
/*-no- #endif  // #if defined(USE_LINUX_BREAKPAD) */
/*-no-  */
/*-no- ................................................... */
/*-no- ....................................................... */
/*-no- .......................................... */
/*-no-  */
/*-no- .................................................. */
/*-no- ......................... */
/*-no- .............................................. */
/*-no-  */
/*-no- .............................. */
/*-no- .................................................................. */
/*-no-  */
/*-no- ............................... */
/*-no-  */
/*-no- ....................................................................... */
/*-no- ................................ */
/*-no-  */
/*-no- ................................ */
/*-no-  */
/*-no- .......................................................................... */
/*-no- .............................................................................. */
/*-no- ................................................................. */
/*-no-  */
/*-no- .................................. */
/*-no- .............................................................................. */
/*-no- ........................................................................ */
/*-no- ...................................................................... */
/*-no- ............... */
/*-no- ............................................................................ */
/*-no- ............................... */
/*-no- .............................................................................. */
/*-no- ................................................................................ */
/*-no- .............................................................................. */
/*-no- ................................ */
/*-no- ................................................................................ */
/*-no- ............................................................................ */
/*-no- ....................................................... */
/*-no- ............................................................................ */
/*-no- .......................................................................... */
/*-no- ........................ */
/*-no- ............ */
/*-no-  */
/*-no- ................................................................ */
/*-no- .................................................................... */
/*-no- .......................................................................... */
/*-no-  */
/*-no- #if defined(OS_WIN) && !defined(NO_TCMALLOC) */
/*-no- ....................................................................... */
/*-no- .......................................... */
/*-no-  */
/*-no- ............................................................................. */
/*-no- .............................................. */
/*-no- #endif  // OS_WIN */
/*-no-  */
/*-no- ......................... */
/*-no- #if defined(OS_WIN) */
/*-no- .......................................................... */
/*-no- #else */
/*-no- .............................................................. */
/*-no- ................................................................... */
/*-no- ............................................................. */
/*-no- ....................................................................... */
/*-no- .................................................................. */
/*-no- ................................................................ */
/*-no- .................................................... */
/*-no- #endif */
/*-no-  */
/*-no- .................................................... */
/*-no-  */
/*-no- ..................................................... */
/*-no- ......................................................... */
/*-no-  */
/*-no- ................................................. */
/*-no- ......................................................... */
/*-no- ................................................................. */
/*-no- ............................................................................... */
/*-no- .......................................................................... */
/*-no- ................................. */
/*-no- ........................................................................... */
/*-no- ......................... */
/*-no- .......... */
/*-no- ....................................................................... */
/*-no- ... */
/*-no-  */
/*-no- ................................................................... */
/*-no- ............................ */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ............................................................................ */
/*-no- .......... */
/*-no- ................................................... */
/*-no-  */
/*-no- ......................................................................... */
/*-no- .......................................................................... */
/*-no- ............................................................................ */
/*-no- ................................................ */
/*-no- ............................................................ */
/*-no-  */
/*-no- ...................................................... */
/*-no- ...................................... */
/*-no-  */
/*-no- ...................................................................... */
/*-no- ................................................................ */
/*-no-  */
/*-no- #if defined(USE_LINUX_BREAKPAD) */
/*-no- ............................................................... */
/*-no- ....................... */
/*-no- ....................................................................... */
/*-no- ................................ */
/*-no-  */
/*-no- ............................................. */
/*-no- ........................ */
/*-no- #endif */
/*-no-  */
/*-no- .......................................................................... */
/*-no- .................................. */
/*-no- ........................... */
/*-no- ..................................................... */
/*-no- .......... */
/*-no- .......................................................................... */
/*-no- .............................................................................. */
/*-no- .......................................... */
/*-no- #if defined(OS_MACOSX) */
/*-no- ............................................................................ */
/*-no- #else */
/*-no- .............................. */
/*-no- .......................................................... */
/*-no- ............................................................................. */
/*-no- ............................................ */
/*-no- ..................................... */
/*-no- ................................................... */
/*-no- ................................................................................ */
/*-no- ........................................................... */
/*-no-  */
/*-no- ................................. */
/*-no- ........................................................................ */
/*-no- ..................................................................... */
/*-no- #endif  // defined(OS_MACOSX) */
/*-no- ... */
/*-no-  */
/*-no- #if defined(TOOLKIT_GTK) */
/*-no- ............................................................................. */
/*-no- #endif */
/*-no-  */
/*-no- .......................... */
/*-no- ......................................................................... */
/*-no- ............................ */
/*-no- #if defined(OS_WIN) */
/*-no- .......................................................................... */
/*-no- ........................................................ */
/*-no- ...................................................... */
/*-no- ....................................................................... */
/*-no- ................................................ */
/*-no- ..................................................... */
/*-no- ................................. */
/*-no- ..... */
/*-no- ....................... */
/*-no- ................................................... */
/*-no- ............................... */
/*-no- ......................................................................... */
/*-no- ...................................... */
/*-no- ............................................. */
/*-no- ............................................... */
/*-no- ........................................... */
/*-no- #else */
/*-no- .............................................................. */
/*-no- .................................... */
/*-no- #endif  // defined(OS_WIN) */
/*-no- ... */
/*-no-  */
/*-no- ........................... */
/*-no-  */
/*-no- .......................................................................... */
/*-no- .......................................................................... */
/*-no- .................... */
/*-no- ..................................... */
/*-no- .................................................................. */
/*-no- ..................... */
/*-no- ......................... */
/*-no- .......................................................................... */
/*-no- ............................................................. */
/*-no-  */
/*-no- ......................................................................... */
/*-no- ...................... */
/*-no- ............................... */
/*-no- ......................................................... */
/*-no- ........................................................... */
/*-no- ............................................................... */
/*-no- ................................. */
/*-no- ... */
/*-no-  */
/*-no- ....................................................... */
/*-no- ................................................................ */
/*-no- ......................................... */
/*-no-  */
/*-no- ................................................ */
/*-no-  */
/*-no- .............................................................................. */
/*-no- ............................................................................. */
/*-no- ............................................................................. */
/*-no- .................................................... */
/*-no- .............................................................. */
/*-no- ................................... */
/*-no-  */
/*-no- ......................................................................... */
/*-no- ............................................................................. */
/*-no- ............. */
/*-no- ............................................................................. */
/*-no-  */
/*-no- ............................................. */
/*-no- ........................................... */
/*-no-  */
/*-no- .......................................................................... */
/*-no- .................................................. */
/*-no- ........................................................................ */
/*-no-  */
/*-no- ................................................................................ */
/*-no- ................. */
/*-no- ............................ */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ........................................................................... */
/*-no- ........................................................................... */
/*-no- ...................... */
/*-no- ................................................................. */
/*-no- .......................... */
/*-no- .............................................................. */
/*-no- ....................................................... */
/*-no- ... */
/*-no-  */
/*-no- #if defined(OS_MACOSX) */
/*-no- ............................................................................... */
/*-no- .............................................................................. */
/*-no- ....................................................................... */
/*-no- ............................... */
/*-no- ............................................................................. */
/*-no- ............................................................. */
/*-no- ..................................................... */
/*-no- #endif */
/*-no-  */
/*-no- ............................................ */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ............................................................. */
/*-no- ............................................................ */
/*-no- ............................................................... */
/*-no- .................................................................... */
/*-no- ...................... */
/*-no-  */
/*-no- ............................................. */
/*-no- .................................................. */
/*-no- #endif */
/*-no-  */
/*-no- ................................................ */
/*-no- ........................................... */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- ............................................................................. */
/*-no- .............................................................................. */
/*-no- ......................................... */
/*-no- ............................................................ */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ..................................................... */
/*-no- .......................................................... */
/*-no- ............................................. */
/*-no- #endif */
/*-no-  */
/*-no- ............................................................ */
/*-no- .......................................................... */
/*-no- .................................................... */
/*-no- ..................................................................... */
/*-no- .................................................... */
/*-no- ......................................................................... */
/*-no- ... */
/*-no-  */
/*-no- ............................................................................... */
/*-no- ............................. */
/*-no- ................................................................ */
/*-no- ................................................. */
/*-no- .................................................................... */
/*-no- ...................................... */
/*-no- ............ */
/*-no- ............................................... */
/*-no- ..... */
/*-no- ... */
/*-no-  */
/*-no- #if !defined(OS_MACOSX) */
/*-no- ...................................................................... */
/*-no- ........................................................................ */
/*-no- ...................................................................... */
/*-no- ...................................................................... */
/*-no- ........................................................... */
/*-no- .................................................................... */
/*-no- #endif */
/*-no- .............................................................................. */
/*-no- .............................................................................. */
/*-no- ............................................................................... */
/*-no- ........................... */
/*-no- ............................................................. */
/*-no- .......................................... */
/*-no- .......................................................................... */
/*-no- .............. */
/*-no-  */
/*-no- .............................................. */
/*-no- #if defined(OS_POSIX) && !defined(OS_MACOSX) */
/*-no- ........................................................... */
/*-no- ............................................................................ */
/*-no- #endif */
/*-no- ........................................ */
/*-no-  */
/*-no- ............................................ */
/*-no- ........................................... */
/*-no-  */
/*-no- ........................................ */
/*-no- ............................................................................. */
/*-no- .............................................................................. */
/*-no- ........................................................................... */
/*-no- ............................................................................... */
/*-no- ......................................................... */
/*-no- ........................................... */
/*-no-  */
/*-no- .............. */
/*-no- ..................... */
/*-no- ..... */
/*-no- #if !defined(OS_MACOSX)  // closing brace for if */
/*-no- ... */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(USE_X11) */
/*-no- ............................... */
/*-no- #endif */
/*-no-  */
/*-no- ................................................................................ */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ....................................... */
/*-no- ......................................................... */
/*-no- ................................................................. */
/*-no-  */
/*-no- ............................................................ */
/*-no- ...................................................... */
/*-no- ...................................... */
/*-no-  */
/*-no- ...................................................................... */
/*-no- ....................................................... */
/*-no- ............................................................ */
/*-no-  */
/*-no- ................................................... */
/*-no- .................................................................. */
/*-no- ....................................... */
/*-no- ... */
/*-no-  */
/*-no- ............................................ */
/*-no- ......................................................................... */
/*-no- ........................................................................ */
/*-no- ..................................................... */
/*-no- ......................... */
/*-no- ............................................................ */
/*-no- ................................................................. */
/*-no- .......................... */
/*-no- ...................................................................... */
/*-no- ............................................................ */
/*-no- ......................................................... */
/*-no-  */
/*-no- ..................................................... */
/*-no- ........................................................ */
/*-no- ... */
/*-no- #endif */
/*-no-  */
/*-no- .............................................................. */
/*-no- ............... */
/*-no- .................................... */
/*-no-  */
/*-no- ................................................................................ */
/*-no-  */
/*-no- ................................................ */
/*-no- ..................... */
/*-no-  */
/*-no- .................................................................. */
/*-no- ............................................................... */
/*-no- ............................ */
/*-no- ........................................................... */
/*-no- ... */
/*-no-  */
/*-no- #if !defined(OS_MACOSX) */
/*-no- ....................................................................... */
/*-no- ............................................ */
/*-no- .............................................................................. */
/*-no- .................................................................... */
/*-no- ......................................................... */
/*-no- ................................................................. */
/*-no- ............................................................. */
/*-no- ... */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- ........................................................................ */
/*-no- ....................................................................... */
/*-no- .................................... */
/*-no- #endif */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ............................................................................ */
/*-no- ............................................................................ */
/*-no- ....................... */
/*-no- ........................................................................ */
/*-no- ..................................................................... */
/*-no- .................... */
/*-no- ................................. */
/*-no- ..................................................... */
/*-no-  */
/*-no- ........................................... */
/*-no- .................................. */
/*-no-  */
/*-no- #if defined(OS_MACOSX) */
/*-no- .............................................................. */
/*-no- ............................................................................ */
/*-no- .......................... */
/*-no- ...................................... */
/*-no- ...................................................................... */
/*-no- ........................................................ */
/*-no- ...................................... */
/*-no- ..... */
/*-no- ... */
/*-no- #endif */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ............................................................................. */
/*-no- .......................................................................... */
/*-no- ......................................................................... */
/*-no- ............................... */
/*-no- ..................... */
/*-no- ............................... */
/*-no- ................................... */
/*-no- ......................................................... */
/*-no- ........................................................ */
/*-no- .......................................................... */
/*-no- ..................................................................... */
/*-no- ........................................................................... */
/*-no- ............................................................ */
/*-no- ............................................... */
/*-no- #if defined(OS_POSIX) */
/*-no- .............................................................................. */
/*-no- .................................. */
/*-no-  */
/*-no- ............................................................................. */
/*-no- .................. */
/*-no- ......................................................... */
/*-no- ....................................................................... */
/*-no- #endif  // OS_POSIX */
/*-no- ................................... */
/*-no-  */
/*-no- ............................................. */
/*-no- ... */
/*-no-  */
/*-no- ......................................................................... */
/*-no- ........................................................................... */
/*-no- ........................................................................... */
/*-no- ....................................................... */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- .............................................................................. */
/*-no- ......................................................................... */
/*-no- ............................................................................... */
/*-no- ........................................................................... */
/*-no- ................................................................................ */
/*-no- ............................................................ */
/*-no- .................................................................. */
/*-no- ................... */
/*-no- ... */
/*-no- #endif  // OS_WIN */
/*-no-  */
/*-no- ........................................................................ */
/*-no- .............................................................. */
/*-no- ........................................................................... */
/*-no- .......... */
/*-no- ....................................................................... */
/*-no- .................................................................. */
/*-no- ............................... */
/*-no- ...................................................................... */
/*-no- .............................. */
/*-no- ................................................. */
/*-no- ................. */
/*-no- .................. */
/*-no- .......................... */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- ................................................. */
/*-no-  */
/*-no- #if defined(GOOGLE_CHROME_BUILD) */
/*-no- .............................................................................. */
/*-no- ............................................................................. */
/*-no- ............................ */
/*-no- .................................................................... */
/*-no- #endif  // GOOGLE_CHROME_BUILD */
/*-no- #endif  // OS_WIN */
/*-no-  */
/*-no- ..................................................... */
/*-no- .............................................................................. */
/*-no- ................................................................... */
/*-no-  */
/*-no- .......................................................... */
/*-no- .............................. */
/*-no- .......................................... */
/*-no- ............................... */
/*-no- ...................................... */
/*-no- ..................................... */
/*-no- ........................................... */
/*-no-  */
/*-no- .............................................................................. */
/*-no- .............................................................................. */
/*-no- ........................................................................... */
/*-no- ............................................................................ */
/*-no- ................... */
/*-no- .... */
/*-no- ............................................................................... */
/*-no- ............................................................................. */
/*-no- .............................................................................. */
/*-no- .... */
/*-no- .............................................................................. */
/*-no- ..................................................... */
/*-no- ........................................ */
/*-no- ................................................ */
/*-no-  */
/*-no- ................................................................. */
/*-no- ............................................. */
/*-no-  */
/*-no- ..................................................... */
/*-no- ............................................................... */
/*-no- ............................................................................ */
/*-no- ..................... */
/*-no- ..................................................... */
/*-no- ............................................................................. */
/*-no- .................................................................... */
/*-no- ............................................. */
/*-no- ............................................................................. */
/*-no- ........................ */
/*-no- ..................................................... */
/*-no-  */
/*-no- ......................................................... */
/*-no- ........................................ */
/*-no- ............................................................. */
/*-no- ........................... */
/*-no- ....................................................................... */
/*-no- .......... */
/*-no- .................................................. */
/*-no- ....................................................... */
/*-no- ............................................ */
/*-no- .................................................................. */
/*-no- ... */
/*-no-  */
/*-no- ........................................................ */
/*-no- ........................................................... */
/*-no- ........................................................ */
/*-no-  */
/*-no- ................................................................................ */
/*-no- ......................................... */
/*-no-  */
/*-no- #if defined(OS_WIN) && !defined(GOOGLE_CHROME_BUILD) */
/*-no- ............................................................. */
/*-no- ................... */
/*-no- ...................................................................... */
/*-no- ......................................................... */
/*-no- ... */
/*-no- #endif */
/*-no-  */
/*-no- ............................................ */
/*-no- ................................... */
/*-no- ................................................ */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ......................................................... */
/*-no- ............................................ */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- .................................. */
/*-no- #endif */
/*-no-  */
/*-no- .......................................................................... */
/*-no- ..................................................................... */
/*-no- ............................................................................ */
/*-no- ....................................... */
/*-no- ............................................................................. */
/*-no- ....................... */
/*-no- ....................................................... */
/*-no- ... */
/*-no-  */
/*-no- .............................................................................. */
/*-no- ............................................................................. */
/*-no- ............ */
/*-no- ..................................................................... */
/*-no- ........................................... */
/*-no- ............................................................................ */
/*-no- ...................................... */
/*-no- ............ */
/*-no- .................................................... */
/*-no- ..... */
/*-no- ... */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- ............................................................................ */
/*-no- ............................................................. */
/*-no- ......................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- ................................................................... */
/*-no- .................................................................... */
/*-no- .................................................. */
/*-no- .................................................................. */
/*-no- .............................................. */
/*-no- ... */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ....................................... */
/*-no- ............................................................................ */
/*-no- ................................................. */
/*-no- .......................................................... */
/*-no- ................................................................. */
/*-no- .................................................................... */
/*-no- ...................................... */
/*-no- ......................................................................... */
/*-no- ....................... */
/*-no- ................................... */
/*-no- ..... */
/*-no- ... */
/*-no-  */
/*-no- ................................................................. */
/*-no- .............................. */
/*-no- ...................... */
/*-no- .......................................................................... */
/*-no- .................................... */
/*-no- ............................... */
/*-no- ....................................................................... */
/*-no- #ifdef OS_WIN */
/*-no- ............................................... */
/*-no- ................................................................................ */
/*-no- ......................................... */
/*-no- ................................................... */
/*-no- #elif defined(OS_POSIX) */
/*-no- .............................................................. */
/*-no- ................................................ */
/*-no- #endif */
/*-no- ..... */
/*-no- ................................................................. */
/*-no- ................................................................ */
/*-no- ....................................................... */
/*-no- ..... */
/*-no- ... */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ...................................................................... */
/*-no- .................................................................... */
/*-no- ..................................................................... */
/*-no- ...................................................................... */
/*-no- ..................................................................... */
/*-no- .............................................. */
/*-no- ............................................................... */
/*-no- ....................................... */
/*-no- #endif */
/*-no-  */
/*-no- ............................................................................... */
/*-no- .............................................................................. */
/*-no- ......................................................... */
/*-no- ....................................... */
/*-no-  */
/*-no- ......................................................................... */
/*-no- .......................................................................... */
/*-no- ................................. */
/*-no- ................................................................... */
/*-no- ........................... */
/*-no-  */
/*-no- ........................................................... */
/*-no- ........................................ */
/*-no-  */
/*-no- ............................................. */
/*-no- ........................... */
/*-no- ......................................................................... */
/*-no- ............. */
/*-no- ...................... */
/*-no- .............................. */
/*-no- .............................. */
/*-no- .......... */
/*-no- ............................................................................... */
/*-no- ......................... */
/*-no- .................................................................... */
/*-no- ........................................... */
/*-no- #if (defined(OS_WIN) || defined(OS_LINUX)) && !defined(OS_CHROMEOS) */
/*-no- ............................................................................ */
/*-no- ............................................................................. */
/*-no- .......................................................................... */
/*-no- ..................................................................... */
/*-no- ................................................ */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(OS_LINUX) && !defined(OS_CHROMEOS) */
/*-no- ........................................................................ */
/*-no- .......................................................................... */
/*-no- ............................................................................. */
/*-no- .............................................................................. */
/*-no- ........................................... */
/*-no- #endif */
/*-no-  */
/*-no- ........................................................ */
/*-no- ............................................. */
/*-no- ............................................................................ */
/*-no- .............................................................................. */
/*-no- ............... */
/*-no- ........................ */
/*-no- .............................................. */
/*-no- ..... */
/*-no- ... */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- ........................................................................ */
/*-no- ...................................................................... */
/*-no- ............................................................................ */
/*-no- ................. */
/*-no- ..................................................................... */
/*-no- .............................................. */
/*-no- ................................................................... */
/*-no- ....................................................................... */
/*-no- ...................... */
/*-no- ......................................... */
/*-no- ............................................................................. */
/*-no- .................................................... */
/*-no- ....................................... */
/*-no- .................................................... */
/*-no- ................................ */
/*-no- .......................................... */
/*-no- ............................. */
/*-no- ............................. */
/*-no- .............................................................................. */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ........................................ */
/*-no- ............................. */
/*-no- ............................................................................ */
/*-no- ............................................... */
/*-no- ...................................................... */
/*-no- .................................... */
/*-no- .............................. */
/*-no- ................................. */
/*-no- ................................. */
/*-no- ................ */
/*-no- .................................................................. */
/*-no- ......... */
/*-no- ....... */
/*-no- ............ */
/*-no- ................................ */
/*-no- ...................................... */
/*-no- ............................. */
/*-no- ............................. */
/*-no- ..... */
/*-no- ... */
/*-no- #endif */
/*-no-  */
/*-no- ............................................................................. */
/*-no-  */
/*-no- .............................. */
/*-no-  */
/*-no- ..................................................... */
/*-no- ....................................... */
/*-no-  */
/*-no- .................. */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ................ */
/*-no- ........................................... */
/*-no- ............................... */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ...................................................................... */
/*-no- ................................................................ */
/*-no- ..................................................................... */
/*-no- ............................................................... */
/*-no- ....................................................... */
/*-no- #endif */
/*-no- ....................................... */
/*-no- ..................... */
/*-no- . */

#endif
/* CONSBENCH file end */
