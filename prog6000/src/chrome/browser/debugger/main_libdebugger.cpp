/* CONSBENCH file begin */
#ifndef MAIN_LIBDEBUGGER_CPP
#define MAIN_LIBDEBUGGER_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_browser_debugger_main_libdebugger_cpp, "chrome/browser/debugger/main_libdebugger.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_browser_debugger_main_libdebugger_cpp, "chrome/browser/debugger/main_libdebugger.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_chrome_browser_debugger_browser_list_tabcontents_provider_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_debugger_debugger_remote_service_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_debugger_devtools_client_host_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_debugger_devtools_http_protocol_handler_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_debugger_devtools_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_debugger_devtools_netlog_observer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_debugger_devtools_protocol_handler_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_debugger_devtools_remote_listen_socket_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_debugger_devtools_remote_message_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_debugger_devtools_remote_service_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_debugger_devtools_window_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_debugger_extension_ports_remote_service_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_debugger_inspectable_tab_proxy_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_browser_debugger_main_libdebugger_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_chrome_browser_debugger_browser_list_tabcontents_provider_cpp(it);
  PUBLIC_chrome_browser_debugger_debugger_remote_service_cpp(it);
  PUBLIC_chrome_browser_debugger_devtools_client_host_cpp(it);
  PUBLIC_chrome_browser_debugger_devtools_http_protocol_handler_cpp(it);
  PUBLIC_chrome_browser_debugger_devtools_manager_cpp(it);
  PUBLIC_chrome_browser_debugger_devtools_netlog_observer_cpp(it);
  PUBLIC_chrome_browser_debugger_devtools_protocol_handler_cpp(it);
  PUBLIC_chrome_browser_debugger_devtools_remote_listen_socket_cpp(it);
  PUBLIC_chrome_browser_debugger_devtools_remote_message_cpp(it);
  PUBLIC_chrome_browser_debugger_devtools_remote_service_cpp(it);
  PUBLIC_chrome_browser_debugger_devtools_window_cpp(it);
  PUBLIC_chrome_browser_debugger_extension_ports_remote_service_cpp(it);
  PUBLIC_chrome_browser_debugger_inspectable_tab_proxy_cpp(it);
}

#endif

