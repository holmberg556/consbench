/* CONSBENCH file begin */
#ifndef EXTENSION_FUNCTION_DISPATCHER_CC_2521
#define EXTENSION_FUNCTION_DISPATCHER_CC_2521

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_browser_extensions_extension_function_dispatcher_cpp, "chrome/browser/extensions/extension_function_dispatcher.cpp");

/* CONSBENCH includes begin */
#include "chrome/browser/extensions/extension_function_dispatcher.h"
#include <map>
#include "base/process_util.h"
#include "base/ref_counted.h"
#include "base/singleton.h"
#include "base/values.h"
#include "build/build_config.h"
#include "chrome/browser/browser_list.h"
#include "chrome/browser/browser_window.h"
#include "chrome/browser/extensions/execute_code_in_tab_function.h"
#include "chrome/browser/extensions/extension_accessibility_api.h"
#include "chrome/browser/extensions/extension_bookmark_manager_api.h"
#include "chrome/browser/extensions/extension_bookmarks_module.h"
#include "chrome/browser/extensions/extension_browser_actions_api.h"
#include "chrome/browser/extensions/extension_clipboard_api.h"
#include "chrome/browser/extensions/extension_context_menu_api.h"
#include "chrome/browser/extensions/extension_cookies_api.h"
#include "chrome/browser/extensions/extension_function.h"
#include "chrome/browser/extensions/extension_history_api.h"
#include "chrome/browser/extensions/extension_i18n_api.h"
#include "chrome/browser/extensions/extension_idle_api.h"
#include "chrome/browser/extensions/extension_infobar_module.h"
#include "chrome/browser/extensions/extension_management_api.h"
#include "chrome/browser/extensions/extension_message_service.h"
#include "chrome/browser/extensions/extension_metrics_module.h"
#include "chrome/browser/extensions/extension_module.h"
#include "chrome/browser/extensions/extension_omnibox_api.h"
#include "chrome/browser/extensions/extension_page_actions_module.h"
#include "chrome/browser/extensions/extension_preference_api.h"
#include "chrome/browser/extensions/extension_process_manager.h"
#include "chrome/browser/extensions/extension_processes_api.h"
#include "chrome/browser/extensions/extension_proxy_api.h"
#include "chrome/browser/extensions/extension_rlz_module.h"
#include "chrome/browser/extensions/extension_service.h"
#include "chrome/browser/extensions/extension_sidebar_api.h"
#include "chrome/browser/extensions/extension_tabs_module.h"
#include "chrome/browser/extensions/extension_test_api.h"
#include "chrome/browser/extensions/extension_tts_api.h"
#include "chrome/browser/extensions/extension_web_ui.h"
#include "chrome/browser/extensions/extension_webrequest_api.h"
#include "chrome/browser/extensions/extension_webstore_private_api.h"
#include "chrome/browser/extensions/extensions_quota_service.h"
#include "chrome/browser/external_protocol_handler.h"
#include "chrome/browser/metrics/user_metrics.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/webui/chrome_url_data_manager.h"
#include "chrome/browser/ui/webui/favicon_source.h"
#include "chrome/common/notification_service.h"
#include "chrome/common/render_messages.h"
#include "chrome/common/render_messages_params.h"
#include "chrome/common/result_codes.h"
#include "chrome/common/url_constants.h"
#include "content/browser/renderer_host/render_process_host.h"
#include "content/browser/renderer_host/render_view_host.h"
#include "third_party/skia/include/core/SkBitmap.h"
#include "chrome/browser/extensions/extension_input_api.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_browser_extensions_extension_function_dispatcher_cpp, "chrome/browser/extensions/extension_function_dispatcher.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(chrome_browser_extensions_extension_function_dispatcher_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include "chrome/browser/extensions/extension_function_dispatcher.h" */
/*-no-  */
/*-no- #include <map> */
/*-no-  */
/*-no- #include "base/process_util.h" */
/*-no- #include "base/ref_counted.h" */
/*-no- #include "base/singleton.h" */
/*-no- #include "base/values.h" */
/*-no- #include "build/build_config.h" */
/*-no- #include "chrome/browser/browser_list.h" */
/*-no- #include "chrome/browser/browser_window.h" */
/*-no- #include "chrome/browser/extensions/execute_code_in_tab_function.h" */
/*-no- #include "chrome/browser/extensions/extension_accessibility_api.h" */
/*-no- #include "chrome/browser/extensions/extension_bookmark_manager_api.h" */
/*-no- #include "chrome/browser/extensions/extension_bookmarks_module.h" */
/*-no- #include "chrome/browser/extensions/extension_browser_actions_api.h" */
/*-no- #include "chrome/browser/extensions/extension_clipboard_api.h" */
/*-no- #include "chrome/browser/extensions/extension_context_menu_api.h" */
/*-no- #include "chrome/browser/extensions/extension_cookies_api.h" */
/*-no- #include "chrome/browser/extensions/extension_function.h" */
/*-no- #include "chrome/browser/extensions/extension_history_api.h" */
/*-no- #include "chrome/browser/extensions/extension_i18n_api.h" */
/*-no- #include "chrome/browser/extensions/extension_idle_api.h" */
/*-no- #include "chrome/browser/extensions/extension_infobar_module.h" */
/*-no- #include "chrome/browser/extensions/extension_management_api.h" */
/*-no- #include "chrome/browser/extensions/extension_message_service.h" */
/*-no- #include "chrome/browser/extensions/extension_metrics_module.h" */
/*-no- #include "chrome/browser/extensions/extension_module.h" */
/*-no- #include "chrome/browser/extensions/extension_omnibox_api.h" */
/*-no- #include "chrome/browser/extensions/extension_page_actions_module.h" */
/*-no- #include "chrome/browser/extensions/extension_preference_api.h" */
/*-no- #include "chrome/browser/extensions/extension_process_manager.h" */
/*-no- #include "chrome/browser/extensions/extension_processes_api.h" */
/*-no- #include "chrome/browser/extensions/extension_proxy_api.h" */
/*-no- #include "chrome/browser/extensions/extension_rlz_module.h" */
/*-no- #include "chrome/browser/extensions/extension_service.h" */
/*-no- #include "chrome/browser/extensions/extension_sidebar_api.h" */
/*-no- #include "chrome/browser/extensions/extension_tabs_module.h" */
/*-no- #include "chrome/browser/extensions/extension_test_api.h" */
/*-no- #include "chrome/browser/extensions/extension_tts_api.h" */
/*-no- #include "chrome/browser/extensions/extension_web_ui.h" */
/*-no- #include "chrome/browser/extensions/extension_webrequest_api.h" */
/*-no- #include "chrome/browser/extensions/extension_webstore_private_api.h" */
/*-no- #include "chrome/browser/extensions/extensions_quota_service.h" */
/*-no- #include "chrome/browser/external_protocol_handler.h" */
/*-no- #include "chrome/browser/metrics/user_metrics.h" */
/*-no- #include "chrome/browser/profiles/profile.h" */
/*-no- #include "chrome/browser/ui/webui/chrome_url_data_manager.h" */
/*-no- #include "chrome/browser/ui/webui/favicon_source.h" */
/*-no- #include "chrome/common/notification_service.h" */
/*-no- #include "chrome/common/render_messages.h" */
/*-no- #include "chrome/common/render_messages_params.h" */
/*-no- #include "chrome/common/result_codes.h" */
/*-no- #include "chrome/common/url_constants.h" */
/*-no- #include "content/browser/renderer_host/render_process_host.h" */
/*-no- #include "content/browser/renderer_host/render_view_host.h" */
/*-no- #include "third_party/skia/include/core/SkBitmap.h" */
/*-no-  */
/*-no- #if defined(TOOLKIT_VIEWS) */
/*-no- #include "chrome/browser/extensions/extension_input_api.h" */
/*-no- #endif */
/*-no-  */
/*-no- ................................................................................ */
/*-no-  */
/*-no- ........... */
/*-no-  */
/*-no- .................................................. */
/*-no- ................. */
/*-no- ........................................... */
/*-no- ................. */
/*-no- . */
/*-no-  */
/*-no- ......................................................................... */
/*-no- ............................ */
/*-no- ....................... */
/*-no- ........ */
/*-no- ........................................ */
/*-no- ......................................... */
/*-no-  */
/*-no- .................................................. */
/*-no- ........................ */
/*-no-  */
/*-no- ........................................ */
/*-no- .................................................... */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ................................................................ */
/*-no- ................................................ */
/*-no- .......................................................... */
/*-no-  */
/*-no- ................................................................... */
/*-no- .......................................................... */
/*-no-  */
/*-no- ......... */
/*-no- ................... */
/*-no- ........................... */
/*-no- .............................................................. */
/*-no- ... */
/*-no-  */
/*-no- ..................................................................... */
/*-no- ........................ */
/*-no- .. */
/*-no-  */
/*-no- ................................................. */
/*-no- ........................................... */
/*-no- . */
/*-no-  */
/*-no- ........................................ */
/*-no- ................................. */
/*-no-  */
/*-no- ............ */
/*-no- ........................................ */
/*-no- ............................................... */
/*-no- ................................................... */
/*-no- ............................................ */
/*-no- ........................................... */
/*-no- ........................................... */
/*-no- ........................................... */
/*-no-  */
/*-no- ......... */
/*-no- ..................................... */
/*-no- ............................................ */
/*-no- ............................................. */
/*-no- ................................................. */
/*-no- ........................................ */
/*-no- ........................................ */
/*-no- ...................................... */
/*-no- ........................................ */
/*-no- ................................................ */
/*-no- ................................................ */
/*-no- ................................................ */
/*-no- ............................................ */
/*-no-  */
/*-no- .................. */
/*-no- ............................................... */
/*-no- ................................................ */
/*-no- ............................................. */
/*-no- ............................................. */
/*-no- ................................................ */
/*-no- ................................................. */
/*-no- ................................................. */
/*-no-  */
/*-no- ..................... */
/*-no- ................................................... */
/*-no- .................................................... */
/*-no- ........................................................ */
/*-no- ................................................................... */
/*-no- .................................................... */
/*-no-  */
/*-no- ............... */
/*-no- ........................................... */
/*-no- .................................................. */
/*-no- ................................................ */
/*-no- .............................................. */
/*-no- .............................................. */
/*-no- ............................................. */
/*-no- ................................................. */
/*-no- ............................................. */
/*-no- ........................................... */
/*-no- ............................................. */
/*-no-  */
/*-no- .............. */
/*-no- .......................................... */
/*-no-  */
/*-no- .................... */
/*-no- .................................................. */
/*-no- ................................................. */
/*-no- ................................................... */
/*-no- ...................................................... */
/*-no- .............................................. */
/*-no- .............................................. */
/*-no- .......................................................... */
/*-no- ........................................................ */
/*-no- ....................................................... */
/*-no- .................................................. */
/*-no- ........................................................ */
/*-no-  */
/*-no- ............ */
/*-no- ............................................ */
/*-no- ............................................... */
/*-no- ................................................. */
/*-no- ............................................... */
/*-no- ............................................... */
/*-no- ............................................ */
/*-no-  */
/*-no- ......... */
/*-no- ...................................................... */
/*-no-  */
/*-no- .......... */
/*-no- ................................................. */
/*-no-  */
/*-no- ............... */
/*-no- ................................................. */
/*-no-  */
/*-no- ............. */
/*-no- ................................................ */
/*-no- ................................................ */
/*-no- ...................................................... */
/*-no- ................................................. */
/*-no- ...................................................... */
/*-no- ................................................. */
/*-no- ...................................................... */
/*-no- ....................................................... */
/*-no- ................................................ */
/*-no- ...................................................... */
/*-no- .................................................... */
/*-no-  */
/*-no- ......... */
/*-no- #if defined(OS_WIN) */
/*-no- .................................................... */
/*-no- ................................................... */
/*-no- ................................................... */
/*-no- ................................................... */
/*-no- #endif */
/*-no-  */
/*-no- ............. */
/*-no- ........................................ */
/*-no- ............................................ */
/*-no- ........................................ */
/*-no- ........................................... */
/*-no- ................................................. */
/*-no-  */
/*-no- .......... */
/*-no- ................................................ */
/*-no- ................................................ */
/*-no- ............................................... */
/*-no- ...................................................... */
/*-no- .............................................................. */
/*-no- ....................................................... */
/*-no- ..................................................... */
/*-no-  */
/*-no- ................... */
/*-no- ................................................ */
/*-no- ...................................................... */
/*-no-  */
/*-no- .................... */
/*-no- ................................................ */
/*-no- ....................................................... */
/*-no- ..................................................... */
/*-no- ......................................................... */
/*-no-  */
/*-no- ............... */
/*-no- ................................................... */
/*-no- .................................................. */
/*-no- .................................................... */
/*-no-  */
/*-no- ................... */
/*-no- ................................................ */
/*-no- ................................................ */
/*-no- ................................................ */
/*-no- .................................................... */
/*-no-  */
/*-no- ............. */
/*-no- ..................................................... */
/*-no- .......................................................... */
/*-no-  */
/*-no- ............. */
/*-no- ............................................... */
/*-no- ............................................... */
/*-no-  */
/*-no- ............. */
/*-no- .............................................. */
/*-no- ............................................ */
/*-no- .............................................. */
/*-no- .......................................... */
/*-no- .............................................. */
/*-no- .................................................. */
/*-no- ............................................. */
/*-no- .............................................. */
/*-no- .......................................... */
/*-no-  */
/*-no- #if defined(TOOLKIT_VIEWS) */
/*-no- ........... */
/*-no- ..................................................... */
/*-no- #endif */
/*-no-  */
/*-no- ................ */
/*-no- ............................................... */
/*-no- ............................................... */
/*-no- ........................................ */
/*-no- ......................................... */
/*-no- ........................................ */
/*-no-  */
/*-no- ...................... */
/*-no- ............................................... */
/*-no-  */
/*-no- ..................... */
/*-no- .............................................. */
/*-no- ............................................ */
/*-no- ............................................ */
/*-no- ................................................. */
/*-no- ........................................... */
/*-no- .............................................. */
/*-no-  */
/*-no- ................ */
/*-no- ................................................. */
/*-no-  */
/*-no- ................. */
/*-no- ............................................ */
/*-no- ............................................ */
/*-no- .............................................. */
/*-no- . */
/*-no-  */
/*-no- .................................................................... */
/*-no- ...................................................... */
/*-no- .......................................... */
/*-no- .................................. */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ............................................................... */
/*-no- .......................................................................... */
/*-no- .................................................... */
/*-no- ................................. */
/*-no- ................. */
/*-no- .......... */
/*-no- ........................... */
/*-no- ................ */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- .......................................................................... */
/*-no- .................................................... */
/*-no- ................................... */
/*-no- ............................................... */
/*-no- ........................... */
/*-no- .................. */
/*-no- . */
/*-no-  */
/*-no- ................ */
/*-no-  */
/*-no- ................................................................................ */
/*-no-  */
/*-no- ...................................................... */
/*-no- ...................................... */
/*-no- ..................................................... */
/*-no- . */
/*-no-  */
/*-no- ................................................... */
/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- . */
/*-no-  */
/*-no- .................................................... */
/*-no- ................................................... */
/*-no- . */
/*-no-  */
/*-no- ................................................................. */
/*-no- ..................................... */
/*-no- ....................... */
/*-no- ...................... */
/*-no- ............................. */
/*-no- .................................................................... */
/*-no- .................. */
/*-no-  */
/*-no- .............................................. */
/*-no- ................ */
/*-no-  */
/*-no- ............................................................... */
/*-no- ................. */
/*-no- ...................................................... */
/*-no-  */
/*-no- ................ */
/*-no- ...................................................................... */
/*-no- ........................................................... */
/*-no- ...... */
/*-no- ................ */
/*-no- . */
/*-no-  */
/*-no- ......................................................... */
/*-no- ..................................... */
/*-no- ....................... */
/*-no- ............................... */
/*-no- .................... */
/*-no- ..................................................... */
/*-no- ........................................ */
/*-no- ........................ */
/*-no- .............. */
/*-no- ................................... */
/*-no- ........................................................... */
/*-no- ...................................................................... */
/*-no- .................... */
/*-no- .................................................. */
/*-no- ........................................................ */
/*-no-  */
/*-no- .................................................................. */
/*-no- ......................................................................... */
/*-no- ............................................... */
/*-no- ................................................................... */
/*-no-  */
/*-no- ................................................................................ */
/*-no- ............................................................... */
/*-no- .......................... */
/*-no- ........................................................................ */
/*-no- ................................................................ */
/*-no- ....................................................................... */
/*-no- ... */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ............................................................................... */
/*-no- ................ */
/*-no- ................................................................. */
/*-no- ...................................................... */
/*-no- .................................................................. */
/*-no- ........................................................ */
/*-no-  */
/*-no- ......................................... */
/*-no- .............................................................. */
/*-no- ................................ */
/*-no- .................................................. */
/*-no- . */
/*-no-  */
/*-no- ............................................................. */
/*-no- ............................ */
/*-no-  */
/*-no- ......................................... */
/*-no- ................................................................ */
/*-no- ................................ */
/*-no- .................................................. */
/*-no- . */
/*-no-  */
/*-no- ........................................................ */
/*-no- ............................. */
/*-no- ............................................. */
/*-no-  */
/*-no- ................................................................................ */
/*-no- .............. */
/*-no- ................... */
/*-no-  */
/*-no- .............................................................................. */
/*-no- ........................................................................... */
/*-no- ........................................................................... */
/*-no- .......................................................................... */
/*-no- ......................... */
/*-no- .............................................................. */
/*-no- ........................................................................... */
/*-no- ................................................................ */
/*-no-  */
/*-no- .............................................................................. */
/*-no- ........................................................................ */
/*-no- ................................................................... */
/*-no- ................................................... */
/*-no- ....................................................................... */
/*-no- ........................................................................... */
/*-no- ................. */
/*-no- . */
/*-no-  */
/*-no- ................................................ */
/*-no- .................................................. */
/*-no- ............................................ */
/*-no- ................................................................ */
/*-no- ....................................... */
/*-no- .................................. */
/*-no- ............................................. */
/*-no- ....................................... */
/*-no- .............................................. */
/*-no- .............................................. */
/*-no- .................................................. */
/*-no- .................................................. */
/*-no- ............................................................... */
/*-no- .................. */
/*-no- ................................................................................ */
/*-no- .................... */
/*-no- ......................................................................... */
/*-no-  */
/*-no- ................................................................... */
/*-no- ....................................................... */
/*-no- ..................................................................... */
/*-no- ........... */
/*-no- ... */
/*-no-  */
/*-no- ........................................................... */
/*-no- ................................................................ */
/*-no- .............................................. */
/*-no- ........................... */
/*-no- ............................................... */
/*-no-  */
/*-no- .................... */
/*-no- .......... */
/*-no- ........................................................................... */
/*-no- .................................................................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ........................................................................... */
/*-no- .............................................................. */
/*-no- ........................................................................... */
/*-no- ................................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ........................................... */
/*-no- .............................. */
/*-no- ........................................... */
/*-no- ..................................................... */
/*-no- .............................................................................. */
/*-no- ................. */
/*-no- .......... */
/*-no- ................. */
/*-no- ............................................................................ */
/*-no- ................................................................ */
/*-no- .............................................................. */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ................................................. */
/*-no- .................. */
/*-no- . */

#endif
/* CONSBENCH file end */
