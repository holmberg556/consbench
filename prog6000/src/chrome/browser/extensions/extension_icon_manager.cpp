/* CONSBENCH file begin */
#ifndef EXTENSION_ICON_MANAGER_CC_2537
#define EXTENSION_ICON_MANAGER_CC_2537

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_browser_extensions_extension_icon_manager_cpp, "chrome/browser/extensions/extension_icon_manager.cpp");

/* CONSBENCH includes begin */
#include "chrome/browser/extensions/extension_icon_manager.h"
#include "base/logging.h"
#include "base/stl_util-inl.h"
#include "chrome/common/extensions/extension.h"
#include "chrome/common/extensions/extension_icon_set.h"
#include "chrome/common/extensions/extension_resource.h"
#include "grit/theme_resources.h"
#include "skia/ext/image_operations.h"
#include "ui/base/resource/resource_bundle.h"
#include "ui/gfx/canvas_skia.h"
#include "ui/gfx/color_utils.h"
#include "ui/gfx/favicon_size.h"
#include "ui/gfx/size.h"
#include "ui/gfx/skbitmap_operations.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_browser_extensions_extension_icon_manager_cpp, "chrome/browser/extensions/extension_icon_manager.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(chrome_browser_extensions_extension_icon_manager_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include "chrome/browser/extensions/extension_icon_manager.h" */
/*-no-  */
/*-no- #include "base/logging.h" */
/*-no- #include "base/stl_util-inl.h" */
/*-no- #include "chrome/common/extensions/extension.h" */
/*-no- #include "chrome/common/extensions/extension_icon_set.h" */
/*-no- #include "chrome/common/extensions/extension_resource.h" */
/*-no- #include "grit/theme_resources.h" */
/*-no- #include "skia/ext/image_operations.h" */
/*-no- #include "ui/base/resource/resource_bundle.h" */
/*-no- #include "ui/gfx/canvas_skia.h" */
/*-no- #include "ui/gfx/color_utils.h" */
/*-no- #include "ui/gfx/favicon_size.h" */
/*-no- #include "ui/gfx/size.h" */
/*-no- #include "ui/gfx/skbitmap_operations.h" */
/*-no-  */
/*-no- ........... */
/*-no-  */
/*-no- .............................................................................. */
/*-no- .............................. */
/*-no- .................................................... */
/*-no- .......................................................... */
/*-no- ..................................... */
/*-no- ........................................................... */
/*-no- ...................................................................... */
/*-no- ........................ */
/*-no- ............. */
/*-no- ............................................ */
/*-no- ..................................................................... */
/*-no- ............. */
/*-no- ................................. */
/*-no- . */
/*-no-  */
/*-no- ............... */
/*-no-  */
/*-no- ............................................ */
/*-no- ........................................................... */
/*-no- .......................... */
/*-no- . */
/*-no-  */
/*-no- ............................................... */
/*-no- . */
/*-no-  */
/*-no- ................................................................. */
/*-no- ............................................................... */
/*-no- ....................................................................... */
/*-no- ................................................ */
/*-no- .......................................................................... */
/*-no- .................................................... */
/*-no- ........................................... */
/*-no- ....................................... */
/*-no- ........................................... */
/*-no- ................................................................... */
/*-no- ......................................................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ................................................................................ */
/*-no- ................................ */
/*-no- .......................................... */
/*-no- ................................... */
/*-no- .......... */
/*-no- ........................ */
/*-no- ............................ */
/*-no- ... */
/*-no- ................. */
/*-no- .............................................................. */
/*-no- ................................................................ */
/*-no- ................. */
/*-no- . */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ............................. */
/*-no- ..................................... */
/*-no- . */
/*-no-  */
/*-no- ......................................................... */
/*-no- .................................................................... */
/*-no- ..................................................... */
/*-no- ............. */
/*-no- ........... */
/*-no-  */
/*-no- ........................................................... */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ................ */
/*-no- ................................................. */
/*-no- ........... */
/*-no-  */
/*-no- ..................................... */
/*-no- ................................................. */
/*-no- . */
/*-no-  */
/*-no- ................................................ */
/*-no- .............................. */
/*-no- ............................................................. */
/*-no- .............................................................. */
/*-no- .......................................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ........................... */
/*-no-  */
/*-no- .......................................................................... */
/*-no- ........................................... */
/*-no- ....................................................... */
/*-no- .................................... */
/*-no- ... */
/*-no-  */
/*-no- .................... */
/*-no- .......................................... */
/*-no- ....................................................................... */
/*-no- ... */
/*-no-  */
/*-no- ........................ */
/*-no- ............................................ */
/*-no-  */
/*-no- ................ */
/*-no- . */

#endif
/* CONSBENCH file end */
