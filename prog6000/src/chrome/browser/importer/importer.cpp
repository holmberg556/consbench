/* CONSBENCH file begin */
#ifndef IMPORTER_CC_2905
#define IMPORTER_CC_2905

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_browser_importer_importer_cpp, "chrome/browser/importer/importer.cpp");

/* CONSBENCH includes begin */
#include "chrome/browser/importer/importer.h"
#include "base/threading/thread.h"
#include "base/utf_string_conversions.h"
#include "base/values.h"
#include "chrome/browser/bookmarks/bookmark_model.h"
#include "chrome/browser/browser_list.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/importer/firefox_profile_lock.h"
#include "chrome/browser/importer/importer_bridge.h"
#include "chrome/browser/search_engines/template_url.h"
#include "chrome/browser/search_engines/template_url_model.h"
#include "chrome/browser/tabs/tab_strip_model.h"
#include "chrome/browser/ui/browser_dialogs.h"
#include "chrome/browser/ui/browser_navigator.h"
#include "chrome/browser/webdata/web_data_service.h"
#include "chrome/common/notification_source.h"
#include "content/browser/browser_thread.h"
#include "content/browser/browsing_instance.h"
#include "content/browser/site_instance.h"
#include "grit/generated_resources.h"
#include "skia/ext/image_operations.h"
#include "ui/base/l10n/l10n_util.h"
#include "ui/gfx/codec/png_codec.h"
#include "ui/gfx/favicon_size.h"
#include "webkit/glue/image_decoder.h"
#include "ui/base/message_box_win.h"
#include "views/window/window.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_browser_importer_importer_cpp, "chrome/browser/importer/importer.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(chrome_browser_importer_importer_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include "chrome/browser/importer/importer.h" */
/*-no-  */
/*-no- #include "base/threading/thread.h" */
/*-no- #include "base/utf_string_conversions.h" */
/*-no- #include "base/values.h" */
/*-no- #include "chrome/browser/bookmarks/bookmark_model.h" */
/*-no- #include "chrome/browser/browser_list.h" */
/*-no- #include "chrome/browser/browser_process.h" */
/*-no- #include "chrome/browser/importer/firefox_profile_lock.h" */
/*-no- #include "chrome/browser/importer/importer_bridge.h" */
/*-no- #include "chrome/browser/search_engines/template_url.h" */
/*-no- #include "chrome/browser/search_engines/template_url_model.h" */
/*-no- #include "chrome/browser/tabs/tab_strip_model.h" */
/*-no- #include "chrome/browser/ui/browser_dialogs.h" */
/*-no- #include "chrome/browser/ui/browser_navigator.h" */
/*-no- #include "chrome/browser/webdata/web_data_service.h" */
/*-no- #include "chrome/common/notification_source.h" */
/*-no- #include "content/browser/browser_thread.h" */
/*-no- #include "content/browser/browsing_instance.h" */
/*-no- #include "content/browser/site_instance.h" */
/*-no- #include "grit/generated_resources.h" */
/*-no- #include "skia/ext/image_operations.h" */
/*-no- #include "ui/base/l10n/l10n_util.h" */
/*-no- #include "ui/gfx/codec/png_codec.h" */
/*-no- #include "ui/gfx/favicon_size.h" */
/*-no- #include "webkit/glue/image_decoder.h" */
/*-no-  */
/*-no- ................................ */
/*-no- #if defined(OS_WIN) */
/*-no- #include "ui/base/message_box_win.h" */
/*-no- #include "views/window/window.h" */
/*-no- #endif */
/*-no-  */
/*-no- ................................ */
/*-no-  */
/*-no- ............ */
/*-no-  */
/*-no- .............................................. */
/*-no-  */
/*-no- .................... */
/*-no- ........................ */
/*-no- ..................................... */
/*-no- ..................................... */
/*-no- . */
/*-no-  */
/*-no- ....................... */
/*-no- . */
/*-no-  */
/*-no- ......... */
/*-no- ............................................................................. */
/*-no- ...................................................................... */
/*-no- ..................................................... */
/*-no- ........................................................................... */
/*-no- ....................................................... */
/*-no- ...................... */
/*-no- ....................................... */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ..................................................... */
/*-no- .................................... */
/*-no- ...................................... */
/*-no- ...................................................... */
/*-no- ............................................ */
/*-no- ................................................................................ */
/*-no- ... */
/*-no-  */
/*-no- ................................ */
/*-no- .............................................................. */
/*-no- .............. */
/*-no- . */
/*-no-  */
/*-no- ................ */
/*-no-  */
/*-no- ............................ */
/*-no- ..................... */
/*-no- ...................... */
/*-no- .................. */
/*-no- ...................... */
/*-no- ............................................ */
/*-no- .......................................... */
/*-no- ................................ */
/*-no- ....................... */
/*-no- ............................ */
/*-no- . */
/*-no-  */
/*-no- ............................... */
/*-no- ........................ */
/*-no- ......................... */
/*-no-  */
/*-no- ..................................... */
/*-no- ................................................................................ */
/*-no- .................................................. */
/*-no- ....................................................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ................................................. */
/*-no- ............................ */
/*-no- .............................. */
/*-no- ......................................... */
/*-no- ....................................... */
/*-no-  */
/*-no- ................................................................ */
/*-no- ..................... */
/*-no- . */
/*-no-  */
/*-no- .................................................................... */
/*-no- ....................................... */
/*-no- . */
/*-no-  */
/*-no- ................................................. */
/*-no- ............................................................ */
/*-no- ................................................................ */
/*-no- .............................................................. */
/*-no- ......................... */
/*-no- ..................... */
/*-no- . */
/*-no-  */
/*-no- ........................................ */
/*-no- .................. */
/*-no- ......................... */
/*-no- .......... */
/*-no- ........................................................ */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- .................................................... */
/*-no- .................... */
/*-no- ................................................................... */
/*-no- .................................................................... */
/*-no- ....................................................... */
/*-no- .......................... */
/*-no- ....................................... */
/*-no- ................................. */
/*-no- ......................... */
/*-no- ............ */
/*-no- .......................... */
/*-no- ..... */
/*-no- .......... */
/*-no- .............................................................. */
/*-no- ...................................................... */
/*-no- ................. */
/*-no- ................. */
/*-no- ..................... */
/*-no- .................. */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ....................................... */
/*-no- .............................................. */
/*-no- ............................ */
/*-no- ................. */
/*-no- .......................... */
/*-no- ..................... */
/*-no- ............................................................................ */
/*-no- .............................. */
/*-no- ............................ */
/*-no- ........................................................................ */
/*-no- ........................................................................... */
/*-no- ...................... */
/*-no- ................... */
/*-no- ............................................................................ */
/*-no- .................................................................. */
/*-no- ................... */
/*-no- .................. */
/*-no- ........... */
/*-no- ... */
/*-no-  */
/*-no- ...................... */
/*-no-  */
/*-no- .............................................................................. */
/*-no- .................................................. */
/*-no- ....................................... */
/*-no- ........................................................ */
/*-no- .............................................................. */
/*-no- ................................... */
/*-no-  */
/*-no- ...................................................... */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- ............................................................................... */
/*-no- ................. */
/*-no- ............................................................... */
/*-no- ................................................................. */
/*-no- ..................... */
/*-no- ............... */
/*-no- ................................................ */
/*-no- ....................................................... */
/*-no- .............. */
/*-no- .............................. */
/*-no-  */
/*-no- ............................................................... */
/*-no- .......................................................... */
/*-no- ...................................... */
/*-no-  */
/*-no- .................................................................... */
/*-no- ...................................................... */
/*-no-  */
/*-no- .................................. */
/*-no- ..... */
/*-no- ... */
/*-no- #endif */
/*-no-  */
/*-no- .............................. */
/*-no- ........... */
/*-no- ..................... */
/*-no- . */
/*-no-  */
/*-no- ............................. */
/*-no- ................ */
/*-no- ........................ */
/*-no- . */
/*-no-  */
/*-no- .................................................... */
/*-no- ....................... */
/*-no- . */
/*-no-  */
/*-no- ....................................... */
/*-no- ................................................................ */
/*-no- ........................... */
/*-no- ........... */
/*-no- ................................................................. */
/*-no- . */
/*-no-  */
/*-no- ................................................................. */
/*-no- ................ */
/*-no- ....................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................... */
/*-no- ................ */
/*-no- ..................................... */
/*-no- . */
/*-no-  */
/*-no- .................................... */
/*-no- ................ */
/*-no- ............................... */
/*-no- . */
/*-no-  */
/*-no- .................................. */
/*-no- .............................................................. */
/*-no- ................ */
/*-no- ............................. */
/*-no- ............ */
/*-no- . */
/*-no-  */
/*-no- .............................................................. */
/*-no- .......................................... */
/*-no- ............................................................. */
/*-no- ............................................................................. */
/*-no- ... */
/*-no- ................................ */
/*-no- . */
/*-no-  */
/*-no- ....................................... */
/*-no- .............................................................................. */
/*-no- ........................................................ */
/*-no- ........................................................ */
/*-no- ................................. */
/*-no- .......................................................................... */
/*-no- ........................................ */
/*-no- ...................................................................... */
/*-no- .......................................................... */
/*-no- .................................. */
/*-no- .......................... */
/*-no- ..... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ....................................................... */
/*-no- ............................................................................. */
/*-no- .............................................................................. */
/*-no- ........................................................................... */
/*-no- .................................................... */
/*-no- .......................................... */
/*-no- ........................................ */
/*-no- ... */
/*-no-  */
/*-no- .............................................................................. */
/*-no- ............................................................................. */
/*-no- .................................................................... */
/*-no- ............................................................................ */
/*-no- ............................................... */
/*-no- ................................................................ */
/*-no- ....................................................................... */
/*-no- ...................................................... */
/*-no- .................... */
/*-no- ..... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- .......................................................... */
/*-no- ................ */
/*-no- ..................................... */
/*-no- ........................ */
/*-no- ....................................... */
/*-no- . */
/*-no-  */
/*-no- ................................................................ */
/*-no- ............................ */
/*-no- .............................. */
/*-no- ......................................... */
/*-no- ....................................... */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ........................................................................ */
/*-no- ............................................................ */
/*-no- ..................................................... */
/*-no- ..................... */
/*-no- . */
/*-no-  */
/*-no- ............................................ */
/*-no- .................... */
/*-no- ............................... */
/*-no- ...................... */
/*-no- ......................................................................... */
/*-no- . */
/*-no-  */
/*-no- ...................................................... */
/*-no- .............................................. */
/*-no- ............................ */
/*-no- ................. */
/*-no- .......................... */
/*-no- ..................... */
/*-no- .................... */
/*-no- ............................ */
/*-no- ................... */
/*-no- ................................ */
/*-no- ................. */
/*-no-  */
/*-no- .................................................................... */
/*-no-  */
/*-no- ................................................................. */
/*-no- ...................................................... */
/*-no- .............................. */
/*-no-  */
/*-no- ..................... */
/*-no- . */
/*-no-  */
/*-no- ...................................................... */
/*-no- ................................................................ */
/*-no- ......................................... */
/*-no- ........... */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ......................................................................... */
/*-no- ..................................................................... */
/*-no- ......................................................................... */
/*-no- ................................ */
/*-no- ................................... */
/*-no- ....................................................... */
/*-no- ........................................................................... */
/*-no- ............................................................................... */
/*-no- .................................. */
/*-no- ................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................. */
/*-no- ............................................... */
/*-no- .............................................. */
/*-no- .............. */
/*-no- .................................... */
/*-no- ................................ */
/*-no- ............................ */
/*-no- ................................ */
/*-no- ................................... */
/*-no- ................................ */
/*-no- ............................................ */
/*-no- ......................................... */
/*-no- .................................. */
/*-no- .................... */
/*-no- ...................................................... */
/*-no- ...................... */
/*-no- ......................... */
/*-no- .................... */
/*-no- .......................................... */
/*-no- . */
/*-no-  */
/*-no- ................................................................. */
/*-no- ..................... */
/*-no- . */
/*-no-  */
/*-no- ............................................. */
/*-no- .................................... */
/*-no- .............................. */
/*-no- ............................................................... */
/*-no- .......................... */
/*-no- ................................... */
/*-no- ............................. */
/*-no- ................................................................. */
/*-no- ..................................................................... */
/*-no- . */
/*-no-  */
/*-no- ........................................................... */
/*-no- ................................ */
/*-no- .................................. */
/*-no- ................................ */
/*-no- ......................................................... */
/*-no- ........................................................................ */
/*-no- ....................................... */
/*-no- . */
/*-no-  */
/*-no- .............................................. */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- .................... */
/*-no- ..................................... */
/*-no- ............................ */
/*-no- ..................................... */
/*-no- ............................... */
/*-no- ............................................................................ */
/*-no- ... */
/*-no- ............ */
/*-no- . */
/*-no-  */
/*-no- ..................................................................... */
/*-no- ............................................................. */
/*-no- . */
/*-no-  */
/*-no- ................................................................. */
/*-no- ....................................... */
/*-no- ...................................................................... */
/*-no- . */
/*-no-  */
/*-no- ..................................................................... */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- ................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................... */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- ............................. */
/*-no- .......................................... */
/*-no- ............ */
/*-no- . */
/*-no-  */
/*-no- ..................................................... */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- ........................... */
/*-no- . */
/*-no-  */
/*-no- .................................................................... */
/*-no- ............................................................................. */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- ................. */
/*-no- ........................................................... */
/*-no- ............ */
/*-no- . */
/*-no-  */
/*-no- ...................................................................... */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- ........................................................................... */
/*-no- . */
/*-no-  */
/*-no- ......................................................................... */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- .................................... */
/*-no- ................................................... */
/*-no- ........................................ */
/*-no- .......................... */
/*-no- ................................... */
/*-no- ............................. */
/*-no- ....................................................................... */
/*-no- ........................ */
/*-no- . */
/*-no-  */
/*-no- ......................................................... */
/*-no- ...................................... */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- ....................................................... */
/*-no- .................................................. */
/*-no- . */
/*-no-  */
/*-no- ......................................................... */
/*-no- ........................................................... */
/*-no- ....................... */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- ....................................................................... */
/*-no- ................................................. */
/*-no- ........................................................ */
/*-no- ........................................... */
/*-no- .............................................................................. */
/*-no- . */
/*-no-  */
/*-no- .......................................................... */
/*-no- ............................ */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- .................................. */
/*-no- . */
/*-no-  */
/*-no- ........................................................... */
/*-no- ......................................... */
/*-no- ................................................ */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- ................................................... */
/*-no- ............................... */
/*-no- ................................................. */
/*-no- ............................................ */
/*-no- . */
/*-no-  */
/*-no- ........................................................... */
/*-no- ....................................................................... */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- .......................................................................... */
/*-no- ............................ */
/*-no- .............................................................. */
/*-no- ........................................... */
/*-no- .................................................... */
/*-no- ......................................................................... */
/*-no- .................................................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- .......................................................... */
/*-no- ................................... */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- ................................................. */
/*-no- ............................................ */
/*-no- . */
/*-no-  */
/*-no- .......................................................... */
/*-no- ........................................................................ */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- .............................................................. */
/*-no- ........................................... */
/*-no- .................................................. */
/*-no- ..................................... */
/*-no- . */
/*-no-  */
/*-no- .............................................................. */
/*-no- ............................................ */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- ................................. */
/*-no- . */
/*-no-  */
/*-no- .......................................................... */
/*-no- .................................................. */
/*-no- .................................................................. */
/*-no- ................. */
/*-no- ........... */
/*-no-  */
/*-no- ............................................. */
/*-no- ................................................. */
/*-no- ................................................ */
/*-no- .................................... */
/*-no- ................................... */
/*-no- ................ */
/*-no- ....................................................... */
/*-no- ... */
/*-no- ............................................................... */
/*-no- ................................................ */
/*-no- . */

#endif
/* CONSBENCH file end */
