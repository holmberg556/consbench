/* CONSBENCH file begin */
#ifndef BROWSER_PREFS_CC_3339
#define BROWSER_PREFS_CC_3339

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_browser_prefs_browser_prefs_cpp, "chrome/browser/prefs/browser_prefs.cpp");

/* CONSBENCH includes begin */
#include "chrome/browser/prefs/browser_prefs.h"
#include "chrome/browser/about_flags.h"
#include "chrome/browser/autofill/autofill_manager.h"
#include "chrome/browser/background_contents_service.h"
#include "chrome/browser/background_mode_manager.h"
#include "chrome/browser/background_page_tracker.h"
#include "chrome/browser/bookmarks/bookmark_utils.h"
#include "chrome/browser/browser_shutdown.h"
#include "chrome/browser/content_settings/host_content_settings_map.h"
#include "chrome/browser/custom_handlers/protocol_handler_registry.h"
#include "chrome/browser/debugger/devtools_manager.h"
#include "chrome/browser/download/download_prefs.h"
#include "chrome/browser/extensions/extension_prefs.h"
#include "chrome/browser/extensions/extension_web_ui.h"
#include "chrome/browser/extensions/extensions_ui.h"
#include "chrome/browser/external_protocol_handler.h"
#include "chrome/browser/geolocation/geolocation_content_settings_map.h"
#include "chrome/browser/geolocation/geolocation_prefs.h"
#include "chrome/browser/google/google_url_tracker.h"
#include "chrome/browser/instant/instant_controller.h"
#include "chrome/browser/intranet_redirect_detector.h"
#include "chrome/browser/metrics/metrics_log.h"
#include "chrome/browser/metrics/metrics_service.h"
#include "chrome/browser/net/net_pref_observer.h"
#include "chrome/browser/net/predictor_api.h"
#include "chrome/browser/net/pref_proxy_config_service.h"
#include "chrome/browser/notifications/desktop_notification_service.h"
#include "chrome/browser/notifications/notification_ui_manager.h"
#include "chrome/browser/page_info_model.h"
#include "chrome/browser/password_manager/password_manager.h"
#include "chrome/browser/policy/browser_policy_connector.h"
#include "chrome/browser/policy/profile_policy_connector.h"
#include "chrome/browser/prefs/session_startup_pref.h"
#include "chrome/browser/profiles/profile_impl.h"
#include "chrome/browser/renderer_host/browser_render_process_host.h"
#include "chrome/browser/renderer_host/web_cache_manager.h"
#include "chrome/browser/safe_browsing/safe_browsing_service.h"
#include "chrome/browser/search_engines/template_url_model.h"
#include "chrome/browser/search_engines/template_url_prepopulate_data.h"
#include "chrome/browser/ssl/ssl_manager.h"
#include "chrome/browser/sync/signin_manager.h"
#include "chrome/browser/tabs/pinned_tab_codec.h"
#include "chrome/browser/task_manager/task_manager.h"
#include "chrome/browser/translate/translate_prefs.h"
#include "chrome/browser/ui/browser.h"
#include "chrome/browser/ui/search_engines/keyword_editor_controller.h"
#include "chrome/browser/ui/tab_contents/tab_contents_wrapper.h"
#include "chrome/browser/ui/webui/flags_ui.h"
#include "chrome/browser/ui/webui/new_tab_ui.h"
#include "chrome/browser/ui/webui/plugins_ui.h"
#include "chrome/browser/upgrade_detector.h"
#include "chrome/common/pref_names.h"
#include "content/browser/host_zoom_map.h"
#include "chrome/browser/ui/views/browser_actions_container.h"
#include "chrome/browser/ui/views/frame/browser_view.h"
#include "chrome/browser/ui/gtk/browser_window_gtk.h"
#include "chrome/browser/chromeos/audio_mixer_alsa.h"
#include "chrome/browser/chromeos/login/apply_services_customization.h"
#include "chrome/browser/chromeos/login/signed_settings_temp_storage.h"
#include "chrome/browser/chromeos/login/user_manager.h"
#include "chrome/browser/chromeos/login/wizard_controller.h"
#include "chrome/browser/chromeos/preferences.h"
#include "chrome/browser/chromeos/status/input_method_menu.h"
#include "chrome/browser/chromeos/user_cros_settings_provider.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_browser_prefs_browser_prefs_cpp, "chrome/browser/prefs/browser_prefs.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(chrome_browser_prefs_browser_prefs_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include "chrome/browser/prefs/browser_prefs.h" */
/*-no-  */
/*-no- #include "chrome/browser/about_flags.h" */
/*-no- #include "chrome/browser/autofill/autofill_manager.h" */
/*-no- #include "chrome/browser/background_contents_service.h" */
/*-no- #include "chrome/browser/background_mode_manager.h" */
/*-no- #include "chrome/browser/background_page_tracker.h" */
/*-no- #include "chrome/browser/bookmarks/bookmark_utils.h" */
/*-no- #include "chrome/browser/browser_shutdown.h" */
/*-no- #include "chrome/browser/content_settings/host_content_settings_map.h" */
/*-no- #include "chrome/browser/custom_handlers/protocol_handler_registry.h" */
/*-no- #include "chrome/browser/debugger/devtools_manager.h" */
/*-no- #include "chrome/browser/download/download_prefs.h" */
/*-no- #include "chrome/browser/extensions/extension_prefs.h" */
/*-no- #include "chrome/browser/extensions/extension_web_ui.h" */
/*-no- #include "chrome/browser/extensions/extensions_ui.h" */
/*-no- #include "chrome/browser/external_protocol_handler.h" */
/*-no- #include "chrome/browser/geolocation/geolocation_content_settings_map.h" */
/*-no- #include "chrome/browser/geolocation/geolocation_prefs.h" */
/*-no- #include "chrome/browser/google/google_url_tracker.h" */
/*-no- #include "chrome/browser/instant/instant_controller.h" */
/*-no- #include "chrome/browser/intranet_redirect_detector.h" */
/*-no- #include "chrome/browser/metrics/metrics_log.h" */
/*-no- #include "chrome/browser/metrics/metrics_service.h" */
/*-no- #include "chrome/browser/net/net_pref_observer.h" */
/*-no- #include "chrome/browser/net/predictor_api.h" */
/*-no- #include "chrome/browser/net/pref_proxy_config_service.h" */
/*-no- #include "chrome/browser/notifications/desktop_notification_service.h" */
/*-no- #include "chrome/browser/notifications/notification_ui_manager.h" */
/*-no- #include "chrome/browser/page_info_model.h" */
/*-no- #include "chrome/browser/password_manager/password_manager.h" */
/*-no- #include "chrome/browser/policy/browser_policy_connector.h" */
/*-no- #include "chrome/browser/policy/profile_policy_connector.h" */
/*-no- #include "chrome/browser/prefs/session_startup_pref.h" */
/*-no- #include "chrome/browser/profiles/profile_impl.h" */
/*-no- #include "chrome/browser/renderer_host/browser_render_process_host.h" */
/*-no- #include "chrome/browser/renderer_host/web_cache_manager.h" */
/*-no- #include "chrome/browser/safe_browsing/safe_browsing_service.h" */
/*-no- #include "chrome/browser/search_engines/template_url_model.h" */
/*-no- #include "chrome/browser/search_engines/template_url_prepopulate_data.h" */
/*-no- #include "chrome/browser/ssl/ssl_manager.h" */
/*-no- #include "chrome/browser/sync/signin_manager.h" */
/*-no- #include "chrome/browser/tabs/pinned_tab_codec.h" */
/*-no- #include "chrome/browser/task_manager/task_manager.h" */
/*-no- #include "chrome/browser/translate/translate_prefs.h" */
/*-no- #include "chrome/browser/ui/browser.h" */
/*-no- #include "chrome/browser/ui/search_engines/keyword_editor_controller.h" */
/*-no- #include "chrome/browser/ui/tab_contents/tab_contents_wrapper.h" */
/*-no- #include "chrome/browser/ui/webui/flags_ui.h" */
/*-no- #include "chrome/browser/ui/webui/new_tab_ui.h" */
/*-no- #include "chrome/browser/ui/webui/plugins_ui.h" */
/*-no- #include "chrome/browser/upgrade_detector.h" */
/*-no- #include "chrome/common/pref_names.h" */
/*-no- #include "content/browser/host_zoom_map.h" */
/*-no-  */
/*-no- #if defined(TOOLKIT_VIEWS)  // TODO(port): whittle this down as we port */
/*-no- #include "chrome/browser/ui/views/browser_actions_container.h" */
/*-no- #include "chrome/browser/ui/views/frame/browser_view.h" */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(TOOLKIT_GTK) */
/*-no- #include "chrome/browser/ui/gtk/browser_window_gtk.h" */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- #include "chrome/browser/chromeos/audio_mixer_alsa.h" */
/*-no- #include "chrome/browser/chromeos/login/apply_services_customization.h" */
/*-no- #include "chrome/browser/chromeos/login/signed_settings_temp_storage.h" */
/*-no- #include "chrome/browser/chromeos/login/user_manager.h" */
/*-no- #include "chrome/browser/chromeos/login/wizard_controller.h" */
/*-no- #include "chrome/browser/chromeos/preferences.h" */
/*-no- #include "chrome/browser/chromeos/status/input_method_menu.h" */
/*-no- #include "chrome/browser/chromeos/user_cros_settings_provider.h" */
/*-no- #endif */
/*-no-  */
/*-no- ................... */
/*-no-  */
/*-no- ................................................... */
/*-no- ......................... */
/*-no- ...................................... */
/*-no- ...................................... */
/*-no- .............................................. */
/*-no- ...................................................... */
/*-no- ............................................... */
/*-no- ....................................................... */
/*-no- ...................................................... */
/*-no- ......................................... */
/*-no- ............................................. */
/*-no- .................................................. */
/*-no- ............................................... */
/*-no- #if defined(TOOLKIT_VIEWS) */
/*-no- ..................................................... */
/*-no- #endif */
/*-no- .............................................. */
/*-no- .......................................... */
/*-no- .......................................... */
/*-no- ..................................................... */
/*-no- .................................................... */
/*-no- .................................................... */
/*-no- ..................................................... */
/*-no- ............................................................. */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ....................................................... */
/*-no- .................................................... */
/*-no- ................................................................. */
/*-no- ............................................... */
/*-no- ........................................................ */
/*-no- ................................................................... */
/*-no- .................................................................. */
/*-no- #endif */
/*-no- . */
/*-no-  */
/*-no- ................................................. */
/*-no- ............... */
/*-no- ................................................. */
/*-no- .................................................... */
/*-no- ......................................... */
/*-no- ................................................. */
/*-no- .................................................... */
/*-no- ............................................... */
/*-no- ................................................ */
/*-no- .................................................... */
/*-no- ............................................................ */
/*-no- ................................................ */
/*-no- .............................................. */
/*-no- .......................................... */
/*-no- ........................................... */
/*-no- ............................................. */
/*-no- ........................................................ */
/*-no- ............................................. */
/*-no- ................................................. */
/*-no- ................................................ */
/*-no- ................................................ */
/*-no- ............................................................... */
/*-no- ................................................ */
/*-no- ............................................................ */
/*-no- .................................................... */
/*-no- #if defined(TOOLKIT_VIEWS) */
/*-no- ......................................................... */
/*-no- #elif defined(TOOLKIT_GTK) */
/*-no- .................................................. */
/*-no- #endif */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ....................................................... */
/*-no- #endif */
/*-no- ........................................................... */
/*-no- ............................................... */
/*-no- .................................................. */
/*-no- ................................................... */
/*-no- ............................................. */
/*-no- ............................................................ */
/*-no- ..................................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................................. */
/*-no- .............................................................................. */
/*-no- .......................................................................... */
/*-no- ....................... */
/*-no- .................................................................... */
/*-no-  */
/*-no- ............................................... */
/*-no- ...................................................... */
/*-no- ........................................................................ */
/*-no- ...................................................................... */
/*-no- .................................................................. */
/*-no- ........................................................... */
/*-no- .................................................................. */
/*-no- ..... */
/*-no- .......................................................... */
/*-no-  */
/*-no- ....................................................... */
/*-no- ........................................................................ */
/*-no- ....................................................................... */
/*-no- ................................................................... */
/*-no- ..................................................... */
/*-no- ......................................................................... */
/*-no- ........................... */
/*-no- ..... */
/*-no- ........................................................... */
/*-no-  */
/*-no- ................................................................. */
/*-no- ............................................................. */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ....................... */

#endif
/* CONSBENCH file end */
