/* CONSBENCH file begin */
#ifndef MAIN_LIBSYNC_CPP
#define MAIN_LIBSYNC_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_browser_sync_engine_main_libsync_cpp, "chrome/browser/sync/engine/main_libsync.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_browser_sync_engine_main_libsync_cpp, "chrome/browser/sync/engine/main_libsync.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_sync_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_encryption_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_app_specifics_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_autofill_specifics_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_bookmark_specifics_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_extension_specifics_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_nigori_specifics_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_password_specifics_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_preference_specifics_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_session_specifics_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_theme_specifics_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_typed_url_specifics_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_all_status_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_apply_updates_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_build_and_process_conflict_sets_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_build_commit_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_change_reorder_buffer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_cleanup_disabled_types_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_clear_data_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_conflict_resolver_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_download_updates_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_get_commit_ids_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_idle_query_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_model_changing_syncer_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_model_safe_worker_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_net_server_connection_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_net_syncapi_server_connection_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_net_url_translator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_polling_constants_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_post_commit_message_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_process_commit_response_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_process_updates_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_resolve_conflicts_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_store_timestamps_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_syncer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_syncer_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_syncer_end_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_syncer_proto_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_syncer_thread_adapter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_syncer_thread2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_syncer_thread_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_syncer_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_update_applicator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_engine_verify_updates_command_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_js_arg_list_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_js_event_handler_list_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_js_sync_manager_observer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_protocol_proto_enum_conversions_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_protocol_proto_value_conversions_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_sessions_ordered_commit_set_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_sessions_session_state_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_sessions_status_controller_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_sessions_sync_session_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_sessions_sync_session_context_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_syncable_directory_backing_store_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_syncable_directory_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_syncable_model_type_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_syncable_nigori_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_syncable_syncable_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_syncable_syncable_id_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_util_crypto_helpers_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_util_cryptographer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_util_extensions_activity_monitor_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_util_nigori_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_util_user_settings_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_util_user_settings_posix_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_browser_sync_engine_main_libsync_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_sync_pb_cpp(it);
  PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_encryption_pb_cpp(it);
  PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_app_specifics_pb_cpp(it);
  PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_autofill_specifics_pb_cpp(it);
  PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_bookmark_specifics_pb_cpp(it);
  PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_extension_specifics_pb_cpp(it);
  PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_nigori_specifics_pb_cpp(it);
  PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_password_specifics_pb_cpp(it);
  PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_preference_specifics_pb_cpp(it);
  PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_session_specifics_pb_cpp(it);
  PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_theme_specifics_pb_cpp(it);
  PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_sync_protocol_typed_url_specifics_pb_cpp(it);
  PUBLIC_chrome_browser_sync_engine_all_status_cpp(it);
  PUBLIC_chrome_browser_sync_engine_apply_updates_command_cpp(it);
  PUBLIC_chrome_browser_sync_engine_build_and_process_conflict_sets_command_cpp(it);
  PUBLIC_chrome_browser_sync_engine_build_commit_command_cpp(it);
  PUBLIC_chrome_browser_sync_engine_change_reorder_buffer_cpp(it);
  PUBLIC_chrome_browser_sync_engine_cleanup_disabled_types_command_cpp(it);
  PUBLIC_chrome_browser_sync_engine_clear_data_command_cpp(it);
  PUBLIC_chrome_browser_sync_engine_conflict_resolver_cpp(it);
  PUBLIC_chrome_browser_sync_engine_download_updates_command_cpp(it);
  PUBLIC_chrome_browser_sync_engine_get_commit_ids_command_cpp(it);
  PUBLIC_chrome_browser_sync_engine_idle_query_linux_cpp(it);
  PUBLIC_chrome_browser_sync_engine_model_changing_syncer_command_cpp(it);
  PUBLIC_chrome_browser_sync_engine_model_safe_worker_cpp(it);
  PUBLIC_chrome_browser_sync_engine_net_server_connection_manager_cpp(it);
  PUBLIC_chrome_browser_sync_engine_net_syncapi_server_connection_manager_cpp(it);
  PUBLIC_chrome_browser_sync_engine_net_url_translator_cpp(it);
  PUBLIC_chrome_browser_sync_engine_polling_constants_cpp(it);
  PUBLIC_chrome_browser_sync_engine_post_commit_message_command_cpp(it);
  PUBLIC_chrome_browser_sync_engine_process_commit_response_command_cpp(it);
  PUBLIC_chrome_browser_sync_engine_process_updates_command_cpp(it);
  PUBLIC_chrome_browser_sync_engine_resolve_conflicts_command_cpp(it);
  PUBLIC_chrome_browser_sync_engine_store_timestamps_command_cpp(it);
  PUBLIC_chrome_browser_sync_engine_syncer_cpp(it);
  PUBLIC_chrome_browser_sync_engine_syncer_command_cpp(it);
  PUBLIC_chrome_browser_sync_engine_syncer_end_command_cpp(it);
  PUBLIC_chrome_browser_sync_engine_syncer_proto_util_cpp(it);
  PUBLIC_chrome_browser_sync_engine_syncer_thread_adapter_cpp(it);
  PUBLIC_chrome_browser_sync_engine_syncer_thread2_cpp(it);
  PUBLIC_chrome_browser_sync_engine_syncer_thread_cpp(it);
  PUBLIC_chrome_browser_sync_engine_syncer_util_cpp(it);
  PUBLIC_chrome_browser_sync_engine_update_applicator_cpp(it);
  PUBLIC_chrome_browser_sync_engine_verify_updates_command_cpp(it);
  PUBLIC_chrome_browser_sync_js_arg_list_cpp(it);
  PUBLIC_chrome_browser_sync_js_event_handler_list_cpp(it);
  PUBLIC_chrome_browser_sync_js_sync_manager_observer_cpp(it);
  PUBLIC_chrome_browser_sync_protocol_proto_enum_conversions_cpp(it);
  PUBLIC_chrome_browser_sync_protocol_proto_value_conversions_cpp(it);
  PUBLIC_chrome_browser_sync_sessions_ordered_commit_set_cpp(it);
  PUBLIC_chrome_browser_sync_sessions_session_state_cpp(it);
  PUBLIC_chrome_browser_sync_sessions_status_controller_cpp(it);
  PUBLIC_chrome_browser_sync_sessions_sync_session_cpp(it);
  PUBLIC_chrome_browser_sync_sessions_sync_session_context_cpp(it);
  PUBLIC_chrome_browser_sync_syncable_directory_backing_store_cpp(it);
  PUBLIC_chrome_browser_sync_syncable_directory_manager_cpp(it);
  PUBLIC_chrome_browser_sync_syncable_model_type_cpp(it);
  PUBLIC_chrome_browser_sync_syncable_nigori_util_cpp(it);
  PUBLIC_chrome_browser_sync_syncable_syncable_cpp(it);
  PUBLIC_chrome_browser_sync_syncable_syncable_id_cpp(it);
  PUBLIC_chrome_browser_sync_util_crypto_helpers_cpp(it);
  PUBLIC_chrome_browser_sync_util_cryptographer_cpp(it);
  PUBLIC_chrome_browser_sync_util_extensions_activity_monitor_cpp(it);
  PUBLIC_chrome_browser_sync_util_nigori_cpp(it);
  PUBLIC_chrome_browser_sync_util_user_settings_cpp(it);
  PUBLIC_chrome_browser_sync_util_user_settings_posix_cpp(it);
}

#endif

