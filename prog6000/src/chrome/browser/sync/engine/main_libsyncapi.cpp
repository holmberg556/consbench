/* CONSBENCH file begin */
#ifndef MAIN_LIBSYNCAPI_CPP
#define MAIN_LIBSYNCAPI_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_browser_sync_engine_main_libsyncapi_cpp, "chrome/browser/sync/engine/main_libsyncapi.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_browser_sync_engine_main_libsyncapi_cpp, "chrome/browser/sync/engine/main_libsyncapi.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_chrome_browser_sync_engine_syncapi_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_browser_sync_engine_main_libsyncapi_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_chrome_browser_sync_engine_syncapi_cpp(it);
}

#endif

