/* CONSBENCH file begin */
#ifndef AUTOFILL_PROFILE_CHANGE_PROCESSOR_H_3799
#define AUTOFILL_PROFILE_CHANGE_PROCESSOR_H_3799

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_browser_sync_glue_autofill_profile_change_processor_h, "chrome/browser/sync/glue/autofill_profile_change_processor.h");

/* CONSBENCH includes begin */
#include <string>
#include <vector>
#include "chrome/browser/autofill/autofill_profile.h"
#include "chrome/browser/autofill/personal_data_manager.h"
#include "chrome/browser/sync/engine/syncapi.h"
#include "chrome/browser/sync/glue/autofill_profile_model_associator.h"
#include "chrome/browser/sync/glue/change_processor.h"
#include "chrome/browser/sync/unrecoverable_error_handler.h"
#include "chrome/browser/webdata/autofill_change.h"
#include "chrome/browser/webdata/web_database.h"
#include "chrome/common/notification_observer.h"
#include "chrome/common/notification_registrar.h"
#include "chrome/common/notification_service.h"
#include "chrome/common/notification_type.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_browser_sync_glue_autofill_profile_change_processor_h, "chrome/browser/sync/glue/autofill_profile_change_processor.h");

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #ifndef CHROME_BROWSER_SYNC_GLUE_AUTOFILL_PROFILE_CHANGE_PROCESSOR_H_ */
/*-no- #define CHROME_BROWSER_SYNC_GLUE_AUTOFILL_PROFILE_CHANGE_PROCESSOR_H_ */
/*-no- #pragma once */
/*-no- #include <string> */
/*-no- #include <vector> */
/*-no-  */
/*-no- #include "chrome/browser/autofill/autofill_profile.h" */
/*-no- #include "chrome/browser/autofill/personal_data_manager.h" */
/*-no- #include "chrome/browser/sync/engine/syncapi.h" */
/*-no- #include "chrome/browser/sync/glue/autofill_profile_model_associator.h" */
/*-no- #include "chrome/browser/sync/glue/change_processor.h" */
/*-no- #include "chrome/browser/sync/unrecoverable_error_handler.h" */
/*-no- #include "chrome/browser/webdata/autofill_change.h" */
/*-no- #include "chrome/browser/webdata/web_database.h" */
/*-no- #include "chrome/common/notification_observer.h" */
/*-no- #include "chrome/common/notification_registrar.h" */
/*-no- #include "chrome/common/notification_service.h" */
/*-no- #include "chrome/common/notification_type.h" */
/*-no-  */
/*-no- ........................ */
/*-no-  */
/*-no- .............................................................. */
/*-no- ................................. */
/*-no- ........ */
/*-no- ................................. */
/*-no- ....................................................... */
/*-no- ................................ */
/*-no- ................................................. */
/*-no- ................................................ */
/*-no-  */
/*-no- ............................................ */
/*-no-  */
/*-no- ................................................ */
/*-no- ......................................... */
/*-no- ................................................... */
/*-no- ......................................................... */
/*-no- ........................ */
/*-no-  */
/*-no- ............................................ */
/*-no-  */
/*-no- ....................................................... */
/*-no- ............................................. */
/*-no- ........................................................ */
/*-no- ........................................................... */
/*-no-  */
/*-no-  */
/*-no- .................................................................. */
/*-no- .............................................................. */
/*-no-  */
/*-no- ........... */
/*-no- ............................................ */
/*-no- ............................................. */
/*-no- ............................ */
/*-no-  */
/*-no- ...................................... */
/*-no- ........................................................ */
/*-no- .............. */
/*-no- ......................................................... */
/*-no- ................................ */
/*-no- ........................................................... */
/*-no- ................. */
/*-no- .................................................................. */
/*-no- .......................... */
/*-no- .................. */
/*-no- .................................................. */
/*-no- .... */
/*-no-  */
/*-no- ............................................................. */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ................................................ */
/*-no- ...................................... */
/*-no-  */
/*-no- ................................................. */
/*-no- ........................................ */
/*-no- ......................................... */
/*-no-  */
/*-no- ......... */
/*-no-  */
/*-no- ......................................................................... */
/*-no- ................................................. */
/*-no- ............................................................................ */
/*-no- ............................. */
/*-no- .......... */
/*-no- ............................................................................ */
/*-no- ........................... */
/*-no-  */
/*-no- ........... */
/*-no- ............................ */
/*-no- ............................................... */
/*-no- .... */
/*-no-  */
/*-no- ........................ */
/*-no- ....................... */
/*-no-  */
/*-no- ................................... */
/*-no-  */
/*-no- .................................. */
/*-no- ....................................................... */
/*-no- ..................................................... */
/*-no- ................... */
/*-no-  */
/*-no- ...................... */
/*-no- .................................................................. */
/*-no-  */
/*-no- .................................................... */
/*-no- .................. */
/*-no-  */
/*-no- ............................. */
/*-no- ...................................... */
/*-no- ................................................ */
/*-no- .. */
/*-no-  */
/*-no- ............................ */
/*-no-  */
/*-no- #endif  // CHROME_BROWSER_SYNC_GLUE_AUTOFILL_PROFILE_CHANGE_PROCESSOR_H_ */
/*-no-  */

#endif
/* CONSBENCH file end */
