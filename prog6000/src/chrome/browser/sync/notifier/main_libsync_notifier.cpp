/* CONSBENCH file begin */
#ifndef MAIN_LIBSYNC_NOTIFIER_CPP
#define MAIN_LIBSYNC_NOTIFIER_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_browser_sync_notifier_main_libsync_notifier_cpp, "chrome/browser/sync/notifier/main_libsync_notifier.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_browser_sync_notifier_main_libsync_notifier_cpp, "chrome/browser/sync/notifier/main_libsync_notifier.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_chrome_browser_sync_notifier_cache_invalidation_packet_handler_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_notifier_chrome_invalidation_client_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_notifier_chrome_system_resources_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_notifier_invalidation_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_notifier_registration_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_sync_notifier_server_notifier_thread_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_browser_sync_notifier_main_libsync_notifier_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_chrome_browser_sync_notifier_cache_invalidation_packet_handler_cpp(it);
  PUBLIC_chrome_browser_sync_notifier_chrome_invalidation_client_cpp(it);
  PUBLIC_chrome_browser_sync_notifier_chrome_system_resources_cpp(it);
  PUBLIC_chrome_browser_sync_notifier_invalidation_util_cpp(it);
  PUBLIC_chrome_browser_sync_notifier_registration_manager_cpp(it);
  PUBLIC_chrome_browser_sync_notifier_server_notifier_thread_cpp(it);
}

#endif

