/* CONSBENCH file begin */
#ifndef FIRST_RUN_DIALOG_CC_4499
#define FIRST_RUN_DIALOG_CC_4499

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_browser_ui_gtk_first_run_dialog_cpp, "chrome/browser/ui/gtk/first_run_dialog.cpp");

/* CONSBENCH includes begin */
#include "chrome/browser/ui/gtk/first_run_dialog.h"
#include <string>
#include <vector>
#include "base/i18n/rtl.h"
#include "base/message_loop.h"
#include "base/utf_string_conversions.h"
#include "chrome/browser/google/google_util.h"
#include "chrome/browser/platform_util.h"
#include "chrome/browser/process_singleton.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/search_engines/template_url.h"
#include "chrome/browser/search_engines/template_url_model.h"
#include "chrome/browser/shell_integration.h"
#include "chrome/browser/ui/gtk/gtk_chrome_link_button.h"
#include "chrome/browser/ui/gtk/gtk_floating_container.h"
#include "chrome/browser/ui/gtk/gtk_util.h"
#include "chrome/common/pref_names.h"
#include "chrome/common/url_constants.h"
#include "chrome/installer/util/google_update_settings.h"
#include "grit/chromium_strings.h"
#include "grit/generated_resources.h"
#include "grit/locale_settings.h"
#include "grit/theme_resources.h"
#include "ui/base/l10n/l10n_util.h"
#include "ui/base/resource/resource_bundle.h"
#include "chrome/app/breakpad_linux.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/prefs/pref_service.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_browser_ui_gtk_first_run_dialog_cpp, "chrome/browser/ui/gtk/first_run_dialog.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(chrome_browser_ui_gtk_first_run_dialog_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include "chrome/browser/ui/gtk/first_run_dialog.h" */
/*-no-  */
/*-no- #include <string> */
/*-no- #include <vector> */
/*-no-  */
/*-no- #include "base/i18n/rtl.h" */
/*-no- #include "base/message_loop.h" */
/*-no- #include "base/utf_string_conversions.h" */
/*-no- #include "chrome/browser/google/google_util.h" */
/*-no- #include "chrome/browser/platform_util.h" */
/*-no- #include "chrome/browser/process_singleton.h" */
/*-no- #include "chrome/browser/profiles/profile.h" */
/*-no- #include "chrome/browser/search_engines/template_url.h" */
/*-no- #include "chrome/browser/search_engines/template_url_model.h" */
/*-no- #include "chrome/browser/shell_integration.h" */
/*-no- #include "chrome/browser/ui/gtk/gtk_chrome_link_button.h" */
/*-no- #include "chrome/browser/ui/gtk/gtk_floating_container.h" */
/*-no- #include "chrome/browser/ui/gtk/gtk_util.h" */
/*-no- #include "chrome/common/pref_names.h" */
/*-no- #include "chrome/common/url_constants.h" */
/*-no- #include "chrome/installer/util/google_update_settings.h" */
/*-no- #include "grit/chromium_strings.h" */
/*-no- #include "grit/generated_resources.h" */
/*-no- #include "grit/locale_settings.h" */
/*-no- #include "grit/theme_resources.h" */
/*-no- #include "ui/base/l10n/l10n_util.h" */
/*-no- #include "ui/base/resource/resource_bundle.h" */
/*-no-  */
/*-no- #if defined(USE_LINUX_BREAKPAD) */
/*-no- #include "chrome/app/breakpad_linux.h" */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(GOOGLE_CHROME_BUILD) */
/*-no- #include "chrome/browser/browser_process.h" */
/*-no- #include "chrome/browser/prefs/pref_service.h" */
/*-no- #endif */
/*-no-  */
/*-no- ........... */
/*-no-  */
/*-no- ............................................................. */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ............................ */
/*-no- ................................. */
/*-no-  */
/*-no- .............................................................. */
/*-no- ..................................... */
/*-no- ..................................... */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ............................................................................. */
/*-no- .............................................................................. */
/*-no- .......................... */
/*-no- ................................... */
/*-no-  */
/*-no- ................................................................................ */
/*-no- ...................................................... */
/*-no-  */
/*-no- .................................................... */
/*-no- ................................... */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ......................................................... */
/*-no- ........................................................ */
/*-no- .................................................. */
/*-no- ........................................... */
/*-no- ........................ */
/*-no- ................................... */
/*-no-  */
/*-no- ..................... */
/*-no- ....................................... */
/*-no-  */
/*-no- ............................... */
/*-no- ..................................................................... */
/*-no- .................................... */
/*-no- ............................. */
/*-no- ............................................................ */
/*-no- ....................................................... */
/*-no-  */
/*-no- .................................................. */
/*-no- ............................. */
/*-no- ............................................................ */
/*-no- ....................................................... */
/*-no- ........................ */
/*-no- . */
/*-no-  */
/*-no- ............... */
/*-no-  */
/*-no- ......... */
/*-no- ........................................... */
/*-no- ............................................................... */
/*-no- ........................................... */
/*-no- ............................................................... */
/*-no- ................................................................................ */
/*-no- ........................................................... */
/*-no- ......................................................... */
/*-no-  */
/*-no- #if defined(GOOGLE_CHROME_BUILD) */
/*-no- ....................................................... */
/*-no- ......................................................... */
/*-no- ....................................................... */
/*-no- ........................................... */
/*-no- ......................................................... */
/*-no- ........................................... */
/*-no- #else */
/*-no- ..................................... */
/*-no- #endif */
/*-no-  */
/*-no- ............................................................ */
/*-no- .................................. */
/*-no-  */
/*-no- .................... */
/*-no- ........................... */
/*-no- ............................. */
/*-no- ........................................... */
/*-no- ................................................ */
/*-no- ................................ */
/*-no-  */
/*-no- ................................................................ */
/*-no- ....................................................... */
/*-no- ........................................................... */
/*-no- ............................................................. */
/*-no- ........................................................... */
/*-no- ................................ */
/*-no-  */
/*-no- ........................................... */
/*-no- . */
/*-no-  */
/*-no- ................................................ */
/*-no- .......................................................... */
/*-no- ............................................................... */
/*-no- ............................................. */
/*-no- .................................. */
/*-no- .................... */
/*-no- ............................ */
/*-no- .......................... */
/*-no- ........................ */
/*-no- .................................. */
/*-no- .................................................... */
/*-no- ........................... */
/*-no- .................................... */
/*-no- .......................... */
/*-no- ........... */
/*-no- ... */
/*-no- .......................................................... */
/*-no-  */
/*-no- ........................... */
/*-no-  */
/*-no- ........................................... */
/*-no- ...................................... */
/*-no- ................................ */
/*-no- ...... */
/*-no- .................................. */
/*-no- . */
/*-no-  */
/*-no- ................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................... */
/*-no- .............................................................. */
/*-no- ..................................................................... */
/*-no- ....................... */
/*-no- ........................................ */
/*-no- ................................................................ */
/*-no- ..................................................................... */
/*-no- .................................................... */
/*-no- ....................................................................... */
/*-no- ................................................... */
/*-no- ........................................................................ */
/*-no-  */
/*-no- ..................... */
/*-no- ................................................................... */
/*-no- ........................................ */
/*-no- ........................................................... */
/*-no- ........................... */
/*-no- .................................................... */
/*-no- ................................................ */
/*-no-  */
/*-no- ......................................................... */
/*-no- .......................................................... */
/*-no- .......................................................................... */
/*-no- ........................... */
/*-no- ....................................................... */
/*-no-  */
/*-no- ..................................................... */
/*-no- ........................................................ */
/*-no- ....................................................................... */
/*-no- ....................................................... */
/*-no- ..................................................... */
/*-no- .................................................................... */
/*-no-  */
/*-no- ..................................................... */
/*-no- ...................................... */
/*-no-  */
/*-no- .......................................................... */
/*-no- .............................................. */
/*-no- ............................................................... */
/*-no-  */
/*-no- ...................................................... */
/*-no- ................................................................ */
/*-no- ................................................................ */
/*-no- .......................................................... */
/*-no- ..................................... */
/*-no-  */
/*-no- ......................................... */
/*-no- ................................................... */
/*-no- ................................................................ */
/*-no- ............................................................. */
/*-no- .......................................................... */
/*-no- ............................................................................. */
/*-no-  */
/*-no- ................................................................ */
/*-no- ...................................................................... */
/*-no- .............................................. */
/*-no- .......................................................................... */
/*-no- ................................................................... */
/*-no- ...................................... */
/*-no-  */
/*-no- ................................................................... */
/*-no- .................................... */
/*-no-  */
/*-no- .................................... */
/*-no- ........................................................ */
/*-no- . */
/*-no-  */
/*-no- ............................................ */
/*-no- ........................................................................... */
/*-no- ............................................................................... */
/*-no- ............................................................................ */
/*-no- ............................................................................ */
/*-no- ..................................................................... */
/*-no- ................................ */
/*-no- ................................................ */
/*-no- ........... */
/*-no- ... */
/*-no-  */
/*-no- ........................................ */
/*-no- ............................................................... */
/*-no- ......................... */
/*-no- .................................................................... */
/*-no- ............ */
/*-no- ...................................... */
/*-no- ............................................................ */
/*-no- ............................................ */
/*-no- ....................................................... */
/*-no-  */
/*-no- ....................................................... */
/*-no-  */
/*-no- ........................................... */
/*-no- ................................................................ */
/*-no-  */
/*-no- ...................................................... */
/*-no-  */
/*-no- .................................................. */
/*-no- .......................................................................... */
/*-no- ....................................................................... */
/*-no- ............................................................................ */
/*-no-  */
/*-no- ........................................... */
/*-no- ......................................... */
/*-no- .................................................................... */
/*-no- ........................................................ */
/*-no- ................................................................. */
/*-no- ...................................................... */
/*-no- ............................................................... */
/*-no- ...................................... */
/*-no-  */
/*-no- .......................................................... */
/*-no- ........................................................ */
/*-no- .................................................................. */
/*-no- .............................................. */
/*-no- ............................................................. */
/*-no- ...................................... */
/*-no- .............................................. */
/*-no- .................................................................. */
/*-no-  */
/*-no- .............................................................................. */
/*-no-  */
/*-no- ....................................... */
/*-no- ............................................................ */
/*-no- ............................... */
/*-no- . */
/*-no-  */
/*-no- .................................................. */
/*-no- ........................................................................ */
/*-no- ................................................................... */
/*-no- .............................................. */
/*-no-  */
/*-no- ................................................................... */
/*-no- .................................................. */
/*-no- ............................................... */
/*-no- ................................. */
/*-no- ................................................ */
/*-no- ............................................. */
/*-no-  */
/*-no- ............................................ */
/*-no- ........................................................ */
/*-no- ....................................... */
/*-no- ..................................... */
/*-no- ......................................... */
/*-no- ............................. */
/*-no- .................................................... */
/*-no- ... */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ..................................................................... */
/*-no- .................................. */
/*-no- ................................................. */
/*-no- .............................. */
/*-no- ........................................................ */
/*-no- ..................................................................... */
/*-no-  */
/*-no- ............................................................ */
/*-no- ............................. */
/*-no- #if defined(GOOGLE_CHROME_BUILD) */
/*-no- ....................... */
/*-no- #endif */
/*-no-  */
/*-no- ................................ */
/*-no- ................................................... */
/*-no- ..................................... */
/*-no- ......................... */
/*-no- ...................................................................... */
/*-no- ...................................................... */
/*-no- ................................................ */
/*-no- .............................................................. */
/*-no- ............................................................... */
/*-no- ........................................................... */
/*-no- .............. */
/*-no- ............................. */
/*-no- ....... */
/*-no-  */
/*-no- ........................................................... */
/*-no- ................................................................ */
/*-no- ............................. */
/*-no- ............ */
/*-no- .................................................. */
/*-no- ............................................. */
/*-no- ....................................................................... */
/*-no- .................................................................... */
/*-no- .......................................................... */
/*-no- ..................... */
/*-no- ................................................. */
/*-no- ............................................................................. */
/*-no- ........................................................................ */
/*-no- ..................................................................... */
/*-no- ..... */
/*-no-  */
/*-no- ......................... */
/*-no- ....................................................................... */
/*-no- ....................................... */
/*-no- ......................................................................... */
/*-no- ......................................................... */
/*-no- ..................................................................... */
/*-no-  */
/*-no- ........................................................ */
/*-no- ......................................................................... */
/*-no- ........................................................................ */
/*-no-  */
/*-no- ................................................................ */
/*-no- ............................................. */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ..................................................................... */
/*-no- .................................................... */
/*-no- ............................................................. */
/*-no- ............................................ */
/*-no- . */
/*-no-  */
/*-no- ..................................................................... */
/*-no- ............................... */
/*-no- .............................. */
/*-no- ........................................................................... */
/*-no- .......................... */
/*-no- .......... */
/*-no- ................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ........................................................................ */
/*-no- .............. */
/*-no- ................................. */
/*-no- ........................ */
/*-no-  */
/*-no- ................................. */
/*-no- ............................. */
/*-no-  */
/*-no- ............................................ */
/*-no- ........................ */
/*-no- ......................................................................... */
/*-no- #if defined(USE_LINUX_BREAKPAD) */
/*-no- ........................................................... */
/*-no- .......................... */
/*-no- #endif */
/*-no- .......... */
/*-no- ........................................................ */
/*-no- ... */
/*-no-  */
/*-no- ........................................ */
/*-no- ...................... */
/*-no- ....................................................................... */
/*-no- ............................................ */
/*-no- ... */
/*-no-  */
/*-no- ................. */
/*-no- . */
/*-no-  */
/*-no- ................................................................ */
/*-no- ................................................................... */
/*-no- ............................................. */
/*-no- . */
/*-no-  */
/*-no- ..................................... */
/*-no- ..................................... */
/*-no-  */
/*-no- .............. */
/*-no- ................................ */
/*-no- ................................. */
/*-no- .............. */
/*-no- . */

#endif
/* CONSBENCH file end */
