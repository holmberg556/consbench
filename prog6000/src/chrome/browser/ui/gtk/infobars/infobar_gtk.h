/* CONSBENCH file begin */
#ifndef INFOBAR_GTK_H_4554
#define INFOBAR_GTK_H_4554

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_browser_ui_gtk_infobars_infobar_gtk_h, "chrome/browser/ui/gtk/infobars/infobar_gtk.h");

/* CONSBENCH includes begin */
#include "base/basictypes.h"
#include "base/scoped_ptr.h"
#include "chrome/browser/tab_contents/infobar_delegate.h"
#include "chrome/browser/ui/gtk/infobars/infobar_arrow_model.h"
#include "chrome/browser/ui/gtk/owned_widget_gtk.h"
#include "chrome/browser/ui/gtk/slide_animator_gtk.h"
#include "content/common/notification_observer.h"
#include "content/common/notification_registrar.h"
#include "third_party/skia/include/core/SkPaint.h"
#include "ui/base/gtk/gtk_signal.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_browser_ui_gtk_infobars_infobar_gtk_h, "chrome/browser/ui/gtk/infobars/infobar_gtk.h");

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #ifndef CHROME_BROWSER_UI_GTK_INFOBARS_INFOBAR_GTK_H_ */
/*-no- #define CHROME_BROWSER_UI_GTK_INFOBARS_INFOBAR_GTK_H_ */
/*-no- #pragma once */
/*-no-  */
/*-no- #include "base/basictypes.h" */
/*-no- #include "base/scoped_ptr.h" */
/*-no- #include "chrome/browser/tab_contents/infobar_delegate.h" */
/*-no- #include "chrome/browser/ui/gtk/infobars/infobar_arrow_model.h" */
/*-no- #include "chrome/browser/ui/gtk/owned_widget_gtk.h" */
/*-no- #include "chrome/browser/ui/gtk/slide_animator_gtk.h" */
/*-no- #include "content/common/notification_observer.h" */
/*-no- #include "content/common/notification_registrar.h" */
/*-no- #include "third_party/skia/include/core/SkPaint.h" */
/*-no- #include "ui/base/gtk/gtk_signal.h" */
/*-no-  */
/*-no- ....................... */
/*-no- ....................... */
/*-no- .......................... */
/*-no- ...................... */
/*-no-  */
/*-no- .................................................. */
/*-no- ............................................ */
/*-no- .................................................... */
/*-no- ........ */
/*-no- .............................................. */
/*-no- ..................... */
/*-no-  */
/*-no- ......................................................... */
/*-no-  */
/*-no- .......................................................... */
/*-no- ...................... */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ............................................ */
/*-no- ................................................................................ */
/*-no-  */
/*-no- ....................................... */
/*-no- ..................... */
/*-no-  */
/*-no- ................................... */
/*-no- .............. */
/*-no-  */
/*-no- ......................................................................... */
/*-no- .......................................................... */
/*-no- ...................... */
/*-no-  */
/*-no- ............................................................................... */
/*-no- ................................................................................ */
/*-no- ............... */
/*-no- ............... */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ..................... */
/*-no-  */
/*-no- ................................................................ */
/*-no- ................... */
/*-no-  */
/*-no- .......................................................... */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ................................................................. */
/*-no- .................................................. */
/*-no-  */
/*-no- ................................................ */
/*-no- ................................... */
/*-no-  */
/*-no- ............................................... */
/*-no- ........................ */
/*-no-  */
/*-no- ......................................... */
/*-no- ............................................. */
/*-no- ........................................................ */
/*-no- ........................................................... */
/*-no-  */
/*-no- ................................................................ */
/*-no- ....................................................................... */
/*-no- ...................................................... */
/*-no- ............................................................ */
/*-no- ......................................................... */
/*-no- ............................................................... */
/*-no-  */
/*-no- ...................................... */
/*-no- .................................. */
/*-no-  */
/*-no- ........... */
/*-no- ................................................ */
/*-no- ...................................... */
/*-no- ............................. */
/*-no- ........................................ */
/*-no-  */
/*-no- ............................................................................ */
/*-no- .............................................. */
/*-no- ............................. */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ............................................................................... */
/*-no- ............................................................................. */
/*-no- ............................................................................. */
/*-no- ........................................................... */
/*-no- ........................................................ */
/*-no- ................................................. */
/*-no- .................................................. */
/*-no-  */
/*-no- ......................................... */
/*-no- ............................................. */
/*-no-  */
/*-no- ......................................................................... */
/*-no- ..................... */
/*-no-  */
/*-no- ..................................................................... */
/*-no- ................... */
/*-no-  */
/*-no- ............................... */
/*-no- ............................................. */
/*-no-  */
/*-no- ............................ */
/*-no- .................................. */
/*-no-  */
/*-no- ............................ */
/*-no- ............................. */
/*-no-  */
/*-no- ........................................................ */
/*-no- .................................... */
/*-no-  */
/*-no- ....................................................................... */
/*-no- ................................... */
/*-no- ................................. */
/*-no-  */
/*-no- ................................... */
/*-no-  */
/*-no- ......... */
/*-no- ..................................................... */
/*-no- ............................................................................... */
/*-no-  */
/*-no- ........................... */
/*-no-  */
/*-no- .................................... */
/*-no- .. */
/*-no-  */
/*-no- #endif  // CHROME_BROWSER_UI_GTK_INFOBARS_INFOBAR_GTK_H_ */

#endif
/* CONSBENCH file end */
