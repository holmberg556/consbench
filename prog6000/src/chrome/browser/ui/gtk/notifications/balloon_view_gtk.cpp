/* CONSBENCH file begin */
#ifndef BALLOON_VIEW_GTK_CC_4572
#define BALLOON_VIEW_GTK_CC_4572

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_browser_ui_gtk_notifications_balloon_view_gtk_cpp, "chrome/browser/ui/gtk/notifications/balloon_view_gtk.cpp");

/* CONSBENCH includes begin */
#include "chrome/browser/ui/gtk/notifications/balloon_view_gtk.h"
#include <gtk/gtk.h>
#include <string>
#include <vector>
#include "base/message_loop.h"
#include "base/string_util.h"
#include "chrome/browser/browser_list.h"
#include "chrome/browser/browser_window.h"
#include "chrome/browser/extensions/extension_host.h"
#include "chrome/browser/extensions/extension_process_manager.h"
#include "chrome/browser/notifications/balloon.h"
#include "chrome/browser/notifications/desktop_notification_service.h"
#include "chrome/browser/notifications/notification.h"
#include "chrome/browser/notifications/notification_options_menu_model.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/themes/browser_theme_provider.h"
#include "chrome/browser/ui/gtk/custom_button.h"
#include "chrome/browser/ui/gtk/gtk_theme_provider.h"
#include "chrome/browser/ui/gtk/gtk_util.h"
#include "chrome/browser/ui/gtk/info_bubble_gtk.h"
#include "chrome/browser/ui/gtk/menu_gtk.h"
#include "chrome/browser/ui/gtk/notifications/balloon_view_host_gtk.h"
#include "chrome/browser/ui/gtk/rounded_window.h"
#include "chrome/common/extensions/extension.h"
#include "content/browser/renderer_host/render_view_host.h"
#include "content/browser/renderer_host/render_widget_host_view.h"
#include "content/common/notification_details.h"
#include "content/common/notification_service.h"
#include "content/common/notification_source.h"
#include "content/common/notification_type.h"
#include "grit/generated_resources.h"
#include "grit/theme_resources.h"
#include "ui/base/animation/slide_animation.h"
#include "ui/base/l10n/l10n_util.h"
#include "ui/base/resource/resource_bundle.h"
#include "ui/gfx/canvas.h"
#include "ui/gfx/insets.h"
#include "ui/gfx/native_widget_types.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_browser_ui_gtk_notifications_balloon_view_gtk_cpp, "chrome/browser/ui/gtk/notifications/balloon_view_gtk.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(chrome_browser_ui_gtk_notifications_balloon_view_gtk_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include "chrome/browser/ui/gtk/notifications/balloon_view_gtk.h" */
/*-no-  */
/*-no- #include <gtk/gtk.h> */
/*-no-  */
/*-no- #include <string> */
/*-no- #include <vector> */
/*-no-  */
/*-no- #include "base/message_loop.h" */
/*-no- #include "base/string_util.h" */
/*-no- #include "chrome/browser/browser_list.h" */
/*-no- #include "chrome/browser/browser_window.h" */
/*-no- #include "chrome/browser/extensions/extension_host.h" */
/*-no- #include "chrome/browser/extensions/extension_process_manager.h" */
/*-no- #include "chrome/browser/notifications/balloon.h" */
/*-no- #include "chrome/browser/notifications/desktop_notification_service.h" */
/*-no- #include "chrome/browser/notifications/notification.h" */
/*-no- #include "chrome/browser/notifications/notification_options_menu_model.h" */
/*-no- #include "chrome/browser/profiles/profile.h" */
/*-no- #include "chrome/browser/themes/browser_theme_provider.h" */
/*-no- #include "chrome/browser/ui/gtk/custom_button.h" */
/*-no- #include "chrome/browser/ui/gtk/gtk_theme_provider.h" */
/*-no- #include "chrome/browser/ui/gtk/gtk_util.h" */
/*-no- #include "chrome/browser/ui/gtk/info_bubble_gtk.h" */
/*-no- #include "chrome/browser/ui/gtk/menu_gtk.h" */
/*-no- #include "chrome/browser/ui/gtk/notifications/balloon_view_host_gtk.h" */
/*-no- #include "chrome/browser/ui/gtk/rounded_window.h" */
/*-no- #include "chrome/common/extensions/extension.h" */
/*-no- #include "content/browser/renderer_host/render_view_host.h" */
/*-no- #include "content/browser/renderer_host/render_widget_host_view.h" */
/*-no- #include "content/common/notification_details.h" */
/*-no- #include "content/common/notification_service.h" */
/*-no- #include "content/common/notification_source.h" */
/*-no- #include "content/common/notification_type.h" */
/*-no- #include "grit/generated_resources.h" */
/*-no- #include "grit/theme_resources.h" */
/*-no- #include "ui/base/animation/slide_animation.h" */
/*-no- #include "ui/base/l10n/l10n_util.h" */
/*-no- #include "ui/base/resource/resource_bundle.h" */
/*-no- #include "ui/gfx/canvas.h" */
/*-no- #include "ui/gfx/insets.h" */
/*-no- #include "ui/gfx/native_widget_types.h" */
/*-no-  */
/*-no- ........... */
/*-no-  */
/*-no- ..................................................................... */
/*-no- ....................... */
/*-no- ......................... */
/*-no- ............................ */
/*-no- .......................... */
/*-no- ........................... */
/*-no-  */
/*-no- .................................................................... */
/*-no- .................. */
/*-no- ..................................... */
/*-no-  */
/*-no- .................................. */
/*-no- ............................... */
/*-no-  */
/*-no- ............................................. */
/*-no- ............................... */
/*-no- ................................ */
/*-no- .............................. */
/*-no- ................................. */
/*-no-  */
/*-no- ........................................................ */
/*-no- ............................. */
/*-no-  */
/*-no- .................................................................... */
/*-no- ...................................... */
/*-no-  */
/*-no- ................................................................... */
/*-no- ......................................... */
/*-no- ................................... */
/*-no- ................................... */
/*-no-  */
/*-no- ............................................................................ */
/*-no- .......... */
/*-no- ............................ */
/*-no-  */
/*-no- .................................................................. */
/*-no- .................................................... */
/*-no- .................................... */
/*-no- .................................................... */
/*-no- .................................................... */
/*-no- .................................................... */
/*-no- ................................................ */
/*-no- ................................................ */
/*-no- ................................................ */
/*-no-  */
/*-no- .................................................................... */
/*-no- ......................................................................... */
/*-no-  */
/*-no- ............... */
/*-no-  */
/*-no- ............................................................... */
/*-no- ..................... */
/*-no- ............................. */
/*-no- ............................ */
/*-no- ............................ */
/*-no- .......................... */
/*-no- ....................... */
/*-no- ........................... */
/*-no- ............................. */
/*-no- . */
/*-no-  */
/*-no- ..................................... */
/*-no- . */
/*-no-  */
/*-no- ........................................... */
/*-no- ........................................................... */
/*-no- .................................. */
/*-no- .......................... */
/*-no- .......... */
/*-no- ..................................... */
/*-no- .................. */
/*-no- .......................................... */
/*-no- ...................................................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ............................................ */
/*-no- ....................................................................... */
/*-no- ...................... */
/*-no- ................ */
/*-no- ....................... */
/*-no-  */
/*-no- ...................................................................... */
/*-no- ........................................................................ */
/*-no- .................................... */
/*-no- .................................................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................... */
/*-no- .............................. */
/*-no- . */
/*-no-  */
/*-no- .................................................. */
/*-no- ............................. */
/*-no- ......................... */
/*-no- ..................................................................... */
/*-no- ...................................................................... */
/*-no- ...................................... */
/*-no- ... */
/*-no- ............................. */
/*-no- . */
/*-no-  */
/*-no- ............................................. */
/*-no- .......................... */
/*-no- ........................................................................... */
/*-no- ........... */
/*-no- ... */
/*-no-  */
/*-no- ................... */
/*-no-  */
/*-no- ...................................................................... */
/*-no- .............. */
/*-no- .............. */
/*-no- .............. */
/*-no- .............. */
/*-no- ............................................................................ */
/*-no- ........................................................................ */
/*-no-  */
/*-no- .......................................... */
/*-no- .......................................... */
/*-no- ..................................... */
/*-no- ...................................... */
/*-no-  */
/*-no- .................................................................... */
/*-no- .......................................................... */
/*-no- ................................................. */
/*-no- ..................... */
/*-no- . */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ......................................... */
/*-no-  */
/*-no- ..................................................... */
/*-no- ............................................ */
/*-no- ........................... */
/*-no-  */
/*-no- ........................... */
/*-no- ...................................................... */
/*-no- .................................................. */
/*-no- ...................................................... */
/*-no- .................................................. */
/*-no- .......................................................... */
/*-no- ...................................................... */
/*-no- ........................................................... */
/*-no- ........................................................ */
/*-no- ................................................. */
/*-no- ..................................................................... */
/*-no- ............................................... */
/*-no- .......................................................... */
/*-no-  */
/*-no- ................................................... */
/*-no- ......................................................... */
/*-no- . */
/*-no-  */
/*-no- .............................................. */
/*-no- .................................................................. */
/*-no-  */
/*-no- .................................................................. */
/*-no- ............................................ */
/*-no- ................................................ */
/*-no- .................................. */
/*-no- .................................................................... */
/*-no- .................................. */
/*-no- ....................................................................... */
/*-no-  */
/*-no- ..................... */
/*-no- ...................................................... */
/*-no-  */
/*-no- ................................ */
/*-no- ........................................................................ */
/*-no- .................................................................... */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ..................................................... */
/*-no- ......................... */
/*-no- ........................................................... */
/*-no- .................................................. */
/*-no- .................................................................. */
/*-no-  */
/*-no- ..................................................................... */
/*-no- ....................................... */
/*-no- ........................................................... */
/*-no-  */
/*-no- .............................. */
/*-no- ................................................. */
/*-no-  */
/*-no- ............................................................... */
/*-no- ............................ */
/*-no- ............................... */
/*-no- ............................................................ */
/*-no- ................................. */
/*-no- ........................................................ */
/*-no- .................................................... */
/*-no-  */
/*-no- .............................................. */
/*-no- ................................. */
/*-no- ....................................................................... */
/*-no- .................................................. */
/*-no- ............................ */
/*-no-  */
/*-no- .................................................... */
/*-no- .................................................... */
/*-no- ............................................... */
/*-no- ..................................................... */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ............. */
/*-no- ................................................. */
/*-no- ...................................................... */
/*-no- ..................................................... */
/*-no- .................................................................... */
/*-no- ......................................................... */
/*-no- ................. */
/*-no- ......................................................... */
/*-no- ........................................................ */
/*-no- ......................................................................... */
/*-no- ..................................................................... */
/*-no- ........................................................... */
/*-no- ....................................................................... */
/*-no- ................................................. */
/*-no- ................................................................... */
/*-no- ....................................................................... */
/*-no-  */
/*-no- ........................................................... */
/*-no-  */
/*-no- ...................................................................... */
/*-no- ......................................................... */
/*-no- ........................................................... */
/*-no- ........................................................... */
/*-no- ........................................................... */
/*-no- ............................................. */
/*-no- ................................................................ */
/*-no- ...................................................................... */
/*-no- ............................................................................. */
/*-no- ...................................................... */
/*-no- ......................................................... */
/*-no- ................................................................. */
/*-no- ..................................................................... */
/*-no- ........................................................... */
/*-no- ....................................................................... */
/*-no- ............................................... */
/*-no- ............................................................................. */
/*-no- ..................................................................... */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ..................................................................... */
/*-no- ....................................................................... */
/*-no- ....................................................................... */
/*-no- ...................................................... */
/*-no- ............................................................. */
/*-no- .................................................... */
/*-no- ........................................................................ */
/*-no- ............................................................... */
/*-no- ........................................................................ */
/*-no- ....................................................................... */
/*-no- ............................................................. */
/*-no- ....................................................................... */
/*-no- ............................................... */
/*-no- ..................................................... */
/*-no- .................................................... */
/*-no- ....................................................................... */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ................................................................. */
/*-no-  */
/*-no- ................................................................. */
/*-no- ........................................................................ */
/*-no- ..................................................... */
/*-no- ..................................................... */
/*-no-  */
/*-no- ................................................................ */
/*-no- ....................................... */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ................................................ */
/*-no- ............................................................. */
/*-no- ................... */
/*-no- ......................................................................... */
/*-no-  */
/*-no- ................................................................ */
/*-no- .................................. */
/*-no- .............................................................. */
/*-no- ......................................................... */
/*-no- ........................................ */
/*-no-  */
/*-no- ................................... */
/*-no- ............................................................................... */
/*-no- . */
/*-no-  */
/*-no- ................................ */
/*-no- ........................................................................... */
/*-no- ......................................... */
/*-no- ...................................................... */
/*-no- ................................................ */
/*-no- . */
/*-no-  */
/*-no- ....................................................... */
/*-no- ................................................... */
/*-no- ..................................................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................. */
/*-no- ..................................... */
/*-no- ............................. */
/*-no- . */
/*-no-  */
/*-no- ................................................... */
/*-no- ........................................... */
/*-no- ........................................................................ */
/*-no- . */
/*-no-  */
/*-no- .................................................... */
/*-no- ............................................ */
/*-no- ......................................................................... */
/*-no- ....................... */
/*-no- . */
/*-no-  */
/*-no- ......................................................... */
/*-no- ........................ */
/*-no- ....................... */
/*-no-  */
/*-no- .................................................... */
/*-no- .......................................... */
/*-no- ................... */
/*-no- ................................................................ */
/*-no- .................................................. */
/*-no- ................................................................ */
/*-no- . */
/*-no-  */
/*-no- .................................................... */
/*-no- ............................................................... */
/*-no- ................................................................... */
/*-no- .............................................................. */
/*-no- ....................................................................... */
/*-no- ................................................................ */
/*-no- ........................................ */
/*-no- ...................................................... */
/*-no- ................................... */
/*-no- ................. */
/*-no- ............................................................... */
/*-no- ............................................................................. */
/*-no- ..................................................... */
/*-no- ............................................ */
/*-no- .......... */
/*-no- ................. */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ........................................................ */
/*-no- .............. */
/*-no- . */
/*-no-  */
/*-no- ......................................................................... */
/*-no- ........................................................................... */
/*-no- ................................................................ */
/*-no- ............................................................. */
/*-no- ................................................................... */
/*-no- ............................................................... */
/*-no- ........................................ */
/*-no- ................. */
/*-no-  */
/*-no- .......................................................................... */
/*-no- .................................................................... */
/*-no- .................................... */
/*-no- ............................................... */
/*-no- ................................ */
/*-no- .......................................................... */
/*-no- ....................... */
/*-no- ................................................... */
/*-no- ..................................................... */
/*-no- ....................... */
/*-no- ................... */
/*-no- .................... */
/*-no-  */
/*-no- ............... */
/*-no- . */
/*-no-  */
/*-no- .............................................................................. */
/*-no- ............................................................... */
/*-no- ........................................ */
/*-no- ................. */
/*-no-  */
/*-no- .................................................... */
/*-no- .......................................... */
/*-no-  */
/*-no- .............................................. */
/*-no- .................................................. */
/*-no- ....................................................................... */
/*-no- .................................................... */
/*-no- ................................................................ */
/*-no- ................. */
/*-no-  */
/*-no- ......................................................... */
/*-no- ................................................ */
/*-no- ....................................................................... */
/*-no- ................................ */
/*-no- .............................................. */
/*-no- ............................................................... */
/*-no- ................... */
/*-no-  */
/*-no- .................... */
/*-no-  */
/*-no- ............... */
/*-no- . */
/*-no-  */
/*-no- ............................................................ */
/*-no- .................................................................. */
/*-no- ....................... */
/*-no- .................................................................... */
/*-no- . */
/*-no-  */
/*-no- ...................................... */
/*-no- ........................................ */
/*-no- ........................ */
/*-no- ....................... */
/*-no- ..................................... */
/*-no- .................. */
/*-no- .......................................... */
/*-no- .................................................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ........................................................ */
/*-no- .......................... */
/*-no- ............... */
/*-no- .............................. */
/*-no- . */

#endif
/* CONSBENCH file end */
