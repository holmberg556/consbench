/* CONSBENCH file begin */
#ifndef OPTIONS_UI_CC_259
#define OPTIONS_UI_CC_259

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_browser_ui_webui_options_options_ui_cpp, "chrome/browser/ui/webui/options/options_ui.cpp");

/* CONSBENCH includes begin */
#include "chrome/browser/ui/webui/options/options_ui.h"
#include <algorithm>
#include <vector>
#include "base/callback.h"
#include "base/command_line.h"
#include "base/message_loop.h"
#include "base/singleton.h"
#include "base/string_piece.h"
#include "base/string_util.h"
#include "base/threading/thread.h"
#include "base/time.h"
#include "base/values.h"
#include "chrome/browser/browser_about_handler.h"
#include "chrome/browser/metrics/user_metrics.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/browser/ui/webui/options/about_page_handler.h"
#include "chrome/browser/ui/webui/options/add_startup_page_handler.h"
#include "chrome/browser/ui/webui/options/advanced_options_handler.h"
#include "chrome/browser/ui/webui/options/autofill_options_handler.h"
#include "chrome/browser/ui/webui/options/browser_options_handler.h"
#include "chrome/browser/ui/webui/options/clear_browser_data_handler.h"
#include "chrome/browser/ui/webui/options/content_settings_handler.h"
#include "chrome/browser/ui/webui/options/cookies_view_handler.h"
#include "chrome/browser/ui/webui/options/core_options_handler.h"
#include "chrome/browser/ui/webui/options/dom_options_util.h"
#include "chrome/browser/ui/webui/options/font_settings_handler.h"
#include "chrome/browser/ui/webui/options/import_data_handler.h"
#include "chrome/browser/ui/webui/options/language_options_handler.h"
#include "chrome/browser/ui/webui/options/password_manager_handler.h"
#include "chrome/browser/ui/webui/options/personal_options_handler.h"
#include "chrome/browser/ui/webui/options/search_engine_manager_handler.h"
#include "chrome/browser/ui/webui/options/stop_syncing_handler.h"
#include "chrome/browser/ui/webui/theme_source.h"
#include "chrome/common/jstemplate_builder.h"
#include "chrome/common/notification_type.h"
#include "chrome/common/time_format.h"
#include "chrome/common/url_constants.h"
#include "content/browser/browser_thread.h"
#include "content/browser/renderer_host/render_view_host.h"
#include "content/browser/tab_contents/tab_contents.h"
#include "content/browser/tab_contents/tab_contents_delegate.h"
#include "grit/browser_resources.h"
#include "grit/chromium_strings.h"
#include "grit/generated_resources.h"
#include "grit/locale_settings.h"
#include "grit/theme_resources.h"
#include "net/base/escape.h"
#include "ui/base/resource/resource_bundle.h"
#include "chrome/browser/chromeos/webui/accounts_options_handler.h"
#include "chrome/browser/chromeos/webui/core_chromeos_options_handler.h"
#include "chrome/browser/chromeos/webui/cros_language_options_handler.h"
#include "chrome/browser/chromeos/webui/internet_options_handler.h"
#include "chrome/browser/chromeos/webui/language_chewing_options_handler.h"
#include "chrome/browser/chromeos/webui/language_customize_modifier_keys_handler.h"
#include "chrome/browser/chromeos/webui/language_hangul_options_handler.h"
#include "chrome/browser/chromeos/webui/language_mozc_options_handler.h"
#include "chrome/browser/chromeos/webui/language_pinyin_options_handler.h"
#include "chrome/browser/chromeos/webui/proxy_handler.h"
#include "chrome/browser/chromeos/webui/stats_options_handler.h"
#include "chrome/browser/chromeos/webui/system_options_handler.h"
#include "chrome/browser/chromeos/webui/user_image_source.h"
#include "chrome/browser/ui/webui/options/certificate_manager_handler.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_browser_ui_webui_options_options_ui_cpp, "chrome/browser/ui/webui/options/options_ui.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(chrome_browser_ui_webui_options_options_ui_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include "chrome/browser/ui/webui/options/options_ui.h" */
/*-no-  */
/*-no- #include <algorithm> */
/*-no- #include <vector> */
/*-no-  */
/*-no- #include "base/callback.h" */
/*-no- #include "base/command_line.h" */
/*-no- #include "base/message_loop.h" */
/*-no- #include "base/singleton.h" */
/*-no- #include "base/string_piece.h" */
/*-no- #include "base/string_util.h" */
/*-no- #include "base/threading/thread.h" */
/*-no- #include "base/time.h" */
/*-no- #include "base/values.h" */
/*-no- #include "chrome/browser/browser_about_handler.h" */
/*-no- #include "chrome/browser/metrics/user_metrics.h" */
/*-no- #include "chrome/browser/profiles/profile.h" */
/*-no- #include "chrome/browser/ui/webui/options/about_page_handler.h" */
/*-no- #include "chrome/browser/ui/webui/options/add_startup_page_handler.h" */
/*-no- #include "chrome/browser/ui/webui/options/advanced_options_handler.h" */
/*-no- #include "chrome/browser/ui/webui/options/autofill_options_handler.h" */
/*-no- #include "chrome/browser/ui/webui/options/browser_options_handler.h" */
/*-no- #include "chrome/browser/ui/webui/options/clear_browser_data_handler.h" */
/*-no- #include "chrome/browser/ui/webui/options/content_settings_handler.h" */
/*-no- #include "chrome/browser/ui/webui/options/cookies_view_handler.h" */
/*-no- #include "chrome/browser/ui/webui/options/core_options_handler.h" */
/*-no- #include "chrome/browser/ui/webui/options/dom_options_util.h" */
/*-no- #include "chrome/browser/ui/webui/options/font_settings_handler.h" */
/*-no- #include "chrome/browser/ui/webui/options/import_data_handler.h" */
/*-no- #include "chrome/browser/ui/webui/options/language_options_handler.h" */
/*-no- #include "chrome/browser/ui/webui/options/password_manager_handler.h" */
/*-no- #include "chrome/browser/ui/webui/options/personal_options_handler.h" */
/*-no- #include "chrome/browser/ui/webui/options/search_engine_manager_handler.h" */
/*-no- #include "chrome/browser/ui/webui/options/stop_syncing_handler.h" */
/*-no- #include "chrome/browser/ui/webui/theme_source.h" */
/*-no- #include "chrome/common/jstemplate_builder.h" */
/*-no- #include "chrome/common/notification_type.h" */
/*-no- #include "chrome/common/time_format.h" */
/*-no- #include "chrome/common/url_constants.h" */
/*-no- #include "content/browser/browser_thread.h" */
/*-no- #include "content/browser/renderer_host/render_view_host.h" */
/*-no- #include "content/browser/tab_contents/tab_contents.h" */
/*-no- #include "content/browser/tab_contents/tab_contents_delegate.h" */
/*-no- #include "grit/browser_resources.h" */
/*-no- #include "grit/chromium_strings.h" */
/*-no- #include "grit/generated_resources.h" */
/*-no- #include "grit/locale_settings.h" */
/*-no- #include "grit/theme_resources.h" */
/*-no- #include "net/base/escape.h" */
/*-no- #include "ui/base/resource/resource_bundle.h" */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- #include "chrome/browser/chromeos/webui/accounts_options_handler.h" */
/*-no- #include "chrome/browser/chromeos/webui/core_chromeos_options_handler.h" */
/*-no- #include "chrome/browser/chromeos/webui/cros_language_options_handler.h" */
/*-no- #include "chrome/browser/chromeos/webui/internet_options_handler.h" */
/*-no- #include "chrome/browser/chromeos/webui/language_chewing_options_handler.h" */
/*-no- #include "chrome/browser/chromeos/webui/language_customize_modifier_keys_handler.h" */
/*-no- #include "chrome/browser/chromeos/webui/language_hangul_options_handler.h" */
/*-no- #include "chrome/browser/chromeos/webui/language_mozc_options_handler.h" */
/*-no- #include "chrome/browser/chromeos/webui/language_pinyin_options_handler.h" */
/*-no- #include "chrome/browser/chromeos/webui/proxy_handler.h" */
/*-no- #include "chrome/browser/chromeos/webui/stats_options_handler.h" */
/*-no- #include "chrome/browser/chromeos/webui/system_options_handler.h" */
/*-no- #include "chrome/browser/chromeos/webui/user_image_source.h" */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(USE_NSS) */
/*-no- #include "chrome/browser/ui/webui/options/certificate_manager_handler.h" */
/*-no- #endif */
/*-no-  */
/*-no- ................................................................................ */
/*-no- .. */
/*-no- ...................... */
/*-no- .. */
/*-no- ................................................................................ */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ......................................................................... */
/*-no- ............................ */
/*-no- .............................................. */
/*-no- . */
/*-no-  */
/*-no- .............................................. */
/*-no-  */
/*-no- ................................................................... */
/*-no- .................................................................. */
/*-no- ............................................................ */
/*-no- .................................................... */
/*-no-  */
/*-no- .............................................. */
/*-no- ............................................................. */
/*-no- ............................. */
/*-no- ........................................................................ */
/*-no- .............................................. */
/*-no-  */
/*-no- ................................................................. */
/*-no- ............................................ */
/*-no- .......................................................................... */
/*-no-  */
/*-no- ....................................... */
/*-no- . */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ..................... */
/*-no- . */
/*-no-  */
/*-no- ................................................................................ */
/*-no- .. */
/*-no- ....................... */
/*-no- .. */
/*-no- ................................................................................ */
/*-no-  */
/*-no- .............................................. */
/*-no- . */
/*-no-  */
/*-no- ............................................... */
/*-no- . */
/*-no-  */
/*-no- ........................................ */
/*-no- .............. */
/*-no- . */
/*-no-  */
/*-no- ................................................... */
/*-no- ...................................... */
/*-no- ........................................................... */
/*-no- . */
/*-no-  */
/*-no- ......... */
/*-no- ........................................... */
/*-no- ....................................... */
/*-no- ........................................... */
/*-no- .................... */
/*-no- ....................................... */
/*-no- ....................................................................... */
/*-no- ................................. */
/*-no- .......................... */
/*-no- ................................................................................ */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ............................................................................ */
/*-no- .......................................................................... */
/*-no- ........................................................ */
/*-no- ............................................. */
/*-no- ........................................... */
/*-no- .......................................................... */
/*-no- ....................................................... */
/*-no- ........................................................ */
/*-no- ................................................ */
/*-no- . */
/*-no-  */
/*-no- ................................................................................ */
/*-no- .. */
/*-no- ............ */
/*-no- .. */
/*-no- ................................................................................ */
/*-no-  */
/*-no- ........................................... */
/*-no- ..................................................... */
/*-no- ............................................................. */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ............................................ */
/*-no- ...................................................................... */
/*-no- #else */
/*-no- ....................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- .......................................................................... */
/*-no- ........................................................................... */
/*-no- ........................................................................... */
/*-no- .......................................................................... */
/*-no- ............................................................................ */
/*-no- ........................................................................... */
/*-no- ....................................................................... */
/*-no- ........................................................................ */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ............................................ */
/*-no- ...................................................................... */
/*-no- #else */
/*-no- ........................................................................... */
/*-no- #endif */
/*-no- ........................................................................... */
/*-no- ........................................................................... */
/*-no- ............................................................................... */
/*-no- ...................................................................... */
/*-no- ....................................................................... */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ..................................................................... */
/*-no- ............................................ */
/*-no- .................................................................. */
/*-no- ........................................................................... */
/*-no- ............................................ */
/*-no- ......................................................................... */
/*-no- ............................................ */
/*-no- ................................................................................ */
/*-no- ............................................ */
/*-no- ........................................................................ */
/*-no- ............................................ */
/*-no- ...................................................................... */
/*-no- ............................................ */
/*-no- ........................................................................ */
/*-no- ........................................................................... */
/*-no- ............................................ */
/*-no- ............................................................... */
/*-no- ......................................................................... */
/*-no- #endif */
/*-no- #if defined(USE_NSS) */
/*-no- .............................................................................. */
/*-no- #endif */
/*-no-  */
/*-no- ..................................................................... */
/*-no- .................................... */
/*-no- ................................................. */
/*-no-  */
/*-no- .......................................... */
/*-no- ............................................................................. */
/*-no-  */
/*-no- ....................................... */
/*-no- ............................................................ */
/*-no- ....................................................................... */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ........................................... */
/*-no- ................................................ */
/*-no- ...................................... */
/*-no- ................................................................ */
/*-no- ......................... */
/*-no- #endif */
/*-no-  */
/*-no- .............................................................................. */
/*-no- .......... */
/*-no- ................................................. */
/*-no- . */
/*-no-  */
/*-no- ......................... */
/*-no- ............................................................................... */
/*-no- ........................................................... */
/*-no- ................................................................................ */
/*-no- ............................... */
/*-no- ................ */
/*-no- .............................................................. */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ............ */
/*-no- ..................................................................... */
/*-no- .................................. */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- .............................................................................. */
/*-no- .......................................... */
/*-no- #else */
/*-no- ................................................................................ */
/*-no- #endif */
/*-no-  */
/*-no- ............................................................................... */
/*-no- ............................................. */
/*-no- . */
/*-no-  */
/*-no- ...................................................... */
/*-no- ........................................................................... */
/*-no- ....................................................................... */
/*-no- ................................................................................ */
/*-no- ............................................................................. */
/*-no- ................................................................................ */
/*-no- .................. */
/*-no- .......................................................... */
/*-no-  */
/*-no- .............................................. */
/*-no- . */
/*-no-  */
/*-no- ......... */
/*-no- ........................................................ */
/*-no- ............................................. */
/*-no- .................................................. */
/*-no- . */
/*-no-  */
/*-no- ...................................... */
/*-no- ....................................................................... */
/*-no-  */
/*-no- ............................................................................... */
/*-no- .......................................................................... */
/*-no- ............................................................................. */
/*-no- .......................................... */
/*-no- ............................ */
/*-no- ........... */
/*-no- ............................... */
/*-no-  */
/*-no- ................................................... */
/*-no- ................................... */
/*-no- ....................................................................... */
/*-no- .............................................................. */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ............................................................................ */
/*-no- ........................................................ */
/*-no- ........................ */
/*-no- .............................................. */
/*-no- ............................. */
/*-no- ................................................... */
/*-no- ........................................................... */
/*-no- ....................................................... */
/*-no- ... */
/*-no- . */

#endif
/* CONSBENCH file end */
