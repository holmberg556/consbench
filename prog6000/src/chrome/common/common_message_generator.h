/* CONSBENCH file begin */
#ifndef COMMON_MESSAGE_GENERATOR_H_403
#define COMMON_MESSAGE_GENERATOR_H_403

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_common_common_message_generator_h, "chrome/common/common_message_generator.h");

/* CONSBENCH includes begin */
#include "chrome/common/database_messages.h"
#include "chrome/common/file_utilities_messages.h"
#include "chrome/common/indexed_db_messages.h"
#include "chrome/common/mime_registry_messages.h"
#include "chrome/common/safebrowsing_messages.h"
#include "chrome/common/speech_input_messages.h"
#include "chrome/common/utility_messages.h"
#include "chrome/common/worker_messages.h"
#include "chrome/common/autofill_messages.h"
#include "chrome/common/automation_messages.h"
#include "chrome/common/devtools_messages.h"
#include "chrome/common/dom_storage_messages.h"
#include "chrome/common/gpu_messages.h"
#include "chrome/common/nacl_messages.h"
#include "chrome/common/pepper_file_messages.h"
#include "chrome/common/pepper_messages.h"
#include "chrome/common/plugin_messages.h"
#include "chrome/common/render_messages.h"
#include "chrome/common/service_messages.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_common_common_message_generator_h, "chrome/common/common_message_generator.h");

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- .................................................. */
/*-no-  */
/*-no- #include "chrome/common/database_messages.h" */
/*-no- #include "chrome/common/file_utilities_messages.h" */
/*-no- #include "chrome/common/indexed_db_messages.h" */
/*-no- #include "chrome/common/mime_registry_messages.h" */
/*-no- #include "chrome/common/safebrowsing_messages.h" */
/*-no- #include "chrome/common/speech_input_messages.h" */
/*-no- #include "chrome/common/utility_messages.h" */
/*-no- #include "chrome/common/worker_messages.h" */
/*-no-  */
/*-no- #if 0  // This feature is not yet enabled for these files. */
/*-no-  */
/*-no- #include "chrome/common/autofill_messages.h" */
/*-no- #include "chrome/common/automation_messages.h" */
/*-no- #include "chrome/common/devtools_messages.h" */
/*-no- #include "chrome/common/dom_storage_messages.h" */
/*-no- #include "chrome/common/gpu_messages.h" */
/*-no- #include "chrome/common/nacl_messages.h" */
/*-no- #include "chrome/common/pepper_file_messages.h" */
/*-no- #include "chrome/common/pepper_messages.h" */
/*-no- #include "chrome/common/plugin_messages.h" */
/*-no- #include "chrome/common/render_messages.h" */
/*-no- #include "chrome/common/service_messages.h" */
/*-no-  */
/*-no- #endif */

#endif
/* CONSBENCH file end */
