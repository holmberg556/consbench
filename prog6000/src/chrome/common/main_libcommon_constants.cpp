/* CONSBENCH file begin */
#ifndef MAIN_LIBCOMMON_CONSTANTS_CPP
#define MAIN_LIBCOMMON_CONSTANTS_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_common_main_libcommon_constants_cpp, "chrome/common/main_libcommon_constants.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_common_main_libcommon_constants_cpp, "chrome/common/main_libcommon_constants.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_chrome_common_chrome_constants_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_chrome_paths_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_chrome_paths_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_chrome_switches_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_env_vars_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_json_value_serializer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_net_gaia_gaia_constants_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_pref_names_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_target_geni_chrome_version_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_common_main_libcommon_constants_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_chrome_common_chrome_constants_cpp(it);
  PUBLIC_chrome_common_chrome_paths_cpp(it);
  PUBLIC_chrome_common_chrome_paths_linux_cpp(it);
  PUBLIC_chrome_common_chrome_switches_cpp(it);
  PUBLIC_chrome_common_env_vars_cpp(it);
  PUBLIC_chrome_common_json_value_serializer_cpp(it);
  PUBLIC_chrome_common_net_gaia_gaia_constants_cpp(it);
  PUBLIC_chrome_common_pref_names_cpp(it);
  PUBLIC_out_Debug_obj_target_geni_chrome_version_cpp(it);
}

#endif

