/* CONSBENCH file begin */
#ifndef MAIN_LIBCOMMON_NET_CPP
#define MAIN_LIBCOMMON_NET_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_common_net_main_libcommon_net_cpp, "chrome/common/net/main_libcommon_net.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_common_net_main_libcommon_net_cpp, "chrome/common/net/main_libcommon_net.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_chrome_common_net_net_resource_provider_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_net_raw_host_resolver_proc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_net_url_fetcher_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_net_url_request_context_getter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_net_url_request_intercept_job_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_net_gaia_gaia_auth_consumer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_net_gaia_gaia_auth_fetcher_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_net_gaia_gaia_authenticator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_net_gaia_google_service_auth_error_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_net_x509_certificate_model_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_common_net_x509_certificate_model_nss_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_common_net_main_libcommon_net_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_chrome_common_net_net_resource_provider_cpp(it);
  PUBLIC_chrome_common_net_raw_host_resolver_proc_cpp(it);
  PUBLIC_chrome_common_net_url_fetcher_cpp(it);
  PUBLIC_chrome_common_net_url_request_context_getter_cpp(it);
  PUBLIC_chrome_common_net_url_request_intercept_job_cpp(it);
  PUBLIC_chrome_common_net_gaia_gaia_auth_consumer_cpp(it);
  PUBLIC_chrome_common_net_gaia_gaia_auth_fetcher_cpp(it);
  PUBLIC_chrome_common_net_gaia_gaia_authenticator_cpp(it);
  PUBLIC_chrome_common_net_gaia_google_service_auth_error_cpp(it);
  PUBLIC_chrome_common_net_x509_certificate_model_cpp(it);
  PUBLIC_chrome_common_net_x509_certificate_model_nss_cpp(it);
}

#endif

