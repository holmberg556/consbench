/* CONSBENCH file begin */
#ifndef MAIN_LIBDEFAULT_PLUGIN_CPP
#define MAIN_LIBDEFAULT_PLUGIN_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_default_plugin_main_libdefault_plugin_cpp, "chrome/default_plugin/main_libdefault_plugin.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_default_plugin_main_libdefault_plugin_cpp, "chrome/default_plugin/main_libdefault_plugin.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_chrome_default_plugin_plugin_impl_gtk_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_default_plugin_plugin_main_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_default_plugin_main_libdefault_plugin_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_chrome_default_plugin_plugin_impl_gtk_cpp(it);
  PUBLIC_chrome_default_plugin_plugin_main_cpp(it);
}

#endif

