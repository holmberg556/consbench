/* CONSBENCH file begin */
#ifndef GPU_MAIN_CC_772
#define GPU_MAIN_CC_772

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_gpu_gpu_main_cpp, "chrome/gpu/gpu_main.cpp");

/* CONSBENCH includes begin */
#include <stdlib.h>
#include "app/win/scoped_com_initializer.h"
#include "base/environment.h"
#include "base/message_loop.h"
#include "base/stringprintf.h"
#include "base/threading/platform_thread.h"
#include "build/build_config.h"
#include "chrome/common/chrome_constants.h"
#include "chrome/common/chrome_switches.h"
#include "chrome/common/env_vars.h"
#include "chrome/common/main_function_params.h"
#include "chrome/gpu/gpu_config.h"
#include "chrome/gpu/gpu_process.h"
#include "chrome/gpu/gpu_thread.h"
#include "chrome/app/breakpad_linux.h"
#include "chrome/common/chrome_application_mac.h"
#include "ui/gfx/gtk_util.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_gpu_gpu_main_cpp, "chrome/gpu/gpu_main.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(chrome_gpu_gpu_main_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include <stdlib.h> */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- #include <windows.h> */
/*-no- #endif */
/*-no-  */
/*-no- #include "app/win/scoped_com_initializer.h" */
/*-no- #include "base/environment.h" */
/*-no- #include "base/message_loop.h" */
/*-no- #include "base/stringprintf.h" */
/*-no- #include "base/threading/platform_thread.h" */
/*-no- #include "build/build_config.h" */
/*-no- #include "chrome/common/chrome_constants.h" */
/*-no- #include "chrome/common/chrome_switches.h" */
/*-no- #include "chrome/common/env_vars.h" */
/*-no- #include "chrome/common/main_function_params.h" */
/*-no- #include "chrome/gpu/gpu_config.h" */
/*-no- #include "chrome/gpu/gpu_process.h" */
/*-no- #include "chrome/gpu/gpu_thread.h" */
/*-no-  */
/*-no- #if defined(USE_LINUX_BREAKPAD) */
/*-no- #include "chrome/app/breakpad_linux.h" */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(OS_MACOSX) */
/*-no- #include "chrome/common/chrome_application_mac.h" */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(USE_X11) */
/*-no- #include "ui/gfx/gtk_util.h" */
/*-no- #endif */
/*-no-  */
/*-no- .............................................. */
/*-no- ................................................... */
/*-no- ............................................ */
/*-no-  */
/*-no- #if defined(USE_LINUX_BREAKPAD) */
/*-no- ............................................................ */
/*-no- ...................... */
/*-no- #endif */
/*-no-  */
/*-no- ............................................................. */
/*-no- ............................................................ */
/*-no- .......................................... */
/*-no- ... */
/*-no-  */
/*-no- #if defined(OS_MACOSX) */
/*-no- .......................................... */
/*-no- #endif */
/*-no-  */
/*-no- ...................................................... */
/*-no- ............................................. */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- .............................................................................. */
/*-no- ........................ */
/*-no- ............... */
/*-no- ............................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- ................................................. */
/*-no-  */
/*-no- #if defined(USE_X11) */
/*-no- ........................................................................ */
/*-no- ......................................................................... */
/*-no- ................................................................... */
/*-no- ....................................................................... */
/*-no- ........................................................................ */
/*-no- ...................... */
/*-no- ............................................ */
/*-no- #endif */
/*-no-  */
/*-no- .................................................................. */
/*-no- ...................................................................... */
/*-no- ................................................................. */
/*-no- ..................................................................... */
/*-no- ................................................................... */
/*-no- ..................................................................... */
/*-no- .................................................... */
/*-no- ......................... */
/*-no-  */
/*-no- ......................... */
/*-no- #if defined(OS_WIN) */
/*-no- ............................................................... */
/*-no- #else */
/*-no- .................... */
/*-no- #endif */
/*-no-  */
/*-no- ............................... */
/*-no-  */
/*-no- .......................................... */
/*-no-  */
/*-no- .......................... */
/*-no-  */
/*-no- ............................. */
/*-no-  */
/*-no- ........... */
/*-no- . */

#endif
/* CONSBENCH file end */
