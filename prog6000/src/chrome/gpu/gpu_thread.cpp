/* CONSBENCH file begin */
#ifndef GPU_THREAD_CC_775
#define GPU_THREAD_CC_775

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_gpu_gpu_thread_cpp, "chrome/gpu/gpu_thread.cpp");

/* CONSBENCH includes begin */
#include "chrome/gpu/gpu_thread.h"
#include <string>
#include <vector>
#include "app/gfx/gl/gl_context.h"
#include "app/gfx/gl/gl_implementation.h"
#include "app/win/scoped_com_initializer.h"
#include "base/command_line.h"
#include "base/threading/worker_pool.h"
#include "build/build_config.h"
#include "chrome/common/child_process_logging.h"
#include "chrome/common/chrome_switches.h"
#include "chrome/common/gpu_messages.h"
#include "chrome/gpu/gpu_info_collector.h"
#include "chrome/gpu/gpu_watchdog_thread.h"
#include "content/common/child_process.h"
#include "ipc/ipc_channel_handle.h"
#include "chrome/common/sandbox_init_wrapper.h"
#include "chrome/common/sandbox_mac.h"
#include "sandbox/src/sandbox.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_gpu_gpu_thread_cpp, "chrome/gpu/gpu_thread.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(chrome_gpu_gpu_thread_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include "chrome/gpu/gpu_thread.h" */
/*-no-  */
/*-no- #include <string> */
/*-no- #include <vector> */
/*-no-  */
/*-no- #include "app/gfx/gl/gl_context.h" */
/*-no- #include "app/gfx/gl/gl_implementation.h" */
/*-no- #include "app/win/scoped_com_initializer.h" */
/*-no- #include "base/command_line.h" */
/*-no- #include "base/threading/worker_pool.h" */
/*-no- #include "build/build_config.h" */
/*-no- #include "chrome/common/child_process_logging.h" */
/*-no- #include "chrome/common/chrome_switches.h" */
/*-no- #include "chrome/common/gpu_messages.h" */
/*-no- #include "chrome/gpu/gpu_info_collector.h" */
/*-no- #include "chrome/gpu/gpu_watchdog_thread.h" */
/*-no- #include "content/common/child_process.h" */
/*-no- #include "ipc/ipc_channel_handle.h" */
/*-no-  */
/*-no- #if defined(OS_MACOSX) */
/*-no- #include "chrome/common/sandbox_init_wrapper.h" */
/*-no- #include "chrome/common/sandbox_mac.h" */
/*-no- #elif defined(OS_WIN) */
/*-no- #include "sandbox/src/sandbox.h" */
/*-no- #endif */
/*-no-  */
/*-no- .............................. */
/*-no-  */
/*-no- ........... */
/*-no-  */
/*-no- ............................. */
/*-no- #if defined(OS_MACOSX) */
/*-no- ...................................................................... */
/*-no- ..................................... */
/*-no- ................................................................ */
/*-no- .................................................................. */
/*-no- #else */
/*-no- .............................................. */
/*-no- .............. */
/*-no- #endif */
/*-no- . */
/*-no-  */
/*-no- ............... */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- .............................................................. */
/*-no- ......................................... */
/*-no- . */
/*-no- #else */
/*-no- ........................ */
/*-no- . */
/*-no- #endif */
/*-no-  */
/*-no- ................................................... */
/*-no- ............................... */
/*-no- #if defined(OS_WIN) */
/*-no- .......................... */
/*-no- #endif */
/*-no- . */
/*-no-  */
/*-no-  */
/*-no- ......................... */
/*-no- ...................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................ */
/*-no- ........................................... */
/*-no- . */
/*-no-  */
/*-no- ................................................ */
/*-no- ................................... */
/*-no- . */
/*-no-  */
/*-no- ................................................................... */
/*-no- ........................ */
/*-no- ...................... */
/*-no- ..................................................... */
/*-no- ........................................................ */
/*-no- .................................................................... */
/*-no- ............................................................ */
/*-no- ....................................................... */
/*-no- ................................................... */
/*-no- .......................................................... */
/*-no- .......................................................................... */
/*-no- #if defined(OS_MACOSX) */
/*-no- ................................................................... */
/*-no- .............................................................. */
/*-no- ............................................................ */
/*-no- ....................................................... */
/*-no- #endif */
/*-no- .............................................. */
/*-no- ............................................ */
/*-no- .......................................... */
/*-no- .......................... */
/*-no- ................. */
/*-no- . */
/*-no-  */
/*-no- ........... */
/*-no-  */
/*-no- .............................................. */
/*-no- ............................................................ */
/*-no- ...................................................... */
/*-no- .......................................................... */
/*-no- .................................................... */
/*-no- .................................................. */
/*-no- ............................... */
/*-no- .............................................................. */
/*-no- ............... */
/*-no- . */
/*-no-  */
/*-no- ............... */
/*-no-  */
/*-no- ................................ */
/*-no- ................................................ */
/*-no- .................................................................... */
/*-no- ................................ */
/*-no- ...................... */
/*-no- ............................................................... */
/*-no-  */
/*-no- ............................................................................... */
/*-no- ............................................................................ */
/*-no- ............................... */
/*-no- ............................................ */
/*-no- ...................................................... */
/*-no- ................................... */
/*-no- ........... */
/*-no- ... */
/*-no- ............................................................................. */
/*-no- ......................... */
/*-no- ....................................... */
/*-no- ..................................................................... */
/*-no- ... */
/*-no- ............................................... */
/*-no- .................................................................. */
/*-no-  */
/*-no- .............................................................................. */
/*-no- ....................................... */
/*-no- ........................................................................... */
/*-no-  */
/*-no- #if defined (OS_MACOSX) */
/*-no- ............................................................ */
/*-no- .................................................................... */
/*-no- ............................... */
/*-no- ........................ */
/*-no- .................................. */
/*-no- ........................................................... */
/*-no- ..................................... */
/*-no- ............. */
/*-no- ..... */
/*-no- .......... */
/*-no- ................................................ */
/*-no- ... */
/*-no- #elif defined(OS_WIN) */
/*-no- ........................................................................... */
/*-no- .......................................................................... */
/*-no- ............. */
/*-no- ....................... */
/*-no- ................................... */
/*-no- #endif */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ........................................................................... */
/*-no- ................................................................................ */
/*-no- ................................ */
/*-no- ........................ */
/*-no- ................................................... */
/*-no- ........................................... */
/*-no- ..................................................................... */
/*-no- ........................... */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ............................................................................. */
/*-no- #ifndef NDEBUG */
/*-no- .......................... */
/*-no- #endif */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ........................................................................... */
/*-no- ............................................................................ */
/*-no- ........................................................................ */
/*-no- ............................................... */
/*-no- #if defined(OS_WIN) */
/*-no- .......................... */
/*-no- #endif */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ............................................................................ */
/*-no- ........................ */
/*-no- .......................................................... */
/*-no- .............................. */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ................................ */
/*-no- ............................... */
/*-no- ............................. */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ..................................................... */
/*-no- .................................... */
/*-no- .................................... */
/*-no- ................... */
/*-no-  */
/*-no- ....................................................................... */
/*-no- .................................. */
/*-no- ................................................ */
/*-no- ...... */
/*-no- ........................... */
/*-no-  */
/*-no- .......................... */
/*-no-  */
/*-no- ...................... */
/*-no- ......................................... */
/*-no- ...... */
/*-no- ................... */
/*-no-  */
/*-no- ...................... */
/*-no- .................................................... */
/*-no- #if defined(OS_POSIX) */
/*-no- ......................................................................... */
/*-no- .................................................. */
/*-no- ........................................................... */
/*-no- ..................................................................... */
/*-no- #endif */
/*-no- ... */
/*-no-  */
/*-no- ..................................................................... */
/*-no- . */
/*-no-  */
/*-no- .......................................................................... */
/*-no- ............................................................ */
/*-no- ............................................. */
/*-no- ................................................................ */
/*-no- ................................ */
/*-no- ............. */
/*-no- ..... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ................................. */
/*-no- .......................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................. */
/*-no- #if defined(OS_WIN) */
/*-no- .............................................................................. */
/*-no- ............................................................ */
/*-no- ............................................. */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ......................... */
/*-no- .................................... */
/*-no- .................. */
/*-no- .................................................................... */
/*-no- ................ */
/*-no-  */
/*-no- ....................................................................... */
/*-no- ................... */
/*-no- ............................................. */
/*-no- ............ */
/*-no- ........................................................................... */
/*-no- ............. */
/*-no- ..... */
/*-no- ... */
/*-no- #endif */
/*-no- ........................................................ */
/*-no- . */
/*-no-  */
/*-no- .......................................... */
/*-no- ................................... */
/*-no- ......................... */
/*-no- ...................... */
/*-no- ...................................................... */
/*-no- .................................... */
/*-no-  */
/*-no- ....................................................................... */
/*-no- .................................... */
/*-no- .......................................... */
/*-no- ........................................................ */
/*-no- ... */
/*-no-  */
/*-no- ...................................................... */
/*-no- . */
/*-no-  */
/*-no- #if defined(OS_MACOSX) */
/*-no- ...................................................... */
/*-no- ................................................................. */
/*-no- ....................................................................... */
/*-no- .................................. */
/*-no- ........... */
/*-no- ................................................... */
/*-no- .......................................................................... */
/*-no- . */
/*-no- ............................................... */
/*-no- ............................................... */
/*-no- ....................................................................... */
/*-no- .................................. */
/*-no- ........... */
/*-no- ................................................... */
/*-no- ................................................ */
/*-no- . */
/*-no- #endif */
/*-no-  */
/*-no- ........................... */
/*-no- ........................................... */
/*-no- ........................... */
/*-no- ............................................................... */
/*-no- .................................................... */
/*-no- . */
/*-no-  */
/*-no- .......................... */
/*-no- .......................................... */
/*-no- ............ */
/*-no- .......................................................................... */
/*-no- ............................................................................. */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no-  */
/*-no- ............................................................................... */
/*-no- ................................................. */
/*-no- ......................................................... */
/*-no- ................................................. */
/*-no-  */
/*-no- .................. */
/*-no- .............................................. */
/*-no-  */
/*-no- ................................... */
/*-no- ................ */
/*-no- ....................................................................... */
/*-no- . */
/*-no-  */
/*-no- .......................... */
/*-no- ............................................................................. */
/*-no- ........................................... */
/*-no- ................................................. */
/*-no- ........................................................................ */
/*-no- . */
/*-no-  */
/*-no- #endif */

#endif
/* CONSBENCH file end */
