/* CONSBENCH file begin */
#ifndef MAIN_LIBCHROME_GPU_CPP
#define MAIN_LIBCHROME_GPU_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_gpu_main_libchrome_gpu_cpp, "chrome/gpu/main_libchrome_gpu.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_gpu_main_libchrome_gpu_cpp, "chrome/gpu/main_libchrome_gpu.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_chrome_gpu_gpu_channel_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_gpu_gpu_command_buffer_stub_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_gpu_gpu_info_collector_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_gpu_gpu_info_collector_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_gpu_gpu_main_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_gpu_gpu_process_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_gpu_gpu_thread_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_gpu_gpu_video_decoder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_gpu_gpu_video_service_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_gpu_gpu_watchdog_thread_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_gpu_media_fake_gl_video_decode_engine_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_gpu_media_fake_gl_video_device_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_gpu_x_util_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_gpu_main_libchrome_gpu_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_chrome_gpu_gpu_channel_cpp(it);
  PUBLIC_chrome_gpu_gpu_command_buffer_stub_cpp(it);
  PUBLIC_chrome_gpu_gpu_info_collector_linux_cpp(it);
  PUBLIC_chrome_gpu_gpu_info_collector_cpp(it);
  PUBLIC_chrome_gpu_gpu_main_cpp(it);
  PUBLIC_chrome_gpu_gpu_process_cpp(it);
  PUBLIC_chrome_gpu_gpu_thread_cpp(it);
  PUBLIC_chrome_gpu_gpu_video_decoder_cpp(it);
  PUBLIC_chrome_gpu_gpu_video_service_cpp(it);
  PUBLIC_chrome_gpu_gpu_watchdog_thread_cpp(it);
  PUBLIC_chrome_gpu_media_fake_gl_video_decode_engine_cpp(it);
  PUBLIC_chrome_gpu_media_fake_gl_video_device_cpp(it);
  PUBLIC_chrome_gpu_x_util_cpp(it);
}

#endif

