/* CONSBENCH file begin */
#ifndef MAIN_LIBINSTALLER_UTIL_CPP
#define MAIN_LIBINSTALLER_UTIL_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_installer_util_main_libinstaller_util_cpp, "chrome/installer/util/main_libinstaller_util.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_installer_util_main_libinstaller_util_cpp, "chrome/installer/util/main_libinstaller_util.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_chrome_installer_util_master_preferences_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_installer_util_master_preferences_constants_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_installer_util_main_libinstaller_util_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_chrome_installer_util_master_preferences_cpp(it);
  PUBLIC_chrome_installer_util_master_preferences_constants_cpp(it);
}

#endif

