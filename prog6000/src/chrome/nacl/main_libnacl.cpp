/* CONSBENCH file begin */
#ifndef MAIN_LIBNACL_CPP
#define MAIN_LIBNACL_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_nacl_main_libnacl_cpp, "chrome/nacl/main_libnacl.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_nacl_main_libnacl_cpp, "chrome/nacl/main_libnacl.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_chrome_nacl_nacl_main_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_nacl_nacl_main_platform_delegate_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_nacl_nacl_thread_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_nacl_main_libnacl_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_chrome_nacl_nacl_main_cpp(it);
  PUBLIC_chrome_nacl_nacl_main_platform_delegate_linux_cpp(it);
  PUBLIC_chrome_nacl_nacl_thread_cpp(it);
}

#endif

