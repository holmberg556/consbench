/* CONSBENCH file begin */
#ifndef MAIN_LIBPLUGIN_CPP
#define MAIN_LIBPLUGIN_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_plugin_main_libplugin_cpp, "chrome/plugin/main_libplugin.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_plugin_main_libplugin_cpp, "chrome/plugin/main_libplugin.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_chrome_plugin_chrome_plugin_host_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_plugin_npobject_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_plugin_npobject_stub_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_plugin_npobject_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_plugin_plugin_channel_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_plugin_plugin_channel_base_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_plugin_plugin_main_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_plugin_plugin_main_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_plugin_plugin_thread_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_plugin_webplugin_delegate_stub_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_plugin_webplugin_proxy_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_plugin_main_libplugin_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_chrome_plugin_chrome_plugin_host_cpp(it);
  PUBLIC_chrome_plugin_npobject_proxy_cpp(it);
  PUBLIC_chrome_plugin_npobject_stub_cpp(it);
  PUBLIC_chrome_plugin_npobject_util_cpp(it);
  PUBLIC_chrome_plugin_plugin_channel_cpp(it);
  PUBLIC_chrome_plugin_plugin_channel_base_cpp(it);
  PUBLIC_chrome_plugin_plugin_main_cpp(it);
  PUBLIC_chrome_plugin_plugin_main_linux_cpp(it);
  PUBLIC_chrome_plugin_plugin_thread_cpp(it);
  PUBLIC_chrome_plugin_webplugin_delegate_stub_cpp(it);
  PUBLIC_chrome_plugin_webplugin_proxy_cpp(it);
}

#endif

