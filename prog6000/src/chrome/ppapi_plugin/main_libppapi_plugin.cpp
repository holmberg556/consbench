/* CONSBENCH file begin */
#ifndef MAIN_LIBPPAPI_PLUGIN_CPP
#define MAIN_LIBPPAPI_PLUGIN_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_ppapi_plugin_main_libppapi_plugin_cpp, "chrome/ppapi_plugin/main_libppapi_plugin.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_ppapi_plugin_main_libppapi_plugin_cpp, "chrome/ppapi_plugin/main_libppapi_plugin.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_chrome_ppapi_plugin_plugin_process_dispatcher_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_ppapi_plugin_ppapi_plugin_main_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_ppapi_plugin_ppapi_process_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_ppapi_plugin_ppapi_thread_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_ppapi_plugin_main_libppapi_plugin_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_chrome_ppapi_plugin_plugin_process_dispatcher_cpp(it);
  PUBLIC_chrome_ppapi_plugin_ppapi_plugin_main_cpp(it);
  PUBLIC_chrome_ppapi_plugin_ppapi_process_cpp(it);
  PUBLIC_chrome_ppapi_plugin_ppapi_thread_cpp(it);
}

#endif

