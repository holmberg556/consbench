/* CONSBENCH file begin */
#ifndef MAIN_LIBPROFILE_IMPORT_CPP
#define MAIN_LIBPROFILE_IMPORT_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_profile_import_main_libprofile_import_cpp, "chrome/profile_import/main_libprofile_import.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_profile_import_main_libprofile_import_cpp, "chrome/profile_import/main_libprofile_import.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_chrome_profile_import_profile_import_main_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_profile_import_profile_import_thread_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_profile_import_main_libprofile_import_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_chrome_profile_import_profile_import_main_cpp(it);
  PUBLIC_chrome_profile_import_profile_import_thread_cpp(it);
}

#endif

