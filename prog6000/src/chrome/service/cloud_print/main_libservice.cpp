/* CONSBENCH file begin */
#ifndef MAIN_LIBSERVICE_CPP
#define MAIN_LIBSERVICE_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_service_cloud_print_main_libservice_cpp, "chrome/service/cloud_print/main_libservice.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_service_cloud_print_main_libservice_cpp, "chrome/service/cloud_print/main_libservice.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_chrome_service_service_child_process_host_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_service_ipc_server_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_service_main_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_service_process_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_service_process_prefs_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_service_utility_process_host_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_cloud_print_cloud_print_consts_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_cloud_print_cloud_print_helpers_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_cloud_print_cloud_print_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_cloud_print_cloud_print_proxy_backend_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_cloud_print_cloud_print_url_fetcher_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_cloud_print_job_status_updater_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_cloud_print_print_system_dummy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_cloud_print_print_system_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_cloud_print_printer_job_handler_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_gaia_service_gaia_authenticator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_net_service_url_request_context_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_remoting_chromoting_host_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_service_cloud_print_print_system_cups_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_service_cloud_print_main_libservice_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_chrome_service_service_child_process_host_cpp(it);
  PUBLIC_chrome_service_service_ipc_server_cpp(it);
  PUBLIC_chrome_service_service_main_cpp(it);
  PUBLIC_chrome_service_service_process_cpp(it);
  PUBLIC_chrome_service_service_process_prefs_cpp(it);
  PUBLIC_chrome_service_service_utility_process_host_cpp(it);
  PUBLIC_chrome_service_cloud_print_cloud_print_consts_cpp(it);
  PUBLIC_chrome_service_cloud_print_cloud_print_helpers_cpp(it);
  PUBLIC_chrome_service_cloud_print_cloud_print_proxy_cpp(it);
  PUBLIC_chrome_service_cloud_print_cloud_print_proxy_backend_cpp(it);
  PUBLIC_chrome_service_cloud_print_cloud_print_url_fetcher_cpp(it);
  PUBLIC_chrome_service_cloud_print_job_status_updater_cpp(it);
  PUBLIC_chrome_service_cloud_print_print_system_dummy_cpp(it);
  PUBLIC_chrome_service_cloud_print_print_system_cpp(it);
  PUBLIC_chrome_service_cloud_print_printer_job_handler_cpp(it);
  PUBLIC_chrome_service_gaia_service_gaia_authenticator_cpp(it);
  PUBLIC_chrome_service_net_service_url_request_context_cpp(it);
  PUBLIC_chrome_service_remoting_chromoting_host_manager_cpp(it);
  PUBLIC_chrome_service_cloud_print_print_system_cups_cpp(it);
}

#endif

