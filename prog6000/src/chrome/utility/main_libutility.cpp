/* CONSBENCH file begin */
#ifndef MAIN_LIBUTILITY_CPP
#define MAIN_LIBUTILITY_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_utility_main_libutility_cpp, "chrome/utility/main_libutility.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_utility_main_libutility_cpp, "chrome/utility/main_libutility.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_chrome_utility_utility_main_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_utility_utility_thread_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_utility_main_libutility_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_chrome_utility_utility_main_cpp(it);
  PUBLIC_chrome_utility_utility_thread_cpp(it);
}

#endif

