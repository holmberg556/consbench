/* CONSBENCH file begin */
#ifndef MAIN_LIBWORKER_CPP
#define MAIN_LIBWORKER_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_worker_main_libworker_cpp, "chrome/worker/main_libworker.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_worker_main_libworker_cpp, "chrome/worker/main_libworker.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_chrome_worker_websharedworker_stub_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_worker_webworker_stub_base_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_worker_webworker_stub_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_worker_webworkerclient_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_worker_worker_main_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_worker_worker_thread_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_worker_worker_webapplicationcachehost_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_worker_worker_webkitclient_impl_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_chrome_worker_main_libworker_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_chrome_worker_websharedworker_stub_cpp(it);
  PUBLIC_chrome_worker_webworker_stub_base_cpp(it);
  PUBLIC_chrome_worker_webworker_stub_cpp(it);
  PUBLIC_chrome_worker_webworkerclient_proxy_cpp(it);
  PUBLIC_chrome_worker_worker_main_cpp(it);
  PUBLIC_chrome_worker_worker_thread_cpp(it);
  PUBLIC_chrome_worker_worker_webapplicationcachehost_impl_cpp(it);
  PUBLIC_chrome_worker_worker_webkitclient_impl_cpp(it);
}

#endif

