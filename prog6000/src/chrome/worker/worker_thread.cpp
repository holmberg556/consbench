/* CONSBENCH file begin */
#ifndef WORKER_THREAD_CC_1833
#define WORKER_THREAD_CC_1833

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, chrome_worker_worker_thread_cpp, "chrome/worker/worker_thread.cpp");

/* CONSBENCH includes begin */
#include "chrome/worker/worker_thread.h"
#include "base/command_line.h"
#include "base/lazy_instance.h"
#include "base/threading/thread_local.h"
#include "chrome/common/appcache/appcache_dispatcher.h"
#include "chrome/common/chrome_switches.h"
#include "chrome/common/db_message_filter.h"
#include "chrome/common/web_database_observer_impl.h"
#include "chrome/common/worker_messages.h"
#include "chrome/worker/webworker_stub.h"
#include "chrome/worker/websharedworker_stub.h"
#include "chrome/worker/worker_webkitclient_impl.h"
#include "ipc/ipc_sync_channel.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/WebBlobRegistry.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/WebDatabase.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/WebKit.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/WebRuntimeFeatures.h"
#include "webkit/glue/webkit_glue.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(chrome_worker_worker_thread_cpp, "chrome/worker/worker_thread.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(chrome_worker_worker_thread_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include "chrome/worker/worker_thread.h" */
/*-no-  */
/*-no- #include "base/command_line.h" */
/*-no- #include "base/lazy_instance.h" */
/*-no- #include "base/threading/thread_local.h" */
/*-no- #include "chrome/common/appcache/appcache_dispatcher.h" */
/*-no- #include "chrome/common/chrome_switches.h" */
/*-no- #include "chrome/common/db_message_filter.h" */
/*-no- #include "chrome/common/web_database_observer_impl.h" */
/*-no- #include "chrome/common/worker_messages.h" */
/*-no- #include "chrome/worker/webworker_stub.h" */
/*-no- #include "chrome/worker/websharedworker_stub.h" */
/*-no- #include "chrome/worker/worker_webkitclient_impl.h" */
/*-no- #include "ipc/ipc_sync_channel.h" */
/*-no- #include "third_party/WebKit/Source/WebKit/chromium/public/WebBlobRegistry.h" */
/*-no- #include "third_party/WebKit/Source/WebKit/chromium/public/WebDatabase.h" */
/*-no- #include "third_party/WebKit/Source/WebKit/chromium/public/WebKit.h" */
/*-no- #include "third_party/WebKit/Source/WebKit/chromium/public/WebRuntimeFeatures.h" */
/*-no- #include "webkit/glue/webkit_glue.h" */
/*-no-  */
/*-no- ................................. */
/*-no-  */
/*-no- ............................................................................ */
/*-no- .............................. */
/*-no-  */
/*-no-  */
/*-no- .............................. */
/*-no- ................................ */
/*-no- ................................................... */
/*-no- ........................................... */
/*-no-  */
/*-no- ........................................................... */
/*-no-  */
/*-no- ....................................................................... */
/*-no- ...................................................................... */
/*-no- ............................................. */
/*-no- ................................................. */
/*-no-  */
/*-no- ...................................................................... */
/*-no-  */
/*-no- ........................................ */
/*-no- ....................................................................... */
/*-no-  */
/*-no- ............................................. */
/*-no- ............................................................ */
/*-no-  */
/*-no- ..................................................... */
/*-no- ................................................................... */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- .............................................................................. */
/*-no- .......................................... */
/*-no- ....................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- .................................... */
/*-no- ............................................................. */
/*-no-  */
/*-no- ....................................... */
/*-no- ............................................................. */
/*-no-  */
/*-no- .................................. */
/*-no- .......................................................... */
/*-no- .................................................................... */
/*-no- . */
/*-no-  */
/*-no- ............................... */
/*-no- ..................................................... */
/*-no- .................................................... */
/*-no- ............................ */
/*-no-  */
/*-no- ..................... */
/*-no- ................................ */
/*-no- . */
/*-no-  */
/*-no- ....................................... */
/*-no- ................................... */
/*-no- . */
/*-no-  */
/*-no- ...................................................................... */
/*-no- ................................................. */
/*-no- ................................................... */
/*-no- ................ */
/*-no-  */
/*-no- ...................... */
/*-no- .......................................... */
/*-no- ...................................................................... */
/*-no- .......................................... */
/*-no- ....................... */
/*-no- ................. */
/*-no- . */
/*-no-  */
/*-no- .................................. */
/*-no- ......................................................... */
/*-no- ............................................ */
/*-no- .................................................. */
/*-no- ...................................... */
/*-no- ........................................ */
/*-no-  */
/*-no- .......................................................... */
/*-no- ....................... */
/*-no- .............................................................................. */
/*-no- ...... */
/*-no- ....................................................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................. */
/*-no- ..................................... */
/*-no- .................................... */
/*-no-  */
/*-no- ............................................................ */
/*-no- ......................................... */
/*-no- ............................ */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- .............................................................. */
/*-no- ............................ */
/*-no- . */
/*-no-  */
/*-no- ........................................................... */
/*-no- ............................. */
/*-no- . */

#endif
/* CONSBENCH file end */
