//----------------------------------------------------------------------
// ConsBench.cpp
//----------------------------------------------------------------------
// Copyright 2011-2012 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "consbench".
//
// "consbench" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "consbench" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "consbench".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

#include "ConsBench.h"
#include "ConsBenchImpl.h"

#include <stdlib.h>
#include <string>
#include <iostream>
#include <sstream>

namespace {
  int call_count = 0;
}

ConsBenchAcc::ConsBenchAcc()
{
  mImpl = new Impl();
}

std::string convert_DATE(const std::string & date)
{
  std::string res;
  res += date.substr(7, 4);
  res += "-";

  std::string month = date.substr(0, 3);
  if      (month == "Jan") res += "01";
  else if (month == "Feb") res += "02";
  else if (month == "Mar") res += "03";
  else if (month == "Apr") res += "04";
  else if (month == "May") res += "05";
  else if (month == "Jun") res += "06";
  else if (month == "Jul") res += "07";
  else if (month == "Aug") res += "08";
  else if (month == "Sep") res += "09";
  else if (month == "Oct") res += "10";
  else if (month == "Nov") res += "11";
  else if (month == "Dec") res += "12";
  else abort();
  res += "-";

  res += date.substr(4, 2);
  if (res[8] == ' ') res[8] = '0';

  return res;
}

void ConsBenchAcc::enter(const char * date_str, const char * time_str, int i, const char * file)
{
  std::string date_string = date_str;
  std::string time_string = time_str;

  //std::cout << "=== begin " << file << std::endl;
  std::string str;
  str += "=== begin ";
  str += file;
  mImpl->mOutput.push_back(str);
  call_count++;

  std::ostringstream os;
  os << i;
  mImpl->mVersions[file] = os.str();

  if (mImpl->mTimeStr == "") {
    mImpl->mTimeStr = convert_DATE(date_string) + "_" + time_string;
  }
  if (mImpl->mSource == "") {
    mImpl->mSource = file;
  }
}

void ConsBenchAcc::leave(const char * file)
{
  //std::cout << "=== begin " << file << std::endl;
  std::string str;
  str += "=== end   ";
  str += file;
  mImpl->mOutput.push_back(str);
  call_count++;
}

void ConsBenchAcc::show()
{
  //std::cout << "... ConsBench_print " <<   call_count << " " << mImpl->mOutput.size() << std::endl;
  for (string_arr_t::iterator it = mImpl->mOutput.begin();
       it != mImpl->mOutput.end();
       ++it)
  {
    std::cout << "... " << (*it) << std::endl;
  }

  for (string_map_t::iterator it = mImpl->mVersions.begin();
       it != mImpl->mVersions.end();
       ++it)
  {
    std::cout << ">>> " << (*it).first << " - " << (*it).second << std::endl;
  }
}
