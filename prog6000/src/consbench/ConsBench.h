//----------------------------------------------------------------------
// ConsBench.h
//----------------------------------------------------------------------
// Copyright 2011-2012 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "consbench".
//
// "consbench" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "consbench" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "consbench".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

#ifndef CONSBENCH_H
#define CONSBENCH_H

void ConsBench_print();

#ifdef TESTING_CONSBENCH
static void * pointers[100];
static int pointers_i = 0;
#endif

class ConsBenchAcc
{
public:
  ConsBenchAcc();
  ConsBenchAcc(ConsBenchAcc & orig); // not implemented
  
  void enter(const char * date_str, const char * time_str, int i, const char * file);
  void leave(const char * file);
  
  void show();
  
  class Impl;
  Impl * get_impl() { return mImpl; }
private:
  Impl * mImpl;
};

//----------------------------------------

namespace {

  ConsBenchAcc & sConsBenchAcc()
  {
    static ConsBenchAcc obj;
    return obj;
  }

  class ConsBenchEnter
  {
  public:
    ConsBenchEnter(const char * date_str, const char * time_str, int i, const char * file)
    {
      sConsBenchAcc().enter(date_str, time_str, i, file);
#ifdef TESTING_CONSBENCH
      pointers[pointers_i++] = &sConsBenchAcc();
#endif
    }
  };

  //----------------------------------------

  class ConsBenchLeave
  {
  public:
    ConsBenchLeave(const char * file)
    {
      sConsBenchAcc().leave(file);
#ifdef TESTING_CONSBENCH
      pointers[pointers_i++] = &sConsBenchAcc();
#endif
    }
  };
}

#define CONSBENCH_ENTER(i, id, file) namespace { ConsBenchEnter ENTER_##id(__DATE__, __TIME__, i, file); }
#define CONSBENCH_LEAVE(id, file) namespace { ConsBenchLeave LEAVE_##id(file); }

typedef void (*ConsBenchAcc_iter_t)(ConsBenchAcc &);

#define CONSBENCH_PUBLIC(id)                             \
  void PUBLIC_##id(ConsBenchAcc_iter_t it)               \
  {                                                      \
    it(sConsBenchAcc());                                 \
  }

#endif
