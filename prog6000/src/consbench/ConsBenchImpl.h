//----------------------------------------------------------------------
// ConsBenchImpl.h
//----------------------------------------------------------------------
// Copyright 2011-2012 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "consbench".
//
// "consbench" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "consbench" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "consbench".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

#ifndef CONSBENCHIMPL_H
#define CONSBENCHIMPL_H

#include <map>
#include <string>
#include <vector>

typedef std::map<std::string,std::string> string_map_t;
typedef std::vector<std::string> string_arr_t;


class ConsBenchAcc::Impl
{
public:
  string_map_t mVersions;
  string_arr_t mOutput;
  std::string mTimeStr;
  std::string mSource;

  Impl()
  {
    mSource  = "";
    mTimeStr = "";
  }

};

#endif

