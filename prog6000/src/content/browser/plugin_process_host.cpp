/* CONSBENCH file begin */
#ifndef PLUGIN_PROCESS_HOST_CC_2251
#define PLUGIN_PROCESS_HOST_CC_2251

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, content_browser_plugin_process_host_cpp, "content/browser/plugin_process_host.cpp");

/* CONSBENCH includes begin */
#include "content/browser/plugin_process_host.h"
#include <utility>  // for pair<>
#include <vector>
#include "app/app_switches.h"
#include "base/command_line.h"
#include "base/file_path.h"
#include "base/file_util.h"
#include "base/logging.h"
#include "base/path_service.h"
#include "base/string_util.h"
#include "base/utf_string_conversions.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/chrome_plugin_browsing_context.h"
#include "chrome/browser/net/url_request_tracking.h"
#include "chrome/browser/plugin_download_helper.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/common/chrome_paths.h"
#include "chrome/common/chrome_plugin_lib.h"
#include "chrome/common/chrome_switches.h"
#include "chrome/common/logging_chrome.h"
#include "chrome/common/net/url_request_context_getter.h"
#include "chrome/common/plugin_messages.h"
#include "chrome/common/render_messages.h"
#include "chrome/common/render_messages_params.h"
#include "content/browser/browser_thread.h"
#include "content/browser/child_process_security_policy.h"
#include "content/browser/plugin_service.h"
#include "content/browser/renderer_host/resource_dispatcher_host.h"
#include "content/browser/renderer_host/resource_message_filter.h"
#include "content/common/resource_messages.h"
#include "ipc/ipc_switches.h"
#include "net/base/cookie_store.h"
#include "net/base/io_buffer.h"
#include "net/url_request/url_request.h"
#include "net/url_request/url_request_context.h"
#include "ui/base/ui_base_switches.h"
#include "ui/gfx/native_widget_types.h"
#include "ui/gfx/gtk_native_view_id_manager.h"
#include "base/mac/mac_util.h"
#include "chrome/common/plugin_carbon_interpose_constants_mac.h"
#include "ui/gfx/rect.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(content_browser_plugin_process_host_cpp, "content/browser/plugin_process_host.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(content_browser_plugin_process_host_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include "content/browser/plugin_process_host.h" */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- #include <windows.h> */
/*-no- #elif defined(OS_POSIX) */
/*-no- #include <utility>  // for pair<> */
/*-no- #endif */
/*-no-  */
/*-no- #include <vector> */
/*-no-  */
/*-no- #include "app/app_switches.h" */
/*-no- #include "base/command_line.h" */
/*-no- #include "base/file_path.h" */
/*-no- #include "base/file_util.h" */
/*-no- #include "base/logging.h" */
/*-no- #include "base/path_service.h" */
/*-no- #include "base/string_util.h" */
/*-no- #include "base/utf_string_conversions.h" */
/*-no- #include "chrome/browser/browser_process.h" */
/*-no- #include "chrome/browser/chrome_plugin_browsing_context.h" */
/*-no- #include "chrome/browser/net/url_request_tracking.h" */
/*-no- #include "chrome/browser/plugin_download_helper.h" */
/*-no- #include "chrome/browser/profiles/profile.h" */
/*-no- #include "chrome/common/chrome_paths.h" */
/*-no- #include "chrome/common/chrome_plugin_lib.h" */
/*-no- #include "chrome/common/chrome_switches.h" */
/*-no- #include "chrome/common/logging_chrome.h" */
/*-no- #include "chrome/common/net/url_request_context_getter.h" */
/*-no- #include "chrome/common/plugin_messages.h" */
/*-no- #include "chrome/common/render_messages.h" */
/*-no- #include "chrome/common/render_messages_params.h" */
/*-no- #include "content/browser/browser_thread.h" */
/*-no- #include "content/browser/child_process_security_policy.h" */
/*-no- #include "content/browser/plugin_service.h" */
/*-no- #include "content/browser/renderer_host/resource_dispatcher_host.h" */
/*-no- #include "content/browser/renderer_host/resource_message_filter.h" */
/*-no- #include "content/common/resource_messages.h" */
/*-no- #include "ipc/ipc_switches.h" */
/*-no- #include "net/base/cookie_store.h" */
/*-no- #include "net/base/io_buffer.h" */
/*-no- #include "net/url_request/url_request.h" */
/*-no- #include "net/url_request/url_request_context.h" */
/*-no- #include "ui/base/ui_base_switches.h" */
/*-no- #include "ui/gfx/native_widget_types.h" */
/*-no-  */
/*-no- #if defined(USE_X11) */
/*-no- #include "ui/gfx/gtk_native_view_id_manager.h" */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(OS_MACOSX) */
/*-no- #include "base/mac/mac_util.h" */
/*-no- #include "chrome/common/plugin_carbon_interpose_constants_mac.h" */
/*-no- #include "ui/gfx/rect.h" */
/*-no- #endif */
/*-no-  */
/*-no- ............................................. */
/*-no- ................................................................... */
/*-no-  */
/*-no- ........... */
/*-no-  */
/*-no- ............................................................................. */
/*-no- .............................................. */
/*-no- ..................................... */
/*-no- ............................................................... */
/*-no- ........ */
/*-no- ..................................... */
/*-no- ... */
/*-no-  */
/*-no- .................................................... */
/*-no- ........................................................ */
/*-no- ........................................................................ */
/*-no- .......................................... */
/*-no- ... */
/*-no-  */
/*-no- ......... */
/*-no- ............................................... */
/*-no- .. */
/*-no-  */
/*-no- ............... */
/*-no-  */
/*-no- #if defined(OS_WIN) */
/*-no- ........................................................................... */
/*-no- ................................................................................ */
/*-no- ........................................... */
/*-no- ......................................... */
/*-no- .............................................. */
/*-no- ....................................................... */
/*-no- ........... */
/*-no-  */
/*-no- ................................................. */
/*-no- ...................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................. */
/*-no- ..................................................... */
/*-no- ........................................................................ */
/*-no- ................................................ */
/*-no- ........................................................................ */
/*-no- ........................................ */
/*-no- ................................................................... */
/*-no- . */
/*-no-  */
/*-no- ................................................ */
/*-no- ............................................ */
/*-no- . */
/*-no-  */
/*-no- #endif  // defined(OS_WIN) */
/*-no-  */
/*-no- #if defined(TOOLKIT_USES_GTK) */
/*-no- ............................................................... */
/*-no- ............................................................................ */
/*-no- .............. */
/*-no- ............................................................... */
/*-no- . */
/*-no- #endif  // defined(TOOLKIT_USES_GTK) */
/*-no-  */
/*-no- ...................................... */
/*-no- .............................. */
/*-no- ......................... */
/*-no- ................................................................... */
/*-no- ................................................. */
/*-no- ........................................................................... */
/*-no- #if defined(OS_MACOSX) */
/*-no- .................................... */
/*-no- #endif */
/*-no- . */
/*-no- . */
/*-no-  */
/*-no- ......................................... */
/*-no- #if defined(OS_WIN) */
/*-no- ......................................................................... */
/*-no- .............................................................................. */
/*-no- ........................................................................... */
/*-no- ............................................................................... */
/*-no- ............................................................................. */
/*-no- .................................................................. */
/*-no- ........................................ */
/*-no- ......................................................... */
/*-no- ........................................................ */
/*-no- ........................ */
/*-no- ............................................... */
/*-no- ... */
/*-no- #elif defined(OS_MACOSX) */
/*-no- ............................................................................... */
/*-no- ............................................ */
/*-no- .......................................... */
/*-no- ............................................................. */
/*-no- ............................................................ */
/*-no- ........................ */
/*-no- ........................................................ */
/*-no- ...................................................................... */
/*-no- ............ */
/*-no- .............................. */
/*-no- ....................................... */
/*-no- ........................................................... */
/*-no- .................................................................. */
/*-no- ..... */
/*-no- ... */
/*-no- .............................................. */
/*-no- ................................ */
/*-no- ........................................................ */
/*-no- ........................................... */
/*-no- ............ */
/*-no- .............................. */
/*-no- ..................................... */
/*-no- ........................................................... */
/*-no- ................................... */
/*-no- ..... */
/*-no- ... */
/*-no- #endif */
/*-no- .......................................... */
/*-no- ................... */
/*-no- . */
/*-no-  */
/*-no- ...................................................................... */
/*-no- ......................................................... */
/*-no- ............... */
/*-no- ........................................ */
/*-no- .............................................. */
/*-no-  */
/*-no- ....................... */
/*-no- ................. */
/*-no-  */
/*-no- ............................................................................ */
/*-no- .......................................................... */
/*-no- .............................................................................. */
/*-no- ........................................... */
/*-no- ........................................................................... */
/*-no- ............................................................ */
/*-no- ....................... */
/*-no- ................. */
/*-no-  */
/*-no- .................................................... */
/*-no- ........................................................................ */
/*-no- ............................................................... */
/*-no- ................................................................................ */
/*-no- ............................................................... */
/*-no-  */
/*-no- ...................................... */
/*-no- ...................................................... */
/*-no-  */
/*-no- ............................................................................ */
/*-no- .................................................................. */
/*-no- ............................................. */
/*-no- ................................... */
/*-no- ......................... */
/*-no- ........................... */
/*-no- ........................... */
/*-no- ......................... */
/*-no- ............................... */
/*-no- ..................................... */
/*-no- ............................. */
/*-no- .............................. */
/*-no- ............................ */
/*-no- ................................. */
/*-no- ........................... */
/*-no- ............................ */
/*-no- .................................. */
/*-no- ............................... */
/*-no- ................................ */
/*-no- ..................... */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ............................ */
/*-no- #endif */
/*-no- .... */
/*-no-  */
/*-no- ................................................................ */
/*-no- ...................................................... */
/*-no-  */
/*-no- .................................................................. */
/*-no- ............................... */
/*-no- .............................................. */
/*-no-  */
/*-no- ........................ */
/*-no- ............................................................................... */
/*-no- ............................................ */
/*-no- ......................................................... */
/*-no- ... */
/*-no-  */
/*-no- ............................................................ */
/*-no- ..................... */
/*-no- ........................................................... */
/*-no- ............................ */
/*-no- ................................................................. */
/*-no-  */
/*-no- ......................................................................... */
/*-no-  */
/*-no- ........................................ */
/*-no-  */
/*-no- #if defined(OS_POSIX) */
/*-no- ............................... */
/*-no- #if defined(OS_MACOSX) && !defined(__LP64__) */
/*-no- ......................................................................... */
/*-no- ............................................................... */
/*-no- .............................................................................. */
/*-no- ............................. */
/*-no- ................................................................ */
/*-no- ...................... */
/*-no- .................................. */
/*-no- ............................................ */
/*-no- ... */
/*-no- .................................................... */
/*-no- ........................................................ */
/*-no- ....................... */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- ......... */
/*-no- #if defined(OS_WIN) */
/*-no- ................. */
/*-no- #elif defined(OS_POSIX) */
/*-no- ............ */
/*-no- .......... */
/*-no- #endif */
/*-no- ................ */
/*-no-  */
/*-no- .............. */
/*-no- . */
/*-no-  */
/*-no- ......................................... */
/*-no- ........................................................ */
/*-no- ................................................................ */
/*-no- ........................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................. */
/*-no- ...................... */
/*-no- ................................................................. */
/*-no- ................................................................................ */
/*-no- ......................................... */
/*-no- ............................................... */
/*-no- .......................................... */
/*-no- ......................................................................... */
/*-no- ............................... */
/*-no- ..... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- .................................................................... */
/*-no- ...................... */
/*-no- ............................................... */
/*-no- .............................................................................. */
/*-no- ................................................................ */
/*-no- ............................................. */
/*-no- ............................................................................ */
/*-no- ...................................................................... */
/*-no- ........................................................................ */
/*-no- ...................................................................... */
/*-no- ................................................... */
/*-no- #if defined(OS_WIN) */
/*-no- ................................................................... */
/*-no- ................................................ */
/*-no- ........................................................................ */
/*-no- #endif */
/*-no- #if defined(TOOLKIT_USES_GTK) */
/*-no- ............................................................. */
/*-no- .......................................... */
/*-no- #endif */
/*-no- #if defined(OS_MACOSX) */
/*-no- ................................................................ */
/*-no- ............................................. */
/*-no- .............................................................. */
/*-no- ........................................... */
/*-no- .............................................................. */
/*-no- ........................................... */
/*-no- ....................................................................... */
/*-no- .................................................... */
/*-no- #endif */
/*-no- .......................................... */
/*-no- ....................... */
/*-no-  */
/*-no- .................. */
/*-no- ................. */
/*-no- . */
/*-no-  */
/*-no- ............................................................ */
/*-no- ......................................................... */
/*-no- ............................................... */
/*-no- ... */
/*-no-  */
/*-no- ............................ */
/*-no- . */
/*-no-  */
/*-no- .......................................... */
/*-no- ................... */
/*-no- . */
/*-no-  */
/*-no- ....................................... */
/*-no- ................................ */
/*-no- . */
/*-no-  */
/*-no- .......................................... */
/*-no- ....................................................... */
/*-no- .................................... */
/*-no- ............................ */
/*-no-  */
/*-no- ................................... */
/*-no- ...................................... */
/*-no- ......................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ............................................................. */
/*-no- .................... */
/*-no- ............................... */
/*-no- .......................... */
/*-no- .................................................................. */
/*-no- ..................................................................... */
/*-no- ....................................... */
/*-no- ........................................ */
/*-no- ........... */
/*-no- ... */
/*-no-  */
/*-no- .......................................................................... */
/*-no- ............................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................ */
/*-no- ..................................................... */
/*-no- ............................................................ */
/*-no- ............................................................................. */
/*-no- ............................................. */
/*-no- ............................................................................ */
/*-no- ............... */
/*-no- .......................................................................... */
/*-no-  */
/*-no- ............................................................................... */
/*-no- ................................. */
/*-no- ........................................... */
/*-no- ........................................................ */
/*-no- .......... */
/*-no- ............................................................. */
/*-no- ..................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ...................................................... */
/*-no- ............................................................................ */
/*-no- ...................................................... */
/*-no- ...................................... */
/*-no- ................................................ */
/*-no-  */
/*-no- ............................................. */
/*-no- .......................................................................... */
/*-no- .................................................. */
/*-no- ..................................................................... */
/*-no- ....................... */
/*-no- ............. */
/*-no- ..... */
/*-no- ... */
/*-no-  */
/*-no- .................. */
/*-no- . */
/*-no-  */
/*-no- ....................................................... */
/*-no- ................................................................. */
/*-no- .................................................. */
/*-no- . */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ........................................................... */
/*-no- ................................................................................ */
/*-no- ...................................................... */
/*-no- ..................................... */
/*-no- .................. */
/*-no- . */
/*-no-  */
/*-no- .............................................................. */
/*-no- .............................................................................. */
/*-no- ............................................................................ */
/*-no- ............................................................................... */
/*-no- ............................................................................. */
/*-no- ...................................................... */
/*-no- ....................................... */
/*-no- ...................................................... */
/*-no- ................................................................. */
/*-no- ......................... */
/*-no- .................. */
/*-no- ................................ */
/*-no- .......... */
/*-no- ...................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ......................................... */
/*-no- ............................................... */
/*-no- .......................................... */
/*-no-  */
/*-no- .......................................... */
/*-no- ....................... */
/*-no- . */
/*-no-  */
/*-no- .............................................................................. */
/*-no- ..................................................... */
/*-no- ........................... */
/*-no- ................. */
/*-no- ........... */
/*-no- ... */
/*-no-  */
/*-no- ..................................................... */
/*-no- .............................................................. */
/*-no- ......................... */
/*-no- ................................................. */
/*-no- .......... */
/*-no- ............................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ........................................ */
/*-no- ..................................... */
/*-no- ........................................................ */
/*-no-  */
/*-no- ..................................................................... */
/*-no- ...................... */
/*-no- ................................................................................ */
/*-no- ....................................................... */
/*-no- .............................................................. */
/*-no- ... */
/*-no- . */

#endif
/* CONSBENCH file end */
