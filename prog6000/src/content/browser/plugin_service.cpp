/* CONSBENCH file begin */
#ifndef PLUGIN_SERVICE_CC_2254
#define PLUGIN_SERVICE_CC_2254

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, content_browser_plugin_service_cpp, "content/browser/plugin_service.cpp");

/* CONSBENCH includes begin */
#include "content/browser/plugin_service.h"
#include <vector>
#include "base/command_line.h"
#include "base/path_service.h"
#include "base/string_util.h"
#include "base/threading/thread.h"
#include "base/utf_string_conversions.h"
#include "base/values.h"
#include "base/synchronization/waitable_event.h"
#include "chrome/browser/browser_process.h"
#include "chrome/browser/chrome_plugin_host.h"
#include "chrome/browser/extensions/extension_service.h"
#include "chrome/browser/plugin_updater.h"
#include "chrome/browser/profiles/profile.h"
#include "chrome/common/chrome_plugin_lib.h"
#include "chrome/common/chrome_paths.h"
#include "chrome/common/chrome_switches.h"
#include "chrome/common/default_plugin.h"
#include "chrome/common/extensions/extension.h"
#include "chrome/common/logging_chrome.h"
#include "chrome/common/notification_type.h"
#include "chrome/common/notification_service.h"
#include "chrome/common/pepper_plugin_registry.h"
#include "chrome/common/plugin_messages.h"
#include "chrome/common/render_messages.h"
#include "content/browser/browser_thread.h"
#include "content/browser/ppapi_plugin_process_host.h"
#include "content/browser/renderer_host/render_process_host.h"
#include "content/browser/renderer_host/render_view_host.h"
#include "webkit/plugins/npapi/plugin_constants_win.h"
#include "webkit/plugins/npapi/plugin_list.h"
#include "webkit/plugins/npapi/webplugininfo.h"
#include "chrome/browser/chromeos/plugin_selection_policy.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(content_browser_plugin_service_cpp, "content/browser/plugin_service.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(content_browser_plugin_service_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include "content/browser/plugin_service.h" */
/*-no-  */
/*-no- #include <vector> */
/*-no-  */
/*-no- #include "base/command_line.h" */
/*-no- #include "base/path_service.h" */
/*-no- #include "base/string_util.h" */
/*-no- #include "base/threading/thread.h" */
/*-no- #include "base/utf_string_conversions.h" */
/*-no- #include "base/values.h" */
/*-no- #include "base/synchronization/waitable_event.h" */
/*-no- #include "chrome/browser/browser_process.h" */
/*-no- #include "chrome/browser/chrome_plugin_host.h" */
/*-no- #include "chrome/browser/extensions/extension_service.h" */
/*-no- #include "chrome/browser/plugin_updater.h" */
/*-no- #include "chrome/browser/profiles/profile.h" */
/*-no- #include "chrome/common/chrome_plugin_lib.h" */
/*-no- #include "chrome/common/chrome_paths.h" */
/*-no- #include "chrome/common/chrome_switches.h" */
/*-no- #include "chrome/common/default_plugin.h" */
/*-no- #include "chrome/common/extensions/extension.h" */
/*-no- #include "chrome/common/logging_chrome.h" */
/*-no- #include "chrome/common/notification_type.h" */
/*-no- #include "chrome/common/notification_service.h" */
/*-no- #include "chrome/common/pepper_plugin_registry.h" */
/*-no- #include "chrome/common/plugin_messages.h" */
/*-no- #include "chrome/common/render_messages.h" */
/*-no- #include "content/browser/browser_thread.h" */
/*-no- #include "content/browser/ppapi_plugin_process_host.h" */
/*-no- #include "content/browser/renderer_host/render_process_host.h" */
/*-no- #include "content/browser/renderer_host/render_view_host.h" */
/*-no- #include "webkit/plugins/npapi/plugin_constants_win.h" */
/*-no- #include "webkit/plugins/npapi/plugin_list.h" */
/*-no- #include "webkit/plugins/npapi/webplugininfo.h" */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- #include "chrome/browser/chromeos/plugin_selection_policy.h" */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(OS_MACOSX) */
/*-no- ......................................... */
/*-no- ........................................................ */
/*-no-  */
/*-no- ................................................................................ */
/*-no- .............................. */
/*-no- ....................................................................... */
/*-no- .............................. */
/*-no- ... */
/*-no- . */
/*-no- #endif */
/*-no-  */
/*-no- ..................................................... */
/*-no- .............................................................................. */
/*-no- ..................................... */
/*-no- ............................................................................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- #if defined(OS_LINUX) */
/*-no- ............................................. */
/*-no- ................................................................... */
/*-no- ........................................................ */
/*-no- ........................................................ */
/*-no- ......................................... */
/*-no- ............................................................. */
/*-no- ... */
/*-no- .......................... */
/*-no- ......................................................................... */
/*-no- .................................................................... */
/*-no- ................. */
/*-no- ... */
/*-no- .. */
/*-no- #endif */
/*-no-  */
/*-no- ......... */
/*-no- .................................................. */
/*-no-  */
/*-no- ......... */
/*-no- .......................................................... */
/*-no- ........................................................ */
/*-no-  */
/*-no- ................................................................... */
/*-no- ....................... */
/*-no- .......................................................................... */
/*-no-  */
/*-no- ................................................................... */
/*-no- ............................................................ */
/*-no- . */
/*-no-  */
/*-no- ......... */
/*-no- ............................................. */
/*-no- ......................................... */
/*-no- . */
/*-no-  */
/*-no- ......... */
/*-no- ...................................................... */
/*-no- .................................. */
/*-no- . */
/*-no-  */
/*-no- .............................. */
/*-no- ................................................. */
/*-no- ...................................... */
/*-no- ............................................................. */
/*-no- .......................... */
/*-no-  */
/*-no- .................................................................. */
/*-no- .............................................. */
/*-no-  */
/*-no- .................................................... */
/*-no- ..................................................................... */
/*-no- .......................................................................... */
/*-no- .................... */
/*-no- ..................................................................... */
/*-no- ..................................................................... */
/*-no- .................... */
/*-no- .................................................................... */
/*-no-  */
/*-no- .......................................... */
/*-no-  */
/*-no- ....................................................... */
/*-no- ................................................... */
/*-no- ............................................. */
/*-no- ........................................................... */
/*-no- ..................................................................... */
/*-no- ... */
/*-no-  */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ................................................................. */
/*-no- ........................................ */
/*-no- #endif */
/*-no-  */
/*-no- ....................................................................... */
/*-no- ........................................................................... */
/*-no- ......................................................................... */
/*-no- #if defined(OS_WIN) */
/*-no- ................... */
/*-no- ............................................................................. */
/*-no- ................... */
/*-no- .............................................................................. */
/*-no- ................................................... */
/*-no- ........................................................................ */
/*-no- ......................................................... */
/*-no- ... */
/*-no-  */
/*-no- ................................................... */
/*-no- ........................................................................ */
/*-no- ......................................................... */
/*-no- ... */
/*-no- #elif defined(OS_POSIX) && !defined(OS_MACOSX) */
/*-no- ...................................................... */
/*-no- ..................................... */
/*-no- ......................... */
/*-no- ................................................................ */
/*-no- .............................................................. */
/*-no- ......................................... */
/*-no- ... */
/*-no- #endif */
/*-no- .............................................................................. */
/*-no- ............................................................................... */
/*-no- ....................................................................... */
/*-no- ............................................................................... */
/*-no- #if defined(OS_LINUX) */
/*-no- .......................................................... */
/*-no- ................................................................... */
/*-no- ......................................................................... */
/*-no- .................................... */
/*-no- ............................................................... */
/*-no- .................... */
/*-no-  */
/*-no- ................................................... */
/*-no- ..................................................... */
/*-no- ....................................................................... */
/*-no- ............................................................................. */
/*-no- ............................................................................ */
/*-no- #if defined(OS_WIN) */
/*-no- ..................................... */
/*-no- ............... */
/*-no- #endif */
/*-no- ..................................................................... */
/*-no- ............................ */
/*-no- ....................................... */
/*-no- ............................ */
/*-no- .................................................... */
/*-no- .............................................................. */
/*-no- ...................................... */
/*-no- ... */
/*-no- #endif */
/*-no- .......................................................... */
/*-no- .................................................... */
/*-no- ............................................................ */
/*-no- .................................................... */
/*-no- #if defined(OS_MACOSX) */
/*-no- ................................................................................ */
/*-no- ......................... */
/*-no- ....................................................... */
/*-no- .................................................... */
/*-no- #endif */
/*-no- ...................................................................... */
/*-no- .................................................... */
/*-no- ...................... */
/*-no- ........................................................... */
/*-no- .................................................... */
/*-no- . */
/*-no-  */
/*-no- ................................. */
/*-no- #if defined(OS_WIN) */
/*-no- ......................................................................... */
/*-no- ............................... */
/*-no- ............................... */
/*-no- ........................ */
/*-no- ........................... */
/*-no- ........................ */
/*-no- ........................... */
/*-no- #endif */
/*-no- . */
/*-no-  */
/*-no- ...................................... */
/*-no- ....................................................... */
/*-no- .............................. */
/*-no- ........... */
/*-no-  */
/*-no- ....................................................... */
/*-no- .................................................................... */
/*-no- . */
/*-no-  */
/*-no- ...................................................................... */
/*-no- ..................................... */
/*-no- . */
/*-no-  */
/*-no- ......................................................... */
/*-no- ................................. */
/*-no- . */
/*-no-  */
/*-no- ................................................. */
/*-no- .................... */
/*-no- . */
/*-no-  */
/*-no- ......................................................... */
/*-no- .................................. */
/*-no- ........................................................ */
/*-no-  */
/*-no- ................................................................................ */
/*-no- .............................. */
/*-no- ....................................................................... */
/*-no- ........................................... */
/*-no- .................... */
/*-no- ... */
/*-no-  */
/*-no- .............. */
/*-no- . */
/*-no-  */
/*-no- .............................................................. */
/*-no- .................................. */
/*-no- ........................................................ */
/*-no-  */
/*-no- .............................................. */
/*-no- ................................................... */
/*-no- .............................. */
/*-no- .................................... */
/*-no- .................................................... */
/*-no- ............................................. */
/*-no- .................... */
/*-no- ... */
/*-no-  */
/*-no- .............. */
/*-no- . */
/*-no-  */
/*-no- ................................................................ */
/*-no- .................................. */
/*-no- ........................................................ */
/*-no-  */
/*-no- ....................................................................... */
/*-no- .................. */
/*-no- ....................... */
/*-no-  */
/*-no- .................................... */
/*-no- ................................................................... */
/*-no- ................................ */
/*-no- ................ */
/*-no- ... */
/*-no-  */
/*-no- ............................................................................. */
/*-no- .................................................................. */
/*-no- .......................................... */
/*-no- ................................................... */
/*-no- ................ */
/*-no- ... */
/*-no- ............................ */
/*-no- . */
/*-no-  */
/*-no- ..................................................................... */
/*-no- .................................. */
/*-no- ........................................................ */
/*-no-  */
/*-no- ............................................................................ */
/*-no- .................. */
/*-no- ....................... */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ......................................................... */
/*-no- ................................ */
/*-no- ...................................................... */
/*-no- ................................................ */
/*-no- ................................ */
/*-no- ............ */
/*-no- ..... */
/*-no- ... */
/*-no- ............ */
/*-no- ................ */
/*-no-  */
/*-no- ............................................................................. */
/*-no- .......................................................................... */
/*-no- ..................................... */
/*-no- ................................................... */
/*-no- ................ */
/*-no- ... */
/*-no- ............................ */
/*-no- . */
/*-no-  */
/*-no- ............................................. */
/*-no- .......................... */
/*-no- ....................... */
/*-no- .................... */
/*-no- ................................. */
/*-no- ........................................ */
/*-no- ................................................................... */
/*-no- ............................................ */
/*-no- .......................... */
/*-no- ..................................... */
/*-no- ........................ */
/*-no- ....................................................................... */
/*-no- ...................................................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................. */
/*-no- ......................... */
/*-no- ............................................. */
/*-no- ............................................................................ */
/*-no- .................. */
/*-no- ............................................. */
/*-no- ...................... */
/*-no- ............................................................................ */
/*-no- . */
/*-no-  */
/*-no- ........................................................... */
/*-no- .......................... */
/*-no- ....................... */
/*-no- .................... */
/*-no- ................................. */
/*-no- ........................................ */
/*-no- .......................................................... */
/*-no- .................................... */
/*-no- ......................................... */
/*-no- ...................................................................... */
/*-no- ....................... */
/*-no- .................................................... */
/*-no- ...................................... */
/*-no-  */
/*-no- ..................................................................... */
/*-no- .......................... */
/*-no- ................................... */
/*-no- ........................ */
/*-no- .......................................................... */
/*-no- ................................ */
/*-no- . */
/*-no-  */
/*-no- .............................................. */
/*-no- ................................ */
/*-no- ........................................ */
/*-no- ........................................................ */
/*-no-  */
/*-no- .............................................................................. */
/*-no- .................. */
/*-no- ............................................. */
/*-no- ...... */
/*-no- ...................... */
/*-no- . */
/*-no-  */
/*-no- .............................................. */
/*-no- .......................... */
/*-no- ....................... */
/*-no- .................... */
/*-no- ................................. */
/*-no- ....................................... */
/*-no- .................................... */
/*-no- ...................................................................... */
/*-no- ........................ */
/*-no- .......................................................... */
/*-no- ............................. */
/*-no- #if defined(OS_CHROMEOS) */
/*-no- ....................................................... */
/*-no- ............................................. */
/*-no- ............................................................. */
/*-no- ....................................................................... */
/*-no-  */
/*-no- .................................................. */
/*-no- ..................................................................... */
/*-no- ............................................................................. */
/*-no- .................................................. */
/*-no- ...................................... */
/*-no- ......................... */
/*-no- ........................................................... */
/*-no- ................ */
/*-no- ... */
/*-no- ............... */
/*-no- #else */
/*-no- ... */
/*-no- ....................................................... */
/*-no- ............................................................. */
/*-no- .......................................................................... */
/*-no- .................................................................... */
/*-no- .............................................. */
/*-no- ............................. */
/*-no- ........................................ */
/*-no- .............................................. */
/*-no- .................... */
/*-no- ....... */
/*-no- ..... */
/*-no- ... */
/*-no- ............................................................... */
/*-no- .............................................................. */
/*-no- #endif */
/*-no- . */
/*-no-  */
/*-no- ............................................ */
/*-no- .......................................... */
/*-no- #if defined(OS_WIN) */
/*-no- ............................................ */
/*-no- .............................. */
/*-no- .......... */
/*-no- .............................. */
/*-no- ... */
/*-no-  */
/*-no- ........................................................... */
/*-no- ............................. */
/*-no- #else */
/*-no- .............................................................. */
/*-no- ............... */
/*-no- #endif  // defined(OS_WIN) */
/*-no- . */
/*-no-  */
/*-no- .............................................................. */
/*-no- ............................. */
/*-no- ........................................................................ */
/*-no- ............. */
/*-no- ............................ */
/*-no- . */
/*-no-  */
/*-no- .................................................. */
/*-no- ............................................................. */
/*-no- ................................................................. */
/*-no- ....................... */
/*-no- .............................................. */
/*-no- ........................................................................... */
/*-no- ................................... */
/*-no- ................................................................ */
/*-no- ...................................................................... */
/*-no- ................................................................. */
/*-no- ................................................................................ */
/*-no- ............................... */
/*-no- .............................. */
/*-no- ........................................................... */
/*-no- ....... */
/*-no- .......................... */
/*-no- .................................... */
/*-no- ............ */
/*-no- ..... */
/*-no-  */
/*-no- ................................................ */
/*-no- .................................. */
/*-no- ............................................................. */
/*-no- ................................... */
/*-no- ................................................................ */
/*-no- ...................................................................... */
/*-no- ............................................................. */
/*-no- ......................................................................... */
/*-no- .................................................................. */
/*-no- ................................................................. */
/*-no- ...................................................................... */
/*-no- ......................... */
/*-no- ............................... */
/*-no- .............................. */
/*-no- .............................................. */
/*-no- ....... */
/*-no- .......................... */
/*-no- .................................... */
/*-no- ............ */
/*-no- ..... */
/*-no-  */
/*-no- #if defined(OS_MACOSX) */
/*-no- ........................................... */
/*-no- ........................................................... */
/*-no- ............................................................................... */
/*-no- ............ */
/*-no- ..... */
/*-no- #endif */
/*-no-  */
/*-no- .......................................................... */
/*-no- ............................................................... */
/*-no- .................................. */
/*-no- ............ */
/*-no- ..... */
/*-no- ..................................................... */
/*-no- ............................................................................ */
/*-no-  */
/*-no- ......................................................... */
/*-no- ............................................................... */
/*-no- ............................................................................ */
/*-no- ..................................................................... */
/*-no- ................ */
/*-no- ......... */
/*-no- ....... */
/*-no- ............ */
/*-no- ..... */
/*-no- ............ */
/*-no- ................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ................................................................. */
/*-no- ..................... */
/*-no- .............................................. */
/*-no-  */
/*-no- ..................................................................... */
/*-no- ................................... */
/*-no- ............................................................................ */
/*-no-  */
/*-no- .......................................................................... */
/*-no- ........................................................... */
/*-no- ........................................ */
/*-no- .................................................. */
/*-no- ............................................. */
/*-no- . */
/*-no-  */
/*-no- ................................................................... */
/*-no- ..................................................... */
/*-no- ........................................ */
/*-no- . */
/*-no-  */
/*-no- ............................................. */
/*-no- ..................................................... */
/*-no-  */
/*-no- ................................................... */
/*-no- .................... */
/*-no- ........................................................ */
/*-no-  */
/*-no- ...................................................... */
/*-no- ...................................... */
/*-no- ................................................ */
/*-no- .............................................................. */
/*-no- ............................................. */
/*-no- ....................................... */
/*-no- ........................................................... */
/*-no- ............................................................ */
/*-no- ................................................... */
/*-no-  */
/*-no- ............................................................... */
/*-no- .................................................................... */
/*-no-  */
/*-no- ................................................................. */
/*-no- ............................................. */
/*-no- ......................... */
/*-no- ............................................................................. */
/*-no- .................................... */
/*-no- ....................................................................... */
/*-no- ....................................................................... */
/*-no- ..... */
/*-no- ................................. */
/*-no-  */
/*-no- ......................................................................... */
/*-no- ... */
/*-no- . */
/*-no-  */
/*-no- #if defined(OS_LINUX) */
/*-no- ......... */
/*-no- ............................................ */
/*-no- ............................. */
/*-no- ......................... */
/*-no- .......................................... */
/*-no- ............................................... */
/*-no- ................. */
/*-no- . */
/*-no- #endif */

#endif
/* CONSBENCH file end */
