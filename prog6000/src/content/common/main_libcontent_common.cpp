/* CONSBENCH file begin */
#ifndef MAIN_LIBCONTENT_COMMON_CPP
#define MAIN_LIBCONTENT_COMMON_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, content_common_main_libcontent_common_cpp, "content/common/main_libcontent_common.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(content_common_main_libcontent_common_cpp, "content/common/main_libcontent_common.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_content_common_child_process_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_child_thread_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_common_param_traits_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_content_message_generator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_content_constants_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_content_switches_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_file_system_file_system_dispatcher_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_file_system_webfilesystem_callback_dispatcher_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_file_system_webfilesystem_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_file_system_webfilewriter_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_message_router_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_notification_details_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_notification_registrar_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_notification_service_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_notification_source_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_p2p_sockets_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_resource_dispatcher_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_resource_response_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_content_common_socket_stream_dispatcher_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_content_common_main_libcontent_common_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_content_common_child_process_cpp(it);
  PUBLIC_content_common_child_thread_cpp(it);
  PUBLIC_content_common_common_param_traits_cpp(it);
  PUBLIC_content_common_content_message_generator_cpp(it);
  PUBLIC_content_common_content_constants_cpp(it);
  PUBLIC_content_common_content_switches_cpp(it);
  PUBLIC_content_common_file_system_file_system_dispatcher_cpp(it);
  PUBLIC_content_common_file_system_webfilesystem_callback_dispatcher_cpp(it);
  PUBLIC_content_common_file_system_webfilesystem_impl_cpp(it);
  PUBLIC_content_common_file_system_webfilewriter_impl_cpp(it);
  PUBLIC_content_common_message_router_cpp(it);
  PUBLIC_content_common_notification_details_cpp(it);
  PUBLIC_content_common_notification_registrar_cpp(it);
  PUBLIC_content_common_notification_service_cpp(it);
  PUBLIC_content_common_notification_source_cpp(it);
  PUBLIC_content_common_p2p_sockets_cpp(it);
  PUBLIC_content_common_resource_dispatcher_cpp(it);
  PUBLIC_content_common_resource_response_cpp(it);
  PUBLIC_content_common_socket_stream_dispatcher_cpp(it);
}

#endif

