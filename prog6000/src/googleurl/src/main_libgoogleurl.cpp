/* CONSBENCH file begin */
#ifndef MAIN_LIBGOOGLEURL_CPP
#define MAIN_LIBGOOGLEURL_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, googleurl_src_main_libgoogleurl_cpp, "googleurl/src/main_libgoogleurl.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(googleurl_src_main_libgoogleurl_cpp, "googleurl/src/main_libgoogleurl.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_googleurl_src_gurl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_googleurl_src_url_canon_etc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_googleurl_src_url_canon_fileurl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_googleurl_src_url_canon_host_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_googleurl_src_url_canon_icu_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_googleurl_src_url_canon_internal_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_googleurl_src_url_canon_ip_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_googleurl_src_url_canon_mailtourl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_googleurl_src_url_canon_path_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_googleurl_src_url_canon_pathurl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_googleurl_src_url_canon_query_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_googleurl_src_url_canon_relative_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_googleurl_src_url_canon_stdurl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_googleurl_src_url_parse_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_googleurl_src_url_parse_file_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_googleurl_src_url_util_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_googleurl_src_main_libgoogleurl_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_googleurl_src_gurl_cpp(it);
  PUBLIC_googleurl_src_url_canon_etc_cpp(it);
  PUBLIC_googleurl_src_url_canon_fileurl_cpp(it);
  PUBLIC_googleurl_src_url_canon_host_cpp(it);
  PUBLIC_googleurl_src_url_canon_icu_cpp(it);
  PUBLIC_googleurl_src_url_canon_internal_cpp(it);
  PUBLIC_googleurl_src_url_canon_ip_cpp(it);
  PUBLIC_googleurl_src_url_canon_mailtourl_cpp(it);
  PUBLIC_googleurl_src_url_canon_path_cpp(it);
  PUBLIC_googleurl_src_url_canon_pathurl_cpp(it);
  PUBLIC_googleurl_src_url_canon_query_cpp(it);
  PUBLIC_googleurl_src_url_canon_relative_cpp(it);
  PUBLIC_googleurl_src_url_canon_stdurl_cpp(it);
  PUBLIC_googleurl_src_url_parse_cpp(it);
  PUBLIC_googleurl_src_url_parse_file_cpp(it);
  PUBLIC_googleurl_src_url_util_cpp(it);
}

#endif

