/* CONSBENCH file begin */
#ifndef GL2_H_3034
#define GL2_H_3034

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, gpu_GLES2_gl2_h, "gpu/GLES2/gl2.h");

/* CONSBENCH includes begin */
#include <GLES2/gl2platform.h>
#include "../command_buffer/client/gles2_lib.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(gpu_GLES2_gl2_h, "gpu/GLES2/gl2.h");

/*-no- #ifndef __gl2_h_ */
/*-no- #define __gl2_h_ */
/*-no-  */
/*-no- ................................................................. */
/*-no-  */
/*-no- #include <GLES2/gl2platform.h> */
/*-no-  */
/*-no- ... */
/*-no- .......................................................................... */
/*-no- ............................................................. */
/*-no- .... */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ........................ */
/*-no- ............................................................................ */
/*-no-  */
/*-no- ................................ */
/*-no- ................................ */
/*-no- ................................... */
/*-no- .................................... */
/*-no- ................................ */
/*-no- ................................. */
/*-no- ............................... */
/*-no- ................................. */
/*-no- ................................. */
/*-no- .................................. */
/*-no- ................................ */
/*-no- ................................. */
/*-no- .................................. */
/*-no- ................................. */
/*-no- ................................ */
/*-no-  */
/*-no- ......................................................... */
/*-no- .................................. */
/*-no- .................................... */
/*-no-  */
/*-no- ............................... */
/*-no- #define GL_ES_VERSION_2_0                 1 */
/*-no-  */
/*-no- ....................... */
/*-no- #define GL_DEPTH_BUFFER_BIT               0x00000100 */
/*-no- #define GL_STENCIL_BUFFER_BIT             0x00000400 */
/*-no- #define GL_COLOR_BUFFER_BIT               0x00004000 */
/*-no-  */
/*-no- ............... */
/*-no- #define GL_FALSE                          0 */
/*-no- #define GL_TRUE                           1 */
/*-no-  */
/*-no- ................. */
/*-no- #define GL_POINTS                         0x0000 */
/*-no- #define GL_LINES                          0x0001 */
/*-no- #define GL_LINE_LOOP                      0x0002 */
/*-no- #define GL_LINE_STRIP                     0x0003 */
/*-no- #define GL_TRIANGLES                      0x0004 */
/*-no- #define GL_TRIANGLE_STRIP                 0x0005 */
/*-no- #define GL_TRIANGLE_FAN                   0x0006 */
/*-no-  */
/*-no- ............................................. */
/*-no- ..................... */
/*-no- .................... */
/*-no- ..................... */
/*-no- ...................... */
/*-no- ....................... */
/*-no- ........................ */
/*-no- ...................... */
/*-no- ...................... */
/*-no-  */
/*-no- .......................... */
/*-no- #define GL_ZERO                           0 */
/*-no- #define GL_ONE                            1 */
/*-no- #define GL_SRC_COLOR                      0x0300 */
/*-no- #define GL_ONE_MINUS_SRC_COLOR            0x0301 */
/*-no- #define GL_SRC_ALPHA                      0x0302 */
/*-no- #define GL_ONE_MINUS_SRC_ALPHA            0x0303 */
/*-no- #define GL_DST_ALPHA                      0x0304 */
/*-no- #define GL_ONE_MINUS_DST_ALPHA            0x0305 */
/*-no-  */
/*-no- ......................... */
/*-no- .................... */
/*-no- ................... */
/*-no- #define GL_DST_COLOR                      0x0306 */
/*-no- #define GL_ONE_MINUS_DST_COLOR            0x0307 */
/*-no- #define GL_SRC_ALPHA_SATURATE             0x0308 */
/*-no- ......................... */
/*-no- ................................... */
/*-no- ......................... */
/*-no- ................................... */
/*-no-  */
/*-no- ............................. */
/*-no- #define GL_FUNC_ADD                       0x8006 */
/*-no- #define GL_BLEND_EQUATION                 0x8009 */
/*-no- #define GL_BLEND_EQUATION_RGB             0x8009    /-* same as BLEND_EQUATION *-/ */
/*-no- #define GL_BLEND_EQUATION_ALPHA           0x883D */
/*-no-  */
/*-no- ..................... */
/*-no- #define GL_FUNC_SUBTRACT                  0x800A */
/*-no- #define GL_FUNC_REVERSE_SUBTRACT          0x800B */
/*-no-  */
/*-no- ................................ */
/*-no- #define GL_BLEND_DST_RGB                  0x80C8 */
/*-no- #define GL_BLEND_SRC_RGB                  0x80C9 */
/*-no- #define GL_BLEND_DST_ALPHA                0x80CA */
/*-no- #define GL_BLEND_SRC_ALPHA                0x80CB */
/*-no- #define GL_CONSTANT_COLOR                 0x8001 */
/*-no- #define GL_ONE_MINUS_CONSTANT_COLOR       0x8002 */
/*-no- #define GL_CONSTANT_ALPHA                 0x8003 */
/*-no- #define GL_ONE_MINUS_CONSTANT_ALPHA       0x8004 */
/*-no- #define GL_BLEND_COLOR                    0x8005 */
/*-no-  */
/*-no- ...................... */
/*-no- #define GL_ARRAY_BUFFER                   0x8892 */
/*-no- #define GL_ELEMENT_ARRAY_BUFFER           0x8893 */
/*-no- #define GL_ARRAY_BUFFER_BINDING           0x8894 */
/*-no- #define GL_ELEMENT_ARRAY_BUFFER_BINDING   0x8895 */
/*-no-  */
/*-no- #define GL_STREAM_DRAW                    0x88E0 */
/*-no- #define GL_STATIC_DRAW                    0x88E4 */
/*-no- #define GL_DYNAMIC_DRAW                   0x88E8 */
/*-no-  */
/*-no- #define GL_BUFFER_SIZE                    0x8764 */
/*-no- #define GL_BUFFER_USAGE                   0x8765 */
/*-no-  */
/*-no- #define GL_CURRENT_VERTEX_ATTRIB          0x8626 */
/*-no-  */
/*-no- .................... */
/*-no- #define GL_FRONT                          0x0404 */
/*-no- #define GL_BACK                           0x0405 */
/*-no- #define GL_FRONT_AND_BACK                 0x0408 */
/*-no-  */
/*-no- ..................... */
/*-no- ..................... */
/*-no- .................... */
/*-no- ..................... */
/*-no- ...................... */
/*-no- ....................... */
/*-no- ........................ */
/*-no- ...................... */
/*-no- ...................... */
/*-no-  */
/*-no- ................. */
/*-no- #define GL_TEXTURE_2D                     0x0DE1 */
/*-no- #define GL_CULL_FACE                      0x0B44 */
/*-no- #define GL_BLEND                          0x0BE2 */
/*-no- #define GL_DITHER                         0x0BD0 */
/*-no- #define GL_STENCIL_TEST                   0x0B90 */
/*-no- #define GL_DEPTH_TEST                     0x0B71 */
/*-no- #define GL_SCISSOR_TEST                   0x0C11 */
/*-no- #define GL_POLYGON_OFFSET_FILL            0x8037 */
/*-no- #define GL_SAMPLE_ALPHA_TO_COVERAGE       0x809E */
/*-no- #define GL_SAMPLE_COVERAGE                0x80A0 */
/*-no-  */
/*-no- ................. */
/*-no- #define GL_NO_ERROR                       0 */
/*-no- #define GL_INVALID_ENUM                   0x0500 */
/*-no- #define GL_INVALID_VALUE                  0x0501 */
/*-no- #define GL_INVALID_OPERATION              0x0502 */
/*-no- #define GL_OUT_OF_MEMORY                  0x0505 */
/*-no- #define GL_CONTEXT_LOST                   0x300E  // TODO(gman): What value? */
/*-no-  */
/*-no- .......................... */
/*-no- #define GL_CW                             0x0900 */
/*-no- #define GL_CCW                            0x0901 */
/*-no-  */
/*-no- ................ */
/*-no- #define GL_LINE_WIDTH                     0x0B21 */
/*-no- #define GL_ALIASED_POINT_SIZE_RANGE       0x846D */
/*-no- #define GL_ALIASED_LINE_WIDTH_RANGE       0x846E */
/*-no- #define GL_CULL_FACE_MODE                 0x0B45 */
/*-no- #define GL_FRONT_FACE                     0x0B46 */
/*-no- #define GL_DEPTH_RANGE                    0x0B70 */
/*-no- #define GL_DEPTH_WRITEMASK                0x0B72 */
/*-no- #define GL_DEPTH_CLEAR_VALUE              0x0B73 */
/*-no- #define GL_DEPTH_FUNC                     0x0B74 */
/*-no- #define GL_STENCIL_CLEAR_VALUE            0x0B91 */
/*-no- #define GL_STENCIL_FUNC                   0x0B92 */
/*-no- #define GL_STENCIL_FAIL                   0x0B94 */
/*-no- #define GL_STENCIL_PASS_DEPTH_FAIL        0x0B95 */
/*-no- #define GL_STENCIL_PASS_DEPTH_PASS        0x0B96 */
/*-no- #define GL_STENCIL_REF                    0x0B97 */
/*-no- #define GL_STENCIL_VALUE_MASK             0x0B93 */
/*-no- #define GL_STENCIL_WRITEMASK              0x0B98 */
/*-no- #define GL_STENCIL_BACK_FUNC              0x8800 */
/*-no- #define GL_STENCIL_BACK_FAIL              0x8801 */
/*-no- #define GL_STENCIL_BACK_PASS_DEPTH_FAIL   0x8802 */
/*-no- #define GL_STENCIL_BACK_PASS_DEPTH_PASS   0x8803 */
/*-no- #define GL_STENCIL_BACK_REF               0x8CA3 */
/*-no- #define GL_STENCIL_BACK_VALUE_MASK        0x8CA4 */
/*-no- #define GL_STENCIL_BACK_WRITEMASK         0x8CA5 */
/*-no- #define GL_VIEWPORT                       0x0BA2 */
/*-no- #define GL_SCISSOR_BOX                    0x0C10 */
/*-no- ............................ */
/*-no- #define GL_COLOR_CLEAR_VALUE              0x0C22 */
/*-no- #define GL_COLOR_WRITEMASK                0x0C23 */
/*-no- #define GL_UNPACK_ALIGNMENT               0x0CF5 */
/*-no- #define GL_PACK_ALIGNMENT                 0x0D05 */
/*-no- #define GL_MAX_TEXTURE_SIZE               0x0D33 */
/*-no- #define GL_MAX_VIEWPORT_DIMS              0x0D3A */
/*-no- #define GL_SUBPIXEL_BITS                  0x0D50 */
/*-no- #define GL_RED_BITS                       0x0D52 */
/*-no- #define GL_GREEN_BITS                     0x0D53 */
/*-no- #define GL_BLUE_BITS                      0x0D54 */
/*-no- #define GL_ALPHA_BITS                     0x0D55 */
/*-no- #define GL_DEPTH_BITS                     0x0D56 */
/*-no- #define GL_STENCIL_BITS                   0x0D57 */
/*-no- #define GL_POLYGON_OFFSET_UNITS           0x2A00 */
/*-no- ................................... */
/*-no- #define GL_POLYGON_OFFSET_FACTOR          0x8038 */
/*-no- #define GL_TEXTURE_BINDING_2D             0x8069 */
/*-no- #define GL_SAMPLE_BUFFERS                 0x80A8 */
/*-no- #define GL_SAMPLES                        0x80A9 */
/*-no- #define GL_SAMPLE_COVERAGE_VALUE          0x80AA */
/*-no- #define GL_SAMPLE_COVERAGE_INVERT         0x80AB */
/*-no-  */
/*-no- ........................... */
/*-no- .................................. */
/*-no- .................................. */
/*-no- .............................. */
/*-no- .............................. */
/*-no-  */
/*-no- #define GL_NUM_COMPRESSED_TEXTURE_FORMATS 0x86A2 */
/*-no- #define GL_COMPRESSED_TEXTURE_FORMATS     0x86A3 */
/*-no-  */
/*-no- ................ */
/*-no- #define GL_DONT_CARE                      0x1100 */
/*-no- #define GL_FASTEST                        0x1101 */
/*-no- #define GL_NICEST                         0x1102 */
/*-no-  */
/*-no- .................. */
/*-no- #define GL_GENERATE_MIPMAP_HINT            0x8192 */
/*-no-  */
/*-no- ................ */
/*-no- #define GL_BYTE                           0x1400 */
/*-no- #define GL_UNSIGNED_BYTE                  0x1401 */
/*-no- #define GL_SHORT                          0x1402 */
/*-no- #define GL_UNSIGNED_SHORT                 0x1403 */
/*-no- #define GL_INT                            0x1404 */
/*-no- #define GL_UNSIGNED_INT                   0x1405 */
/*-no- #define GL_FLOAT                          0x1406 */
/*-no- #define GL_FIXED                          0x140C */
/*-no-  */
/*-no- ................... */
/*-no- #define GL_DEPTH_COMPONENT                0x1902 */
/*-no- #define GL_ALPHA                          0x1906 */
/*-no- #define GL_RGB                            0x1907 */
/*-no- #define GL_RGBA                           0x1908 */
/*-no- #define GL_LUMINANCE                      0x1909 */
/*-no- #define GL_LUMINANCE_ALPHA                0x190A */
/*-no-  */
/*-no- ................. */
/*-no- ............................. */
/*-no- #define GL_UNSIGNED_SHORT_4_4_4_4         0x8033 */
/*-no- #define GL_UNSIGNED_SHORT_5_5_5_1         0x8034 */
/*-no- #define GL_UNSIGNED_SHORT_5_6_5           0x8363 */
/*-no-  */
/*-no- ............... */
/*-no- #define GL_FRAGMENT_SHADER                  0x8B30 */
/*-no- #define GL_VERTEX_SHADER                    0x8B31 */
/*-no- #define GL_MAX_VERTEX_ATTRIBS               0x8869 */
/*-no- #define GL_MAX_VERTEX_UNIFORM_VECTORS       0x8DFB */
/*-no- #define GL_MAX_VARYING_VECTORS              0x8DFC */
/*-no- #define GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS 0x8B4D */
/*-no- #define GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS   0x8B4C */
/*-no- #define GL_MAX_TEXTURE_IMAGE_UNITS          0x8872 */
/*-no- #define GL_MAX_FRAGMENT_UNIFORM_VECTORS     0x8DFD */
/*-no- #define GL_SHADER_TYPE                      0x8B4F */
/*-no- #define GL_DELETE_STATUS                    0x8B80 */
/*-no- #define GL_LINK_STATUS                      0x8B82 */
/*-no- #define GL_VALIDATE_STATUS                  0x8B83 */
/*-no- #define GL_ATTACHED_SHADERS                 0x8B85 */
/*-no- #define GL_ACTIVE_UNIFORMS                  0x8B86 */
/*-no- #define GL_ACTIVE_UNIFORM_MAX_LENGTH        0x8B87 */
/*-no- #define GL_ACTIVE_ATTRIBUTES                0x8B89 */
/*-no- #define GL_ACTIVE_ATTRIBUTE_MAX_LENGTH      0x8B8A */
/*-no- #define GL_SHADING_LANGUAGE_VERSION         0x8B8C */
/*-no- #define GL_CURRENT_PROGRAM                  0x8B8D */
/*-no-  */
/*-no- ....................... */
/*-no- #define GL_NEVER                          0x0200 */
/*-no- #define GL_LESS                           0x0201 */
/*-no- #define GL_EQUAL                          0x0202 */
/*-no- #define GL_LEQUAL                         0x0203 */
/*-no- #define GL_GREATER                        0x0204 */
/*-no- #define GL_NOTEQUAL                       0x0205 */
/*-no- #define GL_GEQUAL                         0x0206 */
/*-no- #define GL_ALWAYS                         0x0207 */
/*-no-  */
/*-no- ................. */
/*-no- .................... */
/*-no- #define GL_KEEP                           0x1E00 */
/*-no- #define GL_REPLACE                        0x1E01 */
/*-no- #define GL_INCR                           0x1E02 */
/*-no- #define GL_DECR                           0x1E03 */
/*-no- #define GL_INVERT                         0x150A */
/*-no- #define GL_INCR_WRAP                      0x8507 */
/*-no- #define GL_DECR_WRAP                      0x8508 */
/*-no-  */
/*-no- .................. */
/*-no- #define GL_VENDOR                         0x1F00 */
/*-no- #define GL_RENDERER                       0x1F01 */
/*-no- #define GL_VERSION                        0x1F02 */
/*-no- #define GL_EXTENSIONS                     0x1F03 */
/*-no-  */
/*-no- ........................ */
/*-no- #define GL_NEAREST                        0x2600 */
/*-no- #define GL_LINEAR                         0x2601 */
/*-no-  */
/*-no- ........................ */
/*-no- ....................... */
/*-no- ...................... */
/*-no- #define GL_NEAREST_MIPMAP_NEAREST         0x2700 */
/*-no- #define GL_LINEAR_MIPMAP_NEAREST          0x2701 */
/*-no- #define GL_NEAREST_MIPMAP_LINEAR          0x2702 */
/*-no- #define GL_LINEAR_MIPMAP_LINEAR           0x2703 */
/*-no-  */
/*-no- ............................ */
/*-no- #define GL_TEXTURE_MAG_FILTER             0x2800 */
/*-no- #define GL_TEXTURE_MIN_FILTER             0x2801 */
/*-no- #define GL_TEXTURE_WRAP_S                 0x2802 */
/*-no- #define GL_TEXTURE_WRAP_T                 0x2803 */
/*-no-  */
/*-no- ..................... */
/*-no- .......................... */
/*-no- #define GL_TEXTURE                        0x1702 */
/*-no-  */
/*-no- #define GL_TEXTURE_CUBE_MAP               0x8513 */
/*-no- #define GL_TEXTURE_BINDING_CUBE_MAP       0x8514 */
/*-no- #define GL_TEXTURE_CUBE_MAP_POSITIVE_X    0x8515 */
/*-no- #define GL_TEXTURE_CUBE_MAP_NEGATIVE_X    0x8516 */
/*-no- #define GL_TEXTURE_CUBE_MAP_POSITIVE_Y    0x8517 */
/*-no- #define GL_TEXTURE_CUBE_MAP_NEGATIVE_Y    0x8518 */
/*-no- #define GL_TEXTURE_CUBE_MAP_POSITIVE_Z    0x8519 */
/*-no- #define GL_TEXTURE_CUBE_MAP_NEGATIVE_Z    0x851A */
/*-no- #define GL_MAX_CUBE_MAP_TEXTURE_SIZE      0x851C */
/*-no-  */
/*-no- ................... */
/*-no- #define GL_TEXTURE0                       0x84C0 */
/*-no- #define GL_TEXTURE1                       0x84C1 */
/*-no- #define GL_TEXTURE2                       0x84C2 */
/*-no- #define GL_TEXTURE3                       0x84C3 */
/*-no- #define GL_TEXTURE4                       0x84C4 */
/*-no- #define GL_TEXTURE5                       0x84C5 */
/*-no- #define GL_TEXTURE6                       0x84C6 */
/*-no- #define GL_TEXTURE7                       0x84C7 */
/*-no- #define GL_TEXTURE8                       0x84C8 */
/*-no- #define GL_TEXTURE9                       0x84C9 */
/*-no- #define GL_TEXTURE10                      0x84CA */
/*-no- #define GL_TEXTURE11                      0x84CB */
/*-no- #define GL_TEXTURE12                      0x84CC */
/*-no- #define GL_TEXTURE13                      0x84CD */
/*-no- #define GL_TEXTURE14                      0x84CE */
/*-no- #define GL_TEXTURE15                      0x84CF */
/*-no- #define GL_TEXTURE16                      0x84D0 */
/*-no- #define GL_TEXTURE17                      0x84D1 */
/*-no- #define GL_TEXTURE18                      0x84D2 */
/*-no- #define GL_TEXTURE19                      0x84D3 */
/*-no- #define GL_TEXTURE20                      0x84D4 */
/*-no- #define GL_TEXTURE21                      0x84D5 */
/*-no- #define GL_TEXTURE22                      0x84D6 */
/*-no- #define GL_TEXTURE23                      0x84D7 */
/*-no- #define GL_TEXTURE24                      0x84D8 */
/*-no- #define GL_TEXTURE25                      0x84D9 */
/*-no- #define GL_TEXTURE26                      0x84DA */
/*-no- #define GL_TEXTURE27                      0x84DB */
/*-no- #define GL_TEXTURE28                      0x84DC */
/*-no- #define GL_TEXTURE29                      0x84DD */
/*-no- #define GL_TEXTURE30                      0x84DE */
/*-no- #define GL_TEXTURE31                      0x84DF */
/*-no- #define GL_ACTIVE_TEXTURE                 0x84E0 */
/*-no-  */
/*-no- ....................... */
/*-no- #define GL_REPEAT                         0x2901 */
/*-no- #define GL_CLAMP_TO_EDGE                  0x812F */
/*-no- #define GL_MIRRORED_REPEAT                0x8370 */
/*-no-  */
/*-no- ..................... */
/*-no- #define GL_FLOAT_VEC2                     0x8B50 */
/*-no- #define GL_FLOAT_VEC3                     0x8B51 */
/*-no- #define GL_FLOAT_VEC4                     0x8B52 */
/*-no- #define GL_INT_VEC2                       0x8B53 */
/*-no- #define GL_INT_VEC3                       0x8B54 */
/*-no- #define GL_INT_VEC4                       0x8B55 */
/*-no- #define GL_BOOL                           0x8B56 */
/*-no- #define GL_BOOL_VEC2                      0x8B57 */
/*-no- #define GL_BOOL_VEC3                      0x8B58 */
/*-no- #define GL_BOOL_VEC4                      0x8B59 */
/*-no- #define GL_FLOAT_MAT2                     0x8B5A */
/*-no- #define GL_FLOAT_MAT3                     0x8B5B */
/*-no- #define GL_FLOAT_MAT4                     0x8B5C */
/*-no- #define GL_SAMPLER_2D                     0x8B5E */
/*-no- #define GL_SAMPLER_CUBE                   0x8B60 */
/*-no-  */
/*-no- ..................... */
/*-no- #define GL_VERTEX_ATTRIB_ARRAY_ENABLED        0x8622 */
/*-no- #define GL_VERTEX_ATTRIB_ARRAY_SIZE           0x8623 */
/*-no- #define GL_VERTEX_ATTRIB_ARRAY_STRIDE         0x8624 */
/*-no- #define GL_VERTEX_ATTRIB_ARRAY_TYPE           0x8625 */
/*-no- #define GL_VERTEX_ATTRIB_ARRAY_NORMALIZED     0x886A */
/*-no- #define GL_VERTEX_ATTRIB_ARRAY_POINTER        0x8645 */
/*-no- #define GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING 0x889F */
/*-no-  */
/*-no- ................... */
/*-no- #define GL_IMPLEMENTATION_COLOR_READ_TYPE   0x8B9A */
/*-no- #define GL_IMPLEMENTATION_COLOR_READ_FORMAT 0x8B9B */
/*-no-  */
/*-no- ..................... */
/*-no- #define GL_COMPILE_STATUS                 0x8B81 */
/*-no- #define GL_INFO_LOG_LENGTH                0x8B84 */
/*-no- #define GL_SHADER_SOURCE_LENGTH           0x8B88 */
/*-no- #define GL_SHADER_COMPILER                0x8DFA */
/*-no-  */
/*-no- ..................... */
/*-no- #define GL_SHADER_BINARY_FORMATS          0x8DF8 */
/*-no- #define GL_NUM_SHADER_BINARY_FORMATS      0x8DF9 */
/*-no-  */
/*-no- ........................................ */
/*-no- #define GL_LOW_FLOAT                      0x8DF0 */
/*-no- #define GL_MEDIUM_FLOAT                   0x8DF1 */
/*-no- #define GL_HIGH_FLOAT                     0x8DF2 */
/*-no- #define GL_LOW_INT                        0x8DF3 */
/*-no- #define GL_MEDIUM_INT                     0x8DF4 */
/*-no- #define GL_HIGH_INT                       0x8DF5 */
/*-no-  */
/*-no- ........................... */
/*-no- #define GL_FRAMEBUFFER                    0x8D40 */
/*-no- #define GL_RENDERBUFFER                   0x8D41 */
/*-no-  */
/*-no- #define GL_RGBA4                          0x8056 */
/*-no- #define GL_RGB5_A1                        0x8057 */
/*-no- #define GL_RGB565                         0x8D62 */
/*-no- #define GL_DEPTH_COMPONENT16              0x81A5 */
/*-no- #define GL_STENCIL_INDEX                  0x1901 */
/*-no- #define GL_STENCIL_INDEX8                 0x8D48 */
/*-no-  */
/*-no- #define GL_RENDERBUFFER_WIDTH             0x8D42 */
/*-no- #define GL_RENDERBUFFER_HEIGHT            0x8D43 */
/*-no- #define GL_RENDERBUFFER_INTERNAL_FORMAT   0x8D44 */
/*-no- #define GL_RENDERBUFFER_RED_SIZE          0x8D50 */
/*-no- #define GL_RENDERBUFFER_GREEN_SIZE        0x8D51 */
/*-no- #define GL_RENDERBUFFER_BLUE_SIZE         0x8D52 */
/*-no- #define GL_RENDERBUFFER_ALPHA_SIZE        0x8D53 */
/*-no- #define GL_RENDERBUFFER_DEPTH_SIZE        0x8D54 */
/*-no- #define GL_RENDERBUFFER_STENCIL_SIZE      0x8D55 */
/*-no-  */
/*-no- #define GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE           0x8CD0 */
/*-no- #define GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME           0x8CD1 */
/*-no- #define GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL         0x8CD2 */
/*-no- #define GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE 0x8CD3 */
/*-no-  */
/*-no- #define GL_COLOR_ATTACHMENT0              0x8CE0 */
/*-no- #define GL_DEPTH_ATTACHMENT               0x8D00 */
/*-no- #define GL_STENCIL_ATTACHMENT             0x8D20 */
/*-no-  */
/*-no- #define GL_NONE                           0 */
/*-no-  */
/*-no- #define GL_FRAMEBUFFER_COMPLETE                      0x8CD5 */
/*-no- #define GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT         0x8CD6 */
/*-no- #define GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT 0x8CD7 */
/*-no- #define GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS         0x8CD9 */
/*-no- #define GL_FRAMEBUFFER_UNSUPPORTED                   0x8CDD */
/*-no-  */
/*-no- #define GL_FRAMEBUFFER_BINDING            0x8CA6 */
/*-no- #define GL_RENDERBUFFER_BINDING           0x8CA7 */
/*-no- #define GL_MAX_RENDERBUFFER_SIZE          0x84E8 */
/*-no-  */
/*-no- #define GL_INVALID_FRAMEBUFFER_OPERATION  0x0506 */
/*-no-  */
/*-no- ......................................................... */
/*-no- .......................................................................... */
/*-no- ....................................................................... */
/*-no- ............................................................................... */
/*-no- ............................................................................... */
/*-no- ...................................................................... */
/*-no- .......... */
/*-no-  */
/*-no- #if defined(__cplusplus) && defined(GLES2_INLINE_OPTIMIZATION) */
/*-no- #include "../command_buffer/client/gles2_lib.h" */
/*-no- #define GLES2_USE_CPP_BINDINGS */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(GLES2_USE_CPP_BINDINGS) */
/*-no- #define GLES2_GET_FUN(name) gles2::GetGLContext()->name */
/*-no- #else */
/*-no- #define GLES2_GET_FUN(name) GLES2 ## name */
/*-no- #endif */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ..................... */
/*-no- ............................................................................ */
/*-no- #undef GL_APICALL */
/*-no- #define GL_APICALL */
/*-no- #undef GL_APIENTRY */
/*-no- #define GL_APIENTRY */
/*-no-  */
/*-no- #define glActiveTexture GLES2_GET_FUN(ActiveTexture) */
/*-no- #define glAttachShader GLES2_GET_FUN(AttachShader) */
/*-no- #define glBindAttribLocation GLES2_GET_FUN(BindAttribLocation) */
/*-no- #define glBindBuffer GLES2_GET_FUN(BindBuffer) */
/*-no- #define glBindFramebuffer GLES2_GET_FUN(BindFramebuffer) */
/*-no- #define glBindRenderbuffer GLES2_GET_FUN(BindRenderbuffer) */
/*-no- #define glBindTexture GLES2_GET_FUN(BindTexture) */
/*-no- #define glBlendColor GLES2_GET_FUN(BlendColor) */
/*-no- #define glBlendEquation GLES2_GET_FUN(BlendEquation) */
/*-no- #define glBlendEquationSeparate GLES2_GET_FUN(BlendEquationSeparate) */
/*-no- #define glBlendFunc GLES2_GET_FUN(BlendFunc) */
/*-no- #define glBlendFuncSeparate GLES2_GET_FUN(BlendFuncSeparate) */
/*-no- #define glBufferData GLES2_GET_FUN(BufferData) */
/*-no- #define glBufferSubData GLES2_GET_FUN(BufferSubData) */
/*-no- #define glCheckFramebufferStatus GLES2_GET_FUN(CheckFramebufferStatus) */
/*-no- #define glClear GLES2_GET_FUN(Clear) */
/*-no- #define glClearColor GLES2_GET_FUN(ClearColor) */
/*-no- #define glClearDepthf GLES2_GET_FUN(ClearDepthf) */
/*-no- #define glClearStencil GLES2_GET_FUN(ClearStencil) */
/*-no- #define glColorMask GLES2_GET_FUN(ColorMask) */
/*-no- #define glCompileShader GLES2_GET_FUN(CompileShader) */
/*-no- #define glCompressedTexImage2D GLES2_GET_FUN(CompressedTexImage2D) */
/*-no- #define glCompressedTexSubImage2D GLES2_GET_FUN(CompressedTexSubImage2D) */
/*-no- #define glCopyTexImage2D GLES2_GET_FUN(CopyTexImage2D) */
/*-no- #define glCopyTexSubImage2D GLES2_GET_FUN(CopyTexSubImage2D) */
/*-no- #define glCreateProgram GLES2_GET_FUN(CreateProgram) */
/*-no- #define glCreateShader GLES2_GET_FUN(CreateShader) */
/*-no- #define glCullFace GLES2_GET_FUN(CullFace) */
/*-no- #define glDeleteBuffers GLES2_GET_FUN(DeleteBuffers) */
/*-no- #define glDeleteFramebuffers GLES2_GET_FUN(DeleteFramebuffers) */
/*-no- #define glDeleteProgram GLES2_GET_FUN(DeleteProgram) */
/*-no- #define glDeleteRenderbuffers GLES2_GET_FUN(DeleteRenderbuffers) */
/*-no- #define glDeleteShader GLES2_GET_FUN(DeleteShader) */
/*-no- #define glDeleteTextures GLES2_GET_FUN(DeleteTextures) */
/*-no- #define glDepthFunc GLES2_GET_FUN(DepthFunc) */
/*-no- #define glDepthMask GLES2_GET_FUN(DepthMask) */
/*-no- #define glDepthRangef GLES2_GET_FUN(DepthRangef) */
/*-no- #define glDetachShader GLES2_GET_FUN(DetachShader) */
/*-no- #define glDisable GLES2_GET_FUN(Disable) */
/*-no- #define glDisableVertexAttribArray GLES2_GET_FUN(DisableVertexAttribArray) */
/*-no- #define glDrawArrays GLES2_GET_FUN(DrawArrays) */
/*-no- #define glDrawElements GLES2_GET_FUN(DrawElements) */
/*-no- #define glEnable GLES2_GET_FUN(Enable) */
/*-no- #define glEnableVertexAttribArray GLES2_GET_FUN(EnableVertexAttribArray) */
/*-no- #define glFinish GLES2_GET_FUN(Finish) */
/*-no- #define glFlush GLES2_GET_FUN(Flush) */
/*-no- #define glFramebufferRenderbuffer GLES2_GET_FUN(FramebufferRenderbuffer) */
/*-no- #define glFramebufferTexture2D GLES2_GET_FUN(FramebufferTexture2D) */
/*-no- #define glFrontFace GLES2_GET_FUN(FrontFace) */
/*-no- #define glGenBuffers GLES2_GET_FUN(GenBuffers) */
/*-no- #define glGenerateMipmap GLES2_GET_FUN(GenerateMipmap) */
/*-no- #define glGenFramebuffers GLES2_GET_FUN(GenFramebuffers) */
/*-no- #define glGenRenderbuffers GLES2_GET_FUN(GenRenderbuffers) */
/*-no- #define glGenTextures GLES2_GET_FUN(GenTextures) */
/*-no- #define glGetActiveAttrib GLES2_GET_FUN(GetActiveAttrib) */
/*-no- #define glGetActiveUniform GLES2_GET_FUN(GetActiveUniform) */
/*-no- #define glGetAttachedShaders GLES2_GET_FUN(GetAttachedShaders) */
/*-no- #define glGetAttribLocation GLES2_GET_FUN(GetAttribLocation) */
/*-no- #define glGetBooleanv GLES2_GET_FUN(GetBooleanv) */
/*-no- #define glGetBufferParameteriv GLES2_GET_FUN(GetBufferParameteriv) */
/*-no- #define glGetError GLES2_GET_FUN(GetError) */
/*-no- #define glGetFloatv GLES2_GET_FUN(GetFloatv) */
/*-no- #define glGetFramebufferAttachmentParameteriv GLES2_GET_FUN(GetFramebufferAttachmentParameteriv) */
/*-no- #define glGetIntegerv GLES2_GET_FUN(GetIntegerv) */
/*-no- #define glGetProgramiv GLES2_GET_FUN(GetProgramiv) */
/*-no- #define glGetProgramInfoLog GLES2_GET_FUN(GetProgramInfoLog) */
/*-no- #define glGetRenderbufferParameteriv GLES2_GET_FUN(GetRenderbufferParameteriv) */
/*-no- #define glGetShaderiv GLES2_GET_FUN(GetShaderiv) */
/*-no- #define glGetShaderInfoLog GLES2_GET_FUN(GetShaderInfoLog) */
/*-no- #define glGetShaderPrecisionFormat GLES2_GET_FUN(GetShaderPrecisionFormat) */
/*-no- #define glGetShaderSource GLES2_GET_FUN(GetShaderSource) */
/*-no- #define glGetString GLES2_GET_FUN(GetString) */
/*-no- #define glGetTexParameterfv GLES2_GET_FUN(GetTexParameterfv) */
/*-no- #define glGetTexParameteriv GLES2_GET_FUN(GetTexParameteriv) */
/*-no- #define glGetUniformfv GLES2_GET_FUN(GetUniformfv) */
/*-no- #define glGetUniformiv GLES2_GET_FUN(GetUniformiv) */
/*-no- #define glGetUniformLocation GLES2_GET_FUN(GetUniformLocation) */
/*-no- #define glGetVertexAttribfv GLES2_GET_FUN(GetVertexAttribfv) */
/*-no- #define glGetVertexAttribiv GLES2_GET_FUN(GetVertexAttribiv) */
/*-no- #define glGetVertexAttribPointerv GLES2_GET_FUN(GetVertexAttribPointerv) */
/*-no- #define glHint GLES2_GET_FUN(Hint) */
/*-no- #define glIsBuffer GLES2_GET_FUN(IsBuffer) */
/*-no- #define glIsEnabled GLES2_GET_FUN(IsEnabled) */
/*-no- #define glIsFramebuffer GLES2_GET_FUN(IsFramebuffer) */
/*-no- #define glIsProgram GLES2_GET_FUN(IsProgram) */
/*-no- #define glIsRenderbuffer GLES2_GET_FUN(IsRenderbuffer) */
/*-no- #define glIsShader GLES2_GET_FUN(IsShader) */
/*-no- #define glIsTexture GLES2_GET_FUN(IsTexture) */
/*-no- #define glLineWidth GLES2_GET_FUN(LineWidth) */
/*-no- #define glLinkProgram GLES2_GET_FUN(LinkProgram) */
/*-no- #define glPixelStorei GLES2_GET_FUN(PixelStorei) */
/*-no- #define glPolygonOffset GLES2_GET_FUN(PolygonOffset) */
/*-no- #define glReadPixels GLES2_GET_FUN(ReadPixels) */
/*-no- #define glReleaseShaderCompiler GLES2_GET_FUN(ReleaseShaderCompiler) */
/*-no- #define glRenderbufferStorage GLES2_GET_FUN(RenderbufferStorage) */
/*-no- #define glSampleCoverage GLES2_GET_FUN(SampleCoverage) */
/*-no- #define glScissor GLES2_GET_FUN(Scissor) */
/*-no- #define glShaderBinary GLES2_GET_FUN(ShaderBinary) */
/*-no- #define glShaderSource GLES2_GET_FUN(ShaderSource) */
/*-no- #define glStencilFunc GLES2_GET_FUN(StencilFunc) */
/*-no- #define glStencilFuncSeparate GLES2_GET_FUN(StencilFuncSeparate) */
/*-no- #define glStencilMask GLES2_GET_FUN(StencilMask) */
/*-no- #define glStencilMaskSeparate GLES2_GET_FUN(StencilMaskSeparate) */
/*-no- #define glStencilOp GLES2_GET_FUN(StencilOp) */
/*-no- #define glStencilOpSeparate GLES2_GET_FUN(StencilOpSeparate) */
/*-no- #define glTexImage2D GLES2_GET_FUN(TexImage2D) */
/*-no- #define glTexParameterf GLES2_GET_FUN(TexParameterf) */
/*-no- #define glTexParameterfv GLES2_GET_FUN(TexParameterfv) */
/*-no- #define glTexParameteri GLES2_GET_FUN(TexParameteri) */
/*-no- #define glTexParameteriv GLES2_GET_FUN(TexParameteriv) */
/*-no- #define glTexSubImage2D GLES2_GET_FUN(TexSubImage2D) */
/*-no- #define glUniform1f GLES2_GET_FUN(Uniform1f) */
/*-no- #define glUniform1fv GLES2_GET_FUN(Uniform1fv) */
/*-no- #define glUniform1i GLES2_GET_FUN(Uniform1i) */
/*-no- #define glUniform1iv GLES2_GET_FUN(Uniform1iv) */
/*-no- #define glUniform2f GLES2_GET_FUN(Uniform2f) */
/*-no- #define glUniform2fv GLES2_GET_FUN(Uniform2fv) */
/*-no- #define glUniform2i GLES2_GET_FUN(Uniform2i) */
/*-no- #define glUniform2iv GLES2_GET_FUN(Uniform2iv) */
/*-no- #define glUniform3f GLES2_GET_FUN(Uniform3f) */
/*-no- #define glUniform3fv GLES2_GET_FUN(Uniform3fv) */
/*-no- #define glUniform3i GLES2_GET_FUN(Uniform3i) */
/*-no- #define glUniform3iv GLES2_GET_FUN(Uniform3iv) */
/*-no- #define glUniform4f GLES2_GET_FUN(Uniform4f) */
/*-no- #define glUniform4fv GLES2_GET_FUN(Uniform4fv) */
/*-no- #define glUniform4i GLES2_GET_FUN(Uniform4i) */
/*-no- #define glUniform4iv GLES2_GET_FUN(Uniform4iv) */
/*-no- #define glUniformMatrix2fv GLES2_GET_FUN(UniformMatrix2fv) */
/*-no- #define glUniformMatrix3fv GLES2_GET_FUN(UniformMatrix3fv) */
/*-no- #define glUniformMatrix4fv GLES2_GET_FUN(UniformMatrix4fv) */
/*-no- #define glUseProgram GLES2_GET_FUN(UseProgram) */
/*-no- #define glValidateProgram GLES2_GET_FUN(ValidateProgram) */
/*-no- #define glVertexAttrib1f GLES2_GET_FUN(VertexAttrib1f) */
/*-no- #define glVertexAttrib1fv GLES2_GET_FUN(VertexAttrib1fv) */
/*-no- #define glVertexAttrib2f GLES2_GET_FUN(VertexAttrib2f) */
/*-no- #define glVertexAttrib2fv GLES2_GET_FUN(VertexAttrib2fv) */
/*-no- #define glVertexAttrib3f GLES2_GET_FUN(VertexAttrib3f) */
/*-no- #define glVertexAttrib3fv GLES2_GET_FUN(VertexAttrib3fv) */
/*-no- #define glVertexAttrib4f GLES2_GET_FUN(VertexAttrib4f) */
/*-no- #define glVertexAttrib4fv GLES2_GET_FUN(VertexAttrib4fv) */
/*-no- #define glVertexAttribPointer GLES2_GET_FUN(VertexAttribPointer) */
/*-no- #define glViewport GLES2_GET_FUN(Viewport) */
/*-no-  */
/*-no- #if !defined(GLES2_USE_CPP_BINDINGS) */
/*-no-  */
/*-no- #if defined(__cplusplus) */
/*-no- ............ */
/*-no- #endif */
/*-no-  */
/*-no- ..................................................................... */
/*-no- ................................................................................... */
/*-no- .......................................................................................................... */
/*-no- ................................................................................ */
/*-no- .......................................................................................... */
/*-no- ............................................................................................ */
/*-no- .................................................................................. */
/*-no- ............................................................................................................... */
/*-no- .................................................................... */
/*-no- ............................................................................................... */
/*-no- ................................................................................. */
/*-no- ......................................................................................................................... */
/*-no- .................................................................................................................. */
/*-no- ........................................................................................................................ */
/*-no- ............................................................................. */
/*-no- .............................................................. */
/*-no- ............................................................................................................... */
/*-no- ................................................................... */
/*-no- ............................................................. */
/*-no- .................................................................................................................. */
/*-no- .................................................................... */
/*-no- ................................................................................................................................................................................................. */
/*-no- ............................................................................................................................................................................................................ */
/*-no- ........................................................................................................................................................................ */
/*-no- .................................................................................................................................................................... */
/*-no- ........................................................... */
/*-no- ................................................................. */
/*-no- ............................................................. */
/*-no- ....................................................................................... */
/*-no- ................................................................................................. */
/*-no- ..................................................................... */
/*-no- ................................................................................................... */
/*-no- ................................................................... */
/*-no- ......................................................................................... */
/*-no- .............................................................. */
/*-no- ................................................................. */
/*-no- .................................................................................. */
/*-no- ................................................................................... */
/*-no- ........................................................... */
/*-no- .............................................................................. */
/*-no- ........................................................................................... */
/*-no- .................................................................................................................. */
/*-no- .......................................................... */
/*-no- ............................................................................. */
/*-no- .................................................... */
/*-no- ................................................... */
/*-no- ................................................................................................................................................. */
/*-no- ............................................................................................................................................. */
/*-no- .............................................................. */
/*-no- .............................................................................. */
/*-no- ..................................................................... */
/*-no- ........................................................................................ */
/*-no- .......................................................................................... */
/*-no- ................................................................................ */
/*-no- .............................................................................................................................................................. */
/*-no- ............................................................................................................................................................... */
/*-no- ............................................................................................................................. */
/*-no- ........................................................................................... */
/*-no- .................................................................................... */
/*-no- ........................................................................................................ */
/*-no- ...................................................... */
/*-no- ................................................................................ */
/*-no- .......................................................................................................................................... */
/*-no- ................................................................................ */
/*-no- ................................................................................................. */
/*-no- .......................................................................................................................... */
/*-no- .............................................................................................................. */
/*-no- ............................................................................................... */
/*-no- ........................................................................................................................ */
/*-no- ......................................................................................................................................... */
/*-no- ...................................................................................................................... */
/*-no- ................................................................ */
/*-no- ....................................................................................................... */
/*-no- ..................................................................................................... */
/*-no- ..................................................................................................... */
/*-no- ................................................................................................... */
/*-no- ............................................................................................ */
/*-no- ...................................................................................................... */
/*-no- .................................................................................................... */
/*-no- ........................................................................................................... */
/*-no- ........................................................................ */
/*-no- ............................................................... */
/*-no- ............................................................. */
/*-no- ......................................................................... */
/*-no- ................................................................. */
/*-no- ........................................................................... */
/*-no- ............................................................... */
/*-no- ................................................................. */
/*-no- ................................................................ */
/*-no- ................................................................... */
/*-no- .............................................................................. */
/*-no- .................................................................................... */
/*-no- ............................................................................................................................................. */
/*-no- ................................................................... */
/*-no- ................................................................................................................................ */
/*-no- ........................................................................................ */
/*-no- ................................................................................................ */
/*-no- ............................................................................................................................................... */
/*-no- ............................................................................................................................ */
/*-no- ........................................................................................ */
/*-no- ............................................................................................................. */
/*-no- ................................................................ */
/*-no- ..................................................................................... */
/*-no- .......................................................................................... */
/*-no- ............................................................................................................... */
/*-no- ................................................................................................................................................................................................. */
/*-no- ................................................................................................. */
/*-no- .......................................................................................................... */
/*-no- ............................................................................................... */
/*-no- ........................................................................................................ */
/*-no- .............................................................................................................................................................................................. */
/*-no- ............................................................................ */
/*-no- ................................................................................................... */
/*-no- .......................................................................... */
/*-no- ................................................................................................. */
/*-no- ....................................................................................... */
/*-no- ................................................................................................... */
/*-no- ................................................................................... */
/*-no- ................................................................................................. */
/*-no- .................................................................................................. */
/*-no- ................................................................................................... */
/*-no- ............................................................................................ */
/*-no- ................................................................................................. */
/*-no- ............................................................................................................. */
/*-no- ................................................................................................... */
/*-no- ..................................................................................................... */
/*-no- ................................................................................................. */
/*-no- .................................................................................................................................. */
/*-no- .................................................................................................................................. */
/*-no- .................................................................................................................................. */
/*-no- .................................................................. */
/*-no- ....................................................................... */
/*-no- .............................................................................. */
/*-no- ........................................................................................... */
/*-no- ......................................................................................... */
/*-no- ........................................................................................... */
/*-no- .................................................................................................... */
/*-no- ........................................................................................... */
/*-no- ............................................................................................................... */
/*-no- ........................................................................................... */
/*-no- ........................................................................................................................................................ */
/*-no- ................................................................................................. */
/*-no-  */
/*-no- #if defined(__cplusplus) */
/*-no- . */
/*-no- #endif */
/*-no-  */
/*-no- #endif  // !GLES2_USE_CPP_BINDINGS */
/*-no-  */
/*-no- #endif /-* __gl2_h_ *-/ */
/*-no-  */

#endif
/* CONSBENCH file end */
