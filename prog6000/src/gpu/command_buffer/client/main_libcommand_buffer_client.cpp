/* CONSBENCH file begin */
#ifndef MAIN_LIBCOMMAND_BUFFER_CLIENT_CPP
#define MAIN_LIBCOMMAND_BUFFER_CLIENT_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, gpu_command_buffer_client_main_libcommand_buffer_client_cpp, "gpu/command_buffer/client/main_libcommand_buffer_client.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(gpu_command_buffer_client_main_libcommand_buffer_client_cpp, "gpu/command_buffer/client/main_libcommand_buffer_client.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_gpu_command_buffer_client_cmd_buffer_helper_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_client_fenced_allocator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_client_mapped_memory_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_client_ring_buffer_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_gpu_command_buffer_client_main_libcommand_buffer_client_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_gpu_command_buffer_client_cmd_buffer_helper_cpp(it);
  PUBLIC_gpu_command_buffer_client_fenced_allocator_cpp(it);
  PUBLIC_gpu_command_buffer_client_mapped_memory_cpp(it);
  PUBLIC_gpu_command_buffer_client_ring_buffer_cpp(it);
}

#endif

