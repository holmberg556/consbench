/* CONSBENCH file begin */
#ifndef MAIN_LIBGLES2_IMPLEMENTATION_CPP
#define MAIN_LIBGLES2_IMPLEMENTATION_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, gpu_command_buffer_client_main_libgles2_implementation_cpp, "gpu/command_buffer/client/main_libgles2_implementation.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(gpu_command_buffer_client_main_libgles2_implementation_cpp, "gpu/command_buffer/client/main_libgles2_implementation.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_gpu_command_buffer_client_gles2_implementation_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_gpu_command_buffer_client_main_libgles2_implementation_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_gpu_command_buffer_client_gles2_implementation_cpp(it);
}

#endif

