/* CONSBENCH file begin */
#ifndef MAIN_LIBCOMMAND_BUFFER_COMMON_CPP
#define MAIN_LIBCOMMAND_BUFFER_COMMON_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, gpu_command_buffer_common_main_libcommand_buffer_common_cpp, "gpu/command_buffer/common/main_libcommand_buffer_common.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(gpu_command_buffer_common_main_libcommand_buffer_common_cpp, "gpu/command_buffer/common/main_libcommand_buffer_common.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_gpu_command_buffer_common_cmd_buffer_common_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_common_gles2_cmd_format_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_common_gles2_cmd_utils_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_common_id_allocator_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_gpu_command_buffer_common_main_libcommand_buffer_common_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_gpu_command_buffer_common_cmd_buffer_common_cpp(it);
  PUBLIC_gpu_command_buffer_common_gles2_cmd_format_cpp(it);
  PUBLIC_gpu_command_buffer_common_gles2_cmd_utils_cpp(it);
  PUBLIC_gpu_command_buffer_common_id_allocator_cpp(it);
}

#endif

