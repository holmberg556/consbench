/* CONSBENCH file begin */
#ifndef MAIN_LIBCOMMAND_BUFFER_SERVICE_CPP
#define MAIN_LIBCOMMAND_BUFFER_SERVICE_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, gpu_command_buffer_service_main_libcommand_buffer_service_cpp, "gpu/command_buffer/service/main_libcommand_buffer_service.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(gpu_command_buffer_service_main_libcommand_buffer_service_cpp, "gpu/command_buffer/service/main_libcommand_buffer_service.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_gpu_command_buffer_service_buffer_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_framebuffer_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_cmd_parser_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_command_buffer_service_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_common_decoder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_context_group_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_feature_info_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_gles2_cmd_decoder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_gles2_cmd_validation_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_gpu_processor_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_gpu_processor_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_id_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_program_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_renderbuffer_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_shader_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_shader_translator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_gpu_command_buffer_service_texture_manager_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_gpu_command_buffer_service_main_libcommand_buffer_service_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_gpu_command_buffer_service_buffer_manager_cpp(it);
  PUBLIC_gpu_command_buffer_service_framebuffer_manager_cpp(it);
  PUBLIC_gpu_command_buffer_service_cmd_parser_cpp(it);
  PUBLIC_gpu_command_buffer_service_command_buffer_service_cpp(it);
  PUBLIC_gpu_command_buffer_service_common_decoder_cpp(it);
  PUBLIC_gpu_command_buffer_service_context_group_cpp(it);
  PUBLIC_gpu_command_buffer_service_feature_info_cpp(it);
  PUBLIC_gpu_command_buffer_service_gles2_cmd_decoder_cpp(it);
  PUBLIC_gpu_command_buffer_service_gles2_cmd_validation_cpp(it);
  PUBLIC_gpu_command_buffer_service_gpu_processor_cpp(it);
  PUBLIC_gpu_command_buffer_service_gpu_processor_linux_cpp(it);
  PUBLIC_gpu_command_buffer_service_id_manager_cpp(it);
  PUBLIC_gpu_command_buffer_service_program_manager_cpp(it);
  PUBLIC_gpu_command_buffer_service_renderbuffer_manager_cpp(it);
  PUBLIC_gpu_command_buffer_service_shader_manager_cpp(it);
  PUBLIC_gpu_command_buffer_service_shader_translator_cpp(it);
  PUBLIC_gpu_command_buffer_service_texture_manager_cpp(it);
}

#endif

