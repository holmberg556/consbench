/* CONSBENCH file begin */
#ifndef MAIN_LIBGPU_IPC_CPP
#define MAIN_LIBGPU_IPC_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, gpu_ipc_main_libgpu_ipc_cpp, "gpu/ipc/main_libgpu_ipc.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(gpu_ipc_main_libgpu_ipc_cpp, "gpu/ipc/main_libgpu_ipc.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_gpu_ipc_gpu_command_buffer_traits_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_gpu_ipc_main_libgpu_ipc_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_gpu_ipc_gpu_command_buffer_traits_cpp(it);
}

#endif

