/* CONSBENCH file begin */
#ifndef MAIN_LIBIPC_CPP
#define MAIN_LIBIPC_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, ipc_main_libipc_cpp, "ipc/main_libipc.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(ipc_main_libipc_cpp, "ipc/main_libipc.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_ipc_file_descriptor_set_posix_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ipc_ipc_channel_posix_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ipc_ipc_channel_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ipc_ipc_logging_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ipc_ipc_message_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ipc_ipc_message_utils_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ipc_ipc_switches_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ipc_ipc_sync_channel_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ipc_ipc_sync_message_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ipc_ipc_sync_message_filter_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_ipc_main_libipc_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_ipc_file_descriptor_set_posix_cpp(it);
  PUBLIC_ipc_ipc_channel_posix_cpp(it);
  PUBLIC_ipc_ipc_channel_proxy_cpp(it);
  PUBLIC_ipc_ipc_logging_cpp(it);
  PUBLIC_ipc_ipc_message_cpp(it);
  PUBLIC_ipc_ipc_message_utils_cpp(it);
  PUBLIC_ipc_ipc_switches_cpp(it);
  PUBLIC_ipc_ipc_sync_channel_cpp(it);
  PUBLIC_ipc_ipc_sync_message_cpp(it);
  PUBLIC_ipc_ipc_sync_message_filter_cpp(it);
}

#endif

