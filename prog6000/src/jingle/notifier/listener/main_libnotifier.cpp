/* CONSBENCH file begin */
#ifndef MAIN_LIBNOTIFIER_CPP
#define MAIN_LIBNOTIFIER_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, jingle_notifier_listener_main_libnotifier_cpp, "jingle/notifier/listener/main_libnotifier.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(jingle_notifier_listener_main_libnotifier_cpp, "jingle/notifier/listener/main_libnotifier.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_jingle_notifier_base_chrome_async_socket_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_base_fake_ssl_client_socket_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_base_notification_method_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_base_task_pump_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_base_weak_xmpp_client_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_base_xmpp_client_socket_factory_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_base_xmpp_connection_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_communicator_connection_options_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_communicator_connection_settings_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_communicator_gaia_token_pre_xmpp_auth_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_communicator_login_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_communicator_login_settings_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_communicator_single_login_attempt_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_communicator_xmpp_connection_generator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_listener_listen_task_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_listener_mediator_thread_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_listener_notification_constants_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_listener_notification_defines_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_listener_push_notifications_listen_task_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_listener_push_notifications_subscribe_task_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_listener_push_notifications_thread_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_listener_send_update_task_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_listener_subscribe_task_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_listener_talk_mediator_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_jingle_notifier_listener_xml_element_util_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_jingle_notifier_listener_main_libnotifier_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_jingle_notifier_base_chrome_async_socket_cpp(it);
  PUBLIC_jingle_notifier_base_fake_ssl_client_socket_cpp(it);
  PUBLIC_jingle_notifier_base_notification_method_cpp(it);
  PUBLIC_jingle_notifier_base_task_pump_cpp(it);
  PUBLIC_jingle_notifier_base_weak_xmpp_client_cpp(it);
  PUBLIC_jingle_notifier_base_xmpp_client_socket_factory_cpp(it);
  PUBLIC_jingle_notifier_base_xmpp_connection_cpp(it);
  PUBLIC_jingle_notifier_communicator_connection_options_cpp(it);
  PUBLIC_jingle_notifier_communicator_connection_settings_cpp(it);
  PUBLIC_jingle_notifier_communicator_gaia_token_pre_xmpp_auth_cpp(it);
  PUBLIC_jingle_notifier_communicator_login_cpp(it);
  PUBLIC_jingle_notifier_communicator_login_settings_cpp(it);
  PUBLIC_jingle_notifier_communicator_single_login_attempt_cpp(it);
  PUBLIC_jingle_notifier_communicator_xmpp_connection_generator_cpp(it);
  PUBLIC_jingle_notifier_listener_listen_task_cpp(it);
  PUBLIC_jingle_notifier_listener_mediator_thread_impl_cpp(it);
  PUBLIC_jingle_notifier_listener_notification_constants_cpp(it);
  PUBLIC_jingle_notifier_listener_notification_defines_cpp(it);
  PUBLIC_jingle_notifier_listener_push_notifications_listen_task_cpp(it);
  PUBLIC_jingle_notifier_listener_push_notifications_subscribe_task_cpp(it);
  PUBLIC_jingle_notifier_listener_push_notifications_thread_cpp(it);
  PUBLIC_jingle_notifier_listener_send_update_task_cpp(it);
  PUBLIC_jingle_notifier_listener_subscribe_task_cpp(it);
  PUBLIC_jingle_notifier_listener_talk_mediator_impl_cpp(it);
  PUBLIC_jingle_notifier_listener_xml_element_util_cpp(it);
}

#endif

