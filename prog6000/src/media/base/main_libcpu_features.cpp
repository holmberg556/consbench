/* CONSBENCH file begin */
#ifndef MAIN_LIBCPU_FEATURES_CPP
#define MAIN_LIBCPU_FEATURES_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, media_base_main_libcpu_features_cpp, "media/base/main_libcpu_features.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(media_base_main_libcpu_features_cpp, "media/base/main_libcpu_features.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_media_base_cpu_features_x86_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_media_base_main_libcpu_features_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_media_base_cpu_features_x86_cpp(it);
}

#endif

