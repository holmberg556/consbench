/* CONSBENCH file begin */
#ifndef MAIN_LIBMEDIA_CPP
#define MAIN_LIBMEDIA_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, media_base_main_libmedia_cpp, "media/base/main_libmedia.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(media_base_main_libmedia_cpp, "media/base/main_libmedia.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_media_audio_audio_buffers_state_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_audio_input_controller_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_audio_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_audio_manager_base_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_audio_output_controller_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_audio_output_dispatcher_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_audio_output_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_audio_parameters_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_audio_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_fake_audio_input_stream_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_fake_audio_output_stream_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_linux_audio_manager_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_linux_alsa_input_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_linux_alsa_output_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_linux_alsa_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_linux_alsa_wrapper_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_audio_simple_sources_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_buffers_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_callback_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_clock_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_composite_filter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_data_buffer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_djb2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_filter_collection_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_filters_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_h264_bitstream_converter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_media_format_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_media_posix_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_media_switches_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_message_loop_factory_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_message_loop_factory_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_pipeline_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_pts_heap_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_seekable_buffer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_state_matrix_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_video_frame_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_ffmpeg_ffmpeg_common_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_ffmpeg_ffmpeg_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_ffmpeg_file_protocol_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_audio_file_reader_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_audio_renderer_algorithm_base_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_audio_renderer_algorithm_default_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_audio_renderer_algorithm_ola_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_audio_renderer_base_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_audio_renderer_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_bitstream_converter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_ffmpeg_audio_decoder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_ffmpeg_demuxer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_ffmpeg_h264_bitstream_converter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_ffmpeg_glue_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_ffmpeg_interfaces_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_ffmpeg_video_decoder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_file_data_source_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_null_audio_renderer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_null_video_renderer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_video_renderer_base_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_video_ffmpeg_video_allocator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_video_ffmpeg_video_decode_engine_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_video_video_decode_engine_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_filters_omx_video_decoder_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_media_base_main_libmedia_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_media_audio_audio_buffers_state_cpp(it);
  PUBLIC_media_audio_audio_input_controller_cpp(it);
  PUBLIC_media_audio_audio_manager_cpp(it);
  PUBLIC_media_audio_audio_manager_base_cpp(it);
  PUBLIC_media_audio_audio_output_controller_cpp(it);
  PUBLIC_media_audio_audio_output_dispatcher_cpp(it);
  PUBLIC_media_audio_audio_output_proxy_cpp(it);
  PUBLIC_media_audio_audio_parameters_cpp(it);
  PUBLIC_media_audio_audio_util_cpp(it);
  PUBLIC_media_audio_fake_audio_input_stream_cpp(it);
  PUBLIC_media_audio_fake_audio_output_stream_cpp(it);
  PUBLIC_media_audio_linux_audio_manager_linux_cpp(it);
  PUBLIC_media_audio_linux_alsa_input_cpp(it);
  PUBLIC_media_audio_linux_alsa_output_cpp(it);
  PUBLIC_media_audio_linux_alsa_util_cpp(it);
  PUBLIC_media_audio_linux_alsa_wrapper_cpp(it);
  PUBLIC_media_audio_simple_sources_cpp(it);
  PUBLIC_media_base_buffers_cpp(it);
  PUBLIC_media_base_callback_cpp(it);
  PUBLIC_media_base_clock_cpp(it);
  PUBLIC_media_base_composite_filter_cpp(it);
  PUBLIC_media_base_data_buffer_cpp(it);
  PUBLIC_media_base_djb2_cpp(it);
  PUBLIC_media_base_filter_collection_cpp(it);
  PUBLIC_media_base_filters_cpp(it);
  PUBLIC_media_base_h264_bitstream_converter_cpp(it);
  PUBLIC_media_base_media_format_cpp(it);
  PUBLIC_media_base_media_posix_cpp(it);
  PUBLIC_media_base_media_switches_cpp(it);
  PUBLIC_media_base_message_loop_factory_cpp(it);
  PUBLIC_media_base_message_loop_factory_impl_cpp(it);
  PUBLIC_media_base_pipeline_impl_cpp(it);
  PUBLIC_media_base_pts_heap_cpp(it);
  PUBLIC_media_base_seekable_buffer_cpp(it);
  PUBLIC_media_base_state_matrix_cpp(it);
  PUBLIC_media_base_video_frame_cpp(it);
  PUBLIC_media_ffmpeg_ffmpeg_common_cpp(it);
  PUBLIC_media_ffmpeg_ffmpeg_util_cpp(it);
  PUBLIC_media_ffmpeg_file_protocol_cpp(it);
  PUBLIC_media_filters_audio_file_reader_cpp(it);
  PUBLIC_media_filters_audio_renderer_algorithm_base_cpp(it);
  PUBLIC_media_filters_audio_renderer_algorithm_default_cpp(it);
  PUBLIC_media_filters_audio_renderer_algorithm_ola_cpp(it);
  PUBLIC_media_filters_audio_renderer_base_cpp(it);
  PUBLIC_media_filters_audio_renderer_impl_cpp(it);
  PUBLIC_media_filters_bitstream_converter_cpp(it);
  PUBLIC_media_filters_ffmpeg_audio_decoder_cpp(it);
  PUBLIC_media_filters_ffmpeg_demuxer_cpp(it);
  PUBLIC_media_filters_ffmpeg_h264_bitstream_converter_cpp(it);
  PUBLIC_media_filters_ffmpeg_glue_cpp(it);
  PUBLIC_media_filters_ffmpeg_interfaces_cpp(it);
  PUBLIC_media_filters_ffmpeg_video_decoder_cpp(it);
  PUBLIC_media_filters_file_data_source_cpp(it);
  PUBLIC_media_filters_null_audio_renderer_cpp(it);
  PUBLIC_media_filters_null_video_renderer_cpp(it);
  PUBLIC_media_filters_video_renderer_base_cpp(it);
  PUBLIC_media_video_ffmpeg_video_allocator_cpp(it);
  PUBLIC_media_video_ffmpeg_video_decode_engine_cpp(it);
  PUBLIC_media_video_video_decode_engine_cpp(it);
  PUBLIC_media_filters_omx_video_decoder_cpp(it);
}

#endif

