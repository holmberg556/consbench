/* CONSBENCH file begin */
#ifndef MAIN_LIBYUV_CONVERT_CPP
#define MAIN_LIBYUV_CONVERT_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, media_base_main_libyuv_convert_cpp, "media/base/main_libyuv_convert.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(media_base_main_libyuv_convert_cpp, "media/base/main_libyuv_convert.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_media_base_yuv_convert_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_yuv_convert_c_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_yuv_row_posix_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_yuv_row_table_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_media_base_main_libyuv_convert_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_media_base_yuv_convert_cpp(it);
  PUBLIC_media_base_yuv_convert_c_cpp(it);
  PUBLIC_media_base_yuv_row_posix_cpp(it);
  PUBLIC_media_base_yuv_row_table_cpp(it);
}

#endif

