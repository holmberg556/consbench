/* CONSBENCH file begin */
#ifndef MAIN_LIBYUV_CONVERT_SSE2_CPP
#define MAIN_LIBYUV_CONVERT_SSE2_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, media_base_main_libyuv_convert_sse2_cpp, "media/base/main_libyuv_convert_sse2.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(media_base_main_libyuv_convert_sse2_cpp, "media/base/main_libyuv_convert_sse2.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_media_base_yuv_convert_sse2_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_media_base_main_libyuv_convert_sse2_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_media_base_yuv_convert_sse2_cpp(it);
}

#endif

