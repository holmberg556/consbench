/* CONSBENCH file begin */
#ifndef MAIN_LIBOMX_WRAPPER_CPP
#define MAIN_LIBOMX_WRAPPER_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, media_omx_main_libomx_wrapper_cpp, "media/omx/main_libomx_wrapper.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(media_omx_main_libomx_wrapper_cpp, "media/omx/main_libomx_wrapper.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_media_omx_omx_configurator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_video_omx_video_decode_engine_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_media_omx_main_libomx_wrapper_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_media_omx_omx_configurator_cpp(it);
  PUBLIC_media_video_omx_video_decode_engine_cpp(it);
}

#endif

