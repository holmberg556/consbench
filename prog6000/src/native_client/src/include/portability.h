/* CONSBENCH file begin */
#ifndef PORTABILITY_H_3612
#define PORTABILITY_H_3612

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_include_portability_h, "native_client/src/include/portability.h");

/* CONSBENCH includes begin */
#include <stdlib.h>
#include "native_client/src/include/nacl_base.h"
#include "native_client/src/trusted/service_runtime/include/bits/wordsize.h"
# include "native_client/src/include/win/port_win.h"
# include <sys/types.h>
#include "native_client/src/third_party/valgrind/memcheck.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_include_portability_h, "native_client/src/include/portability.h");

/*-no- ... */
/*-no- .............................. */
/*-no- ....................... */
/*-no- .. */
/*-no- ..................................................................... */
/*-no- ......................................................................... */
/*-no- ....... */
/*-no- .. */
/*-no- ....................................................................... */
/*-no- ................................................................ */
/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ................................................................ */
/*-no- ................ */
/*-no- ............................................................. */
/*-no- ....................................................................... */
/*-no- ........................................................... */
/*-no- .. */
/*-no- ...................................................................... */
/*-no- .................................................................... */
/*-no- ........................................................................ */
/*-no- ....................................................................... */
/*-no- ........................................................................ */
/*-no- ................................................................... */
/*-no- ........................................................................ */
/*-no- ........................................................................ */
/*-no- ...................................................................... */
/*-no- ........................................................................ */
/*-no- ....................................................................... */
/*-no- .... */
/*-no-  */
/*-no-  */
/*-no- ... */
/*-no- .................................................................... */
/*-no- ................................................................. */
/*-no- ............ */
/*-no- .. */
/*-no- ...................................................................... */
/*-no- ............................... */
/*-no- .... */
/*-no-  */
/*-no- #ifndef NATIVE_CLIENT_SRC_INCLUDE_PORTABILITY_H_ */
/*-no- #define NATIVE_CLIENT_SRC_INCLUDE_PORTABILITY_H_ 1 */
/*-no-  */
/*-no- #include <stdlib.h> */
/*-no-  */
/*-no- #include "native_client/src/include/nacl_base.h" */
/*-no- #ifdef __native_client__ */
/*-no- #include <bits/wordsize.h> */
/*-no- #else */
/*-no- #include "native_client/src/trusted/service_runtime/include/bits/wordsize.h" */
/*-no- #endif */
/*-no-  */
/*-no- #if NACL_WINDOWS */
/*-no- ................................................................... */
/*-no- #pragma warning(disable : 4996) */
/*-no- # include <malloc.h> */
/*-no- .................................. */
/*-no- # include "native_client/src/include/win/port_win.h" */
/*-no- #else */
/*-no- # include <sys/types.h> */
/*-no- # include <stdint.h> */
/*-no- # include <unistd.h> */
/*-no- # include <sys/time.h> */
/*-no- #endif  /-*NACL_WINDOWS*-/ */
/*-no-  */
/*-no- .......................................... */
/*-no- #if NACL_WINDOWS */
/*-no- # define INLINE __forceinline */
/*-no- #else */
/*-no- # define INLINE __inline__ */
/*-no- #endif */
/*-no-  */
/*-no- #if NACL_WINDOWS */
/*-no- # define DLLEXPORT __declspec(dllexport) */
/*-no- #elif defined(NACL_LINUX) || defined(NACL_OSX) */
/*-no- # define DLLEXPORT __attribute__ ((visibility("default"))) */
/*-no- #elif defined(__native_client__) */
/*-no- .................. */
/*-no- #else */
/*-no- # error "what platform?" */
/*-no- #endif */
/*-no-  */
/*-no- #if NACL_WINDOWS */
/*-no- # define ATTRIBUTE_FORMAT_PRINTF(m, n) */
/*-no- #else */
/*-no- # define ATTRIBUTE_FORMAT_PRINTF(m, n) __attribute__((format(printf, m, n))) */
/*-no- #endif */
/*-no-  */
/*-no- #if NACL_WINDOWS */
/*-no- # define UNREFERENCED_PARAMETER(P) (P) */
/*-no- #else */
/*-no- # define UNREFERENCED_PARAMETER(P) do { (void) P; } while (0) */
/*-no- #endif */
/*-no-  */
/*-no- #if NACL_WINDOWS */
/*-no- # define NORETURN __declspec(noreturn) */
/*-no- #else */
/*-no- # define NORETURN __attribute__((noreturn))  /-* assumes gcc *-/ */
/*-no- # define _cdecl /-* empty *-/ */
/*-no- #endif */
/*-no-  */
/*-no- #if NACL_WINDOWS */
/*-no- # define THREAD __declspec(thread) */
/*-no- #else */
/*-no- # define THREAD __thread */
/*-no- # define WINAPI */
/*-no- #endif */
/*-no-  */
/*-no- #if NACL_WINDOWS */
/*-no- # define NACL_WUR */
/*-no- #else */
/*-no- # define NACL_WUR __attribute__((__warn_unused_result__)) */
/*-no- #endif */
/*-no-  */
/*-no- ... */
/*-no- ............................................................................ */
/*-no- .............................................................................. */
/*-no- .............................................................................. */
/*-no- ............................................................................. */
/*-no- ......................................................... */
/*-no- .... */
/*-no-  */
/*-no- #define GG_INT8_C(x)    (x) */
/*-no- #define GG_INT16_C(x)   (x) */
/*-no- #define GG_INT32_C(x)   (x) */
/*-no- #define GG_INT64_C(x)   GG_LONGLONG(x) */
/*-no-  */
/*-no- #define GG_UINT8_C(x)   (x ## U) */
/*-no- #define GG_UINT16_C(x)  (x ## U) */
/*-no- #define GG_UINT32_C(x)  (x ## U) */
/*-no- #define GG_UINT64_C(x)  GG_ULONGLONG(x) */
/*-no-  */
/*-no- #if NACL_WINDOWS */
/*-no- #define GG_LONGLONG(x) x##I64 */
/*-no- #define GG_ULONGLONG(x) x##UI64 */
/*-no- #else */
/*-no- #define GG_LONGLONG(x) x##LL */
/*-no- #define GG_ULONGLONG(x) x##ULL */
/*-no- #endif */
/*-no-  */
/*-no- #if NACL_WINDOWS */
/*-no- # define LOCALTIME_R(in_time_t_ptr, out_struct_tm_ptr) \ */
/*-no- ......................................................... */
/*-no- ......................................................................... */
/*-no-  */
/*-no- ................. */
/*-no- ..................... */
/*-no- ................. */
/*-no- .. */
/*-no-  */
/*-no- #else */
/*-no-  */
/*-no- # define LOCALTIME_R(in_time_t_ptr, out_struct_tm_ptr) \ */
/*-no- ............................................... */
/*-no- #endif */
/*-no-  */
/*-no-  */
/*-no- .... */
/*-no- ............................................................... */
/*-no- ................................... */
/*-no- ........................................ */
/*-no- ........................................................ */
/*-no- ........................................................ */
/*-no- ......................................... */
/*-no- .... */
/*-no- #if defined(_M_X64) || defined(__x86_64__) */
/*-no- #define NACL_ARCH_CPU_X86_FAMILY 1 */
/*-no- #define NACL_ARCH_CPU_X86_64 1 */
/*-no- #define NACL_ARCH_CPU_64_BITS 1 */
/*-no- #define NACL_HOST_WORDSIZE 64 */
/*-no- #elif defined(_M_IX86) || defined(__i386__) */
/*-no- #define NACL_ARCH_CPU_X86_FAMILY 1 */
/*-no- #define NACL_ARCH_CPU_X86 1 */
/*-no- #define NACL_ARCH_CPU_32_BITS 1 */
/*-no- #define NACL_HOST_WORDSIZE 32 */
/*-no- #elif defined(__ARMEL__) */
/*-no- #define NACL_ARCH_CPU_ARM_FAMILY 1 */
/*-no- #define NACL_ARCH_CPU_ARMEL 1 */
/*-no- #define NACL_ARCH_CPU_32_BITS 1 */
/*-no- #define NACL_HOST_WORDSIZE 32 */
/*-no- #define NACL_WCHAR_T_IS_UNSIGNED 1 */
/*-no- #else */
/*-no- #error Unrecognized host architecture */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef SIZE_T_MAX */
/*-no- # define SIZE_T_MAX ((size_t) -1) */
/*-no- #endif */
/*-no-  */
/*-no- ................................................................. */
/*-no- #ifndef OFF_T_MIN */
/*-no- # define OFF_T_MIN ((off_t) (((uint64_t) 1) << (8 * sizeof(off_t) - 1))) */
/*-no- #endif */
/*-no- #ifndef OFF_T_MAX */
/*-no- # define OFF_T_MAX ((off_t) ~(((uint64_t) 1) << (8 * sizeof(off_t) - 1))) */
/*-no- #endif */
/*-no-  */
/*-no-  */
/*-no- ... */
/*-no- ................................................................. */
/*-no- ............................................................ */
/*-no- ................................................................. */
/*-no- ....... */
/*-no- .... */
/*-no- #if NACL_WINDOWS */
/*-no- # if defined(_WIN64) */
/*-no- #  define  NACL___PRIS_PREFIX "I64" */
/*-no- # else */
/*-no- #  define  NACL___PRIS_PREFIX */
/*-no- # endif */
/*-no- #elif NACL_OSX */
/*-no- # define  NACL___PRIS_PREFIX "l" /-* -pedantic C++ programs w/ xcode *-/ */
/*-no- #elif defined(__native_client__) */
/*-no- # define NACL___PRIS_PREFIX */
/*-no- #elif __WORDSIZE == 64 */
/*-no- # define NACL___PRIS_PREFIX "l" */
/*-no- #else */
/*-no- # define NACL___PRIS_PREFIX */
/*-no- #endif */
/*-no-  */
/*-no- #if !defined(NACL_PRIdS) */
/*-no- #define NACL_PRIdS NACL___PRIS_PREFIX "d" */
/*-no- #endif */
/*-no- #if !defined(NACL_PRIiS) */
/*-no- #define NACL_PRIiS NACL___PRIS_PREFIX "i" */
/*-no- #endif */
/*-no- #if !defined(NACL_PRIoS) */
/*-no- #define NACL_PRIoS NACL___PRIS_PREFIX "o" */
/*-no- #endif */
/*-no- #if !defined (NACL_PRIuS) */
/*-no- #define NACL_PRIuS NACL___PRIS_PREFIX "u" */
/*-no- #endif */
/*-no- #if !defined(NACL_PRIxS) */
/*-no- #define NACL_PRIxS NACL___PRIS_PREFIX "x" */
/*-no- #endif */
/*-no- #if !defined(NACL_PRIXS) */
/*-no- #define NACL_PRIXS NACL___PRIS_PREFIX "X" */
/*-no- #endif */
/*-no-  */
/*-no- ... */
/*-no- ............................................................ */
/*-no- .... */
/*-no- #if !NACL_WINDOWS */
/*-no- #ifndef __STDC_FORMAT_MACROS */
/*-no- # define __STDC_FORMAT_MACROS  /-* C++ *-/ */
/*-no- #endif */
/*-no- # include <inttypes.h> */
/*-no-  */
/*-no- #define NACL_PRIxPTR PRIxPTR */
/*-no- #define NACL_PRIXPTR PRIXPTR */
/*-no- #define NACL_PRIdPTR PRIdPTR */
/*-no-  */
/*-no- #define NACL_PRId64 PRId64 */
/*-no- #define NACL_PRIu64 PRIu64 */
/*-no- #define NACL_PRIx64 PRIx64 */
/*-no- #define NACL_PRIX64 PRIX64 */
/*-no-  */
/*-no- #define NACL_PRIx32 PRIx32 */
/*-no- #define NACL_PRIX32 PRIX32 */
/*-no- #define NACL_PRId32 PRId32 */
/*-no- #define NACL_PRIu32 PRIu32 */
/*-no-  */
/*-no- #define NACL_PRId16 PRId16 */
/*-no- #define NACL_PRIu16 PRIu16 */
/*-no- #define NACL_PRIx16 PRIx16 */
/*-no-  */
/*-no- #define NACL_PRId8 PRId8 */
/*-no- #define NACL_PRIu8 PRIu8 */
/*-no- #define NACL_PRIx8 PRIx8 */
/*-no-  */
/*-no-  */
/*-no- # if NACL_OSX */
/*-no- ... */
/*-no- ................................................................... */
/*-no- ..................................................... */
/*-no- .... */
/*-no- #  undef NACL_PRId8 */
/*-no- #  undef NACL_PRIi8 */
/*-no- #  undef NACL_PRIo8 */
/*-no- #  undef NACL_PRIu8 */
/*-no- #  undef NACL_PRIx8 */
/*-no- #  undef NACL_PRIX8 */
/*-no- #  define NACL_PRId8  "d" */
/*-no- #  define NACL_PRIi8  "i" */
/*-no- #  define NACL_PRIo8  "o" */
/*-no- #  define NACL_PRIu8  "u" */
/*-no- #  define NACL_PRIx8  "x" */
/*-no- #  define NACL_PRIX8  "X" */
/*-no- # endif */
/*-no- #else */
/*-no- .................... */
/*-no- # if defined(_WIN64) */
/*-no- #  define NACL___PRIPTR_PREFIX "I64" */
/*-no- # else */
/*-no- #  define NACL___PRIPTR_PREFIX "l" */
/*-no- # endif */
/*-no- # define NACL_PRIdPTR NACL___PRIPTR_PREFIX "d" */
/*-no- # define NACL_PRIiPTR NACL___PRIPTR_PREFIX "i" */
/*-no- # define NACL_PRIoPTR NACL___PRIPTR_PREFIX "o" */
/*-no- # define NACL_PRIuPTR NACL___PRIPTR_PREFIX "u" */
/*-no- # define NACL_PRIxPTR NACL___PRIPTR_PREFIX "x" */
/*-no- # define NACL_PRIXPTR NACL___PRIPTR_PREFIX "X" */
/*-no-  */
/*-no- # define NACL_PRId8  "d" */
/*-no- # define NACL_PRIi8  "i" */
/*-no- # define NACL_PRIo8  "o" */
/*-no- # define NACL_PRIu8  "u" */
/*-no- # define NACL_PRIx8  "x" */
/*-no- # define NACL_PRIX8  "X" */
/*-no-  */
/*-no- # define NACL_PRId16 "d" */
/*-no- # define NACL_PRIi16 "i" */
/*-no- # define NACL_PRIo16 "o" */
/*-no- # define NACL_PRIu16 "u" */
/*-no- # define NACL_PRIx16 "x" */
/*-no- # define NACL_PRIX16 "X" */
/*-no-  */
/*-no- # define NACL___PRI32_PREFIX "I32" */
/*-no-  */
/*-no- # define NACL_PRId32 NACL___PRI32_PREFIX "d" */
/*-no- # define NACL_PRIi32 NACL___PRI32_PREFIX "i" */
/*-no- # define NACL_PRIo32 NACL___PRI32_PREFIX "o" */
/*-no- # define NACL_PRIu32 NACL___PRI32_PREFIX "u" */
/*-no- # define NACL_PRIx32 NACL___PRI32_PREFIX "x" */
/*-no- # define NACL_PRIX32 NACL___PRI32_PREFIX "X" */
/*-no-  */
/*-no- # define NACL___PRI64_PREFIX "I64" */
/*-no-  */
/*-no- #if !defined(NACL_PRId64) */
/*-no- # define NACL_PRId64 NACL___PRI64_PREFIX "d" */
/*-no- #endif */
/*-no- #if !defined(NACL_PRIi64) */
/*-no- # define NACL_PRIi64 NACL___PRI64_PREFIX "i" */
/*-no- #endif */
/*-no- #if !defined(NACL_PRIo64) */
/*-no- # define NACL_PRIo64 NACL___PRI64_PREFIX "o" */
/*-no- #endif */
/*-no- #if !defined(NACL_PRIu64) */
/*-no- # define NACL_PRIu64 NACL___PRI64_PREFIX "u" */
/*-no- #endif */
/*-no- #if !defined(NACL_PRIx64) */
/*-no- # define NACL_PRIx64 NACL___PRI64_PREFIX "x" */
/*-no- #endif */
/*-no- #if !defined(NACL_PRIX64) */
/*-no- # define NACL_PRIX64 NACL___PRI64_PREFIX "X" */
/*-no- #endif */
/*-no-  */
/*-no- #endif */
/*-no-  */
/*-no- ... */
/*-no- ................................................................... */
/*-no- .... */
/*-no- #if defined(_DEBUG) && NACL_LINUX */
/*-no- #include "native_client/src/third_party/valgrind/memcheck.h" */
/*-no- #define NACL_MAKE_MEM_UNDEFINED(a, b) VALGRIND_MAKE_MEM_UNDEFINED(a, b) */
/*-no- #else */
/*-no- #define NACL_MAKE_MEM_UNDEFINED(a, b) */
/*-no- #endif */
/*-no-  */
/*-no- #endif  /-* NATIVE_CLIENT_SRC_INCLUDE_PORTABILITY_H_ *-/ */

#endif
/* CONSBENCH file end */
