/* CONSBENCH file begin */
#ifndef MAIN_LIBGIO_CPP
#define MAIN_LIBGIO_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_shared_gio_main_libgio_cpp, "native_client/src/shared/gio/main_libgio.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_shared_gio_main_libgio_cpp, "native_client/src/shared/gio/main_libgio.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_shared_gio_gio_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_shared_gio_gio_mem_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_shared_gio_gprintf_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_shared_gio_gio_mem_snapshot_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_shared_gio_main_libgio_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_shared_gio_gio_cpp(it);
  PUBLIC_native_client_src_shared_gio_gio_mem_cpp(it);
  PUBLIC_native_client_src_shared_gio_gprintf_cpp(it);
  PUBLIC_native_client_src_shared_gio_gio_mem_snapshot_cpp(it);
}

#endif

