/* CONSBENCH file begin */
#ifndef MAIN_LIBIMC_CPP
#define MAIN_LIBIMC_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_shared_imc_main_libimc_cpp, "native_client/src/shared/imc/main_libimc.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_shared_imc_main_libimc_cpp, "native_client/src/shared/imc/main_libimc.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_shared_imc_nacl_imc_c_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_shared_imc_nacl_imc_common_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_shared_imc_nacl_imc_unistd_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_shared_imc_linux_nacl_imc_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_shared_imc_main_libimc_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_shared_imc_nacl_imc_c_cpp(it);
  PUBLIC_native_client_src_shared_imc_nacl_imc_common_cpp(it);
  PUBLIC_native_client_src_shared_imc_nacl_imc_unistd_cpp(it);
  PUBLIC_native_client_src_shared_imc_linux_nacl_imc_cpp(it);
}

#endif

