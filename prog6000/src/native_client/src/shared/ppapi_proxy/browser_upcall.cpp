/* CONSBENCH file begin */
#ifndef BROWSER_UPCALL_CC_3746
#define BROWSER_UPCALL_CC_3746

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_shared_ppapi_proxy_browser_upcall_cpp, "native_client/src/shared/ppapi_proxy/browser_upcall.cpp");

/* CONSBENCH includes begin */
#include "native_client/src/shared/ppapi_proxy/browser_upcall.h"
#include <new>
#include "native_client/src/include/portability.h"
#include "native_client/src/include/nacl_macros.h"
#include "native_client/src/include/nacl_scoped_ptr.h"
#include "native_client/src/shared/platform/nacl_threads.h"
#include "native_client/src/shared/ppapi_proxy/browser_callback.h"
#include "native_client/src/shared/ppapi_proxy/browser_globals.h"
#include "native_client/src/shared/ppapi_proxy/utility.h"
#include "native_client/src/shared/srpc/nacl_srpc.h"
#include "native_client/src/trusted/desc/nacl_desc_wrapper.h"
#include "ppapi/c/pp_completion_callback.h"
#include "ppapi/c/pp_errors.h"
#include "ppapi/c/ppb_core.h"
#include "srpcgen/upcall.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_shared_ppapi_proxy_browser_upcall_cpp, "native_client/src/shared/ppapi_proxy/browser_upcall.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(native_client_src_shared_ppapi_proxy_browser_upcall_cpp);

/*-no- ..................................................................... */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no- .. */
/*-no- .................................................................. */
/*-no- .. */
/*-no- ....................................................... */
/*-no- ..................................................................... */
/*-no- ........................................................ */
/*-no- .. */
/*-no- .................................................................. */
/*-no- .. */
/*-no- ............................................................................. */
/*-no- ............................................................................ */
/*-no- ..................... */
/*-no-  */
/*-no- #include "native_client/src/shared/ppapi_proxy/browser_upcall.h" */
/*-no-  */
/*-no- #include <new> */
/*-no-  */
/*-no- #include "native_client/src/include/portability.h" */
/*-no- #include "native_client/src/include/nacl_macros.h" */
/*-no- #include "native_client/src/include/nacl_scoped_ptr.h" */
/*-no- #include "native_client/src/shared/platform/nacl_threads.h" */
/*-no- #include "native_client/src/shared/ppapi_proxy/browser_callback.h" */
/*-no- #include "native_client/src/shared/ppapi_proxy/browser_globals.h" */
/*-no- #include "native_client/src/shared/ppapi_proxy/utility.h" */
/*-no- #include "native_client/src/shared/srpc/nacl_srpc.h" */
/*-no- #include "native_client/src/trusted/desc/nacl_desc_wrapper.h" */
/*-no- #include "ppapi/c/pp_completion_callback.h" */
/*-no- #include "ppapi/c/pp_errors.h" */
/*-no- #include "ppapi/c/ppb_core.h" */
/*-no- #include "srpcgen/upcall.h" */
/*-no-  */
/*-no- ........................ */
/*-no- ............................... */
/*-no-  */
/*-no- ........... */
/*-no-  */
/*-no- ........................................................................ */
/*-no- ........................................................................... */
/*-no- ........................ */
/*-no- ................... */
/*-no- ........................................ */
/*-no- ........................... */
/*-no- .. */
/*-no-  */
/*-no- ..................................... */
/*-no- ........................................................................... */
/*-no- ................................................................... */
/*-no- ........................................................................ */
/*-no- ........................................... */
/*-no- .............................................. */
/*-no- .................................... */
/*-no- . */
/*-no-  */
/*-no- ............... */
/*-no-  */
/*-no- ....................... */
/*-no-  */
/*-no- ................................................................. */
/*-no- .................................................................... */
/*-no- ................................................ */
/*-no- ............................. */
/*-no- ........................................ */
/*-no- ..................................... */
/*-no- ................ */
/*-no- ... */
/*-no- ..................................................... */
/*-no- .................................................... */
/*-no- ............................................... */
/*-no- .................................................... */
/*-no- ......................................... */
/*-no- ................................. */
/*-no- .................................................... */
/*-no- ........................ */
/*-no- ................................................. */
/*-no- .......................................... */
/*-no- ............................................ */
/*-no- ............................................. */
/*-no- ........................................... */
/*-no- .................................................... */
/*-no- ................ */
/*-no- ... */
/*-no- ........................................................................... */
/*-no- ................. */
/*-no- ............................................................................ */
/*-no- .............................. */
/*-no- . */
/*-no-  */
/*-no- ........................... */

#endif
/* CONSBENCH file end */
