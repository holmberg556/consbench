/* CONSBENCH file begin */
#ifndef MAIN_LIBNONNACL_SRPC_CPP
#define MAIN_LIBNONNACL_SRPC_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_shared_srpc_main_libnonnacl_srpc_cpp, "native_client/src/shared/srpc/main_libnonnacl_srpc.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_shared_srpc_main_libnonnacl_srpc_cpp, "native_client/src/shared/srpc/main_libnonnacl_srpc.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_shared_srpc_invoke_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_shared_srpc_module_init_fini_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_shared_srpc_nacl_srpc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_shared_srpc_nacl_srpc_message_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_shared_srpc_rpc_serialize_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_shared_srpc_rpc_service_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_shared_srpc_rpc_server_loop_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_shared_srpc_utility_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_shared_srpc_main_libnonnacl_srpc_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_shared_srpc_invoke_cpp(it);
  PUBLIC_native_client_src_shared_srpc_module_init_fini_cpp(it);
  PUBLIC_native_client_src_shared_srpc_nacl_srpc_cpp(it);
  PUBLIC_native_client_src_shared_srpc_nacl_srpc_message_cpp(it);
  PUBLIC_native_client_src_shared_srpc_rpc_serialize_cpp(it);
  PUBLIC_native_client_src_shared_srpc_rpc_service_cpp(it);
  PUBLIC_native_client_src_shared_srpc_rpc_server_loop_cpp(it);
  PUBLIC_native_client_src_shared_srpc_utility_cpp(it);
}

#endif

