/* CONSBENCH file begin */
#ifndef MAIN_LIBUTILS_CPP
#define MAIN_LIBUTILS_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_shared_utils_main_libutils_cpp, "native_client/src/shared/utils/main_libutils.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_shared_utils_main_libutils_cpp, "native_client/src/shared/utils/main_libutils.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_shared_utils_flags_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_shared_utils_formatting_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_shared_utils_main_libutils_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_shared_utils_flags_cpp(it);
  PUBLIC_native_client_src_shared_utils_formatting_cpp(it);
}

#endif

