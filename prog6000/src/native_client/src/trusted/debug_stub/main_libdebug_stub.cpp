/* CONSBENCH file begin */
#ifndef MAIN_LIBDEBUG_STUB_CPP
#define MAIN_LIBDEBUG_STUB_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_debug_stub_main_libdebug_stub_cpp, "native_client/src/trusted/debug_stub/main_libdebug_stub.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_debug_stub_main_libdebug_stub_cpp, "native_client/src/trusted/debug_stub/main_libdebug_stub.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_trusted_debug_stub_debug_stub_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_debug_stub_event_common_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_debug_stub_platform_common_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_debug_stub_transport_common_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_debug_stub_posix_debug_stub_posix_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_debug_stub_posix_mutex_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_debug_stub_posix_platform_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_debug_stub_posix_thread_impl_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_trusted_debug_stub_main_libdebug_stub_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_trusted_debug_stub_debug_stub_cpp(it);
  PUBLIC_native_client_src_trusted_debug_stub_event_common_cpp(it);
  PUBLIC_native_client_src_trusted_debug_stub_platform_common_cpp(it);
  PUBLIC_native_client_src_trusted_debug_stub_transport_common_cpp(it);
  PUBLIC_native_client_src_trusted_debug_stub_posix_debug_stub_posix_cpp(it);
  PUBLIC_native_client_src_trusted_debug_stub_posix_mutex_impl_cpp(it);
  PUBLIC_native_client_src_trusted_debug_stub_posix_platform_impl_cpp(it);
  PUBLIC_native_client_src_trusted_debug_stub_posix_thread_impl_cpp(it);
}

#endif

