/* CONSBENCH file begin */
#ifndef MAIN_LIBGDB_RSP_CPP
#define MAIN_LIBGDB_RSP_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_gdb_rsp_main_libgdb_rsp_cpp, "native_client/src/trusted/gdb_rsp/main_libgdb_rsp.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_gdb_rsp_main_libgdb_rsp_cpp, "native_client/src/trusted/gdb_rsp/main_libgdb_rsp.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_trusted_gdb_rsp_abi_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_gdb_rsp_host_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_gdb_rsp_packet_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_gdb_rsp_session_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_gdb_rsp_target_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_gdb_rsp_util_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_trusted_gdb_rsp_main_libgdb_rsp_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_trusted_gdb_rsp_abi_cpp(it);
  PUBLIC_native_client_src_trusted_gdb_rsp_host_cpp(it);
  PUBLIC_native_client_src_trusted_gdb_rsp_packet_cpp(it);
  PUBLIC_native_client_src_trusted_gdb_rsp_session_cpp(it);
  PUBLIC_native_client_src_trusted_gdb_rsp_target_cpp(it);
  PUBLIC_native_client_src_trusted_gdb_rsp_util_cpp(it);
}

#endif

