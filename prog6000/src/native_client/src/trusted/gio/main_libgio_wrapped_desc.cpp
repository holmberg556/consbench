/* CONSBENCH file begin */
#ifndef MAIN_LIBGIO_WRAPPED_DESC_CPP
#define MAIN_LIBGIO_WRAPPED_DESC_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_gio_main_libgio_wrapped_desc_cpp, "native_client/src/trusted/gio/main_libgio_wrapped_desc.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_gio_main_libgio_wrapped_desc_cpp, "native_client/src/trusted/gio/main_libgio_wrapped_desc.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_trusted_gio_gio_shm_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_gio_gio_shm_unbounded_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_gio_gio_nacl_desc_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_trusted_gio_main_libgio_wrapped_desc_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_trusted_gio_gio_shm_cpp(it);
  PUBLIC_native_client_src_trusted_gio_gio_shm_unbounded_cpp(it);
  PUBLIC_native_client_src_trusted_gio_gio_nacl_desc_cpp(it);
}

#endif

