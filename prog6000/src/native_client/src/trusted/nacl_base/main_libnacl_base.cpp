/* CONSBENCH file begin */
#ifndef MAIN_LIBNACL_BASE_CPP
#define MAIN_LIBNACL_BASE_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_nacl_base_main_libnacl_base_cpp, "native_client/src/trusted/nacl_base/main_libnacl_base.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_nacl_base_main_libnacl_base_cpp, "native_client/src/trusted/nacl_base/main_libnacl_base.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_trusted_nacl_base_nacl_refcount_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_trusted_nacl_base_main_libnacl_base_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_trusted_nacl_base_nacl_refcount_cpp(it);
}

#endif

