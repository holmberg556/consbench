/* CONSBENCH file begin */
#ifndef MAIN_LIBSEL_LDR_LAUNCHER_CPP
#define MAIN_LIBSEL_LDR_LAUNCHER_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_nonnacl_util_main_libsel_ldr_launcher_cpp, "native_client/src/trusted/nonnacl_util/main_libsel_ldr_launcher.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_nonnacl_util_main_libsel_ldr_launcher_cpp, "native_client/src/trusted/nonnacl_util/main_libsel_ldr_launcher.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_trusted_nonnacl_util_sel_ldr_launcher_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_nonnacl_util_sel_ldr_launcher_chrome_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_trusted_nonnacl_util_main_libsel_ldr_launcher_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_trusted_nonnacl_util_sel_ldr_launcher_cpp(it);
  PUBLIC_native_client_src_trusted_nonnacl_util_sel_ldr_launcher_chrome_cpp(it);
}

#endif

