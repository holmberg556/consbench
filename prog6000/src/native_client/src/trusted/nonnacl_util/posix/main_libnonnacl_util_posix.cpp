/* CONSBENCH file begin */
#ifndef MAIN_LIBNONNACL_UTIL_POSIX_CPP
#define MAIN_LIBNONNACL_UTIL_POSIX_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_nonnacl_util_posix_main_libnonnacl_util_posix_cpp, "native_client/src/trusted/nonnacl_util/posix/main_libnonnacl_util_posix.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_nonnacl_util_posix_main_libnonnacl_util_posix_cpp, "native_client/src/trusted/nonnacl_util/posix/main_libnonnacl_util_posix.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_trusted_nonnacl_util_posix_get_plugin_dirname_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_nonnacl_util_posix_sel_ldr_launcher_posix_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_trusted_nonnacl_util_posix_main_libnonnacl_util_posix_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_trusted_nonnacl_util_posix_get_plugin_dirname_cpp(it);
  PUBLIC_native_client_src_trusted_nonnacl_util_posix_sel_ldr_launcher_posix_cpp(it);
}

#endif

