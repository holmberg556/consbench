/* CONSBENCH file begin */
#ifndef MAIN_LIBPLATFORM_QUAL_LIB_CPP
#define MAIN_LIBPLATFORM_QUAL_LIB_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_platform_qualify_linux_main_libplatform_qual_lib_cpp, "native_client/src/trusted/platform_qualify/linux/main_libplatform_qual_lib.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_platform_qualify_linux_main_libplatform_qual_lib_cpp, "native_client/src/trusted/platform_qualify/linux/main_libplatform_qual_lib.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_trusted_platform_qualify_linux_nacl_os_qualify_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_platform_qualify_linux_sysv_shm_and_mmap_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_platform_qualify_posix_nacl_dep_qualify_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_platform_qualify_arch_x86_nacl_cpuwhitelist_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_platform_qualify_arch_x86_64_nacl_dep_qualify_arch_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_trusted_platform_qualify_linux_main_libplatform_qual_lib_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_trusted_platform_qualify_linux_nacl_os_qualify_cpp(it);
  PUBLIC_native_client_src_trusted_platform_qualify_linux_sysv_shm_and_mmap_cpp(it);
  PUBLIC_native_client_src_trusted_platform_qualify_posix_nacl_dep_qualify_cpp(it);
  PUBLIC_native_client_src_trusted_platform_qualify_arch_x86_nacl_cpuwhitelist_cpp(it);
  PUBLIC_native_client_src_trusted_platform_qualify_arch_x86_64_nacl_dep_qualify_arch_cpp(it);
}

#endif

