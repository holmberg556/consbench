/* CONSBENCH file begin */
#ifndef MAIN_LIBSERVICE_RUNTIME_X86_COMMON_CPP
#define MAIN_LIBSERVICE_RUNTIME_X86_COMMON_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_service_runtime_arch_x86_main_libservice_runtime_x86_common_cpp, "native_client/src/trusted/service_runtime/arch/x86/main_libservice_runtime_x86_common.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_service_runtime_arch_x86_main_libservice_runtime_x86_common_cpp, "native_client/src/trusted/service_runtime/arch/x86/main_libservice_runtime_x86_common.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_trusted_service_runtime_arch_x86_nacl_ldt_x86_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_trusted_service_runtime_arch_x86_main_libservice_runtime_x86_common_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_trusted_service_runtime_arch_x86_nacl_ldt_x86_cpp(it);
}

#endif

