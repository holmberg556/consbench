/* CONSBENCH file begin */
#ifndef SEL_LDR_X86_H_4116
#define SEL_LDR_X86_H_4116

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_service_runtime_arch_x86_sel_ldr_x86_h, "native_client/src/trusted/service_runtime/arch/x86/sel_ldr_x86.h");

/* CONSBENCH includes begin */
#include "native_client/src/trusted/service_runtime/arch/x86/nacl_ldt_x86.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_service_runtime_arch_x86_sel_ldr_x86_h, "native_client/src/trusted/service_runtime/arch/x86/sel_ldr_x86.h");

/*-no- ... */
/*-no- ................................................................. */
/*-no- ...................................................................... */
/*-no- ................................ */
/*-no- .... */
/*-no-  */
/*-no- #ifndef SERVICE_RUNTIME_ARCH_X86_SEL_LDR_H__ */
/*-no- #define SERVICE_RUNTIME_ARCH_X86_SEL_LDR_H__ 1 */
/*-no-  */
/*-no- ..................................... */
/*-no- #if NACL_WINDOWS */
/*-no- # define LDT_ENTRIES 8192 */
/*-no- #elif NACL_OSX */
/*-no- # define LDT_ENTRIES 8192 */
/*-no- #elif NACL_LINUX */
/*-no- # include <asm/ldt.h> */
/*-no- #endif */
/*-no-  */
/*-no- #include "native_client/src/trusted/service_runtime/arch/x86/nacl_ldt_x86.h" */
/*-no-  */
/*-no-  */
/*-no- #if NACL_BUILD_SUBARCH == 32 */
/*-no- # define NACL_MAX_ADDR_BITS  (30) */
/*-no- # define NACL_THREAD_MAX     LDT_ENTRIES  /-* cannot be larger *-/ */
/*-no- #elif NACL_BUILD_SUBARCH == 64 */
/*-no- # define NACL_MAX_ADDR_BITS  (32) */
/*-no- # define NACL_THREAD_MAX     LDT_ENTRIES  /-* can be larger *-/ */
/*-no- #else */
/*-no- # error "Did Intel or AMD introduce the 128-bit x86?" */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_NOOP_OPCODE    0x90 */
/*-no- #define NACL_HALT_OPCODE    0xf4 */
/*-no- #define NACL_HALT_LEN       1           /-* length of halt instruction *-/ */
/*-no- #define NACL_HALT_WORD      0xf4f4f4f4U */
/*-no-  */
/*-no- #endif /-* SERVICE_RUNTIME_ARCH_X86_SEL_LDR_H__ *-/ */

#endif
/* CONSBENCH file end */
