/* CONSBENCH file begin */
#ifndef NACL_SYSCALLS_H_4158
#define NACL_SYSCALLS_H_4158

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_service_runtime_include_bits_nacl_syscalls_h, "native_client/src/trusted/service_runtime/include/bits/nacl_syscalls.h");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_service_runtime_include_bits_nacl_syscalls_h, "native_client/src/trusted/service_runtime/include/bits/nacl_syscalls.h");

/*-no- ... */
/*-no- ................................................................. */
/*-no- ...................................................................... */
/*-no- ................................ */
/*-no- .... */
/*-no-  */
/*-no- ... */
/*-no- ..................................................... */
/*-no- .... */
/*-no-  */
/*-no- #ifndef NATIVE_CLIENT_SRC_TRUSTED_SERVICE_RUNTIME_INCLUDE_BITS_NACL_SYSCALLS_H_ */
/*-no- #define NATIVE_CLIENT_SRC_TRUSTED_SERVICE_RUNTIME_INCLUDE_BITS_NACL_SYSCALLS_H_ */
/*-no-  */
/*-no- .................................... */
/*-no-  */
/*-no- ... */
/*-no- ................................................................... */
/*-no- .... */
/*-no-  */
/*-no- #define NACL_sys_null                    1 */
/*-no-  */
/*-no- #define NACL_sys_dup                     8 */
/*-no- #define NACL_sys_dup2                    9 */
/*-no- #define NACL_sys_open                   10 */
/*-no- #define NACL_sys_close                  11 */
/*-no- #define NACL_sys_read                   12 */
/*-no- #define NACL_sys_write                  13 */
/*-no- #define NACL_sys_lseek                  14 */
/*-no- #define NACL_sys_ioctl                  15 */
/*-no- #define NACL_sys_stat                   16 */
/*-no- #define NACL_sys_fstat                  17 */
/*-no- #define NACL_sys_chmod                  18 */
/*-no- ...................................... */
/*-no-  */
/*-no- #define NACL_sys_sysbrk                 20 */
/*-no- #define NACL_sys_mmap                   21 */
/*-no- #define NACL_sys_munmap                 22 */
/*-no-  */
/*-no- #define NACL_sys_getdents               23 */
/*-no-  */
/*-no- #define NACL_sys_exit                   30 */
/*-no- #define NACL_sys_getpid                 31 */
/*-no- #define NACL_sys_sched_yield            32 */
/*-no- #define NACL_sys_sysconf                33 */
/*-no-  */
/*-no- #define NACL_sys_gettimeofday           40 */
/*-no- #define NACL_sys_clock                  41 */
/*-no- #define NACL_sys_nanosleep              42 */
/*-no-  */
/*-no- ..................................................... */
/*-no-  */
/*-no- #define NACL_sys_imc_makeboundsock      60 */
/*-no- #define NACL_sys_imc_accept             61 */
/*-no- #define NACL_sys_imc_connect            62 */
/*-no- #define NACL_sys_imc_sendmsg            63 */
/*-no- #define NACL_sys_imc_recvmsg            64 */
/*-no- #define NACL_sys_imc_mem_obj_create     65 */
/*-no- #define NACL_sys_imc_socketpair         66 */
/*-no-  */
/*-no- #define NACL_sys_mutex_create           70 */
/*-no- #define NACL_sys_mutex_lock             71 */
/*-no- #define NACL_sys_mutex_trylock          72 */
/*-no- #define NACL_sys_mutex_unlock           73 */
/*-no- #define NACL_sys_cond_create            74 */
/*-no- #define NACL_sys_cond_wait              75 */
/*-no- #define NACL_sys_cond_signal            76 */
/*-no- #define NACL_sys_cond_broadcast         77 */
/*-no- #define NACL_sys_cond_timed_wait_abs    79 */
/*-no-  */
/*-no- #define NACL_sys_thread_create          80 */
/*-no- #define NACL_sys_thread_exit            81 */
/*-no- #define NACL_sys_tls_init               82 */
/*-no- #define NACL_sys_thread_nice            83 */
/*-no- #define NACL_sys_tls_get                84 */
/*-no-  */
/*-no- #define NACL_sys_sem_create             100 */
/*-no- #define NACL_sys_sem_wait               101 */
/*-no- #define NACL_sys_sem_post               102 */
/*-no- #define NACL_sys_sem_get_value          103 */
/*-no-  */
/*-no- ... */
/*-no- ................................................................ */
/*-no- .... */
/*-no- #define NACL_sys_dyncode_copy           104 */
/*-no-  */
/*-no- #define NACL_sys_dyncode_create         104 */
/*-no- #define NACL_sys_dyncode_modify         105 */
/*-no- #define NACL_sys_dyncode_delete         106 */
/*-no-  */
/*-no- #define NACL_MAX_SYSCALLS               110 */
/*-no-  */
/*-no- #endif */

#endif
/* CONSBENCH file end */
