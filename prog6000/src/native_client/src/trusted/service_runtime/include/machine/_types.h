/* CONSBENCH file begin */
#ifndef _TYPES_H_4161
#define _TYPES_H_4161

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_service_runtime_include_machine__types_h, "native_client/src/trusted/service_runtime/include/machine/_types.h");

/* CONSBENCH includes begin */
# include "native_client/src/include/portability.h"
#include <stddef.h>
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_service_runtime_include_machine__types_h, "native_client/src/trusted/service_runtime/include/machine/_types.h");

/*-no- ... */
/*-no- ................................................................. */
/*-no- ...................................................................... */
/*-no- ................................ */
/*-no- .... */
/*-no-  */
/*-no- ... */
/*-no- .................................................. */
/*-no- ......................................................... */
/*-no- .... */
/*-no-  */
/*-no- #ifndef NATIVE_CLIENT_SRC_TRUSTED_SERVICE_RUNTIME_INCLUDE_MACHINE__TYPES_H_ */
/*-no- #define NATIVE_CLIENT_SRC_TRUSTED_SERVICE_RUNTIME_INCLUDE_MACHINE__TYPES_H_ */
/*-no-  */
/*-no- #ifdef __native_client__ */
/*-no- # include <stdint.h> */
/*-no- # include <machine/_default_types.h> */
/*-no- #else */
/*-no- # include "native_client/src/include/portability.h" */
/*-no- #endif */
/*-no-  */
/*-no- #define __need_size_t */
/*-no- #include <stddef.h> */
/*-no-  */
/*-no- #ifndef NULL */
/*-no- #define NULL 0 */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_ABI_WORDSIZE 32 */
/*-no-  */
/*-no- #define NACL_ABI_MAKE_WORDSIZE_TYPE(T)  T ## NACL_ABI_WORDSIZE ## _t */
/*-no- #define NACL_ABI_SIGNED_WORD            NACL_ABI_MAKE_WORDSIZE_TYPE(int) */
/*-no- #define NACL_ABI_UNSIGNED_WORD          NACL_ABI_MAKE_WORDSIZE_TYPE(uint) */
/*-no-  */
/*-no- #ifndef __native_client__ */
/*-no- # if (NACL_ABI_WORDSIZE == NACL_HOST_WORDSIZE) */
/*-no- #   define NACL_ABI_WORDSIZE_IS_NATIVE 1 */
/*-no- # else */
/*-no- #   define NACL_ABI_WORDSIZE_IS_NATIVE 0 */
/*-no- # endif */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_CONCAT3_(a, b, c) a ## b ## c */
/*-no- #define NACL_PRI_(fmt, size) NACL_CONCAT3_(NACL_PRI, fmt, size) */
/*-no-  */
/*-no- #ifndef nacl_abi___dev_t_defined */
/*-no- #define nacl_abi___dev_t_defined */
/*-no- ................................. */
/*-no- #ifndef __native_client__ */
/*-no- ........................................ */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_PRIdNACL_DEV NACL_PRI_(d, 64) */
/*-no- #define NACL_PRIiNACL_DEV NACL_PRI_(i, 64) */
/*-no- #define NACL_PRIoNACL_DEV NACL_PRI_(o, 64) */
/*-no- #define NACL_PRIuNACL_DEV NACL_PRI_(u, 64) */
/*-no- #define NACL_PRIxNACL_DEV NACL_PRI_(x, 64) */
/*-no- #define NACL_PRIXNACL_DEV NACL_PRI_(X, 64) */
/*-no-  */
/*-no- #define NACL_ABI_DEV_T_MIN ((nacl_abi_dev_t) 1 << 63) */
/*-no- #define NACL_ABI_DEV_T_MAX (~((nacl_abi_dev_t) 1 << 63)) */
/*-no-  */
/*-no- #ifndef nacl_abi___ino_t_defined */
/*-no- #define nacl_abi___ino_t_defined */
/*-no- .................................. */
/*-no- #ifndef __native_client__ */
/*-no- ........................................ */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_PRIdNACL_INO NACL_PRI_(d, 64) */
/*-no- #define NACL_PRIiNACL_INO NACL_PRI_(i, 64) */
/*-no- #define NACL_PRIoNACL_INO NACL_PRI_(o, 64) */
/*-no- #define NACL_PRIuNACL_INO NACL_PRI_(u, 64) */
/*-no- #define NACL_PRIxNACL_INO NACL_PRI_(x, 64) */
/*-no- #define NACL_PRIXNACL_INO NACL_PRI_(X, 64) */
/*-no-  */
/*-no- #define NACL_ABI_INO_T_MIN ((nacl_abi_ino_t) 0) */
/*-no- #define NACL_ABI_INO_T_MAX ((nacl_abi_ino_t) -1) */
/*-no-  */
/*-no- #ifndef nacl_abi___mode_t_defined */
/*-no- #define nacl_abi___mode_t_defined */
/*-no- ................................... */
/*-no- #ifndef __native_client__ */
/*-no- .......................................... */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_PRIdNACL_MODE NACL_PRI_(d, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIiNACL_MODE NACL_PRI_(i, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIoNACL_MODE NACL_PRI_(o, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIuNACL_MODE NACL_PRI_(u, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIxNACL_MODE NACL_PRI_(x, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIXNACL_MODE NACL_PRI_(X, NACL_ABI_WORDSIZE) */
/*-no-  */
/*-no- #define NACL_ABI_MODE_T_MIN ((nacl_abi_mode_t) 0) */
/*-no- #define NACL_ABI_MODE_T_MAX ((nacl_abi_mode_t) -1) */
/*-no-  */
/*-no- #ifndef nacl_abi___nlink_t_defined */
/*-no- #define nacl_abi___nlink_t_defined */
/*-no- .................................... */
/*-no- #ifndef __native_client__ */
/*-no- ............................................ */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_PRIdNACL_NLINK NACL_PRI_(d, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIiNACL_NLINK NACL_PRI_(i, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIoNACL_NLINK NACL_PRI_(o, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIuNACL_NLINK NACL_PRI_(u, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIxNACL_NLINK NACL_PRI_(x, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIXNACL_NLINK NACL_PRI_(X, NACL_ABI_WORDSIZE) */
/*-no-  */
/*-no- #define NACL_ABI_NLINK_T_MIN ((nacl_abi_nlink_t) 0) */
/*-no- #define NACL_ABI_NLINK_T_MAX ((nacl_abi_nlink_t) -1) */
/*-no-  */
/*-no- #ifndef nacl_abi___uid_t_defined */
/*-no- #define nacl_abi___uid_t_defined */
/*-no- .................................. */
/*-no- #ifndef __native_client__ */
/*-no- ........................................ */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_PRIdNACL_UID NACL_PRI_(d, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIiNACL_UID NACL_PRI_(i, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIoNACL_UID NACL_PRI_(o, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIuNACL_UID NACL_PRI_(u, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIxNACL_UID NACL_PRI_(x, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIXNACL_UID NACL_PRI_(X, NACL_ABI_WORDSIZE) */
/*-no-  */
/*-no- #define NACL_ABI_UID_T_MIN ((nacl_abi_uid_t) 0) */
/*-no- #define NACL_ABI_UID_T_MAX ((nacl_abi_uid_t) -1) */
/*-no-  */
/*-no- #ifndef nacl_abi___gid_t_defined */
/*-no- #define nacl_abi___gid_t_defined */
/*-no- .................................. */
/*-no- #ifndef __native_client__ */
/*-no- ........................................ */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_PRIdNACL_GID NACL_PRI_(d, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIiNACL_GID NACL_PRI_(i, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIoNACL_GID NACL_PRI_(o, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIuNACL_GID NACL_PRI_(u, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIxNACL_GID NACL_PRI_(x, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIXNACL_GID NACL_PRI_(X, NACL_ABI_WORDSIZE) */
/*-no-  */
/*-no- #define NACL_ABI_GID_T_MIN ((nacl_abi_gid_t) 0) */
/*-no- #define NACL_ABI_GID_T_MAX ((nacl_abi_gid_t) -1) */
/*-no-  */
/*-no- #ifndef nacl_abi___off_t_defined */
/*-no- #define nacl_abi___off_t_defined */
/*-no- ................................ */
/*-no- #ifndef __native_client__ */
/*-no- ....................................... */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_PRIdNACL_OFF NACL_PRI_(d, 64) */
/*-no- #define NACL_PRIiNACL_OFF NACL_PRI_(i, 64) */
/*-no- #define NACL_PRIoNACL_OFF NACL_PRI_(o, 64) */
/*-no- #define NACL_PRIuNACL_OFF NACL_PRI_(u, 64) */
/*-no- #define NACL_PRIxNACL_OFF NACL_PRI_(x, 64) */
/*-no- #define NACL_PRIXNACL_OFF NACL_PRI_(X, 64) */
/*-no-  */
/*-no- #define NACL_ABI_OFF_T_MIN ((nacl_abi_off_t) 1 << 63) */
/*-no- #define NACL_ABI_OFF_T_MAX (~((nacl_abi_off_t) 1 << 63)) */
/*-no-  */
/*-no- #ifndef nacl_abi___off64_t_defined */
/*-no- #define nacl_abi___off64_t_defined */
/*-no- .................................. */
/*-no- #ifndef __native_client__ */
/*-no- ........................................... */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_PRIdNACL_OFF64 NACL_PRI_(d, 64) */
/*-no- #define NACL_PRIiNACL_OFF64 NACL_PRI_(i, 64) */
/*-no- #define NACL_PRIoNACL_OFF64 NACL_PRI_(o, 64) */
/*-no- #define NACL_PRIuNACL_OFF64 NACL_PRI_(u, 64) */
/*-no- #define NACL_PRIxNACL_OFF64 NACL_PRI_(x, 64) */
/*-no- #define NACL_PRIXNACL_OFF64 NACL_PRI_(X, 64) */
/*-no-  */
/*-no- #define NACL_ABI_OFF64_T_MIN ((nacl_abi_off64_t) 1 << 63) */
/*-no- #define NACL_ABI_OFF64_T_MAX (~((nacl_abi_off64_t) 1 << 63)) */
/*-no-  */
/*-no-  */
/*-no- #if !(defined(__GLIBC__) && defined(__native_client__)) */
/*-no-  */
/*-no- #ifndef nacl_abi___blksize_t_defined */
/*-no- #define nacl_abi___blksize_t_defined */
/*-no- ..................................... */
/*-no- ................................................ */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_PRIdNACL_BLKSIZE NACL_PRI_(d, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIiNACL_BLKSIZE NACL_PRI_(i, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIoNACL_BLKSIZE NACL_PRI_(o, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIuNACL_BLKSIZE NACL_PRI_(u, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIxNACL_BLKSIZE NACL_PRI_(x, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIXNACL_BLKSIZE NACL_PRI_(X, NACL_ABI_WORDSIZE) */
/*-no-  */
/*-no- #endif */
/*-no-  */
/*-no-  */
/*-no- #define NACL_ABI_BLKSIZE_T_MIN \ */
/*-no- ..................................................... */
/*-no- #define NACL_ABI_BLKSIZE_T_MAX \ */
/*-no- ........................................................ */
/*-no-  */
/*-no- #ifndef nacl_abi___blkcnt_t_defined */
/*-no- #define nacl_abi___blkcnt_t_defined */
/*-no- .................................... */
/*-no- .............................................. */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_PRIdNACL_BLKCNT NACL_PRI_(d, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIiNACL_BLKCNT NACL_PRI_(i, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIoNACL_BLKCNT NACL_PRI_(o, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIuNACL_BLKCNT NACL_PRI_(u, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIxNACL_BLKCNT NACL_PRI_(x, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIXNACL_BLKCNT NACL_PRI_(X, NACL_ABI_WORDSIZE) */
/*-no-  */
/*-no- #define NACL_ABI_BLKCNT_T_MIN \ */
/*-no- .................................................... */
/*-no- #define NACL_ABI_BLKCNT_T_MAX \ */
/*-no- ....................................................... */
/*-no-  */
/*-no- #ifndef nacl_abi___time_t_defined */
/*-no- #define nacl_abi___time_t_defined */
/*-no- ........................................ */
/*-no- .......................................... */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_PRIdNACL_TIME NACL_PRI_(d, 64) */
/*-no- #define NACL_PRIiNACL_TIME NACL_PRI_(i, 64) */
/*-no- #define NACL_PRIoNACL_TIME NACL_PRI_(o, 64) */
/*-no- #define NACL_PRIuNACL_TIME NACL_PRI_(u, 64) */
/*-no- #define NACL_PRIxNACL_TIME NACL_PRI_(x, 64) */
/*-no- #define NACL_PRIXNACL_TIME NACL_PRI_(X, 64) */
/*-no-  */
/*-no- #define NACL_ABI_TIME_T_MIN ((nacl_abi_time_t) 1 << 63) */
/*-no- #define NACL_ABI_TIME_T_MAX (~((nacl_abi_time_t) 1 << 63)) */
/*-no-  */
/*-no- ... */
/*-no- .................................................................... */
/*-no- ....................................... */
/*-no- .................................................... */
/*-no- .... */
/*-no- #define NACL_NO_STRIP(t) nacl_ ## abi_ ## t */
/*-no-  */
/*-no- ... */
/*-no- ...................................................................... */
/*-no- ........................................................................... */
/*-no- ........................................................................ */
/*-no- ....................................................................... */
/*-no- ........................................ */
/*-no- .... */
/*-no- #if defined(__native_client__) && !defined(NACL_ABI_WORDSIZE) */
/*-no- #define NACL_ABI_WORDSIZE WORDSIZE */
/*-no- #endif  /-* defined(__native_client__) && !defined(NACL_ABI_WORDSIZE) *-/ */
/*-no-  */
/*-no- #ifndef nacl_abi_size_t_defined */
/*-no- #define nacl_abi_size_t_defined */
/*-no- ....................................... */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_ABI_SIZE_T_MIN ((nacl_abi_size_t) 0) */
/*-no- #define NACL_ABI_SIZE_T_MAX ((nacl_abi_size_t) -1) */
/*-no-  */
/*-no- #ifndef nacl_abi_ssize_t_defined */
/*-no- #define nacl_abi_ssize_t_defined */
/*-no- ....................................... */
/*-no- #endif */
/*-no-  */
/*-no- #define NACL_ABI_SSIZE_T_MAX \ */
/*-no- ................................................. */
/*-no- #define NACL_ABI_SSIZE_T_MIN \ */
/*-no- ......................... */
/*-no-  */
/*-no- #define NACL_PRIdNACL_SIZE NACL_PRI_(d, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIiNACL_SIZE NACL_PRI_(i, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIoNACL_SIZE NACL_PRI_(o, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIuNACL_SIZE NACL_PRI_(u, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIxNACL_SIZE NACL_PRI_(x, NACL_ABI_WORDSIZE) */
/*-no- #define NACL_PRIXNACL_SIZE NACL_PRI_(X, NACL_ABI_WORDSIZE) */
/*-no-  */
/*-no- .... */
/*-no- ..................................................................... */
/*-no- ..................... */
/*-no- .. */
/*-no- ..................................................................... */
/*-no- .......................................................................... */
/*-no- ............................................................................... */
/*-no- .. */
/*-no- ............................................................. */
/*-no- .... */
/*-no- #ifdef __native_client__ */
/*-no- ...... */
/*-no- ........................................................................ */
/*-no- ............................................................................. */
/*-no- .......................................................................... */
/*-no- ...... */
/*-no-   #define nacl_abi_size_t_saturate(x) (x) */
/*-no-   #define nacl_abi_ssize_t_saturate(x) (x) */
/*-no- #else /-* __native_client *-/ */
/*-no- .................................................................... */
/*-no- .................................. */
/*-no- ................................. */
/*-no- ............ */
/*-no- ................................ */
/*-no- ..... */
/*-no- ... */
/*-no-  */
/*-no- ....................................................................... */
/*-no- ................................... */
/*-no- .................................. */
/*-no- .......................................... */
/*-no- .................................. */
/*-no- ............ */
/*-no- .................................. */
/*-no- ..... */
/*-no- ... */
/*-no- #endif /-* __native_client *-/ */
/*-no-  */
/*-no- #undef NACL_NO_STRIP */
/*-no-  */
/*-no- #endif */

#endif
/* CONSBENCH file end */
