/* CONSBENCH file begin */
#ifndef MAIN_LIBENV_CLEANSER_CPP
#define MAIN_LIBENV_CLEANSER_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_service_runtime_main_libenv_cleanser_cpp, "native_client/src/trusted/service_runtime/main_libenv_cleanser.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_service_runtime_main_libenv_cleanser_cpp, "native_client/src/trusted/service_runtime/main_libenv_cleanser.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_trusted_service_runtime_env_cleanser_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_trusted_service_runtime_main_libenv_cleanser_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_trusted_service_runtime_env_cleanser_cpp(it);
}

#endif

