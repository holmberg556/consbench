/* CONSBENCH file begin */
#ifndef MAIN_LIBSEL_CPP
#define MAIN_LIBSEL_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_service_runtime_main_libsel_cpp, "native_client/src/trusted/service_runtime/main_libsel.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_service_runtime_main_libsel_cpp, "native_client/src/trusted/service_runtime/main_libsel.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_trusted_service_runtime_dyn_array_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_nacl_all_modules_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_nacl_app_thread_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_nacl_bottom_half_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_nacl_closure_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_nacl_debug_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_nacl_desc_effector_ldr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_nacl_globals_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_nacl_memory_object_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_nacl_signal_common_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_nacl_sync_queue_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_nacl_syscall_common_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_nacl_syscall_hook_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_nacl_text_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_sel_addrspace_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_sel_ldr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_sel_ldr_inl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_sel_ldr_standard_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_elf_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_sel_main_chrome_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_sel_mem_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_sel_qualify_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_sel_util_inl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_sel_validate_image_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_linux_sel_memory_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_linux_nacl_thread_nice_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_linux_x86_nacl_ldt_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_linux_x86_sel_segments_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_service_runtime_posix_nacl_signal_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_target_geni_nacl_syscall_handlers_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_trusted_service_runtime_main_libsel_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_trusted_service_runtime_dyn_array_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_nacl_all_modules_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_nacl_app_thread_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_nacl_bottom_half_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_nacl_closure_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_nacl_debug_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_nacl_desc_effector_ldr_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_nacl_globals_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_nacl_memory_object_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_nacl_signal_common_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_nacl_sync_queue_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_nacl_syscall_common_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_nacl_syscall_hook_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_nacl_text_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_sel_addrspace_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_sel_ldr_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_sel_ldr_inl_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_sel_ldr_standard_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_elf_util_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_sel_main_chrome_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_sel_mem_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_sel_qualify_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_sel_util_inl_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_sel_validate_image_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_linux_sel_memory_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_linux_nacl_thread_nice_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_linux_x86_nacl_ldt_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_linux_x86_sel_segments_cpp(it);
  PUBLIC_native_client_src_trusted_service_runtime_posix_nacl_signal_cpp(it);
  PUBLIC_out_Debug_obj_target_geni_nacl_syscall_handlers_cpp(it);
}

#endif

