/* CONSBENCH file begin */
#ifndef SEL_LDR_H_4237
#define SEL_LDR_H_4237

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_service_runtime_sel_ldr_h, "native_client/src/trusted/service_runtime/sel_ldr.h");

/* CONSBENCH includes begin */
#include "native_client/src/include/nacl_base.h"
#include "native_client/src/include/portability.h"
#include "native_client/src/include/elf.h"
#include "native_client/src/shared/platform/nacl_host_desc.h"
#include "native_client/src/shared/platform/nacl_log.h"
#include "native_client/src/shared/platform/nacl_threads.h"
#include "native_client/src/trusted/service_runtime/dyn_array.h"
#include "native_client/src/trusted/service_runtime/nacl_config_dangerous.h"
#include "native_client/src/trusted/service_runtime/nacl_error_code.h"
#include "native_client/src/trusted/service_runtime/nacl_sync_queue.h"
#include "native_client/src/trusted/service_runtime/sel_mem.h"
#include "native_client/src/trusted/service_runtime/sel_util.h"
#include "native_client/src/trusted/service_runtime/sel_rt.h"
#include "native_client/src/trusted/service_runtime/sel_ldr-inl.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_service_runtime_sel_ldr_h, "native_client/src/trusted/service_runtime/sel_ldr.h");

/*-no- ... */
/*-no- .................................................................. */
/*-no- ...................................................................... */
/*-no- ................................ */
/*-no- .... */
/*-no-  */
/*-no- ... */
/*-no- ............................................ */
/*-no- .. */
/*-no- ................................................................... */
/*-no- ......................................................... */
/*-no- .. */
/*-no- ............................................................. */
/*-no- .................................................................. */
/*-no- ............................................................. */
/*-no- ..................... */
/*-no- .. */
/*-no- .................................................................. */
/*-no- .................................................................... */
/*-no- .......................................................... */
/*-no- .. */
/*-no- ...................................................................... */
/*-no- .......................................................... */
/*-no- ..................................................................... */
/*-no- .............................................. */
/*-no- .... */
/*-no-  */
/*-no- #ifndef NATIVE_CLIENT_SRC_TRUSTED_SERVICE_RUNTIME_SEL_LDR_H_ */
/*-no- #define NATIVE_CLIENT_SRC_TRUSTED_SERVICE_RUNTIME_SEL_LDR_H_ 1 */
/*-no-  */
/*-no- #include "native_client/src/include/nacl_base.h" */
/*-no- #include "native_client/src/include/portability.h" */
/*-no- #include "native_client/src/include/elf.h" */
/*-no-  */
/*-no- #include "native_client/src/shared/platform/nacl_host_desc.h" */
/*-no- #include "native_client/src/shared/platform/nacl_log.h" */
/*-no- #include "native_client/src/shared/platform/nacl_threads.h" */
/*-no-  */
/*-no- #include "native_client/src/trusted/service_runtime/dyn_array.h" */
/*-no- #include "native_client/src/trusted/service_runtime/nacl_config_dangerous.h" */
/*-no- #include "native_client/src/trusted/service_runtime/nacl_error_code.h" */
/*-no- #include "native_client/src/trusted/service_runtime/nacl_sync_queue.h" */
/*-no- #include "native_client/src/trusted/service_runtime/sel_mem.h" */
/*-no- #include "native_client/src/trusted/service_runtime/sel_util.h" */
/*-no- #include "native_client/src/trusted/service_runtime/sel_rt.h" */
/*-no-  */
/*-no-  */
/*-no- .............. */
/*-no-  */
/*-no- #define NACL_SERVICE_PORT_DESCRIPTOR    3 */
/*-no- #define NACL_SERVICE_ADDRESS_DESCRIPTOR 4 */
/*-no-  */
/*-no- #define NACL_DEFAULT_ALLOC_MAX  (32 << 20)  /-* total brk and mmap allocs *-/ */
/*-no- #define NACL_DEFAULT_STACK_MAX  (16 << 20)  /-* main thread stack *-/ */
/*-no-  */
/*-no- #define NACL_SANDBOX_CHROOT_FD  "SBX_D" */
/*-no-  */
/*-no- #if NACL_WINDOWS */
/*-no- #define WINDOWS_EXCEPTION_TRY do { __try { */
/*-no- #define WINDOWS_EXCEPTION_CATCH } __except(EXCEPTION_EXECUTE_HANDLER) { \ */
/*-no- ...................................................... */
/*-no- ......................................................................... */
/*-no- ............................................ */
/*-no- ................................... */
/*-no- ......................................... */
/*-no- #else */
/*-no- #define WINDOWS_EXCEPTION_TRY do { */
/*-no- #define WINDOWS_EXCEPTION_CATCH } while (0) */
/*-no- #endif */
/*-no-  */
/*-no- ..................... */
/*-no- ............................................................................. */
/*-no- ......................... */
/*-no-  */
/*-no- ................ */
/*-no- ..... */
/*-no- .............................................. */
/*-no- ...... */
/*-no- ...................................... */
/*-no- ........................................... */
/*-no- ....................................... */
/*-no- ..... */
/*-no- .............................................................. */
/*-no- ............................................................. */
/*-no- .................................................................. */
/*-no- ................................................................... */
/*-no- ................................................................ */
/*-no- ............................................................... */
/*-no- ........................................................ */
/*-no- .... */
/*-no- ................................................................... */
/*-no- ............................................................... */
/*-no- ................................................................ */
/*-no- ................................................................ */
/*-no- ....................... */
/*-no- ...... */
/*-no-  */
/*-no- ..... */
/*-no- .................................................................... */
/*-no- ............................................................... */
/*-no- ................................................................... */
/*-no- .................. */
/*-no- ...... */
/*-no- ...................................... */
/*-no-  */
/*-no- ..... */
/*-no- ............................................ */
/*-no- .............................................................. */
/*-no- ...... */
/*-no- ...................................... */
/*-no-  */
/*-no- #if NACL_ARCH(NACL_BUILD_ARCH) == NACL_x86 && NACL_BUILD_SUBARCH == 32 && __PIC__ */
/*-no- ........................................ */
/*-no- #endif */
/*-no-  */
/*-no- .................................................... */
/*-no- ........................................................................... */
/*-no- .............................................. */
/*-no-  */
/*-no- ..... */
/*-no- ...................................................................... */
/*-no- ...................................................................... */
/*-no- .......... */
/*-no- ...... */
/*-no- ............................................... */
/*-no- ............................................. */
/*-no-  */
/*-no- ..... */
/*-no- ................................................................... */
/*-no- ............................... */
/*-no- ...... */
/*-no- ....................................................................... */
/*-no- ........................................................................... */
/*-no- ..... */
/*-no- ................................................................. */
/*-no- .............................................................. */
/*-no- ................................................................ */
/*-no- ...................................................................... */
/*-no- ...................................................................... */
/*-no- ................................................................. */
/*-no- .......................................................... */
/*-no- ...... */
/*-no-  */
/*-no- ..................................... */
/*-no- .............................. */
/*-no-  */
/*-no- ..................................... */
/*-no-  */
/*-no- ..... */
/*-no- .................................................................... */
/*-no- .............................................................. */
/*-no- ...... */
/*-no- ........................................ */
/*-no-  */
/*-no- .................................................................... */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ..... */
/*-no- ........................................................................... */
/*-no- ....................... */
/*-no- ...... */
/*-no-  */
/*-no- ..... */
/*-no- ...................................................................... */
/*-no- ...................................................................... */
/*-no- .................................. */
/*-no- ...... */
/*-no- .......................................... */
/*-no- ............................................. */
/*-no-  */
/*-no- ............................................ */
/*-no- ................................................................................. */
/*-no-  */
/*-no- ............................... */
/*-no- ............................... */
/*-no- ............................................... */
/*-no- ............................................. */
/*-no-  */
/*-no- ..... */
/*-no- ..................................................................... */
/*-no- .................................................................. */
/*-no- .......................................................... */
/*-no- .................................................................. */
/*-no- ...................................................... */
/*-no- ..................................................................... */
/*-no- ............ */
/*-no- ...... */
/*-no-  */
/*-no- ..... */
/*-no- ..................................... */
/*-no- ...... */
/*-no- .................................... */
/*-no-  */
/*-no- ..... */
/*-no- .............................................................................. */
/*-no- ...... */
/*-no- ..................................................... */
/*-no- ...................................... */
/*-no- ............................................... */
/*-no-  */
/*-no- ..... */
/*-no- ............................................................... */
/*-no- ..................................................... */
/*-no- ...... */
/*-no- ............................................. */
/*-no- ................................................ */
/*-no- ...................................................... */
/*-no-  */
/*-no- ..... */
/*-no- ...................................................................... */
/*-no- ..................................................................... */
/*-no- ..................................................... */
/*-no- ...... */
/*-no- .................................................... */
/*-no- .................................................. */
/*-no- ................................................. */
/*-no-  */
/*-no- ..... */
/*-no- ................................................................. */
/*-no- ..................................................... */
/*-no- ...... */
/*-no- ...................................................... */
/*-no-  */
/*-no-  */
/*-no- .................................... */
/*-no- ........................................ */
/*-no-  */
/*-no- .................................................... */
/*-no- .................................................... */
/*-no- ...................................................................... */
/*-no- ....................................... */
/*-no-  */
/*-no- #if NACL_ARCH(NACL_BUILD_ARCH) == NACL_x86 && NACL_BUILD_SUBARCH == 32 */
/*-no- ......................................... */
/*-no- ......................................... */
/*-no- #endif */
/*-no-  */
/*-no- ........................................................... */
/*-no- ................................................ */
/*-no-  */
/*-no- ..... */
/*-no- ..................................................................... */
/*-no- ................................................................... */
/*-no- .................................... */
/*-no- ...... */
/*-no- ....................................... */
/*-no- ....................................... */
/*-no- ..................................................................... */
/*-no- ......................................................................... */
/*-no-  */
/*-no- .................................... */
/*-no- ................................................................ */
/*-no- .. */
/*-no-  */
/*-no-  */
/*-no-  */
/*-no- ................................. */
/*-no-  */
/*-no- ................................................. */
/*-no-  */
/*-no- ... */
/*-no- ................................................................... */
/*-no- .. */
/*-no- ...................................................................... */
/*-no- ................................................................... */
/*-no- ..................................................................... */
/*-no- .................................................................... */
/*-no- ........................ */
/*-no- .. */
/*-no- ................................................................... */
/*-no- .................................................. */
/*-no- .. */
/*-no- .................................................... */
/*-no- .................................................................. */
/*-no- .................................................................. */
/*-no- ................................................. */
/*-no- .. */
/*-no- .............................................................. */
/*-no- ..................................................................... */
/*-no- ............................................................ */
/*-no- ..................................................................... */
/*-no- ............... */
/*-no- .... */
/*-no- ......................... */
/*-no- ............................. */
/*-no- ............................. */
/*-no- .. */
/*-no-  */
/*-no- .................................................. */
/*-no- ................................................... */
/*-no- .......................................................................... */
/*-no-  */
/*-no- ................................ */
/*-no- ................................... */
/*-no-  */
/*-no- ............................................... */
/*-no- ............................................... */
/*-no-  */
/*-no- ...................................................... */
/*-no- ................................................................. */
/*-no-  */
/*-no- ......................................... */
/*-no- ............................................... */
/*-no- .......................................... */
/*-no- ................................................... */
/*-no-  */
/*-no- ... */
/*-no- ........................................................................ */
/*-no- .............................. */
/*-no- .... */
/*-no- ....................................................... */
/*-no- ..................................................... */
/*-no- .................................................... */
/*-no- .................................................... */
/*-no- ................................................ */
/*-no-  */
/*-no- ... */
/*-no- ............................................................. */
/*-no- .... */
/*-no- ........................................................... */
/*-no- ...................................................... */
/*-no- .............................. */
/*-no-  */
/*-no- ............................................................... */
/*-no-  */
/*-no-  */
/*-no- ............................................... */
/*-no- ................................................ */
/*-no-  */
/*-no- ... */
/*-no- ........................................................................ */
/*-no- .... */
/*-no- ............................................... */
/*-no- ....................................................... */
/*-no- ............................................... */
/*-no- ..................................................... */
/*-no-  */
/*-no- ... */
/*-no- ............................. */
/*-no- .... */
/*-no- ........................................... */
/*-no- ........................................ */
/*-no- ................................................. */
/*-no-  */
/*-no- ... */
/*-no- ................................................................. */
/*-no- ................................................................. */
/*-no- ................................................................ */
/*-no- ................................................................ */
/*-no- ......... */
/*-no- .... */
/*-no- ................................................. */
/*-no- ................................................. */
/*-no- ................................................... */
/*-no- ............................................................ */
/*-no-  */
/*-no- ...................................................... */
/*-no-  */
/*-no- ................................................................................ */
/*-no-  */
/*-no- ... */
/*-no- ........................ */
/*-no- .... */
/*-no- ....................................................... */
/*-no- ........................................................... */
/*-no- ............................................................ */
/*-no- .......................................................... */
/*-no- ..................................................................... */
/*-no-  */
/*-no- ............................................. */
/*-no-  */
/*-no- ............................................... */
/*-no-  */
/*-no- ........................................................ */
/*-no-  */
/*-no- #ifndef NACL_NO_INLINE */
/*-no- #include "native_client/src/trusted/service_runtime/sel_ldr-inl.h" */
/*-no- #endif */
/*-no-  */
/*-no- ... */
/*-no- ............................................................... */
/*-no- .................................................................... */
/*-no- .................................................................... */
/*-no- ........ */
/*-no- .... */
/*-no- ................................................. */
/*-no- ............................................... */
/*-no-  */
/*-no- ... */
/*-no- .......................... */
/*-no- .... */
/*-no- ....................................... */
/*-no- .................................... */
/*-no- ........................................ */
/*-no-  */
/*-no-  */
/*-no- ........................................... */
/*-no- ............................................ */
/*-no-  */
/*-no- ... */
/*-no- .................................................................. */
/*-no- .... */
/*-no- ................................................... */
/*-no- ................................................. */
/*-no-  */
/*-no- ......................................... */
/*-no- ...................................... */
/*-no- .......................................... */
/*-no-  */
/*-no- ............................................. */
/*-no- .............................................. */
/*-no-  */
/*-no-  */
/*-no- ............................................. */
/*-no- ............................................... */
/*-no-  */
/*-no- ............................................... */
/*-no- ................................................. */
/*-no-  */
/*-no- ........................................... */
/*-no- .................................................. */
/*-no-  */
/*-no- ............................................. */
/*-no- .................................................... */
/*-no-  */
/*-no- ........................................................... */
/*-no- .................................................................. */
/*-no-  */
/*-no- ............................................... */
/*-no- ................................................... */
/*-no- ................................................. */
/*-no- ............................................... */
/*-no- ................................................ */
/*-no- .................................................. */
/*-no-  */
/*-no- ..................................................... */
/*-no- ........................................................... */
/*-no-  */
/*-no- ....................................................... */
/*-no- ............................................................. */
/*-no-  */
/*-no- .................................................. */
/*-no-  */
/*-no- ................................................... */
/*-no- .................................................... */
/*-no-  */
/*-no- .................................................... */
/*-no-  */
/*-no- ........................................................................ */
/*-no-  */
/*-no- .......................................................................... */
/*-no-  */
/*-no- ............................................................ */
/*-no-  */
/*-no- ................................................... */
/*-no-  */
/*-no- .................................................. */
/*-no-  */
/*-no- #if NACL_ARCH(NACL_BUILD_ARCH) == NACL_x86 && NACL_BUILD_SUBARCH == 32 && __PIC__ */
/*-no-  */
/*-no- ............................................ */
/*-no-  */
/*-no- #endif */
/*-no-  */
/*-no- ................................................ */
/*-no- ................................................... */
/*-no- ... */
/*-no- ...................................................................... */
/*-no- ...................................................................... */
/*-no- ..................................................................... */
/*-no- ..................................................................... */
/*-no- ............ */
/*-no- .... */
/*-no- .................. */
/*-no- ............................. */
/*-no- ............................ */
/*-no- .. */
/*-no-  */
/*-no- ...................... */
/*-no- .......................... */
/*-no- .......................... */
/*-no- ............................. */
/*-no-  */
/*-no- ............................. */
/*-no- ................................ */
/*-no-  */
/*-no- ............................. */
/*-no- ................................ */
/*-no-  */
/*-no- ............................. */
/*-no- ................................ */
/*-no-  */
/*-no- #if NACL_TARGET_SUBARCH == 32 */
/*-no- ............................. */
/*-no- ................................ */
/*-no- #endif */
/*-no-  */
/*-no- ............................. */
/*-no- ................................ */
/*-no- .. */
/*-no-  */
/*-no- .................................................................... */
/*-no-  */
/*-no- ......................................................... */
/*-no-  */
/*-no- .......................................................... */
/*-no- ......................................................... */
/*-no- ............................................................. */
/*-no- .............................................................. */
/*-no- .............................................................. */
/*-no-  */
/*-no- ........................................................... */
/*-no-  */
/*-no-  */
/*-no- ... */
/*-no- .............................................................. */
/*-no- ...................................................................... */
/*-no- .................................................................... */
/*-no- .................................................................. */
/*-no- ................................................. */
/*-no- ......................................................... */
/*-no- ..................................................................... */
/*-no- .................................................................... */
/*-no- ...................................................................... */
/*-no- ....... */
/*-no- .... */
/*-no- ..................................................................... */
/*-no-  */
/*-no- ................................................................... */
/*-no-  */
/*-no-  */
/*-no- ............................................ */
/*-no-  */
/*-no- ............ */
/*-no-  */
/*-no- #endif  /-* NATIVE_CLIENT_SRC_TRUSTED_SERVICE_RUNTIME_SEL_LDR_H_ *-/ */

#endif
/* CONSBENCH file end */
