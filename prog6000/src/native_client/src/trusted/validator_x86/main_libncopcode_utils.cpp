/* CONSBENCH file begin */
#ifndef MAIN_LIBNCOPCODE_UTILS_CPP
#define MAIN_LIBNCOPCODE_UTILS_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_validator_x86_main_libncopcode_utils_cpp, "native_client/src/trusted/validator_x86/main_libncopcode_utils.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_validator_x86_main_libncopcode_utils_cpp, "native_client/src/trusted/validator_x86/main_libncopcode_utils.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_trusted_validator_x86_ncopcode_desc_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_trusted_validator_x86_main_libncopcode_utils_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_trusted_validator_x86_ncopcode_desc_cpp(it);
}

#endif

