/* CONSBENCH file begin */
#ifndef MAIN_LIBNCVALIDATE_CPP
#define MAIN_LIBNCVALIDATE_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_validator_x86_main_libncvalidate_cpp, "native_client/src/trusted/validator_x86/main_libncvalidate.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_validator_x86_main_libncvalidate_cpp, "native_client/src/trusted/validator_x86/main_libncvalidate.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_trusted_validator_x86_nacl_cpuid_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_ncdecode_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_ncinstbuffer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_nc_segment_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_nc_inst_iter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_nc_inst_state_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_nc_inst_trans_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_ncop_exps_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_ncvalidate_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_nccopycode_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_trusted_validator_x86_main_libncvalidate_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_trusted_validator_x86_nacl_cpuid_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_ncdecode_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_ncinstbuffer_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_nc_segment_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_nc_inst_iter_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_nc_inst_state_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_nc_inst_trans_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_ncop_exps_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_ncvalidate_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_nccopycode_cpp(it);
}

#endif

