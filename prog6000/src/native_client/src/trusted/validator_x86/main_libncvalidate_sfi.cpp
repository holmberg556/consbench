/* CONSBENCH file begin */
#ifndef MAIN_LIBNCVALIDATE_SFI_CPP
#define MAIN_LIBNCVALIDATE_SFI_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, native_client_src_trusted_validator_x86_main_libncvalidate_sfi_cpp, "native_client/src/trusted/validator_x86/main_libncvalidate_sfi.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(native_client_src_trusted_validator_x86_main_libncvalidate_sfi_cpp, "native_client/src/trusted/validator_x86/main_libncvalidate_sfi.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_native_client_src_trusted_validator_x86_ncvalidate_iter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_ncvalidator_registry_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_nc_opcode_histogram_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_nc_cpu_checks_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_nc_illegal_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_nc_protect_base_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_nc_memory_protect_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_ncvalidate_utils_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_native_client_src_trusted_validator_x86_nc_jumps_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_native_client_src_trusted_validator_x86_main_libncvalidate_sfi_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_native_client_src_trusted_validator_x86_ncvalidate_iter_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_ncvalidator_registry_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_nc_opcode_histogram_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_nc_cpu_checks_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_nc_illegal_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_nc_protect_base_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_nc_memory_protect_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_ncvalidate_utils_cpp(it);
  PUBLIC_native_client_src_trusted_validator_x86_nc_jumps_cpp(it);
}

#endif

