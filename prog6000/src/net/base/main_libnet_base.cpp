/* CONSBENCH file begin */
#ifndef MAIN_LIBNET_BASE_CPP
#define MAIN_LIBNET_BASE_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, net_base_main_libnet_base_cpp, "net/base/main_libnet_base.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(net_base_main_libnet_base_cpp, "net/base/main_libnet_base.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_net_base_address_list_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_address_list_net_log_param_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_auth_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_bandwidth_metrics_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_capturing_net_log_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_cert_database_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_cert_database_nss_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_cert_status_flags_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_cert_verifier_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_connection_type_histograms_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_cookie_monster_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_cookie_store_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_crypto_module_nss_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_data_url_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_directory_lister_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_dns_reload_timer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_dnssec_chain_verifier_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_dnssec_keyset_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_dns_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_dnsrr_resolver_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_escape_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_ev_root_ca_metadata_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_file_stream_posix_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_filter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_gzip_filter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_gzip_header_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_host_cache_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_host_mapping_rules_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_host_port_pair_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_host_resolver_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_host_resolver_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_host_resolver_proc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_io_buffer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_ip_endpoint_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_keygen_handler_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_keygen_handler_nss_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_listen_socket_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_mapped_host_resolver_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_mime_sniffer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_mime_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_mock_host_resolver_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_net_errors_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_net_log_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_net_module_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_net_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_net_util_posix_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_network_change_notifier_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_network_change_notifier_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_network_change_notifier_netlink_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_network_delegate_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_nss_memio_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_pem_tokenizer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_platform_mime_util_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_registry_controlled_domain_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_sdch_filter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_sdch_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_ssl_cert_request_info_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_ssl_cipher_suite_names_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_ssl_client_auth_cache_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_ssl_config_service_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_ssl_config_service_defaults_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_ssl_false_start_blacklist_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_ssl_info_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_static_cookie_policy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_test_root_certs_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_test_root_certs_nss_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_transport_security_state_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_upload_data_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_upload_data_stream_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_x509_certificate_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_x509_certificate_nss_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_base_x509_cert_types_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_third_party_mozilla_security_manager_nsKeygenHandler_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_third_party_mozilla_security_manager_nsNSSCertificateDB_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_third_party_mozilla_security_manager_nsNSSCertTrust_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_third_party_mozilla_security_manager_nsPKCS12Blob_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_net_base_ssl_false_start_blacklist_data_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_net_base_main_libnet_base_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_net_base_address_list_cpp(it);
  PUBLIC_net_base_address_list_net_log_param_cpp(it);
  PUBLIC_net_base_auth_cpp(it);
  PUBLIC_net_base_bandwidth_metrics_cpp(it);
  PUBLIC_net_base_capturing_net_log_cpp(it);
  PUBLIC_net_base_cert_database_cpp(it);
  PUBLIC_net_base_cert_database_nss_cpp(it);
  PUBLIC_net_base_cert_status_flags_cpp(it);
  PUBLIC_net_base_cert_verifier_cpp(it);
  PUBLIC_net_base_connection_type_histograms_cpp(it);
  PUBLIC_net_base_cookie_monster_cpp(it);
  PUBLIC_net_base_cookie_store_cpp(it);
  PUBLIC_net_base_crypto_module_nss_cpp(it);
  PUBLIC_net_base_data_url_cpp(it);
  PUBLIC_net_base_directory_lister_cpp(it);
  PUBLIC_net_base_dns_reload_timer_cpp(it);
  PUBLIC_net_base_dnssec_chain_verifier_cpp(it);
  PUBLIC_net_base_dnssec_keyset_cpp(it);
  PUBLIC_net_base_dns_util_cpp(it);
  PUBLIC_net_base_dnsrr_resolver_cpp(it);
  PUBLIC_net_base_escape_cpp(it);
  PUBLIC_net_base_ev_root_ca_metadata_cpp(it);
  PUBLIC_net_base_file_stream_posix_cpp(it);
  PUBLIC_net_base_filter_cpp(it);
  PUBLIC_net_base_gzip_filter_cpp(it);
  PUBLIC_net_base_gzip_header_cpp(it);
  PUBLIC_net_base_host_cache_cpp(it);
  PUBLIC_net_base_host_mapping_rules_cpp(it);
  PUBLIC_net_base_host_port_pair_cpp(it);
  PUBLIC_net_base_host_resolver_cpp(it);
  PUBLIC_net_base_host_resolver_impl_cpp(it);
  PUBLIC_net_base_host_resolver_proc_cpp(it);
  PUBLIC_net_base_io_buffer_cpp(it);
  PUBLIC_net_base_ip_endpoint_cpp(it);
  PUBLIC_net_base_keygen_handler_cpp(it);
  PUBLIC_net_base_keygen_handler_nss_cpp(it);
  PUBLIC_net_base_listen_socket_cpp(it);
  PUBLIC_net_base_mapped_host_resolver_cpp(it);
  PUBLIC_net_base_mime_sniffer_cpp(it);
  PUBLIC_net_base_mime_util_cpp(it);
  PUBLIC_net_base_mock_host_resolver_cpp(it);
  PUBLIC_net_base_net_errors_cpp(it);
  PUBLIC_net_base_net_log_cpp(it);
  PUBLIC_net_base_net_module_cpp(it);
  PUBLIC_net_base_net_util_cpp(it);
  PUBLIC_net_base_net_util_posix_cpp(it);
  PUBLIC_net_base_network_change_notifier_cpp(it);
  PUBLIC_net_base_network_change_notifier_linux_cpp(it);
  PUBLIC_net_base_network_change_notifier_netlink_linux_cpp(it);
  PUBLIC_net_base_network_delegate_cpp(it);
  PUBLIC_net_base_nss_memio_cpp(it);
  PUBLIC_net_base_pem_tokenizer_cpp(it);
  PUBLIC_net_base_platform_mime_util_linux_cpp(it);
  PUBLIC_net_base_registry_controlled_domain_cpp(it);
  PUBLIC_net_base_sdch_filter_cpp(it);
  PUBLIC_net_base_sdch_manager_cpp(it);
  PUBLIC_net_base_ssl_cert_request_info_cpp(it);
  PUBLIC_net_base_ssl_cipher_suite_names_cpp(it);
  PUBLIC_net_base_ssl_client_auth_cache_cpp(it);
  PUBLIC_net_base_ssl_config_service_cpp(it);
  PUBLIC_net_base_ssl_config_service_defaults_cpp(it);
  PUBLIC_net_base_ssl_false_start_blacklist_cpp(it);
  PUBLIC_net_base_ssl_info_cpp(it);
  PUBLIC_net_base_static_cookie_policy_cpp(it);
  PUBLIC_net_base_test_root_certs_cpp(it);
  PUBLIC_net_base_test_root_certs_nss_cpp(it);
  PUBLIC_net_base_transport_security_state_cpp(it);
  PUBLIC_net_base_upload_data_cpp(it);
  PUBLIC_net_base_upload_data_stream_cpp(it);
  PUBLIC_net_base_x509_certificate_cpp(it);
  PUBLIC_net_base_x509_certificate_nss_cpp(it);
  PUBLIC_net_base_x509_cert_types_cpp(it);
  PUBLIC_net_third_party_mozilla_security_manager_nsKeygenHandler_cpp(it);
  PUBLIC_net_third_party_mozilla_security_manager_nsNSSCertificateDB_cpp(it);
  PUBLIC_net_third_party_mozilla_security_manager_nsNSSCertTrust_cpp(it);
  PUBLIC_net_third_party_mozilla_security_manager_nsPKCS12Blob_cpp(it);
  PUBLIC_out_Debug_obj_gen_net_base_ssl_false_start_blacklist_data_cpp(it);
}

#endif

