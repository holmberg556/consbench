/* CONSBENCH file begin */
#ifndef MAIN_LIBHTTP_SERVER_CPP
#define MAIN_LIBHTTP_SERVER_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, net_server_main_libhttp_server_cpp, "net/server/main_libhttp_server.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(net_server_main_libhttp_server_cpp, "net/server/main_libhttp_server.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_net_server_http_server_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_net_server_http_server_request_info_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_net_server_main_libhttp_server_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_net_server_http_server_cpp(it);
  PUBLIC_net_server_http_server_request_info_cpp(it);
}

#endif

