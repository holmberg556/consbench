/* CONSBENCH file begin */
#ifndef MAIN_LIBV8_SNAPSHOT_CPP
#define MAIN_LIBV8_SNAPSHOT_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, out_Debug_obj_target_geni_main_libv8_snapshot_cpp, "out/Debug/obj.target/geni/main_libv8_snapshot.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(out_Debug_obj_target_geni_main_libv8_snapshot_cpp, "out/Debug/obj.target/geni/main_libv8_snapshot.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_out_Debug_obj_gen_libraries_empty_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_target_geni_snapshot_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_out_Debug_obj_target_geni_main_libv8_snapshot_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_out_Debug_obj_gen_libraries_empty_cpp(it);
  PUBLIC_out_Debug_obj_target_geni_snapshot_cpp(it);
}

#endif

