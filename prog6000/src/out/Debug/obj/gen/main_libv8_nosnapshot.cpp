/* CONSBENCH file begin */
#ifndef MAIN_LIBV8_NOSNAPSHOT_CPP
#define MAIN_LIBV8_NOSNAPSHOT_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, out_Debug_obj_gen_main_libv8_nosnapshot_cpp, "out/Debug/obj/gen/main_libv8_nosnapshot.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(out_Debug_obj_gen_main_libv8_nosnapshot_cpp, "out/Debug/obj/gen/main_libv8_nosnapshot.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_out_Debug_obj_gen_libraries_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_v8_src_snapshot_empty_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_out_Debug_obj_gen_main_libv8_nosnapshot_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_out_Debug_obj_gen_libraries_cpp(it);
  PUBLIC_v8_src_snapshot_empty_cpp(it);
}

#endif

