/* CONSBENCH file begin */
#ifndef MAIN_LIBPOLICY_CPP
#define MAIN_LIBPOLICY_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, out_Debug_obj_gen_policy_policy_main_libpolicy_cpp, "out/Debug/obj/gen/policy/policy/main_libpolicy.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(out_Debug_obj_gen_policy_policy_main_libpolicy_cpp, "out/Debug/obj/gen/policy/policy/main_libpolicy.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_out_Debug_obj_gen_policy_policy_policy_constants_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_policy_policy_cloud_policy_generated_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_policy_proto_cloud_policy_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_chrome_browser_policy_policy_map_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_out_Debug_obj_gen_policy_policy_main_libpolicy_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_out_Debug_obj_gen_policy_policy_policy_constants_cpp(it);
  PUBLIC_out_Debug_obj_gen_policy_policy_cloud_policy_generated_cpp(it);
  PUBLIC_out_Debug_obj_gen_protoc_out_chrome_browser_policy_proto_cloud_policy_pb_cpp(it);
  PUBLIC_chrome_browser_policy_policy_map_cpp(it);
}

#endif

