/* CONSBENCH file begin */
#ifndef MAIN_LIBTRACE_PROTO_LIB_CPP
#define MAIN_LIBTRACE_PROTO_LIB_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, out_Debug_obj_gen_protoc_out_remoting_proto_main_libtrace_proto_lib_cpp, "out/Debug/obj/gen/protoc_out/remoting/proto/main_libtrace_proto_lib.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(out_Debug_obj_gen_protoc_out_remoting_proto_main_libtrace_proto_lib_cpp, "out/Debug/obj/gen/protoc_out/remoting/proto/main_libtrace_proto_lib.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_out_Debug_obj_gen_protoc_out_remoting_proto_trace_pb_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_out_Debug_obj_gen_protoc_out_remoting_proto_main_libtrace_proto_lib_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_out_Debug_obj_gen_protoc_out_remoting_proto_trace_pb_cpp(it);
}

#endif

