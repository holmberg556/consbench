/* CONSBENCH file begin */
#ifndef V8DERIVEDSOURCES4_CPP_2120
#define V8DERIVEDSOURCES4_CPP_2120

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, out_Debug_obj_gen_webkit_bindings_V8DerivedSources4_cpp, "out/Debug/obj/gen/webkit/bindings/V8DerivedSources4.cpp");

/* CONSBENCH includes begin */
#include "bindings/V8CanvasPixelArray.cpp"
#include "bindings/V8CanvasRenderingContext.cpp"
#include "bindings/V8CanvasRenderingContext2D.cpp"
#include "bindings/V8Clipboard.cpp"
#include "bindings/V8Counter.cpp"
#include "bindings/V8DOMImplementation.cpp"
#include "bindings/V8DOMMimeTypeArray.cpp"
#include "bindings/V8DOMTokenList.cpp"
#include "bindings/V8DataView.cpp"
#include "bindings/V8Document.cpp"
#include "bindings/V8DocumentFragment.cpp"
#include "bindings/V8HTMLAreaElement.cpp"
#include "bindings/V8HTMLKeygenElement.cpp"
#include "bindings/V8HTMLQuoteElement.cpp"
#include "bindings/V8HTMLTableCellElement.cpp"
#include "bindings/V8HTMLTableElement.cpp"
#include "bindings/V8Int16Array.cpp"
#include "bindings/V8MessageEvent.cpp"
#include "bindings/V8MessagePort.cpp"
#include "bindings/V8OverflowEvent.cpp"
#include "bindings/V8StyleMedia.cpp"
#include "bindings/V8TextMetrics.cpp"
#include "bindings/V8WebKitCSSTransformValue.cpp"
#include "bindings/V8SQLException.cpp"
#include "bindings/V8SQLStatementErrorCallback.cpp"
#include "bindings/V8SQLTransactionCallback.cpp"
#include "bindings/V8HTMLDataGridColElement.cpp"
#include "bindings/V8DirectoryReader.cpp"
#include "bindings/V8FileEntry.cpp"
#include "bindings/V8IDBDatabase.cpp"
#include "bindings/V8IDBVersionChangeRequest.cpp"
#include "bindings/V8JavaScriptCallFrame.cpp"
#include "bindings/V8DOMApplicationCache.cpp"
#include "bindings/V8SVGAnimatedInteger.cpp"
#include "bindings/V8SVGAnimatedString.cpp"
#include "bindings/V8SVGAnimatedTransformList.cpp"
#include "bindings/V8SVGColor.cpp"
#include "bindings/V8SVGPathElement.cpp"
#include "bindings/V8SVGPathSegArcRel.cpp"
#include "bindings/V8SVGPathSegCurvetoQuadraticAbs.cpp"
#include "bindings/V8SVGPathSegCurvetoQuadraticSmoothAbs.cpp"
#include "bindings/V8SVGRect.cpp"
#include "bindings/V8SVGTextContentElement.cpp"
#include "bindings/V8SVGTextElement.cpp"
#include "bindings/V8SVGTextPositioningElement.cpp"
#include "bindings/V8SVGFEBlendElement.cpp"
#include "bindings/V8SVGFECompositeElement.cpp"
#include "bindings/V8SVGFEFuncGElement.cpp"
#include "bindings/V8SVGFEMorphologyElement.cpp"
#include "bindings/V8SVGFontFaceNameElement.cpp"
#include "bindings/V8SVGGlyphElement.cpp"
#include "bindings/V8SVGVKernElement.cpp"
#include "bindings/V8WebGLRenderingContext.cpp"
#include "bindings/V8WebGLShader.cpp"
#include "bindings/V8AudioContext.cpp"
#include "bindings/V8AudioNode.cpp"
#include "bindings/V8AudioParam.cpp"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(out_Debug_obj_gen_webkit_bindings_V8DerivedSources4_cpp, "out/Debug/obj/gen/webkit/bindings/V8DerivedSources4.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(out_Debug_obj_gen_webkit_bindings_V8DerivedSources4_cpp);

/*-no- ... */
/*-no- ...................................................... */
/*-no- .. */
/*-no- ........................................................... */
/*-no- .. */
/*-no- ....................................................... */
/*-no- .. */
/*-no- ..................................................................... */
/*-no- ..................................................................... */
/*-no- ........... */
/*-no- .................................................................... */
/*-no- ................................................................... */
/*-no- ....................................................................... */
/*-no- ......................................................................... */
/*-no- .......................................................................... */
/*-no- .. */
/*-no- ...................................................................... */
/*-no- .................................................................... */
/*-no- ..................................................................... */
/*-no- ..................................................................... */
/*-no- ........................................................................ */
/*-no- ...................................................................... */
/*-no- ..................................................................... */
/*-no- ...................................................................... */
/*-no- ............................................................... */
/*-no- ........................................................................ */
/*-no- ....................................................................... */
/*-no- .... */
/*-no- #define NO_IMPLICIT_ATOMICSTRING */
/*-no-  */
/*-no- #include "bindings/V8CanvasPixelArray.cpp" */
/*-no- #include "bindings/V8CanvasRenderingContext.cpp" */
/*-no- #include "bindings/V8CanvasRenderingContext2D.cpp" */
/*-no- #include "bindings/V8Clipboard.cpp" */
/*-no- #include "bindings/V8Counter.cpp" */
/*-no- #include "bindings/V8DOMImplementation.cpp" */
/*-no- #include "bindings/V8DOMMimeTypeArray.cpp" */
/*-no- #include "bindings/V8DOMTokenList.cpp" */
/*-no- #include "bindings/V8DataView.cpp" */
/*-no- #include "bindings/V8Document.cpp" */
/*-no- #include "bindings/V8DocumentFragment.cpp" */
/*-no- #include "bindings/V8HTMLAreaElement.cpp" */
/*-no- #include "bindings/V8HTMLKeygenElement.cpp" */
/*-no- #include "bindings/V8HTMLQuoteElement.cpp" */
/*-no- #include "bindings/V8HTMLTableCellElement.cpp" */
/*-no- #include "bindings/V8HTMLTableElement.cpp" */
/*-no- #include "bindings/V8Int16Array.cpp" */
/*-no- #include "bindings/V8MessageEvent.cpp" */
/*-no- #include "bindings/V8MessagePort.cpp" */
/*-no- #include "bindings/V8OverflowEvent.cpp" */
/*-no- #include "bindings/V8StyleMedia.cpp" */
/*-no- #include "bindings/V8TextMetrics.cpp" */
/*-no- #include "bindings/V8WebKitCSSTransformValue.cpp" */
/*-no-  */
/*-no- #if ENABLE(DATABASE) */
/*-no- #include "bindings/V8SQLException.cpp" */
/*-no- #include "bindings/V8SQLStatementErrorCallback.cpp" */
/*-no- #include "bindings/V8SQLTransactionCallback.cpp" */
/*-no- #endif */
/*-no-  */
/*-no- #if ENABLE(DATAGRID) */
/*-no- #include "bindings/V8HTMLDataGridColElement.cpp" */
/*-no- #endif */
/*-no-  */
/*-no- #if ENABLE(FILE_SYSTEM) */
/*-no- #include "bindings/V8DirectoryReader.cpp" */
/*-no- #include "bindings/V8FileEntry.cpp" */
/*-no- #endif */
/*-no-  */
/*-no- #if ENABLE(INDEXED_DATABASE) */
/*-no- #include "bindings/V8IDBDatabase.cpp" */
/*-no- #include "bindings/V8IDBVersionChangeRequest.cpp" */
/*-no- #endif */
/*-no-  */
/*-no- #if ENABLE(JAVASCRIPT_DEBUGGER) */
/*-no- #include "bindings/V8JavaScriptCallFrame.cpp" */
/*-no- #endif */
/*-no-  */
/*-no- #if ENABLE(OFFLINE_WEB_APPLICATIONS) */
/*-no- #include "bindings/V8DOMApplicationCache.cpp" */
/*-no- #endif */
/*-no-  */
/*-no- #if ENABLE(SVG) */
/*-no- #include "bindings/V8SVGAnimatedInteger.cpp" */
/*-no- #include "bindings/V8SVGAnimatedString.cpp" */
/*-no- #include "bindings/V8SVGAnimatedTransformList.cpp" */
/*-no- #include "bindings/V8SVGColor.cpp" */
/*-no- #include "bindings/V8SVGPathElement.cpp" */
/*-no- #include "bindings/V8SVGPathSegArcRel.cpp" */
/*-no- #include "bindings/V8SVGPathSegCurvetoQuadraticAbs.cpp" */
/*-no- #include "bindings/V8SVGPathSegCurvetoQuadraticSmoothAbs.cpp" */
/*-no- #include "bindings/V8SVGRect.cpp" */
/*-no- #include "bindings/V8SVGTextContentElement.cpp" */
/*-no- #include "bindings/V8SVGTextElement.cpp" */
/*-no- #include "bindings/V8SVGTextPositioningElement.cpp" */
/*-no- #endif */
/*-no-  */
/*-no- #if ENABLE(SVG) && ENABLE(FILTERS) */
/*-no- #include "bindings/V8SVGFEBlendElement.cpp" */
/*-no- #include "bindings/V8SVGFECompositeElement.cpp" */
/*-no- #include "bindings/V8SVGFEFuncGElement.cpp" */
/*-no- #include "bindings/V8SVGFEMorphologyElement.cpp" */
/*-no- #endif */
/*-no-  */
/*-no- #if ENABLE(SVG) && ENABLE(SVG_FONTS) */
/*-no- #include "bindings/V8SVGFontFaceNameElement.cpp" */
/*-no- #include "bindings/V8SVGGlyphElement.cpp" */
/*-no- #include "bindings/V8SVGVKernElement.cpp" */
/*-no- #endif */
/*-no-  */
/*-no- #if ENABLE(WEBGL) */
/*-no- #include "bindings/V8WebGLRenderingContext.cpp" */
/*-no- #include "bindings/V8WebGLShader.cpp" */
/*-no- #endif */
/*-no-  */
/*-no- #if ENABLE(WEB_AUDIO) */
/*-no- #include "bindings/V8AudioContext.cpp" */
/*-no- #include "bindings/V8AudioNode.cpp" */
/*-no- #include "bindings/V8AudioParam.cpp" */
/*-no- #endif */

#endif
/* CONSBENCH file end */
