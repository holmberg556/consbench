/* CONSBENCH file begin */
#ifndef MAIN_LIBPPAPI_CPP_OBJECTS_CPP
#define MAIN_LIBPPAPI_CPP_OBJECTS_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, ppapi_cpp_dev_main_libppapi_cpp_objects_cpp, "ppapi/cpp/dev/main_libppapi_cpp_objects.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(ppapi_cpp_dev_main_libppapi_cpp_objects_cpp, "ppapi/cpp/dev/main_libppapi_cpp_objects.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_ppapi_cpp_audio_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_audio_config_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_core_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_graphics_2d_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_image_data_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_instance_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_module_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_paint_aggregator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_paint_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_rect_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_resource_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_url_loader_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_url_request_info_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_url_response_info_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_var_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_buffer_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_context_3d_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_directory_entry_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_directory_reader_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_file_chooser_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_file_io_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_file_ref_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_file_system_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_find_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_font_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_fullscreen_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_graphics_3d_client_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_graphics_3d_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_printing_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_scrollbar_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_selection_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_surface_3d_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_transport_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_url_util_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_video_decoder_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_widget_client_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_widget_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_zoom_dev_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_cpp_dev_scriptable_object_deprecated_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_ppapi_cpp_dev_main_libppapi_cpp_objects_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_ppapi_cpp_audio_cpp(it);
  PUBLIC_ppapi_cpp_audio_config_cpp(it);
  PUBLIC_ppapi_cpp_core_cpp(it);
  PUBLIC_ppapi_cpp_graphics_2d_cpp(it);
  PUBLIC_ppapi_cpp_image_data_cpp(it);
  PUBLIC_ppapi_cpp_instance_cpp(it);
  PUBLIC_ppapi_cpp_module_cpp(it);
  PUBLIC_ppapi_cpp_paint_aggregator_cpp(it);
  PUBLIC_ppapi_cpp_paint_manager_cpp(it);
  PUBLIC_ppapi_cpp_rect_cpp(it);
  PUBLIC_ppapi_cpp_resource_cpp(it);
  PUBLIC_ppapi_cpp_url_loader_cpp(it);
  PUBLIC_ppapi_cpp_url_request_info_cpp(it);
  PUBLIC_ppapi_cpp_url_response_info_cpp(it);
  PUBLIC_ppapi_cpp_var_cpp(it);
  PUBLIC_ppapi_cpp_dev_buffer_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_context_3d_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_directory_entry_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_directory_reader_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_file_chooser_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_file_io_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_file_ref_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_file_system_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_find_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_font_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_fullscreen_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_graphics_3d_client_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_graphics_3d_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_printing_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_scrollbar_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_selection_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_surface_3d_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_transport_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_url_util_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_video_decoder_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_widget_client_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_widget_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_zoom_dev_cpp(it);
  PUBLIC_ppapi_cpp_dev_scriptable_object_deprecated_cpp(it);
}

#endif

