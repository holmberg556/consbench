/* CONSBENCH file begin */
#ifndef MAIN_LIBPPAPI_CPP_CPP
#define MAIN_LIBPPAPI_CPP_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, ppapi_cpp_main_libppapi_cpp_cpp, "ppapi/cpp/main_libppapi_cpp.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(ppapi_cpp_main_libppapi_cpp_cpp, "ppapi/cpp/main_libppapi_cpp.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_ppapi_cpp_ppp_entrypoints_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_ppapi_cpp_main_libppapi_cpp_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_ppapi_cpp_ppp_entrypoints_cpp(it);
}

#endif

