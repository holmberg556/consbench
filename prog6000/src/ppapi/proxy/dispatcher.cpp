/* CONSBENCH file begin */
#ifndef DISPATCHER_CC_2727
#define DISPATCHER_CC_2727

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, ppapi_proxy_dispatcher_cpp, "ppapi/proxy/dispatcher.cpp");

/* CONSBENCH includes begin */
#include "ppapi/proxy/dispatcher.h"
#include <string.h>  // For memset.
#include <map>
#include "base/compiler_specific.h"
#include "base/logging.h"
#include "base/singleton.h"
#include "ipc/ipc_message.h"
#include "ipc/ipc_sync_channel.h"
#include "ipc/ipc_test_sink.h"
#include "ppapi/c/dev/ppb_buffer_dev.h"
#include "ppapi/c/dev/ppb_char_set_dev.h"
#include "ppapi/c/dev/ppb_context_3d_dev.h"
#include "ppapi/c/dev/ppb_cursor_control_dev.h"
#include "ppapi/c/dev/ppb_gles_chromium_texture_mapping_dev.h"
#include "ppapi/c/dev/ppb_font_dev.h"
#include "ppapi/c/dev/ppb_fullscreen_dev.h"
#include "ppapi/c/dev/ppb_opengles_dev.h"
#include "ppapi/c/dev/ppb_surface_3d_dev.h"
#include "ppapi/c/dev/ppb_testing_dev.h"
#include "ppapi/c/dev/ppb_var_deprecated.h"
#include "ppapi/c/pp_errors.h"
#include "ppapi/c/ppb_audio.h"
#include "ppapi/c/ppb_audio_config.h"
#include "ppapi/c/ppb_core.h"
#include "ppapi/c/ppb_graphics_2d.h"
#include "ppapi/c/ppb_image_data.h"
#include "ppapi/c/ppb_instance.h"
#include "ppapi/c/ppb_url_loader.h"
#include "ppapi/c/ppb_url_request_info.h"
#include "ppapi/c/ppb_url_response_info.h"
#include "ppapi/c/ppp_instance.h"
#include "ppapi/c/private/ppb_flash.h"
#include "ppapi/c/private/ppb_flash_file.h"
#include "ppapi/c/private/ppb_flash_menu.h"
#include "ppapi/c/private/ppb_pdf.h"
#include "ppapi/c/trusted/ppb_url_loader_trusted.h"
#include "ppapi/proxy/ppapi_messages.h"
#include "ppapi/proxy/ppb_audio_config_proxy.h"
#include "ppapi/proxy/ppb_audio_proxy.h"
#include "ppapi/proxy/ppb_buffer_proxy.h"
#include "ppapi/proxy/ppb_char_set_proxy.h"
#include "ppapi/proxy/ppb_context_3d_proxy.h"
#include "ppapi/proxy/ppb_core_proxy.h"
#include "ppapi/proxy/ppb_cursor_control_proxy.h"
#include "ppapi/proxy/ppb_file_chooser_proxy.h"
#include "ppapi/proxy/ppb_file_ref_proxy.h"
#include "ppapi/proxy/ppb_file_system_proxy.h"
#include "ppapi/proxy/ppb_flash_file_proxy.h"
#include "ppapi/proxy/ppb_flash_proxy.h"
#include "ppapi/proxy/ppb_flash_menu_proxy.h"
#include "ppapi/proxy/ppb_font_proxy.h"
#include "ppapi/proxy/ppb_fullscreen_proxy.h"
#include "ppapi/proxy/ppb_gles_chromium_texture_mapping_proxy.h"
#include "ppapi/proxy/ppb_graphics_2d_proxy.h"
#include "ppapi/proxy/ppb_image_data_proxy.h"
#include "ppapi/proxy/ppb_instance_proxy.h"
#include "ppapi/proxy/ppb_opengles2_proxy.h"
#include "ppapi/proxy/ppb_pdf_proxy.h"
#include "ppapi/proxy/ppb_surface_3d_proxy.h"
#include "ppapi/proxy/ppb_testing_proxy.h"
#include "ppapi/proxy/ppb_url_loader_proxy.h"
#include "ppapi/proxy/ppb_url_request_info_proxy.h"
#include "ppapi/proxy/ppb_url_response_info_proxy.h"
#include "ppapi/proxy/ppb_var_deprecated_proxy.h"
#include "ppapi/proxy/ppp_class_proxy.h"
#include "ppapi/proxy/ppp_instance_proxy.h"
#include "ppapi/proxy/var_serialization_rules.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(ppapi_proxy_dispatcher_cpp, "ppapi/proxy/dispatcher.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(ppapi_proxy_dispatcher_cpp);

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #include "ppapi/proxy/dispatcher.h" */
/*-no-  */
/*-no- #include <string.h>  // For memset. */
/*-no-  */
/*-no- #include <map> */
/*-no-  */
/*-no- #include "base/compiler_specific.h" */
/*-no- #include "base/logging.h" */
/*-no- #include "base/singleton.h" */
/*-no- #include "ipc/ipc_message.h" */
/*-no- #include "ipc/ipc_sync_channel.h" */
/*-no- #include "ipc/ipc_test_sink.h" */
/*-no- #include "ppapi/c/dev/ppb_buffer_dev.h" */
/*-no- #include "ppapi/c/dev/ppb_char_set_dev.h" */
/*-no- #include "ppapi/c/dev/ppb_context_3d_dev.h" */
/*-no- #include "ppapi/c/dev/ppb_cursor_control_dev.h" */
/*-no- #include "ppapi/c/dev/ppb_gles_chromium_texture_mapping_dev.h" */
/*-no- #include "ppapi/c/dev/ppb_font_dev.h" */
/*-no- #include "ppapi/c/dev/ppb_fullscreen_dev.h" */
/*-no- #include "ppapi/c/dev/ppb_opengles_dev.h" */
/*-no- #include "ppapi/c/dev/ppb_surface_3d_dev.h" */
/*-no- #include "ppapi/c/dev/ppb_testing_dev.h" */
/*-no- #include "ppapi/c/dev/ppb_var_deprecated.h" */
/*-no- #include "ppapi/c/pp_errors.h" */
/*-no- #include "ppapi/c/ppb_audio.h" */
/*-no- #include "ppapi/c/ppb_audio_config.h" */
/*-no- #include "ppapi/c/ppb_core.h" */
/*-no- #include "ppapi/c/ppb_graphics_2d.h" */
/*-no- #include "ppapi/c/ppb_image_data.h" */
/*-no- #include "ppapi/c/ppb_instance.h" */
/*-no- #include "ppapi/c/ppb_url_loader.h" */
/*-no- #include "ppapi/c/ppb_url_request_info.h" */
/*-no- #include "ppapi/c/ppb_url_response_info.h" */
/*-no- #include "ppapi/c/ppp_instance.h" */
/*-no- #include "ppapi/c/private/ppb_flash.h" */
/*-no- #include "ppapi/c/private/ppb_flash_file.h" */
/*-no- #include "ppapi/c/private/ppb_flash_menu.h" */
/*-no- #include "ppapi/c/private/ppb_pdf.h" */
/*-no- #include "ppapi/c/trusted/ppb_url_loader_trusted.h" */
/*-no- #include "ppapi/proxy/ppapi_messages.h" */
/*-no- #include "ppapi/proxy/ppb_audio_config_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_audio_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_buffer_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_char_set_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_context_3d_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_core_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_cursor_control_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_file_chooser_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_file_ref_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_file_system_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_flash_file_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_flash_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_flash_menu_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_font_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_fullscreen_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_gles_chromium_texture_mapping_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_graphics_2d_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_image_data_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_instance_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_opengles2_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_pdf_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_surface_3d_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_testing_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_url_loader_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_url_request_info_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_url_response_info_proxy.h" */
/*-no- #include "ppapi/proxy/ppb_var_deprecated_proxy.h" */
/*-no- #include "ppapi/proxy/ppp_class_proxy.h" */
/*-no- #include "ppapi/proxy/ppp_instance_proxy.h" */
/*-no- #include "ppapi/proxy/var_serialization_rules.h" */
/*-no-  */
/*-no- .............. */
/*-no- ................. */
/*-no-  */
/*-no- ........... */
/*-no-  */
/*-no- ...................... */
/*-no- .................. */
/*-no-  */
/*-no- ...................................... */
/*-no-  */
/*-no- ................................................ */
/*-no- ................................................ */
/*-no-  */
/*-no- ........................................................................ */
/*-no- .................................. */
/*-no- ................................... */
/*-no-  */
/*-no- ..................................................................... */
/*-no- ...................................................................... */
/*-no- .. */
/*-no-  */
/*-no- ................................ */
/*-no- ............................................................ */
/*-no- .............................................................. */
/*-no-  */
/*-no- .............................. */
/*-no- ........................................... */
/*-no- ..................................... */
/*-no- ...................................... */
/*-no- ....................................... */
/*-no- ......................................... */
/*-no- .................................... */
/*-no- ............................................. */
/*-no- ........................................... */
/*-no- ....................................... */
/*-no- .......................................... */
/*-no- .................................................. */
/*-no- ...................................................... */
/*-no- ..................................... */
/*-no- .......................................... */
/*-no- .................................... */
/*-no- .......................................... */
/*-no- .......................................................... */
/*-no- .......................................... */
/*-no- ......................................... */
/*-no- ........................................ */
/*-no- ......................................... */
/*-no- ................................... */
/*-no- ......................................... */
/*-no- ....................................... */
/*-no- ......................................... */
/*-no- ................................................ */
/*-no- .............................................. */
/*-no- ............................................... */
/*-no- .............................................. */
/*-no-  */
/*-no- ............................. */
/*-no- ........................................ */
/*-no- . */
/*-no-  */
/*-no- .............................................................. */
/*-no- ................................................. */
/*-no- ..................................... */
/*-no- ........................................................ */
/*-no- ............................................... */
/*-no-  */
/*-no- .......................................... */
/*-no- ...................................... */
/*-no- . */
/*-no-  */
/*-no- .............................................................. */
/*-no- .................................................. */
/*-no- ...................................... */
/*-no- ........................................................ */
/*-no- ................................................ */
/*-no-  */
/*-no- ........................................................ */
/*-no- ....................................... */
/*-no- . */
/*-no-  */
/*-no- ......... */
/*-no- ............................................. */
/*-no- ......................................... */
/*-no- . */
/*-no-  */
/*-no- ............... */
/*-no-  */
/*-no- ................................................................. */
/*-no- ............................................................ */
/*-no- .................................................... */
/*-no- ....................... */
/*-no- ............................................................................... */
/*-no- ................................................ */
/*-no- ............................................................... */
/*-no- . */
/*-no-  */
/*-no- ........................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................... */
/*-no- .......................................................................... */
/*-no- ................................................ */
/*-no- ....................................................................... */
/*-no- ................................................................. */
/*-no- .................................................................. */
/*-no- ................................................................. */
/*-no- ................................................................................ */
/*-no- .............. */
/*-no- . */
/*-no-  */
/*-no- ............................................................. */
/*-no- ...................... */
/*-no- ......................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................. */
/*-no- ...................... */
/*-no- ................................................ */
/*-no- ........................ */
/*-no- .......................................... */
/*-no- ....................................................................... */
/*-no- ............................................................................ */
/*-no- ............................................ */
/*-no- ......................... */
/*-no- ................... */
/*-no- ... */
/*-no- ............... */
/*-no- . */
/*-no-  */
/*-no- ................................... */
/*-no- ................... */
/*-no- . */
/*-no-  */
/*-no- ......... */
/*-no- ............................................................ */
/*-no- .............................. */
/*-no- ........................................................... */
/*-no- ................................................... */
/*-no- ............................................. */
/*-no- ................................................. */
/*-no- ................ */
/*-no- ....................... */
/*-no- . */
/*-no-  */
/*-no- ......... */
/*-no- ............................................................................. */
/*-no- .......................................... */
/*-no- ................ */
/*-no- ........................................................... */
/*-no- ....................................... */
/*-no- . */
/*-no-  */
/*-no- ......... */
/*-no- ............................................................ */
/*-no- .............................. */
/*-no- ........................................................... */
/*-no- ................................................... */
/*-no- ............................................ */
/*-no- ................................................ */
/*-no- ................ */
/*-no- ....................... */
/*-no- . */
/*-no-  */
/*-no- ......... */
/*-no- ............................................................................. */
/*-no- .......................................... */
/*-no- ................ */
/*-no- ........................................................... */
/*-no- ...................................... */
/*-no- . */
/*-no-  */
/*-no- ....................................... */
/*-no- ..................................................... */
/*-no- ...................................................... */
/*-no- . */
/*-no-  */
/*-no- .................................................................. */
/*-no- ......................................... */
/*-no- . */
/*-no-  */
/*-no- .............................................................. */
/*-no- ................................ */
/*-no- ................................. */
/*-no- ......................................... */
/*-no- #if defined(OS_WIN) */
/*-no- ........................................ */
/*-no- .......................... */
/*-no- ...................................... */
/*-no- ............................................... */
/*-no- ................................ */
/*-no- ................................................ */
/*-no- ..................................... */
/*-no- ........................... */
/*-no- ............................... */
/*-no- .................................. */
/*-no- ...................................................... */
/*-no- #elif defined(OS_POSIX) */
/*-no- ................................................................................ */
/*-no- ............................. */
/*-no- ........................................................ */
/*-no- .............................................. */
/*-no- #else */
/*-no-   #error Not implemented. */
/*-no- #endif */
/*-no- .................... */
/*-no- . */
/*-no-  */
/*-no- .......................................... */
/*-no- ................. */
/*-no- ................................. */
/*-no- ..................... */
/*-no- ............................... */
/*-no-  */
/*-no- ............................................ */
/*-no- ............. */
/*-no- ............... */
/*-no- . */
/*-no-  */
/*-no- ..................... */
/*-no- .................. */
/*-no-  */

#endif
/* CONSBENCH file end */
