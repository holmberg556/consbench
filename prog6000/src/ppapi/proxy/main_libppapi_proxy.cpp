/* CONSBENCH file begin */
#ifndef MAIN_LIBPPAPI_PROXY_CPP
#define MAIN_LIBPPAPI_PROXY_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, ppapi_proxy_main_libppapi_proxy_cpp, "ppapi/proxy/main_libppapi_proxy.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(ppapi_proxy_main_libppapi_proxy_cpp, "ppapi/proxy/main_libppapi_proxy.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_ppapi_proxy_callback_tracker_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_dispatcher_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_host_dispatcher_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_host_var_serialization_rules_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_interface_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_plugin_dispatcher_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_plugin_resource_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_plugin_resource_tracker_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_plugin_var_serialization_rules_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_plugin_var_tracker_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppapi_messages_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppapi_param_traits_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_audio_config_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_audio_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_buffer_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_char_set_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_context_3d_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_core_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_cursor_control_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_file_chooser_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_file_ref_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_file_system_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_flash_file_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_flash_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_flash_menu_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_font_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_fullscreen_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_gles_chromium_texture_mapping_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_graphics_2d_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_image_data_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_instance_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_opengles2_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_pdf_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_surface_3d_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_testing_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_url_loader_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_url_request_info_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_url_response_info_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppb_var_deprecated_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppp_class_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_ppp_instance_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_serialized_flash_menu_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_serialized_structs_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_proxy_serialized_var_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_ppapi_proxy_main_libppapi_proxy_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_ppapi_proxy_callback_tracker_cpp(it);
  PUBLIC_ppapi_proxy_dispatcher_cpp(it);
  PUBLIC_ppapi_proxy_host_dispatcher_cpp(it);
  PUBLIC_ppapi_proxy_host_var_serialization_rules_cpp(it);
  PUBLIC_ppapi_proxy_interface_proxy_cpp(it);
  PUBLIC_ppapi_proxy_plugin_dispatcher_cpp(it);
  PUBLIC_ppapi_proxy_plugin_resource_cpp(it);
  PUBLIC_ppapi_proxy_plugin_resource_tracker_cpp(it);
  PUBLIC_ppapi_proxy_plugin_var_serialization_rules_cpp(it);
  PUBLIC_ppapi_proxy_plugin_var_tracker_cpp(it);
  PUBLIC_ppapi_proxy_ppapi_messages_cpp(it);
  PUBLIC_ppapi_proxy_ppapi_param_traits_cpp(it);
  PUBLIC_ppapi_proxy_ppb_audio_config_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_audio_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_buffer_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_char_set_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_context_3d_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_core_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_cursor_control_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_file_chooser_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_file_ref_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_file_system_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_flash_file_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_flash_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_flash_menu_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_font_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_fullscreen_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_gles_chromium_texture_mapping_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_graphics_2d_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_image_data_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_instance_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_opengles2_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_pdf_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_surface_3d_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_testing_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_url_loader_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_url_request_info_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_url_response_info_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppb_var_deprecated_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppp_class_proxy_cpp(it);
  PUBLIC_ppapi_proxy_ppp_instance_proxy_cpp(it);
  PUBLIC_ppapi_proxy_serialized_flash_menu_cpp(it);
  PUBLIC_ppapi_proxy_serialized_structs_cpp(it);
  PUBLIC_ppapi_proxy_serialized_var_cpp(it);
}

#endif

