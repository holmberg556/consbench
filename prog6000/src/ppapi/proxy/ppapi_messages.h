/* CONSBENCH file begin */
#ifndef PPAPI_MESSAGES_H_2754
#define PPAPI_MESSAGES_H_2754

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, ppapi_proxy_ppapi_messages_h, "ppapi/proxy/ppapi_messages.h");

/* CONSBENCH includes begin */
#include <string>
#include <vector>
#include "base/basictypes.h"
#include "base/process.h"
#include "base/shared_memory.h"
#include "base/string16.h"
#include "base/sync_socket.h"
#include "ipc/ipc_message_utils.h"
#include "ipc/ipc_platform_file.h"
#include "ppapi/c/pp_bool.h"
#include "ppapi/c/pp_instance.h"
#include "ppapi/c/pp_module.h"
#include "ppapi/c/pp_resource.h"
#include "ppapi/proxy/ppapi_param_traits.h"
#include "ppapi/proxy/serialized_flash_menu.h"
#include "ppapi/proxy/serialized_structs.h"
#include "ppapi/proxy/ppapi_messages_internal.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(ppapi_proxy_ppapi_messages_h, "ppapi/proxy/ppapi_messages.h");

/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ............................. */
/*-no-  */
/*-no- #ifndef PPAPI_PROXY_PPAPI_MESSAGES_H_ */
/*-no- #define PPAPI_PROXY_PPAPI_MESSAGES_H_ */
/*-no- #pragma once */
/*-no-  */
/*-no- #include <string> */
/*-no- #include <vector> */
/*-no-  */
/*-no- #include "base/basictypes.h" */
/*-no- #include "base/process.h" */
/*-no- #include "base/shared_memory.h" */
/*-no- #include "base/string16.h" */
/*-no- #include "base/sync_socket.h" */
/*-no- #include "ipc/ipc_message_utils.h" */
/*-no- #include "ipc/ipc_platform_file.h" */
/*-no- #include "ppapi/c/pp_bool.h" */
/*-no- #include "ppapi/c/pp_instance.h" */
/*-no- #include "ppapi/c/pp_module.h" */
/*-no- #include "ppapi/c/pp_resource.h" */
/*-no- #include "ppapi/proxy/ppapi_param_traits.h" */
/*-no- #include "ppapi/proxy/serialized_flash_menu.h" */
/*-no- #include "ppapi/proxy/serialized_structs.h" */
/*-no-  */
/*-no- #include "ppapi/proxy/ppapi_messages_internal.h" */
/*-no-  */
/*-no- #endif  // PPAPI_PROXY_PPAPI_MESSAGES_H_ */

#endif
/* CONSBENCH file end */
