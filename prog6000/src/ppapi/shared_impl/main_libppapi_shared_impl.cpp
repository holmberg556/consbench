/* CONSBENCH file begin */
#ifndef MAIN_LIBPPAPI_SHARED_IMPL_CPP
#define MAIN_LIBPPAPI_SHARED_IMPL_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, ppapi_shared_impl_main_libppapi_shared_impl_cpp, "ppapi/shared_impl/main_libppapi_shared_impl.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(ppapi_shared_impl_main_libppapi_shared_impl_cpp, "ppapi/shared_impl/main_libppapi_shared_impl.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_ppapi_shared_impl_audio_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ppapi_shared_impl_image_data_impl_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_ppapi_shared_impl_main_libppapi_shared_impl_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_ppapi_shared_impl_audio_impl_cpp(it);
  PUBLIC_ppapi_shared_impl_image_data_impl_cpp(it);
}

#endif

