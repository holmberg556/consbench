/* CONSBENCH file begin */
#ifndef MAIN_LIBPRINTING_CPP
#define MAIN_LIBPRINTING_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, printing_main_libprinting_cpp, "printing/main_libprinting.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(printing_main_libprinting_cpp, "printing/main_libprinting.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_printing_backend_print_backend_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_backend_print_backend_consts_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_backend_print_backend_dummy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_image_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_image_cairo_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_native_metafile_factory_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_page_number_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_page_overlays_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_page_range_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_page_setup_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_pdf_ps_metafile_cairo_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_printed_document_cairo_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_printed_document_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_printed_document_posix_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_printed_page_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_printing_context_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_printing_context_cairo_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_print_settings_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_print_settings_initializer_gtk_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_units_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_backend_cups_helper_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_printing_backend_print_backend_cups_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_printing_main_libprinting_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_printing_backend_print_backend_cpp(it);
  PUBLIC_printing_backend_print_backend_consts_cpp(it);
  PUBLIC_printing_backend_print_backend_dummy_cpp(it);
  PUBLIC_printing_image_cpp(it);
  PUBLIC_printing_image_cairo_cpp(it);
  PUBLIC_printing_native_metafile_factory_cpp(it);
  PUBLIC_printing_page_number_cpp(it);
  PUBLIC_printing_page_overlays_cpp(it);
  PUBLIC_printing_page_range_cpp(it);
  PUBLIC_printing_page_setup_cpp(it);
  PUBLIC_printing_pdf_ps_metafile_cairo_cpp(it);
  PUBLIC_printing_printed_document_cairo_cpp(it);
  PUBLIC_printing_printed_document_cpp(it);
  PUBLIC_printing_printed_document_posix_cpp(it);
  PUBLIC_printing_printed_page_cpp(it);
  PUBLIC_printing_printing_context_cpp(it);
  PUBLIC_printing_printing_context_cairo_cpp(it);
  PUBLIC_printing_print_settings_cpp(it);
  PUBLIC_printing_print_settings_initializer_gtk_cpp(it);
  PUBLIC_printing_units_cpp(it);
  PUBLIC_printing_backend_cups_helper_cpp(it);
  PUBLIC_printing_backend_print_backend_cups_cpp(it);
}

#endif

