//----------------------------------------------------------------------
// prog6000.cpp
//----------------------------------------------------------------------
// Copyright 2011-2012 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "consbench".
//
// "consbench" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "consbench" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "consbench".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

#include "ConsBench.h"
#include "ConsBenchImpl.h"

#include <iostream>
#include <set>
#include <stdlib.h>
#include <string.h>

extern void LIB_PUBLIC_third_party_protobuf_src_google_protobuf_main_libprotobuf_full_do_not_use_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_protobuf_src_google_protobuf_main_libprotobuf_lite_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_yasm_source_patched_yasm_libyasm_main_libgenperf_libs_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_v8_src_main_libv8_base_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_out_Debug_obj_gen_main_libv8_nosnapshot_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_app_gfx_gl_main_libapp_base_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_tcmalloc_chromium_src_main_liballocator_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_base_main_libbase_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_base_i18n_main_libbase_i18n_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_base_third_party_symbolize_main_libsymbolize_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_base_third_party_xdg_mime_main_libxdg_mime_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_base_third_party_dynamic_annotations_main_libdynamic_annotations_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_googleurl_src_main_libgoogleurl_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_out_Debug_obj_gen_policy_policy_main_libpolicy_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_default_plugin_main_libdefault_plugin_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_browser_main_libbrowser_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_gpu_main_libchrome_gpu_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_common_main_libcommon_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_common_main_libcommon_constants_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_common_net_main_libcommon_net_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_browser_debugger_main_libdebugger_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_installer_util_main_libinstaller_util_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_nacl_main_libnacl_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_plugin_main_libplugin_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_ppapi_plugin_main_libppapi_plugin_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_profile_import_main_libprofile_import_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_renderer_main_librenderer_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_service_cloud_print_main_libservice_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_browser_sync_engine_main_libsync_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_browser_sync_notifier_main_libsync_notifier_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_browser_sync_engine_main_libsyncapi_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_utility_main_libutility_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_chrome_worker_main_libworker_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_content_browser_renderer_host_main_libcontent_browser_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_content_common_main_libcontent_common_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_gpu_command_buffer_client_main_libcommand_buffer_client_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_gpu_command_buffer_common_main_libcommand_buffer_common_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_gpu_command_buffer_service_main_libcommand_buffer_service_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_gpu_command_buffer_client_main_libgles2_c_lib_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_gpu_command_buffer_client_main_libgles2_cmd_helper_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_gpu_command_buffer_client_main_libgles2_implementation_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_gpu_ipc_main_libgpu_ipc_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_ipc_main_libipc_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_jingle_notifier_listener_main_libnotifier_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_media_base_main_libcpu_features_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_media_base_main_libmedia_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_media_omx_main_libomx_wrapper_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_media_base_main_libyuv_convert_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_media_base_main_libyuv_convert_sse2_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_shared_gio_main_libgio_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_shared_imc_main_libimc_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_shared_platform_main_libplatform_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_shared_ppapi_proxy_main_libnacl_ppapi_browser_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_shared_srpc_main_libnonnacl_srpc_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_shared_utils_main_libutils_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_trusted_debug_stub_main_libdebug_stub_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_trusted_desc_main_libnrd_xfer_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_trusted_gdb_rsp_main_libgdb_rsp_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_trusted_gio_main_libgio_wrapped_desc_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_trusted_nacl_base_main_libnacl_base_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_trusted_nonnacl_util_main_libsel_ldr_launcher_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_trusted_nonnacl_util_posix_main_libnonnacl_util_posix_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_trusted_platform_qualify_linux_main_libplatform_qual_lib_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_trusted_service_runtime_arch_x86_main_libservice_runtime_x86_common_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_trusted_service_runtime_arch_x86_64_main_libservice_runtime_x86_64_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_trusted_service_runtime_main_libenv_cleanser_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_trusted_service_runtime_main_libsel_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_trusted_validator_x86_main_libncopcode_utils_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_trusted_validator_x86_main_libncvalidate_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_native_client_src_trusted_validator_x86_main_libncvalidate_sfi_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_net_server_main_libhttp_server_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_net_http_main_libnet_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_net_base_main_libnet_base_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_net_third_party_nss_ssl_main_libssl_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_ppapi_cpp_main_libppapi_cpp_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_ppapi_cpp_dev_main_libppapi_cpp_objects_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_ppapi_proxy_main_libppapi_proxy_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_ppapi_shared_impl_main_libppapi_shared_impl_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_printing_main_libprinting_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_remoting_base_main_libchromoting_base_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_remoting_client_main_libchromoting_client_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_remoting_host_main_libchromoting_host_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_remoting_jingle_glue_main_libchromoting_jingle_glue_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_remoting_client_plugin_main_libchromoting_plugin_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_remoting_protocol_main_libchromoting_protocol_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_remoting_host_main_libdiffer_block_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_remoting_host_main_libdiffer_block_sse2_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_out_Debug_obj_gen_protoc_out_remoting_proto_main_libchromotocol_proto_lib_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_out_Debug_obj_gen_protoc_out_remoting_proto_main_libtrace_proto_lib_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_sdch_open_vcdiff_src_main_libsdch_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_seccompsandbox_main_libseccomp_sandbox_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_skia_src_core_main_libskia_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_skia_src_opts_main_libskia_opts_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_testing_gtest_src_main_libgtest_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_WebKit_Source_JavaScriptCore_wtf_main_libwtf_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_WebKit_Source_JavaScriptCore_yarr_main_libyarr_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_out_Debug_obj_gen_webkit_main_libwebcore_bindings_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_WebKit_Source_WebCore_html_main_libwebcore_html_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_WebKit_Source_WebCore_platform_graphics_main_libwebcore_platform_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_WebKit_Source_WebCore_dom_main_libwebcore_remaining_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_WebKit_Source_WebCore_rendering_main_libwebcore_rendering_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_WebKit_Source_WebCore_svg_main_libwebcore_svg_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_WebKit_Source_WebKit_chromium_src_main_libwebkit_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_angle_src_compiler_main_libtranslator_common_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_angle_src_compiler_main_libtranslator_glsl_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_main_libcacheinvalidation_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_cld_encodings_compact_lang_det_main_libcld_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_out_Debug_obj_target_geni_main_libffmpeg_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_flac_src_libFLAC_main_libflac_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_harfbuzz_src_main_libharfbuzz_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_hunspell_src_hunspell_main_libhunspell_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_iccjpeg_main_libiccjpeg_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_icu_source_i18n_main_libicui18n_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_icu_source_common_main_libicuuc_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_libevent_main_libevent_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_libjingle_source_talk_base_main_libjingle_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_libjingle_source_talk_p2p_base_main_libjingle_p2p_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_libjpeg_turbo_main_libjpeg_turbo_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_libsrtp_src_crypto_cipher_main_libsrtp_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_libvpx_source_libvpx_vp8_common_main_libvpx_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_libwebp_main_libwebp_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_libxml_src_main_libxml2_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_libxslt_libxslt_main_libxslt_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_modp_b64_main_libmodp_b64_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_out_Debug_obj_target_geni_main_libil_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_ots_src_main_libots_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_protobuf_src_google_protobuf_main_libprotobuf_lite_2_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_speex_libspeex_main_libspeex_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_sqlite_src_src_main_libsqlite3_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_undoview_main_libundoview_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_third_party_zlib_contrib_minizip_main_libzlib_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_ui_gfx_main_libgfx_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_ui_base_animation_main_libui_base_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_v8_src_extensions_experimental_main_libi18n_api_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_v8_src_main_libv8_base_2_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_out_Debug_obj_target_geni_main_libv8_snapshot_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_webkit_appcache_main_libappcache_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_webkit_blob_main_libblob_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_webkit_database_main_libdatabase_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_webkit_fileapi_main_libfileapi_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_webkit_plugins_ppapi_main_libglue_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_webkit_quota_main_libquota_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_webkit_gpu_main_libwebkit_gpu_cpp(ConsBenchAcc_iter_t it);
extern void LIB_PUBLIC_webkit_glue_main_libwebkit_user_agent_cpp(ConsBenchAcc_iter_t it);

void MAIN_PUBLIC_prog(ConsBenchAcc_iter_t it)
{
#ifndef MINIMAL_PROG
  LIB_PUBLIC_third_party_protobuf_src_google_protobuf_main_libprotobuf_full_do_not_use_cpp(it);
  LIB_PUBLIC_third_party_protobuf_src_google_protobuf_main_libprotobuf_lite_cpp(it);
  LIB_PUBLIC_third_party_yasm_source_patched_yasm_libyasm_main_libgenperf_libs_cpp(it);
  LIB_PUBLIC_v8_src_main_libv8_base_cpp(it);
  LIB_PUBLIC_out_Debug_obj_gen_main_libv8_nosnapshot_cpp(it);
  LIB_PUBLIC_app_gfx_gl_main_libapp_base_cpp(it);
  LIB_PUBLIC_third_party_tcmalloc_chromium_src_main_liballocator_cpp(it);
  LIB_PUBLIC_base_main_libbase_cpp(it);
  LIB_PUBLIC_base_i18n_main_libbase_i18n_cpp(it);
  LIB_PUBLIC_base_third_party_symbolize_main_libsymbolize_cpp(it);
  LIB_PUBLIC_base_third_party_xdg_mime_main_libxdg_mime_cpp(it);
  LIB_PUBLIC_base_third_party_dynamic_annotations_main_libdynamic_annotations_cpp(it);
  LIB_PUBLIC_googleurl_src_main_libgoogleurl_cpp(it);
  LIB_PUBLIC_out_Debug_obj_gen_policy_policy_main_libpolicy_cpp(it);
  LIB_PUBLIC_chrome_default_plugin_main_libdefault_plugin_cpp(it);
  LIB_PUBLIC_chrome_browser_main_libbrowser_cpp(it);
  LIB_PUBLIC_chrome_gpu_main_libchrome_gpu_cpp(it);
  LIB_PUBLIC_chrome_common_main_libcommon_cpp(it);
  LIB_PUBLIC_chrome_common_main_libcommon_constants_cpp(it);
  LIB_PUBLIC_chrome_common_net_main_libcommon_net_cpp(it);
  LIB_PUBLIC_chrome_browser_debugger_main_libdebugger_cpp(it);
  LIB_PUBLIC_chrome_installer_util_main_libinstaller_util_cpp(it);
  LIB_PUBLIC_chrome_nacl_main_libnacl_cpp(it);
  LIB_PUBLIC_chrome_plugin_main_libplugin_cpp(it);
  LIB_PUBLIC_chrome_ppapi_plugin_main_libppapi_plugin_cpp(it);
  LIB_PUBLIC_chrome_profile_import_main_libprofile_import_cpp(it);
  LIB_PUBLIC_chrome_renderer_main_librenderer_cpp(it);
  LIB_PUBLIC_chrome_service_cloud_print_main_libservice_cpp(it);
  LIB_PUBLIC_chrome_browser_sync_engine_main_libsync_cpp(it);
  LIB_PUBLIC_chrome_browser_sync_notifier_main_libsync_notifier_cpp(it);
  LIB_PUBLIC_chrome_browser_sync_engine_main_libsyncapi_cpp(it);
  LIB_PUBLIC_chrome_utility_main_libutility_cpp(it);
  LIB_PUBLIC_chrome_worker_main_libworker_cpp(it);
  LIB_PUBLIC_content_browser_renderer_host_main_libcontent_browser_cpp(it);
  LIB_PUBLIC_content_common_main_libcontent_common_cpp(it);
  LIB_PUBLIC_gpu_command_buffer_client_main_libcommand_buffer_client_cpp(it);
  LIB_PUBLIC_gpu_command_buffer_common_main_libcommand_buffer_common_cpp(it);
  LIB_PUBLIC_gpu_command_buffer_service_main_libcommand_buffer_service_cpp(it);
  LIB_PUBLIC_gpu_command_buffer_client_main_libgles2_c_lib_cpp(it);
  LIB_PUBLIC_gpu_command_buffer_client_main_libgles2_cmd_helper_cpp(it);
  LIB_PUBLIC_gpu_command_buffer_client_main_libgles2_implementation_cpp(it);
  LIB_PUBLIC_gpu_ipc_main_libgpu_ipc_cpp(it);
  LIB_PUBLIC_ipc_main_libipc_cpp(it);
  LIB_PUBLIC_jingle_notifier_listener_main_libnotifier_cpp(it);
  LIB_PUBLIC_media_base_main_libcpu_features_cpp(it);
  LIB_PUBLIC_media_base_main_libmedia_cpp(it);
  LIB_PUBLIC_media_omx_main_libomx_wrapper_cpp(it);
  LIB_PUBLIC_media_base_main_libyuv_convert_cpp(it);
  LIB_PUBLIC_media_base_main_libyuv_convert_sse2_cpp(it);
  LIB_PUBLIC_native_client_src_shared_gio_main_libgio_cpp(it);
  LIB_PUBLIC_native_client_src_shared_imc_main_libimc_cpp(it);
  LIB_PUBLIC_native_client_src_shared_platform_main_libplatform_cpp(it);
  LIB_PUBLIC_native_client_src_shared_ppapi_proxy_main_libnacl_ppapi_browser_cpp(it);
  LIB_PUBLIC_native_client_src_shared_srpc_main_libnonnacl_srpc_cpp(it);
  LIB_PUBLIC_native_client_src_shared_utils_main_libutils_cpp(it);
  LIB_PUBLIC_native_client_src_trusted_debug_stub_main_libdebug_stub_cpp(it);
  LIB_PUBLIC_native_client_src_trusted_desc_main_libnrd_xfer_cpp(it);
  LIB_PUBLIC_native_client_src_trusted_gdb_rsp_main_libgdb_rsp_cpp(it);
  LIB_PUBLIC_native_client_src_trusted_gio_main_libgio_wrapped_desc_cpp(it);
  LIB_PUBLIC_native_client_src_trusted_nacl_base_main_libnacl_base_cpp(it);
  LIB_PUBLIC_native_client_src_trusted_nonnacl_util_main_libsel_ldr_launcher_cpp(it);
  LIB_PUBLIC_native_client_src_trusted_nonnacl_util_posix_main_libnonnacl_util_posix_cpp(it);
  LIB_PUBLIC_native_client_src_trusted_platform_qualify_linux_main_libplatform_qual_lib_cpp(it);
  LIB_PUBLIC_native_client_src_trusted_service_runtime_arch_x86_main_libservice_runtime_x86_common_cpp(it);
  LIB_PUBLIC_native_client_src_trusted_service_runtime_arch_x86_64_main_libservice_runtime_x86_64_cpp(it);
  LIB_PUBLIC_native_client_src_trusted_service_runtime_main_libenv_cleanser_cpp(it);
  LIB_PUBLIC_native_client_src_trusted_service_runtime_main_libsel_cpp(it);
  LIB_PUBLIC_native_client_src_trusted_validator_x86_main_libncopcode_utils_cpp(it);
  LIB_PUBLIC_native_client_src_trusted_validator_x86_main_libncvalidate_cpp(it);
  LIB_PUBLIC_native_client_src_trusted_validator_x86_main_libncvalidate_sfi_cpp(it);
  LIB_PUBLIC_net_server_main_libhttp_server_cpp(it);
  LIB_PUBLIC_net_http_main_libnet_cpp(it);
  LIB_PUBLIC_net_base_main_libnet_base_cpp(it);
  LIB_PUBLIC_net_third_party_nss_ssl_main_libssl_cpp(it);
  LIB_PUBLIC_ppapi_cpp_main_libppapi_cpp_cpp(it);
  LIB_PUBLIC_ppapi_cpp_dev_main_libppapi_cpp_objects_cpp(it);
  LIB_PUBLIC_ppapi_proxy_main_libppapi_proxy_cpp(it);
  LIB_PUBLIC_ppapi_shared_impl_main_libppapi_shared_impl_cpp(it);
  LIB_PUBLIC_printing_main_libprinting_cpp(it);
  LIB_PUBLIC_remoting_base_main_libchromoting_base_cpp(it);
  LIB_PUBLIC_remoting_client_main_libchromoting_client_cpp(it);
  LIB_PUBLIC_remoting_host_main_libchromoting_host_cpp(it);
  LIB_PUBLIC_remoting_jingle_glue_main_libchromoting_jingle_glue_cpp(it);
  LIB_PUBLIC_remoting_client_plugin_main_libchromoting_plugin_cpp(it);
  LIB_PUBLIC_remoting_protocol_main_libchromoting_protocol_cpp(it);
  LIB_PUBLIC_remoting_host_main_libdiffer_block_cpp(it);
  LIB_PUBLIC_remoting_host_main_libdiffer_block_sse2_cpp(it);
  LIB_PUBLIC_out_Debug_obj_gen_protoc_out_remoting_proto_main_libchromotocol_proto_lib_cpp(it);
  LIB_PUBLIC_out_Debug_obj_gen_protoc_out_remoting_proto_main_libtrace_proto_lib_cpp(it);
  LIB_PUBLIC_sdch_open_vcdiff_src_main_libsdch_cpp(it);
  LIB_PUBLIC_seccompsandbox_main_libseccomp_sandbox_cpp(it);
  LIB_PUBLIC_third_party_skia_src_core_main_libskia_cpp(it);
  LIB_PUBLIC_third_party_skia_src_opts_main_libskia_opts_cpp(it);
  LIB_PUBLIC_testing_gtest_src_main_libgtest_cpp(it);
  LIB_PUBLIC_third_party_WebKit_Source_JavaScriptCore_wtf_main_libwtf_cpp(it);
  LIB_PUBLIC_third_party_WebKit_Source_JavaScriptCore_yarr_main_libyarr_cpp(it);
  LIB_PUBLIC_out_Debug_obj_gen_webkit_main_libwebcore_bindings_cpp(it);
  LIB_PUBLIC_third_party_WebKit_Source_WebCore_html_main_libwebcore_html_cpp(it);
  LIB_PUBLIC_third_party_WebKit_Source_WebCore_platform_graphics_main_libwebcore_platform_cpp(it);
  LIB_PUBLIC_third_party_WebKit_Source_WebCore_dom_main_libwebcore_remaining_cpp(it);
  LIB_PUBLIC_third_party_WebKit_Source_WebCore_rendering_main_libwebcore_rendering_cpp(it);
  LIB_PUBLIC_third_party_WebKit_Source_WebCore_svg_main_libwebcore_svg_cpp(it);
  LIB_PUBLIC_third_party_WebKit_Source_WebKit_chromium_src_main_libwebkit_cpp(it);
  LIB_PUBLIC_third_party_angle_src_compiler_main_libtranslator_common_cpp(it);
  LIB_PUBLIC_third_party_angle_src_compiler_main_libtranslator_glsl_cpp(it);
  LIB_PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_main_libcacheinvalidation_cpp(it);
  LIB_PUBLIC_third_party_cld_encodings_compact_lang_det_main_libcld_cpp(it);
  LIB_PUBLIC_out_Debug_obj_target_geni_main_libffmpeg_cpp(it);
  LIB_PUBLIC_third_party_flac_src_libFLAC_main_libflac_cpp(it);
  LIB_PUBLIC_third_party_harfbuzz_src_main_libharfbuzz_cpp(it);
  LIB_PUBLIC_third_party_hunspell_src_hunspell_main_libhunspell_cpp(it);
  LIB_PUBLIC_third_party_iccjpeg_main_libiccjpeg_cpp(it);
  LIB_PUBLIC_third_party_icu_source_i18n_main_libicui18n_cpp(it);
  LIB_PUBLIC_third_party_icu_source_common_main_libicuuc_cpp(it);
  LIB_PUBLIC_third_party_libevent_main_libevent_cpp(it);
  LIB_PUBLIC_third_party_libjingle_source_talk_base_main_libjingle_cpp(it);
  LIB_PUBLIC_third_party_libjingle_source_talk_p2p_base_main_libjingle_p2p_cpp(it);
  LIB_PUBLIC_third_party_libjpeg_turbo_main_libjpeg_turbo_cpp(it);
  LIB_PUBLIC_third_party_libsrtp_src_crypto_cipher_main_libsrtp_cpp(it);
  LIB_PUBLIC_third_party_libvpx_source_libvpx_vp8_common_main_libvpx_cpp(it);
  LIB_PUBLIC_third_party_libwebp_main_libwebp_cpp(it);
  LIB_PUBLIC_third_party_libxml_src_main_libxml2_cpp(it);
  LIB_PUBLIC_third_party_libxslt_libxslt_main_libxslt_cpp(it);
  LIB_PUBLIC_third_party_modp_b64_main_libmodp_b64_cpp(it);
  LIB_PUBLIC_out_Debug_obj_target_geni_main_libil_cpp(it);
  LIB_PUBLIC_third_party_ots_src_main_libots_cpp(it);
  LIB_PUBLIC_third_party_protobuf_src_google_protobuf_main_libprotobuf_lite_2_cpp(it);
  LIB_PUBLIC_third_party_speex_libspeex_main_libspeex_cpp(it);
  LIB_PUBLIC_third_party_sqlite_src_src_main_libsqlite3_cpp(it);
  LIB_PUBLIC_third_party_undoview_main_libundoview_cpp(it);
  LIB_PUBLIC_third_party_zlib_contrib_minizip_main_libzlib_cpp(it);
  LIB_PUBLIC_ui_gfx_main_libgfx_cpp(it);
  LIB_PUBLIC_ui_base_animation_main_libui_base_cpp(it);
  LIB_PUBLIC_v8_src_extensions_experimental_main_libi18n_api_cpp(it);
  LIB_PUBLIC_v8_src_main_libv8_base_2_cpp(it);
  LIB_PUBLIC_out_Debug_obj_target_geni_main_libv8_snapshot_cpp(it);
  LIB_PUBLIC_webkit_appcache_main_libappcache_cpp(it);
  LIB_PUBLIC_webkit_blob_main_libblob_cpp(it);
  LIB_PUBLIC_webkit_database_main_libdatabase_cpp(it);
  LIB_PUBLIC_webkit_fileapi_main_libfileapi_cpp(it);
  LIB_PUBLIC_webkit_plugins_ppapi_main_libglue_cpp(it);
  LIB_PUBLIC_webkit_quota_main_libquota_cpp(it);
#endif
  LIB_PUBLIC_webkit_gpu_main_libwebkit_gpu_cpp(it);
  LIB_PUBLIC_webkit_glue_main_libwebkit_user_agent_cpp(it);
}

static string_map_t versions;
static int n_files;
static int n_includes;

void callback(ConsBenchAcc & acc)
{
  static int nnn = 0;
  //std::cout << "hello " << nnn++ << std::endl;
  ConsBenchAcc::Impl * impl = acc.get_impl();
  //std::cout << "hello " << impl->mOutput.size() << std::endl;
  //std::cout << "hello Versions " << impl->mVersions.size() << std::endl;
  //std::cout << "hello " << impl->mOutput[0] << std::endl;

  n_files += 1;
  string_map_t & versions1 = impl->mVersions;
  for (string_map_t::iterator it1 = versions1.begin(); it1 != versions1.end(); ++it1) {
    n_includes += 1;
    string_map_t::iterator it = versions.find( (*it1).first );
    if (it == versions.end()) {
      // insert new file
      versions[ (*it1).first ] = (*it1).second;
    }
    else {
      if ((*it).second == (*it1).second) {
        // ok, same value
      }
      else {
        std::cout << "ERROR: " << impl->mOutput[0]
                  << " i1=" << (*it1).second << " i=" << (*it).second
                  << std::endl;
      }
    }
  }
}

//----------------------------------------------------------------------
std::string time_arg;
bool quiet_arg;
std::string file_arg;
std::string nr_arg;

void CALLBACK_newer_src_files(ConsBenchAcc & acc)
{
  ConsBenchAcc::Impl * impl = acc.get_impl();

  if (impl->mTimeStr >= time_arg) {
    if (! quiet_arg) std::cout << impl->mTimeStr << "   ";
    std::cout << impl->mSource << std::endl;
  }
}

//----------------------------------------------------------------------

bool find_include(ConsBenchAcc & acc, const std::string & include, std::string & nr)
{
  ConsBenchAcc::Impl * impl = acc.get_impl();
  string_map_t & versions1 = impl->mVersions;
  
  string_map_t::iterator it1 = versions1.find(include);
  if (it1 == versions1.end()) {
    return false;
  }
  else {
    nr = (*it1).second;
    return true;
  }
}

void CALLBACK_using_src_files(ConsBenchAcc & acc)
{
  ConsBenchAcc::Impl * impl = acc.get_impl();

  std::string expected_nr = nr_arg;

  std::string nr;
  if (find_include(acc, file_arg, nr)) {
    std::cout << impl->mTimeStr << "   " << impl->mSource;
    if (nr_arg == "") {
      std::cout << "   " << nr;
    }
    std::cout << std::endl;
    if (expected_nr == "" || expected_nr == ".") {
      expected_nr = nr;
    }
    else if (nr != expected_nr) {
      std::cout << "ERROR: wrong nr: " << nr
                << " instead of " << expected_nr
                << " in " << impl->mSource
                << std::endl;
      exit(1);
    }
    else {
      // ok
    }
  }
}

//----------------------------------------------------------------------

typedef std::set<std::string> string_set_t;
string_set_t all_files_ret;

void CALLBACK_all_files(ConsBenchAcc & acc)
{
  ConsBenchAcc::Impl * impl = acc.get_impl();

  string_map_t & versions1 = impl->mVersions;
  for (string_map_t::iterator it1 = versions1.begin(); it1 != versions1.end(); ++it1) {
    all_files_ret.insert( (*it1).first );
  }
}

//----------------------------------------------------------------------

string_set_t all_src_files_ret;

void CALLBACK_all_src_files(ConsBenchAcc & acc)
{
  ConsBenchAcc::Impl * impl = acc.get_impl();
  all_src_files_ret.insert( impl->mSource );
}

//----------------------------------------------------------------------

void usage_and_exit()
{
  std::cout << "Usage: prog newer-src-files TIME [quiet]" << std::endl;
  std::cout << "Usage: prog using-src-files FILE [N]" << std::endl;
  std::cout << "Usage: prog all-files" << std::endl;
  std::cout << "Usage: prog all-src-files" << std::endl;
  exit(1);
}

//----------------------------------------------------------------------

int main(int argc, char *argv[])
{
  int nargs = argc - 1;
  if (nargs < 1) usage_and_exit();

  std::string what = argv[1];

  if (what == "newer-src-files") {
    if (nargs < 2) usage_and_exit();
    std::string time  = argv[2];
    bool quiet = (nargs >= 3) && strcmp(argv[3], "quiet") == 0;

    time_arg = time;
    quiet_arg = quiet;
    MAIN_PUBLIC_prog( &CALLBACK_newer_src_files );
  }

  else if (what == "using-src-files") {
    if (nargs < 2) usage_and_exit();
    std::string file = argv[2];
    std::string nr = argv[3] ? argv[3] : "";

    file_arg = file;
    nr_arg = nr;
    MAIN_PUBLIC_prog( &CALLBACK_using_src_files );
  }

  else if (what == "all-files") {
    all_files_ret.clear();
    MAIN_PUBLIC_prog( &CALLBACK_all_files );

    for (string_set_t::iterator it1 = all_files_ret.begin(); it1 != all_files_ret.end(); ++it1) {
      std::cout << (*it1) << std::endl;
    }
  }

  else if (what == "all-src-files") {
    all_src_files_ret.clear();
    MAIN_PUBLIC_prog( &CALLBACK_all_src_files );

    for (string_set_t::iterator it1 = all_src_files_ret.begin(); it1 != all_src_files_ret.end(); ++it1) {
      std::cout << (*it1) << std::endl;
    }
  }

  else {
    std::cout << "ERROR: unknown action: " << what << std::endl;
    exit(1);
  }

  //std::cout << "n_files=" << n_files << " n_includes=" << n_includes << std::endl;
  return 0;
}

