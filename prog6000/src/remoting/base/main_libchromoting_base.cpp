/* CONSBENCH file begin */
#ifndef MAIN_LIBCHROMOTING_BASE_CPP
#define MAIN_LIBCHROMOTING_BASE_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, remoting_base_main_libchromoting_base_cpp, "remoting/base/main_libchromoting_base.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(remoting_base_main_libchromoting_base_cpp, "remoting/base/main_libchromoting_base.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_remoting_base_capture_data_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_base_compound_buffer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_base_compressor_verbatim_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_base_compressor_zlib_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_base_constants_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_base_decoder_vp8_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_base_decoder_row_based_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_base_decompressor_verbatim_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_base_decompressor_zlib_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_base_encoder_vp8_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_base_encoder_row_based_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_base_tracer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_base_util_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_remoting_base_main_libchromoting_base_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_remoting_base_capture_data_cpp(it);
  PUBLIC_remoting_base_compound_buffer_cpp(it);
  PUBLIC_remoting_base_compressor_verbatim_cpp(it);
  PUBLIC_remoting_base_compressor_zlib_cpp(it);
  PUBLIC_remoting_base_constants_cpp(it);
  PUBLIC_remoting_base_decoder_vp8_cpp(it);
  PUBLIC_remoting_base_decoder_row_based_cpp(it);
  PUBLIC_remoting_base_decompressor_verbatim_cpp(it);
  PUBLIC_remoting_base_decompressor_zlib_cpp(it);
  PUBLIC_remoting_base_encoder_vp8_cpp(it);
  PUBLIC_remoting_base_encoder_row_based_cpp(it);
  PUBLIC_remoting_base_tracer_cpp(it);
  PUBLIC_remoting_base_util_cpp(it);
}

#endif

