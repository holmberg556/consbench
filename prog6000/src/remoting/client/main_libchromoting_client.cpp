/* CONSBENCH file begin */
#ifndef MAIN_LIBCHROMOTING_CLIENT_CPP
#define MAIN_LIBCHROMOTING_CLIENT_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, remoting_client_main_libchromoting_client_cpp, "remoting/client/main_libchromoting_client.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(remoting_client_main_libchromoting_client_cpp, "remoting/client/main_libchromoting_client.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_remoting_client_chromoting_client_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_client_chromoting_view_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_client_client_context_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_client_client_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_client_input_handler_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_client_rectangle_update_decoder_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_remoting_client_main_libchromoting_client_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_remoting_client_chromoting_client_cpp(it);
  PUBLIC_remoting_client_chromoting_view_cpp(it);
  PUBLIC_remoting_client_client_context_cpp(it);
  PUBLIC_remoting_client_client_util_cpp(it);
  PUBLIC_remoting_client_input_handler_cpp(it);
  PUBLIC_remoting_client_rectangle_update_decoder_cpp(it);
}

#endif

