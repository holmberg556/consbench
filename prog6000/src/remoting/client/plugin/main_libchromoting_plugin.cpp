/* CONSBENCH file begin */
#ifndef MAIN_LIBCHROMOTING_PLUGIN_CPP
#define MAIN_LIBCHROMOTING_PLUGIN_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, remoting_client_plugin_main_libchromoting_plugin_cpp, "remoting/client/plugin/main_libchromoting_plugin.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(remoting_client_plugin_main_libchromoting_plugin_cpp, "remoting/client/plugin/main_libchromoting_plugin.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_remoting_client_plugin_chromoting_instance_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_client_plugin_chromoting_scriptable_object_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_client_plugin_pepper_entrypoints_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_client_plugin_pepper_input_handler_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_client_plugin_pepper_port_allocator_session_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_client_plugin_pepper_view_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_client_plugin_pepper_view_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_client_plugin_pepper_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_client_plugin_pepper_xmpp_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_yuv_convert_2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_yuv_row_table_2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_media_base_yuv_row_posix_2_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_remoting_client_plugin_main_libchromoting_plugin_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_remoting_client_plugin_chromoting_instance_cpp(it);
  PUBLIC_remoting_client_plugin_chromoting_scriptable_object_cpp(it);
  PUBLIC_remoting_client_plugin_pepper_entrypoints_cpp(it);
  PUBLIC_remoting_client_plugin_pepper_input_handler_cpp(it);
  PUBLIC_remoting_client_plugin_pepper_port_allocator_session_cpp(it);
  PUBLIC_remoting_client_plugin_pepper_view_cpp(it);
  PUBLIC_remoting_client_plugin_pepper_view_proxy_cpp(it);
  PUBLIC_remoting_client_plugin_pepper_util_cpp(it);
  PUBLIC_remoting_client_plugin_pepper_xmpp_proxy_cpp(it);
  PUBLIC_media_base_yuv_convert_2_cpp(it);
  PUBLIC_media_base_yuv_row_table_2_cpp(it);
  PUBLIC_media_base_yuv_row_posix_2_cpp(it);
}

#endif

