/* CONSBENCH file begin */
#ifndef MAIN_LIBCHROMOTING_HOST_CPP
#define MAIN_LIBCHROMOTING_HOST_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, remoting_host_main_libchromoting_host_cpp, "remoting/host/main_libchromoting_host.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(remoting_host_main_libchromoting_host_cpp, "remoting/host/main_libchromoting_host.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_remoting_host_access_verifier_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_capturer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_capturer_fake_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_chromoting_host_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_chromoting_host_context_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_desktop_environment_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_differ_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_screen_recorder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_heartbeat_sender_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_host_config_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_host_key_pair_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_json_host_config_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_in_memory_host_config_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_capturer_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_event_executor_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_user_authenticator_pam_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_host_user_authenticator_linux_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_remoting_host_main_libchromoting_host_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_remoting_host_access_verifier_cpp(it);
  PUBLIC_remoting_host_capturer_cpp(it);
  PUBLIC_remoting_host_capturer_fake_cpp(it);
  PUBLIC_remoting_host_chromoting_host_cpp(it);
  PUBLIC_remoting_host_chromoting_host_context_cpp(it);
  PUBLIC_remoting_host_desktop_environment_cpp(it);
  PUBLIC_remoting_host_differ_cpp(it);
  PUBLIC_remoting_host_screen_recorder_cpp(it);
  PUBLIC_remoting_host_heartbeat_sender_cpp(it);
  PUBLIC_remoting_host_host_config_cpp(it);
  PUBLIC_remoting_host_host_key_pair_cpp(it);
  PUBLIC_remoting_host_json_host_config_cpp(it);
  PUBLIC_remoting_host_in_memory_host_config_cpp(it);
  PUBLIC_remoting_host_capturer_linux_cpp(it);
  PUBLIC_remoting_host_event_executor_linux_cpp(it);
  PUBLIC_remoting_host_user_authenticator_pam_cpp(it);
  PUBLIC_remoting_host_user_authenticator_linux_cpp(it);
}

#endif

