/* CONSBENCH file begin */
#ifndef MAIN_LIBDIFFER_BLOCK_SSE2_CPP
#define MAIN_LIBDIFFER_BLOCK_SSE2_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, remoting_host_main_libdiffer_block_sse2_cpp, "remoting/host/main_libdiffer_block_sse2.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(remoting_host_main_libdiffer_block_sse2_cpp, "remoting/host/main_libdiffer_block_sse2.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_remoting_host_differ_block_sse2_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_remoting_host_main_libdiffer_block_sse2_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_remoting_host_differ_block_sse2_cpp(it);
}

#endif

