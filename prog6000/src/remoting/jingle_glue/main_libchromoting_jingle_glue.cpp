/* CONSBENCH file begin */
#ifndef MAIN_LIBCHROMOTING_JINGLE_GLUE_CPP
#define MAIN_LIBCHROMOTING_JINGLE_GLUE_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, remoting_jingle_glue_main_libchromoting_jingle_glue_cpp, "remoting/jingle_glue/main_libchromoting_jingle_glue.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(remoting_jingle_glue_main_libchromoting_jingle_glue_cpp, "remoting/jingle_glue/main_libchromoting_jingle_glue.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_remoting_jingle_glue_channel_socket_adapter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_jingle_glue_http_port_allocator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_jingle_glue_iq_request_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_jingle_glue_jingle_client_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_jingle_glue_jingle_thread_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_jingle_glue_stream_socket_adapter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_jingle_glue_ssl_adapter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_jingle_glue_ssl_socket_adapter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_jingle_glue_utils_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_jingle_glue_xmpp_socket_adapter_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_remoting_jingle_glue_main_libchromoting_jingle_glue_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_remoting_jingle_glue_channel_socket_adapter_cpp(it);
  PUBLIC_remoting_jingle_glue_http_port_allocator_cpp(it);
  PUBLIC_remoting_jingle_glue_iq_request_cpp(it);
  PUBLIC_remoting_jingle_glue_jingle_client_cpp(it);
  PUBLIC_remoting_jingle_glue_jingle_thread_cpp(it);
  PUBLIC_remoting_jingle_glue_stream_socket_adapter_cpp(it);
  PUBLIC_remoting_jingle_glue_ssl_adapter_cpp(it);
  PUBLIC_remoting_jingle_glue_ssl_socket_adapter_cpp(it);
  PUBLIC_remoting_jingle_glue_utils_cpp(it);
  PUBLIC_remoting_jingle_glue_xmpp_socket_adapter_cpp(it);
}

#endif

