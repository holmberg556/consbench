/* CONSBENCH file begin */
#ifndef MAIN_LIBCHROMOTING_PROTOCOL_CPP
#define MAIN_LIBCHROMOTING_PROTOCOL_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, remoting_protocol_main_libchromoting_protocol_cpp, "remoting/protocol/main_libchromoting_protocol.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(remoting_protocol_main_libchromoting_protocol_cpp, "remoting/protocol/main_libchromoting_protocol.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_remoting_protocol_buffered_socket_writer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_client_control_sender_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_client_message_dispatcher_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_client_stub_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_connection_to_client_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_connection_to_host_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_host_control_sender_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_host_message_dispatcher_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_host_stub_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_input_sender_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_input_stub_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_jingle_session_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_jingle_session_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_message_decoder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_message_reader_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_protobuf_video_reader_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_protobuf_video_writer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_rtcp_writer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_rtp_reader_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_rtp_utils_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_rtp_video_reader_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_rtp_video_writer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_rtp_writer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_session_config_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_socket_reader_base_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_socket_wrapper_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_video_reader_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_remoting_protocol_video_writer_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_remoting_protocol_main_libchromoting_protocol_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_remoting_protocol_buffered_socket_writer_cpp(it);
  PUBLIC_remoting_protocol_client_control_sender_cpp(it);
  PUBLIC_remoting_protocol_client_message_dispatcher_cpp(it);
  PUBLIC_remoting_protocol_client_stub_cpp(it);
  PUBLIC_remoting_protocol_connection_to_client_cpp(it);
  PUBLIC_remoting_protocol_connection_to_host_cpp(it);
  PUBLIC_remoting_protocol_host_control_sender_cpp(it);
  PUBLIC_remoting_protocol_host_message_dispatcher_cpp(it);
  PUBLIC_remoting_protocol_host_stub_cpp(it);
  PUBLIC_remoting_protocol_input_sender_cpp(it);
  PUBLIC_remoting_protocol_input_stub_cpp(it);
  PUBLIC_remoting_protocol_jingle_session_cpp(it);
  PUBLIC_remoting_protocol_jingle_session_manager_cpp(it);
  PUBLIC_remoting_protocol_message_decoder_cpp(it);
  PUBLIC_remoting_protocol_message_reader_cpp(it);
  PUBLIC_remoting_protocol_protobuf_video_reader_cpp(it);
  PUBLIC_remoting_protocol_protobuf_video_writer_cpp(it);
  PUBLIC_remoting_protocol_rtcp_writer_cpp(it);
  PUBLIC_remoting_protocol_rtp_reader_cpp(it);
  PUBLIC_remoting_protocol_rtp_utils_cpp(it);
  PUBLIC_remoting_protocol_rtp_video_reader_cpp(it);
  PUBLIC_remoting_protocol_rtp_video_writer_cpp(it);
  PUBLIC_remoting_protocol_rtp_writer_cpp(it);
  PUBLIC_remoting_protocol_session_config_cpp(it);
  PUBLIC_remoting_protocol_socket_reader_base_cpp(it);
  PUBLIC_remoting_protocol_socket_wrapper_cpp(it);
  PUBLIC_remoting_protocol_util_cpp(it);
  PUBLIC_remoting_protocol_video_reader_cpp(it);
  PUBLIC_remoting_protocol_video_writer_cpp(it);
}

#endif

