/* CONSBENCH file begin */
#ifndef MAIN_LIBSDCH_CPP
#define MAIN_LIBSDCH_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, sdch_open_vcdiff_src_main_libsdch_cpp, "sdch/open-vcdiff/src/main_libsdch.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(sdch_open_vcdiff_src_main_libsdch_cpp, "sdch/open-vcdiff/src/main_libsdch.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_sdch_open_vcdiff_src_addrcache_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_sdch_open_vcdiff_src_adler32_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_sdch_open_vcdiff_src_blockhash_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_sdch_open_vcdiff_src_codetable_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_sdch_open_vcdiff_src_decodetable_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_sdch_open_vcdiff_src_encodetable_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_sdch_open_vcdiff_src_headerparser_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_sdch_open_vcdiff_src_instruction_map_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_sdch_open_vcdiff_src_logging_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_sdch_open_vcdiff_src_varint_bigendian_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_sdch_open_vcdiff_src_vcdecoder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_sdch_open_vcdiff_src_vcdiffengine_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_sdch_open_vcdiff_src_main_libsdch_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_sdch_open_vcdiff_src_addrcache_cpp(it);
  PUBLIC_sdch_open_vcdiff_src_adler32_cpp(it);
  PUBLIC_sdch_open_vcdiff_src_blockhash_cpp(it);
  PUBLIC_sdch_open_vcdiff_src_codetable_cpp(it);
  PUBLIC_sdch_open_vcdiff_src_decodetable_cpp(it);
  PUBLIC_sdch_open_vcdiff_src_encodetable_cpp(it);
  PUBLIC_sdch_open_vcdiff_src_headerparser_cpp(it);
  PUBLIC_sdch_open_vcdiff_src_instruction_map_cpp(it);
  PUBLIC_sdch_open_vcdiff_src_logging_cpp(it);
  PUBLIC_sdch_open_vcdiff_src_varint_bigendian_cpp(it);
  PUBLIC_sdch_open_vcdiff_src_vcdecoder_cpp(it);
  PUBLIC_sdch_open_vcdiff_src_vcdiffengine_cpp(it);
}

#endif

