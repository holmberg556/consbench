/* CONSBENCH file begin */
#ifndef MAIN_LIBSECCOMP_SANDBOX_CPP
#define MAIN_LIBSECCOMP_SANDBOX_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, seccompsandbox_main_libseccomp_sandbox_cpp, "seccompsandbox/main_libseccomp_sandbox.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(seccompsandbox_main_libseccomp_sandbox_cpp, "seccompsandbox/main_libseccomp_sandbox.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_seccompsandbox_access_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_allocator_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_clone_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_exit_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_debug_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_getpid_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_gettid_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_ioctl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_ipc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_library_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_madvise_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_maps_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_mmap_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_mprotect_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_munmap_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_open_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_prctl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_sandbox_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_securemem_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_sigaction_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_sigprocmask_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_socketcall_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_stat_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_syscall_entrypoint_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_syscall_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_trusted_process_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_trusted_thread_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_seccompsandbox_x86_decode_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_seccompsandbox_main_libseccomp_sandbox_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_seccompsandbox_access_cpp(it);
  PUBLIC_seccompsandbox_allocator_cpp(it);
  PUBLIC_seccompsandbox_clone_cpp(it);
  PUBLIC_seccompsandbox_exit_cpp(it);
  PUBLIC_seccompsandbox_debug_cpp(it);
  PUBLIC_seccompsandbox_getpid_cpp(it);
  PUBLIC_seccompsandbox_gettid_cpp(it);
  PUBLIC_seccompsandbox_ioctl_cpp(it);
  PUBLIC_seccompsandbox_ipc_cpp(it);
  PUBLIC_seccompsandbox_library_cpp(it);
  PUBLIC_seccompsandbox_madvise_cpp(it);
  PUBLIC_seccompsandbox_maps_cpp(it);
  PUBLIC_seccompsandbox_mmap_cpp(it);
  PUBLIC_seccompsandbox_mprotect_cpp(it);
  PUBLIC_seccompsandbox_munmap_cpp(it);
  PUBLIC_seccompsandbox_open_cpp(it);
  PUBLIC_seccompsandbox_prctl_cpp(it);
  PUBLIC_seccompsandbox_sandbox_cpp(it);
  PUBLIC_seccompsandbox_securemem_cpp(it);
  PUBLIC_seccompsandbox_sigaction_cpp(it);
  PUBLIC_seccompsandbox_sigprocmask_cpp(it);
  PUBLIC_seccompsandbox_socketcall_cpp(it);
  PUBLIC_seccompsandbox_stat_cpp(it);
  PUBLIC_seccompsandbox_syscall_entrypoint_cpp(it);
  PUBLIC_seccompsandbox_syscall_table_cpp(it);
  PUBLIC_seccompsandbox_trusted_process_cpp(it);
  PUBLIC_seccompsandbox_trusted_thread_cpp(it);
  PUBLIC_seccompsandbox_x86_decode_cpp(it);
}

#endif

