/* CONSBENCH file begin */
#ifndef MAIN_LIBGTEST_CPP
#define MAIN_LIBGTEST_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, testing_gtest_src_main_libgtest_cpp, "testing/gtest/src/main_libgtest.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(testing_gtest_src_main_libgtest_cpp, "testing/gtest/src/main_libgtest.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_testing_gtest_src_gtest_death_test_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_testing_gtest_src_gtest_filepath_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_testing_gtest_src_gtest_port_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_testing_gtest_src_gtest_printers_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_testing_gtest_src_gtest_test_part_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_testing_gtest_src_gtest_typed_test_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_testing_gtest_src_gtest_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_testing_multiprocess_func_list_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_testing_gtest_src_main_libgtest_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_testing_gtest_src_gtest_death_test_cpp(it);
  PUBLIC_testing_gtest_src_gtest_filepath_cpp(it);
  PUBLIC_testing_gtest_src_gtest_port_cpp(it);
  PUBLIC_testing_gtest_src_gtest_printers_cpp(it);
  PUBLIC_testing_gtest_src_gtest_test_part_cpp(it);
  PUBLIC_testing_gtest_src_gtest_typed_test_cpp(it);
  PUBLIC_testing_gtest_src_gtest_cpp(it);
  PUBLIC_testing_multiprocess_func_list_cpp(it);
}

#endif

