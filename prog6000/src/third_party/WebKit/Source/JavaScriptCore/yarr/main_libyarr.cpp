/* CONSBENCH file begin */
#ifndef MAIN_LIBYARR_CPP
#define MAIN_LIBYARR_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_WebKit_Source_JavaScriptCore_yarr_main_libyarr_cpp, "third_party/WebKit/Source/JavaScriptCore/yarr/main_libyarr.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_WebKit_Source_JavaScriptCore_yarr_main_libyarr_cpp, "third_party/WebKit/Source/JavaScriptCore/yarr/main_libyarr.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_WebKit_Source_JavaScriptCore_yarr_YarrInterpreter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_JavaScriptCore_yarr_YarrPattern_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_JavaScriptCore_yarr_YarrSyntaxChecker_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_WebKit_Source_JavaScriptCore_yarr_main_libyarr_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_WebKit_Source_JavaScriptCore_yarr_YarrInterpreter_cpp(it);
  PUBLIC_third_party_WebKit_Source_JavaScriptCore_yarr_YarrPattern_cpp(it);
  PUBLIC_third_party_WebKit_Source_JavaScriptCore_yarr_YarrSyntaxChecker_cpp(it);
}

#endif

