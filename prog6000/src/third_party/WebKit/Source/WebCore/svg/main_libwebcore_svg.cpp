/* CONSBENCH file begin */
#ifndef MAIN_LIBWEBCORE_SVG_CPP
#define MAIN_LIBWEBCORE_SVG_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_WebKit_Source_WebCore_svg_main_libwebcore_svg_cpp, "third_party/WebKit/Source/WebCore/svg/main_libwebcore_svg.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_WebKit_Source_WebCore_svg_main_libwebcore_svg_cpp, "third_party/WebKit/Source/WebCore/svg/main_libwebcore_svg.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_style_SVGRenderStyle_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_style_SVGRenderStyleDefs_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGBlock_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGContainer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGForeignObject_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGGradientStop_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGHiddenContainer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGImage_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGInline_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGInlineText_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGModelObject_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGPath_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResource_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceClipper_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceContainer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceFilter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceFilterPrimitive_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceGradient_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceLinearGradient_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceMarker_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceMasker_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourcePattern_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceRadialGradient_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceSolidColor_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGRoot_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGShadowTreeRootContainer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGTSpan_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGText_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGTextPath_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGTransformableContainer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGViewportContainer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGImageBufferTools_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGInlineFlowBox_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGInlineTextBox_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGMarkerLayoutInfo_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGRenderSupport_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGRenderTreeAsText_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGResources_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGResourcesCache_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGResourcesCycleSolver_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGRootInlineBox_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGShadowTreeElements_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextChunk_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextChunkBuilder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextLayoutAttributes_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextLayoutAttributesBuilder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextLayoutEngine_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextLayoutEngineBaseline_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextLayoutEngineSpacing_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextMetrics_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextQuery_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_ColorDistance_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAltGlyphElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAngle_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAnimateColorElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAnimateElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAnimateMotionElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAnimateTransformElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAnimationElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGCircleElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGClipPathElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGColor_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGComponentTransferFunctionElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGCursorElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGDefsElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGDescElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGDocument_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGDocumentExtensions_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGElementInstance_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGElementInstanceList_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGEllipseElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGExternalResourcesRequired_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEBlendElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEColorMatrixElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEComponentTransferElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFECompositeElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEConvolveMatrixElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEDiffuseLightingElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEDisplacementMapElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEDistantLightElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEFloodElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEFuncAElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEFuncBElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEFuncGElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEFuncRElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEGaussianBlurElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEImageElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFELightElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEMergeElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEMergeNodeElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEMorphologyElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEOffsetElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEPointLightElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFESpecularLightingElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFESpotLightElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFETileElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFETurbulenceElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFilterElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFilterPrimitiveStandardAttributes_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFitToViewBox_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFont_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFontData_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFontElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFontFaceElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFontFaceFormatElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFontFaceNameElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFontFaceSrcElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFontFaceUriElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGForeignObjectElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGGElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGGlyphElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGGradientElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGHKernElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGImageElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGImageLoader_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGLangSpace_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGLength_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGLengthList_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGLineElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGLinearGradientElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGLocatable_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGMPathElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGMarkerElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGMaskElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGMetadataElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGMissingGlyphElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGNumberList_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPaint_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGParserUtilities_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathBlender_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathBuilder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathByteStreamBuilder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathByteStreamSource_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathParser_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathParserFactory_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathSegList_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathSegListBuilder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathSegListSource_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathStringBuilder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathStringSource_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathTraversalStateBuilder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPatternElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPointList_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPolyElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPolygonElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPolylineElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPreserveAspectRatio_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGRadialGradientElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGRectElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGSVGElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGScriptElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGSetElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGStopElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGStringList_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGStylable_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGStyleElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGStyledElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGStyledLocatableElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGStyledTransformableElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGSwitchElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGSymbolElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTRefElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTSpanElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTests_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTextContentElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTextElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTextPathElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTextPositioningElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTitleElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTransform_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTransformDistance_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTransformList_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTransformable_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGURIReference_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGUseElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGVKernElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGViewElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGViewSpec_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGZoomAndPan_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGZoomEvent_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_animation_SMILTime_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_animation_SMILTimeContainer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_animation_SVGSMILElement_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_graphics_SVGImage_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_graphics_filters_SVGFEImage_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_graphics_filters_SVGFilter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_graphics_filters_SVGFilterBuilder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_WebKit_Source_WebCore_svg_properties_SVGPathSegListPropertyTearOff_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_WebKit_Source_WebCore_svg_main_libwebcore_svg_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_style_SVGRenderStyle_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_style_SVGRenderStyleDefs_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGBlock_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGContainer_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGForeignObject_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGGradientStop_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGHiddenContainer_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGImage_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGInline_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGInlineText_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGModelObject_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGPath_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResource_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceClipper_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceContainer_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceFilter_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceFilterPrimitive_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceGradient_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceLinearGradient_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceMarker_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceMasker_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourcePattern_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceRadialGradient_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGResourceSolidColor_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGRoot_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGShadowTreeRootContainer_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGTSpan_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGText_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGTextPath_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGTransformableContainer_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_RenderSVGViewportContainer_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGImageBufferTools_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGInlineFlowBox_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGInlineTextBox_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGMarkerLayoutInfo_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGRenderSupport_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGRenderTreeAsText_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGResources_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGResourcesCache_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGResourcesCycleSolver_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGRootInlineBox_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGShadowTreeElements_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextChunk_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextChunkBuilder_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextLayoutAttributes_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextLayoutAttributesBuilder_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextLayoutEngine_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextLayoutEngineBaseline_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextLayoutEngineSpacing_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextMetrics_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_rendering_svg_SVGTextQuery_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_ColorDistance_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAltGlyphElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAngle_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAnimateColorElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAnimateElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAnimateMotionElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAnimateTransformElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGAnimationElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGCircleElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGClipPathElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGColor_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGComponentTransferFunctionElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGCursorElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGDefsElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGDescElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGDocument_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGDocumentExtensions_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGElementInstance_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGElementInstanceList_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGEllipseElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGExternalResourcesRequired_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEBlendElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEColorMatrixElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEComponentTransferElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFECompositeElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEConvolveMatrixElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEDiffuseLightingElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEDisplacementMapElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEDistantLightElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEFloodElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEFuncAElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEFuncBElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEFuncGElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEFuncRElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEGaussianBlurElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEImageElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFELightElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEMergeElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEMergeNodeElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEMorphologyElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEOffsetElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFEPointLightElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFESpecularLightingElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFESpotLightElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFETileElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFETurbulenceElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFilterElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFilterPrimitiveStandardAttributes_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFitToViewBox_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFont_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFontData_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFontElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFontFaceElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFontFaceFormatElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFontFaceNameElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFontFaceSrcElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGFontFaceUriElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGForeignObjectElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGGElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGGlyphElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGGradientElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGHKernElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGImageElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGImageLoader_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGLangSpace_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGLength_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGLengthList_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGLineElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGLinearGradientElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGLocatable_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGMPathElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGMarkerElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGMaskElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGMetadataElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGMissingGlyphElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGNumberList_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPaint_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGParserUtilities_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathBlender_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathBuilder_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathByteStreamBuilder_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathByteStreamSource_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathParser_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathParserFactory_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathSegList_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathSegListBuilder_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathSegListSource_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathStringBuilder_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathStringSource_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPathTraversalStateBuilder_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPatternElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPointList_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPolyElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPolygonElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPolylineElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGPreserveAspectRatio_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGRadialGradientElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGRectElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGSVGElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGScriptElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGSetElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGStopElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGStringList_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGStylable_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGStyleElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGStyledElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGStyledLocatableElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGStyledTransformableElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGSwitchElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGSymbolElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTRefElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTSpanElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTests_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTextContentElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTextElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTextPathElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTextPositioningElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTitleElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTransform_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTransformDistance_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTransformList_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGTransformable_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGURIReference_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGUseElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGVKernElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGViewElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGViewSpec_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGZoomAndPan_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_SVGZoomEvent_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_animation_SMILTime_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_animation_SMILTimeContainer_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_animation_SVGSMILElement_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_graphics_SVGImage_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_graphics_filters_SVGFEImage_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_graphics_filters_SVGFilter_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_graphics_filters_SVGFilterBuilder_cpp(it);
  PUBLIC_third_party_WebKit_Source_WebCore_svg_properties_SVGPathSegListPropertyTearOff_cpp(it);
}

#endif

