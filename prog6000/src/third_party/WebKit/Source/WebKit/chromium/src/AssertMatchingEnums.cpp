/* CONSBENCH file begin */
#ifndef ASSERTMATCHINGENUMS_CPP_2018
#define ASSERTMATCHINGENUMS_CPP_2018

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_WebKit_Source_WebKit_chromium_src_AssertMatchingEnums_cpp, "third_party/WebKit/Source/WebKit/chromium/src/AssertMatchingEnums.cpp");

/* CONSBENCH includes begin */
#include "config.h"
#include "AccessibilityObject.h"
#include "ApplicationCacheHost.h"
#include "AsyncFileSystem.h"
#include "DocumentMarker.h"
#include "EditorInsertAction.h"
#include "FileError.h"
#include "FileMetadata.h"
#include "FontDescription.h"
#include "FontSmoothingMode.h"
#include "GeolocationError.h"
#include "GeolocationPosition.h"
#include "HTMLInputElement.h"
#include "IDBKey.h"
#include "MediaPlayer.h"
#include "NotificationPresenter.h"
#include "PasteboardPrivate.h"
#include "PlatformCursor.h"
#include "Settings.h"
#include "TextAffinity.h"
#include "UserContentTypes.h"
#include "UserScriptTypes.h"
#include "UserStyleSheetTypes.h"
#include "VideoFrameChromium.h"
#include "WebAccessibilityObject.h"
#include "WebApplicationCacheHost.h"
#include "WebClipboard.h"
#include "WebCursorInfo.h"
#include "WebEditingAction.h"
#include "WebFileError.h"
#include "WebFileInfo.h"
#include "WebFileSystem.h"
#include "WebFontDescription.h"
#include "WebGeolocationError.h"
#include "WebGeolocationPosition.h"
#include "WebIDBKey.h"
#include "WebInputElement.h"
#include "WebMediaPlayer.h"
#include "WebNotificationPresenter.h"
#include "WebScrollbar.h"
#include "WebSettings.h"
#include "WebTextAffinity.h"
#include "WebTextCaseSensitivity.h"
#include "WebTextCheckingResult.h"
#include "WebVideoFrame.h"
#include "WebView.h"
#include <wtf/Assertions.h>
#include <wtf/text/StringImpl.h>
#include "PlatformBridge.h"
#include "mac/WebThemeEngine.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_WebKit_Source_WebKit_chromium_src_AssertMatchingEnums_cpp, "third_party/WebKit/Source/WebKit/chromium/src/AssertMatchingEnums.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(third_party_WebKit_Source_WebKit_chromium_src_AssertMatchingEnums_cpp);

/*-no- ... */
/*-no- ...................................................... */
/*-no- .. */
/*-no- ..................................................................... */
/*-no- ......................................................................... */
/*-no- ....... */
/*-no- .. */
/*-no- ....................................................................... */
/*-no- ................................................................ */
/*-no- ................................................................ */
/*-no- ......................................................................... */
/*-no- ................................................................ */
/*-no- ................ */
/*-no- ............................................................. */
/*-no- ....................................................................... */
/*-no- ........................................................... */
/*-no- .. */
/*-no- ...................................................................... */
/*-no- .................................................................... */
/*-no- ........................................................................ */
/*-no- ....................................................................... */
/*-no- ........................................................................ */
/*-no- ................................................................... */
/*-no- ........................................................................ */
/*-no- ........................................................................ */
/*-no- ...................................................................... */
/*-no- ........................................................................ */
/*-no- ....................................................................... */
/*-no- .... */
/*-no-  */
/*-no- ....................................................................... */
/*-no- ........................................ */
/*-no-  */
/*-no- #include "config.h" */
/*-no-  */
/*-no- #include "AccessibilityObject.h" */
/*-no- #include "ApplicationCacheHost.h" */
/*-no- #include "AsyncFileSystem.h" */
/*-no- #include "DocumentMarker.h" */
/*-no- #include "EditorInsertAction.h" */
/*-no- #include "FileError.h" */
/*-no- #include "FileMetadata.h" */
/*-no- #include "FontDescription.h" */
/*-no- #include "FontSmoothingMode.h" */
/*-no- #include "GeolocationError.h" */
/*-no- #include "GeolocationPosition.h" */
/*-no- #include "HTMLInputElement.h" */
/*-no- #include "IDBKey.h" */
/*-no- #include "MediaPlayer.h" */
/*-no- #include "NotificationPresenter.h" */
/*-no- #include "PasteboardPrivate.h" */
/*-no- #include "PlatformCursor.h" */
/*-no- #include "Settings.h" */
/*-no- #include "TextAffinity.h" */
/*-no- #include "UserContentTypes.h" */
/*-no- #include "UserScriptTypes.h" */
/*-no- #include "UserStyleSheetTypes.h" */
/*-no- #include "VideoFrameChromium.h" */
/*-no- #include "WebAccessibilityObject.h" */
/*-no- #include "WebApplicationCacheHost.h" */
/*-no- #include "WebClipboard.h" */
/*-no- #include "WebCursorInfo.h" */
/*-no- #include "WebEditingAction.h" */
/*-no- #include "WebFileError.h" */
/*-no- #include "WebFileInfo.h" */
/*-no- #include "WebFileSystem.h" */
/*-no- #include "WebFontDescription.h" */
/*-no- #include "WebGeolocationError.h" */
/*-no- #include "WebGeolocationPosition.h" */
/*-no- #include "WebIDBKey.h" */
/*-no- #include "WebInputElement.h" */
/*-no- #include "WebMediaPlayer.h" */
/*-no- #include "WebNotificationPresenter.h" */
/*-no- #include "WebScrollbar.h" */
/*-no- #include "WebSettings.h" */
/*-no- #include "WebTextAffinity.h" */
/*-no- #include "WebTextCaseSensitivity.h" */
/*-no- #include "WebTextCheckingResult.h" */
/*-no- #include "WebVideoFrame.h" */
/*-no- #include "WebView.h" */
/*-no- #include <wtf/Assertions.h> */
/*-no- #include <wtf/text/StringImpl.h> */
/*-no-  */
/*-no- #if OS(DARWIN) */
/*-no- #include "PlatformBridge.h" */
/*-no- #include "mac/WebThemeEngine.h" */
/*-no- #endif */
/*-no-  */
/*-no- #define COMPILE_ASSERT_MATCHING_ENUM(webkit_name, webcore_name) \ */
/*-no- ............................................................................................. */
/*-no-  */
/*-no- .......................................................................................... */
/*-no- ................... */
/*-no- ................................. */
/*-no- ................................... */
/*-no- .. */
/*-no-  */
/*-no- ....................................................................... */
/*-no- ..................................................................... */
/*-no- ............................................................................... */
/*-no- ......................................................................... */
/*-no- ..................................................................... */
/*-no- ......................................................................... */
/*-no- ........................................................................... */
/*-no- ............................................................................. */
/*-no- ......................................................................... */
/*-no- ............................................................................. */
/*-no- ............................................................................... */
/*-no- ............................................................................. */
/*-no- ................................................................... */
/*-no- ............................................................................... */
/*-no- ................................................................... */
/*-no- ............................................................................. */
/*-no- ................................................................. */
/*-no- ........................................................................... */
/*-no- ..................................................................................... */
/*-no- ................................................................... */
/*-no- ....................................................................... */
/*-no- ................................................................. */
/*-no- ......................................................................... */
/*-no- ..................................................................... */
/*-no- ............................................................... */
/*-no- ....................................................................... */
/*-no- ................................................................................... */
/*-no- ........................................................................................... */
/*-no- ..................................................................... */
/*-no- ..................................................................... */
/*-no- ............................................................................. */
/*-no- ....................................................................... */
/*-no- ............................................................................... */
/*-no- ....................................................................... */
/*-no- ......................................................................... */
/*-no- ............................................................................. */
/*-no- ......................................................................... */
/*-no- ........................................................................... */
/*-no- ......................................................................... */
/*-no- ................................................................... */
/*-no- ....................................................................... */
/*-no- ................................................................... */
/*-no- ................................................................... */
/*-no- ............................................................................... */
/*-no- ................................................................. */
/*-no- ............................................................................................. */
/*-no- ................................................................. */
/*-no- ................................................................. */
/*-no- ................................................................................. */
/*-no- ........................................................................... */
/*-no- ............................................................................... */
/*-no- ................................................................................. */
/*-no- ......................................................................... */
/*-no- ............................................................................. */
/*-no- ....................................................................... */
/*-no- ....................................................................... */
/*-no- ....................................................................... */
/*-no- ................................................................................... */
/*-no- ..................................................................................... */
/*-no- ................................................................................... */
/*-no- ................................................................................................. */
/*-no- ............................................................................................. */
/*-no- ......................................................................................................... */
/*-no- ............................................................................. */
/*-no- ............................................................................... */
/*-no- ....................................................................... */
/*-no- ..................................................................................... */
/*-no- ............................................................... */
/*-no- ....................................................................... */
/*-no- ......................................................................... */
/*-no- ..................................................................... */
/*-no- ......................................................................... */
/*-no- ............................................................................. */
/*-no- ........................................................................... */
/*-no- ................................................................................. */
/*-no- ......................................................................... */
/*-no- ............................................................................................... */
/*-no- ..................................................................................... */
/*-no- ................................................................................................... */
/*-no- ............................................................................................... */
/*-no- ................................................................................. */
/*-no- ............................................................................................. */
/*-no- ..................................................................................... */
/*-no- ......................................................................................... */
/*-no- ..................................................................................................... */
/*-no- ........................................................................................... */
/*-no- ..................................................................................... */
/*-no- ............................................................................................. */
/*-no- ........................................................................................... */
/*-no- ......................................................................................... */
/*-no- ......................................................................... */
/*-no- ....................................................................................... */
/*-no- ................................................................................. */
/*-no- ................................................................................. */
/*-no- ..................................................................................... */
/*-no- ................................................................................................. */
/*-no-  */
/*-no- #if ENABLE(OFFLINE_WEB_APPLICATIONS) */
/*-no- ................................................................................................ */
/*-no- ........................................................................................ */
/*-no- ................................................................................................ */
/*-no- ...................................................................................................... */
/*-no- ...................................................................................................... */
/*-no- ................................................................................................ */
/*-no- ........................................................................................................... */
/*-no- ..................................................................................................... */
/*-no- ........................................................................................................... */
/*-no- ................................................................................................................. */
/*-no- ........................................................................................................... */
/*-no- ................................................................................................................. */
/*-no- ....................................................................................................... */
/*-no- ........................................................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- ...................................................................................... */
/*-no- .............................................................................................. */
/*-no- ..................................................................................................... */
/*-no-  */
/*-no- .............................................................................................. */
/*-no- ................................................................................................ */
/*-no- ...................................................................................... */
/*-no-  */
/*-no- ...................................................................................... */
/*-no- .................................................................................. */
/*-no- ................................................................................ */
/*-no- .................................................................................. */
/*-no- ................................................................................ */
/*-no- ................................................................................ */
/*-no- ............................................................................................ */
/*-no- .............................................................................................. */
/*-no- ...................................................................................................... */
/*-no- ...................................................................................................... */
/*-no- .............................................................................................. */
/*-no- ...................................................................................................... */
/*-no- ...................................................................................................... */
/*-no- ............................................................................................ */
/*-no- ........................................................................................................ */
/*-no- .................................................................................................... */
/*-no- ........................................................................................................................ */
/*-no- ........................................................................................................................ */
/*-no- ................................................................................................ */
/*-no- .......................................................................................... */
/*-no- .................................................................................................. */
/*-no- .............................................................................................. */
/*-no- ................................................................................................ */
/*-no- ........................................................................................................ */
/*-no- ........................................................................................................ */
/*-no- ................................................................................................ */
/*-no- ........................................................................................................ */
/*-no- ........................................................................................................ */
/*-no- .............................................................................................. */
/*-no- ................................................................................ */
/*-no- ................................................................................................ */
/*-no- ................................................................................ */
/*-no- .............................................................................................. */
/*-no- .................................................................................. */
/*-no- ........................................................................................ */
/*-no- .................................................................................... */
/*-no- ................................................................................ */
/*-no- ................................................................................ */
/*-no- ............................................................................................ */
/*-no- .................................................................................... */
/*-no- ...................................................................................... */
/*-no- ................................................................................ */
/*-no- ........................................................................................ */
/*-no- .................................................................................... */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ............................................................................... */
/*-no- ................................................................................. */
/*-no-  */
/*-no- ............................................................................................... */
/*-no- ......................................................................................................... */
/*-no- ................................................................................................... */
/*-no- ........................................................................................................... */
/*-no- ........................................................................................................... */
/*-no- ....................................................................................................... */
/*-no- ....................................................................................................... */
/*-no-  */
/*-no- ............................................................................... */
/*-no- ............................................................................. */
/*-no- .................................................................................. */
/*-no- ......................................................................................... */
/*-no-  */
/*-no- ........................................................................... */
/*-no- ........................................................................... */
/*-no- ........................................................................... */
/*-no- ........................................................................... */
/*-no- ........................................................................... */
/*-no- ........................................................................... */
/*-no- ........................................................................... */
/*-no- ........................................................................... */
/*-no- ........................................................................... */
/*-no- ................................................................................. */
/*-no- ............................................................................. */
/*-no-  */
/*-no- ....................................................................... */
/*-no- ........................................................................... */
/*-no- ................................................................. */
/*-no- .................................................................................. */
/*-no- ........................................................................................ */
/*-no- ..................................................................... */
/*-no- ..................................................................................................... */
/*-no- ....................................................................... */
/*-no- ......................................................................... */
/*-no- .................................................................................. */
/*-no- .......................................................................................... */
/*-no- ......................................................................... */
/*-no- ...................................................................................... */
/*-no-  */
/*-no- #if ENABLE(VIDEO) */
/*-no- ........................................................................ */
/*-no- ...................................................................... */
/*-no- ............................................................................ */
/*-no- .......................................................................... */
/*-no- .................................................................................... */
/*-no- ...................................................................................... */
/*-no- .................................................................................... */
/*-no-  */
/*-no- .................................................................................... */
/*-no- ...................................................................................... */
/*-no- ............................................................................................ */
/*-no- .......................................................................................... */
/*-no- .......................................................................................... */
/*-no-  */
/*-no- ............................................................................ */
/*-no- .............................................................................. */
/*-no- ...................................................................................... */
/*-no- .................................................................................. */
/*-no-  */
/*-no- ........................................................................................ */
/*-no- ...................................................................................... */
/*-no- ...................................................................................... */
/*-no- .................................................................................... */
/*-no- .................................................................................... */
/*-no- .................................................................................. */
/*-no- .................................................................................. */
/*-no- .................................................................................. */
/*-no- .................................................................................. */
/*-no- .................................................................................... */
/*-no- .................................................................................... */
/*-no-  */
/*-no- ........................................................................................................... */
/*-no- ................................................................................................. */
/*-no- #endif */
/*-no-  */
/*-no- #if ENABLE(NOTIFICATIONS) */
/*-no- .................................................................................................................... */
/*-no- .......................................................................................................................... */
/*-no- .................................................................................................................. */
/*-no- #endif */
/*-no-  */
/*-no- ............................................................................ */
/*-no- ........................................................................ */
/*-no-  */
/*-no- ....................................................................... */
/*-no- ....................................................................... */
/*-no- ............................................................................... */
/*-no- ......................................................................... */
/*-no-  */
/*-no- .................................................................................. */
/*-no- ...................................................................................... */
/*-no- .................................................................................... */
/*-no-  */
/*-no- ................................................................ */
/*-no- .................................................................... */
/*-no-  */
/*-no- ...................................................................... */
/*-no- .......................................................................... */
/*-no-  */
/*-no- .............................................................................................. */
/*-no- .......................................................................................... */
/*-no- ....................................................................................... */
/*-no- ............................................................................................. */
/*-no- ..................................................................................................... */
/*-no- ......................................................................................................... */
/*-no-  */
/*-no- .................................................................... */
/*-no- ........................................................................ */
/*-no- .................................................................... */
/*-no- ........................................................................ */
/*-no-  */
/*-no- #if ENABLE(FILE_SYSTEM) */
/*-no- ....................................................................................... */
/*-no- ......................................................................................... */
/*-no- .................................................................................. */
/*-no- ............................................................................ */
/*-no- ...................................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ............................................................................ */
/*-no- ...................................................................... */
/*-no- ................................................................................... */
/*-no- ............................................................................ */
/*-no- ........................................................................................................ */
/*-no- ..................................................................................... */
/*-no- ........................................................................ */
/*-no- ................................................................................................... */
/*-no- ....................................................................................... */
/*-no- ..................................................................................... */
/*-no- ................................................................................. */
/*-no-  */
/*-no- ............................................................................................................. */
/*-no- ................................................................................................................... */
/*-no-  */
/*-no- ............................................................................................. */
/*-no- ........................................................................................... */
/*-no-  */
/*-no- #if OS(DARWIN) */
/*-no- ........................................................................................... */
/*-no- ........................................................................................... */
/*-no- ....................................................................................... */
/*-no- ......................................................................................... */
/*-no-  */
/*-no- ....................................................................................... */
/*-no- ................................................................................... */
/*-no-  */
/*-no- ............................................................................................................................. */
/*-no- ......................................................................................................................... */
/*-no-  */
/*-no- ................................................................................................................... */
/*-no- ..................................................................................................................... */
/*-no- #endif */

#endif
/* CONSBENCH file end */
