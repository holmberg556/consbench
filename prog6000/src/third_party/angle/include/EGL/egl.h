/* CONSBENCH file begin */
#ifndef EGL_H_2278
#define EGL_H_2278

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_angle_include_EGL_egl_h, "third_party/angle/include/EGL/egl.h");

/* CONSBENCH includes begin */
#include <EGL/eglplatform.h>
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_angle_include_EGL_egl_h, "third_party/angle/include/EGL/egl.h");

/*-no- ...................................... */
/*-no- .......................... */
/*-no- ........................................... */
/*-no- ............................................................................. */
/*-no- .... */
/*-no-  */
/*-no- ... */
/*-no- ................................................. */
/*-no- .. */
/*-no- .......................................................................... */
/*-no- ................................................................... */
/*-no- ........................................................................ */
/*-no- ...................................................................... */
/*-no- ...................................................................... */
/*-no- .......................................................................... */
/*-no- ............................ */
/*-no- .. */
/*-no- .......................................................................... */
/*-no- .......................................................... */
/*-no- .. */
/*-no- .................................................................... */
/*-no- ..................................................................... */
/*-no- ......................................................................... */
/*-no- ....................................................................... */
/*-no- ....................................................................... */
/*-no- .................................................................... */
/*-no- ........................................................... */
/*-no- ... */
/*-no-  */
/*-no- #ifndef __egl_h_ */
/*-no- #define __egl_h_ */
/*-no-  */
/*-no- ...................................................................... */
/*-no- ............................................... */
/*-no- .... */
/*-no- #include <EGL/eglplatform.h> */
/*-no-  */
/*-no- #ifdef __cplusplus */
/*-no- ............ */
/*-no- #endif */
/*-no-  */
/*-no- ................. */
/*-no- .......................................... */
/*-no- ................................ */
/*-no- ............................. */
/*-no- ........................ */
/*-no- ......................... */
/*-no- ......................... */
/*-no- ......................... */
/*-no- .............................. */
/*-no-  */
/*-no- ...................... */
/*-no- #define EGL_VERSION_1_0			1 */
/*-no- #define EGL_VERSION_1_1			1 */
/*-no- #define EGL_VERSION_1_2			1 */
/*-no- #define EGL_VERSION_1_3			1 */
/*-no- #define EGL_VERSION_1_4			1 */
/*-no-  */
/*-no- .................................................................... */
/*-no- ....................................................... */
/*-no- .... */
/*-no-  */
/*-no- ................... */
/*-no- #define EGL_FALSE			0 */
/*-no- #define EGL_TRUE			1 */
/*-no-  */
/*-no- ................................. */
/*-no- #define EGL_DEFAULT_DISPLAY		((EGLNativeDisplayType)0) */
/*-no- #define EGL_NO_CONTEXT			((EGLContext)0) */
/*-no- #define EGL_NO_DISPLAY			((EGLDisplay)0) */
/*-no- #define EGL_NO_SURFACE			((EGLSurface)0) */
/*-no-  */
/*-no- ................................... */
/*-no- #define EGL_DONT_CARE			((EGLint)-1) */
/*-no-  */
/*-no- ....................................... */
/*-no- #define EGL_SUCCESS			0x3000 */
/*-no- #define EGL_NOT_INITIALIZED		0x3001 */
/*-no- #define EGL_BAD_ACCESS			0x3002 */
/*-no- #define EGL_BAD_ALLOC			0x3003 */
/*-no- #define EGL_BAD_ATTRIBUTE		0x3004 */
/*-no- #define EGL_BAD_CONFIG			0x3005 */
/*-no- #define EGL_BAD_CONTEXT			0x3006 */
/*-no- #define EGL_BAD_CURRENT_SURFACE		0x3007 */
/*-no- #define EGL_BAD_DISPLAY			0x3008 */
/*-no- #define EGL_BAD_MATCH			0x3009 */
/*-no- #define EGL_BAD_NATIVE_PIXMAP		0x300A */
/*-no- #define EGL_BAD_NATIVE_WINDOW		0x300B */
/*-no- #define EGL_BAD_PARAMETER		0x300C */
/*-no- #define EGL_BAD_SURFACE			0x300D */
/*-no- #define EGL_CONTEXT_LOST		0x300E	/-* EGL 1.1 - IMG_power_management *-/ */
/*-no-  */
/*-no- .................................................... */
/*-no-  */
/*-no- ......................... */
/*-no- #define EGL_BUFFER_SIZE			0x3020 */
/*-no- #define EGL_ALPHA_SIZE			0x3021 */
/*-no- #define EGL_BLUE_SIZE			0x3022 */
/*-no- #define EGL_GREEN_SIZE			0x3023 */
/*-no- #define EGL_RED_SIZE			0x3024 */
/*-no- #define EGL_DEPTH_SIZE			0x3025 */
/*-no- #define EGL_STENCIL_SIZE		0x3026 */
/*-no- #define EGL_CONFIG_CAVEAT		0x3027 */
/*-no- #define EGL_CONFIG_ID			0x3028 */
/*-no- #define EGL_LEVEL			0x3029 */
/*-no- #define EGL_MAX_PBUFFER_HEIGHT		0x302A */
/*-no- #define EGL_MAX_PBUFFER_PIXELS		0x302B */
/*-no- #define EGL_MAX_PBUFFER_WIDTH		0x302C */
/*-no- #define EGL_NATIVE_RENDERABLE		0x302D */
/*-no- #define EGL_NATIVE_VISUAL_ID		0x302E */
/*-no- #define EGL_NATIVE_VISUAL_TYPE		0x302F */
/*-no- #define EGL_SAMPLES			0x3031 */
/*-no- #define EGL_SAMPLE_BUFFERS		0x3032 */
/*-no- #define EGL_SURFACE_TYPE		0x3033 */
/*-no- #define EGL_TRANSPARENT_TYPE		0x3034 */
/*-no- #define EGL_TRANSPARENT_BLUE_VALUE	0x3035 */
/*-no- #define EGL_TRANSPARENT_GREEN_VALUE	0x3036 */
/*-no- #define EGL_TRANSPARENT_RED_VALUE	0x3037 */
/*-no- #define EGL_NONE			0x3038	/-* Attrib list terminator *-/ */
/*-no- #define EGL_BIND_TO_TEXTURE_RGB		0x3039 */
/*-no- #define EGL_BIND_TO_TEXTURE_RGBA	0x303A */
/*-no- #define EGL_MIN_SWAP_INTERVAL		0x303B */
/*-no- #define EGL_MAX_SWAP_INTERVAL		0x303C */
/*-no- #define EGL_LUMINANCE_SIZE		0x303D */
/*-no- #define EGL_ALPHA_MASK_SIZE		0x303E */
/*-no- #define EGL_COLOR_BUFFER_TYPE		0x303F */
/*-no- #define EGL_RENDERABLE_TYPE		0x3040 */
/*-no- #define EGL_MATCH_NATIVE_PIXMAP		0x3041	/-* Pseudo-attribute (not queryable) *-/ */
/*-no- #define EGL_CONFORMANT			0x3042 */
/*-no-  */
/*-no- ............................................................... */
/*-no-  */
/*-no- ............................... */
/*-no- #define EGL_SLOW_CONFIG			0x3050	/-* EGL_CONFIG_CAVEAT value *-/ */
/*-no- #define EGL_NON_CONFORMANT_CONFIG	0x3051	/-* EGL_CONFIG_CAVEAT value *-/ */
/*-no- #define EGL_TRANSPARENT_RGB		0x3052	/-* EGL_TRANSPARENT_TYPE value *-/ */
/*-no- #define EGL_RGB_BUFFER			0x308E	/-* EGL_COLOR_BUFFER_TYPE value *-/ */
/*-no- #define EGL_LUMINANCE_BUFFER		0x308F	/-* EGL_COLOR_BUFFER_TYPE value *-/ */
/*-no-  */
/*-no- ............................................................ */
/*-no- #define EGL_NO_TEXTURE			0x305C */
/*-no- #define EGL_TEXTURE_RGB			0x305D */
/*-no- #define EGL_TEXTURE_RGBA		0x305E */
/*-no- #define EGL_TEXTURE_2D			0x305F */
/*-no-  */
/*-no- .................................. */
/*-no- #define EGL_PBUFFER_BIT			0x0001	/-* EGL_SURFACE_TYPE mask bits *-/ */
/*-no- #define EGL_PIXMAP_BIT			0x0002	/-* EGL_SURFACE_TYPE mask bits *-/ */
/*-no- #define EGL_WINDOW_BIT			0x0004	/-* EGL_SURFACE_TYPE mask bits *-/ */
/*-no- #define EGL_VG_COLORSPACE_LINEAR_BIT	0x0020	/-* EGL_SURFACE_TYPE mask bits *-/ */
/*-no- #define EGL_VG_ALPHA_FORMAT_PRE_BIT	0x0040	/-* EGL_SURFACE_TYPE mask bits *-/ */
/*-no- #define EGL_MULTISAMPLE_RESOLVE_BOX_BIT 0x0200	/-* EGL_SURFACE_TYPE mask bits *-/ */
/*-no- #define EGL_SWAP_BEHAVIOR_PRESERVED_BIT 0x0400	/-* EGL_SURFACE_TYPE mask bits *-/ */
/*-no-  */
/*-no- #define EGL_OPENGL_ES_BIT		0x0001	/-* EGL_RENDERABLE_TYPE mask bits *-/ */
/*-no- #define EGL_OPENVG_BIT			0x0002	/-* EGL_RENDERABLE_TYPE mask bits *-/ */
/*-no- #define EGL_OPENGL_ES2_BIT		0x0004	/-* EGL_RENDERABLE_TYPE mask bits *-/ */
/*-no- #define EGL_OPENGL_BIT			0x0008	/-* EGL_RENDERABLE_TYPE mask bits *-/ */
/*-no-  */
/*-no- ........................... */
/*-no- #define EGL_VENDOR			0x3053 */
/*-no- #define EGL_VERSION			0x3054 */
/*-no- #define EGL_EXTENSIONS			0x3055 */
/*-no- #define EGL_CLIENT_APIS			0x308D */
/*-no-  */
/*-no- ................................................................... */
/*-no- #define EGL_HEIGHT			0x3056 */
/*-no- #define EGL_WIDTH			0x3057 */
/*-no- #define EGL_LARGEST_PBUFFER		0x3058 */
/*-no- #define EGL_TEXTURE_FORMAT		0x3080 */
/*-no- #define EGL_TEXTURE_TARGET		0x3081 */
/*-no- #define EGL_MIPMAP_TEXTURE		0x3082 */
/*-no- #define EGL_MIPMAP_LEVEL		0x3083 */
/*-no- #define EGL_RENDER_BUFFER		0x3086 */
/*-no- #define EGL_VG_COLORSPACE		0x3087 */
/*-no- #define EGL_VG_ALPHA_FORMAT		0x3088 */
/*-no- #define EGL_HORIZONTAL_RESOLUTION	0x3090 */
/*-no- #define EGL_VERTICAL_RESOLUTION		0x3091 */
/*-no- #define EGL_PIXEL_ASPECT_RATIO		0x3092 */
/*-no- #define EGL_SWAP_BEHAVIOR		0x3093 */
/*-no- #define EGL_MULTISAMPLE_RESOLVE		0x3099 */
/*-no-  */
/*-no- ................................................................................ */
/*-no- #define EGL_BACK_BUFFER			0x3084 */
/*-no- #define EGL_SINGLE_BUFFER		0x3085 */
/*-no-  */
/*-no- ........................... */
/*-no- #define EGL_VG_COLORSPACE_sRGB		0x3089	/-* EGL_VG_COLORSPACE value *-/ */
/*-no- #define EGL_VG_COLORSPACE_LINEAR	0x308A	/-* EGL_VG_COLORSPACE value *-/ */
/*-no-  */
/*-no- ............................ */
/*-no- #define EGL_VG_ALPHA_FORMAT_NONPRE	0x308B	/-* EGL_ALPHA_FORMAT value *-/ */
/*-no- #define EGL_VG_ALPHA_FORMAT_PRE		0x308C	/-* EGL_ALPHA_FORMAT value *-/ */
/*-no-  */
/*-no- ................................................................... */
/*-no- .......................................................... */
/*-no- .... */
/*-no- #define EGL_DISPLAY_SCALING		10000 */
/*-no-  */
/*-no- ............................................... */
/*-no- #define EGL_UNKNOWN			((EGLint)-1) */
/*-no-  */
/*-no- .................................. */
/*-no- #define EGL_BUFFER_PRESERVED		0x3094	/-* EGL_SWAP_BEHAVIOR value *-/ */
/*-no- #define EGL_BUFFER_DESTROYED		0x3095	/-* EGL_SWAP_BEHAVIOR value *-/ */
/*-no-  */
/*-no- .................................................. */
/*-no- #define EGL_OPENVG_IMAGE		0x3096 */
/*-no-  */
/*-no- ............................ */
/*-no- #define EGL_CONTEXT_CLIENT_TYPE		0x3097 */
/*-no-  */
/*-no- ................................ */
/*-no- #define EGL_CONTEXT_CLIENT_VERSION	0x3098 */
/*-no-  */
/*-no- ........................................ */
/*-no- #define EGL_MULTISAMPLE_RESOLVE_DEFAULT 0x309A	/-* EGL_MULTISAMPLE_RESOLVE value *-/ */
/*-no- #define EGL_MULTISAMPLE_RESOLVE_BOX	0x309B	/-* EGL_MULTISAMPLE_RESOLVE value *-/ */
/*-no-  */
/*-no- ................................ */
/*-no- #define EGL_OPENGL_ES_API		0x30A0 */
/*-no- #define EGL_OPENVG_API			0x30A1 */
/*-no- #define EGL_OPENGL_API			0x30A2 */
/*-no-  */
/*-no- ................................. */
/*-no- #define EGL_DRAW			0x3059 */
/*-no- #define EGL_READ			0x305A */
/*-no-  */
/*-no- .......................... */
/*-no- #define EGL_CORE_NATIVE_ENGINE		0x305B */
/*-no-  */
/*-no- ......................................................... */
/*-no- #define EGL_COLORSPACE			EGL_VG_COLORSPACE */
/*-no- #define EGL_ALPHA_FORMAT		EGL_VG_ALPHA_FORMAT */
/*-no- #define EGL_COLORSPACE_sRGB		EGL_VG_COLORSPACE_sRGB */
/*-no- #define EGL_COLORSPACE_LINEAR		EGL_VG_COLORSPACE_LINEAR */
/*-no- #define EGL_ALPHA_FORMAT_NONPRE		EGL_VG_ALPHA_FORMAT_NONPRE */
/*-no- #define EGL_ALPHA_FORMAT_PRE		EGL_VG_ALPHA_FORMAT_PRE */
/*-no-  */
/*-no- ............................................................ */
/*-no- .............................................................. */
/*-no- ..................................................... */
/*-no- .... */
/*-no-  */
/*-no-  */
/*-no-  */
/*-no- ..................... */
/*-no-  */
/*-no- ............................................ */
/*-no-  */
/*-no- ............................................................................. */
/*-no- .......................................................................................... */
/*-no- ........................................................... */
/*-no-  */
/*-no- ............................................................................ */
/*-no-  */
/*-no- ............................................................................... */
/*-no- ............................................ */
/*-no- ........................................................................................ */
/*-no- ............................................. */
/*-no- .......................... */
/*-no- .................................................................................. */
/*-no- .......................................... */
/*-no-  */
/*-no- ...................................................................................... */
/*-no- .............................. */
/*-no- ................................. */
/*-no- ....................................................................................... */
/*-no- .................................. */
/*-no- ...................................................................................... */
/*-no- ................................. */
/*-no- ................................. */
/*-no- .................................................................................... */
/*-no- ................................................................................. */
/*-no- ....................................... */
/*-no-  */
/*-no- ...................................................... */
/*-no- ............................................. */
/*-no-  */
/*-no- .................................................. */
/*-no-  */
/*-no- ..................................................... */
/*-no-  */
/*-no- ............................................................... */
/*-no- ............................................................... */
/*-no- .................................................... */
/*-no-  */
/*-no- .................................................................................. */
/*-no- ....................................... */
/*-no- ................................................................................................. */
/*-no- .................................................................................................... */
/*-no-  */
/*-no-  */
/*-no- ............................................................................... */
/*-no-  */
/*-no-  */
/*-no- ................................................................................ */
/*-no- ................................ */
/*-no- .................................. */
/*-no- ................................................................................ */
/*-no- ............................................................................. */
/*-no- ...................................... */
/*-no-  */
/*-no- ......................................................... */
/*-no- .................................................................... */
/*-no- ......................................................... */
/*-no- ............................................................................. */
/*-no- ....................................... */
/*-no-  */
/*-no- .............................................. */
/*-no- ........................................................... */
/*-no- ................................................................................. */
/*-no- ................................................................................ */
/*-no- ................................. */
/*-no-  */
/*-no- ......................................................................... */
/*-no- .................................................................. */
/*-no- .... */
/*-no- ............................................................... */
/*-no-  */
/*-no- .......................................................................... */
/*-no- ........................................................... */
/*-no- ............................................... */
/*-no-  */
/*-no- #ifdef __cplusplus */
/*-no- . */
/*-no- #endif */
/*-no-  */
/*-no- #endif /-* __egl_h_ *-/ */

#endif
/* CONSBENCH file end */
