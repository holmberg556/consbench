/* CONSBENCH file begin */
#ifndef MAIN_LIBTRANSLATOR_GLSL_CPP
#define MAIN_LIBTRANSLATOR_GLSL_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_angle_src_compiler_main_libtranslator_glsl_cpp, "third_party/angle/src/compiler/main_libtranslator_glsl.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_angle_src_compiler_main_libtranslator_glsl_cpp, "third_party/angle/src/compiler/main_libtranslator_glsl.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_angle_src_compiler_CodeGenGLSL_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_angle_src_compiler_OutputGLSL_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_angle_src_compiler_TranslatorGLSL_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_angle_src_compiler_VersionGLSL_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_angle_src_compiler_main_libtranslator_glsl_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_angle_src_compiler_CodeGenGLSL_cpp(it);
  PUBLIC_third_party_angle_src_compiler_OutputGLSL_cpp(it);
  PUBLIC_third_party_angle_src_compiler_TranslatorGLSL_cpp(it);
  PUBLIC_third_party_angle_src_compiler_VersionGLSL_cpp(it);
}

#endif

