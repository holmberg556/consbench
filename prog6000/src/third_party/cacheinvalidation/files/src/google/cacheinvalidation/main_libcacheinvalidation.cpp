/* CONSBENCH file begin */
#ifndef MAIN_LIBCACHEINVALIDATION_CPP
#define MAIN_LIBCACHEINVALIDATION_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_cacheinvalidation_files_src_google_cacheinvalidation_main_libcacheinvalidation_cpp, "third_party/cacheinvalidation/files/src/google/cacheinvalidation/main_libcacheinvalidation.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_cacheinvalidation_files_src_google_cacheinvalidation_main_libcacheinvalidation_cpp, "third_party/cacheinvalidation/files/src/google/cacheinvalidation/main_libcacheinvalidation.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_out_Debug_obj_gen_protoc_out_google_cacheinvalidation_internal_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_protoc_out_google_cacheinvalidation_ticl_persistence_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_out_Debug_obj_gen_protoc_out_google_cacheinvalidation_types_pb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_invalidation_client_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_invalidation_client_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_network_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_persistence_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_persistence_utils_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_proto_converter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_registration_update_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_session_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_throttle_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_version_manager_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_main_libcacheinvalidation_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_out_Debug_obj_gen_protoc_out_google_cacheinvalidation_internal_pb_cpp(it);
  PUBLIC_out_Debug_obj_gen_protoc_out_google_cacheinvalidation_ticl_persistence_pb_cpp(it);
  PUBLIC_out_Debug_obj_gen_protoc_out_google_cacheinvalidation_types_pb_cpp(it);
  PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_invalidation_client_impl_cpp(it);
  PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_invalidation_client_cpp(it);
  PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_network_manager_cpp(it);
  PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_persistence_manager_cpp(it);
  PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_persistence_utils_cpp(it);
  PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_proto_converter_cpp(it);
  PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_registration_update_manager_cpp(it);
  PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_session_manager_cpp(it);
  PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_throttle_cpp(it);
  PUBLIC_third_party_cacheinvalidation_files_src_google_cacheinvalidation_version_manager_cpp(it);
}

#endif

