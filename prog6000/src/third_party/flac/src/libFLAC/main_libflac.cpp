/* CONSBENCH file begin */
#ifndef MAIN_LIBFLAC_CPP
#define MAIN_LIBFLAC_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_flac_src_libFLAC_main_libflac_cpp, "third_party/flac/src/libFLAC/main_libflac.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_flac_src_libFLAC_main_libflac_cpp, "third_party/flac/src/libFLAC/main_libflac.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_flac_src_libFLAC_alloc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_flac_src_libFLAC_bitmath_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_flac_src_libFLAC_bitreader_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_flac_src_libFLAC_bitwriter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_flac_src_libFLAC_cpu_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_flac_src_libFLAC_crc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_flac_src_libFLAC_fixed_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_flac_src_libFLAC_float_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_flac_src_libFLAC_format_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_flac_src_libFLAC_lpc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_flac_src_libFLAC_md5_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_flac_src_libFLAC_memory_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_flac_src_libFLAC_stream_decoder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_flac_src_libFLAC_stream_encoder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_flac_src_libFLAC_stream_encoder_framing_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_flac_src_libFLAC_window_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_flac_src_libFLAC_main_libflac_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_flac_src_libFLAC_alloc_cpp(it);
  PUBLIC_third_party_flac_src_libFLAC_bitmath_cpp(it);
  PUBLIC_third_party_flac_src_libFLAC_bitreader_cpp(it);
  PUBLIC_third_party_flac_src_libFLAC_bitwriter_cpp(it);
  PUBLIC_third_party_flac_src_libFLAC_cpu_cpp(it);
  PUBLIC_third_party_flac_src_libFLAC_crc_cpp(it);
  PUBLIC_third_party_flac_src_libFLAC_fixed_cpp(it);
  PUBLIC_third_party_flac_src_libFLAC_float_cpp(it);
  PUBLIC_third_party_flac_src_libFLAC_format_cpp(it);
  PUBLIC_third_party_flac_src_libFLAC_lpc_cpp(it);
  PUBLIC_third_party_flac_src_libFLAC_md5_cpp(it);
  PUBLIC_third_party_flac_src_libFLAC_memory_cpp(it);
  PUBLIC_third_party_flac_src_libFLAC_stream_decoder_cpp(it);
  PUBLIC_third_party_flac_src_libFLAC_stream_encoder_cpp(it);
  PUBLIC_third_party_flac_src_libFLAC_stream_encoder_framing_cpp(it);
  PUBLIC_third_party_flac_src_libFLAC_window_cpp(it);
}

#endif

