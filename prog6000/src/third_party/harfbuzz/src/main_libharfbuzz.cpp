/* CONSBENCH file begin */
#ifndef MAIN_LIBHARFBUZZ_CPP
#define MAIN_LIBHARFBUZZ_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_harfbuzz_src_main_libharfbuzz_cpp, "third_party/harfbuzz/src/main_libharfbuzz.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_harfbuzz_src_main_libharfbuzz_cpp, "third_party/harfbuzz/src/main_libharfbuzz.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_harfbuzz_contrib_harfbuzz_freetype_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_contrib_harfbuzz_unicode_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_contrib_harfbuzz_unicode_tables_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_buffer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_stream_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_dump_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_gdef_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_gpos_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_gsub_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_open_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_shaper_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_tibetan_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_khmer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_indic_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_hebrew_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_arabic_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_hangul_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_myanmar_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_harfbuzz_src_harfbuzz_thai_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_harfbuzz_src_main_libharfbuzz_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_harfbuzz_contrib_harfbuzz_freetype_cpp(it);
  PUBLIC_third_party_harfbuzz_contrib_harfbuzz_unicode_cpp(it);
  PUBLIC_third_party_harfbuzz_contrib_harfbuzz_unicode_tables_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_buffer_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_stream_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_dump_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_gdef_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_gpos_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_gsub_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_impl_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_open_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_shaper_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_tibetan_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_khmer_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_indic_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_hebrew_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_arabic_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_hangul_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_myanmar_cpp(it);
  PUBLIC_third_party_harfbuzz_src_harfbuzz_thai_cpp(it);
}

#endif

