/* CONSBENCH file begin */
#ifndef MAIN_LIBHUNSPELL_CPP
#define MAIN_LIBHUNSPELL_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_hunspell_src_hunspell_main_libhunspell_cpp, "third_party/hunspell/src/hunspell/main_libhunspell.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_hunspell_src_hunspell_main_libhunspell_cpp, "third_party/hunspell/src/hunspell/main_libhunspell.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_hunspell_google_bdict_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_hunspell_google_bdict_reader_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_hunspell_google_bdict_writer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_hunspell_src_hunspell_affentry_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_hunspell_src_hunspell_affixmgr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_hunspell_src_hunspell_csutil_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_hunspell_src_hunspell_dictmgr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_hunspell_src_hunspell_filemgr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_hunspell_src_hunspell_hashmgr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_hunspell_src_hunspell_hunspell_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_hunspell_src_hunspell_hunzip_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_hunspell_src_hunspell_phonet_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_hunspell_src_hunspell_replist_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_hunspell_src_hunspell_suggestmgr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_hunspell_src_parsers_textparser_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_hunspell_src_hunspell_main_libhunspell_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_hunspell_google_bdict_cpp(it);
  PUBLIC_third_party_hunspell_google_bdict_reader_cpp(it);
  PUBLIC_third_party_hunspell_google_bdict_writer_cpp(it);
  PUBLIC_third_party_hunspell_src_hunspell_affentry_cpp(it);
  PUBLIC_third_party_hunspell_src_hunspell_affixmgr_cpp(it);
  PUBLIC_third_party_hunspell_src_hunspell_csutil_cpp(it);
  PUBLIC_third_party_hunspell_src_hunspell_dictmgr_cpp(it);
  PUBLIC_third_party_hunspell_src_hunspell_filemgr_cpp(it);
  PUBLIC_third_party_hunspell_src_hunspell_hashmgr_cpp(it);
  PUBLIC_third_party_hunspell_src_hunspell_hunspell_cpp(it);
  PUBLIC_third_party_hunspell_src_hunspell_hunzip_cpp(it);
  PUBLIC_third_party_hunspell_src_hunspell_phonet_cpp(it);
  PUBLIC_third_party_hunspell_src_hunspell_replist_cpp(it);
  PUBLIC_third_party_hunspell_src_hunspell_suggestmgr_cpp(it);
  PUBLIC_third_party_hunspell_src_parsers_textparser_cpp(it);
}

#endif

