/* CONSBENCH file begin */
#ifndef MAIN_LIBICCJPEG_CPP
#define MAIN_LIBICCJPEG_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_iccjpeg_main_libiccjpeg_cpp, "third_party/iccjpeg/main_libiccjpeg.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_iccjpeg_main_libiccjpeg_cpp, "third_party/iccjpeg/main_libiccjpeg.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_iccjpeg_iccjpeg_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_iccjpeg_main_libiccjpeg_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_iccjpeg_iccjpeg_cpp(it);
}

#endif

