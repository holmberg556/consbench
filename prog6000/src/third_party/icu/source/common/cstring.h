/* CONSBENCH file begin */
#ifndef CSTRING_H_275
#define CSTRING_H_275

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_icu_source_common_cstring_h, "third_party/icu/source/common/cstring.h");

/* CONSBENCH includes begin */
#include "unicode/utypes.h"
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_icu_source_common_cstring_h, "third_party/icu/source/common/cstring.h");

/*-no- ... */
/*-no- .............................................................................. */
/*-no- . */
/*-no- ............................................................ */
/*-no- ................................................. */
/*-no- . */
/*-no- .............................................................................. */
/*-no- . */
/*-no- ................ */
/*-no- . */
/*-no- ............................ */
/*-no- . */
/*-no- ........................... */
/*-no- . */
/*-no- ....................... */
/*-no- . */
/*-no- ....................................... */
/*-no- .................................... */
/*-no- ............................................................. */
/*-no- ....................................................................... */
/*-no- . */
/*-no- .............................................................................. */
/*-no- ... */
/*-no-  */
/*-no- #ifndef CSTRING_H */
/*-no- #define CSTRING_H 1 */
/*-no-  */
/*-no- #include "unicode/utypes.h" */
/*-no- #include <string.h> */
/*-no- #include <stdlib.h> */
/*-no- #include <ctype.h> */
/*-no-  */
/*-no- #define uprv_strcpy(dst, src) U_STANDARD_CPP_NAMESPACE  strcpy(dst, src) */
/*-no- #define uprv_strncpy(dst, src, size) U_STANDARD_CPP_NAMESPACE strncpy(dst, src, size) */
/*-no- #define uprv_strlen(str) U_STANDARD_CPP_NAMESPACE strlen(str) */
/*-no- #define uprv_strcmp(s1, s2) U_STANDARD_CPP_NAMESPACE strcmp(s1, s2) */
/*-no- #define uprv_strncmp(s1, s2, n) U_STANDARD_CPP_NAMESPACE strncmp(s1, s2, n) */
/*-no- #define uprv_strcat(dst, src) U_STANDARD_CPP_NAMESPACE strcat(dst, src) */
/*-no- #define uprv_strncat(dst, src, n) U_STANDARD_CPP_NAMESPACE strncat(dst, src, n) */
/*-no- #define uprv_strchr(s, c) U_STANDARD_CPP_NAMESPACE strchr(s, c) */
/*-no- #define uprv_strstr(s, c) U_STANDARD_CPP_NAMESPACE strstr(s, c) */
/*-no- #define uprv_strrchr(s, c) U_STANDARD_CPP_NAMESPACE strrchr(s, c) */
/*-no-  */
/*-no- ..................... */
/*-no- ..................... */
/*-no-  */
/*-no-  */
/*-no- ..................... */
/*-no- .......................... */
/*-no-  */
/*-no- ..................... */
/*-no- ........................... */
/*-no-  */
/*-no- #if U_CHARSET_FAMILY==U_ASCII_FAMILY */
/*-no- #   define uprv_tolower uprv_asciitolower */
/*-no- #elif U_CHARSET_FAMILY==U_EBCDIC_FAMILY */
/*-no- #   define uprv_tolower uprv_ebcdictolower */
/*-no- #else */
/*-no- #   error U_CHARSET_FAMILY is not valid */
/*-no- #endif */
/*-no-  */
/*-no- #define uprv_strtod(source, end) U_STANDARD_CPP_NAMESPACE strtod(source, end) */
/*-no- #define uprv_strtoul(str, end, base) U_STANDARD_CPP_NAMESPACE strtoul(str, end, base) */
/*-no- #define uprv_strtol(str, end, base) U_STANDARD_CPP_NAMESPACE strtol(str, end, base) */
/*-no- #ifdef U_WINDOWS */
/*-no- #   if defined(__BORLANDC__) */
/*-no- #       define uprv_stricmp(str1, str2) U_STANDARD_CPP_NAMESPACE stricmp(str1, str2) */
/*-no- #       define uprv_strnicmp(str1, str2, n) U_STANDARD_CPP_NAMESPACE strnicmp(str1, str2, n) */
/*-no- #   else */
/*-no- #       define uprv_stricmp(str1, str2) U_STANDARD_CPP_NAMESPACE _stricmp(str1, str2) */
/*-no- #       define uprv_strnicmp(str1, str2, n) U_STANDARD_CPP_NAMESPACE _strnicmp(str1, str2, n) */
/*-no- #   endif */
/*-no- #elif defined(POSIX)  */
/*-no- #   define uprv_stricmp(str1, str2) U_STANDARD_CPP_NAMESPACE strcasecmp(str1, str2)  */
/*-no- #   define uprv_strnicmp(str1, str2, n) U_STANDARD_CPP_NAMESPACE strncasecmp(str1, str2, n)  */
/*-no- #else */
/*-no- #   define uprv_stricmp(str1, str2) T_CString_stricmp(str1, str2) */
/*-no- #   define uprv_strnicmp(str1, str2, n) T_CString_strnicmp(str1, str2, n) */
/*-no- #endif */
/*-no-  */
/*-no- .......................................................................... */
/*-no- ...................................... */
/*-no- #define T_CString_itosOffset(a) ((a)<=9?('0'+(a)):('A'+(a)-10)) */
/*-no-  */
/*-no- ...................... */
/*-no- ............................. */
/*-no-  */
/*-no- .... */
/*-no- ........................................................................ */
/*-no- .......................................................................... */
/*-no- ............. */
/*-no- ........................................................... */
/*-no- ............................................................... */
/*-no- ............ */
/*-no- .... */
/*-no- ...................... */
/*-no- ......................................... */
/*-no-  */
/*-no- ...................... */
/*-no- ................................. */
/*-no-  */
/*-no- ...................... */
/*-no- ................................. */
/*-no-  */
/*-no- ........................ */
/*-no- .................................................................. */
/*-no-  */
/*-no- ........................ */
/*-no- ................................................................. */
/*-no-  */
/*-no- ........................ */
/*-no- .................................................................... */
/*-no-  */
/*-no- .................... */
/*-no- ...................................................... */
/*-no-  */
/*-no- .................... */
/*-no- ................................................................... */
/*-no-  */
/*-no- #endif /-* ! CSTRING_H *-/ */

#endif
/* CONSBENCH file end */
