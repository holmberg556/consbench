/* CONSBENCH file begin */
#ifndef MAIN_LIBICUUC_CPP
#define MAIN_LIBICUUC_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_icu_source_common_main_libicuuc_cpp, "third_party/icu/source/common/main_libicuuc.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_icu_source_common_main_libicuuc_cpp, "third_party/icu/source/common/main_libicuuc.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_icu_source_common_bmpset_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_brkeng_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_brkiter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_bytestream_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_caniter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_chariter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_charstr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_cmemory_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_cstring_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_cwchar_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_dictbe_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_dtintrv_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_errorcode_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_filterednormalizer2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_icudataver_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_icuplug_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_locavailable_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_locbased_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_locdispnames_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_locid_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_loclikely_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_locmap_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_locresdata_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_locutil_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_mutex_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_normalizer2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_normalizer2impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_normlzr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_parsepos_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_propname_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_propsvec_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_punycode_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_putil_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_rbbi_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_rbbidata_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_rbbinode_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_rbbirb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_rbbiscan_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_rbbisetb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_rbbistbl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_rbbitblb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_resbund_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_resbund_cnv_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ruleiter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_schriter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_serv_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_servlk_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_servlkf_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_servls_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_servnotf_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_servrbf_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_servslkf_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_stringpiece_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_triedict_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uarrsort_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ubidi_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ubidi_props_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ubidiln_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ubidiwrt_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ubrk_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucase_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucasemap_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucat_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uchar_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uchriter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucln_cmn_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucmndata_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnv_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnv2022_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnv_bld_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnv_cb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnv_cnv_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnv_err_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnv_ext_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnv_io_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnv_lmb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnv_set_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnv_u16_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnv_u32_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnv_u7_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnv_u8_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnvbocu_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnvdisp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnvhz_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnvisci_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnvlat1_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnvmbcs_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnvscsu_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucnvsel_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ucol_swp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_udata_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_udatamem_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_udataswp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uenum_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uhash_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uhash_us_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uidna_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uinit_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uinvchar_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uiter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ulist_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uloc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uloc_tag_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_umapfile_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_umath_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_umutex_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_unames_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_unifilt_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_unifunct_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uniset_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uniset_props_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_unisetspan_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_unistr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_unistr_case_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_unistr_cnv_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_unistr_props_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_unorm_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_unorm_it_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_unormcmp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uobject_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uprops_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ures_cnv_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uresbund_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uresdata_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_usc_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uscript_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uset_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uset_props_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_usetiter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ushape_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_usprep_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ustack_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ustr_cnv_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ustr_wcs_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ustrcase_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ustrenum_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ustrfmt_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ustring_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_ustrtrns_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_utext_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_utf_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_util_props_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_utrace_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_utrie_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_utrie2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_utrie2_builder_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uts46_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_utypes_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uvector_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uvectr32_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_uvectr64_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_icu_source_common_wintz_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_icu_source_common_main_libicuuc_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_icu_source_common_bmpset_cpp(it);
  PUBLIC_third_party_icu_source_common_brkeng_cpp(it);
  PUBLIC_third_party_icu_source_common_brkiter_cpp(it);
  PUBLIC_third_party_icu_source_common_bytestream_cpp(it);
  PUBLIC_third_party_icu_source_common_caniter_cpp(it);
  PUBLIC_third_party_icu_source_common_chariter_cpp(it);
  PUBLIC_third_party_icu_source_common_charstr_cpp(it);
  PUBLIC_third_party_icu_source_common_cmemory_cpp(it);
  PUBLIC_third_party_icu_source_common_cstring_cpp(it);
  PUBLIC_third_party_icu_source_common_cwchar_cpp(it);
  PUBLIC_third_party_icu_source_common_dictbe_cpp(it);
  PUBLIC_third_party_icu_source_common_dtintrv_cpp(it);
  PUBLIC_third_party_icu_source_common_errorcode_cpp(it);
  PUBLIC_third_party_icu_source_common_filterednormalizer2_cpp(it);
  PUBLIC_third_party_icu_source_common_icudataver_cpp(it);
  PUBLIC_third_party_icu_source_common_icuplug_cpp(it);
  PUBLIC_third_party_icu_source_common_locavailable_cpp(it);
  PUBLIC_third_party_icu_source_common_locbased_cpp(it);
  PUBLIC_third_party_icu_source_common_locdispnames_cpp(it);
  PUBLIC_third_party_icu_source_common_locid_cpp(it);
  PUBLIC_third_party_icu_source_common_loclikely_cpp(it);
  PUBLIC_third_party_icu_source_common_locmap_cpp(it);
  PUBLIC_third_party_icu_source_common_locresdata_cpp(it);
  PUBLIC_third_party_icu_source_common_locutil_cpp(it);
  PUBLIC_third_party_icu_source_common_mutex_cpp(it);
  PUBLIC_third_party_icu_source_common_normalizer2_cpp(it);
  PUBLIC_third_party_icu_source_common_normalizer2impl_cpp(it);
  PUBLIC_third_party_icu_source_common_normlzr_cpp(it);
  PUBLIC_third_party_icu_source_common_parsepos_cpp(it);
  PUBLIC_third_party_icu_source_common_propname_cpp(it);
  PUBLIC_third_party_icu_source_common_propsvec_cpp(it);
  PUBLIC_third_party_icu_source_common_punycode_cpp(it);
  PUBLIC_third_party_icu_source_common_putil_cpp(it);
  PUBLIC_third_party_icu_source_common_rbbi_cpp(it);
  PUBLIC_third_party_icu_source_common_rbbidata_cpp(it);
  PUBLIC_third_party_icu_source_common_rbbinode_cpp(it);
  PUBLIC_third_party_icu_source_common_rbbirb_cpp(it);
  PUBLIC_third_party_icu_source_common_rbbiscan_cpp(it);
  PUBLIC_third_party_icu_source_common_rbbisetb_cpp(it);
  PUBLIC_third_party_icu_source_common_rbbistbl_cpp(it);
  PUBLIC_third_party_icu_source_common_rbbitblb_cpp(it);
  PUBLIC_third_party_icu_source_common_resbund_cpp(it);
  PUBLIC_third_party_icu_source_common_resbund_cnv_cpp(it);
  PUBLIC_third_party_icu_source_common_ruleiter_cpp(it);
  PUBLIC_third_party_icu_source_common_schriter_cpp(it);
  PUBLIC_third_party_icu_source_common_serv_cpp(it);
  PUBLIC_third_party_icu_source_common_servlk_cpp(it);
  PUBLIC_third_party_icu_source_common_servlkf_cpp(it);
  PUBLIC_third_party_icu_source_common_servls_cpp(it);
  PUBLIC_third_party_icu_source_common_servnotf_cpp(it);
  PUBLIC_third_party_icu_source_common_servrbf_cpp(it);
  PUBLIC_third_party_icu_source_common_servslkf_cpp(it);
  PUBLIC_third_party_icu_source_common_stringpiece_cpp(it);
  PUBLIC_third_party_icu_source_common_triedict_cpp(it);
  PUBLIC_third_party_icu_source_common_uarrsort_cpp(it);
  PUBLIC_third_party_icu_source_common_ubidi_cpp(it);
  PUBLIC_third_party_icu_source_common_ubidi_props_cpp(it);
  PUBLIC_third_party_icu_source_common_ubidiln_cpp(it);
  PUBLIC_third_party_icu_source_common_ubidiwrt_cpp(it);
  PUBLIC_third_party_icu_source_common_ubrk_cpp(it);
  PUBLIC_third_party_icu_source_common_ucase_cpp(it);
  PUBLIC_third_party_icu_source_common_ucasemap_cpp(it);
  PUBLIC_third_party_icu_source_common_ucat_cpp(it);
  PUBLIC_third_party_icu_source_common_uchar_cpp(it);
  PUBLIC_third_party_icu_source_common_uchriter_cpp(it);
  PUBLIC_third_party_icu_source_common_ucln_cmn_cpp(it);
  PUBLIC_third_party_icu_source_common_ucmndata_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnv_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnv2022_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnv_bld_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnv_cb_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnv_cnv_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnv_err_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnv_ext_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnv_io_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnv_lmb_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnv_set_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnv_u16_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnv_u32_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnv_u7_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnv_u8_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnvbocu_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnvdisp_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnvhz_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnvisci_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnvlat1_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnvmbcs_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnvscsu_cpp(it);
  PUBLIC_third_party_icu_source_common_ucnvsel_cpp(it);
  PUBLIC_third_party_icu_source_common_ucol_swp_cpp(it);
  PUBLIC_third_party_icu_source_common_udata_cpp(it);
  PUBLIC_third_party_icu_source_common_udatamem_cpp(it);
  PUBLIC_third_party_icu_source_common_udataswp_cpp(it);
  PUBLIC_third_party_icu_source_common_uenum_cpp(it);
  PUBLIC_third_party_icu_source_common_uhash_cpp(it);
  PUBLIC_third_party_icu_source_common_uhash_us_cpp(it);
  PUBLIC_third_party_icu_source_common_uidna_cpp(it);
  PUBLIC_third_party_icu_source_common_uinit_cpp(it);
  PUBLIC_third_party_icu_source_common_uinvchar_cpp(it);
  PUBLIC_third_party_icu_source_common_uiter_cpp(it);
  PUBLIC_third_party_icu_source_common_ulist_cpp(it);
  PUBLIC_third_party_icu_source_common_uloc_cpp(it);
  PUBLIC_third_party_icu_source_common_uloc_tag_cpp(it);
  PUBLIC_third_party_icu_source_common_umapfile_cpp(it);
  PUBLIC_third_party_icu_source_common_umath_cpp(it);
  PUBLIC_third_party_icu_source_common_umutex_cpp(it);
  PUBLIC_third_party_icu_source_common_unames_cpp(it);
  PUBLIC_third_party_icu_source_common_unifilt_cpp(it);
  PUBLIC_third_party_icu_source_common_unifunct_cpp(it);
  PUBLIC_third_party_icu_source_common_uniset_cpp(it);
  PUBLIC_third_party_icu_source_common_uniset_props_cpp(it);
  PUBLIC_third_party_icu_source_common_unisetspan_cpp(it);
  PUBLIC_third_party_icu_source_common_unistr_cpp(it);
  PUBLIC_third_party_icu_source_common_unistr_case_cpp(it);
  PUBLIC_third_party_icu_source_common_unistr_cnv_cpp(it);
  PUBLIC_third_party_icu_source_common_unistr_props_cpp(it);
  PUBLIC_third_party_icu_source_common_unorm_cpp(it);
  PUBLIC_third_party_icu_source_common_unorm_it_cpp(it);
  PUBLIC_third_party_icu_source_common_unormcmp_cpp(it);
  PUBLIC_third_party_icu_source_common_uobject_cpp(it);
  PUBLIC_third_party_icu_source_common_uprops_cpp(it);
  PUBLIC_third_party_icu_source_common_ures_cnv_cpp(it);
  PUBLIC_third_party_icu_source_common_uresbund_cpp(it);
  PUBLIC_third_party_icu_source_common_uresdata_cpp(it);
  PUBLIC_third_party_icu_source_common_usc_impl_cpp(it);
  PUBLIC_third_party_icu_source_common_uscript_cpp(it);
  PUBLIC_third_party_icu_source_common_uset_cpp(it);
  PUBLIC_third_party_icu_source_common_uset_props_cpp(it);
  PUBLIC_third_party_icu_source_common_usetiter_cpp(it);
  PUBLIC_third_party_icu_source_common_ushape_cpp(it);
  PUBLIC_third_party_icu_source_common_usprep_cpp(it);
  PUBLIC_third_party_icu_source_common_ustack_cpp(it);
  PUBLIC_third_party_icu_source_common_ustr_cnv_cpp(it);
  PUBLIC_third_party_icu_source_common_ustr_wcs_cpp(it);
  PUBLIC_third_party_icu_source_common_ustrcase_cpp(it);
  PUBLIC_third_party_icu_source_common_ustrenum_cpp(it);
  PUBLIC_third_party_icu_source_common_ustrfmt_cpp(it);
  PUBLIC_third_party_icu_source_common_ustring_cpp(it);
  PUBLIC_third_party_icu_source_common_ustrtrns_cpp(it);
  PUBLIC_third_party_icu_source_common_utext_cpp(it);
  PUBLIC_third_party_icu_source_common_utf_impl_cpp(it);
  PUBLIC_third_party_icu_source_common_util_cpp(it);
  PUBLIC_third_party_icu_source_common_util_props_cpp(it);
  PUBLIC_third_party_icu_source_common_utrace_cpp(it);
  PUBLIC_third_party_icu_source_common_utrie_cpp(it);
  PUBLIC_third_party_icu_source_common_utrie2_cpp(it);
  PUBLIC_third_party_icu_source_common_utrie2_builder_cpp(it);
  PUBLIC_third_party_icu_source_common_uts46_cpp(it);
  PUBLIC_third_party_icu_source_common_utypes_cpp(it);
  PUBLIC_third_party_icu_source_common_uvector_cpp(it);
  PUBLIC_third_party_icu_source_common_uvectr32_cpp(it);
  PUBLIC_third_party_icu_source_common_uvectr64_cpp(it);
  PUBLIC_third_party_icu_source_common_wintz_cpp(it);
}

#endif

