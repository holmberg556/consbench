/* CONSBENCH file begin */
#ifndef MAIN_LIBEVENT_CPP
#define MAIN_LIBEVENT_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_libevent_main_libevent_cpp, "third_party/libevent/main_libevent.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_libevent_main_libevent_cpp, "third_party/libevent/main_libevent.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_libevent_buffer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libevent_evbuffer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libevent_evdns_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libevent_event_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libevent_event_tagging_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libevent_evrpc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libevent_evutil_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libevent_http_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libevent_log_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libevent_poll_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libevent_select_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libevent_signal_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libevent_strlcpy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libevent_epoll_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libevent_epoll_sub_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_libevent_main_libevent_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_libevent_buffer_cpp(it);
  PUBLIC_third_party_libevent_evbuffer_cpp(it);
  PUBLIC_third_party_libevent_evdns_cpp(it);
  PUBLIC_third_party_libevent_event_cpp(it);
  PUBLIC_third_party_libevent_event_tagging_cpp(it);
  PUBLIC_third_party_libevent_evrpc_cpp(it);
  PUBLIC_third_party_libevent_evutil_cpp(it);
  PUBLIC_third_party_libevent_http_cpp(it);
  PUBLIC_third_party_libevent_log_cpp(it);
  PUBLIC_third_party_libevent_poll_cpp(it);
  PUBLIC_third_party_libevent_select_cpp(it);
  PUBLIC_third_party_libevent_signal_cpp(it);
  PUBLIC_third_party_libevent_strlcpy_cpp(it);
  PUBLIC_third_party_libevent_epoll_cpp(it);
  PUBLIC_third_party_libevent_epoll_sub_cpp(it);
}

#endif

