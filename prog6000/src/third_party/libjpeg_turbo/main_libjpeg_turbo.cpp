/* CONSBENCH file begin */
#ifndef MAIN_LIBJPEG_TURBO_CPP
#define MAIN_LIBJPEG_TURBO_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_libjpeg_turbo_main_libjpeg_turbo_cpp, "third_party/libjpeg_turbo/main_libjpeg_turbo.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_libjpeg_turbo_main_libjpeg_turbo_cpp, "third_party/libjpeg_turbo/main_libjpeg_turbo.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_libjpeg_turbo_jcapimin_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jcapistd_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jccoefct_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jccolor_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jcdctmgr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jchuff_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jcinit_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jcmainct_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jcmarker_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jcmaster_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jcomapi_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jcparam_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jcphuff_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jcprepct_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jcsample_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jdapimin_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jdapistd_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jdatadst_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jdatasrc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jdcoefct_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jdcolor_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jddctmgr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jdhuff_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jdinput_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jdmainct_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jdmarker_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jdmaster_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jdmerge_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jdphuff_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jdpostct_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jdsample_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jerror_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jfdctflt_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jfdctfst_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jfdctint_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jidctflt_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jidctfst_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jidctint_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jidctred_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jmemmgr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jmemnobs_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jquant1_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jquant2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_jutils_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libjpeg_turbo_simd_jsimd_x86_64_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_libjpeg_turbo_main_libjpeg_turbo_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_libjpeg_turbo_jcapimin_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jcapistd_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jccoefct_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jccolor_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jcdctmgr_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jchuff_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jcinit_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jcmainct_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jcmarker_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jcmaster_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jcomapi_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jcparam_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jcphuff_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jcprepct_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jcsample_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jdapimin_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jdapistd_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jdatadst_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jdatasrc_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jdcoefct_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jdcolor_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jddctmgr_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jdhuff_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jdinput_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jdmainct_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jdmarker_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jdmaster_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jdmerge_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jdphuff_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jdpostct_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jdsample_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jerror_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jfdctflt_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jfdctfst_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jfdctint_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jidctflt_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jidctfst_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jidctint_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jidctred_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jmemmgr_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jmemnobs_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jquant1_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jquant2_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_jutils_cpp(it);
  PUBLIC_third_party_libjpeg_turbo_simd_jsimd_x86_64_cpp(it);
}

#endif

