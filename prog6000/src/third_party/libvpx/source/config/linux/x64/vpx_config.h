/* CONSBENCH file begin */
#ifndef VPX_CONFIG_H_1996
#define VPX_CONFIG_H_1996

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_libvpx_source_config_linux_x64_vpx_config_h, "third_party/libvpx/source/config/linux/x64/vpx_config.h");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_libvpx_source_config_linux_x64_vpx_config_h, "third_party/libvpx/source/config/linux/x64/vpx_config.h");

/*-no- .................................................................... */
/*-no- #define RESTRICT     */
/*-no- #define ARCH_ARM 0 */
/*-no- #define ARCH_MIPS 0 */
/*-no- #define ARCH_X86 0 */
/*-no- #define ARCH_X86_64 1 */
/*-no- #define ARCH_PPC32 0 */
/*-no- #define ARCH_PPC64 0 */
/*-no- #define HAVE_ARMV5TE 0 */
/*-no- #define HAVE_ARMV6 0 */
/*-no- #define HAVE_ARMV7 0 */
/*-no- #define HAVE_IWMMXT 0 */
/*-no- #define HAVE_IWMMXT2 0 */
/*-no- #define HAVE_MIPS32 0 */
/*-no- #define HAVE_MMX 1 */
/*-no- #define HAVE_SSE 1 */
/*-no- #define HAVE_SSE2 1 */
/*-no- #define HAVE_SSE3 1 */
/*-no- #define HAVE_SSSE3 1 */
/*-no- #define HAVE_SSE4_1 1 */
/*-no- #define HAVE_ALTIVEC 0 */
/*-no- #define HAVE_VPX_PORTS 1 */
/*-no- #define HAVE_STDINT_H 1 */
/*-no- #define HAVE_ALT_TREE_LAYOUT 0 */
/*-no- #define HAVE_PTHREAD_H 1 */
/*-no- #define HAVE_SYS_MMAN_H 1 */
/*-no- #define CONFIG_EXTERNAL_BUILD 0 */
/*-no- #define CONFIG_INSTALL_DOCS 0 */
/*-no- #define CONFIG_INSTALL_BINS 1 */
/*-no- #define CONFIG_INSTALL_LIBS 1 */
/*-no- #define CONFIG_INSTALL_SRCS 0 */
/*-no- #define CONFIG_DEBUG 0 */
/*-no- #define CONFIG_GPROF 0 */
/*-no- #define CONFIG_GCOV 0 */
/*-no- #define CONFIG_RVCT 0 */
/*-no- #define CONFIG_GCC 1 */
/*-no- #define CONFIG_MSVS 0 */
/*-no- #define CONFIG_PIC 1 */
/*-no- #define CONFIG_BIG_ENDIAN 0 */
/*-no- #define CONFIG_CODEC_SRCS 0 */
/*-no- #define CONFIG_DEBUG_LIBS 0 */
/*-no- #define CONFIG_FAST_UNALIGNED 1 */
/*-no- #define CONFIG_MEM_MANAGER 0 */
/*-no- #define CONFIG_MEM_TRACKER 0 */
/*-no- #define CONFIG_MEM_CHECKS 0 */
/*-no- #define CONFIG_MD5 1 */
/*-no- #define CONFIG_DEQUANT_TOKENS 0 */
/*-no- #define CONFIG_DC_RECON 0 */
/*-no- #define CONFIG_RUNTIME_CPU_DETECT 1 */
/*-no- #define CONFIG_POSTPROC 0 */
/*-no- #define CONFIG_MULTITHREAD 1 */
/*-no- #define CONFIG_PSNR 0 */
/*-no- #define CONFIG_VP8_ENCODER 1 */
/*-no- #define CONFIG_VP8_DECODER 1 */
/*-no- #define CONFIG_VP8 1 */
/*-no- #define CONFIG_ENCODERS 1 */
/*-no- #define CONFIG_DECODERS 1 */
/*-no- #define CONFIG_STATIC_MSVCRT 0 */
/*-no- #define CONFIG_SPATIAL_RESAMPLING 1 */
/*-no- #define CONFIG_REALTIME_ONLY 0 */
/*-no- #define CONFIG_SHARED 0 */
/*-no- #define CONFIG_SMALL 0 */
/*-no- #define CONFIG_ARM_ASM_DETOK 0 */

#endif
/* CONSBENCH file end */
