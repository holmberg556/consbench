/* CONSBENCH file begin */
#ifndef SUBPIXEL_ARM_H_2023
#define SUBPIXEL_ARM_H_2023

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_libvpx_source_libvpx_vp8_common_arm_subpixel_arm_h, "third_party/libvpx/source/libvpx/vp8/common/arm/subpixel_arm.h");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_libvpx_source_libvpx_vp8_common_arm_subpixel_arm_h, "third_party/libvpx/source/libvpx/vp8/common/arm/subpixel_arm.h");

/*-no- ... */
/*-no- ..................................................................... */
/*-no- .. */
/*-no- .............................................................. */
/*-no- ................................................................... */
/*-no- ....................................................................... */
/*-no- .............................................................. */
/*-no- ................................................................ */
/*-no- .... */
/*-no-  */
/*-no-  */
/*-no- #ifndef SUBPIXEL_ARM_H */
/*-no- #define SUBPIXEL_ARM_H */
/*-no-  */
/*-no- #if HAVE_ARMV6 */
/*-no- ................................................................. */
/*-no- ............................................................... */
/*-no- ............................................................... */
/*-no- ............................................................ */
/*-no- ................................................................... */
/*-no- ................................................................. */
/*-no- ................................................................. */
/*-no- ................................................................. */
/*-no-  */
/*-no- #if !CONFIG_RUNTIME_CPU_DETECT */
/*-no- #undef  vp8_subpix_sixtap16x16 */
/*-no- #define vp8_subpix_sixtap16x16 vp8_sixtap_predict16x16_armv6 */
/*-no-  */
/*-no- #undef  vp8_subpix_sixtap8x8 */
/*-no- #define vp8_subpix_sixtap8x8 vp8_sixtap_predict8x8_armv6 */
/*-no-  */
/*-no- #undef  vp8_subpix_sixtap8x4 */
/*-no- #define vp8_subpix_sixtap8x4 vp8_sixtap_predict8x4_armv6 */
/*-no-  */
/*-no- #undef  vp8_subpix_sixtap4x4 */
/*-no- #define vp8_subpix_sixtap4x4 vp8_sixtap_predict_armv6 */
/*-no-  */
/*-no- #undef  vp8_subpix_bilinear16x16 */
/*-no- #define vp8_subpix_bilinear16x16 vp8_bilinear_predict16x16_armv6 */
/*-no-  */
/*-no- #undef  vp8_subpix_bilinear8x8 */
/*-no- #define vp8_subpix_bilinear8x8 vp8_bilinear_predict8x8_armv6 */
/*-no-  */
/*-no- #undef  vp8_subpix_bilinear8x4 */
/*-no- #define vp8_subpix_bilinear8x4 vp8_bilinear_predict8x4_armv6 */
/*-no-  */
/*-no- #undef  vp8_subpix_bilinear4x4 */
/*-no- #define vp8_subpix_bilinear4x4 vp8_bilinear_predict4x4_armv6 */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if HAVE_ARMV7 */
/*-no- ................................................................ */
/*-no- .............................................................. */
/*-no- .............................................................. */
/*-no- ........................................................... */
/*-no- .................................................................. */
/*-no- ................................................................ */
/*-no- ................................................................ */
/*-no- ................................................................ */
/*-no-  */
/*-no- #if !CONFIG_RUNTIME_CPU_DETECT */
/*-no- #undef  vp8_subpix_sixtap16x16 */
/*-no- #define vp8_subpix_sixtap16x16 vp8_sixtap_predict16x16_neon */
/*-no-  */
/*-no- #undef  vp8_subpix_sixtap8x8 */
/*-no- #define vp8_subpix_sixtap8x8 vp8_sixtap_predict8x8_neon */
/*-no-  */
/*-no- #undef  vp8_subpix_sixtap8x4 */
/*-no- #define vp8_subpix_sixtap8x4 vp8_sixtap_predict8x4_neon */
/*-no-  */
/*-no- #undef  vp8_subpix_sixtap4x4 */
/*-no- #define vp8_subpix_sixtap4x4 vp8_sixtap_predict_neon */
/*-no-  */
/*-no- #undef  vp8_subpix_bilinear16x16 */
/*-no- #define vp8_subpix_bilinear16x16 vp8_bilinear_predict16x16_neon */
/*-no-  */
/*-no- #undef  vp8_subpix_bilinear8x8 */
/*-no- #define vp8_subpix_bilinear8x8 vp8_bilinear_predict8x8_neon */
/*-no-  */
/*-no- #undef  vp8_subpix_bilinear8x4 */
/*-no- #define vp8_subpix_bilinear8x4 vp8_bilinear_predict8x4_neon */
/*-no-  */
/*-no- #undef  vp8_subpix_bilinear4x4 */
/*-no- #define vp8_subpix_bilinear4x4 vp8_bilinear_predict4x4_neon */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #endif */

#endif
/* CONSBENCH file end */
