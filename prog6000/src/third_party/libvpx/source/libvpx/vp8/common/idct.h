/* CONSBENCH file begin */
#ifndef IDCT_H_2052
#define IDCT_H_2052

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_libvpx_source_libvpx_vp8_common_idct_h, "third_party/libvpx/source/libvpx/vp8/common/idct.h");

/* CONSBENCH includes begin */
#include "x86/idct_x86.h"
#include "arm/idct_arm.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_libvpx_source_libvpx_vp8_common_idct_h, "third_party/libvpx/source/libvpx/vp8/common/idct.h");

/*-no- ... */
/*-no- ..................................................................... */
/*-no- .. */
/*-no- .............................................................. */
/*-no- ................................................................... */
/*-no- ....................................................................... */
/*-no- .............................................................. */
/*-no- ................................................................ */
/*-no- .... */
/*-no-  */
/*-no-  */
/*-no- #ifndef __INC_IDCT_H */
/*-no- #define __INC_IDCT_H */
/*-no-  */
/*-no- #define prototype_second_order(sym) \ */
/*-no- ......................................... */
/*-no-  */
/*-no- #define prototype_idct(sym) \ */
/*-no- .................................................... */
/*-no-  */
/*-no- #define prototype_idct_scalar_add(sym) \ */
/*-no- ........................... */
/*-no- .......................................................... */
/*-no- ................................... */
/*-no-  */
/*-no- #if ARCH_X86 || ARCH_X86_64 */
/*-no- #include "x86/idct_x86.h" */
/*-no- #endif */
/*-no-  */
/*-no- #if ARCH_ARM */
/*-no- #include "arm/idct_arm.h" */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef vp8_idct_idct1 */
/*-no- #define vp8_idct_idct1 vp8_short_idct4x4llm_1_c */
/*-no- #endif */
/*-no- ...................................... */
/*-no-  */
/*-no- #ifndef vp8_idct_idct16 */
/*-no- #define vp8_idct_idct16 vp8_short_idct4x4llm_c */
/*-no- #endif */
/*-no- ....................................... */
/*-no-  */
/*-no- #ifndef vp8_idct_idct1_scalar_add */
/*-no- #define vp8_idct_idct1_scalar_add vp8_dc_only_idct_add_c */
/*-no- #endif */
/*-no- ............................................................ */
/*-no-  */
/*-no-  */
/*-no- #ifndef vp8_idct_iwalsh1 */
/*-no- #define vp8_idct_iwalsh1 vp8_short_inv_walsh4x4_1_c */
/*-no- #endif */
/*-no- ................................................ */
/*-no-  */
/*-no- #ifndef vp8_idct_iwalsh16 */
/*-no- #define vp8_idct_iwalsh16 vp8_short_inv_walsh4x4_c */
/*-no- #endif */
/*-no- ................................................. */
/*-no-  */
/*-no- ......................................... */
/*-no- ............................................................... */
/*-no- ......................................................... */
/*-no-  */
/*-no- .............. */
/*-no- . */
/*-no- ................................... */
/*-no- .................................... */
/*-no- .............................................. */
/*-no-  */
/*-no- .................................. */
/*-no- ................................... */
/*-no- ......................... */
/*-no-  */
/*-no- #if CONFIG_RUNTIME_CPU_DETECT */
/*-no- #define IDCT_INVOKE(ctx,fn) (ctx)->fn */
/*-no- #else */
/*-no- #define IDCT_INVOKE(ctx,fn) vp8_idct_##fn */
/*-no- #endif */
/*-no-  */
/*-no- #endif */

#endif
/* CONSBENCH file end */
