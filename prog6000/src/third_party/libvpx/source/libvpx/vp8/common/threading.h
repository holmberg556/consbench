/* CONSBENCH file begin */
#ifndef THREADING_H_2098
#define THREADING_H_2098

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_libvpx_source_libvpx_vp8_common_threading_h, "third_party/libvpx/source/libvpx/vp8/common/threading.h");

/* CONSBENCH includes begin */
#include <time.h>
#include "vpx_ports/x86.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_libvpx_source_libvpx_vp8_common_threading_h, "third_party/libvpx/source/libvpx/vp8/common/threading.h");

/*-no- ... */
/*-no- ..................................................................... */
/*-no- .. */
/*-no- .............................................................. */
/*-no- ................................................................... */
/*-no- ....................................................................... */
/*-no- .............................................................. */
/*-no- ................................................................ */
/*-no- .... */
/*-no-  */
/*-no-  */
/*-no- #ifndef _PTHREAD_EMULATION */
/*-no- #define _PTHREAD_EMULATION */
/*-no-  */
/*-no- #define VPXINFINITE 10000       /-* 10second. *-/ */
/*-no-  */
/*-no- ................................ */
/*-no- #ifdef _WIN32 */
/*-no- ............. */
/*-no- #define _WIN32_WINNT 0x500 /-* WINBASE.H - Enable signal_object_and_wait *-/ */
/*-no- #include <process.h> */
/*-no- #include <windows.h> */
/*-no- #define THREAD_FUNCTION DWORD WINAPI */
/*-no- #define THREAD_FUNCTION_RETURN DWORD */
/*-no- #define THREAD_SPECIFIC_INDEX DWORD */
/*-no- #define pthread_t HANDLE */
/*-no- #define pthread_attr_t DWORD */
/*-no- #define pthread_create(thhandle,attr,thfunc,tharg) (int)((*thhandle=(HANDLE)_beginthreadex(NULL,0,(unsigned int (__stdcall *)(void *))thfunc,tharg,0,NULL))==NULL) */
/*-no- #define pthread_join(thread, result) ((WaitForSingleObject((thread),VPXINFINITE)!=WAIT_OBJECT_0) || !CloseHandle(thread)) */
/*-no- #define pthread_detach(thread) if(thread!=NULL)CloseHandle(thread) */
/*-no- #define thread_sleep(nms) Sleep(nms) */
/*-no- #define pthread_cancel(thread) terminate_thread(thread,0) */
/*-no- #define ts_key_create(ts_key, destructor) {ts_key = TlsAlloc();}; */
/*-no- #define pthread_getspecific(ts_key) TlsGetValue(ts_key) */
/*-no- #define pthread_setspecific(ts_key, value) TlsSetValue(ts_key, (void *)value) */
/*-no- #define pthread_self() GetCurrentThreadId() */
/*-no- #else */
/*-no- #ifdef __APPLE__ */
/*-no- #include <mach/semaphore.h> */
/*-no- #include <mach/task.h> */
/*-no- #include <time.h> */
/*-no- #include <unistd.h> */
/*-no-  */
/*-no- #else */
/*-no- #include <semaphore.h> */
/*-no- #endif */
/*-no-  */
/*-no- #include <pthread.h> */
/*-no- ................ */
/*-no- ............................................ */
/*-no- #define THREAD_FUNCTION void * */
/*-no- #define THREAD_FUNCTION_RETURN void * */
/*-no- #define THREAD_SPECIFIC_INDEX pthread_key_t */
/*-no- #define ts_key_create(ts_key, destructor) pthread_key_create (&(ts_key), destructor); */
/*-no- #endif */
/*-no-  */
/*-no- ................................................... */
/*-no- #ifdef _WIN32 */
/*-no- #define sem_t HANDLE */
/*-no- #define pause(voidpara) __asm PAUSE */
/*-no- #define sem_init(sem, sem_attr1, sem_init_value) (int)((*sem = CreateEvent(NULL,FALSE,FALSE,NULL))==NULL) */
/*-no- #define sem_wait(sem) (int)(WAIT_OBJECT_0 != WaitForSingleObject(*sem,VPXINFINITE)) */
/*-no- #define sem_post(sem) SetEvent(*sem) */
/*-no- #define sem_destroy(sem) if(*sem)((int)(CloseHandle(*sem))==TRUE) */
/*-no- #define thread_sleep(nms) Sleep(nms) */
/*-no-  */
/*-no- #else */
/*-no-  */
/*-no- #ifdef __APPLE__ */
/*-no- #define sem_t semaphore_t */
/*-no- #define sem_init(X,Y,Z) semaphore_create(mach_task_self(), X, SYNC_POLICY_FIFO, Z) */
/*-no- #define sem_wait(sem) (semaphore_wait(*sem) ) */
/*-no- #define sem_post(sem) semaphore_signal(*sem) */
/*-no- #define sem_destroy(sem) semaphore_destroy(mach_task_self(),*sem) */
/*-no- #define thread_sleep(nms) /-* { struct timespec ts;ts.tv_sec=0; ts.tv_nsec = 1000*nms;nanosleep(&ts, NULL);} *-/ */
/*-no- #else */
/*-no- #include <unistd.h> */
/*-no- #include <sched.h> */
/*-no- #define thread_sleep(nms) sched_yield();/-* {struct timespec ts;ts.tv_sec=0; ts.tv_nsec = 1000*nms;nanosleep(&ts, NULL);} *-/ */
/*-no- #endif */
/*-no- .................................... */
/*-no-  */
/*-no- #endif */
/*-no-  */
/*-no- #if ARCH_X86 || ARCH_X86_64 */
/*-no- #include "vpx_ports/x86.h" */
/*-no- #else */
/*-no- #define x86_pause_hint() */
/*-no- #endif */
/*-no-  */
/*-no- #endif */

#endif
/* CONSBENCH file end */
