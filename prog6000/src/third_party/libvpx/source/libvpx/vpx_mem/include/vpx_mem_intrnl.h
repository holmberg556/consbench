/* CONSBENCH file begin */
#ifndef VPX_MEM_INTRNL_H_2236
#define VPX_MEM_INTRNL_H_2236

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_libvpx_source_libvpx_vpx_mem_include_vpx_mem_intrnl_h, "third_party/libvpx/source/libvpx/vpx_mem/include/vpx_mem_intrnl.h");

/* CONSBENCH includes begin */
#include "vpx_ports/config.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_libvpx_source_libvpx_vpx_mem_include_vpx_mem_intrnl_h, "third_party/libvpx/source/libvpx/vpx_mem/include/vpx_mem_intrnl.h");

/*-no- ... */
/*-no- ..................................................................... */
/*-no- .. */
/*-no- .............................................................. */
/*-no- ................................................................... */
/*-no- ....................................................................... */
/*-no- .............................................................. */
/*-no- ................................................................ */
/*-no- .... */
/*-no-  */
/*-no-  */
/*-no- #ifndef __VPX_MEM_INTRNL_H__ */
/*-no- #define __VPX_MEM_INTRNL_H__ */
/*-no- #include "vpx_ports/config.h" */
/*-no-  */
/*-no- #ifndef CONFIG_MEM_MANAGER */
/*-no- # if defined(VXWORKS) */
/*-no- #  define CONFIG_MEM_MANAGER  1 /-*include heap manager functionality,*-/ */
/*-no- ................................. */
/*-no- # else */
/*-no- #  define CONFIG_MEM_MANAGER  0 /-*include heap manager functionality*-/ */
/*-no- # endif */
/*-no- #endif /-*CONFIG_MEM_MANAGER*-/ */
/*-no-  */
/*-no- #ifndef CONFIG_MEM_TRACKER */
/*-no- # define CONFIG_MEM_TRACKER     1 /-*include xvpx_* calls in the lib*-/ */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef CONFIG_MEM_CHECKS */
/*-no- # define CONFIG_MEM_CHECKS      0 /-*include some basic safety checks in */
/*-no- .................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef USE_GLOBAL_FUNCTION_POINTERS */
/*-no- # define USE_GLOBAL_FUNCTION_POINTERS   0  /-*use function pointers instead of compiled functions.*-/ */
/*-no- #endif */
/*-no-  */
/*-no- #if CONFIG_MEM_TRACKER */
/*-no- # include "vpx_mem_tracker.h" */
/*-no- # if VPX_MEM_TRACKER_VERSION_CHIEF != 2 || VPX_MEM_TRACKER_VERSION_MAJOR != 5 */
/*-no- #  error "vpx_mem requires memory tracker version 2.5 to track memory usage" */
/*-no- # endif */
/*-no- #endif */
/*-no-  */
/*-no- #define ADDRESS_STORAGE_SIZE      sizeof(size_t) */
/*-no-  */
/*-no- #ifndef DEFAULT_ALIGNMENT */
/*-no- # if defined(VXWORKS) */
/*-no- #  define DEFAULT_ALIGNMENT        32        /-*default addr alignment to use in */
/*-no- ............................................................................. */
/*-no- ................................................................... */
/*-no- # else */
/*-no- #  define DEFAULT_ALIGNMENT        1 */
/*-no- # endif */
/*-no- #endif */
/*-no-  */
/*-no- #if DEFAULT_ALIGNMENT < 1 */
/*-no- # error "DEFAULT_ALIGNMENT must be >= 1!" */
/*-no- #endif */
/*-no-  */
/*-no- #if CONFIG_MEM_TRACKER */
/*-no- # define TRY_BOUNDS_CHECK         1        /-*when set to 1 pads each allocation, */
/*-no- ........................................................................... */
/*-no- ............................................................................... */
/*-no- ...................................................................... */
/*-no- .............................. */
/*-no- #else */
/*-no- # define TRY_BOUNDS_CHECK         0 */
/*-no- #endif /-*CONFIG_MEM_TRACKER*-/ */
/*-no-  */
/*-no- #if TRY_BOUNDS_CHECK */
/*-no- # define TRY_BOUNDS_CHECK_ON_FREE 0          /-*checks mem integrity on every */
/*-no- ...................................................................... */
/*-no- # define BOUNDS_CHECK_VALUE       0xdeadbeef /-*value stored before/after ea. */
/*-no- .............................................................................. */
/*-no- # define BOUNDS_CHECK_PAD_SIZE    32         /-*size of the padding before and */
/*-no- ............................................................................... */
/*-no- ....................................................................... */
/*-no- ................................................................................ */
/*-no- #else */
/*-no- # define BOUNDS_CHECK_VALUE       0 */
/*-no- # define BOUNDS_CHECK_PAD_SIZE    0 */
/*-no- #endif /-*TRY_BOUNDS_CHECK*-/ */
/*-no-  */
/*-no- #ifndef REMOVE_PRINTFS */
/*-no- # define REMOVE_PRINTFS 0 */
/*-no- #endif */
/*-no-  */
/*-no- ...................................................... */
/*-no- #if REMOVE_PRINTFS */
/*-no- # define _P(x) */
/*-no- #else */
/*-no- # define _P(x) x */
/*-no- #endif */
/*-no-  */
/*-no- ..................................................................... */
/*-no- #define align_addr(addr,align) (void*)(((size_t)(addr) + ((align) - 1)) & (size_t)-(align)) */
/*-no-  */
/*-no- #endif /-*__VPX_MEM_INTRNL_H__*-/ */

#endif
/* CONSBENCH file end */
