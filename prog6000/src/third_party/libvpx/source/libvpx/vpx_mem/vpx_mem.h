/* CONSBENCH file begin */
#ifndef VPX_MEM_H_2256
#define VPX_MEM_H_2256

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_libvpx_source_libvpx_vpx_mem_vpx_mem_h, "third_party/libvpx/source/libvpx/vpx_mem/vpx_mem.h");

/* CONSBENCH includes begin */
#include <stdlib.h>
#include <stddef.h>
#include <stdarg.h>
#  include <string.h>
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_libvpx_source_libvpx_vpx_mem_vpx_mem_h, "third_party/libvpx/source/libvpx/vpx_mem/vpx_mem.h");

/*-no- ... */
/*-no- ..................................................................... */
/*-no- .. */
/*-no- .............................................................. */
/*-no- ................................................................... */
/*-no- ....................................................................... */
/*-no- .............................................................. */
/*-no- ................................................................ */
/*-no- .... */
/*-no-  */
/*-no-  */
/*-no- #ifndef __VPX_MEM_H__ */
/*-no- #define __VPX_MEM_H__ */
/*-no-  */
/*-no- #if defined(__uClinux__) */
/*-no- # include <lddk.h> */
/*-no- #endif */
/*-no-  */
/*-no- ............................ */
/*-no- #define vpx_mem_version "2.2.1.5" */
/*-no-  */
/*-no- #define VPX_MEM_VERSION_CHIEF 2 */
/*-no- #define VPX_MEM_VERSION_MAJOR 2 */
/*-no- #define VPX_MEM_VERSION_MINOR 1 */
/*-no- #define VPX_MEM_VERSION_PATCH 5 */
/*-no- .................................. */
/*-no-  */
/*-no- #ifndef VPX_TRACK_MEM_USAGE */
/*-no- # define VPX_TRACK_MEM_USAGE       0  /-* enable memory tracking/integrity checks *-/ */
/*-no- #endif */
/*-no- #ifndef VPX_CHECK_MEM_FUNCTIONS */
/*-no- # define VPX_CHECK_MEM_FUNCTIONS   0  /-* enable basic safety checks in _memcpy, */
/*-no- .................................................................. */
/*-no- #endif */
/*-no- #ifndef REPLACE_BUILTIN_FUNCTIONS */
/*-no- # define REPLACE_BUILTIN_FUNCTIONS 0  /-* replace builtin functions with their */
/*-no- ............................................................. */
/*-no- #endif */
/*-no-  */
/*-no- #include <stdlib.h> */
/*-no- #include <stddef.h> */
/*-no-  */
/*-no- #if defined(__cplusplus) */
/*-no- ............ */
/*-no- #endif */
/*-no-  */
/*-no- ....... */
/*-no- ............................. */
/*-no- .................................................................................. */
/*-no- ................................................................................. */
/*-no- ................... */
/*-no- ....... */
/*-no- ........................................... */
/*-no-  */
/*-no- ....... */
/*-no- .......................................... */
/*-no- .............................................................................. */
/*-no- ................................................... */
/*-no- ............... */
/*-no- ....................... */
/*-no- ............................................................................... */
/*-no- .......................................................................... */
/*-no- .................................................................. */
/*-no- ....... */
/*-no- ........................................... */
/*-no-  */
/*-no- .................................................. */
/*-no- .................................. */
/*-no- .............................................. */
/*-no- ................................................. */
/*-no- ................................ */
/*-no-  */
/*-no- ................................................................. */
/*-no- ......................................................... */
/*-no- ................................................................. */
/*-no-  */
/*-no- .................................... */
/*-no- ........................................................... */
/*-no- ...................................................... */
/*-no-  */
/*-no- ................................................... */
/*-no- ........................................... */
/*-no- ................................................... */
/*-no- .................................................... */
/*-no- ......................................... */
/*-no- ................................................................. */
/*-no- ........................................................ */
/*-no- .................................................................. */
/*-no-  */
/*-no- ...................................................... */
/*-no- ........................................................ */
/*-no- .......................................................... */
/*-no- .................................................... */
/*-no- ........................................................ */
/*-no- ........................................................ */
/*-no- ............................................................ */
/*-no- ...................................... */
/*-no-  */
/*-no-  */
/*-no- ................................................... */
/*-no- #define DMEM_GENERAL 0 */
/*-no-  */
/*-no- #define duck_memalign(X,Y,Z) vpx_memalign(X,Y) */
/*-no- #define duck_malloc(X,Y) vpx_malloc(X) */
/*-no- #define duck_calloc(X,Y,Z) vpx_calloc(X,Y) */
/*-no- #define duck_realloc  vpx_realloc */
/*-no- #define duck_free     vpx_free */
/*-no- #define duck_memcpy   vpx_memcpy */
/*-no- #define duck_memmove  vpx_memmove */
/*-no- #define duck_memset   vpx_memset */
/*-no-  */
/*-no- #if REPLACE_BUILTIN_FUNCTIONS */
/*-no- # ifndef __VPX_MEM_C__ */
/*-no- #  define memalign vpx_memalign */
/*-no- #  define malloc   vpx_malloc */
/*-no- #  define calloc   vpx_calloc */
/*-no- #  define realloc  vpx_realloc */
/*-no- #  define free     vpx_free */
/*-no- #  define memcpy   vpx_memcpy */
/*-no- #  define memmove  vpx_memmove */
/*-no- #  define memset   vpx_memset */
/*-no- # endif */
/*-no- #endif */
/*-no-  */
/*-no- #if CONFIG_MEM_TRACKER */
/*-no- #include <stdarg.h> */
/*-no- ........................................ */
/*-no- .......................................... */
/*-no- .................................................................................. */
/*-no- ....................................................................... */
/*-no- .............................................................. */
/*-no- .......................................... */
/*-no- ........................................................... */
/*-no- # ifndef __VPX_MEM_C__ */
/*-no- #  define vpx_memalign(align, size) xvpx_memalign((align), (size), __FILE__, __LINE__) */
/*-no- #  define vpx_malloc(size)          xvpx_malloc((size), __FILE__, __LINE__) */
/*-no- #  define vpx_calloc(num, size)     xvpx_calloc(num, size, __FILE__, __LINE__) */
/*-no- #  define vpx_realloc(addr, size)   xvpx_realloc(addr, size, __FILE__, __LINE__) */
/*-no- #  define vpx_free(addr)            xvpx_free(addr, __FILE__, __LINE__) */
/*-no- #  define vpx_memory_tracker_check_integrity() vpx_memory_tracker_check_integrity(__FILE__, __LINE__) */
/*-no- #  define vpx_mem_alloc(id,size,align) xvpx_mem_alloc(id, size, align, __FILE__, __LINE__) */
/*-no- #  define vpx_mem_free(id,mem,size) xvpx_mem_free(id, mem, size, __FILE__, __LINE__) */
/*-no- # endif */
/*-no-  */
/*-no- ......................................................................... */
/*-no- ......................................................... */
/*-no- ..................................................................... */
/*-no- ........................................................................ */
/*-no- ....................................................... */
/*-no- .................................................................................. */
/*-no- ............................................................................. */
/*-no-  */
/*-no- #else */
/*-no- # ifndef __VPX_MEM_C__ */
/*-no- #  define vpx_memory_tracker_dump() */
/*-no- #  define vpx_memory_tracker_check_integrity() */
/*-no- #  define vpx_memory_tracker_set_log_type(t,o) 0 */
/*-no- #  define vpx_memory_tracker_set_log_func(u,f) 0 */
/*-no- # endif */
/*-no- #endif */
/*-no-  */
/*-no- #if !VPX_CHECK_MEM_FUNCTIONS */
/*-no- # ifndef __VPX_MEM_C__ */
/*-no- #  include <string.h> */
/*-no- #  define vpx_memcpy  memcpy */
/*-no- #  define vpx_memset  memset */
/*-no- #  define vpx_memmove memmove */
/*-no- # endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef VPX_MEM_PLTFRM */
/*-no- ........................................ */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(__cplusplus) */
/*-no- . */
/*-no- #endif */
/*-no-  */
/*-no- #endif /-* __VPX_MEM_H__ *-/ */

#endif
/* CONSBENCH file end */
