/* CONSBENCH file begin */
#ifndef MAIN_LIBWEBP_CPP
#define MAIN_LIBWEBP_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_libwebp_main_libwebp_cpp, "third_party/libwebp/main_libwebp.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_libwebp_main_libwebp_cpp, "third_party/libwebp/main_libwebp.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_libwebp_bits_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libwebp_dsp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libwebp_frame_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libwebp_quant_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libwebp_tree_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libwebp_vp8_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libwebp_webp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libwebp_yuv_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_libwebp_main_libwebp_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_libwebp_bits_cpp(it);
  PUBLIC_third_party_libwebp_dsp_cpp(it);
  PUBLIC_third_party_libwebp_frame_cpp(it);
  PUBLIC_third_party_libwebp_quant_cpp(it);
  PUBLIC_third_party_libwebp_tree_cpp(it);
  PUBLIC_third_party_libwebp_vp8_cpp(it);
  PUBLIC_third_party_libwebp_webp_cpp(it);
  PUBLIC_third_party_libwebp_yuv_cpp(it);
}

#endif

