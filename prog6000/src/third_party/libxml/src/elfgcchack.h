/* CONSBENCH file begin */
#ifndef ELFGCCHACK_H_2338
#define ELFGCCHACK_H_2338

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_libxml_src_elfgcchack_h, "third_party/libxml/src/elfgcchack.h");

/* CONSBENCH includes begin */
#include "libxml/c14n.h"
#include "libxml/catalog.h"
#include "libxml/chvalid.h"
#include "libxml/debugXML.h"
#include "libxml/dict.h"
#include "libxml/DOCBparser.h"
#include "libxml/encoding.h"
#include "libxml/entities.h"
#include "libxml/globals.h"
#include "libxml/hash.h"
#include "libxml/HTMLparser.h"
#include "libxml/HTMLtree.h"
#include "libxml/list.h"
#include "libxml/nanoftp.h"
#include "libxml/nanohttp.h"
#include "libxml/parser.h"
#include "libxml/parserInternals.h"
#include "libxml/pattern.h"
#include "libxml/relaxng.h"
#include "libxml/SAX2.h"
#include "libxml/SAX.h"
#include "libxml/schemasInternals.h"
#include "libxml/schematron.h"
#include "libxml/threads.h"
#include "libxml/tree.h"
#include "libxml/uri.h"
#include "libxml/valid.h"
#include "libxml/xinclude.h"
#include "libxml/xlink.h"
#include "libxml/xmlautomata.h"
#include "libxml/xmlerror.h"
#include "libxml/xmlexports.h"
#include "libxml/xmlIO.h"
#include "libxml/xmlmemory.h"
#include "libxml/xmlreader.h"
#include "libxml/xmlregexp.h"
#include "libxml/xmlsave.h"
#include "libxml/xmlschemas.h"
#include "libxml/xmlschemastypes.h"
#include "libxml/xmlstring.h"
#include "libxml/xmlunicode.h"
#include "libxml/xmlversion.h"
#include "libxml/xmlwriter.h"
#include "libxml/xpath.h"
#include "libxml/xpathInternals.h"
#include "libxml/xpointer.h"
#include "libxml/xmlmodule.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_libxml_src_elfgcchack_h, "third_party/libxml/src/elfgcchack.h");

/*-no- ... */
/*-no- ...................................................................... */
/*-no- ........................................................................ */
/*-no- .. */
/*-no- ...................................................................... */
/*-no- .......................................................................... */
/*-no- ....................... */
/*-no- ..................................................................... */
/*-no- .... */
/*-no-  */
/*-no- #ifdef IN_LIBXML */
/*-no- #ifdef __GNUC__ */
/*-no- #ifdef PIC */
/*-no- #ifdef linux */
/*-no- #if (__GNUC__ == 3 && __GNUC_MINOR__ >= 3) || (__GNUC__ > 3) */
/*-no-  */
/*-no- #include "libxml/c14n.h" */
/*-no- #include "libxml/catalog.h" */
/*-no- #include "libxml/chvalid.h" */
/*-no- #include "libxml/debugXML.h" */
/*-no- #include "libxml/dict.h" */
/*-no- #include "libxml/DOCBparser.h" */
/*-no- #include "libxml/encoding.h" */
/*-no- #include "libxml/entities.h" */
/*-no- #include "libxml/globals.h" */
/*-no- #include "libxml/hash.h" */
/*-no- #include "libxml/HTMLparser.h" */
/*-no- #include "libxml/HTMLtree.h" */
/*-no- #include "libxml/list.h" */
/*-no- #include "libxml/nanoftp.h" */
/*-no- #include "libxml/nanohttp.h" */
/*-no- #include "libxml/parser.h" */
/*-no- #include "libxml/parserInternals.h" */
/*-no- #include "libxml/pattern.h" */
/*-no- #include "libxml/relaxng.h" */
/*-no- #include "libxml/SAX2.h" */
/*-no- #include "libxml/SAX.h" */
/*-no- #include "libxml/schemasInternals.h" */
/*-no- #include "libxml/schematron.h" */
/*-no- #include "libxml/threads.h" */
/*-no- #include "libxml/tree.h" */
/*-no- #include "libxml/uri.h" */
/*-no- #include "libxml/valid.h" */
/*-no- #include "libxml/xinclude.h" */
/*-no- #include "libxml/xlink.h" */
/*-no- #include "libxml/xmlautomata.h" */
/*-no- #include "libxml/xmlerror.h" */
/*-no- #include "libxml/xmlexports.h" */
/*-no- #include "libxml/xmlIO.h" */
/*-no- #include "libxml/xmlmemory.h" */
/*-no- #include "libxml/xmlreader.h" */
/*-no- #include "libxml/xmlregexp.h" */
/*-no- #include "libxml/xmlsave.h" */
/*-no- #include "libxml/xmlschemas.h" */
/*-no- #include "libxml/xmlschemastypes.h" */
/*-no- #include "libxml/xmlstring.h" */
/*-no- #include "libxml/xmlunicode.h" */
/*-no- #include "libxml/xmlversion.h" */
/*-no- #include "libxml/xmlwriter.h" */
/*-no- #include "libxml/xpath.h" */
/*-no- #include "libxml/xpathInternals.h" */
/*-no- #include "libxml/xpointer.h" */
/*-no- #include "libxml/xmlmodule.h" */
/*-no-  */
/*-no- .......................................... */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef __xmlGenericError */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef __xmlGenericError */
/*-no- .......................................................................................................... */
/*-no- #define __xmlGenericError __xmlGenericError__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef __xmlGenericErrorContext */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef __xmlGenericErrorContext */
/*-no- ........................................................................................................................ */
/*-no- #define __xmlGenericErrorContext __xmlGenericErrorContext__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- ........................................... */
/*-no- #if defined(LIBXML_DOCB_ENABLED) */
/*-no- #ifdef bottom_DOCBparser */
/*-no- #undef docbCreatePushParserCtxt */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef docbCreatePushParserCtxt */
/*-no- ........................................................................................................................ */
/*-no- #define docbCreatePushParserCtxt docbCreatePushParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlAttrAllowed */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlAttrAllowed */
/*-no- ...................................................................................................... */
/*-no- #define htmlAttrAllowed htmlAttrAllowed__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlAutoCloseTag */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef htmlAutoCloseTag */
/*-no- ........................................................................................................ */
/*-no- #define htmlAutoCloseTag htmlAutoCloseTag__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlCreateFileParserCtxt */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlCreateFileParserCtxt */
/*-no- ........................................................................................................................ */
/*-no- #define htmlCreateFileParserCtxt htmlCreateFileParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlCreateMemoryParserCtxt */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlCreateMemoryParserCtxt */
/*-no- ............................................................................................................................ */
/*-no- #define htmlCreateMemoryParserCtxt htmlCreateMemoryParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) && defined(LIBXML_PUSH_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlCreatePushParserCtxt */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlCreatePushParserCtxt */
/*-no- ........................................................................................................................ */
/*-no- #define htmlCreatePushParserCtxt htmlCreatePushParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlCtxtReadDoc */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlCtxtReadDoc */
/*-no- ...................................................................................................... */
/*-no- #define htmlCtxtReadDoc htmlCtxtReadDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlCtxtReadFd */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlCtxtReadFd */
/*-no- .................................................................................................... */
/*-no- #define htmlCtxtReadFd htmlCtxtReadFd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlCtxtReadFile */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef htmlCtxtReadFile */
/*-no- ........................................................................................................ */
/*-no- #define htmlCtxtReadFile htmlCtxtReadFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlCtxtReadIO */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlCtxtReadIO */
/*-no- .................................................................................................... */
/*-no- #define htmlCtxtReadIO htmlCtxtReadIO__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlCtxtReadMemory */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlCtxtReadMemory */
/*-no- ............................................................................................................ */
/*-no- #define htmlCtxtReadMemory htmlCtxtReadMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlCtxtReset */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlCtxtReset */
/*-no- .................................................................................................. */
/*-no- #define htmlCtxtReset htmlCtxtReset__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlCtxtUseOptions */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlCtxtUseOptions */
/*-no- ............................................................................................................ */
/*-no- #define htmlCtxtUseOptions htmlCtxtUseOptions__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef htmlDefaultSAXHandlerInit */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef htmlDefaultSAXHandlerInit */
/*-no- .......................................................................................................................... */
/*-no- #define htmlDefaultSAXHandlerInit htmlDefaultSAXHandlerInit__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlDocContentDumpFormatOutput */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlDocContentDumpFormatOutput */
/*-no- .................................................................................................................................... */
/*-no- #define htmlDocContentDumpFormatOutput htmlDocContentDumpFormatOutput__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlDocContentDumpOutput */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlDocContentDumpOutput */
/*-no- ........................................................................................................................ */
/*-no- #define htmlDocContentDumpOutput htmlDocContentDumpOutput__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlDocDump */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef htmlDocDump */
/*-no- .............................................................................................. */
/*-no- #define htmlDocDump htmlDocDump__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlDocDumpMemory */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef htmlDocDumpMemory */
/*-no- .......................................................................................................... */
/*-no- #define htmlDocDumpMemory htmlDocDumpMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlDocDumpMemoryFormat */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef htmlDocDumpMemoryFormat */
/*-no- ...................................................................................................................... */
/*-no- #define htmlDocDumpMemoryFormat htmlDocDumpMemoryFormat__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlElementAllowedHere */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlElementAllowedHere */
/*-no- .................................................................................................................... */
/*-no- #define htmlElementAllowedHere htmlElementAllowedHere__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlElementStatusHere */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef htmlElementStatusHere */
/*-no- .................................................................................................................. */
/*-no- #define htmlElementStatusHere htmlElementStatusHere__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlEncodeEntities */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlEncodeEntities */
/*-no- ............................................................................................................ */
/*-no- #define htmlEncodeEntities htmlEncodeEntities__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlEntityLookup */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef htmlEntityLookup */
/*-no- ........................................................................................................ */
/*-no- #define htmlEntityLookup htmlEntityLookup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlEntityValueLookup */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef htmlEntityValueLookup */
/*-no- .................................................................................................................. */
/*-no- #define htmlEntityValueLookup htmlEntityValueLookup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlFreeParserCtxt */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlFreeParserCtxt */
/*-no- ............................................................................................................ */
/*-no- #define htmlFreeParserCtxt htmlFreeParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlGetMetaEncoding */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlGetMetaEncoding */
/*-no- .............................................................................................................. */
/*-no- #define htmlGetMetaEncoding htmlGetMetaEncoding__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlHandleOmittedElem */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef htmlHandleOmittedElem */
/*-no- .................................................................................................................. */
/*-no- #define htmlHandleOmittedElem htmlHandleOmittedElem__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlInitAutoClose */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef htmlInitAutoClose */
/*-no- .......................................................................................................... */
/*-no- #define htmlInitAutoClose htmlInitAutoClose__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlIsAutoClosed */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef htmlIsAutoClosed */
/*-no- ........................................................................................................ */
/*-no- #define htmlIsAutoClosed htmlIsAutoClosed__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlIsBooleanAttr */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef htmlIsBooleanAttr */
/*-no- .......................................................................................................... */
/*-no- #define htmlIsBooleanAttr htmlIsBooleanAttr__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlIsScriptAttribute */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef htmlIsScriptAttribute */
/*-no- .................................................................................................................. */
/*-no- #define htmlIsScriptAttribute htmlIsScriptAttribute__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlNewDoc */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlNewDoc */
/*-no- ............................................................................................ */
/*-no- #define htmlNewDoc htmlNewDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlNewDocNoDtD */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlNewDocNoDtD */
/*-no- ...................................................................................................... */
/*-no- #define htmlNewDocNoDtD htmlNewDocNoDtD__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlNewParserCtxt */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef htmlNewParserCtxt */
/*-no- .......................................................................................................... */
/*-no- #define htmlNewParserCtxt htmlNewParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlNodeDump */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef htmlNodeDump */
/*-no- ................................................................................................ */
/*-no- #define htmlNodeDump htmlNodeDump__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlNodeDumpFile */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef htmlNodeDumpFile */
/*-no- ........................................................................................................ */
/*-no- #define htmlNodeDumpFile htmlNodeDumpFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlNodeDumpFileFormat */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlNodeDumpFileFormat */
/*-no- .................................................................................................................... */
/*-no- #define htmlNodeDumpFileFormat htmlNodeDumpFileFormat__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlNodeDumpFormatOutput */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlNodeDumpFormatOutput */
/*-no- ........................................................................................................................ */
/*-no- #define htmlNodeDumpFormatOutput htmlNodeDumpFormatOutput__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlNodeDumpOutput */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlNodeDumpOutput */
/*-no- ............................................................................................................ */
/*-no- #define htmlNodeDumpOutput htmlNodeDumpOutput__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlNodeStatus */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlNodeStatus */
/*-no- .................................................................................................... */
/*-no- #define htmlNodeStatus htmlNodeStatus__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlParseCharRef */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef htmlParseCharRef */
/*-no- ........................................................................................................ */
/*-no- #define htmlParseCharRef htmlParseCharRef__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) && defined(LIBXML_PUSH_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlParseChunk */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlParseChunk */
/*-no- .................................................................................................... */
/*-no- #define htmlParseChunk htmlParseChunk__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlParseDoc */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef htmlParseDoc */
/*-no- ................................................................................................ */
/*-no- #define htmlParseDoc htmlParseDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlParseDocument */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef htmlParseDocument */
/*-no- .......................................................................................................... */
/*-no- #define htmlParseDocument htmlParseDocument__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlParseElement */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef htmlParseElement */
/*-no- ........................................................................................................ */
/*-no- #define htmlParseElement htmlParseElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlParseEntityRef */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlParseEntityRef */
/*-no- ............................................................................................................ */
/*-no- #define htmlParseEntityRef htmlParseEntityRef__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlParseFile */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlParseFile */
/*-no- .................................................................................................. */
/*-no- #define htmlParseFile htmlParseFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlReadDoc */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef htmlReadDoc */
/*-no- .............................................................................................. */
/*-no- #define htmlReadDoc htmlReadDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlReadFd */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlReadFd */
/*-no- ............................................................................................ */
/*-no- #define htmlReadFd htmlReadFd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlReadFile */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef htmlReadFile */
/*-no- ................................................................................................ */
/*-no- #define htmlReadFile htmlReadFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlReadIO */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlReadIO */
/*-no- ............................................................................................ */
/*-no- #define htmlReadIO htmlReadIO__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlReadMemory */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlReadMemory */
/*-no- .................................................................................................... */
/*-no- #define htmlReadMemory htmlReadMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlSAXParseDoc */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlSAXParseDoc */
/*-no- ...................................................................................................... */
/*-no- #define htmlSAXParseDoc htmlSAXParseDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlSAXParseFile */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef htmlSAXParseFile */
/*-no- ........................................................................................................ */
/*-no- #define htmlSAXParseFile htmlSAXParseFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlSaveFile */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef htmlSaveFile */
/*-no- ................................................................................................ */
/*-no- #define htmlSaveFile htmlSaveFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlSaveFileEnc */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlSaveFileEnc */
/*-no- ...................................................................................................... */
/*-no- #define htmlSaveFileEnc htmlSaveFileEnc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlSaveFileFormat */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlSaveFileFormat */
/*-no- ............................................................................................................ */
/*-no- #define htmlSaveFileFormat htmlSaveFileFormat__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLtree */
/*-no- #undef htmlSetMetaEncoding */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlSetMetaEncoding */
/*-no- .............................................................................................................. */
/*-no- #define htmlSetMetaEncoding htmlSetMetaEncoding__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_HTMLparser */
/*-no- #undef htmlTagLookup */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef htmlTagLookup */
/*-no- .................................................................................................. */
/*-no- #define htmlTagLookup htmlTagLookup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef inputPop */
/*-no- ..................................................................................... */
/*-no- #else */
/*-no- #ifndef inputPop */
/*-no- ........................................................................................ */
/*-no- #define inputPop inputPop__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef inputPush */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef inputPush */
/*-no- .......................................................................................... */
/*-no- #define inputPush inputPush__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef namePop */
/*-no- .................................................................................. */
/*-no- #else */
/*-no- #ifndef namePop */
/*-no- ...................................................................................... */
/*-no- #define namePop namePop__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef namePush */
/*-no- ..................................................................................... */
/*-no- #else */
/*-no- #ifndef namePush */
/*-no- ........................................................................................ */
/*-no- #define namePush namePush__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef nodePop */
/*-no- .................................................................................. */
/*-no- #else */
/*-no- #ifndef nodePop */
/*-no- ...................................................................................... */
/*-no- #define nodePop nodePop__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef nodePush */
/*-no- ..................................................................................... */
/*-no- #else */
/*-no- #ifndef nodePush */
/*-no- ........................................................................................ */
/*-no- #define nodePush nodePush__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef valuePop */
/*-no- ..................................................................................... */
/*-no- #else */
/*-no- #ifndef valuePop */
/*-no- ........................................................................................ */
/*-no- #define valuePop valuePop__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef valuePush */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef valuePush */
/*-no- .......................................................................................... */
/*-no- #define valuePush valuePush__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlACatalogAdd */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlACatalogAdd */
/*-no- .................................................................................................... */
/*-no- #define xmlACatalogAdd xmlACatalogAdd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlACatalogDump */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlACatalogDump */
/*-no- ...................................................................................................... */
/*-no- #define xmlACatalogDump xmlACatalogDump__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlACatalogRemove */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlACatalogRemove */
/*-no- .......................................................................................................... */
/*-no- #define xmlACatalogRemove xmlACatalogRemove__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlACatalogResolve */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlACatalogResolve */
/*-no- ............................................................................................................ */
/*-no- #define xmlACatalogResolve xmlACatalogResolve__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlACatalogResolvePublic */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlACatalogResolvePublic */
/*-no- ........................................................................................................................ */
/*-no- #define xmlACatalogResolvePublic xmlACatalogResolvePublic__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlACatalogResolveSystem */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlACatalogResolveSystem */
/*-no- ........................................................................................................................ */
/*-no- #define xmlACatalogResolveSystem xmlACatalogResolveSystem__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlACatalogResolveURI */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlACatalogResolveURI */
/*-no- .................................................................................................................. */
/*-no- #define xmlACatalogResolveURI xmlACatalogResolveURI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlAddAttributeDecl */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAddAttributeDecl */
/*-no- .............................................................................................................. */
/*-no- #define xmlAddAttributeDecl xmlAddAttributeDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlAddChild */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlAddChild */
/*-no- .............................................................................................. */
/*-no- #define xmlAddChild xmlAddChild__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlAddChildList */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAddChildList */
/*-no- ...................................................................................................... */
/*-no- #define xmlAddChildList xmlAddChildList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_entities */
/*-no- #undef xmlAddDocEntity */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAddDocEntity */
/*-no- ...................................................................................................... */
/*-no- #define xmlAddDocEntity xmlAddDocEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_entities */
/*-no- #undef xmlAddDtdEntity */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAddDtdEntity */
/*-no- ...................................................................................................... */
/*-no- #define xmlAddDtdEntity xmlAddDtdEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlAddElementDecl */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlAddElementDecl */
/*-no- .......................................................................................................... */
/*-no- #define xmlAddElementDecl xmlAddElementDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlAddEncodingAlias */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAddEncodingAlias */
/*-no- .............................................................................................................. */
/*-no- #define xmlAddEncodingAlias xmlAddEncodingAlias__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlAddID */
/*-no- ..................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAddID */
/*-no- ........................................................................................ */
/*-no- #define xmlAddID xmlAddID__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlAddNextSibling */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlAddNextSibling */
/*-no- .......................................................................................................... */
/*-no- #define xmlAddNextSibling xmlAddNextSibling__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlAddNotationDecl */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAddNotationDecl */
/*-no- ............................................................................................................ */
/*-no- #define xmlAddNotationDecl xmlAddNotationDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_HTML_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlAddPrevSibling */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlAddPrevSibling */
/*-no- .......................................................................................................... */
/*-no- #define xmlAddPrevSibling xmlAddPrevSibling__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlAddRef */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlAddRef */
/*-no- .......................................................................................... */
/*-no- #define xmlAddRef xmlAddRef__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlAddSibling */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAddSibling */
/*-no- .................................................................................................. */
/*-no- #define xmlAddSibling xmlAddSibling__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlAllocOutputBuffer */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAllocOutputBuffer */
/*-no- ................................................................................................................ */
/*-no- #define xmlAllocOutputBuffer xmlAllocOutputBuffer__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlAllocParserInputBuffer */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlAllocParserInputBuffer */
/*-no- .......................................................................................................................... */
/*-no- #define xmlAllocParserInputBuffer xmlAllocParserInputBuffer__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlAttrSerializeTxtContent */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAttrSerializeTxtContent */
/*-no- ............................................................................................................................ */
/*-no- #define xmlAttrSerializeTxtContent xmlAttrSerializeTxtContent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataCompile */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAutomataCompile */
/*-no- ............................................................................................................ */
/*-no- #define xmlAutomataCompile xmlAutomataCompile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataGetInitState */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlAutomataGetInitState */
/*-no- ...................................................................................................................... */
/*-no- #define xmlAutomataGetInitState xmlAutomataGetInitState__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataIsDeterminist */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAutomataIsDeterminist */
/*-no- ........................................................................................................................ */
/*-no- #define xmlAutomataIsDeterminist xmlAutomataIsDeterminist__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataNewAllTrans */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAutomataNewAllTrans */
/*-no- .................................................................................................................... */
/*-no- #define xmlAutomataNewAllTrans xmlAutomataNewAllTrans__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataNewCountTrans */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAutomataNewCountTrans */
/*-no- ........................................................................................................................ */
/*-no- #define xmlAutomataNewCountTrans xmlAutomataNewCountTrans__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataNewCountTrans2 */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlAutomataNewCountTrans2 */
/*-no- .......................................................................................................................... */
/*-no- #define xmlAutomataNewCountTrans2 xmlAutomataNewCountTrans2__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataNewCountedTrans */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAutomataNewCountedTrans */
/*-no- ............................................................................................................................ */
/*-no- #define xmlAutomataNewCountedTrans xmlAutomataNewCountedTrans__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataNewCounter */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlAutomataNewCounter */
/*-no- .................................................................................................................. */
/*-no- #define xmlAutomataNewCounter xmlAutomataNewCounter__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataNewCounterTrans */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAutomataNewCounterTrans */
/*-no- ............................................................................................................................ */
/*-no- #define xmlAutomataNewCounterTrans xmlAutomataNewCounterTrans__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataNewEpsilon */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlAutomataNewEpsilon */
/*-no- .................................................................................................................. */
/*-no- #define xmlAutomataNewEpsilon xmlAutomataNewEpsilon__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataNewNegTrans */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAutomataNewNegTrans */
/*-no- .................................................................................................................... */
/*-no- #define xmlAutomataNewNegTrans xmlAutomataNewNegTrans__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataNewOnceTrans */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlAutomataNewOnceTrans */
/*-no- ...................................................................................................................... */
/*-no- #define xmlAutomataNewOnceTrans xmlAutomataNewOnceTrans__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataNewOnceTrans2 */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAutomataNewOnceTrans2 */
/*-no- ........................................................................................................................ */
/*-no- #define xmlAutomataNewOnceTrans2 xmlAutomataNewOnceTrans2__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataNewState */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAutomataNewState */
/*-no- .............................................................................................................. */
/*-no- #define xmlAutomataNewState xmlAutomataNewState__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataNewTransition */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAutomataNewTransition */
/*-no- ........................................................................................................................ */
/*-no- #define xmlAutomataNewTransition xmlAutomataNewTransition__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataNewTransition2 */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlAutomataNewTransition2 */
/*-no- .......................................................................................................................... */
/*-no- #define xmlAutomataNewTransition2 xmlAutomataNewTransition2__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlAutomataSetFinalState */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlAutomataSetFinalState */
/*-no- ........................................................................................................................ */
/*-no- #define xmlAutomataSetFinalState xmlAutomataSetFinalState__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlBoolToText */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBoolToText */
/*-no- .................................................................................................. */
/*-no- #define xmlBoolToText xmlBoolToText__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferAdd */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlBufferAdd */
/*-no- ................................................................................................ */
/*-no- #define xmlBufferAdd xmlBufferAdd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferAddHead */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlBufferAddHead */
/*-no- ........................................................................................................ */
/*-no- #define xmlBufferAddHead xmlBufferAddHead__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferCCat */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBufferCCat */
/*-no- .................................................................................................. */
/*-no- #define xmlBufferCCat xmlBufferCCat__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferCat */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlBufferCat */
/*-no- ................................................................................................ */
/*-no- #define xmlBufferCat xmlBufferCat__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferContent */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlBufferContent */
/*-no- ........................................................................................................ */
/*-no- #define xmlBufferContent xmlBufferContent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferCreate */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBufferCreate */
/*-no- ...................................................................................................... */
/*-no- #define xmlBufferCreate xmlBufferCreate__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferCreateSize */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBufferCreateSize */
/*-no- .............................................................................................................. */
/*-no- #define xmlBufferCreateSize xmlBufferCreateSize__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferCreateStatic */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlBufferCreateStatic */
/*-no- .................................................................................................................. */
/*-no- #define xmlBufferCreateStatic xmlBufferCreateStatic__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferDump */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBufferDump */
/*-no- .................................................................................................. */
/*-no- #define xmlBufferDump xmlBufferDump__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferEmpty */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBufferEmpty */
/*-no- .................................................................................................... */
/*-no- #define xmlBufferEmpty xmlBufferEmpty__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferFree */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBufferFree */
/*-no- .................................................................................................. */
/*-no- #define xmlBufferFree xmlBufferFree__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferGrow */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBufferGrow */
/*-no- .................................................................................................. */
/*-no- #define xmlBufferGrow xmlBufferGrow__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferLength */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBufferLength */
/*-no- ...................................................................................................... */
/*-no- #define xmlBufferLength xmlBufferLength__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferResize */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBufferResize */
/*-no- ...................................................................................................... */
/*-no- #define xmlBufferResize xmlBufferResize__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferSetAllocationScheme */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlBufferSetAllocationScheme */
/*-no- ................................................................................................................................ */
/*-no- #define xmlBufferSetAllocationScheme xmlBufferSetAllocationScheme__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferShrink */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBufferShrink */
/*-no- ...................................................................................................... */
/*-no- #define xmlBufferShrink xmlBufferShrink__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferWriteCHAR */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBufferWriteCHAR */
/*-no- ............................................................................................................ */
/*-no- #define xmlBufferWriteCHAR xmlBufferWriteCHAR__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferWriteChar */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBufferWriteChar */
/*-no- ............................................................................................................ */
/*-no- #define xmlBufferWriteChar xmlBufferWriteChar__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBufferWriteQuotedString */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBufferWriteQuotedString */
/*-no- ............................................................................................................................ */
/*-no- #define xmlBufferWriteQuotedString xmlBufferWriteQuotedString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlBuildQName */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBuildQName */
/*-no- .................................................................................................. */
/*-no- #define xmlBuildQName xmlBuildQName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_uri */
/*-no- #undef xmlBuildRelativeURI */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlBuildRelativeURI */
/*-no- .............................................................................................................. */
/*-no- #define xmlBuildRelativeURI xmlBuildRelativeURI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_uri */
/*-no- #undef xmlBuildURI */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlBuildURI */
/*-no- .............................................................................................. */
/*-no- #define xmlBuildURI xmlBuildURI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlByteConsumed */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlByteConsumed */
/*-no- ...................................................................................................... */
/*-no- #define xmlByteConsumed xmlByteConsumed__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_C14N_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_c14n */
/*-no- #undef xmlC14NDocDumpMemory */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlC14NDocDumpMemory */
/*-no- ................................................................................................................ */
/*-no- #define xmlC14NDocDumpMemory xmlC14NDocDumpMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_C14N_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_c14n */
/*-no- #undef xmlC14NDocSave */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlC14NDocSave */
/*-no- .................................................................................................... */
/*-no- #define xmlC14NDocSave xmlC14NDocSave__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_C14N_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_c14n */
/*-no- #undef xmlC14NDocSaveTo */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlC14NDocSaveTo */
/*-no- ........................................................................................................ */
/*-no- #define xmlC14NDocSaveTo xmlC14NDocSaveTo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_C14N_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_c14n */
/*-no- #undef xmlC14NExecute */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlC14NExecute */
/*-no- .................................................................................................... */
/*-no- #define xmlC14NExecute xmlC14NExecute__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_uri */
/*-no- #undef xmlCanonicPath */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCanonicPath */
/*-no- .................................................................................................... */
/*-no- #define xmlCanonicPath xmlCanonicPath__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogAdd */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCatalogAdd */
/*-no- .................................................................................................. */
/*-no- #define xmlCatalogAdd xmlCatalogAdd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogAddLocal */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCatalogAddLocal */
/*-no- ............................................................................................................ */
/*-no- #define xmlCatalogAddLocal xmlCatalogAddLocal__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogCleanup */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCatalogCleanup */
/*-no- .......................................................................................................... */
/*-no- #define xmlCatalogCleanup xmlCatalogCleanup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogConvert */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCatalogConvert */
/*-no- .......................................................................................................... */
/*-no- #define xmlCatalogConvert xmlCatalogConvert__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogDump */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCatalogDump */
/*-no- .................................................................................................... */
/*-no- #define xmlCatalogDump xmlCatalogDump__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogFreeLocal */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCatalogFreeLocal */
/*-no- .............................................................................................................. */
/*-no- #define xmlCatalogFreeLocal xmlCatalogFreeLocal__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogGetDefaults */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCatalogGetDefaults */
/*-no- .................................................................................................................. */
/*-no- #define xmlCatalogGetDefaults xmlCatalogGetDefaults__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogGetPublic */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCatalogGetPublic */
/*-no- .............................................................................................................. */
/*-no- #define xmlCatalogGetPublic xmlCatalogGetPublic__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogGetSystem */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCatalogGetSystem */
/*-no- .............................................................................................................. */
/*-no- #define xmlCatalogGetSystem xmlCatalogGetSystem__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogIsEmpty */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCatalogIsEmpty */
/*-no- .......................................................................................................... */
/*-no- #define xmlCatalogIsEmpty xmlCatalogIsEmpty__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogLocalResolve */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCatalogLocalResolve */
/*-no- .................................................................................................................... */
/*-no- #define xmlCatalogLocalResolve xmlCatalogLocalResolve__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogLocalResolveURI */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCatalogLocalResolveURI */
/*-no- .......................................................................................................................... */
/*-no- #define xmlCatalogLocalResolveURI xmlCatalogLocalResolveURI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogRemove */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCatalogRemove */
/*-no- ........................................................................................................ */
/*-no- #define xmlCatalogRemove xmlCatalogRemove__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogResolve */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCatalogResolve */
/*-no- .......................................................................................................... */
/*-no- #define xmlCatalogResolve xmlCatalogResolve__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogResolvePublic */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCatalogResolvePublic */
/*-no- ...................................................................................................................... */
/*-no- #define xmlCatalogResolvePublic xmlCatalogResolvePublic__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogResolveSystem */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCatalogResolveSystem */
/*-no- ...................................................................................................................... */
/*-no- #define xmlCatalogResolveSystem xmlCatalogResolveSystem__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogResolveURI */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCatalogResolveURI */
/*-no- ................................................................................................................ */
/*-no- #define xmlCatalogResolveURI xmlCatalogResolveURI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogSetDebug */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCatalogSetDebug */
/*-no- ............................................................................................................ */
/*-no- #define xmlCatalogSetDebug xmlCatalogSetDebug__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogSetDefaultPrefer */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCatalogSetDefaultPrefer */
/*-no- ............................................................................................................................ */
/*-no- #define xmlCatalogSetDefaultPrefer xmlCatalogSetDefaultPrefer__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlCatalogSetDefaults */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCatalogSetDefaults */
/*-no- .................................................................................................................. */
/*-no- #define xmlCatalogSetDefaults xmlCatalogSetDefaults__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlCharEncCloseFunc */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCharEncCloseFunc */
/*-no- .............................................................................................................. */
/*-no- #define xmlCharEncCloseFunc xmlCharEncCloseFunc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlCharEncFirstLine */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCharEncFirstLine */
/*-no- .............................................................................................................. */
/*-no- #define xmlCharEncFirstLine xmlCharEncFirstLine__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlCharEncInFunc */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCharEncInFunc */
/*-no- ........................................................................................................ */
/*-no- #define xmlCharEncInFunc xmlCharEncInFunc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlCharEncOutFunc */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCharEncOutFunc */
/*-no- .......................................................................................................... */
/*-no- #define xmlCharEncOutFunc xmlCharEncOutFunc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_chvalid */
/*-no- #undef xmlCharInRange */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCharInRange */
/*-no- .................................................................................................... */
/*-no- #define xmlCharInRange xmlCharInRange__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlCharStrdup */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCharStrdup */
/*-no- .................................................................................................. */
/*-no- #define xmlCharStrdup xmlCharStrdup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlCharStrndup */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCharStrndup */
/*-no- .................................................................................................... */
/*-no- #define xmlCharStrndup xmlCharStrndup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlCheckFilename */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCheckFilename */
/*-no- ........................................................................................................ */
/*-no- #define xmlCheckFilename xmlCheckFilename__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlCheckHTTPInput */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCheckHTTPInput */
/*-no- .......................................................................................................... */
/*-no- #define xmlCheckHTTPInput xmlCheckHTTPInput__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCheckLanguageID */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCheckLanguageID */
/*-no- ............................................................................................................ */
/*-no- #define xmlCheckLanguageID xmlCheckLanguageID__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlCheckUTF8 */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCheckUTF8 */
/*-no- ................................................................................................ */
/*-no- #define xmlCheckUTF8 xmlCheckUTF8__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlCheckVersion */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCheckVersion */
/*-no- ...................................................................................................... */
/*-no- #define xmlCheckVersion xmlCheckVersion__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlChildElementCount */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlChildElementCount */
/*-no- ................................................................................................................ */
/*-no- #define xmlChildElementCount xmlChildElementCount__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlCleanupCharEncodingHandlers */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCleanupCharEncodingHandlers */
/*-no- .................................................................................................................................... */
/*-no- #define xmlCleanupCharEncodingHandlers xmlCleanupCharEncodingHandlers__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlCleanupEncodingAliases */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCleanupEncodingAliases */
/*-no- .......................................................................................................................... */
/*-no- #define xmlCleanupEncodingAliases xmlCleanupEncodingAliases__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlCleanupGlobals */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCleanupGlobals */
/*-no- .......................................................................................................... */
/*-no- #define xmlCleanupGlobals xmlCleanupGlobals__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlCleanupInputCallbacks */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCleanupInputCallbacks */
/*-no- ........................................................................................................................ */
/*-no- #define xmlCleanupInputCallbacks xmlCleanupInputCallbacks__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlCleanupMemory */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCleanupMemory */
/*-no- ........................................................................................................ */
/*-no- #define xmlCleanupMemory xmlCleanupMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlCleanupOutputCallbacks */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCleanupOutputCallbacks */
/*-no- .......................................................................................................................... */
/*-no- #define xmlCleanupOutputCallbacks xmlCleanupOutputCallbacks__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCleanupParser */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCleanupParser */
/*-no- ........................................................................................................ */
/*-no- #define xmlCleanupParser xmlCleanupParser__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlCleanupPredefinedEntities */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCleanupPredefinedEntities */
/*-no- ................................................................................................................................ */
/*-no- #define xmlCleanupPredefinedEntities xmlCleanupPredefinedEntities__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_threads */
/*-no- #undef xmlCleanupThreads */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCleanupThreads */
/*-no- .......................................................................................................... */
/*-no- #define xmlCleanupThreads xmlCleanupThreads__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlClearNodeInfoSeq */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlClearNodeInfoSeq */
/*-no- .............................................................................................................. */
/*-no- #define xmlClearNodeInfoSeq xmlClearNodeInfoSeq__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlClearParserCtxt */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlClearParserCtxt */
/*-no- ............................................................................................................ */
/*-no- #define xmlClearParserCtxt xmlClearParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlConvertSGMLCatalog */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlConvertSGMLCatalog */
/*-no- .................................................................................................................. */
/*-no- #define xmlConvertSGMLCatalog xmlConvertSGMLCatalog__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlCopyAttributeTable */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCopyAttributeTable */
/*-no- .................................................................................................................. */
/*-no- #define xmlCopyAttributeTable xmlCopyAttributeTable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlCopyChar */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCopyChar */
/*-no- .............................................................................................. */
/*-no- #define xmlCopyChar xmlCopyChar__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlCopyCharMultiByte */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCopyCharMultiByte */
/*-no- ................................................................................................................ */
/*-no- #define xmlCopyCharMultiByte xmlCopyCharMultiByte__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlCopyDoc */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCopyDoc */
/*-no- ............................................................................................ */
/*-no- #define xmlCopyDoc xmlCopyDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlCopyDocElementContent */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCopyDocElementContent */
/*-no- ........................................................................................................................ */
/*-no- #define xmlCopyDocElementContent xmlCopyDocElementContent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlCopyDtd */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCopyDtd */
/*-no- ............................................................................................ */
/*-no- #define xmlCopyDtd xmlCopyDtd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlCopyElementContent */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCopyElementContent */
/*-no- .................................................................................................................. */
/*-no- #define xmlCopyElementContent xmlCopyElementContent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlCopyElementTable */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCopyElementTable */
/*-no- .............................................................................................................. */
/*-no- #define xmlCopyElementTable xmlCopyElementTable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_entities */
/*-no- #undef xmlCopyEntitiesTable */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCopyEntitiesTable */
/*-no- ................................................................................................................ */
/*-no- #define xmlCopyEntitiesTable xmlCopyEntitiesTable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlCopyEnumeration */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCopyEnumeration */
/*-no- ............................................................................................................ */
/*-no- #define xmlCopyEnumeration xmlCopyEnumeration__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_error */
/*-no- #undef xmlCopyError */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCopyError */
/*-no- ................................................................................................ */
/*-no- #define xmlCopyError xmlCopyError__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlCopyNamespace */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCopyNamespace */
/*-no- ........................................................................................................ */
/*-no- #define xmlCopyNamespace xmlCopyNamespace__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlCopyNamespaceList */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCopyNamespaceList */
/*-no- ................................................................................................................ */
/*-no- #define xmlCopyNamespaceList xmlCopyNamespaceList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlCopyNode */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCopyNode */
/*-no- .............................................................................................. */
/*-no- #define xmlCopyNode xmlCopyNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlCopyNodeList */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCopyNodeList */
/*-no- ...................................................................................................... */
/*-no- #define xmlCopyNodeList xmlCopyNodeList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlCopyNotationTable */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCopyNotationTable */
/*-no- ................................................................................................................ */
/*-no- #define xmlCopyNotationTable xmlCopyNotationTable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlCopyProp */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCopyProp */
/*-no- .............................................................................................. */
/*-no- #define xmlCopyProp xmlCopyProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlCopyPropList */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCopyPropList */
/*-no- ...................................................................................................... */
/*-no- #define xmlCopyPropList xmlCopyPropList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCreateDocParserCtxt */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCreateDocParserCtxt */
/*-no- .................................................................................................................... */
/*-no- #define xmlCreateDocParserCtxt xmlCreateDocParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_entities */
/*-no- #undef xmlCreateEntitiesTable */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCreateEntitiesTable */
/*-no- .................................................................................................................... */
/*-no- #define xmlCreateEntitiesTable xmlCreateEntitiesTable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCreateEntityParserCtxt */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCreateEntityParserCtxt */
/*-no- .......................................................................................................................... */
/*-no- #define xmlCreateEntityParserCtxt xmlCreateEntityParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlCreateEnumeration */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCreateEnumeration */
/*-no- ................................................................................................................ */
/*-no- #define xmlCreateEnumeration xmlCreateEnumeration__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCreateFileParserCtxt */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCreateFileParserCtxt */
/*-no- ...................................................................................................................... */
/*-no- #define xmlCreateFileParserCtxt xmlCreateFileParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCreateIOParserCtxt */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCreateIOParserCtxt */
/*-no- .................................................................................................................. */
/*-no- #define xmlCreateIOParserCtxt xmlCreateIOParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlCreateIntSubset */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCreateIntSubset */
/*-no- ............................................................................................................ */
/*-no- #define xmlCreateIntSubset xmlCreateIntSubset__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCreateMemoryParserCtxt */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCreateMemoryParserCtxt */
/*-no- .......................................................................................................................... */
/*-no- #define xmlCreateMemoryParserCtxt xmlCreateMemoryParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PUSH_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCreatePushParserCtxt */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCreatePushParserCtxt */
/*-no- ...................................................................................................................... */
/*-no- #define xmlCreatePushParserCtxt xmlCreatePushParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_uri */
/*-no- #undef xmlCreateURI */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCreateURI */
/*-no- ................................................................................................ */
/*-no- #define xmlCreateURI xmlCreateURI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCreateURLParserCtxt */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCreateURLParserCtxt */
/*-no- .................................................................................................................... */
/*-no- #define xmlCreateURLParserCtxt xmlCreateURLParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_error */
/*-no- #undef xmlCtxtGetLastError */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCtxtGetLastError */
/*-no- .............................................................................................................. */
/*-no- #define xmlCtxtGetLastError xmlCtxtGetLastError__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCtxtReadDoc */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCtxtReadDoc */
/*-no- .................................................................................................... */
/*-no- #define xmlCtxtReadDoc xmlCtxtReadDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCtxtReadFd */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCtxtReadFd */
/*-no- .................................................................................................. */
/*-no- #define xmlCtxtReadFd xmlCtxtReadFd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCtxtReadFile */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCtxtReadFile */
/*-no- ...................................................................................................... */
/*-no- #define xmlCtxtReadFile xmlCtxtReadFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCtxtReadIO */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCtxtReadIO */
/*-no- .................................................................................................. */
/*-no- #define xmlCtxtReadIO xmlCtxtReadIO__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCtxtReadMemory */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCtxtReadMemory */
/*-no- .......................................................................................................... */
/*-no- #define xmlCtxtReadMemory xmlCtxtReadMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCtxtReset */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCtxtReset */
/*-no- ................................................................................................ */
/*-no- #define xmlCtxtReset xmlCtxtReset__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_error */
/*-no- #undef xmlCtxtResetLastError */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCtxtResetLastError */
/*-no- .................................................................................................................. */
/*-no- #define xmlCtxtResetLastError xmlCtxtResetLastError__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCtxtResetPush */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlCtxtResetPush */
/*-no- ........................................................................................................ */
/*-no- #define xmlCtxtResetPush xmlCtxtResetPush__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlCtxtUseOptions */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlCtxtUseOptions */
/*-no- .......................................................................................................... */
/*-no- #define xmlCtxtUseOptions xmlCtxtUseOptions__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlCurrentChar */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlCurrentChar */
/*-no- .................................................................................................... */
/*-no- #define xmlCurrentChar xmlCurrentChar__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlDOMWrapAdoptNode */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDOMWrapAdoptNode */
/*-no- .............................................................................................................. */
/*-no- #define xmlDOMWrapAdoptNode xmlDOMWrapAdoptNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlDOMWrapCloneNode */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDOMWrapCloneNode */
/*-no- .............................................................................................................. */
/*-no- #define xmlDOMWrapCloneNode xmlDOMWrapCloneNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlDOMWrapFreeCtxt */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDOMWrapFreeCtxt */
/*-no- ............................................................................................................ */
/*-no- #define xmlDOMWrapFreeCtxt xmlDOMWrapFreeCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlDOMWrapNewCtxt */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlDOMWrapNewCtxt */
/*-no- .......................................................................................................... */
/*-no- #define xmlDOMWrapNewCtxt xmlDOMWrapNewCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlDOMWrapReconcileNamespaces */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDOMWrapReconcileNamespaces */
/*-no- .................................................................................................................................. */
/*-no- #define xmlDOMWrapReconcileNamespaces xmlDOMWrapReconcileNamespaces__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlDOMWrapRemoveNode */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDOMWrapRemoveNode */
/*-no- ................................................................................................................ */
/*-no- #define xmlDOMWrapRemoveNode xmlDOMWrapRemoveNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlDebugCheckDocument */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlDebugCheckDocument */
/*-no- .................................................................................................................. */
/*-no- #define xmlDebugCheckDocument xmlDebugCheckDocument__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlDebugDumpAttr */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlDebugDumpAttr */
/*-no- ........................................................................................................ */
/*-no- #define xmlDebugDumpAttr xmlDebugDumpAttr__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlDebugDumpAttrList */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDebugDumpAttrList */
/*-no- ................................................................................................................ */
/*-no- #define xmlDebugDumpAttrList xmlDebugDumpAttrList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlDebugDumpDTD */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDebugDumpDTD */
/*-no- ...................................................................................................... */
/*-no- #define xmlDebugDumpDTD xmlDebugDumpDTD__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlDebugDumpDocument */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDebugDumpDocument */
/*-no- ................................................................................................................ */
/*-no- #define xmlDebugDumpDocument xmlDebugDumpDocument__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlDebugDumpDocumentHead */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDebugDumpDocumentHead */
/*-no- ........................................................................................................................ */
/*-no- #define xmlDebugDumpDocumentHead xmlDebugDumpDocumentHead__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlDebugDumpEntities */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDebugDumpEntities */
/*-no- ................................................................................................................ */
/*-no- #define xmlDebugDumpEntities xmlDebugDumpEntities__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlDebugDumpNode */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlDebugDumpNode */
/*-no- ........................................................................................................ */
/*-no- #define xmlDebugDumpNode xmlDebugDumpNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlDebugDumpNodeList */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDebugDumpNodeList */
/*-no- ................................................................................................................ */
/*-no- #define xmlDebugDumpNodeList xmlDebugDumpNodeList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlDebugDumpOneNode */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDebugDumpOneNode */
/*-no- .............................................................................................................. */
/*-no- #define xmlDebugDumpOneNode xmlDebugDumpOneNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlDebugDumpString */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDebugDumpString */
/*-no- ............................................................................................................ */
/*-no- #define xmlDebugDumpString xmlDebugDumpString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlDecodeEntities */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlDecodeEntities */
/*-no- .......................................................................................................... */
/*-no- #define xmlDecodeEntities xmlDecodeEntities__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlDefaultSAXHandlerInit */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDefaultSAXHandlerInit */
/*-no- ........................................................................................................................ */
/*-no- #define xmlDefaultSAXHandlerInit xmlDefaultSAXHandlerInit__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlDelEncodingAlias */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDelEncodingAlias */
/*-no- .............................................................................................................. */
/*-no- #define xmlDelEncodingAlias xmlDelEncodingAlias__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlDeregisterNodeDefault */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDeregisterNodeDefault */
/*-no- ........................................................................................................................ */
/*-no- #define xmlDeregisterNodeDefault xmlDeregisterNodeDefault__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlDetectCharEncoding */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlDetectCharEncoding */
/*-no- .................................................................................................................. */
/*-no- #define xmlDetectCharEncoding xmlDetectCharEncoding__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_dict */
/*-no- #undef xmlDictCleanup */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDictCleanup */
/*-no- .................................................................................................... */
/*-no- #define xmlDictCleanup xmlDictCleanup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_dict */
/*-no- #undef xmlDictCreate */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDictCreate */
/*-no- .................................................................................................. */
/*-no- #define xmlDictCreate xmlDictCreate__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_dict */
/*-no- #undef xmlDictCreateSub */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlDictCreateSub */
/*-no- ........................................................................................................ */
/*-no- #define xmlDictCreateSub xmlDictCreateSub__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_dict */
/*-no- #undef xmlDictExists */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDictExists */
/*-no- .................................................................................................. */
/*-no- #define xmlDictExists xmlDictExists__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_dict */
/*-no- #undef xmlDictFree */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlDictFree */
/*-no- .............................................................................................. */
/*-no- #define xmlDictFree xmlDictFree__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_dict */
/*-no- #undef xmlDictLookup */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDictLookup */
/*-no- .................................................................................................. */
/*-no- #define xmlDictLookup xmlDictLookup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_dict */
/*-no- #undef xmlDictOwns */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlDictOwns */
/*-no- .............................................................................................. */
/*-no- #define xmlDictOwns xmlDictOwns__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_dict */
/*-no- #undef xmlDictQLookup */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDictQLookup */
/*-no- .................................................................................................... */
/*-no- #define xmlDictQLookup xmlDictQLookup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_dict */
/*-no- #undef xmlDictReference */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlDictReference */
/*-no- ........................................................................................................ */
/*-no- #define xmlDictReference xmlDictReference__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_dict */
/*-no- #undef xmlDictSize */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlDictSize */
/*-no- .............................................................................................. */
/*-no- #define xmlDictSize xmlDictSize__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlDocCopyNode */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDocCopyNode */
/*-no- .................................................................................................... */
/*-no- #define xmlDocCopyNode xmlDocCopyNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlDocCopyNodeList */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDocCopyNodeList */
/*-no- ............................................................................................................ */
/*-no- #define xmlDocCopyNodeList xmlDocCopyNodeList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlDocDump */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDocDump */
/*-no- ............................................................................................ */
/*-no- #define xmlDocDump xmlDocDump__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlDocDumpFormatMemory */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDocDumpFormatMemory */
/*-no- .................................................................................................................... */
/*-no- #define xmlDocDumpFormatMemory xmlDocDumpFormatMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlDocDumpFormatMemoryEnc */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlDocDumpFormatMemoryEnc */
/*-no- .......................................................................................................................... */
/*-no- #define xmlDocDumpFormatMemoryEnc xmlDocDumpFormatMemoryEnc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlDocDumpMemory */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlDocDumpMemory */
/*-no- ........................................................................................................ */
/*-no- #define xmlDocDumpMemory xmlDocDumpMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlDocDumpMemoryEnc */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDocDumpMemoryEnc */
/*-no- .............................................................................................................. */
/*-no- #define xmlDocDumpMemoryEnc xmlDocDumpMemoryEnc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlDocFormatDump */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlDocFormatDump */
/*-no- ........................................................................................................ */
/*-no- #define xmlDocFormatDump xmlDocFormatDump__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlDocGetRootElement */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDocGetRootElement */
/*-no- ................................................................................................................ */
/*-no- #define xmlDocGetRootElement xmlDocGetRootElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlDocSetRootElement */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDocSetRootElement */
/*-no- ................................................................................................................ */
/*-no- #define xmlDocSetRootElement xmlDocSetRootElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlDumpAttributeDecl */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDumpAttributeDecl */
/*-no- ................................................................................................................ */
/*-no- #define xmlDumpAttributeDecl xmlDumpAttributeDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlDumpAttributeTable */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlDumpAttributeTable */
/*-no- .................................................................................................................. */
/*-no- #define xmlDumpAttributeTable xmlDumpAttributeTable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlDumpElementDecl */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDumpElementDecl */
/*-no- ............................................................................................................ */
/*-no- #define xmlDumpElementDecl xmlDumpElementDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlDumpElementTable */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDumpElementTable */
/*-no- .............................................................................................................. */
/*-no- #define xmlDumpElementTable xmlDumpElementTable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_entities */
/*-no- #undef xmlDumpEntitiesTable */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDumpEntitiesTable */
/*-no- ................................................................................................................ */
/*-no- #define xmlDumpEntitiesTable xmlDumpEntitiesTable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_entities */
/*-no- #undef xmlDumpEntityDecl */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlDumpEntityDecl */
/*-no- .......................................................................................................... */
/*-no- #define xmlDumpEntityDecl xmlDumpEntityDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlDumpNotationDecl */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDumpNotationDecl */
/*-no- .............................................................................................................. */
/*-no- #define xmlDumpNotationDecl xmlDumpNotationDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlDumpNotationTable */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlDumpNotationTable */
/*-no- ................................................................................................................ */
/*-no- #define xmlDumpNotationTable xmlDumpNotationTable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlElemDump */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlElemDump */
/*-no- .............................................................................................. */
/*-no- #define xmlElemDump xmlElemDump__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlEncodeEntities */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlEncodeEntities */
/*-no- .......................................................................................................... */
/*-no- #define xmlEncodeEntities xmlEncodeEntities__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_entities */
/*-no- #undef xmlEncodeEntitiesReentrant */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlEncodeEntitiesReentrant */
/*-no- ............................................................................................................................ */
/*-no- #define xmlEncodeEntitiesReentrant xmlEncodeEntitiesReentrant__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_entities */
/*-no- #undef xmlEncodeSpecialChars */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlEncodeSpecialChars */
/*-no- .................................................................................................................. */
/*-no- #define xmlEncodeSpecialChars xmlEncodeSpecialChars__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlErrMemory */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlErrMemory */
/*-no- ................................................................................................ */
/*-no- #define xmlErrMemory xmlErrMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpCtxtNbCons */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlExpCtxtNbCons */
/*-no- ........................................................................................................ */
/*-no- #define xmlExpCtxtNbCons xmlExpCtxtNbCons__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpCtxtNbNodes */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlExpCtxtNbNodes */
/*-no- .......................................................................................................... */
/*-no- #define xmlExpCtxtNbNodes xmlExpCtxtNbNodes__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpDump */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlExpDump */
/*-no- ............................................................................................ */
/*-no- #define xmlExpDump xmlExpDump__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpExpDerive */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlExpExpDerive */
/*-no- ...................................................................................................... */
/*-no- #define xmlExpExpDerive xmlExpExpDerive__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpFree */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlExpFree */
/*-no- ............................................................................................ */
/*-no- #define xmlExpFree xmlExpFree__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpFreeCtxt */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlExpFreeCtxt */
/*-no- .................................................................................................... */
/*-no- #define xmlExpFreeCtxt xmlExpFreeCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpGetLanguage */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlExpGetLanguage */
/*-no- .......................................................................................................... */
/*-no- #define xmlExpGetLanguage xmlExpGetLanguage__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpGetStart */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlExpGetStart */
/*-no- .................................................................................................... */
/*-no- #define xmlExpGetStart xmlExpGetStart__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpIsNillable */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlExpIsNillable */
/*-no- ........................................................................................................ */
/*-no- #define xmlExpIsNillable xmlExpIsNillable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpMaxToken */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlExpMaxToken */
/*-no- .................................................................................................... */
/*-no- #define xmlExpMaxToken xmlExpMaxToken__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpNewAtom */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlExpNewAtom */
/*-no- .................................................................................................. */
/*-no- #define xmlExpNewAtom xmlExpNewAtom__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpNewCtxt */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlExpNewCtxt */
/*-no- .................................................................................................. */
/*-no- #define xmlExpNewCtxt xmlExpNewCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpNewOr */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlExpNewOr */
/*-no- .............................................................................................. */
/*-no- #define xmlExpNewOr xmlExpNewOr__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpNewRange */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlExpNewRange */
/*-no- .................................................................................................... */
/*-no- #define xmlExpNewRange xmlExpNewRange__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpNewSeq */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlExpNewSeq */
/*-no- ................................................................................................ */
/*-no- #define xmlExpNewSeq xmlExpNewSeq__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpParse */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlExpParse */
/*-no- .............................................................................................. */
/*-no- #define xmlExpParse xmlExpParse__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpRef */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlExpRef */
/*-no- .......................................................................................... */
/*-no- #define xmlExpRef xmlExpRef__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpStringDerive */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlExpStringDerive */
/*-no- ............................................................................................................ */
/*-no- #define xmlExpStringDerive xmlExpStringDerive__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_EXPR_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlExpSubsume */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlExpSubsume */
/*-no- .................................................................................................. */
/*-no- #define xmlExpSubsume xmlExpSubsume__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlFileClose */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlFileClose */
/*-no- ................................................................................................ */
/*-no- #define xmlFileClose xmlFileClose__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlFileMatch */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlFileMatch */
/*-no- ................................................................................................ */
/*-no- #define xmlFileMatch xmlFileMatch__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlFileOpen */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlFileOpen */
/*-no- .............................................................................................. */
/*-no- #define xmlFileOpen xmlFileOpen__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlFileRead */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlFileRead */
/*-no- .............................................................................................. */
/*-no- #define xmlFileRead xmlFileRead__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlFindCharEncodingHandler */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFindCharEncodingHandler */
/*-no- ............................................................................................................................ */
/*-no- #define xmlFindCharEncodingHandler xmlFindCharEncodingHandler__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlFirstElementChild */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFirstElementChild */
/*-no- ................................................................................................................ */
/*-no- #define xmlFirstElementChild xmlFirstElementChild__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlFreeAttributeTable */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlFreeAttributeTable */
/*-no- .................................................................................................................. */
/*-no- #define xmlFreeAttributeTable xmlFreeAttributeTable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlFreeAutomata */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeAutomata */
/*-no- ...................................................................................................... */
/*-no- #define xmlFreeAutomata xmlFreeAutomata__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlFreeCatalog */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeCatalog */
/*-no- .................................................................................................... */
/*-no- #define xmlFreeCatalog xmlFreeCatalog__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlFreeDoc */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeDoc */
/*-no- ............................................................................................ */
/*-no- #define xmlFreeDoc xmlFreeDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlFreeDocElementContent */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeDocElementContent */
/*-no- ........................................................................................................................ */
/*-no- #define xmlFreeDocElementContent xmlFreeDocElementContent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlFreeDtd */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeDtd */
/*-no- ............................................................................................ */
/*-no- #define xmlFreeDtd xmlFreeDtd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlFreeElementContent */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlFreeElementContent */
/*-no- .................................................................................................................. */
/*-no- #define xmlFreeElementContent xmlFreeElementContent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlFreeElementTable */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeElementTable */
/*-no- .............................................................................................................. */
/*-no- #define xmlFreeElementTable xmlFreeElementTable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_entities */
/*-no- #undef xmlFreeEntitiesTable */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeEntitiesTable */
/*-no- ................................................................................................................ */
/*-no- #define xmlFreeEntitiesTable xmlFreeEntitiesTable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlFreeEnumeration */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeEnumeration */
/*-no- ............................................................................................................ */
/*-no- #define xmlFreeEnumeration xmlFreeEnumeration__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlFreeIDTable */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeIDTable */
/*-no- .................................................................................................... */
/*-no- #define xmlFreeIDTable xmlFreeIDTable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlFreeInputStream */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeInputStream */
/*-no- ............................................................................................................ */
/*-no- #define xmlFreeInputStream xmlFreeInputStream__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_threads */
/*-no- #undef xmlFreeMutex */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlFreeMutex */
/*-no- ................................................................................................ */
/*-no- #define xmlFreeMutex xmlFreeMutex__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlFreeNode */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlFreeNode */
/*-no- .............................................................................................. */
/*-no- #define xmlFreeNode xmlFreeNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlFreeNodeList */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeNodeList */
/*-no- ...................................................................................................... */
/*-no- #define xmlFreeNodeList xmlFreeNodeList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlFreeNotationTable */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeNotationTable */
/*-no- ................................................................................................................ */
/*-no- #define xmlFreeNotationTable xmlFreeNotationTable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlFreeNs */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlFreeNs */
/*-no- .......................................................................................... */
/*-no- #define xmlFreeNs xmlFreeNs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlFreeNsList */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeNsList */
/*-no- .................................................................................................. */
/*-no- #define xmlFreeNsList xmlFreeNsList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlFreeParserCtxt */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlFreeParserCtxt */
/*-no- .......................................................................................................... */
/*-no- #define xmlFreeParserCtxt xmlFreeParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlFreeParserInputBuffer */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeParserInputBuffer */
/*-no- ........................................................................................................................ */
/*-no- #define xmlFreeParserInputBuffer xmlFreeParserInputBuffer__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_pattern */
/*-no- #undef xmlFreePattern */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreePattern */
/*-no- .................................................................................................... */
/*-no- #define xmlFreePattern xmlFreePattern__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_pattern */
/*-no- #undef xmlFreePatternList */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreePatternList */
/*-no- ............................................................................................................ */
/*-no- #define xmlFreePatternList xmlFreePatternList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlFreeProp */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlFreeProp */
/*-no- .............................................................................................. */
/*-no- #define xmlFreeProp xmlFreeProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlFreePropList */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreePropList */
/*-no- ...................................................................................................... */
/*-no- #define xmlFreePropList xmlFreePropList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_threads */
/*-no- #undef xmlFreeRMutex */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeRMutex */
/*-no- .................................................................................................. */
/*-no- #define xmlFreeRMutex xmlFreeRMutex__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlFreeRefTable */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeRefTable */
/*-no- ...................................................................................................... */
/*-no- #define xmlFreeRefTable xmlFreeRefTable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_pattern */
/*-no- #undef xmlFreeStreamCtxt */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlFreeStreamCtxt */
/*-no- .......................................................................................................... */
/*-no- #define xmlFreeStreamCtxt xmlFreeStreamCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlFreeTextReader */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlFreeTextReader */
/*-no- .......................................................................................................... */
/*-no- #define xmlFreeTextReader xmlFreeTextReader__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlFreeTextWriter */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlFreeTextWriter */
/*-no- .......................................................................................................... */
/*-no- #define xmlFreeTextWriter xmlFreeTextWriter__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_uri */
/*-no- #undef xmlFreeURI */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlFreeURI */
/*-no- ............................................................................................ */
/*-no- #define xmlFreeURI xmlFreeURI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlFreeValidCtxt */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlFreeValidCtxt */
/*-no- ........................................................................................................ */
/*-no- #define xmlFreeValidCtxt xmlFreeValidCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlGcMemGet */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlGcMemGet */
/*-no- .............................................................................................. */
/*-no- #define xmlGcMemGet xmlGcMemGet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlGcMemSetup */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGcMemSetup */
/*-no- .................................................................................................. */
/*-no- #define xmlGcMemSetup xmlGcMemSetup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlGetBufferAllocationScheme */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlGetBufferAllocationScheme */
/*-no- ................................................................................................................................ */
/*-no- #define xmlGetBufferAllocationScheme xmlGetBufferAllocationScheme__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlGetCharEncodingHandler */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlGetCharEncodingHandler */
/*-no- .......................................................................................................................... */
/*-no- #define xmlGetCharEncodingHandler xmlGetCharEncodingHandler__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlGetCharEncodingName */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetCharEncodingName */
/*-no- .................................................................................................................... */
/*-no- #define xmlGetCharEncodingName xmlGetCharEncodingName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlGetCompressMode */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetCompressMode */
/*-no- ............................................................................................................ */
/*-no- #define xmlGetCompressMode xmlGetCompressMode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlGetDocCompressMode */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlGetDocCompressMode */
/*-no- .................................................................................................................. */
/*-no- #define xmlGetDocCompressMode xmlGetDocCompressMode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_entities */
/*-no- #undef xmlGetDocEntity */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetDocEntity */
/*-no- ...................................................................................................... */
/*-no- #define xmlGetDocEntity xmlGetDocEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlGetDtdAttrDesc */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlGetDtdAttrDesc */
/*-no- .......................................................................................................... */
/*-no- #define xmlGetDtdAttrDesc xmlGetDtdAttrDesc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlGetDtdElementDesc */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetDtdElementDesc */
/*-no- ................................................................................................................ */
/*-no- #define xmlGetDtdElementDesc xmlGetDtdElementDesc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_entities */
/*-no- #undef xmlGetDtdEntity */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetDtdEntity */
/*-no- ...................................................................................................... */
/*-no- #define xmlGetDtdEntity xmlGetDtdEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlGetDtdNotationDesc */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlGetDtdNotationDesc */
/*-no- .................................................................................................................. */
/*-no- #define xmlGetDtdNotationDesc xmlGetDtdNotationDesc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlGetDtdQAttrDesc */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetDtdQAttrDesc */
/*-no- ............................................................................................................ */
/*-no- #define xmlGetDtdQAttrDesc xmlGetDtdQAttrDesc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlGetDtdQElementDesc */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlGetDtdQElementDesc */
/*-no- .................................................................................................................. */
/*-no- #define xmlGetDtdQElementDesc xmlGetDtdQElementDesc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlGetEncodingAlias */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetEncodingAlias */
/*-no- .............................................................................................................. */
/*-no- #define xmlGetEncodingAlias xmlGetEncodingAlias__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlGetExternalEntityLoader */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetExternalEntityLoader */
/*-no- ............................................................................................................................ */
/*-no- #define xmlGetExternalEntityLoader xmlGetExternalEntityLoader__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlGetFeature */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetFeature */
/*-no- .................................................................................................. */
/*-no- #define xmlGetFeature xmlGetFeature__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlGetFeaturesList */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetFeaturesList */
/*-no- ............................................................................................................ */
/*-no- #define xmlGetFeaturesList xmlGetFeaturesList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_threads */
/*-no- #undef xmlGetGlobalState */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlGetGlobalState */
/*-no- .......................................................................................................... */
/*-no- #define xmlGetGlobalState xmlGetGlobalState__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlGetID */
/*-no- ..................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetID */
/*-no- ........................................................................................ */
/*-no- #define xmlGetID xmlGetID__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlGetIntSubset */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetIntSubset */
/*-no- ...................................................................................................... */
/*-no- #define xmlGetIntSubset xmlGetIntSubset__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlGetLastChild */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetLastChild */
/*-no- ...................................................................................................... */
/*-no- #define xmlGetLastChild xmlGetLastChild__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_error */
/*-no- #undef xmlGetLastError */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetLastError */
/*-no- ...................................................................................................... */
/*-no- #define xmlGetLastError xmlGetLastError__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlGetLineNo */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlGetLineNo */
/*-no- ................................................................................................ */
/*-no- #define xmlGetLineNo xmlGetLineNo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlGetNoNsProp */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetNoNsProp */
/*-no- .................................................................................................... */
/*-no- #define xmlGetNoNsProp xmlGetNoNsProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlGetNodePath */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetNodePath */
/*-no- .................................................................................................... */
/*-no- #define xmlGetNodePath xmlGetNodePath__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_XPATH_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlGetNsList */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlGetNsList */
/*-no- ................................................................................................ */
/*-no- #define xmlGetNsList xmlGetNsList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlGetNsProp */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlGetNsProp */
/*-no- ................................................................................................ */
/*-no- #define xmlGetNsProp xmlGetNsProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_entities */
/*-no- #undef xmlGetParameterEntity */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlGetParameterEntity */
/*-no- .................................................................................................................. */
/*-no- #define xmlGetParameterEntity xmlGetParameterEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_entities */
/*-no- #undef xmlGetPredefinedEntity */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetPredefinedEntity */
/*-no- .................................................................................................................... */
/*-no- #define xmlGetPredefinedEntity xmlGetPredefinedEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlGetProp */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetProp */
/*-no- ............................................................................................ */
/*-no- #define xmlGetProp xmlGetProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlGetRefs */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetRefs */
/*-no- ............................................................................................ */
/*-no- #define xmlGetRefs xmlGetRefs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_threads */
/*-no- #undef xmlGetThreadId */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetThreadId */
/*-no- .................................................................................................... */
/*-no- #define xmlGetThreadId xmlGetThreadId__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlGetUTF8Char */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlGetUTF8Char */
/*-no- .................................................................................................... */
/*-no- #define xmlGetUTF8Char xmlGetUTF8Char__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlHandleEntity */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHandleEntity */
/*-no- ...................................................................................................... */
/*-no- #define xmlHandleEntity xmlHandleEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlHasFeature */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHasFeature */
/*-no- .................................................................................................. */
/*-no- #define xmlHasFeature xmlHasFeature__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlHasNsProp */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlHasNsProp */
/*-no- ................................................................................................ */
/*-no- #define xmlHasNsProp xmlHasNsProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlHasProp */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHasProp */
/*-no- ............................................................................................ */
/*-no- #define xmlHasProp xmlHasProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashAddEntry */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHashAddEntry */
/*-no- ...................................................................................................... */
/*-no- #define xmlHashAddEntry xmlHashAddEntry__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashAddEntry2 */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlHashAddEntry2 */
/*-no- ........................................................................................................ */
/*-no- #define xmlHashAddEntry2 xmlHashAddEntry2__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashAddEntry3 */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlHashAddEntry3 */
/*-no- ........................................................................................................ */
/*-no- #define xmlHashAddEntry3 xmlHashAddEntry3__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashCopy */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlHashCopy */
/*-no- .............................................................................................. */
/*-no- #define xmlHashCopy xmlHashCopy__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashCreate */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHashCreate */
/*-no- .................................................................................................. */
/*-no- #define xmlHashCreate xmlHashCreate__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashCreateDict */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlHashCreateDict */
/*-no- .......................................................................................................... */
/*-no- #define xmlHashCreateDict xmlHashCreateDict__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashFree */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlHashFree */
/*-no- .............................................................................................. */
/*-no- #define xmlHashFree xmlHashFree__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashLookup */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHashLookup */
/*-no- .................................................................................................. */
/*-no- #define xmlHashLookup xmlHashLookup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashLookup2 */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHashLookup2 */
/*-no- .................................................................................................... */
/*-no- #define xmlHashLookup2 xmlHashLookup2__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashLookup3 */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHashLookup3 */
/*-no- .................................................................................................... */
/*-no- #define xmlHashLookup3 xmlHashLookup3__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashQLookup */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHashQLookup */
/*-no- .................................................................................................... */
/*-no- #define xmlHashQLookup xmlHashQLookup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashQLookup2 */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHashQLookup2 */
/*-no- ...................................................................................................... */
/*-no- #define xmlHashQLookup2 xmlHashQLookup2__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashQLookup3 */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHashQLookup3 */
/*-no- ...................................................................................................... */
/*-no- #define xmlHashQLookup3 xmlHashQLookup3__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashRemoveEntry */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHashRemoveEntry */
/*-no- ............................................................................................................ */
/*-no- #define xmlHashRemoveEntry xmlHashRemoveEntry__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashRemoveEntry2 */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHashRemoveEntry2 */
/*-no- .............................................................................................................. */
/*-no- #define xmlHashRemoveEntry2 xmlHashRemoveEntry2__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashRemoveEntry3 */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHashRemoveEntry3 */
/*-no- .............................................................................................................. */
/*-no- #define xmlHashRemoveEntry3 xmlHashRemoveEntry3__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashScan */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlHashScan */
/*-no- .............................................................................................. */
/*-no- #define xmlHashScan xmlHashScan__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashScan3 */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlHashScan3 */
/*-no- ................................................................................................ */
/*-no- #define xmlHashScan3 xmlHashScan3__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashScanFull */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHashScanFull */
/*-no- ...................................................................................................... */
/*-no- #define xmlHashScanFull xmlHashScanFull__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashScanFull3 */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlHashScanFull3 */
/*-no- ........................................................................................................ */
/*-no- #define xmlHashScanFull3 xmlHashScanFull3__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashSize */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlHashSize */
/*-no- .............................................................................................. */
/*-no- #define xmlHashSize xmlHashSize__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashUpdateEntry */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHashUpdateEntry */
/*-no- ............................................................................................................ */
/*-no- #define xmlHashUpdateEntry xmlHashUpdateEntry__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashUpdateEntry2 */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHashUpdateEntry2 */
/*-no- .............................................................................................................. */
/*-no- #define xmlHashUpdateEntry2 xmlHashUpdateEntry2__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_hash */
/*-no- #undef xmlHashUpdateEntry3 */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlHashUpdateEntry3 */
/*-no- .............................................................................................................. */
/*-no- #define xmlHashUpdateEntry3 xmlHashUpdateEntry3__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlIOFTPClose */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIOFTPClose */
/*-no- .................................................................................................. */
/*-no- #define xmlIOFTPClose xmlIOFTPClose__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlIOFTPMatch */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIOFTPMatch */
/*-no- .................................................................................................. */
/*-no- #define xmlIOFTPMatch xmlIOFTPMatch__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlIOFTPOpen */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlIOFTPOpen */
/*-no- ................................................................................................ */
/*-no- #define xmlIOFTPOpen xmlIOFTPOpen__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlIOFTPRead */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlIOFTPRead */
/*-no- ................................................................................................ */
/*-no- #define xmlIOFTPRead xmlIOFTPRead__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlIOHTTPClose */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIOHTTPClose */
/*-no- .................................................................................................... */
/*-no- #define xmlIOHTTPClose xmlIOHTTPClose__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlIOHTTPMatch */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIOHTTPMatch */
/*-no- .................................................................................................... */
/*-no- #define xmlIOHTTPMatch xmlIOHTTPMatch__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlIOHTTPOpen */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIOHTTPOpen */
/*-no- .................................................................................................. */
/*-no- #define xmlIOHTTPOpen xmlIOHTTPOpen__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlIOHTTPOpenW */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIOHTTPOpenW */
/*-no- .................................................................................................... */
/*-no- #define xmlIOHTTPOpenW xmlIOHTTPOpenW__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlIOHTTPRead */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIOHTTPRead */
/*-no- .................................................................................................. */
/*-no- #define xmlIOHTTPRead xmlIOHTTPRead__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlIOParseDTD */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIOParseDTD */
/*-no- .................................................................................................. */
/*-no- #define xmlIOParseDTD xmlIOParseDTD__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlInitCharEncodingHandlers */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlInitCharEncodingHandlers */
/*-no- .............................................................................................................................. */
/*-no- #define xmlInitCharEncodingHandlers xmlInitCharEncodingHandlers__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlInitGlobals */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlInitGlobals */
/*-no- .................................................................................................... */
/*-no- #define xmlInitGlobals xmlInitGlobals__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlInitMemory */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlInitMemory */
/*-no- .................................................................................................. */
/*-no- #define xmlInitMemory xmlInitMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlInitNodeInfoSeq */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlInitNodeInfoSeq */
/*-no- ............................................................................................................ */
/*-no- #define xmlInitNodeInfoSeq xmlInitNodeInfoSeq__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlInitParser */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlInitParser */
/*-no- .................................................................................................. */
/*-no- #define xmlInitParser xmlInitParser__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlInitParserCtxt */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlInitParserCtxt */
/*-no- .......................................................................................................... */
/*-no- #define xmlInitParserCtxt xmlInitParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_threads */
/*-no- #undef xmlInitThreads */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlInitThreads */
/*-no- .................................................................................................... */
/*-no- #define xmlInitThreads xmlInitThreads__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlInitializeCatalog */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlInitializeCatalog */
/*-no- ................................................................................................................ */
/*-no- #define xmlInitializeCatalog xmlInitializeCatalog__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlInitializeGlobalState */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlInitializeGlobalState */
/*-no- ........................................................................................................................ */
/*-no- #define xmlInitializeGlobalState xmlInitializeGlobalState__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlInitializePredefinedEntities */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlInitializePredefinedEntities */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlInitializePredefinedEntities xmlInitializePredefinedEntities__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_chvalid */
/*-no- #undef xmlIsBaseChar */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIsBaseChar */
/*-no- .................................................................................................. */
/*-no- #define xmlIsBaseChar xmlIsBaseChar__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_chvalid */
/*-no- #undef xmlIsBlank */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIsBlank */
/*-no- ............................................................................................ */
/*-no- #define xmlIsBlank xmlIsBlank__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlIsBlankNode */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIsBlankNode */
/*-no- .................................................................................................... */
/*-no- #define xmlIsBlankNode xmlIsBlankNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_chvalid */
/*-no- #undef xmlIsChar */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlIsChar */
/*-no- .......................................................................................... */
/*-no- #define xmlIsChar xmlIsChar__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_chvalid */
/*-no- #undef xmlIsCombining */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIsCombining */
/*-no- .................................................................................................... */
/*-no- #define xmlIsCombining xmlIsCombining__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_chvalid */
/*-no- #undef xmlIsDigit */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIsDigit */
/*-no- ............................................................................................ */
/*-no- #define xmlIsDigit xmlIsDigit__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_chvalid */
/*-no- #undef xmlIsExtender */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIsExtender */
/*-no- .................................................................................................. */
/*-no- #define xmlIsExtender xmlIsExtender__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlIsID */
/*-no- .................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlIsID */
/*-no- ...................................................................................... */
/*-no- #define xmlIsID xmlIsID__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_chvalid */
/*-no- #undef xmlIsIdeographic */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlIsIdeographic */
/*-no- ........................................................................................................ */
/*-no- #define xmlIsIdeographic xmlIsIdeographic__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlIsLetter */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlIsLetter */
/*-no- .............................................................................................. */
/*-no- #define xmlIsLetter xmlIsLetter__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_threads */
/*-no- #undef xmlIsMainThread */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIsMainThread */
/*-no- ...................................................................................................... */
/*-no- #define xmlIsMainThread xmlIsMainThread__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlIsMixedElement */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlIsMixedElement */
/*-no- .......................................................................................................... */
/*-no- #define xmlIsMixedElement xmlIsMixedElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_chvalid */
/*-no- #undef xmlIsPubidChar */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIsPubidChar */
/*-no- .................................................................................................... */
/*-no- #define xmlIsPubidChar xmlIsPubidChar__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlIsRef */
/*-no- ..................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIsRef */
/*-no- ........................................................................................ */
/*-no- #define xmlIsRef xmlIsRef__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlIsXHTML */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlIsXHTML */
/*-no- ............................................................................................ */
/*-no- #define xmlIsXHTML xmlIsXHTML__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlKeepBlanksDefault */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlKeepBlanksDefault */
/*-no- ................................................................................................................ */
/*-no- #define xmlKeepBlanksDefault xmlKeepBlanksDefault__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlLastElementChild */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlLastElementChild */
/*-no- .............................................................................................................. */
/*-no- #define xmlLastElementChild xmlLastElementChild__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlLineNumbersDefault */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlLineNumbersDefault */
/*-no- .................................................................................................................. */
/*-no- #define xmlLineNumbersDefault xmlLineNumbersDefault__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlLinkGetData */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlLinkGetData */
/*-no- .................................................................................................... */
/*-no- #define xmlLinkGetData xmlLinkGetData__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListAppend */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlListAppend */
/*-no- .................................................................................................. */
/*-no- #define xmlListAppend xmlListAppend__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListClear */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlListClear */
/*-no- ................................................................................................ */
/*-no- #define xmlListClear xmlListClear__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListCopy */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlListCopy */
/*-no- .............................................................................................. */
/*-no- #define xmlListCopy xmlListCopy__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListCreate */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlListCreate */
/*-no- .................................................................................................. */
/*-no- #define xmlListCreate xmlListCreate__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListDelete */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlListDelete */
/*-no- .................................................................................................. */
/*-no- #define xmlListDelete xmlListDelete__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListDup */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlListDup */
/*-no- ............................................................................................ */
/*-no- #define xmlListDup xmlListDup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListEmpty */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlListEmpty */
/*-no- ................................................................................................ */
/*-no- #define xmlListEmpty xmlListEmpty__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListEnd */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlListEnd */
/*-no- ............................................................................................ */
/*-no- #define xmlListEnd xmlListEnd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListFront */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlListFront */
/*-no- ................................................................................................ */
/*-no- #define xmlListFront xmlListFront__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListInsert */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlListInsert */
/*-no- .................................................................................................. */
/*-no- #define xmlListInsert xmlListInsert__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListMerge */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlListMerge */
/*-no- ................................................................................................ */
/*-no- #define xmlListMerge xmlListMerge__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListPopBack */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlListPopBack */
/*-no- .................................................................................................... */
/*-no- #define xmlListPopBack xmlListPopBack__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListPopFront */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlListPopFront */
/*-no- ...................................................................................................... */
/*-no- #define xmlListPopFront xmlListPopFront__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListPushBack */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlListPushBack */
/*-no- ...................................................................................................... */
/*-no- #define xmlListPushBack xmlListPushBack__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListPushFront */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlListPushFront */
/*-no- ........................................................................................................ */
/*-no- #define xmlListPushFront xmlListPushFront__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListRemoveAll */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlListRemoveAll */
/*-no- ........................................................................................................ */
/*-no- #define xmlListRemoveAll xmlListRemoveAll__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListRemoveFirst */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlListRemoveFirst */
/*-no- ............................................................................................................ */
/*-no- #define xmlListRemoveFirst xmlListRemoveFirst__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListRemoveLast */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlListRemoveLast */
/*-no- .......................................................................................................... */
/*-no- #define xmlListRemoveLast xmlListRemoveLast__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListReverse */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlListReverse */
/*-no- .................................................................................................... */
/*-no- #define xmlListReverse xmlListReverse__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListReverseSearch */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlListReverseSearch */
/*-no- ................................................................................................................ */
/*-no- #define xmlListReverseSearch xmlListReverseSearch__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListReverseWalk */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlListReverseWalk */
/*-no- ............................................................................................................ */
/*-no- #define xmlListReverseWalk xmlListReverseWalk__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListSearch */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlListSearch */
/*-no- .................................................................................................. */
/*-no- #define xmlListSearch xmlListSearch__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListSize */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlListSize */
/*-no- .............................................................................................. */
/*-no- #define xmlListSize xmlListSize__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListSort */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlListSort */
/*-no- .............................................................................................. */
/*-no- #define xmlListSort xmlListSort__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_list */
/*-no- #undef xmlListWalk */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlListWalk */
/*-no- .............................................................................................. */
/*-no- #define xmlListWalk xmlListWalk__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlLoadACatalog */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlLoadACatalog */
/*-no- ...................................................................................................... */
/*-no- #define xmlLoadACatalog xmlLoadACatalog__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlLoadCatalog */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlLoadCatalog */
/*-no- .................................................................................................... */
/*-no- #define xmlLoadCatalog xmlLoadCatalog__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlLoadCatalogs */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlLoadCatalogs */
/*-no- ...................................................................................................... */
/*-no- #define xmlLoadCatalogs xmlLoadCatalogs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlLoadExternalEntity */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlLoadExternalEntity */
/*-no- .................................................................................................................. */
/*-no- #define xmlLoadExternalEntity xmlLoadExternalEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlLoadSGMLSuperCatalog */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlLoadSGMLSuperCatalog */
/*-no- ...................................................................................................................... */
/*-no- #define xmlLoadSGMLSuperCatalog xmlLoadSGMLSuperCatalog__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_threads */
/*-no- #undef xmlLockLibrary */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlLockLibrary */
/*-no- .................................................................................................... */
/*-no- #define xmlLockLibrary xmlLockLibrary__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlLsCountNode */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlLsCountNode */
/*-no- .................................................................................................... */
/*-no- #define xmlLsCountNode xmlLsCountNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlLsOneNode */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlLsOneNode */
/*-no- ................................................................................................ */
/*-no- #define xmlLsOneNode xmlLsOneNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlMallocAtomicLoc */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlMallocAtomicLoc */
/*-no- ............................................................................................................ */
/*-no- #define xmlMallocAtomicLoc xmlMallocAtomicLoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlMallocLoc */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlMallocLoc */
/*-no- ................................................................................................ */
/*-no- #define xmlMallocLoc xmlMallocLoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlMemBlocks */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlMemBlocks */
/*-no- ................................................................................................ */
/*-no- #define xmlMemBlocks xmlMemBlocks__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlMemDisplay */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlMemDisplay */
/*-no- .................................................................................................. */
/*-no- #define xmlMemDisplay xmlMemDisplay__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlMemDisplayLast */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlMemDisplayLast */
/*-no- .......................................................................................................... */
/*-no- #define xmlMemDisplayLast xmlMemDisplayLast__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlMemFree */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlMemFree */
/*-no- ............................................................................................ */
/*-no- #define xmlMemFree xmlMemFree__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlMemGet */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlMemGet */
/*-no- .......................................................................................... */
/*-no- #define xmlMemGet xmlMemGet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlMemMalloc */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlMemMalloc */
/*-no- ................................................................................................ */
/*-no- #define xmlMemMalloc xmlMemMalloc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlMemRealloc */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlMemRealloc */
/*-no- .................................................................................................. */
/*-no- #define xmlMemRealloc xmlMemRealloc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlMemSetup */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlMemSetup */
/*-no- .............................................................................................. */
/*-no- #define xmlMemSetup xmlMemSetup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlMemShow */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlMemShow */
/*-no- ............................................................................................ */
/*-no- #define xmlMemShow xmlMemShow__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlMemStrdupLoc */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlMemStrdupLoc */
/*-no- ...................................................................................................... */
/*-no- #define xmlMemStrdupLoc xmlMemStrdupLoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlMemUsed */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlMemUsed */
/*-no- ............................................................................................ */
/*-no- #define xmlMemUsed xmlMemUsed__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlMemoryDump */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlMemoryDump */
/*-no- .................................................................................................. */
/*-no- #define xmlMemoryDump xmlMemoryDump__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlMemoryStrdup */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlMemoryStrdup */
/*-no- ...................................................................................................... */
/*-no- #define xmlMemoryStrdup xmlMemoryStrdup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_MODULES_ENABLED) */
/*-no- #ifdef bottom_xmlmodule */
/*-no- #undef xmlModuleClose */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlModuleClose */
/*-no- .................................................................................................... */
/*-no- #define xmlModuleClose xmlModuleClose__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_MODULES_ENABLED) */
/*-no- #ifdef bottom_xmlmodule */
/*-no- #undef xmlModuleFree */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlModuleFree */
/*-no- .................................................................................................. */
/*-no- #define xmlModuleFree xmlModuleFree__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_MODULES_ENABLED) */
/*-no- #ifdef bottom_xmlmodule */
/*-no- #undef xmlModuleOpen */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlModuleOpen */
/*-no- .................................................................................................. */
/*-no- #define xmlModuleOpen xmlModuleOpen__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_MODULES_ENABLED) */
/*-no- #ifdef bottom_xmlmodule */
/*-no- #undef xmlModuleSymbol */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlModuleSymbol */
/*-no- ...................................................................................................... */
/*-no- #define xmlModuleSymbol xmlModuleSymbol__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_threads */
/*-no- #undef xmlMutexLock */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlMutexLock */
/*-no- ................................................................................................ */
/*-no- #define xmlMutexLock xmlMutexLock__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_threads */
/*-no- #undef xmlMutexUnlock */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlMutexUnlock */
/*-no- .................................................................................................... */
/*-no- #define xmlMutexUnlock xmlMutexUnlock__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlNamespaceParseNCName */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNamespaceParseNCName */
/*-no- ...................................................................................................................... */
/*-no- #define xmlNamespaceParseNCName xmlNamespaceParseNCName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlNamespaceParseNSDef */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNamespaceParseNSDef */
/*-no- .................................................................................................................... */
/*-no- #define xmlNamespaceParseNSDef xmlNamespaceParseNSDef__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlNamespaceParseQName */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNamespaceParseQName */
/*-no- .................................................................................................................... */
/*-no- #define xmlNamespaceParseQName xmlNamespaceParseQName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPCheckResponse */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPCheckResponse */
/*-no- ...................................................................................................................... */
/*-no- #define xmlNanoFTPCheckResponse xmlNanoFTPCheckResponse__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPCleanup */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPCleanup */
/*-no- .......................................................................................................... */
/*-no- #define xmlNanoFTPCleanup xmlNanoFTPCleanup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPClose */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPClose */
/*-no- ...................................................................................................... */
/*-no- #define xmlNanoFTPClose xmlNanoFTPClose__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPCloseConnection */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPCloseConnection */
/*-no- .......................................................................................................................... */
/*-no- #define xmlNanoFTPCloseConnection xmlNanoFTPCloseConnection__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPConnect */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPConnect */
/*-no- .......................................................................................................... */
/*-no- #define xmlNanoFTPConnect xmlNanoFTPConnect__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPConnectTo */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPConnectTo */
/*-no- .............................................................................................................. */
/*-no- #define xmlNanoFTPConnectTo xmlNanoFTPConnectTo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPCwd */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPCwd */
/*-no- .................................................................................................. */
/*-no- #define xmlNanoFTPCwd xmlNanoFTPCwd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPDele */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPDele */
/*-no- .................................................................................................... */
/*-no- #define xmlNanoFTPDele xmlNanoFTPDele__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPFreeCtxt */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPFreeCtxt */
/*-no- ............................................................................................................ */
/*-no- #define xmlNanoFTPFreeCtxt xmlNanoFTPFreeCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPGet */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPGet */
/*-no- .................................................................................................. */
/*-no- #define xmlNanoFTPGet xmlNanoFTPGet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPGetConnection */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPGetConnection */
/*-no- ...................................................................................................................... */
/*-no- #define xmlNanoFTPGetConnection xmlNanoFTPGetConnection__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPGetResponse */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPGetResponse */
/*-no- .................................................................................................................. */
/*-no- #define xmlNanoFTPGetResponse xmlNanoFTPGetResponse__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPGetSocket */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPGetSocket */
/*-no- .............................................................................................................. */
/*-no- #define xmlNanoFTPGetSocket xmlNanoFTPGetSocket__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPInit */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPInit */
/*-no- .................................................................................................... */
/*-no- #define xmlNanoFTPInit xmlNanoFTPInit__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPList */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPList */
/*-no- .................................................................................................... */
/*-no- #define xmlNanoFTPList xmlNanoFTPList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPNewCtxt */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPNewCtxt */
/*-no- .......................................................................................................... */
/*-no- #define xmlNanoFTPNewCtxt xmlNanoFTPNewCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPOpen */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPOpen */
/*-no- .................................................................................................... */
/*-no- #define xmlNanoFTPOpen xmlNanoFTPOpen__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPProxy */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPProxy */
/*-no- ...................................................................................................... */
/*-no- #define xmlNanoFTPProxy xmlNanoFTPProxy__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPQuit */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPQuit */
/*-no- .................................................................................................... */
/*-no- #define xmlNanoFTPQuit xmlNanoFTPQuit__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPRead */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPRead */
/*-no- .................................................................................................... */
/*-no- #define xmlNanoFTPRead xmlNanoFTPRead__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPScanProxy */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPScanProxy */
/*-no- .............................................................................................................. */
/*-no- #define xmlNanoFTPScanProxy xmlNanoFTPScanProxy__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_FTP_ENABLED) */
/*-no- #ifdef bottom_nanoftp */
/*-no- #undef xmlNanoFTPUpdateURL */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoFTPUpdateURL */
/*-no- .............................................................................................................. */
/*-no- #define xmlNanoFTPUpdateURL xmlNanoFTPUpdateURL__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPAuthHeader */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPAuthHeader */
/*-no- .................................................................................................................. */
/*-no- #define xmlNanoHTTPAuthHeader xmlNanoHTTPAuthHeader__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPCleanup */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPCleanup */
/*-no- ............................................................................................................ */
/*-no- #define xmlNanoHTTPCleanup xmlNanoHTTPCleanup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPClose */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPClose */
/*-no- ........................................................................................................ */
/*-no- #define xmlNanoHTTPClose xmlNanoHTTPClose__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPContentLength */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPContentLength */
/*-no- ........................................................................................................................ */
/*-no- #define xmlNanoHTTPContentLength xmlNanoHTTPContentLength__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPEncoding */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPEncoding */
/*-no- .............................................................................................................. */
/*-no- #define xmlNanoHTTPEncoding xmlNanoHTTPEncoding__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPFetch */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPFetch */
/*-no- ........................................................................................................ */
/*-no- #define xmlNanoHTTPFetch xmlNanoHTTPFetch__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPInit */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPInit */
/*-no- ...................................................................................................... */
/*-no- #define xmlNanoHTTPInit xmlNanoHTTPInit__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPMethod */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPMethod */
/*-no- .......................................................................................................... */
/*-no- #define xmlNanoHTTPMethod xmlNanoHTTPMethod__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPMethodRedir */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPMethodRedir */
/*-no- .................................................................................................................... */
/*-no- #define xmlNanoHTTPMethodRedir xmlNanoHTTPMethodRedir__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPMimeType */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPMimeType */
/*-no- .............................................................................................................. */
/*-no- #define xmlNanoHTTPMimeType xmlNanoHTTPMimeType__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPOpen */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPOpen */
/*-no- ...................................................................................................... */
/*-no- #define xmlNanoHTTPOpen xmlNanoHTTPOpen__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPOpenRedir */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPOpenRedir */
/*-no- ................................................................................................................ */
/*-no- #define xmlNanoHTTPOpenRedir xmlNanoHTTPOpenRedir__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPRead */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPRead */
/*-no- ...................................................................................................... */
/*-no- #define xmlNanoHTTPRead xmlNanoHTTPRead__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPRedir */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPRedir */
/*-no- ........................................................................................................ */
/*-no- #define xmlNanoHTTPRedir xmlNanoHTTPRedir__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPReturnCode */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPReturnCode */
/*-no- .................................................................................................................. */
/*-no- #define xmlNanoHTTPReturnCode xmlNanoHTTPReturnCode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPSave */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPSave */
/*-no- ...................................................................................................... */
/*-no- #define xmlNanoHTTPSave xmlNanoHTTPSave__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_nanohttp */
/*-no- #undef xmlNanoHTTPScanProxy */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNanoHTTPScanProxy */
/*-no- ................................................................................................................ */
/*-no- #define xmlNanoHTTPScanProxy xmlNanoHTTPScanProxy__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) && defined(LIBXML_AUTOMATA_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlNewAutomata */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewAutomata */
/*-no- .................................................................................................... */
/*-no- #define xmlNewAutomata xmlNewAutomata__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewCDataBlock */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewCDataBlock */
/*-no- ........................................................................................................ */
/*-no- #define xmlNewCDataBlock xmlNewCDataBlock__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlNewCatalog */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewCatalog */
/*-no- .................................................................................................. */
/*-no- #define xmlNewCatalog xmlNewCatalog__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlNewCharEncodingHandler */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNewCharEncodingHandler */
/*-no- .......................................................................................................................... */
/*-no- #define xmlNewCharEncodingHandler xmlNewCharEncodingHandler__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewCharRef */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewCharRef */
/*-no- .................................................................................................. */
/*-no- #define xmlNewCharRef xmlNewCharRef__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewChild */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewChild */
/*-no- .............................................................................................. */
/*-no- #define xmlNewChild xmlNewChild__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewComment */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewComment */
/*-no- .................................................................................................. */
/*-no- #define xmlNewComment xmlNewComment__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewDoc */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNewDoc */
/*-no- .......................................................................................... */
/*-no- #define xmlNewDoc xmlNewDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewDocComment */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewDocComment */
/*-no- ........................................................................................................ */
/*-no- #define xmlNewDocComment xmlNewDocComment__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlNewDocElementContent */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewDocElementContent */
/*-no- ...................................................................................................................... */
/*-no- #define xmlNewDocElementContent xmlNewDocElementContent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewDocFragment */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNewDocFragment */
/*-no- .......................................................................................................... */
/*-no- #define xmlNewDocFragment xmlNewDocFragment__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewDocNode */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewDocNode */
/*-no- .................................................................................................. */
/*-no- #define xmlNewDocNode xmlNewDocNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewDocNodeEatName */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewDocNodeEatName */
/*-no- ................................................................................................................ */
/*-no- #define xmlNewDocNodeEatName xmlNewDocNodeEatName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewDocPI */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewDocPI */
/*-no- .............................................................................................. */
/*-no- #define xmlNewDocPI xmlNewDocPI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewDocProp */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewDocProp */
/*-no- .................................................................................................. */
/*-no- #define xmlNewDocProp xmlNewDocProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewDocRawNode */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewDocRawNode */
/*-no- ........................................................................................................ */
/*-no- #define xmlNewDocRawNode xmlNewDocRawNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewDocText */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewDocText */
/*-no- .................................................................................................. */
/*-no- #define xmlNewDocText xmlNewDocText__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewDocTextLen */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewDocTextLen */
/*-no- ........................................................................................................ */
/*-no- #define xmlNewDocTextLen xmlNewDocTextLen__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewDtd */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNewDtd */
/*-no- .......................................................................................... */
/*-no- #define xmlNewDtd xmlNewDtd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlNewElementContent */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewElementContent */
/*-no- ................................................................................................................ */
/*-no- #define xmlNewElementContent xmlNewElementContent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_entities */
/*-no- #undef xmlNewEntity */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewEntity */
/*-no- ................................................................................................ */
/*-no- #define xmlNewEntity xmlNewEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlNewEntityInputStream */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewEntityInputStream */
/*-no- ...................................................................................................................... */
/*-no- #define xmlNewEntityInputStream xmlNewEntityInputStream__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlNewGlobalNs */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewGlobalNs */
/*-no- .................................................................................................... */
/*-no- #define xmlNewGlobalNs xmlNewGlobalNs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlNewIOInputStream */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewIOInputStream */
/*-no- .............................................................................................................. */
/*-no- #define xmlNewIOInputStream xmlNewIOInputStream__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlNewInputFromFile */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewInputFromFile */
/*-no- .............................................................................................................. */
/*-no- #define xmlNewInputFromFile xmlNewInputFromFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlNewInputStream */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNewInputStream */
/*-no- .......................................................................................................... */
/*-no- #define xmlNewInputStream xmlNewInputStream__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_threads */
/*-no- #undef xmlNewMutex */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewMutex */
/*-no- .............................................................................................. */
/*-no- #define xmlNewMutex xmlNewMutex__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewNode */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewNode */
/*-no- ............................................................................................ */
/*-no- #define xmlNewNode xmlNewNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewNodeEatName */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNewNodeEatName */
/*-no- .......................................................................................................... */
/*-no- #define xmlNewNodeEatName xmlNewNodeEatName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewNs */
/*-no- ..................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewNs */
/*-no- ........................................................................................ */
/*-no- #define xmlNewNs xmlNewNs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewNsProp */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewNsProp */
/*-no- ................................................................................................ */
/*-no- #define xmlNewNsProp xmlNewNsProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewNsPropEatName */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewNsPropEatName */
/*-no- .............................................................................................................. */
/*-no- #define xmlNewNsPropEatName xmlNewNsPropEatName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewPI */
/*-no- ..................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewPI */
/*-no- ........................................................................................ */
/*-no- #define xmlNewPI xmlNewPI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlNewParserCtxt */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewParserCtxt */
/*-no- ........................................................................................................ */
/*-no- #define xmlNewParserCtxt xmlNewParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_HTML_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewProp */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewProp */
/*-no- ............................................................................................ */
/*-no- #define xmlNewProp xmlNewProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_threads */
/*-no- #undef xmlNewRMutex */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewRMutex */
/*-no- ................................................................................................ */
/*-no- #define xmlNewRMutex xmlNewRMutex__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewReference */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewReference */
/*-no- ...................................................................................................... */
/*-no- #define xmlNewReference xmlNewReference__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlNewStringInputStream */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewStringInputStream */
/*-no- ...................................................................................................................... */
/*-no- #define xmlNewStringInputStream xmlNewStringInputStream__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewText */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewText */
/*-no- ............................................................................................ */
/*-no- #define xmlNewText xmlNewText__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewTextChild */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewTextChild */
/*-no- ...................................................................................................... */
/*-no- #define xmlNewTextChild xmlNewTextChild__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNewTextLen */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewTextLen */
/*-no- .................................................................................................. */
/*-no- #define xmlNewTextLen xmlNewTextLen__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlNewTextReader */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewTextReader */
/*-no- ........................................................................................................ */
/*-no- #define xmlNewTextReader xmlNewTextReader__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlNewTextReaderFilename */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewTextReaderFilename */
/*-no- ........................................................................................................................ */
/*-no- #define xmlNewTextReaderFilename xmlNewTextReaderFilename__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlNewTextWriter */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNewTextWriter */
/*-no- ........................................................................................................ */
/*-no- #define xmlNewTextWriter xmlNewTextWriter__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlNewTextWriterDoc */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewTextWriterDoc */
/*-no- .............................................................................................................. */
/*-no- #define xmlNewTextWriterDoc xmlNewTextWriterDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlNewTextWriterFilename */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewTextWriterFilename */
/*-no- ........................................................................................................................ */
/*-no- #define xmlNewTextWriterFilename xmlNewTextWriterFilename__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlNewTextWriterMemory */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewTextWriterMemory */
/*-no- .................................................................................................................... */
/*-no- #define xmlNewTextWriterMemory xmlNewTextWriterMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlNewTextWriterPushParser */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewTextWriterPushParser */
/*-no- ............................................................................................................................ */
/*-no- #define xmlNewTextWriterPushParser xmlNewTextWriterPushParser__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlNewTextWriterTree */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewTextWriterTree */
/*-no- ................................................................................................................ */
/*-no- #define xmlNewTextWriterTree xmlNewTextWriterTree__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlNewValidCtxt */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNewValidCtxt */
/*-no- ...................................................................................................... */
/*-no- #define xmlNewValidCtxt xmlNewValidCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlNextChar */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNextChar */
/*-no- .............................................................................................. */
/*-no- #define xmlNextChar xmlNextChar__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNextElementSibling */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNextElementSibling */
/*-no- .................................................................................................................. */
/*-no- #define xmlNextElementSibling xmlNextElementSibling__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlNoNetExternalEntityLoader */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNoNetExternalEntityLoader */
/*-no- ................................................................................................................................ */
/*-no- #define xmlNoNetExternalEntityLoader xmlNoNetExternalEntityLoader__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeAddContent */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNodeAddContent */
/*-no- .......................................................................................................... */
/*-no- #define xmlNodeAddContent xmlNodeAddContent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeAddContentLen */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNodeAddContentLen */
/*-no- ................................................................................................................ */
/*-no- #define xmlNodeAddContentLen xmlNodeAddContentLen__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeBufGetContent */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNodeBufGetContent */
/*-no- ................................................................................................................ */
/*-no- #define xmlNodeBufGetContent xmlNodeBufGetContent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlNodeDump */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNodeDump */
/*-no- .............................................................................................. */
/*-no- #define xmlNodeDump xmlNodeDump__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlNodeDumpOutput */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNodeDumpOutput */
/*-no- .......................................................................................................... */
/*-no- #define xmlNodeDumpOutput xmlNodeDumpOutput__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeGetBase */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNodeGetBase */
/*-no- .................................................................................................... */
/*-no- #define xmlNodeGetBase xmlNodeGetBase__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeGetContent */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNodeGetContent */
/*-no- .......................................................................................................... */
/*-no- #define xmlNodeGetContent xmlNodeGetContent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeGetLang */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNodeGetLang */
/*-no- .................................................................................................... */
/*-no- #define xmlNodeGetLang xmlNodeGetLang__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeGetSpacePreserve */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNodeGetSpacePreserve */
/*-no- ...................................................................................................................... */
/*-no- #define xmlNodeGetSpacePreserve xmlNodeGetSpacePreserve__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeIsText */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNodeIsText */
/*-no- .................................................................................................. */
/*-no- #define xmlNodeIsText xmlNodeIsText__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeListGetRawString */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNodeListGetRawString */
/*-no- ...................................................................................................................... */
/*-no- #define xmlNodeListGetRawString xmlNodeListGetRawString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeListGetString */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNodeListGetString */
/*-no- ................................................................................................................ */
/*-no- #define xmlNodeListGetString xmlNodeListGetString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_XINCLUDE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeSetBase */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNodeSetBase */
/*-no- .................................................................................................... */
/*-no- #define xmlNodeSetBase xmlNodeSetBase__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeSetContent */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlNodeSetContent */
/*-no- .......................................................................................................... */
/*-no- #define xmlNodeSetContent xmlNodeSetContent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeSetContentLen */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNodeSetContentLen */
/*-no- ................................................................................................................ */
/*-no- #define xmlNodeSetContentLen xmlNodeSetContentLen__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeSetLang */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNodeSetLang */
/*-no- .................................................................................................... */
/*-no- #define xmlNodeSetLang xmlNodeSetLang__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeSetName */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNodeSetName */
/*-no- .................................................................................................... */
/*-no- #define xmlNodeSetName xmlNodeSetName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlNodeSetSpacePreserve */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNodeSetSpacePreserve */
/*-no- ...................................................................................................................... */
/*-no- #define xmlNodeSetSpacePreserve xmlNodeSetSpacePreserve__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_uri */
/*-no- #undef xmlNormalizeURIPath */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlNormalizeURIPath */
/*-no- .............................................................................................................. */
/*-no- #define xmlNormalizeURIPath xmlNormalizeURIPath__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlNormalizeWindowsPath */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlNormalizeWindowsPath */
/*-no- ...................................................................................................................... */
/*-no- #define xmlNormalizeWindowsPath xmlNormalizeWindowsPath__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlOutputBufferClose */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlOutputBufferClose */
/*-no- ................................................................................................................ */
/*-no- #define xmlOutputBufferClose xmlOutputBufferClose__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlOutputBufferCreateBuffer */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlOutputBufferCreateBuffer */
/*-no- .............................................................................................................................. */
/*-no- #define xmlOutputBufferCreateBuffer xmlOutputBufferCreateBuffer__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlOutputBufferCreateFd */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlOutputBufferCreateFd */
/*-no- ...................................................................................................................... */
/*-no- #define xmlOutputBufferCreateFd xmlOutputBufferCreateFd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlOutputBufferCreateFile */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlOutputBufferCreateFile */
/*-no- .......................................................................................................................... */
/*-no- #define xmlOutputBufferCreateFile xmlOutputBufferCreateFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlOutputBufferCreateFilename */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlOutputBufferCreateFilename */
/*-no- .................................................................................................................................. */
/*-no- #define xmlOutputBufferCreateFilename xmlOutputBufferCreateFilename__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlOutputBufferCreateFilenameDefault */
/*-no- ......................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlOutputBufferCreateFilenameDefault */
/*-no- ................................................................................................................................................ */
/*-no- #define xmlOutputBufferCreateFilenameDefault xmlOutputBufferCreateFilenameDefault__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlOutputBufferCreateIO */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlOutputBufferCreateIO */
/*-no- ...................................................................................................................... */
/*-no- #define xmlOutputBufferCreateIO xmlOutputBufferCreateIO__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlOutputBufferFlush */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlOutputBufferFlush */
/*-no- ................................................................................................................ */
/*-no- #define xmlOutputBufferFlush xmlOutputBufferFlush__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlOutputBufferWrite */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlOutputBufferWrite */
/*-no- ................................................................................................................ */
/*-no- #define xmlOutputBufferWrite xmlOutputBufferWrite__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlOutputBufferWriteEscape */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlOutputBufferWriteEscape */
/*-no- ............................................................................................................................ */
/*-no- #define xmlOutputBufferWriteEscape xmlOutputBufferWriteEscape__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlOutputBufferWriteString */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlOutputBufferWriteString */
/*-no- ............................................................................................................................ */
/*-no- #define xmlOutputBufferWriteString xmlOutputBufferWriteString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseAttValue */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParseAttValue */
/*-no- ........................................................................................................ */
/*-no- #define xmlParseAttValue xmlParseAttValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseAttribute */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlParseAttribute */
/*-no- .......................................................................................................... */
/*-no- #define xmlParseAttribute xmlParseAttribute__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseAttributeListDecl */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlParseAttributeListDecl */
/*-no- .......................................................................................................................... */
/*-no- #define xmlParseAttributeListDecl xmlParseAttributeListDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseAttributeType */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlParseAttributeType */
/*-no- .................................................................................................................. */
/*-no- #define xmlParseAttributeType xmlParseAttributeType__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseBalancedChunkMemory */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParseBalancedChunkMemory */
/*-no- .............................................................................................................................. */
/*-no- #define xmlParseBalancedChunkMemory xmlParseBalancedChunkMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseBalancedChunkMemoryRecover */
/*-no- ................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseBalancedChunkMemoryRecover */
/*-no- ............................................................................................................................................ */
/*-no- #define xmlParseBalancedChunkMemoryRecover xmlParseBalancedChunkMemoryRecover__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseCDSect */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseCDSect */
/*-no- .................................................................................................... */
/*-no- #define xmlParseCDSect xmlParseCDSect__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_CATALOG_ENABLED) */
/*-no- #ifdef bottom_catalog */
/*-no- #undef xmlParseCatalogFile */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseCatalogFile */
/*-no- .............................................................................................................. */
/*-no- #define xmlParseCatalogFile xmlParseCatalogFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseCharData */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParseCharData */
/*-no- ........................................................................................................ */
/*-no- #define xmlParseCharData xmlParseCharData__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlParseCharEncoding */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseCharEncoding */
/*-no- ................................................................................................................ */
/*-no- #define xmlParseCharEncoding xmlParseCharEncoding__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseCharRef */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseCharRef */
/*-no- ...................................................................................................... */
/*-no- #define xmlParseCharRef xmlParseCharRef__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PUSH_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseChunk */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseChunk */
/*-no- .................................................................................................. */
/*-no- #define xmlParseChunk xmlParseChunk__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseComment */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseComment */
/*-no- ...................................................................................................... */
/*-no- #define xmlParseComment xmlParseComment__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseContent */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseContent */
/*-no- ...................................................................................................... */
/*-no- #define xmlParseContent xmlParseContent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseCtxtExternalEntity */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseCtxtExternalEntity */
/*-no- ............................................................................................................................ */
/*-no- #define xmlParseCtxtExternalEntity xmlParseCtxtExternalEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseDTD */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParseDTD */
/*-no- .............................................................................................. */
/*-no- #define xmlParseDTD xmlParseDTD__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseDefaultDecl */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseDefaultDecl */
/*-no- .............................................................................................................. */
/*-no- #define xmlParseDefaultDecl xmlParseDefaultDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseDoc */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParseDoc */
/*-no- .............................................................................................. */
/*-no- #define xmlParseDoc xmlParseDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseDocTypeDecl */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseDocTypeDecl */
/*-no- .............................................................................................................. */
/*-no- #define xmlParseDocTypeDecl xmlParseDocTypeDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseDocument */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParseDocument */
/*-no- ........................................................................................................ */
/*-no- #define xmlParseDocument xmlParseDocument__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseElement */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseElement */
/*-no- ...................................................................................................... */
/*-no- #define xmlParseElement xmlParseElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseElementChildrenContentDecl */
/*-no- ................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseElementChildrenContentDecl */
/*-no- ............................................................................................................................................ */
/*-no- #define xmlParseElementChildrenContentDecl xmlParseElementChildrenContentDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseElementContentDecl */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseElementContentDecl */
/*-no- ............................................................................................................................ */
/*-no- #define xmlParseElementContentDecl xmlParseElementContentDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseElementDecl */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseElementDecl */
/*-no- .............................................................................................................. */
/*-no- #define xmlParseElementDecl xmlParseElementDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseElementMixedContentDecl */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseElementMixedContentDecl */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlParseElementMixedContentDecl xmlParseElementMixedContentDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseEncName */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseEncName */
/*-no- ...................................................................................................... */
/*-no- #define xmlParseEncName xmlParseEncName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseEncodingDecl */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseEncodingDecl */
/*-no- ................................................................................................................ */
/*-no- #define xmlParseEncodingDecl xmlParseEncodingDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseEndTag */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseEndTag */
/*-no- .................................................................................................... */
/*-no- #define xmlParseEndTag xmlParseEndTag__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseEntity */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseEntity */
/*-no- .................................................................................................... */
/*-no- #define xmlParseEntity xmlParseEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseEntityDecl */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseEntityDecl */
/*-no- ............................................................................................................ */
/*-no- #define xmlParseEntityDecl xmlParseEntityDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseEntityRef */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlParseEntityRef */
/*-no- .......................................................................................................... */
/*-no- #define xmlParseEntityRef xmlParseEntityRef__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseEntityValue */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseEntityValue */
/*-no- .............................................................................................................. */
/*-no- #define xmlParseEntityValue xmlParseEntityValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseEnumeratedType */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseEnumeratedType */
/*-no- .................................................................................................................... */
/*-no- #define xmlParseEnumeratedType xmlParseEnumeratedType__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseEnumerationType */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParseEnumerationType */
/*-no- ...................................................................................................................... */
/*-no- #define xmlParseEnumerationType xmlParseEnumerationType__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseExtParsedEnt */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseExtParsedEnt */
/*-no- ................................................................................................................ */
/*-no- #define xmlParseExtParsedEnt xmlParseExtParsedEnt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseExternalEntity */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseExternalEntity */
/*-no- .................................................................................................................... */
/*-no- #define xmlParseExternalEntity xmlParseExternalEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseExternalID */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseExternalID */
/*-no- ............................................................................................................ */
/*-no- #define xmlParseExternalID xmlParseExternalID__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseExternalSubset */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseExternalSubset */
/*-no- .................................................................................................................... */
/*-no- #define xmlParseExternalSubset xmlParseExternalSubset__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseFile */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParseFile */
/*-no- ................................................................................................ */
/*-no- #define xmlParseFile xmlParseFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseInNodeContext */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlParseInNodeContext */
/*-no- .................................................................................................................. */
/*-no- #define xmlParseInNodeContext xmlParseInNodeContext__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseMarkupDecl */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseMarkupDecl */
/*-no- ............................................................................................................ */
/*-no- #define xmlParseMarkupDecl xmlParseMarkupDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseMemory */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseMemory */
/*-no- .................................................................................................... */
/*-no- #define xmlParseMemory xmlParseMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseMisc */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParseMisc */
/*-no- ................................................................................................ */
/*-no- #define xmlParseMisc xmlParseMisc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseName */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParseName */
/*-no- ................................................................................................ */
/*-no- #define xmlParseName xmlParseName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlParseNamespace */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlParseNamespace */
/*-no- .......................................................................................................... */
/*-no- #define xmlParseNamespace xmlParseNamespace__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseNmtoken */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseNmtoken */
/*-no- ...................................................................................................... */
/*-no- #define xmlParseNmtoken xmlParseNmtoken__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseNotationDecl */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseNotationDecl */
/*-no- ................................................................................................................ */
/*-no- #define xmlParseNotationDecl xmlParseNotationDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseNotationType */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseNotationType */
/*-no- ................................................................................................................ */
/*-no- #define xmlParseNotationType xmlParseNotationType__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParsePEReference */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParsePEReference */
/*-no- .............................................................................................................. */
/*-no- #define xmlParsePEReference xmlParsePEReference__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParsePI */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParsePI */
/*-no- ............................................................................................ */
/*-no- #define xmlParsePI xmlParsePI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParsePITarget */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParsePITarget */
/*-no- ........................................................................................................ */
/*-no- #define xmlParsePITarget xmlParsePITarget__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParsePubidLiteral */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParsePubidLiteral */
/*-no- ................................................................................................................ */
/*-no- #define xmlParsePubidLiteral xmlParsePubidLiteral__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlParseQuotedString */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseQuotedString */
/*-no- ................................................................................................................ */
/*-no- #define xmlParseQuotedString xmlParseQuotedString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseReference */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlParseReference */
/*-no- .......................................................................................................... */
/*-no- #define xmlParseReference xmlParseReference__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseSDDecl */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseSDDecl */
/*-no- .................................................................................................... */
/*-no- #define xmlParseSDDecl xmlParseSDDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseStartTag */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParseStartTag */
/*-no- ........................................................................................................ */
/*-no- #define xmlParseStartTag xmlParseStartTag__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseSystemLiteral */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlParseSystemLiteral */
/*-no- .................................................................................................................. */
/*-no- #define xmlParseSystemLiteral xmlParseSystemLiteral__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseTextDecl */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParseTextDecl */
/*-no- ........................................................................................................ */
/*-no- #define xmlParseTextDecl xmlParseTextDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_uri */
/*-no- #undef xmlParseURI */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParseURI */
/*-no- .............................................................................................. */
/*-no- #define xmlParseURI xmlParseURI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_uri */
/*-no- #undef xmlParseURIRaw */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseURIRaw */
/*-no- .................................................................................................... */
/*-no- #define xmlParseURIRaw xmlParseURIRaw__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_uri */
/*-no- #undef xmlParseURIReference */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseURIReference */
/*-no- ................................................................................................................ */
/*-no- #define xmlParseURIReference xmlParseURIReference__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseVersionInfo */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseVersionInfo */
/*-no- .............................................................................................................. */
/*-no- #define xmlParseVersionInfo xmlParseVersionInfo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseVersionNum */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseVersionNum */
/*-no- ............................................................................................................ */
/*-no- #define xmlParseVersionNum xmlParseVersionNum__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParseXMLDecl */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParseXMLDecl */
/*-no- ...................................................................................................... */
/*-no- #define xmlParseXMLDecl xmlParseXMLDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlParserAddNodeInfo */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserAddNodeInfo */
/*-no- ................................................................................................................ */
/*-no- #define xmlParserAddNodeInfo xmlParserAddNodeInfo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_error */
/*-no- #undef xmlParserError */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserError */
/*-no- .................................................................................................... */
/*-no- #define xmlParserError xmlParserError__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlParserFindNodeInfo */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlParserFindNodeInfo */
/*-no- .................................................................................................................. */
/*-no- #define xmlParserFindNodeInfo xmlParserFindNodeInfo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlParserFindNodeInfoIndex */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserFindNodeInfoIndex */
/*-no- ............................................................................................................................ */
/*-no- #define xmlParserFindNodeInfoIndex xmlParserFindNodeInfoIndex__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlParserGetDirectory */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlParserGetDirectory */
/*-no- .................................................................................................................. */
/*-no- #define xmlParserGetDirectory xmlParserGetDirectory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlParserHandlePEReference */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserHandlePEReference */
/*-no- ............................................................................................................................ */
/*-no- #define xmlParserHandlePEReference xmlParserHandlePEReference__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlParserHandleReference */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserHandleReference */
/*-no- ........................................................................................................................ */
/*-no- #define xmlParserHandleReference xmlParserHandleReference__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlParserInputBufferCreateFd */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParserInputBufferCreateFd */
/*-no- ................................................................................................................................ */
/*-no- #define xmlParserInputBufferCreateFd xmlParserInputBufferCreateFd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlParserInputBufferCreateFile */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserInputBufferCreateFile */
/*-no- .................................................................................................................................... */
/*-no- #define xmlParserInputBufferCreateFile xmlParserInputBufferCreateFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlParserInputBufferCreateFilename */
/*-no- ................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserInputBufferCreateFilename */
/*-no- ............................................................................................................................................ */
/*-no- #define xmlParserInputBufferCreateFilename xmlParserInputBufferCreateFilename__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlParserInputBufferCreateFilenameDefault */
/*-no- ........................................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlParserInputBufferCreateFilenameDefault */
/*-no- .......................................................................................................................................................... */
/*-no- #define xmlParserInputBufferCreateFilenameDefault xmlParserInputBufferCreateFilenameDefault__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlParserInputBufferCreateIO */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParserInputBufferCreateIO */
/*-no- ................................................................................................................................ */
/*-no- #define xmlParserInputBufferCreateIO xmlParserInputBufferCreateIO__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlParserInputBufferCreateMem */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserInputBufferCreateMem */
/*-no- .................................................................................................................................. */
/*-no- #define xmlParserInputBufferCreateMem xmlParserInputBufferCreateMem__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlParserInputBufferCreateStatic */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParserInputBufferCreateStatic */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlParserInputBufferCreateStatic xmlParserInputBufferCreateStatic__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlParserInputBufferGrow */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserInputBufferGrow */
/*-no- ........................................................................................................................ */
/*-no- #define xmlParserInputBufferGrow xmlParserInputBufferGrow__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlParserInputBufferPush */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserInputBufferPush */
/*-no- ........................................................................................................................ */
/*-no- #define xmlParserInputBufferPush xmlParserInputBufferPush__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlParserInputBufferRead */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserInputBufferRead */
/*-no- ........................................................................................................................ */
/*-no- #define xmlParserInputBufferRead xmlParserInputBufferRead__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlParserInputGrow */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserInputGrow */
/*-no- ............................................................................................................ */
/*-no- #define xmlParserInputGrow xmlParserInputGrow__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlParserInputRead */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserInputRead */
/*-no- ............................................................................................................ */
/*-no- #define xmlParserInputRead xmlParserInputRead__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlParserInputShrink */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserInputShrink */
/*-no- ................................................................................................................ */
/*-no- #define xmlParserInputShrink xmlParserInputShrink__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_error */
/*-no- #undef xmlParserPrintFileContext */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlParserPrintFileContext */
/*-no- .......................................................................................................................... */
/*-no- #define xmlParserPrintFileContext xmlParserPrintFileContext__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_error */
/*-no- #undef xmlParserPrintFileInfo */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserPrintFileInfo */
/*-no- .................................................................................................................... */
/*-no- #define xmlParserPrintFileInfo xmlParserPrintFileInfo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_error */
/*-no- #undef xmlParserValidityError */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserValidityError */
/*-no- .................................................................................................................... */
/*-no- #define xmlParserValidityError xmlParserValidityError__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_error */
/*-no- #undef xmlParserValidityWarning */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlParserValidityWarning */
/*-no- ........................................................................................................................ */
/*-no- #define xmlParserValidityWarning xmlParserValidityWarning__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_error */
/*-no- #undef xmlParserWarning */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlParserWarning */
/*-no- ........................................................................................................ */
/*-no- #define xmlParserWarning xmlParserWarning__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_uri */
/*-no- #undef xmlPathToURI */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlPathToURI */
/*-no- ................................................................................................ */
/*-no- #define xmlPathToURI xmlPathToURI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_pattern */
/*-no- #undef xmlPatternFromRoot */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlPatternFromRoot */
/*-no- ............................................................................................................ */
/*-no- #define xmlPatternFromRoot xmlPatternFromRoot__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_pattern */
/*-no- #undef xmlPatternGetStreamCtxt */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlPatternGetStreamCtxt */
/*-no- ...................................................................................................................... */
/*-no- #define xmlPatternGetStreamCtxt xmlPatternGetStreamCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_pattern */
/*-no- #undef xmlPatternMatch */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlPatternMatch */
/*-no- ...................................................................................................... */
/*-no- #define xmlPatternMatch xmlPatternMatch__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_pattern */
/*-no- #undef xmlPatternMaxDepth */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlPatternMaxDepth */
/*-no- ............................................................................................................ */
/*-no- #define xmlPatternMaxDepth xmlPatternMaxDepth__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_pattern */
/*-no- #undef xmlPatternMinDepth */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlPatternMinDepth */
/*-no- ............................................................................................................ */
/*-no- #define xmlPatternMinDepth xmlPatternMinDepth__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_pattern */
/*-no- #undef xmlPatternStreamable */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlPatternStreamable */
/*-no- ................................................................................................................ */
/*-no- #define xmlPatternStreamable xmlPatternStreamable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_pattern */
/*-no- #undef xmlPatterncompile */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlPatterncompile */
/*-no- .......................................................................................................... */
/*-no- #define xmlPatterncompile xmlPatterncompile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlPedanticParserDefault */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlPedanticParserDefault */
/*-no- ........................................................................................................................ */
/*-no- #define xmlPedanticParserDefault xmlPedanticParserDefault__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlPopInput */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlPopInput */
/*-no- .............................................................................................. */
/*-no- #define xmlPopInput xmlPopInput__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlPopInputCallbacks */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlPopInputCallbacks */
/*-no- ................................................................................................................ */
/*-no- #define xmlPopInputCallbacks xmlPopInputCallbacks__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlPreviousElementSibling */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlPreviousElementSibling */
/*-no- .......................................................................................................................... */
/*-no- #define xmlPreviousElementSibling xmlPreviousElementSibling__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_uri */
/*-no- #undef xmlPrintURI */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlPrintURI */
/*-no- .............................................................................................. */
/*-no- #define xmlPrintURI xmlPrintURI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlPushInput */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlPushInput */
/*-no- ................................................................................................ */
/*-no- #define xmlPushInput xmlPushInput__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_threads */
/*-no- #undef xmlRMutexLock */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRMutexLock */
/*-no- .................................................................................................. */
/*-no- #define xmlRMutexLock xmlRMutexLock__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_threads */
/*-no- #undef xmlRMutexUnlock */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRMutexUnlock */
/*-no- ...................................................................................................... */
/*-no- #define xmlRMutexUnlock xmlRMutexUnlock__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlReadDoc */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlReadDoc */
/*-no- ............................................................................................ */
/*-no- #define xmlReadDoc xmlReadDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlReadFd */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlReadFd */
/*-no- .......................................................................................... */
/*-no- #define xmlReadFd xmlReadFd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlReadFile */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlReadFile */
/*-no- .............................................................................................. */
/*-no- #define xmlReadFile xmlReadFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlReadIO */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlReadIO */
/*-no- .......................................................................................... */
/*-no- #define xmlReadIO xmlReadIO__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlReadMemory */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlReadMemory */
/*-no- .................................................................................................. */
/*-no- #define xmlReadMemory xmlReadMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlReaderForDoc */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlReaderForDoc */
/*-no- ...................................................................................................... */
/*-no- #define xmlReaderForDoc xmlReaderForDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlReaderForFd */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlReaderForFd */
/*-no- .................................................................................................... */
/*-no- #define xmlReaderForFd xmlReaderForFd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlReaderForFile */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlReaderForFile */
/*-no- ........................................................................................................ */
/*-no- #define xmlReaderForFile xmlReaderForFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlReaderForIO */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlReaderForIO */
/*-no- .................................................................................................... */
/*-no- #define xmlReaderForIO xmlReaderForIO__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlReaderForMemory */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlReaderForMemory */
/*-no- ............................................................................................................ */
/*-no- #define xmlReaderForMemory xmlReaderForMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlReaderNewDoc */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlReaderNewDoc */
/*-no- ...................................................................................................... */
/*-no- #define xmlReaderNewDoc xmlReaderNewDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlReaderNewFd */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlReaderNewFd */
/*-no- .................................................................................................... */
/*-no- #define xmlReaderNewFd xmlReaderNewFd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlReaderNewFile */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlReaderNewFile */
/*-no- ........................................................................................................ */
/*-no- #define xmlReaderNewFile xmlReaderNewFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlReaderNewIO */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlReaderNewIO */
/*-no- .................................................................................................... */
/*-no- #define xmlReaderNewIO xmlReaderNewIO__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlReaderNewMemory */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlReaderNewMemory */
/*-no- ............................................................................................................ */
/*-no- #define xmlReaderNewMemory xmlReaderNewMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlReaderNewWalker */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlReaderNewWalker */
/*-no- ............................................................................................................ */
/*-no- #define xmlReaderNewWalker xmlReaderNewWalker__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlReaderWalker */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlReaderWalker */
/*-no- ...................................................................................................... */
/*-no- #define xmlReaderWalker xmlReaderWalker__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlmemory */
/*-no- #undef xmlReallocLoc */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlReallocLoc */
/*-no- .................................................................................................. */
/*-no- #define xmlReallocLoc xmlReallocLoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlReconciliateNs */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlReconciliateNs */
/*-no- .......................................................................................................... */
/*-no- #define xmlReconciliateNs xmlReconciliateNs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlRecoverDoc */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRecoverDoc */
/*-no- .................................................................................................. */
/*-no- #define xmlRecoverDoc xmlRecoverDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlRecoverFile */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRecoverFile */
/*-no- .................................................................................................... */
/*-no- #define xmlRecoverFile xmlRecoverFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlRecoverMemory */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlRecoverMemory */
/*-no- ........................................................................................................ */
/*-no- #define xmlRecoverMemory xmlRecoverMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlRegExecErrInfo */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlRegExecErrInfo */
/*-no- .......................................................................................................... */
/*-no- #define xmlRegExecErrInfo xmlRegExecErrInfo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlRegExecNextValues */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRegExecNextValues */
/*-no- ................................................................................................................ */
/*-no- #define xmlRegExecNextValues xmlRegExecNextValues__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlRegExecPushString */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRegExecPushString */
/*-no- ................................................................................................................ */
/*-no- #define xmlRegExecPushString xmlRegExecPushString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlRegExecPushString2 */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlRegExecPushString2 */
/*-no- .................................................................................................................. */
/*-no- #define xmlRegExecPushString2 xmlRegExecPushString2__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlRegFreeExecCtxt */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRegFreeExecCtxt */
/*-no- ............................................................................................................ */
/*-no- #define xmlRegFreeExecCtxt xmlRegFreeExecCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlRegFreeRegexp */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlRegFreeRegexp */
/*-no- ........................................................................................................ */
/*-no- #define xmlRegFreeRegexp xmlRegFreeRegexp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlRegNewExecCtxt */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlRegNewExecCtxt */
/*-no- .......................................................................................................... */
/*-no- #define xmlRegNewExecCtxt xmlRegNewExecCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlRegexpCompile */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlRegexpCompile */
/*-no- ........................................................................................................ */
/*-no- #define xmlRegexpCompile xmlRegexpCompile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlRegexpExec */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRegexpExec */
/*-no- .................................................................................................. */
/*-no- #define xmlRegexpExec xmlRegexpExec__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlRegexpIsDeterminist */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRegexpIsDeterminist */
/*-no- .................................................................................................................... */
/*-no- #define xmlRegexpIsDeterminist xmlRegexpIsDeterminist__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_REGEXP_ENABLED) */
/*-no- #ifdef bottom_xmlregexp */
/*-no- #undef xmlRegexpPrint */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRegexpPrint */
/*-no- .................................................................................................... */
/*-no- #define xmlRegexpPrint xmlRegexpPrint__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_encoding */
/*-no- #undef xmlRegisterCharEncodingHandler */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRegisterCharEncodingHandler */
/*-no- .................................................................................................................................... */
/*-no- #define xmlRegisterCharEncodingHandler xmlRegisterCharEncodingHandler__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlRegisterDefaultInputCallbacks */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlRegisterDefaultInputCallbacks */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlRegisterDefaultInputCallbacks xmlRegisterDefaultInputCallbacks__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlRegisterDefaultOutputCallbacks */
/*-no- ................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlRegisterDefaultOutputCallbacks */
/*-no- .......................................................................................................................................... */
/*-no- #define xmlRegisterDefaultOutputCallbacks xmlRegisterDefaultOutputCallbacks__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) && defined(LIBXML_HTTP_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlRegisterHTTPPostCallbacks */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlRegisterHTTPPostCallbacks */
/*-no- ................................................................................................................................ */
/*-no- #define xmlRegisterHTTPPostCallbacks xmlRegisterHTTPPostCallbacks__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlRegisterInputCallbacks */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlRegisterInputCallbacks */
/*-no- .......................................................................................................................... */
/*-no- #define xmlRegisterInputCallbacks xmlRegisterInputCallbacks__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlRegisterNodeDefault */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRegisterNodeDefault */
/*-no- .................................................................................................................... */
/*-no- #define xmlRegisterNodeDefault xmlRegisterNodeDefault__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlRegisterOutputCallbacks */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRegisterOutputCallbacks */
/*-no- ............................................................................................................................ */
/*-no- #define xmlRegisterOutputCallbacks xmlRegisterOutputCallbacks__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGCleanupTypes */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGCleanupTypes */
/*-no- .................................................................................................................... */
/*-no- #define xmlRelaxNGCleanupTypes xmlRelaxNGCleanupTypes__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGDump */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGDump */
/*-no- .................................................................................................... */
/*-no- #define xmlRelaxNGDump xmlRelaxNGDump__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGDumpTree */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGDumpTree */
/*-no- ............................................................................................................ */
/*-no- #define xmlRelaxNGDumpTree xmlRelaxNGDumpTree__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGFree */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGFree */
/*-no- .................................................................................................... */
/*-no- #define xmlRelaxNGFree xmlRelaxNGFree__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGFreeParserCtxt */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGFreeParserCtxt */
/*-no- ........................................................................................................................ */
/*-no- #define xmlRelaxNGFreeParserCtxt xmlRelaxNGFreeParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGFreeValidCtxt */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGFreeValidCtxt */
/*-no- ...................................................................................................................... */
/*-no- #define xmlRelaxNGFreeValidCtxt xmlRelaxNGFreeValidCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGGetParserErrors */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGGetParserErrors */
/*-no- .......................................................................................................................... */
/*-no- #define xmlRelaxNGGetParserErrors xmlRelaxNGGetParserErrors__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGGetValidErrors */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGGetValidErrors */
/*-no- ........................................................................................................................ */
/*-no- #define xmlRelaxNGGetValidErrors xmlRelaxNGGetValidErrors__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGInitTypes */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGInitTypes */
/*-no- .............................................................................................................. */
/*-no- #define xmlRelaxNGInitTypes xmlRelaxNGInitTypes__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGNewDocParserCtxt */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGNewDocParserCtxt */
/*-no- ............................................................................................................................ */
/*-no- #define xmlRelaxNGNewDocParserCtxt xmlRelaxNGNewDocParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGNewMemParserCtxt */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGNewMemParserCtxt */
/*-no- ............................................................................................................................ */
/*-no- #define xmlRelaxNGNewMemParserCtxt xmlRelaxNGNewMemParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGNewParserCtxt */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGNewParserCtxt */
/*-no- ...................................................................................................................... */
/*-no- #define xmlRelaxNGNewParserCtxt xmlRelaxNGNewParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGNewValidCtxt */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGNewValidCtxt */
/*-no- .................................................................................................................... */
/*-no- #define xmlRelaxNGNewValidCtxt xmlRelaxNGNewValidCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGParse */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGParse */
/*-no- ...................................................................................................... */
/*-no- #define xmlRelaxNGParse xmlRelaxNGParse__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGSetParserErrors */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGSetParserErrors */
/*-no- .......................................................................................................................... */
/*-no- #define xmlRelaxNGSetParserErrors xmlRelaxNGSetParserErrors__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGSetParserStructuredErrors */
/*-no- ...................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGSetParserStructuredErrors */
/*-no- .............................................................................................................................................. */
/*-no- #define xmlRelaxNGSetParserStructuredErrors xmlRelaxNGSetParserStructuredErrors__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGSetValidErrors */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGSetValidErrors */
/*-no- ........................................................................................................................ */
/*-no- #define xmlRelaxNGSetValidErrors xmlRelaxNGSetValidErrors__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGSetValidStructuredErrors */
/*-no- ................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGSetValidStructuredErrors */
/*-no- ............................................................................................................................................ */
/*-no- #define xmlRelaxNGSetValidStructuredErrors xmlRelaxNGSetValidStructuredErrors__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGValidateDoc */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGValidateDoc */
/*-no- .................................................................................................................. */
/*-no- #define xmlRelaxNGValidateDoc xmlRelaxNGValidateDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGValidateFullElement */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGValidateFullElement */
/*-no- .................................................................................................................................. */
/*-no- #define xmlRelaxNGValidateFullElement xmlRelaxNGValidateFullElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGValidatePopElement */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGValidatePopElement */
/*-no- ................................................................................................................................ */
/*-no- #define xmlRelaxNGValidatePopElement xmlRelaxNGValidatePopElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGValidatePushCData */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGValidatePushCData */
/*-no- .............................................................................................................................. */
/*-no- #define xmlRelaxNGValidatePushCData xmlRelaxNGValidatePushCData__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxNGValidatePushElement */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRelaxNGValidatePushElement */
/*-no- .................................................................................................................................. */
/*-no- #define xmlRelaxNGValidatePushElement xmlRelaxNGValidatePushElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_relaxng */
/*-no- #undef xmlRelaxParserSetFlag */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlRelaxParserSetFlag */
/*-no- .................................................................................................................. */
/*-no- #define xmlRelaxParserSetFlag xmlRelaxParserSetFlag__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlRemoveID */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlRemoveID */
/*-no- .............................................................................................. */
/*-no- #define xmlRemoveID xmlRemoveID__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlRemoveProp */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlRemoveProp */
/*-no- .................................................................................................. */
/*-no- #define xmlRemoveProp xmlRemoveProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlRemoveRef */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlRemoveRef */
/*-no- ................................................................................................ */
/*-no- #define xmlRemoveRef xmlRemoveRef__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlReplaceNode */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlReplaceNode */
/*-no- .................................................................................................... */
/*-no- #define xmlReplaceNode xmlReplaceNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_error */
/*-no- #undef xmlResetError */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlResetError */
/*-no- .................................................................................................. */
/*-no- #define xmlResetError xmlResetError__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_error */
/*-no- #undef xmlResetLastError */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlResetLastError */
/*-no- .......................................................................................................... */
/*-no- #define xmlResetLastError xmlResetLastError__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2AttributeDecl */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2AttributeDecl */
/*-no- ................................................................................................................ */
/*-no- #define xmlSAX2AttributeDecl xmlSAX2AttributeDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2CDataBlock */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSAX2CDataBlock */
/*-no- .......................................................................................................... */
/*-no- #define xmlSAX2CDataBlock xmlSAX2CDataBlock__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2Characters */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSAX2Characters */
/*-no- .......................................................................................................... */
/*-no- #define xmlSAX2Characters xmlSAX2Characters__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2Comment */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2Comment */
/*-no- .................................................................................................... */
/*-no- #define xmlSAX2Comment xmlSAX2Comment__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2ElementDecl */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2ElementDecl */
/*-no- ............................................................................................................ */
/*-no- #define xmlSAX2ElementDecl xmlSAX2ElementDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2EndDocument */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2EndDocument */
/*-no- ............................................................................................................ */
/*-no- #define xmlSAX2EndDocument xmlSAX2EndDocument__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) || defined(LIBXML_HTML_ENABLED) || defined(LIBXML_WRITER_ENABLED) || defined(LIBXML_DOCB_ENABLED) */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2EndElement */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSAX2EndElement */
/*-no- .......................................................................................................... */
/*-no- #define xmlSAX2EndElement xmlSAX2EndElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2EndElementNs */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2EndElementNs */
/*-no- .............................................................................................................. */
/*-no- #define xmlSAX2EndElementNs xmlSAX2EndElementNs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2EntityDecl */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSAX2EntityDecl */
/*-no- .......................................................................................................... */
/*-no- #define xmlSAX2EntityDecl xmlSAX2EntityDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2ExternalSubset */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSAX2ExternalSubset */
/*-no- .................................................................................................................. */
/*-no- #define xmlSAX2ExternalSubset xmlSAX2ExternalSubset__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2GetColumnNumber */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2GetColumnNumber */
/*-no- .................................................................................................................... */
/*-no- #define xmlSAX2GetColumnNumber xmlSAX2GetColumnNumber__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2GetEntity */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSAX2GetEntity */
/*-no- ........................................................................................................ */
/*-no- #define xmlSAX2GetEntity xmlSAX2GetEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2GetLineNumber */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2GetLineNumber */
/*-no- ................................................................................................................ */
/*-no- #define xmlSAX2GetLineNumber xmlSAX2GetLineNumber__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2GetParameterEntity */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSAX2GetParameterEntity */
/*-no- .......................................................................................................................... */
/*-no- #define xmlSAX2GetParameterEntity xmlSAX2GetParameterEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2GetPublicId */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2GetPublicId */
/*-no- ............................................................................................................ */
/*-no- #define xmlSAX2GetPublicId xmlSAX2GetPublicId__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2GetSystemId */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2GetSystemId */
/*-no- ............................................................................................................ */
/*-no- #define xmlSAX2GetSystemId xmlSAX2GetSystemId__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2HasExternalSubset */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2HasExternalSubset */
/*-no- ........................................................................................................................ */
/*-no- #define xmlSAX2HasExternalSubset xmlSAX2HasExternalSubset__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2HasInternalSubset */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2HasInternalSubset */
/*-no- ........................................................................................................................ */
/*-no- #define xmlSAX2HasInternalSubset xmlSAX2HasInternalSubset__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2IgnorableWhitespace */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2IgnorableWhitespace */
/*-no- ............................................................................................................................ */
/*-no- #define xmlSAX2IgnorableWhitespace xmlSAX2IgnorableWhitespace__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2InitDefaultSAXHandler */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSAX2InitDefaultSAXHandler */
/*-no- ................................................................................................................................ */
/*-no- #define xmlSAX2InitDefaultSAXHandler xmlSAX2InitDefaultSAXHandler__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DOCB_ENABLED) */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2InitDocbDefaultSAXHandler */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSAX2InitDocbDefaultSAXHandler */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlSAX2InitDocbDefaultSAXHandler xmlSAX2InitDocbDefaultSAXHandler__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2InitHtmlDefaultSAXHandler */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSAX2InitHtmlDefaultSAXHandler */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlSAX2InitHtmlDefaultSAXHandler xmlSAX2InitHtmlDefaultSAXHandler__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2InternalSubset */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSAX2InternalSubset */
/*-no- .................................................................................................................. */
/*-no- #define xmlSAX2InternalSubset xmlSAX2InternalSubset__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2IsStandalone */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2IsStandalone */
/*-no- .............................................................................................................. */
/*-no- #define xmlSAX2IsStandalone xmlSAX2IsStandalone__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2NotationDecl */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2NotationDecl */
/*-no- .............................................................................................................. */
/*-no- #define xmlSAX2NotationDecl xmlSAX2NotationDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2ProcessingInstruction */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSAX2ProcessingInstruction */
/*-no- ................................................................................................................................ */
/*-no- #define xmlSAX2ProcessingInstruction xmlSAX2ProcessingInstruction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2Reference */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSAX2Reference */
/*-no- ........................................................................................................ */
/*-no- #define xmlSAX2Reference xmlSAX2Reference__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2ResolveEntity */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2ResolveEntity */
/*-no- ................................................................................................................ */
/*-no- #define xmlSAX2ResolveEntity xmlSAX2ResolveEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2SetDocumentLocator */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSAX2SetDocumentLocator */
/*-no- .......................................................................................................................... */
/*-no- #define xmlSAX2SetDocumentLocator xmlSAX2SetDocumentLocator__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2StartDocument */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2StartDocument */
/*-no- ................................................................................................................ */
/*-no- #define xmlSAX2StartDocument xmlSAX2StartDocument__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) || defined(LIBXML_HTML_ENABLED) || defined(LIBXML_WRITER_ENABLED) || defined(LIBXML_DOCB_ENABLED) */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2StartElement */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAX2StartElement */
/*-no- .............................................................................................................. */
/*-no- #define xmlSAX2StartElement xmlSAX2StartElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2StartElementNs */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSAX2StartElementNs */
/*-no- .................................................................................................................. */
/*-no- #define xmlSAX2StartElementNs xmlSAX2StartElementNs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAX2UnparsedEntityDecl */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSAX2UnparsedEntityDecl */
/*-no- .......................................................................................................................... */
/*-no- #define xmlSAX2UnparsedEntityDecl xmlSAX2UnparsedEntityDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAXDefaultVersion */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAXDefaultVersion */
/*-no- ................................................................................................................ */
/*-no- #define xmlSAXDefaultVersion xmlSAXDefaultVersion__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlSAXParseDTD */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAXParseDTD */
/*-no- .................................................................................................... */
/*-no- #define xmlSAXParseDTD xmlSAXParseDTD__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlSAXParseDoc */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAXParseDoc */
/*-no- .................................................................................................... */
/*-no- #define xmlSAXParseDoc xmlSAXParseDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlSAXParseEntity */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSAXParseEntity */
/*-no- .......................................................................................................... */
/*-no- #define xmlSAXParseEntity xmlSAXParseEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlSAXParseFile */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAXParseFile */
/*-no- ...................................................................................................... */
/*-no- #define xmlSAXParseFile xmlSAXParseFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlSAXParseFileWithData */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSAXParseFileWithData */
/*-no- ...................................................................................................................... */
/*-no- #define xmlSAXParseFileWithData xmlSAXParseFileWithData__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlSAXParseMemory */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSAXParseMemory */
/*-no- .......................................................................................................... */
/*-no- #define xmlSAXParseMemory xmlSAXParseMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlSAXParseMemoryWithData */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSAXParseMemoryWithData */
/*-no- .......................................................................................................................... */
/*-no- #define xmlSAXParseMemoryWithData xmlSAXParseMemoryWithData__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlSAXUserParseFile */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAXUserParseFile */
/*-no- .............................................................................................................. */
/*-no- #define xmlSAXUserParseFile xmlSAXUserParseFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlSAXUserParseMemory */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSAXUserParseMemory */
/*-no- .................................................................................................................. */
/*-no- #define xmlSAXUserParseMemory xmlSAXUserParseMemory__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_SAX2 */
/*-no- #undef xmlSAXVersion */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSAXVersion */
/*-no- .................................................................................................. */
/*-no- #define xmlSAXVersion xmlSAXVersion__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveClose */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSaveClose */
/*-no- ................................................................................................ */
/*-no- #define xmlSaveClose xmlSaveClose__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveDoc */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSaveDoc */
/*-no- ............................................................................................ */
/*-no- #define xmlSaveDoc xmlSaveDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveFile */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSaveFile */
/*-no- .............................................................................................. */
/*-no- #define xmlSaveFile xmlSaveFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveFileEnc */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSaveFileEnc */
/*-no- .................................................................................................... */
/*-no- #define xmlSaveFileEnc xmlSaveFileEnc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveFileTo */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSaveFileTo */
/*-no- .................................................................................................. */
/*-no- #define xmlSaveFileTo xmlSaveFileTo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveFlush */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSaveFlush */
/*-no- ................................................................................................ */
/*-no- #define xmlSaveFlush xmlSaveFlush__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveFormatFile */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSaveFormatFile */
/*-no- .......................................................................................................... */
/*-no- #define xmlSaveFormatFile xmlSaveFormatFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveFormatFileEnc */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSaveFormatFileEnc */
/*-no- ................................................................................................................ */
/*-no- #define xmlSaveFormatFileEnc xmlSaveFormatFileEnc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveFormatFileTo */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSaveFormatFileTo */
/*-no- .............................................................................................................. */
/*-no- #define xmlSaveFormatFileTo xmlSaveFormatFileTo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveSetAttrEscape */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSaveSetAttrEscape */
/*-no- ................................................................................................................ */
/*-no- #define xmlSaveSetAttrEscape xmlSaveSetAttrEscape__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveSetEscape */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSaveSetEscape */
/*-no- ........................................................................................................ */
/*-no- #define xmlSaveSetEscape xmlSaveSetEscape__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveToBuffer */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSaveToBuffer */
/*-no- ...................................................................................................... */
/*-no- #define xmlSaveToBuffer xmlSaveToBuffer__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveToFd */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSaveToFd */
/*-no- .............................................................................................. */
/*-no- #define xmlSaveToFd xmlSaveToFd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveToFilename */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSaveToFilename */
/*-no- .......................................................................................................... */
/*-no- #define xmlSaveToFilename xmlSaveToFilename__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveToIO */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSaveToIO */
/*-no- .............................................................................................. */
/*-no- #define xmlSaveToIO xmlSaveToIO__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlsave */
/*-no- #undef xmlSaveTree */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSaveTree */
/*-no- .............................................................................................. */
/*-no- #define xmlSaveTree xmlSaveTree__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_uri */
/*-no- #undef xmlSaveUri */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSaveUri */
/*-no- ............................................................................................ */
/*-no- #define xmlSaveUri xmlSaveUri__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlScanName */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlScanName */
/*-no- .............................................................................................. */
/*-no- #define xmlScanName xmlScanName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaCheckFacet */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaCheckFacet */
/*-no- .............................................................................................................. */
/*-no- #define xmlSchemaCheckFacet xmlSchemaCheckFacet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaCleanupTypes */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchemaCleanupTypes */
/*-no- .................................................................................................................. */
/*-no- #define xmlSchemaCleanupTypes xmlSchemaCleanupTypes__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaCollapseString */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaCollapseString */
/*-no- ...................................................................................................................... */
/*-no- #define xmlSchemaCollapseString xmlSchemaCollapseString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaCompareValues */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaCompareValues */
/*-no- .................................................................................................................... */
/*-no- #define xmlSchemaCompareValues xmlSchemaCompareValues__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaCompareValuesWhtsp */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaCompareValuesWhtsp */
/*-no- .............................................................................................................................. */
/*-no- #define xmlSchemaCompareValuesWhtsp xmlSchemaCompareValuesWhtsp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaCopyValue */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaCopyValue */
/*-no- ............................................................................................................ */
/*-no- #define xmlSchemaCopyValue xmlSchemaCopyValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaDump */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaDump */
/*-no- .................................................................................................. */
/*-no- #define xmlSchemaDump xmlSchemaDump__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaFree */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaFree */
/*-no- .................................................................................................. */
/*-no- #define xmlSchemaFree xmlSchemaFree__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaFreeFacet */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaFreeFacet */
/*-no- ............................................................................................................ */
/*-no- #define xmlSchemaFreeFacet xmlSchemaFreeFacet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaFreeParserCtxt */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaFreeParserCtxt */
/*-no- ...................................................................................................................... */
/*-no- #define xmlSchemaFreeParserCtxt xmlSchemaFreeParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaFreeType */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchemaFreeType */
/*-no- .......................................................................................................... */
/*-no- #define xmlSchemaFreeType xmlSchemaFreeType__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaFreeValidCtxt */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaFreeValidCtxt */
/*-no- .................................................................................................................... */
/*-no- #define xmlSchemaFreeValidCtxt xmlSchemaFreeValidCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaFreeValue */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaFreeValue */
/*-no- ............................................................................................................ */
/*-no- #define xmlSchemaFreeValue xmlSchemaFreeValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaFreeWildcard */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchemaFreeWildcard */
/*-no- .................................................................................................................. */
/*-no- #define xmlSchemaFreeWildcard xmlSchemaFreeWildcard__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaGetBuiltInListSimpleTypeItemType */
/*-no- ........................................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchemaGetBuiltInListSimpleTypeItemType */
/*-no- .......................................................................................................................................................... */
/*-no- #define xmlSchemaGetBuiltInListSimpleTypeItemType xmlSchemaGetBuiltInListSimpleTypeItemType__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaGetBuiltInType */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaGetBuiltInType */
/*-no- ...................................................................................................................... */
/*-no- #define xmlSchemaGetBuiltInType xmlSchemaGetBuiltInType__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaGetCanonValue */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaGetCanonValue */
/*-no- .................................................................................................................... */
/*-no- #define xmlSchemaGetCanonValue xmlSchemaGetCanonValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaGetCanonValueWhtsp */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaGetCanonValueWhtsp */
/*-no- .............................................................................................................................. */
/*-no- #define xmlSchemaGetCanonValueWhtsp xmlSchemaGetCanonValueWhtsp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaGetFacetValueAsULong */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaGetFacetValueAsULong */
/*-no- .................................................................................................................................. */
/*-no- #define xmlSchemaGetFacetValueAsULong xmlSchemaGetFacetValueAsULong__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaGetParserErrors */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaGetParserErrors */
/*-no- ........................................................................................................................ */
/*-no- #define xmlSchemaGetParserErrors xmlSchemaGetParserErrors__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaGetPredefinedType */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaGetPredefinedType */
/*-no- ............................................................................................................................ */
/*-no- #define xmlSchemaGetPredefinedType xmlSchemaGetPredefinedType__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaGetValType */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaGetValType */
/*-no- .............................................................................................................. */
/*-no- #define xmlSchemaGetValType xmlSchemaGetValType__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaGetValidErrors */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaGetValidErrors */
/*-no- ...................................................................................................................... */
/*-no- #define xmlSchemaGetValidErrors xmlSchemaGetValidErrors__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaInitTypes */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaInitTypes */
/*-no- ............................................................................................................ */
/*-no- #define xmlSchemaInitTypes xmlSchemaInitTypes__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaIsBuiltInTypeFacet */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaIsBuiltInTypeFacet */
/*-no- .............................................................................................................................. */
/*-no- #define xmlSchemaIsBuiltInTypeFacet xmlSchemaIsBuiltInTypeFacet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaIsValid */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaIsValid */
/*-no- ........................................................................................................ */
/*-no- #define xmlSchemaIsValid xmlSchemaIsValid__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaNewDocParserCtxt */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchemaNewDocParserCtxt */
/*-no- .......................................................................................................................... */
/*-no- #define xmlSchemaNewDocParserCtxt xmlSchemaNewDocParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaNewFacet */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchemaNewFacet */
/*-no- .......................................................................................................... */
/*-no- #define xmlSchemaNewFacet xmlSchemaNewFacet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaNewMemParserCtxt */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchemaNewMemParserCtxt */
/*-no- .......................................................................................................................... */
/*-no- #define xmlSchemaNewMemParserCtxt xmlSchemaNewMemParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaNewNOTATIONValue */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchemaNewNOTATIONValue */
/*-no- .......................................................................................................................... */
/*-no- #define xmlSchemaNewNOTATIONValue xmlSchemaNewNOTATIONValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaNewParserCtxt */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaNewParserCtxt */
/*-no- .................................................................................................................... */
/*-no- #define xmlSchemaNewParserCtxt xmlSchemaNewParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaNewQNameValue */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaNewQNameValue */
/*-no- .................................................................................................................... */
/*-no- #define xmlSchemaNewQNameValue xmlSchemaNewQNameValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaNewStringValue */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaNewStringValue */
/*-no- ...................................................................................................................... */
/*-no- #define xmlSchemaNewStringValue xmlSchemaNewStringValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaNewValidCtxt */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchemaNewValidCtxt */
/*-no- .................................................................................................................. */
/*-no- #define xmlSchemaNewValidCtxt xmlSchemaNewValidCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaParse */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaParse */
/*-no- .................................................................................................... */
/*-no- #define xmlSchemaParse xmlSchemaParse__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaSAXPlug */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaSAXPlug */
/*-no- ........................................................................................................ */
/*-no- #define xmlSchemaSAXPlug xmlSchemaSAXPlug__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaSAXUnplug */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaSAXUnplug */
/*-no- ............................................................................................................ */
/*-no- #define xmlSchemaSAXUnplug xmlSchemaSAXUnplug__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaSetParserErrors */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaSetParserErrors */
/*-no- ........................................................................................................................ */
/*-no- #define xmlSchemaSetParserErrors xmlSchemaSetParserErrors__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaSetParserStructuredErrors */
/*-no- ................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaSetParserStructuredErrors */
/*-no- ............................................................................................................................................ */
/*-no- #define xmlSchemaSetParserStructuredErrors xmlSchemaSetParserStructuredErrors__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaSetValidErrors */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaSetValidErrors */
/*-no- ...................................................................................................................... */
/*-no- #define xmlSchemaSetValidErrors xmlSchemaSetValidErrors__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaSetValidOptions */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaSetValidOptions */
/*-no- ........................................................................................................................ */
/*-no- #define xmlSchemaSetValidOptions xmlSchemaSetValidOptions__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaSetValidStructuredErrors */
/*-no- ................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchemaSetValidStructuredErrors */
/*-no- .......................................................................................................................................... */
/*-no- #define xmlSchemaSetValidStructuredErrors xmlSchemaSetValidStructuredErrors__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaValPredefTypeNode */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaValPredefTypeNode */
/*-no- ............................................................................................................................ */
/*-no- #define xmlSchemaValPredefTypeNode xmlSchemaValPredefTypeNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaValPredefTypeNodeNoNorm */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaValPredefTypeNodeNoNorm */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlSchemaValPredefTypeNodeNoNorm xmlSchemaValPredefTypeNodeNoNorm__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaValidCtxtGetOptions */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaValidCtxtGetOptions */
/*-no- ................................................................................................................................ */
/*-no- #define xmlSchemaValidCtxtGetOptions xmlSchemaValidCtxtGetOptions__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaValidCtxtGetParserCtxt */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaValidCtxtGetParserCtxt */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlSchemaValidCtxtGetParserCtxt xmlSchemaValidCtxtGetParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaValidateDoc */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaValidateDoc */
/*-no- ................................................................................................................ */
/*-no- #define xmlSchemaValidateDoc xmlSchemaValidateDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaValidateFacet */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaValidateFacet */
/*-no- .................................................................................................................... */
/*-no- #define xmlSchemaValidateFacet xmlSchemaValidateFacet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaValidateFacetWhtsp */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaValidateFacetWhtsp */
/*-no- .............................................................................................................................. */
/*-no- #define xmlSchemaValidateFacetWhtsp xmlSchemaValidateFacetWhtsp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaValidateFile */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchemaValidateFile */
/*-no- .................................................................................................................. */
/*-no- #define xmlSchemaValidateFile xmlSchemaValidateFile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaValidateLengthFacet */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaValidateLengthFacet */
/*-no- ................................................................................................................................ */
/*-no- #define xmlSchemaValidateLengthFacet xmlSchemaValidateLengthFacet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaValidateLengthFacetWhtsp */
/*-no- ................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchemaValidateLengthFacetWhtsp */
/*-no- .......................................................................................................................................... */
/*-no- #define xmlSchemaValidateLengthFacetWhtsp xmlSchemaValidateLengthFacetWhtsp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaValidateListSimpleTypeFacet */
/*-no- ......................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaValidateListSimpleTypeFacet */
/*-no- ................................................................................................................................................ */
/*-no- #define xmlSchemaValidateListSimpleTypeFacet xmlSchemaValidateListSimpleTypeFacet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaValidateOneElement */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaValidateOneElement */
/*-no- .............................................................................................................................. */
/*-no- #define xmlSchemaValidateOneElement xmlSchemaValidateOneElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaValidatePredefinedType */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaValidatePredefinedType */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlSchemaValidatePredefinedType xmlSchemaValidatePredefinedType__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemas */
/*-no- #undef xmlSchemaValidateStream */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchemaValidateStream */
/*-no- ...................................................................................................................... */
/*-no- #define xmlSchemaValidateStream xmlSchemaValidateStream__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaValueAppend */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaValueAppend */
/*-no- ................................................................................................................ */
/*-no- #define xmlSchemaValueAppend xmlSchemaValueAppend__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaValueGetAsBoolean */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaValueGetAsBoolean */
/*-no- ............................................................................................................................ */
/*-no- #define xmlSchemaValueGetAsBoolean xmlSchemaValueGetAsBoolean__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaValueGetAsString */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchemaValueGetAsString */
/*-no- .......................................................................................................................... */
/*-no- #define xmlSchemaValueGetAsString xmlSchemaValueGetAsString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaValueGetNext */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchemaValueGetNext */
/*-no- .................................................................................................................. */
/*-no- #define xmlSchemaValueGetNext xmlSchemaValueGetNext__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlschemastypes */
/*-no- #undef xmlSchemaWhiteSpaceReplace */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchemaWhiteSpaceReplace */
/*-no- ............................................................................................................................ */
/*-no- #define xmlSchemaWhiteSpaceReplace xmlSchemaWhiteSpaceReplace__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMATRON_ENABLED) */
/*-no- #ifdef bottom_schematron */
/*-no- #undef xmlSchematronFree */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchematronFree */
/*-no- .......................................................................................................... */
/*-no- #define xmlSchematronFree xmlSchematronFree__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMATRON_ENABLED) */
/*-no- #ifdef bottom_schematron */
/*-no- #undef xmlSchematronFreeParserCtxt */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSchematronFreeParserCtxt */
/*-no- .............................................................................................................................. */
/*-no- #define xmlSchematronFreeParserCtxt xmlSchematronFreeParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMATRON_ENABLED) */
/*-no- #ifdef bottom_schematron */
/*-no- #undef xmlSchematronFreeValidCtxt */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchematronFreeValidCtxt */
/*-no- ............................................................................................................................ */
/*-no- #define xmlSchematronFreeValidCtxt xmlSchematronFreeValidCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMATRON_ENABLED) */
/*-no- #ifdef bottom_schematron */
/*-no- #undef xmlSchematronNewDocParserCtxt */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchematronNewDocParserCtxt */
/*-no- .................................................................................................................................. */
/*-no- #define xmlSchematronNewDocParserCtxt xmlSchematronNewDocParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMATRON_ENABLED) */
/*-no- #ifdef bottom_schematron */
/*-no- #undef xmlSchematronNewMemParserCtxt */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchematronNewMemParserCtxt */
/*-no- .................................................................................................................................. */
/*-no- #define xmlSchematronNewMemParserCtxt xmlSchematronNewMemParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMATRON_ENABLED) */
/*-no- #ifdef bottom_schematron */
/*-no- #undef xmlSchematronNewParserCtxt */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchematronNewParserCtxt */
/*-no- ............................................................................................................................ */
/*-no- #define xmlSchematronNewParserCtxt xmlSchematronNewParserCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMATRON_ENABLED) */
/*-no- #ifdef bottom_schematron */
/*-no- #undef xmlSchematronNewValidCtxt */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchematronNewValidCtxt */
/*-no- .......................................................................................................................... */
/*-no- #define xmlSchematronNewValidCtxt xmlSchematronNewValidCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMATRON_ENABLED) */
/*-no- #ifdef bottom_schematron */
/*-no- #undef xmlSchematronParse */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchematronParse */
/*-no- ............................................................................................................ */
/*-no- #define xmlSchematronParse xmlSchematronParse__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMATRON_ENABLED) */
/*-no- #ifdef bottom_schematron */
/*-no- #undef xmlSchematronSetValidStructuredErrors */
/*-no- ............................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSchematronSetValidStructuredErrors */
/*-no- .................................................................................................................................................. */
/*-no- #define xmlSchematronSetValidStructuredErrors xmlSchematronSetValidStructuredErrors__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SCHEMATRON_ENABLED) */
/*-no- #ifdef bottom_schematron */
/*-no- #undef xmlSchematronValidateDoc */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSchematronValidateDoc */
/*-no- ........................................................................................................................ */
/*-no- #define xmlSchematronValidateDoc xmlSchematronValidateDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlSearchNs */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSearchNs */
/*-no- .............................................................................................. */
/*-no- #define xmlSearchNs xmlSearchNs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlSearchNsByHref */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSearchNsByHref */
/*-no- .......................................................................................................... */
/*-no- #define xmlSearchNsByHref xmlSearchNsByHref__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlSetBufferAllocationScheme */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSetBufferAllocationScheme */
/*-no- ................................................................................................................................ */
/*-no- #define xmlSetBufferAllocationScheme xmlSetBufferAllocationScheme__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlSetCompressMode */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSetCompressMode */
/*-no- ............................................................................................................ */
/*-no- #define xmlSetCompressMode xmlSetCompressMode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlSetDocCompressMode */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSetDocCompressMode */
/*-no- .................................................................................................................. */
/*-no- #define xmlSetDocCompressMode xmlSetDocCompressMode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlSetEntityReferenceFunc */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSetEntityReferenceFunc */
/*-no- .......................................................................................................................... */
/*-no- #define xmlSetEntityReferenceFunc xmlSetEntityReferenceFunc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlIO */
/*-no- #undef xmlSetExternalEntityLoader */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSetExternalEntityLoader */
/*-no- ............................................................................................................................ */
/*-no- #define xmlSetExternalEntityLoader xmlSetExternalEntityLoader__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_LEGACY_ENABLED) */
/*-no- #ifdef bottom_legacy */
/*-no- #undef xmlSetFeature */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSetFeature */
/*-no- .................................................................................................. */
/*-no- #define xmlSetFeature xmlSetFeature__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_error */
/*-no- #undef xmlSetGenericErrorFunc */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSetGenericErrorFunc */
/*-no- .................................................................................................................... */
/*-no- #define xmlSetGenericErrorFunc xmlSetGenericErrorFunc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlSetListDoc */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSetListDoc */
/*-no- .................................................................................................. */
/*-no- #define xmlSetListDoc xmlSetListDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlSetNs */
/*-no- ..................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSetNs */
/*-no- ........................................................................................ */
/*-no- #define xmlSetNs xmlSetNs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_XINCLUDE_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) || defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlSetNsProp */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSetNsProp */
/*-no- ................................................................................................ */
/*-no- #define xmlSetNsProp xmlSetNsProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_XINCLUDE_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) || defined(LIBXML_HTML_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlSetProp */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSetProp */
/*-no- ............................................................................................ */
/*-no- #define xmlSetProp xmlSetProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_error */
/*-no- #undef xmlSetStructuredErrorFunc */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSetStructuredErrorFunc */
/*-no- .......................................................................................................................... */
/*-no- #define xmlSetStructuredErrorFunc xmlSetStructuredErrorFunc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlSetTreeDoc */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSetTreeDoc */
/*-no- .................................................................................................. */
/*-no- #define xmlSetTreeDoc xmlSetTreeDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_SAX1_ENABLED) */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlSetupParserForBuffer */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSetupParserForBuffer */
/*-no- ...................................................................................................................... */
/*-no- #define xmlSetupParserForBuffer xmlSetupParserForBuffer__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) && defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlShell */
/*-no- ..................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlShell */
/*-no- ........................................................................................ */
/*-no- #define xmlShell xmlShell__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) && defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlShellBase */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlShellBase */
/*-no- ................................................................................................ */
/*-no- #define xmlShellBase xmlShellBase__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) && defined(LIBXML_XPATH_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlShellCat */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlShellCat */
/*-no- .............................................................................................. */
/*-no- #define xmlShellCat xmlShellCat__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) && defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlShellDir */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlShellDir */
/*-no- .............................................................................................. */
/*-no- #define xmlShellDir xmlShellDir__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) && defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlShellDu */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlShellDu */
/*-no- ............................................................................................ */
/*-no- #define xmlShellDu xmlShellDu__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) && defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlShellList */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlShellList */
/*-no- ................................................................................................ */
/*-no- #define xmlShellList xmlShellList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) && defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlShellLoad */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlShellLoad */
/*-no- ................................................................................................ */
/*-no- #define xmlShellLoad xmlShellLoad__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) && defined(LIBXML_XPATH_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlShellPrintNode */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlShellPrintNode */
/*-no- .......................................................................................................... */
/*-no- #define xmlShellPrintNode xmlShellPrintNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) && defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlShellPrintXPathError */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlShellPrintXPathError */
/*-no- ...................................................................................................................... */
/*-no- #define xmlShellPrintXPathError xmlShellPrintXPathError__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) && defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlShellPrintXPathResult */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlShellPrintXPathResult */
/*-no- ........................................................................................................................ */
/*-no- #define xmlShellPrintXPathResult xmlShellPrintXPathResult__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) && defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlShellPwd */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlShellPwd */
/*-no- .............................................................................................. */
/*-no- #define xmlShellPwd xmlShellPwd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) && defined(LIBXML_XPATH_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlShellSave */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlShellSave */
/*-no- ................................................................................................ */
/*-no- #define xmlShellSave xmlShellSave__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) && defined(LIBXML_XPATH_ENABLED) && defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlShellValidate */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlShellValidate */
/*-no- ........................................................................................................ */
/*-no- #define xmlShellValidate xmlShellValidate__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_DEBUG_ENABLED) && defined(LIBXML_XPATH_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_debugXML */
/*-no- #undef xmlShellWrite */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlShellWrite */
/*-no- .................................................................................................. */
/*-no- #define xmlShellWrite xmlShellWrite__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlSkipBlankChars */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSkipBlankChars */
/*-no- .......................................................................................................... */
/*-no- #define xmlSkipBlankChars xmlSkipBlankChars__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlSnprintfElementContent */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSnprintfElementContent */
/*-no- .......................................................................................................................... */
/*-no- #define xmlSnprintfElementContent xmlSnprintfElementContent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlSplitQName */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSplitQName */
/*-no- .................................................................................................. */
/*-no- #define xmlSplitQName xmlSplitQName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlSplitQName2 */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSplitQName2 */
/*-no- .................................................................................................... */
/*-no- #define xmlSplitQName2 xmlSplitQName2__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlSplitQName3 */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSplitQName3 */
/*-no- .................................................................................................... */
/*-no- #define xmlSplitQName3 xmlSplitQName3__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_OUTPUT_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlSprintfElementContent */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSprintfElementContent */
/*-no- ........................................................................................................................ */
/*-no- #define xmlSprintfElementContent xmlSprintfElementContent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlStopParser */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlStopParser */
/*-no- .................................................................................................. */
/*-no- #define xmlStopParser xmlStopParser__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrEqual */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlStrEqual */
/*-no- .............................................................................................. */
/*-no- #define xmlStrEqual xmlStrEqual__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrPrintf */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlStrPrintf */
/*-no- ................................................................................................ */
/*-no- #define xmlStrPrintf xmlStrPrintf__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrQEqual */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlStrQEqual */
/*-no- ................................................................................................ */
/*-no- #define xmlStrQEqual xmlStrQEqual__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrVPrintf */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlStrVPrintf */
/*-no- .................................................................................................. */
/*-no- #define xmlStrVPrintf xmlStrVPrintf__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrcasecmp */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlStrcasecmp */
/*-no- .................................................................................................. */
/*-no- #define xmlStrcasecmp xmlStrcasecmp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrcasestr */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlStrcasestr */
/*-no- .................................................................................................. */
/*-no- #define xmlStrcasestr xmlStrcasestr__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrcat */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlStrcat */
/*-no- .......................................................................................... */
/*-no- #define xmlStrcat xmlStrcat__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrchr */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlStrchr */
/*-no- .......................................................................................... */
/*-no- #define xmlStrchr xmlStrchr__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrcmp */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlStrcmp */
/*-no- .......................................................................................... */
/*-no- #define xmlStrcmp xmlStrcmp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrdup */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlStrdup */
/*-no- .......................................................................................... */
/*-no- #define xmlStrdup xmlStrdup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_pattern */
/*-no- #undef xmlStreamPop */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlStreamPop */
/*-no- ................................................................................................ */
/*-no- #define xmlStreamPop xmlStreamPop__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_pattern */
/*-no- #undef xmlStreamPush */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlStreamPush */
/*-no- .................................................................................................. */
/*-no- #define xmlStreamPush xmlStreamPush__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_pattern */
/*-no- #undef xmlStreamPushAttr */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlStreamPushAttr */
/*-no- .......................................................................................................... */
/*-no- #define xmlStreamPushAttr xmlStreamPushAttr__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_pattern */
/*-no- #undef xmlStreamPushNode */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlStreamPushNode */
/*-no- .......................................................................................................... */
/*-no- #define xmlStreamPushNode xmlStreamPushNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_pattern */
/*-no- #undef xmlStreamWantsAnyNode */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlStreamWantsAnyNode */
/*-no- .................................................................................................................. */
/*-no- #define xmlStreamWantsAnyNode xmlStreamWantsAnyNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlStringCurrentChar */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlStringCurrentChar */
/*-no- ................................................................................................................ */
/*-no- #define xmlStringCurrentChar xmlStringCurrentChar__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlStringDecodeEntities */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlStringDecodeEntities */
/*-no- ...................................................................................................................... */
/*-no- #define xmlStringDecodeEntities xmlStringDecodeEntities__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlStringGetNodeList */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlStringGetNodeList */
/*-no- ................................................................................................................ */
/*-no- #define xmlStringGetNodeList xmlStringGetNodeList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parser */
/*-no- #undef xmlStringLenDecodeEntities */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlStringLenDecodeEntities */
/*-no- ............................................................................................................................ */
/*-no- #define xmlStringLenDecodeEntities xmlStringLenDecodeEntities__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlStringLenGetNodeList */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlStringLenGetNodeList */
/*-no- ...................................................................................................................... */
/*-no- #define xmlStringLenGetNodeList xmlStringLenGetNodeList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrlen */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlStrlen */
/*-no- .......................................................................................... */
/*-no- #define xmlStrlen xmlStrlen__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrncasecmp */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlStrncasecmp */
/*-no- .................................................................................................... */
/*-no- #define xmlStrncasecmp xmlStrncasecmp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrncat */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlStrncat */
/*-no- ............................................................................................ */
/*-no- #define xmlStrncat xmlStrncat__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrncatNew */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlStrncatNew */
/*-no- .................................................................................................. */
/*-no- #define xmlStrncatNew xmlStrncatNew__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrncmp */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlStrncmp */
/*-no- ............................................................................................ */
/*-no- #define xmlStrncmp xmlStrncmp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrndup */
/*-no- ........................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlStrndup */
/*-no- ............................................................................................ */
/*-no- #define xmlStrndup xmlStrndup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrstr */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlStrstr */
/*-no- .......................................................................................... */
/*-no- #define xmlStrstr xmlStrstr__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlStrsub */
/*-no- ........................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlStrsub */
/*-no- .......................................................................................... */
/*-no- #define xmlStrsub xmlStrsub__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlSubstituteEntitiesDefault */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlSubstituteEntitiesDefault */
/*-no- ................................................................................................................................ */
/*-no- #define xmlSubstituteEntitiesDefault xmlSubstituteEntitiesDefault__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlSwitchEncoding */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlSwitchEncoding */
/*-no- .......................................................................................................... */
/*-no- #define xmlSwitchEncoding xmlSwitchEncoding__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlSwitchInputEncoding */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSwitchInputEncoding */
/*-no- .................................................................................................................... */
/*-no- #define xmlSwitchInputEncoding xmlSwitchInputEncoding__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_parserInternals */
/*-no- #undef xmlSwitchToEncoding */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlSwitchToEncoding */
/*-no- .............................................................................................................. */
/*-no- #define xmlSwitchToEncoding xmlSwitchToEncoding__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlTextConcat */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextConcat */
/*-no- .................................................................................................. */
/*-no- #define xmlTextConcat xmlTextConcat__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlTextMerge */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextMerge */
/*-no- ................................................................................................ */
/*-no- #define xmlTextMerge xmlTextMerge__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderAttributeCount */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderAttributeCount */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextReaderAttributeCount xmlTextReaderAttributeCount__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderBaseUri */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderBaseUri */
/*-no- ................................................................................................................ */
/*-no- #define xmlTextReaderBaseUri xmlTextReaderBaseUri__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderByteConsumed */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextReaderByteConsumed */
/*-no- .......................................................................................................................... */
/*-no- #define xmlTextReaderByteConsumed xmlTextReaderByteConsumed__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderClose */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderClose */
/*-no- ............................................................................................................ */
/*-no- #define xmlTextReaderClose xmlTextReaderClose__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderConstBaseUri */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextReaderConstBaseUri */
/*-no- .......................................................................................................................... */
/*-no- #define xmlTextReaderConstBaseUri xmlTextReaderConstBaseUri__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderConstEncoding */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderConstEncoding */
/*-no- ............................................................................................................................ */
/*-no- #define xmlTextReaderConstEncoding xmlTextReaderConstEncoding__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderConstLocalName */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderConstLocalName */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextReaderConstLocalName xmlTextReaderConstLocalName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderConstName */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderConstName */
/*-no- .................................................................................................................... */
/*-no- #define xmlTextReaderConstName xmlTextReaderConstName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderConstNamespaceUri */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderConstNamespaceUri */
/*-no- .................................................................................................................................... */
/*-no- #define xmlTextReaderConstNamespaceUri xmlTextReaderConstNamespaceUri__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderConstPrefix */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderConstPrefix */
/*-no- ........................................................................................................................ */
/*-no- #define xmlTextReaderConstPrefix xmlTextReaderConstPrefix__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderConstString */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderConstString */
/*-no- ........................................................................................................................ */
/*-no- #define xmlTextReaderConstString xmlTextReaderConstString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderConstValue */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderConstValue */
/*-no- ...................................................................................................................... */
/*-no- #define xmlTextReaderConstValue xmlTextReaderConstValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderConstXmlLang */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextReaderConstXmlLang */
/*-no- .......................................................................................................................... */
/*-no- #define xmlTextReaderConstXmlLang xmlTextReaderConstXmlLang__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderConstXmlVersion */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderConstXmlVersion */
/*-no- ................................................................................................................................ */
/*-no- #define xmlTextReaderConstXmlVersion xmlTextReaderConstXmlVersion__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderCurrentDoc */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderCurrentDoc */
/*-no- ...................................................................................................................... */
/*-no- #define xmlTextReaderCurrentDoc xmlTextReaderCurrentDoc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderCurrentNode */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderCurrentNode */
/*-no- ........................................................................................................................ */
/*-no- #define xmlTextReaderCurrentNode xmlTextReaderCurrentNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderDepth */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderDepth */
/*-no- ............................................................................................................ */
/*-no- #define xmlTextReaderDepth xmlTextReaderDepth__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderExpand */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderExpand */
/*-no- .............................................................................................................. */
/*-no- #define xmlTextReaderExpand xmlTextReaderExpand__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderGetAttribute */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextReaderGetAttribute */
/*-no- .......................................................................................................................... */
/*-no- #define xmlTextReaderGetAttribute xmlTextReaderGetAttribute__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderGetAttributeNo */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderGetAttributeNo */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextReaderGetAttributeNo xmlTextReaderGetAttributeNo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderGetAttributeNs */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderGetAttributeNs */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextReaderGetAttributeNs xmlTextReaderGetAttributeNs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderGetErrorHandler */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderGetErrorHandler */
/*-no- ................................................................................................................................ */
/*-no- #define xmlTextReaderGetErrorHandler xmlTextReaderGetErrorHandler__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderGetParserColumnNumber */
/*-no- ................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderGetParserColumnNumber */
/*-no- ............................................................................................................................................ */
/*-no- #define xmlTextReaderGetParserColumnNumber xmlTextReaderGetParserColumnNumber__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderGetParserLineNumber */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderGetParserLineNumber */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlTextReaderGetParserLineNumber xmlTextReaderGetParserLineNumber__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderGetParserProp */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderGetParserProp */
/*-no- ............................................................................................................................ */
/*-no- #define xmlTextReaderGetParserProp xmlTextReaderGetParserProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderGetRemainder */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextReaderGetRemainder */
/*-no- .......................................................................................................................... */
/*-no- #define xmlTextReaderGetRemainder xmlTextReaderGetRemainder__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderHasAttributes */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderHasAttributes */
/*-no- ............................................................................................................................ */
/*-no- #define xmlTextReaderHasAttributes xmlTextReaderHasAttributes__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderHasValue */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextReaderHasValue */
/*-no- .................................................................................................................. */
/*-no- #define xmlTextReaderHasValue xmlTextReaderHasValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderIsDefault */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderIsDefault */
/*-no- .................................................................................................................... */
/*-no- #define xmlTextReaderIsDefault xmlTextReaderIsDefault__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderIsEmptyElement */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderIsEmptyElement */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextReaderIsEmptyElement xmlTextReaderIsEmptyElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderIsNamespaceDecl */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderIsNamespaceDecl */
/*-no- ................................................................................................................................ */
/*-no- #define xmlTextReaderIsNamespaceDecl xmlTextReaderIsNamespaceDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderIsValid */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderIsValid */
/*-no- ................................................................................................................ */
/*-no- #define xmlTextReaderIsValid xmlTextReaderIsValid__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderLocalName */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderLocalName */
/*-no- .................................................................................................................... */
/*-no- #define xmlTextReaderLocalName xmlTextReaderLocalName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderLocatorBaseURI */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderLocatorBaseURI */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextReaderLocatorBaseURI xmlTextReaderLocatorBaseURI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderLocatorLineNumber */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderLocatorLineNumber */
/*-no- .................................................................................................................................... */
/*-no- #define xmlTextReaderLocatorLineNumber xmlTextReaderLocatorLineNumber__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderLookupNamespace */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderLookupNamespace */
/*-no- ................................................................................................................................ */
/*-no- #define xmlTextReaderLookupNamespace xmlTextReaderLookupNamespace__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderMoveToAttribute */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderMoveToAttribute */
/*-no- ................................................................................................................................ */
/*-no- #define xmlTextReaderMoveToAttribute xmlTextReaderMoveToAttribute__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderMoveToAttributeNo */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderMoveToAttributeNo */
/*-no- .................................................................................................................................... */
/*-no- #define xmlTextReaderMoveToAttributeNo xmlTextReaderMoveToAttributeNo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderMoveToAttributeNs */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderMoveToAttributeNs */
/*-no- .................................................................................................................................... */
/*-no- #define xmlTextReaderMoveToAttributeNs xmlTextReaderMoveToAttributeNs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderMoveToElement */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderMoveToElement */
/*-no- ............................................................................................................................ */
/*-no- #define xmlTextReaderMoveToElement xmlTextReaderMoveToElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderMoveToFirstAttribute */
/*-no- ................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextReaderMoveToFirstAttribute */
/*-no- .......................................................................................................................................... */
/*-no- #define xmlTextReaderMoveToFirstAttribute xmlTextReaderMoveToFirstAttribute__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderMoveToNextAttribute */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderMoveToNextAttribute */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlTextReaderMoveToNextAttribute xmlTextReaderMoveToNextAttribute__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderName */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextReaderName */
/*-no- .......................................................................................................... */
/*-no- #define xmlTextReaderName xmlTextReaderName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderNamespaceUri */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextReaderNamespaceUri */
/*-no- .......................................................................................................................... */
/*-no- #define xmlTextReaderNamespaceUri xmlTextReaderNamespaceUri__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderNext */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextReaderNext */
/*-no- .......................................................................................................... */
/*-no- #define xmlTextReaderNext xmlTextReaderNext__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderNextSibling */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderNextSibling */
/*-no- ........................................................................................................................ */
/*-no- #define xmlTextReaderNextSibling xmlTextReaderNextSibling__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderNodeType */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextReaderNodeType */
/*-no- .................................................................................................................. */
/*-no- #define xmlTextReaderNodeType xmlTextReaderNodeType__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderNormalization */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderNormalization */
/*-no- ............................................................................................................................ */
/*-no- #define xmlTextReaderNormalization xmlTextReaderNormalization__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderPrefix */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderPrefix */
/*-no- .............................................................................................................. */
/*-no- #define xmlTextReaderPrefix xmlTextReaderPrefix__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderPreserve */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextReaderPreserve */
/*-no- .................................................................................................................. */
/*-no- #define xmlTextReaderPreserve xmlTextReaderPreserve__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) && defined(LIBXML_PATTERN_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderPreservePattern */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderPreservePattern */
/*-no- ................................................................................................................................ */
/*-no- #define xmlTextReaderPreservePattern xmlTextReaderPreservePattern__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderQuoteChar */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderQuoteChar */
/*-no- .................................................................................................................... */
/*-no- #define xmlTextReaderQuoteChar xmlTextReaderQuoteChar__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderRead */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextReaderRead */
/*-no- .......................................................................................................... */
/*-no- #define xmlTextReaderRead xmlTextReaderRead__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderReadAttributeValue */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderReadAttributeValue */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlTextReaderReadAttributeValue xmlTextReaderReadAttributeValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) && defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderReadInnerXml */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextReaderReadInnerXml */
/*-no- .......................................................................................................................... */
/*-no- #define xmlTextReaderReadInnerXml xmlTextReaderReadInnerXml__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) && defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderReadOuterXml */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextReaderReadOuterXml */
/*-no- .......................................................................................................................... */
/*-no- #define xmlTextReaderReadOuterXml xmlTextReaderReadOuterXml__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderReadState */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderReadState */
/*-no- .................................................................................................................... */
/*-no- #define xmlTextReaderReadState xmlTextReaderReadState__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderReadString */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderReadString */
/*-no- ...................................................................................................................... */
/*-no- #define xmlTextReaderReadString xmlTextReaderReadString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) && defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderRelaxNGSetSchema */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderRelaxNGSetSchema */
/*-no- .................................................................................................................................. */
/*-no- #define xmlTextReaderRelaxNGSetSchema xmlTextReaderRelaxNGSetSchema__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) && defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderRelaxNGValidate */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderRelaxNGValidate */
/*-no- ................................................................................................................................ */
/*-no- #define xmlTextReaderRelaxNGValidate xmlTextReaderRelaxNGValidate__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) && defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderSchemaValidate */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderSchemaValidate */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextReaderSchemaValidate xmlTextReaderSchemaValidate__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) && defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderSchemaValidateCtxt */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderSchemaValidateCtxt */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlTextReaderSchemaValidateCtxt xmlTextReaderSchemaValidateCtxt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderSetErrorHandler */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderSetErrorHandler */
/*-no- ................................................................................................................................ */
/*-no- #define xmlTextReaderSetErrorHandler xmlTextReaderSetErrorHandler__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderSetParserProp */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderSetParserProp */
/*-no- ............................................................................................................................ */
/*-no- #define xmlTextReaderSetParserProp xmlTextReaderSetParserProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) && defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderSetSchema */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderSetSchema */
/*-no- .................................................................................................................... */
/*-no- #define xmlTextReaderSetSchema xmlTextReaderSetSchema__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderSetStructuredErrorHandler */
/*-no- ............................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderSetStructuredErrorHandler */
/*-no- .................................................................................................................................................... */
/*-no- #define xmlTextReaderSetStructuredErrorHandler xmlTextReaderSetStructuredErrorHandler__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderSetup */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderSetup */
/*-no- ............................................................................................................ */
/*-no- #define xmlTextReaderSetup xmlTextReaderSetup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderStandalone */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextReaderStandalone */
/*-no- ...................................................................................................................... */
/*-no- #define xmlTextReaderStandalone xmlTextReaderStandalone__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderValue */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderValue */
/*-no- ............................................................................................................ */
/*-no- #define xmlTextReaderValue xmlTextReaderValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_READER_ENABLED) */
/*-no- #ifdef bottom_xmlreader */
/*-no- #undef xmlTextReaderXmlLang */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextReaderXmlLang */
/*-no- ................................................................................................................ */
/*-no- #define xmlTextReaderXmlLang xmlTextReaderXmlLang__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterEndAttribute */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextWriterEndAttribute */
/*-no- .......................................................................................................................... */
/*-no- #define xmlTextWriterEndAttribute xmlTextWriterEndAttribute__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterEndCDATA */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextWriterEndCDATA */
/*-no- .................................................................................................................. */
/*-no- #define xmlTextWriterEndCDATA xmlTextWriterEndCDATA__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterEndComment */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterEndComment */
/*-no- ...................................................................................................................... */
/*-no- #define xmlTextWriterEndComment xmlTextWriterEndComment__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterEndDTD */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterEndDTD */
/*-no- .............................................................................................................. */
/*-no- #define xmlTextWriterEndDTD xmlTextWriterEndDTD__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterEndDTDAttlist */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterEndDTDAttlist */
/*-no- ............................................................................................................................ */
/*-no- #define xmlTextWriterEndDTDAttlist xmlTextWriterEndDTDAttlist__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterEndDTDElement */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterEndDTDElement */
/*-no- ............................................................................................................................ */
/*-no- #define xmlTextWriterEndDTDElement xmlTextWriterEndDTDElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterEndDTDEntity */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextWriterEndDTDEntity */
/*-no- .......................................................................................................................... */
/*-no- #define xmlTextWriterEndDTDEntity xmlTextWriterEndDTDEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterEndDocument */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterEndDocument */
/*-no- ........................................................................................................................ */
/*-no- #define xmlTextWriterEndDocument xmlTextWriterEndDocument__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterEndElement */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterEndElement */
/*-no- ...................................................................................................................... */
/*-no- #define xmlTextWriterEndElement xmlTextWriterEndElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterEndPI */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterEndPI */
/*-no- ............................................................................................................ */
/*-no- #define xmlTextWriterEndPI xmlTextWriterEndPI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterFlush */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterFlush */
/*-no- ............................................................................................................ */
/*-no- #define xmlTextWriterFlush xmlTextWriterFlush__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterFullEndElement */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterFullEndElement */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextWriterFullEndElement xmlTextWriterFullEndElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterSetIndent */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterSetIndent */
/*-no- .................................................................................................................... */
/*-no- #define xmlTextWriterSetIndent xmlTextWriterSetIndent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterSetIndentString */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterSetIndentString */
/*-no- ................................................................................................................................ */
/*-no- #define xmlTextWriterSetIndentString xmlTextWriterSetIndentString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterStartAttribute */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterStartAttribute */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextWriterStartAttribute xmlTextWriterStartAttribute__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterStartAttributeNS */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterStartAttributeNS */
/*-no- .................................................................................................................................. */
/*-no- #define xmlTextWriterStartAttributeNS xmlTextWriterStartAttributeNS__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterStartCDATA */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterStartCDATA */
/*-no- ...................................................................................................................... */
/*-no- #define xmlTextWriterStartCDATA xmlTextWriterStartCDATA__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterStartComment */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextWriterStartComment */
/*-no- .......................................................................................................................... */
/*-no- #define xmlTextWriterStartComment xmlTextWriterStartComment__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterStartDTD */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextWriterStartDTD */
/*-no- .................................................................................................................. */
/*-no- #define xmlTextWriterStartDTD xmlTextWriterStartDTD__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterStartDTDAttlist */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterStartDTDAttlist */
/*-no- ................................................................................................................................ */
/*-no- #define xmlTextWriterStartDTDAttlist xmlTextWriterStartDTDAttlist__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterStartDTDElement */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterStartDTDElement */
/*-no- ................................................................................................................................ */
/*-no- #define xmlTextWriterStartDTDElement xmlTextWriterStartDTDElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterStartDTDEntity */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterStartDTDEntity */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextWriterStartDTDEntity xmlTextWriterStartDTDEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterStartDocument */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterStartDocument */
/*-no- ............................................................................................................................ */
/*-no- #define xmlTextWriterStartDocument xmlTextWriterStartDocument__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterStartElement */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextWriterStartElement */
/*-no- .......................................................................................................................... */
/*-no- #define xmlTextWriterStartElement xmlTextWriterStartElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterStartElementNS */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterStartElementNS */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextWriterStartElementNS xmlTextWriterStartElementNS__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterStartPI */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterStartPI */
/*-no- ................................................................................................................ */
/*-no- #define xmlTextWriterStartPI xmlTextWriterStartPI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteAttribute */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteAttribute */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextWriterWriteAttribute xmlTextWriterWriteAttribute__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteAttributeNS */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteAttributeNS */
/*-no- .................................................................................................................................. */
/*-no- #define xmlTextWriterWriteAttributeNS xmlTextWriterWriteAttributeNS__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteBase64 */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteBase64 */
/*-no- ........................................................................................................................ */
/*-no- #define xmlTextWriterWriteBase64 xmlTextWriterWriteBase64__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteBinHex */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteBinHex */
/*-no- ........................................................................................................................ */
/*-no- #define xmlTextWriterWriteBinHex xmlTextWriterWriteBinHex__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteCDATA */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteCDATA */
/*-no- ...................................................................................................................... */
/*-no- #define xmlTextWriterWriteCDATA xmlTextWriterWriteCDATA__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteComment */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteComment */
/*-no- .......................................................................................................................... */
/*-no- #define xmlTextWriterWriteComment xmlTextWriterWriteComment__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteDTD */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteDTD */
/*-no- .................................................................................................................. */
/*-no- #define xmlTextWriterWriteDTD xmlTextWriterWriteDTD__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteDTDAttlist */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteDTDAttlist */
/*-no- ................................................................................................................................ */
/*-no- #define xmlTextWriterWriteDTDAttlist xmlTextWriterWriteDTDAttlist__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteDTDElement */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteDTDElement */
/*-no- ................................................................................................................................ */
/*-no- #define xmlTextWriterWriteDTDElement xmlTextWriterWriteDTDElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteDTDEntity */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteDTDEntity */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextWriterWriteDTDEntity xmlTextWriterWriteDTDEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteDTDExternalEntity */
/*-no- ...................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteDTDExternalEntity */
/*-no- .............................................................................................................................................. */
/*-no- #define xmlTextWriterWriteDTDExternalEntity xmlTextWriterWriteDTDExternalEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteDTDExternalEntityContents */
/*-no- .............................................................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteDTDExternalEntityContents */
/*-no- .............................................................................................................................................................. */
/*-no- #define xmlTextWriterWriteDTDExternalEntityContents xmlTextWriterWriteDTDExternalEntityContents__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteDTDInternalEntity */
/*-no- ...................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteDTDInternalEntity */
/*-no- .............................................................................................................................................. */
/*-no- #define xmlTextWriterWriteDTDInternalEntity xmlTextWriterWriteDTDInternalEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteDTDNotation */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteDTDNotation */
/*-no- .................................................................................................................................. */
/*-no- #define xmlTextWriterWriteDTDNotation xmlTextWriterWriteDTDNotation__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteElement */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteElement */
/*-no- .......................................................................................................................... */
/*-no- #define xmlTextWriterWriteElement xmlTextWriterWriteElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteElementNS */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteElementNS */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextWriterWriteElementNS xmlTextWriterWriteElementNS__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteFormatAttribute */
/*-no- ................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteFormatAttribute */
/*-no- .......................................................................................................................................... */
/*-no- #define xmlTextWriterWriteFormatAttribute xmlTextWriterWriteFormatAttribute__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteFormatAttributeNS */
/*-no- ...................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteFormatAttributeNS */
/*-no- .............................................................................................................................................. */
/*-no- #define xmlTextWriterWriteFormatAttributeNS xmlTextWriterWriteFormatAttributeNS__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteFormatCDATA */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteFormatCDATA */
/*-no- .................................................................................................................................. */
/*-no- #define xmlTextWriterWriteFormatCDATA xmlTextWriterWriteFormatCDATA__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteFormatComment */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteFormatComment */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlTextWriterWriteFormatComment xmlTextWriterWriteFormatComment__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteFormatDTD */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteFormatDTD */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextWriterWriteFormatDTD xmlTextWriterWriteFormatDTD__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteFormatDTDAttlist */
/*-no- ................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteFormatDTDAttlist */
/*-no- ............................................................................................................................................ */
/*-no- #define xmlTextWriterWriteFormatDTDAttlist xmlTextWriterWriteFormatDTDAttlist__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteFormatDTDElement */
/*-no- ................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteFormatDTDElement */
/*-no- ............................................................................................................................................ */
/*-no- #define xmlTextWriterWriteFormatDTDElement xmlTextWriterWriteFormatDTDElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteFormatDTDInternalEntity */
/*-no- ........................................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteFormatDTDInternalEntity */
/*-no- .......................................................................................................................................................... */
/*-no- #define xmlTextWriterWriteFormatDTDInternalEntity xmlTextWriterWriteFormatDTDInternalEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteFormatElement */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteFormatElement */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlTextWriterWriteFormatElement xmlTextWriterWriteFormatElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteFormatElementNS */
/*-no- ................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteFormatElementNS */
/*-no- .......................................................................................................................................... */
/*-no- #define xmlTextWriterWriteFormatElementNS xmlTextWriterWriteFormatElementNS__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteFormatPI */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteFormatPI */
/*-no- ............................................................................................................................ */
/*-no- #define xmlTextWriterWriteFormatPI xmlTextWriterWriteFormatPI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteFormatRaw */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteFormatRaw */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextWriterWriteFormatRaw xmlTextWriterWriteFormatRaw__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteFormatString */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteFormatString */
/*-no- .................................................................................................................................... */
/*-no- #define xmlTextWriterWriteFormatString xmlTextWriterWriteFormatString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWritePI */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWritePI */
/*-no- ................................................................................................................ */
/*-no- #define xmlTextWriterWritePI xmlTextWriterWritePI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteRaw */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteRaw */
/*-no- .................................................................................................................. */
/*-no- #define xmlTextWriterWriteRaw xmlTextWriterWriteRaw__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteRawLen */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteRawLen */
/*-no- ........................................................................................................................ */
/*-no- #define xmlTextWriterWriteRawLen xmlTextWriterWriteRawLen__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteString */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteString */
/*-no- ........................................................................................................................ */
/*-no- #define xmlTextWriterWriteString xmlTextWriterWriteString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteVFormatAttribute */
/*-no- ................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteVFormatAttribute */
/*-no- ............................................................................................................................................ */
/*-no- #define xmlTextWriterWriteVFormatAttribute xmlTextWriterWriteVFormatAttribute__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteVFormatAttributeNS */
/*-no- ......................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteVFormatAttributeNS */
/*-no- ................................................................................................................................................ */
/*-no- #define xmlTextWriterWriteVFormatAttributeNS xmlTextWriterWriteVFormatAttributeNS__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteVFormatCDATA */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteVFormatCDATA */
/*-no- .................................................................................................................................... */
/*-no- #define xmlTextWriterWriteVFormatCDATA xmlTextWriterWriteVFormatCDATA__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteVFormatComment */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteVFormatComment */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlTextWriterWriteVFormatComment xmlTextWriterWriteVFormatComment__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteVFormatDTD */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteVFormatDTD */
/*-no- ................................................................................................................................ */
/*-no- #define xmlTextWriterWriteVFormatDTD xmlTextWriterWriteVFormatDTD__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteVFormatDTDAttlist */
/*-no- ...................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteVFormatDTDAttlist */
/*-no- .............................................................................................................................................. */
/*-no- #define xmlTextWriterWriteVFormatDTDAttlist xmlTextWriterWriteVFormatDTDAttlist__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteVFormatDTDElement */
/*-no- ...................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteVFormatDTDElement */
/*-no- .............................................................................................................................................. */
/*-no- #define xmlTextWriterWriteVFormatDTDElement xmlTextWriterWriteVFormatDTDElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteVFormatDTDInternalEntity */
/*-no- ........................................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteVFormatDTDInternalEntity */
/*-no- ............................................................................................................................................................ */
/*-no- #define xmlTextWriterWriteVFormatDTDInternalEntity xmlTextWriterWriteVFormatDTDInternalEntity__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteVFormatElement */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteVFormatElement */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlTextWriterWriteVFormatElement xmlTextWriterWriteVFormatElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteVFormatElementNS */
/*-no- ................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteVFormatElementNS */
/*-no- ............................................................................................................................................ */
/*-no- #define xmlTextWriterWriteVFormatElementNS xmlTextWriterWriteVFormatElementNS__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteVFormatPI */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteVFormatPI */
/*-no- .............................................................................................................................. */
/*-no- #define xmlTextWriterWriteVFormatPI xmlTextWriterWriteVFormatPI__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteVFormatRaw */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteVFormatRaw */
/*-no- ................................................................................................................................ */
/*-no- #define xmlTextWriterWriteVFormatRaw xmlTextWriterWriteVFormatRaw__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_WRITER_ENABLED) */
/*-no- #ifdef bottom_xmlwriter */
/*-no- #undef xmlTextWriterWriteVFormatString */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlTextWriterWriteVFormatString */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlTextWriterWriteVFormatString xmlTextWriterWriteVFormatString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefBufferAllocScheme */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlThrDefBufferAllocScheme */
/*-no- ............................................................................................................................ */
/*-no- #define xmlThrDefBufferAllocScheme xmlThrDefBufferAllocScheme__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefDefaultBufferSize */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlThrDefDefaultBufferSize */
/*-no- ............................................................................................................................ */
/*-no- #define xmlThrDefDefaultBufferSize xmlThrDefDefaultBufferSize__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefDeregisterNodeDefault */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlThrDefDeregisterNodeDefault */
/*-no- .................................................................................................................................... */
/*-no- #define xmlThrDefDeregisterNodeDefault xmlThrDefDeregisterNodeDefault__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefDoValidityCheckingDefaultValue */
/*-no- .................................................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlThrDefDoValidityCheckingDefaultValue */
/*-no- ...................................................................................................................................................... */
/*-no- #define xmlThrDefDoValidityCheckingDefaultValue xmlThrDefDoValidityCheckingDefaultValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefGetWarningsDefaultValue */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlThrDefGetWarningsDefaultValue */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlThrDefGetWarningsDefaultValue xmlThrDefGetWarningsDefaultValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefIndentTreeOutput */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlThrDefIndentTreeOutput */
/*-no- .......................................................................................................................... */
/*-no- #define xmlThrDefIndentTreeOutput xmlThrDefIndentTreeOutput__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefKeepBlanksDefaultValue */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlThrDefKeepBlanksDefaultValue */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlThrDefKeepBlanksDefaultValue xmlThrDefKeepBlanksDefaultValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefLineNumbersDefaultValue */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlThrDefLineNumbersDefaultValue */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlThrDefLineNumbersDefaultValue xmlThrDefLineNumbersDefaultValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefLoadExtDtdDefaultValue */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlThrDefLoadExtDtdDefaultValue */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlThrDefLoadExtDtdDefaultValue xmlThrDefLoadExtDtdDefaultValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefOutputBufferCreateFilenameDefault */
/*-no- ........................................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlThrDefOutputBufferCreateFilenameDefault */
/*-no- ............................................................................................................................................................ */
/*-no- #define xmlThrDefOutputBufferCreateFilenameDefault xmlThrDefOutputBufferCreateFilenameDefault__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefParserDebugEntities */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlThrDefParserDebugEntities */
/*-no- ................................................................................................................................ */
/*-no- #define xmlThrDefParserDebugEntities xmlThrDefParserDebugEntities__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefParserInputBufferCreateFilenameDefault */
/*-no- .......................................................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlThrDefParserInputBufferCreateFilenameDefault */
/*-no- ...................................................................................................................................................................... */
/*-no- #define xmlThrDefParserInputBufferCreateFilenameDefault xmlThrDefParserInputBufferCreateFilenameDefault__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefPedanticParserDefaultValue */
/*-no- ...................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlThrDefPedanticParserDefaultValue */
/*-no- .............................................................................................................................................. */
/*-no- #define xmlThrDefPedanticParserDefaultValue xmlThrDefPedanticParserDefaultValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefRegisterNodeDefault */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlThrDefRegisterNodeDefault */
/*-no- ................................................................................................................................ */
/*-no- #define xmlThrDefRegisterNodeDefault xmlThrDefRegisterNodeDefault__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefSaveNoEmptyTags */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlThrDefSaveNoEmptyTags */
/*-no- ........................................................................................................................ */
/*-no- #define xmlThrDefSaveNoEmptyTags xmlThrDefSaveNoEmptyTags__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefSetGenericErrorFunc */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlThrDefSetGenericErrorFunc */
/*-no- ................................................................................................................................ */
/*-no- #define xmlThrDefSetGenericErrorFunc xmlThrDefSetGenericErrorFunc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefSetStructuredErrorFunc */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlThrDefSetStructuredErrorFunc */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlThrDefSetStructuredErrorFunc xmlThrDefSetStructuredErrorFunc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefSubstituteEntitiesDefaultValue */
/*-no- .................................................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlThrDefSubstituteEntitiesDefaultValue */
/*-no- ...................................................................................................................................................... */
/*-no- #define xmlThrDefSubstituteEntitiesDefaultValue xmlThrDefSubstituteEntitiesDefaultValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_globals */
/*-no- #undef xmlThrDefTreeIndentString */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlThrDefTreeIndentString */
/*-no- .......................................................................................................................... */
/*-no- #define xmlThrDefTreeIndentString xmlThrDefTreeIndentString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsAegeanNumbers */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsAegeanNumbers */
/*-no- .................................................................................................................. */
/*-no- #define xmlUCSIsAegeanNumbers xmlUCSIsAegeanNumbers__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsAlphabeticPresentationForms */
/*-no- ...................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsAlphabeticPresentationForms */
/*-no- .............................................................................................................................................. */
/*-no- #define xmlUCSIsAlphabeticPresentationForms xmlUCSIsAlphabeticPresentationForms__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsArabic */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsArabic */
/*-no- .................................................................................................... */
/*-no- #define xmlUCSIsArabic xmlUCSIsArabic__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsArabicPresentationFormsA */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsArabicPresentationFormsA */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlUCSIsArabicPresentationFormsA xmlUCSIsArabicPresentationFormsA__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsArabicPresentationFormsB */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsArabicPresentationFormsB */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlUCSIsArabicPresentationFormsB xmlUCSIsArabicPresentationFormsB__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsArmenian */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsArmenian */
/*-no- ........................................................................................................ */
/*-no- #define xmlUCSIsArmenian xmlUCSIsArmenian__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsArrows */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsArrows */
/*-no- .................................................................................................... */
/*-no- #define xmlUCSIsArrows xmlUCSIsArrows__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsBasicLatin */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsBasicLatin */
/*-no- ............................................................................................................ */
/*-no- #define xmlUCSIsBasicLatin xmlUCSIsBasicLatin__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsBengali */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsBengali */
/*-no- ...................................................................................................... */
/*-no- #define xmlUCSIsBengali xmlUCSIsBengali__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsBlock */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsBlock */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsBlock xmlUCSIsBlock__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsBlockElements */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsBlockElements */
/*-no- .................................................................................................................. */
/*-no- #define xmlUCSIsBlockElements xmlUCSIsBlockElements__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsBopomofo */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsBopomofo */
/*-no- ........................................................................................................ */
/*-no- #define xmlUCSIsBopomofo xmlUCSIsBopomofo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsBopomofoExtended */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsBopomofoExtended */
/*-no- ........................................................................................................................ */
/*-no- #define xmlUCSIsBopomofoExtended xmlUCSIsBopomofoExtended__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsBoxDrawing */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsBoxDrawing */
/*-no- ............................................................................................................ */
/*-no- #define xmlUCSIsBoxDrawing xmlUCSIsBoxDrawing__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsBraillePatterns */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsBraillePatterns */
/*-no- ...................................................................................................................... */
/*-no- #define xmlUCSIsBraillePatterns xmlUCSIsBraillePatterns__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsBuhid */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsBuhid */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsBuhid xmlUCSIsBuhid__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsByzantineMusicalSymbols */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsByzantineMusicalSymbols */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlUCSIsByzantineMusicalSymbols xmlUCSIsByzantineMusicalSymbols__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCJKCompatibility */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCJKCompatibility */
/*-no- ........................................................................................................................ */
/*-no- #define xmlUCSIsCJKCompatibility xmlUCSIsCJKCompatibility__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCJKCompatibilityForms */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCJKCompatibilityForms */
/*-no- .................................................................................................................................. */
/*-no- #define xmlUCSIsCJKCompatibilityForms xmlUCSIsCJKCompatibilityForms__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCJKCompatibilityIdeographs */
/*-no- ................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCJKCompatibilityIdeographs */
/*-no- ............................................................................................................................................ */
/*-no- #define xmlUCSIsCJKCompatibilityIdeographs xmlUCSIsCJKCompatibilityIdeographs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCJKCompatibilityIdeographsSupplement */
/*-no- ................................................................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCJKCompatibilityIdeographsSupplement */
/*-no- ................................................................................................................................................................ */
/*-no- #define xmlUCSIsCJKCompatibilityIdeographsSupplement xmlUCSIsCJKCompatibilityIdeographsSupplement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCJKRadicalsSupplement */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCJKRadicalsSupplement */
/*-no- .................................................................................................................................. */
/*-no- #define xmlUCSIsCJKRadicalsSupplement xmlUCSIsCJKRadicalsSupplement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCJKSymbolsandPunctuation */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCJKSymbolsandPunctuation */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlUCSIsCJKSymbolsandPunctuation xmlUCSIsCJKSymbolsandPunctuation__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCJKUnifiedIdeographs */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCJKUnifiedIdeographs */
/*-no- ................................................................................................................................ */
/*-no- #define xmlUCSIsCJKUnifiedIdeographs xmlUCSIsCJKUnifiedIdeographs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCJKUnifiedIdeographsExtensionA */
/*-no- ............................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCJKUnifiedIdeographsExtensionA */
/*-no- .................................................................................................................................................... */
/*-no- #define xmlUCSIsCJKUnifiedIdeographsExtensionA xmlUCSIsCJKUnifiedIdeographsExtensionA__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCJKUnifiedIdeographsExtensionB */
/*-no- ............................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCJKUnifiedIdeographsExtensionB */
/*-no- .................................................................................................................................................... */
/*-no- #define xmlUCSIsCJKUnifiedIdeographsExtensionB xmlUCSIsCJKUnifiedIdeographsExtensionB__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCat */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCat */
/*-no- .............................................................................................. */
/*-no- #define xmlUCSIsCat xmlUCSIsCat__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatC */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatC */
/*-no- ................................................................................................ */
/*-no- #define xmlUCSIsCatC xmlUCSIsCatC__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatCc */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatCc */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatCc xmlUCSIsCatCc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatCf */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatCf */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatCf xmlUCSIsCatCf__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatCo */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatCo */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatCo xmlUCSIsCatCo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatCs */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatCs */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatCs xmlUCSIsCatCs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatL */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatL */
/*-no- ................................................................................................ */
/*-no- #define xmlUCSIsCatL xmlUCSIsCatL__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatLl */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatLl */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatLl xmlUCSIsCatLl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatLm */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatLm */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatLm xmlUCSIsCatLm__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatLo */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatLo */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatLo xmlUCSIsCatLo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatLt */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatLt */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatLt xmlUCSIsCatLt__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatLu */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatLu */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatLu xmlUCSIsCatLu__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatM */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatM */
/*-no- ................................................................................................ */
/*-no- #define xmlUCSIsCatM xmlUCSIsCatM__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatMc */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatMc */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatMc xmlUCSIsCatMc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatMe */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatMe */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatMe xmlUCSIsCatMe__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatMn */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatMn */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatMn xmlUCSIsCatMn__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatN */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatN */
/*-no- ................................................................................................ */
/*-no- #define xmlUCSIsCatN xmlUCSIsCatN__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatNd */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatNd */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatNd xmlUCSIsCatNd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatNl */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatNl */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatNl xmlUCSIsCatNl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatNo */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatNo */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatNo xmlUCSIsCatNo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatP */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatP */
/*-no- ................................................................................................ */
/*-no- #define xmlUCSIsCatP xmlUCSIsCatP__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatPc */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatPc */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatPc xmlUCSIsCatPc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatPd */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatPd */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatPd xmlUCSIsCatPd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatPe */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatPe */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatPe xmlUCSIsCatPe__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatPf */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatPf */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatPf xmlUCSIsCatPf__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatPi */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatPi */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatPi xmlUCSIsCatPi__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatPo */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatPo */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatPo xmlUCSIsCatPo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatPs */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatPs */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatPs xmlUCSIsCatPs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatS */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatS */
/*-no- ................................................................................................ */
/*-no- #define xmlUCSIsCatS xmlUCSIsCatS__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatSc */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatSc */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatSc xmlUCSIsCatSc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatSk */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatSk */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatSk xmlUCSIsCatSk__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatSm */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatSm */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatSm xmlUCSIsCatSm__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatSo */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatSo */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatSo xmlUCSIsCatSo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatZ */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatZ */
/*-no- ................................................................................................ */
/*-no- #define xmlUCSIsCatZ xmlUCSIsCatZ__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatZl */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatZl */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatZl xmlUCSIsCatZl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatZp */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatZp */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatZp xmlUCSIsCatZp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCatZs */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCatZs */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsCatZs xmlUCSIsCatZs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCherokee */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCherokee */
/*-no- ........................................................................................................ */
/*-no- #define xmlUCSIsCherokee xmlUCSIsCherokee__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCombiningDiacriticalMarks */
/*-no- ................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCombiningDiacriticalMarks */
/*-no- .......................................................................................................................................... */
/*-no- #define xmlUCSIsCombiningDiacriticalMarks xmlUCSIsCombiningDiacriticalMarks__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCombiningDiacriticalMarksforSymbols */
/*-no- .............................................................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCombiningDiacriticalMarksforSymbols */
/*-no- .............................................................................................................................................................. */
/*-no- #define xmlUCSIsCombiningDiacriticalMarksforSymbols xmlUCSIsCombiningDiacriticalMarksforSymbols__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCombiningHalfMarks */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCombiningHalfMarks */
/*-no- ............................................................................................................................ */
/*-no- #define xmlUCSIsCombiningHalfMarks xmlUCSIsCombiningHalfMarks__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCombiningMarksforSymbols */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCombiningMarksforSymbols */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlUCSIsCombiningMarksforSymbols xmlUCSIsCombiningMarksforSymbols__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsControlPictures */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsControlPictures */
/*-no- ...................................................................................................................... */
/*-no- #define xmlUCSIsControlPictures xmlUCSIsControlPictures__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCurrencySymbols */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCurrencySymbols */
/*-no- ...................................................................................................................... */
/*-no- #define xmlUCSIsCurrencySymbols xmlUCSIsCurrencySymbols__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCypriotSyllabary */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCypriotSyllabary */
/*-no- ........................................................................................................................ */
/*-no- #define xmlUCSIsCypriotSyllabary xmlUCSIsCypriotSyllabary__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCyrillic */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCyrillic */
/*-no- ........................................................................................................ */
/*-no- #define xmlUCSIsCyrillic xmlUCSIsCyrillic__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsCyrillicSupplement */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsCyrillicSupplement */
/*-no- ............................................................................................................................ */
/*-no- #define xmlUCSIsCyrillicSupplement xmlUCSIsCyrillicSupplement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsDeseret */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsDeseret */
/*-no- ...................................................................................................... */
/*-no- #define xmlUCSIsDeseret xmlUCSIsDeseret__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsDevanagari */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsDevanagari */
/*-no- ............................................................................................................ */
/*-no- #define xmlUCSIsDevanagari xmlUCSIsDevanagari__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsDingbats */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsDingbats */
/*-no- ........................................................................................................ */
/*-no- #define xmlUCSIsDingbats xmlUCSIsDingbats__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsEnclosedAlphanumerics */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsEnclosedAlphanumerics */
/*-no- .................................................................................................................................. */
/*-no- #define xmlUCSIsEnclosedAlphanumerics xmlUCSIsEnclosedAlphanumerics__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsEnclosedCJKLettersandMonths */
/*-no- ...................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsEnclosedCJKLettersandMonths */
/*-no- .............................................................................................................................................. */
/*-no- #define xmlUCSIsEnclosedCJKLettersandMonths xmlUCSIsEnclosedCJKLettersandMonths__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsEthiopic */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsEthiopic */
/*-no- ........................................................................................................ */
/*-no- #define xmlUCSIsEthiopic xmlUCSIsEthiopic__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsGeneralPunctuation */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsGeneralPunctuation */
/*-no- ............................................................................................................................ */
/*-no- #define xmlUCSIsGeneralPunctuation xmlUCSIsGeneralPunctuation__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsGeometricShapes */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsGeometricShapes */
/*-no- ...................................................................................................................... */
/*-no- #define xmlUCSIsGeometricShapes xmlUCSIsGeometricShapes__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsGeorgian */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsGeorgian */
/*-no- ........................................................................................................ */
/*-no- #define xmlUCSIsGeorgian xmlUCSIsGeorgian__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsGothic */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsGothic */
/*-no- .................................................................................................... */
/*-no- #define xmlUCSIsGothic xmlUCSIsGothic__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsGreek */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsGreek */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsGreek xmlUCSIsGreek__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsGreekExtended */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsGreekExtended */
/*-no- .................................................................................................................. */
/*-no- #define xmlUCSIsGreekExtended xmlUCSIsGreekExtended__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsGreekandCoptic */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsGreekandCoptic */
/*-no- .................................................................................................................... */
/*-no- #define xmlUCSIsGreekandCoptic xmlUCSIsGreekandCoptic__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsGujarati */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsGujarati */
/*-no- ........................................................................................................ */
/*-no- #define xmlUCSIsGujarati xmlUCSIsGujarati__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsGurmukhi */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsGurmukhi */
/*-no- ........................................................................................................ */
/*-no- #define xmlUCSIsGurmukhi xmlUCSIsGurmukhi__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsHalfwidthandFullwidthForms */
/*-no- ................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsHalfwidthandFullwidthForms */
/*-no- ............................................................................................................................................ */
/*-no- #define xmlUCSIsHalfwidthandFullwidthForms xmlUCSIsHalfwidthandFullwidthForms__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsHangulCompatibilityJamo */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsHangulCompatibilityJamo */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlUCSIsHangulCompatibilityJamo xmlUCSIsHangulCompatibilityJamo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsHangulJamo */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsHangulJamo */
/*-no- ............................................................................................................ */
/*-no- #define xmlUCSIsHangulJamo xmlUCSIsHangulJamo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsHangulSyllables */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsHangulSyllables */
/*-no- ...................................................................................................................... */
/*-no- #define xmlUCSIsHangulSyllables xmlUCSIsHangulSyllables__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsHanunoo */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsHanunoo */
/*-no- ...................................................................................................... */
/*-no- #define xmlUCSIsHanunoo xmlUCSIsHanunoo__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsHebrew */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsHebrew */
/*-no- .................................................................................................... */
/*-no- #define xmlUCSIsHebrew xmlUCSIsHebrew__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsHighPrivateUseSurrogates */
/*-no- ............................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsHighPrivateUseSurrogates */
/*-no- ........................................................................................................................................ */
/*-no- #define xmlUCSIsHighPrivateUseSurrogates xmlUCSIsHighPrivateUseSurrogates__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsHighSurrogates */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsHighSurrogates */
/*-no- .................................................................................................................... */
/*-no- #define xmlUCSIsHighSurrogates xmlUCSIsHighSurrogates__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsHiragana */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsHiragana */
/*-no- ........................................................................................................ */
/*-no- #define xmlUCSIsHiragana xmlUCSIsHiragana__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsIPAExtensions */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsIPAExtensions */
/*-no- .................................................................................................................. */
/*-no- #define xmlUCSIsIPAExtensions xmlUCSIsIPAExtensions__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsIdeographicDescriptionCharacters */
/*-no- ..................................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsIdeographicDescriptionCharacters */
/*-no- ........................................................................................................................................................ */
/*-no- #define xmlUCSIsIdeographicDescriptionCharacters xmlUCSIsIdeographicDescriptionCharacters__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsKanbun */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsKanbun */
/*-no- .................................................................................................... */
/*-no- #define xmlUCSIsKanbun xmlUCSIsKanbun__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsKangxiRadicals */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsKangxiRadicals */
/*-no- .................................................................................................................... */
/*-no- #define xmlUCSIsKangxiRadicals xmlUCSIsKangxiRadicals__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsKannada */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsKannada */
/*-no- ...................................................................................................... */
/*-no- #define xmlUCSIsKannada xmlUCSIsKannada__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsKatakana */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsKatakana */
/*-no- ........................................................................................................ */
/*-no- #define xmlUCSIsKatakana xmlUCSIsKatakana__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsKatakanaPhoneticExtensions */
/*-no- ................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsKatakanaPhoneticExtensions */
/*-no- ............................................................................................................................................ */
/*-no- #define xmlUCSIsKatakanaPhoneticExtensions xmlUCSIsKatakanaPhoneticExtensions__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsKhmer */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsKhmer */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsKhmer xmlUCSIsKhmer__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsKhmerSymbols */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsKhmerSymbols */
/*-no- ................................................................................................................ */
/*-no- #define xmlUCSIsKhmerSymbols xmlUCSIsKhmerSymbols__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsLao */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsLao */
/*-no- .............................................................................................. */
/*-no- #define xmlUCSIsLao xmlUCSIsLao__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsLatin1Supplement */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsLatin1Supplement */
/*-no- ........................................................................................................................ */
/*-no- #define xmlUCSIsLatin1Supplement xmlUCSIsLatin1Supplement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsLatinExtendedA */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsLatinExtendedA */
/*-no- .................................................................................................................... */
/*-no- #define xmlUCSIsLatinExtendedA xmlUCSIsLatinExtendedA__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsLatinExtendedAdditional */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsLatinExtendedAdditional */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlUCSIsLatinExtendedAdditional xmlUCSIsLatinExtendedAdditional__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsLatinExtendedB */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsLatinExtendedB */
/*-no- .................................................................................................................... */
/*-no- #define xmlUCSIsLatinExtendedB xmlUCSIsLatinExtendedB__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsLetterlikeSymbols */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsLetterlikeSymbols */
/*-no- .......................................................................................................................... */
/*-no- #define xmlUCSIsLetterlikeSymbols xmlUCSIsLetterlikeSymbols__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsLimbu */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsLimbu */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsLimbu xmlUCSIsLimbu__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsLinearBIdeograms */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsLinearBIdeograms */
/*-no- ........................................................................................................................ */
/*-no- #define xmlUCSIsLinearBIdeograms xmlUCSIsLinearBIdeograms__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsLinearBSyllabary */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsLinearBSyllabary */
/*-no- ........................................................................................................................ */
/*-no- #define xmlUCSIsLinearBSyllabary xmlUCSIsLinearBSyllabary__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsLowSurrogates */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsLowSurrogates */
/*-no- .................................................................................................................. */
/*-no- #define xmlUCSIsLowSurrogates xmlUCSIsLowSurrogates__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsMalayalam */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsMalayalam */
/*-no- .......................................................................................................... */
/*-no- #define xmlUCSIsMalayalam xmlUCSIsMalayalam__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsMathematicalAlphanumericSymbols */
/*-no- .................................................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsMathematicalAlphanumericSymbols */
/*-no- ...................................................................................................................................................... */
/*-no- #define xmlUCSIsMathematicalAlphanumericSymbols xmlUCSIsMathematicalAlphanumericSymbols__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsMathematicalOperators */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsMathematicalOperators */
/*-no- .................................................................................................................................. */
/*-no- #define xmlUCSIsMathematicalOperators xmlUCSIsMathematicalOperators__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsMiscellaneousMathematicalSymbolsA */
/*-no- ........................................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsMiscellaneousMathematicalSymbolsA */
/*-no- .......................................................................................................................................................... */
/*-no- #define xmlUCSIsMiscellaneousMathematicalSymbolsA xmlUCSIsMiscellaneousMathematicalSymbolsA__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsMiscellaneousMathematicalSymbolsB */
/*-no- ........................................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsMiscellaneousMathematicalSymbolsB */
/*-no- .......................................................................................................................................................... */
/*-no- #define xmlUCSIsMiscellaneousMathematicalSymbolsB xmlUCSIsMiscellaneousMathematicalSymbolsB__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsMiscellaneousSymbols */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsMiscellaneousSymbols */
/*-no- ................................................................................................................................ */
/*-no- #define xmlUCSIsMiscellaneousSymbols xmlUCSIsMiscellaneousSymbols__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsMiscellaneousSymbolsandArrows */
/*-no- ............................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsMiscellaneousSymbolsandArrows */
/*-no- .................................................................................................................................................. */
/*-no- #define xmlUCSIsMiscellaneousSymbolsandArrows xmlUCSIsMiscellaneousSymbolsandArrows__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsMiscellaneousTechnical */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsMiscellaneousTechnical */
/*-no- .................................................................................................................................... */
/*-no- #define xmlUCSIsMiscellaneousTechnical xmlUCSIsMiscellaneousTechnical__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsMongolian */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsMongolian */
/*-no- .......................................................................................................... */
/*-no- #define xmlUCSIsMongolian xmlUCSIsMongolian__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsMusicalSymbols */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsMusicalSymbols */
/*-no- .................................................................................................................... */
/*-no- #define xmlUCSIsMusicalSymbols xmlUCSIsMusicalSymbols__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsMyanmar */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsMyanmar */
/*-no- ...................................................................................................... */
/*-no- #define xmlUCSIsMyanmar xmlUCSIsMyanmar__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsNumberForms */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsNumberForms */
/*-no- .............................................................................................................. */
/*-no- #define xmlUCSIsNumberForms xmlUCSIsNumberForms__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsOgham */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsOgham */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsOgham xmlUCSIsOgham__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsOldItalic */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsOldItalic */
/*-no- .......................................................................................................... */
/*-no- #define xmlUCSIsOldItalic xmlUCSIsOldItalic__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsOpticalCharacterRecognition */
/*-no- ...................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsOpticalCharacterRecognition */
/*-no- .............................................................................................................................................. */
/*-no- #define xmlUCSIsOpticalCharacterRecognition xmlUCSIsOpticalCharacterRecognition__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsOriya */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsOriya */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsOriya xmlUCSIsOriya__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsOsmanya */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsOsmanya */
/*-no- ...................................................................................................... */
/*-no- #define xmlUCSIsOsmanya xmlUCSIsOsmanya__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsPhoneticExtensions */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsPhoneticExtensions */
/*-no- ............................................................................................................................ */
/*-no- #define xmlUCSIsPhoneticExtensions xmlUCSIsPhoneticExtensions__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsPrivateUse */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsPrivateUse */
/*-no- ............................................................................................................ */
/*-no- #define xmlUCSIsPrivateUse xmlUCSIsPrivateUse__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsPrivateUseArea */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsPrivateUseArea */
/*-no- .................................................................................................................... */
/*-no- #define xmlUCSIsPrivateUseArea xmlUCSIsPrivateUseArea__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsRunic */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsRunic */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsRunic xmlUCSIsRunic__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsShavian */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsShavian */
/*-no- ...................................................................................................... */
/*-no- #define xmlUCSIsShavian xmlUCSIsShavian__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsSinhala */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsSinhala */
/*-no- ...................................................................................................... */
/*-no- #define xmlUCSIsSinhala xmlUCSIsSinhala__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsSmallFormVariants */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsSmallFormVariants */
/*-no- .......................................................................................................................... */
/*-no- #define xmlUCSIsSmallFormVariants xmlUCSIsSmallFormVariants__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsSpacingModifierLetters */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsSpacingModifierLetters */
/*-no- .................................................................................................................................... */
/*-no- #define xmlUCSIsSpacingModifierLetters xmlUCSIsSpacingModifierLetters__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsSpecials */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsSpecials */
/*-no- ........................................................................................................ */
/*-no- #define xmlUCSIsSpecials xmlUCSIsSpecials__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsSuperscriptsandSubscripts */
/*-no- ................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsSuperscriptsandSubscripts */
/*-no- .......................................................................................................................................... */
/*-no- #define xmlUCSIsSuperscriptsandSubscripts xmlUCSIsSuperscriptsandSubscripts__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsSupplementalArrowsA */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsSupplementalArrowsA */
/*-no- .............................................................................................................................. */
/*-no- #define xmlUCSIsSupplementalArrowsA xmlUCSIsSupplementalArrowsA__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsSupplementalArrowsB */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsSupplementalArrowsB */
/*-no- .............................................................................................................................. */
/*-no- #define xmlUCSIsSupplementalArrowsB xmlUCSIsSupplementalArrowsB__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsSupplementalMathematicalOperators */
/*-no- ........................................................................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlUCSIsSupplementalMathematicalOperators */
/*-no- .......................................................................................................................................................... */
/*-no- #define xmlUCSIsSupplementalMathematicalOperators xmlUCSIsSupplementalMathematicalOperators__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsSupplementaryPrivateUseAreaA */
/*-no- ......................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsSupplementaryPrivateUseAreaA */
/*-no- ................................................................................................................................................ */
/*-no- #define xmlUCSIsSupplementaryPrivateUseAreaA xmlUCSIsSupplementaryPrivateUseAreaA__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsSupplementaryPrivateUseAreaB */
/*-no- ......................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsSupplementaryPrivateUseAreaB */
/*-no- ................................................................................................................................................ */
/*-no- #define xmlUCSIsSupplementaryPrivateUseAreaB xmlUCSIsSupplementaryPrivateUseAreaB__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsSyriac */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsSyriac */
/*-no- .................................................................................................... */
/*-no- #define xmlUCSIsSyriac xmlUCSIsSyriac__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsTagalog */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsTagalog */
/*-no- ...................................................................................................... */
/*-no- #define xmlUCSIsTagalog xmlUCSIsTagalog__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsTagbanwa */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsTagbanwa */
/*-no- ........................................................................................................ */
/*-no- #define xmlUCSIsTagbanwa xmlUCSIsTagbanwa__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsTags */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsTags */
/*-no- ................................................................................................ */
/*-no- #define xmlUCSIsTags xmlUCSIsTags__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsTaiLe */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsTaiLe */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsTaiLe xmlUCSIsTaiLe__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsTaiXuanJingSymbols */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsTaiXuanJingSymbols */
/*-no- ............................................................................................................................ */
/*-no- #define xmlUCSIsTaiXuanJingSymbols xmlUCSIsTaiXuanJingSymbols__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsTamil */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsTamil */
/*-no- .................................................................................................. */
/*-no- #define xmlUCSIsTamil xmlUCSIsTamil__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsTelugu */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsTelugu */
/*-no- .................................................................................................... */
/*-no- #define xmlUCSIsTelugu xmlUCSIsTelugu__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsThaana */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsThaana */
/*-no- .................................................................................................... */
/*-no- #define xmlUCSIsThaana xmlUCSIsThaana__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsThai */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsThai */
/*-no- ................................................................................................ */
/*-no- #define xmlUCSIsThai xmlUCSIsThai__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsTibetan */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsTibetan */
/*-no- ...................................................................................................... */
/*-no- #define xmlUCSIsTibetan xmlUCSIsTibetan__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsUgaritic */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUCSIsUgaritic */
/*-no- ........................................................................................................ */
/*-no- #define xmlUCSIsUgaritic xmlUCSIsUgaritic__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsUnifiedCanadianAboriginalSyllabics */
/*-no- ........................................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsUnifiedCanadianAboriginalSyllabics */
/*-no- ............................................................................................................................................................ */
/*-no- #define xmlUCSIsUnifiedCanadianAboriginalSyllabics xmlUCSIsUnifiedCanadianAboriginalSyllabics__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsVariationSelectors */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsVariationSelectors */
/*-no- ............................................................................................................................ */
/*-no- #define xmlUCSIsVariationSelectors xmlUCSIsVariationSelectors__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsVariationSelectorsSupplement */
/*-no- ......................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsVariationSelectorsSupplement */
/*-no- ................................................................................................................................................ */
/*-no- #define xmlUCSIsVariationSelectorsSupplement xmlUCSIsVariationSelectorsSupplement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsYiRadicals */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsYiRadicals */
/*-no- ............................................................................................................ */
/*-no- #define xmlUCSIsYiRadicals xmlUCSIsYiRadicals__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsYiSyllables */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsYiSyllables */
/*-no- .............................................................................................................. */
/*-no- #define xmlUCSIsYiSyllables xmlUCSIsYiSyllables__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_UNICODE_ENABLED) */
/*-no- #ifdef bottom_xmlunicode */
/*-no- #undef xmlUCSIsYijingHexagramSymbols */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUCSIsYijingHexagramSymbols */
/*-no- .................................................................................................................................. */
/*-no- #define xmlUCSIsYijingHexagramSymbols xmlUCSIsYijingHexagramSymbols__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_uri */
/*-no- #undef xmlURIEscape */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlURIEscape */
/*-no- ................................................................................................ */
/*-no- #define xmlURIEscape xmlURIEscape__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_uri */
/*-no- #undef xmlURIEscapeStr */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlURIEscapeStr */
/*-no- ...................................................................................................... */
/*-no- #define xmlURIEscapeStr xmlURIEscapeStr__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_uri */
/*-no- #undef xmlURIUnescapeString */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlURIUnescapeString */
/*-no- ................................................................................................................ */
/*-no- #define xmlURIUnescapeString xmlURIUnescapeString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlUTF8Charcmp */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUTF8Charcmp */
/*-no- .................................................................................................... */
/*-no- #define xmlUTF8Charcmp xmlUTF8Charcmp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlUTF8Size */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUTF8Size */
/*-no- .............................................................................................. */
/*-no- #define xmlUTF8Size xmlUTF8Size__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlUTF8Strlen */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUTF8Strlen */
/*-no- .................................................................................................. */
/*-no- #define xmlUTF8Strlen xmlUTF8Strlen__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlUTF8Strloc */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUTF8Strloc */
/*-no- .................................................................................................. */
/*-no- #define xmlUTF8Strloc xmlUTF8Strloc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlUTF8Strndup */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUTF8Strndup */
/*-no- .................................................................................................... */
/*-no- #define xmlUTF8Strndup xmlUTF8Strndup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlUTF8Strpos */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUTF8Strpos */
/*-no- .................................................................................................. */
/*-no- #define xmlUTF8Strpos xmlUTF8Strpos__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlUTF8Strsize */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUTF8Strsize */
/*-no- .................................................................................................... */
/*-no- #define xmlUTF8Strsize xmlUTF8Strsize__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_xmlstring */
/*-no- #undef xmlUTF8Strsub */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUTF8Strsub */
/*-no- .................................................................................................. */
/*-no- #define xmlUTF8Strsub xmlUTF8Strsub__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlUnlinkNode */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUnlinkNode */
/*-no- .................................................................................................. */
/*-no- #define xmlUnlinkNode xmlUnlinkNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef bottom_threads */
/*-no- #undef xmlUnlockLibrary */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUnlockLibrary */
/*-no- ........................................................................................................ */
/*-no- #define xmlUnlockLibrary xmlUnlockLibrary__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlUnsetNsProp */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlUnsetNsProp */
/*-no- .................................................................................................... */
/*-no- #define xmlUnsetNsProp xmlUnsetNsProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlUnsetProp */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlUnsetProp */
/*-no- ................................................................................................ */
/*-no- #define xmlUnsetProp xmlUnsetProp__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) && defined(LIBXML_REGEXP_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidBuildContentModel */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlValidBuildContentModel */
/*-no- .......................................................................................................................... */
/*-no- #define xmlValidBuildContentModel xmlValidBuildContentModel__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidCtxtNormalizeAttributeValue */
/*-no- ...................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidCtxtNormalizeAttributeValue */
/*-no- .............................................................................................................................................. */
/*-no- #define xmlValidCtxtNormalizeAttributeValue xmlValidCtxtNormalizeAttributeValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidGetPotentialChildren */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlValidGetPotentialChildren */
/*-no- ................................................................................................................................ */
/*-no- #define xmlValidGetPotentialChildren xmlValidGetPotentialChildren__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidGetValidElements */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidGetValidElements */
/*-no- ........................................................................................................................ */
/*-no- #define xmlValidGetValidElements xmlValidGetValidElements__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidNormalizeAttributeValue */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidNormalizeAttributeValue */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlValidNormalizeAttributeValue xmlValidNormalizeAttributeValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateAttributeDecl */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidateAttributeDecl */
/*-no- ........................................................................................................................ */
/*-no- #define xmlValidateAttributeDecl xmlValidateAttributeDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateAttributeValue */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlValidateAttributeValue */
/*-no- .......................................................................................................................... */
/*-no- #define xmlValidateAttributeValue xmlValidateAttributeValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateDocument */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidateDocument */
/*-no- .............................................................................................................. */
/*-no- #define xmlValidateDocument xmlValidateDocument__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateDocumentFinal */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidateDocumentFinal */
/*-no- ........................................................................................................................ */
/*-no- #define xmlValidateDocumentFinal xmlValidateDocumentFinal__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateDtd */
/*-no- ....................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidateDtd */
/*-no- .................................................................................................... */
/*-no- #define xmlValidateDtd xmlValidateDtd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateDtdFinal */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidateDtdFinal */
/*-no- .............................................................................................................. */
/*-no- #define xmlValidateDtdFinal xmlValidateDtdFinal__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateElement */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidateElement */
/*-no- ............................................................................................................ */
/*-no- #define xmlValidateElement xmlValidateElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateElementDecl */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidateElementDecl */
/*-no- .................................................................................................................... */
/*-no- #define xmlValidateElementDecl xmlValidateElementDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_XPATH_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) || defined(LIBXML_DEBUG_ENABLED) || defined (LIBXML_HTML_ENABLED) || defined(LIBXML_SAX1_ENABLED) || defined(LIBXML_HTML_ENABLED) || defined(LIBXML_WRITER_ENABLED) || defined(LIBXML_DOCB_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlValidateNCName */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlValidateNCName */
/*-no- .......................................................................................................... */
/*-no- #define xmlValidateNCName xmlValidateNCName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlValidateNMToken */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidateNMToken */
/*-no- ............................................................................................................ */
/*-no- #define xmlValidateNMToken xmlValidateNMToken__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlValidateName */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidateName */
/*-no- ...................................................................................................... */
/*-no- #define xmlValidateName xmlValidateName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateNameValue */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidateNameValue */
/*-no- ................................................................................................................ */
/*-no- #define xmlValidateNameValue xmlValidateNameValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateNamesValue */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlValidateNamesValue */
/*-no- .................................................................................................................. */
/*-no- #define xmlValidateNamesValue xmlValidateNamesValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateNmtokenValue */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlValidateNmtokenValue */
/*-no- ...................................................................................................................... */
/*-no- #define xmlValidateNmtokenValue xmlValidateNmtokenValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateNmtokensValue */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidateNmtokensValue */
/*-no- ........................................................................................................................ */
/*-no- #define xmlValidateNmtokensValue xmlValidateNmtokensValue__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateNotationDecl */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlValidateNotationDecl */
/*-no- ...................................................................................................................... */
/*-no- #define xmlValidateNotationDecl xmlValidateNotationDecl__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateNotationUse */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidateNotationUse */
/*-no- .................................................................................................................... */
/*-no- #define xmlValidateNotationUse xmlValidateNotationUse__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateOneAttribute */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlValidateOneAttribute */
/*-no- ...................................................................................................................... */
/*-no- #define xmlValidateOneAttribute xmlValidateOneAttribute__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateOneElement */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlValidateOneElement */
/*-no- .................................................................................................................. */
/*-no- #define xmlValidateOneElement xmlValidateOneElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateOneNamespace */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlValidateOneNamespace */
/*-no- ...................................................................................................................... */
/*-no- #define xmlValidateOneNamespace xmlValidateOneNamespace__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) && defined(LIBXML_REGEXP_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidatePopElement */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlValidatePopElement */
/*-no- .................................................................................................................. */
/*-no- #define xmlValidatePopElement xmlValidatePopElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) && defined(LIBXML_REGEXP_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidatePushCData */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidatePushCData */
/*-no- ................................................................................................................ */
/*-no- #define xmlValidatePushCData xmlValidatePushCData__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) && defined(LIBXML_REGEXP_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidatePushElement */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidatePushElement */
/*-no- .................................................................................................................... */
/*-no- #define xmlValidatePushElement xmlValidatePushElement__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_TREE_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_tree */
/*-no- #undef xmlValidateQName */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlValidateQName */
/*-no- ........................................................................................................ */
/*-no- #define xmlValidateQName xmlValidateQName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_VALID_ENABLED) */
/*-no- #ifdef bottom_valid */
/*-no- #undef xmlValidateRoot */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlValidateRoot */
/*-no- ...................................................................................................... */
/*-no- #define xmlValidateRoot xmlValidateRoot__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XINCLUDE_ENABLED) */
/*-no- #ifdef bottom_xinclude */
/*-no- #undef xmlXIncludeFreeContext */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXIncludeFreeContext */
/*-no- .................................................................................................................... */
/*-no- #define xmlXIncludeFreeContext xmlXIncludeFreeContext__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XINCLUDE_ENABLED) */
/*-no- #ifdef bottom_xinclude */
/*-no- #undef xmlXIncludeNewContext */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXIncludeNewContext */
/*-no- .................................................................................................................. */
/*-no- #define xmlXIncludeNewContext xmlXIncludeNewContext__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XINCLUDE_ENABLED) */
/*-no- #ifdef bottom_xinclude */
/*-no- #undef xmlXIncludeProcess */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXIncludeProcess */
/*-no- ............................................................................................................ */
/*-no- #define xmlXIncludeProcess xmlXIncludeProcess__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XINCLUDE_ENABLED) */
/*-no- #ifdef bottom_xinclude */
/*-no- #undef xmlXIncludeProcessFlags */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXIncludeProcessFlags */
/*-no- ...................................................................................................................... */
/*-no- #define xmlXIncludeProcessFlags xmlXIncludeProcessFlags__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XINCLUDE_ENABLED) */
/*-no- #ifdef bottom_xinclude */
/*-no- #undef xmlXIncludeProcessFlagsData */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXIncludeProcessFlagsData */
/*-no- .............................................................................................................................. */
/*-no- #define xmlXIncludeProcessFlagsData xmlXIncludeProcessFlagsData__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XINCLUDE_ENABLED) */
/*-no- #ifdef bottom_xinclude */
/*-no- #undef xmlXIncludeProcessNode */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXIncludeProcessNode */
/*-no- .................................................................................................................... */
/*-no- #define xmlXIncludeProcessNode xmlXIncludeProcessNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XINCLUDE_ENABLED) */
/*-no- #ifdef bottom_xinclude */
/*-no- #undef xmlXIncludeProcessTree */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXIncludeProcessTree */
/*-no- .................................................................................................................... */
/*-no- #define xmlXIncludeProcessTree xmlXIncludeProcessTree__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XINCLUDE_ENABLED) */
/*-no- #ifdef bottom_xinclude */
/*-no- #undef xmlXIncludeProcessTreeFlags */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXIncludeProcessTreeFlags */
/*-no- .............................................................................................................................. */
/*-no- #define xmlXIncludeProcessTreeFlags xmlXIncludeProcessTreeFlags__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XINCLUDE_ENABLED) */
/*-no- #ifdef bottom_xinclude */
/*-no- #undef xmlXIncludeProcessTreeFlagsData */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXIncludeProcessTreeFlagsData */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlXIncludeProcessTreeFlagsData xmlXIncludeProcessTreeFlagsData__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XINCLUDE_ENABLED) */
/*-no- #ifdef bottom_xinclude */
/*-no- #undef xmlXIncludeSetFlags */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXIncludeSetFlags */
/*-no- .............................................................................................................. */
/*-no- #define xmlXIncludeSetFlags xmlXIncludeSetFlags__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathAddValues */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathAddValues */
/*-no- .......................................................................................................... */
/*-no- #define xmlXPathAddValues xmlXPathAddValues__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathBooleanFunction */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathBooleanFunction */
/*-no- ...................................................................................................................... */
/*-no- #define xmlXPathBooleanFunction xmlXPathBooleanFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCastBooleanToNumber */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathCastBooleanToNumber */
/*-no- .............................................................................................................................. */
/*-no- #define xmlXPathCastBooleanToNumber xmlXPathCastBooleanToNumber__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCastBooleanToString */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathCastBooleanToString */
/*-no- .............................................................................................................................. */
/*-no- #define xmlXPathCastBooleanToString xmlXPathCastBooleanToString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCastNodeSetToBoolean */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathCastNodeSetToBoolean */
/*-no- ................................................................................................................................ */
/*-no- #define xmlXPathCastNodeSetToBoolean xmlXPathCastNodeSetToBoolean__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCastNodeSetToNumber */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathCastNodeSetToNumber */
/*-no- .............................................................................................................................. */
/*-no- #define xmlXPathCastNodeSetToNumber xmlXPathCastNodeSetToNumber__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCastNodeSetToString */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathCastNodeSetToString */
/*-no- .............................................................................................................................. */
/*-no- #define xmlXPathCastNodeSetToString xmlXPathCastNodeSetToString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCastNodeToNumber */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathCastNodeToNumber */
/*-no- ........................................................................................................................ */
/*-no- #define xmlXPathCastNodeToNumber xmlXPathCastNodeToNumber__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCastNodeToString */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathCastNodeToString */
/*-no- ........................................................................................................................ */
/*-no- #define xmlXPathCastNodeToString xmlXPathCastNodeToString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCastNumberToBoolean */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathCastNumberToBoolean */
/*-no- .............................................................................................................................. */
/*-no- #define xmlXPathCastNumberToBoolean xmlXPathCastNumberToBoolean__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCastNumberToString */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathCastNumberToString */
/*-no- ............................................................................................................................ */
/*-no- #define xmlXPathCastNumberToString xmlXPathCastNumberToString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCastStringToBoolean */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathCastStringToBoolean */
/*-no- .............................................................................................................................. */
/*-no- #define xmlXPathCastStringToBoolean xmlXPathCastStringToBoolean__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCastStringToNumber */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathCastStringToNumber */
/*-no- ............................................................................................................................ */
/*-no- #define xmlXPathCastStringToNumber xmlXPathCastStringToNumber__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCastToBoolean */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathCastToBoolean */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathCastToBoolean xmlXPathCastToBoolean__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCastToNumber */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathCastToNumber */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathCastToNumber xmlXPathCastToNumber__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCastToString */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathCastToString */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathCastToString xmlXPathCastToString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCeilingFunction */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathCeilingFunction */
/*-no- ...................................................................................................................... */
/*-no- #define xmlXPathCeilingFunction xmlXPathCeilingFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCmpNodes */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathCmpNodes */
/*-no- ........................................................................................................ */
/*-no- #define xmlXPathCmpNodes xmlXPathCmpNodes__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCompareValues */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathCompareValues */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathCompareValues xmlXPathCompareValues__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCompile */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathCompile */
/*-no- ...................................................................................................... */
/*-no- #define xmlXPathCompile xmlXPathCompile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCompiledEval */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathCompiledEval */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathCompiledEval xmlXPathCompiledEval__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCompiledEvalToBoolean */
/*-no- .................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathCompiledEvalToBoolean */
/*-no- .................................................................................................................................. */
/*-no- #define xmlXPathCompiledEvalToBoolean xmlXPathCompiledEvalToBoolean__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathConcatFunction */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathConcatFunction */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPathConcatFunction xmlXPathConcatFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathContainsFunction */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathContainsFunction */
/*-no- ........................................................................................................................ */
/*-no- #define xmlXPathContainsFunction xmlXPathContainsFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathContextSetCache */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathContextSetCache */
/*-no- ...................................................................................................................... */
/*-no- #define xmlXPathContextSetCache xmlXPathContextSetCache__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathConvertBoolean */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathConvertBoolean */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPathConvertBoolean xmlXPathConvertBoolean__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathConvertNumber */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathConvertNumber */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathConvertNumber xmlXPathConvertNumber__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathConvertString */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathConvertString */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathConvertString xmlXPathConvertString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCountFunction */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathCountFunction */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathCountFunction xmlXPathCountFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathCtxtCompile */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathCtxtCompile */
/*-no- .............................................................................................................. */
/*-no- #define xmlXPathCtxtCompile xmlXPathCtxtCompile__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) && defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathDebugDumpCompExpr */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathDebugDumpCompExpr */
/*-no- .......................................................................................................................... */
/*-no- #define xmlXPathDebugDumpCompExpr xmlXPathDebugDumpCompExpr__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) && defined(LIBXML_DEBUG_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathDebugDumpObject */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathDebugDumpObject */
/*-no- ...................................................................................................................... */
/*-no- #define xmlXPathDebugDumpObject xmlXPathDebugDumpObject__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathDifference */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathDifference */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathDifference xmlXPathDifference__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathDistinct */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathDistinct */
/*-no- ........................................................................................................ */
/*-no- #define xmlXPathDistinct xmlXPathDistinct__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathDistinctSorted */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathDistinctSorted */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPathDistinctSorted xmlXPathDistinctSorted__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathDivValues */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathDivValues */
/*-no- .......................................................................................................... */
/*-no- #define xmlXPathDivValues xmlXPathDivValues__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathEqualValues */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathEqualValues */
/*-no- .............................................................................................................. */
/*-no- #define xmlXPathEqualValues xmlXPathEqualValues__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathErr */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathErr */
/*-no- .............................................................................................. */
/*-no- #define xmlXPathErr xmlXPathErr__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathEval */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathEval */
/*-no- ................................................................................................ */
/*-no- #define xmlXPathEval xmlXPathEval__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathEvalExpr */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathEvalExpr */
/*-no- ........................................................................................................ */
/*-no- #define xmlXPathEvalExpr xmlXPathEvalExpr__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathEvalExpression */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathEvalExpression */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPathEvalExpression xmlXPathEvalExpression__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathEvalPredicate */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathEvalPredicate */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathEvalPredicate xmlXPathEvalPredicate__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathEvaluatePredicateResult */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathEvaluatePredicateResult */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlXPathEvaluatePredicateResult xmlXPathEvaluatePredicateResult__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathFalseFunction */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathFalseFunction */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathFalseFunction xmlXPathFalseFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathFloorFunction */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathFloorFunction */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathFloorFunction xmlXPathFloorFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathFreeCompExpr */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathFreeCompExpr */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathFreeCompExpr xmlXPathFreeCompExpr__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathFreeContext */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathFreeContext */
/*-no- .............................................................................................................. */
/*-no- #define xmlXPathFreeContext xmlXPathFreeContext__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathFreeNodeSet */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathFreeNodeSet */
/*-no- .............................................................................................................. */
/*-no- #define xmlXPathFreeNodeSet xmlXPathFreeNodeSet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathFreeNodeSetList */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathFreeNodeSetList */
/*-no- ...................................................................................................................... */
/*-no- #define xmlXPathFreeNodeSetList xmlXPathFreeNodeSetList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathFreeObject */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathFreeObject */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathFreeObject xmlXPathFreeObject__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathFreeParserContext */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathFreeParserContext */
/*-no- .......................................................................................................................... */
/*-no- #define xmlXPathFreeParserContext xmlXPathFreeParserContext__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathFunctionLookup */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathFunctionLookup */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPathFunctionLookup xmlXPathFunctionLookup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathFunctionLookupNS */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathFunctionLookupNS */
/*-no- ........................................................................................................................ */
/*-no- #define xmlXPathFunctionLookupNS xmlXPathFunctionLookupNS__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathHasSameNodes */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathHasSameNodes */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathHasSameNodes xmlXPathHasSameNodes__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathIdFunction */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathIdFunction */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathIdFunction xmlXPathIdFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathInit */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathInit */
/*-no- ................................................................................................ */
/*-no- #define xmlXPathInit xmlXPathInit__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathIntersection */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathIntersection */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathIntersection xmlXPathIntersection__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathIsInf */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathIsInf */
/*-no- .................................................................................................. */
/*-no- #define xmlXPathIsInf xmlXPathIsInf__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) || defined(LIBXML_SCHEMAS_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathIsNaN */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathIsNaN */
/*-no- .................................................................................................. */
/*-no- #define xmlXPathIsNaN xmlXPathIsNaN__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathIsNodeType */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathIsNodeType */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathIsNodeType xmlXPathIsNodeType__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathLangFunction */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathLangFunction */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathLangFunction xmlXPathLangFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathLastFunction */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathLastFunction */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathLastFunction xmlXPathLastFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathLeading */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathLeading */
/*-no- ...................................................................................................... */
/*-no- #define xmlXPathLeading xmlXPathLeading__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathLeadingSorted */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathLeadingSorted */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathLeadingSorted xmlXPathLeadingSorted__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathLocalNameFunction */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathLocalNameFunction */
/*-no- .......................................................................................................................... */
/*-no- #define xmlXPathLocalNameFunction xmlXPathLocalNameFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathModValues */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathModValues */
/*-no- .......................................................................................................... */
/*-no- #define xmlXPathModValues xmlXPathModValues__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathMultValues */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathMultValues */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathMultValues xmlXPathMultValues__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNamespaceURIFunction */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathNamespaceURIFunction */
/*-no- ................................................................................................................................ */
/*-no- #define xmlXPathNamespaceURIFunction xmlXPathNamespaceURIFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNewBoolean */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNewBoolean */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathNewBoolean xmlXPathNewBoolean__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNewCString */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNewCString */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathNewCString xmlXPathNewCString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNewContext */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNewContext */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathNewContext xmlXPathNewContext__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNewFloat */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathNewFloat */
/*-no- ........................................................................................................ */
/*-no- #define xmlXPathNewFloat xmlXPathNewFloat__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNewNodeSet */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNewNodeSet */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathNewNodeSet xmlXPathNewNodeSet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNewNodeSetList */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNewNodeSetList */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPathNewNodeSetList xmlXPathNewNodeSetList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNewParserContext */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNewParserContext */
/*-no- ........................................................................................................................ */
/*-no- #define xmlXPathNewParserContext xmlXPathNewParserContext__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNewString */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathNewString */
/*-no- .......................................................................................................... */
/*-no- #define xmlXPathNewString xmlXPathNewString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNewValueTree */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNewValueTree */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathNewValueTree xmlXPathNewValueTree__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNextAncestor */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNextAncestor */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathNextAncestor xmlXPathNextAncestor__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNextAncestorOrSelf */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNextAncestorOrSelf */
/*-no- ............................................................................................................................ */
/*-no- #define xmlXPathNextAncestorOrSelf xmlXPathNextAncestorOrSelf__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNextAttribute */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathNextAttribute */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathNextAttribute xmlXPathNextAttribute__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNextChild */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathNextChild */
/*-no- .......................................................................................................... */
/*-no- #define xmlXPathNextChild xmlXPathNextChild__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNextDescendant */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNextDescendant */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPathNextDescendant xmlXPathNextDescendant__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNextDescendantOrSelf */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathNextDescendantOrSelf */
/*-no- ................................................................................................................................ */
/*-no- #define xmlXPathNextDescendantOrSelf xmlXPathNextDescendantOrSelf__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNextFollowing */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathNextFollowing */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathNextFollowing xmlXPathNextFollowing__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNextFollowingSibling */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathNextFollowingSibling */
/*-no- ................................................................................................................................ */
/*-no- #define xmlXPathNextFollowingSibling xmlXPathNextFollowingSibling__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNextNamespace */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathNextNamespace */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathNextNamespace xmlXPathNextNamespace__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNextParent */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNextParent */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathNextParent xmlXPathNextParent__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNextPreceding */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathNextPreceding */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathNextPreceding xmlXPathNextPreceding__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNextPrecedingSibling */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathNextPrecedingSibling */
/*-no- ................................................................................................................................ */
/*-no- #define xmlXPathNextPrecedingSibling xmlXPathNextPrecedingSibling__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNextSelf */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathNextSelf */
/*-no- ........................................................................................................ */
/*-no- #define xmlXPathNextSelf xmlXPathNextSelf__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNodeLeading */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNodeLeading */
/*-no- .............................................................................................................. */
/*-no- #define xmlXPathNodeLeading xmlXPathNodeLeading__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNodeLeadingSorted */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathNodeLeadingSorted */
/*-no- .......................................................................................................................... */
/*-no- #define xmlXPathNodeLeadingSorted xmlXPathNodeLeadingSorted__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNodeSetAdd */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNodeSetAdd */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathNodeSetAdd xmlXPathNodeSetAdd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNodeSetAddNs */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNodeSetAddNs */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathNodeSetAddNs xmlXPathNodeSetAddNs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNodeSetAddUnique */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNodeSetAddUnique */
/*-no- ........................................................................................................................ */
/*-no- #define xmlXPathNodeSetAddUnique xmlXPathNodeSetAddUnique__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNodeSetContains */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathNodeSetContains */
/*-no- ...................................................................................................................... */
/*-no- #define xmlXPathNodeSetContains xmlXPathNodeSetContains__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNodeSetCreate */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathNodeSetCreate */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathNodeSetCreate xmlXPathNodeSetCreate__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNodeSetDel */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNodeSetDel */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathNodeSetDel xmlXPathNodeSetDel__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNodeSetFreeNs */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathNodeSetFreeNs */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathNodeSetFreeNs xmlXPathNodeSetFreeNs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNodeSetMerge */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNodeSetMerge */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathNodeSetMerge xmlXPathNodeSetMerge__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNodeSetRemove */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathNodeSetRemove */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathNodeSetRemove xmlXPathNodeSetRemove__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNodeSetSort */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNodeSetSort */
/*-no- .............................................................................................................. */
/*-no- #define xmlXPathNodeSetSort xmlXPathNodeSetSort__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNodeTrailing */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNodeTrailing */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathNodeTrailing xmlXPathNodeTrailing__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNodeTrailingSorted */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNodeTrailingSorted */
/*-no- ............................................................................................................................ */
/*-no- #define xmlXPathNodeTrailingSorted xmlXPathNodeTrailingSorted__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNormalizeFunction */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathNormalizeFunction */
/*-no- .......................................................................................................................... */
/*-no- #define xmlXPathNormalizeFunction xmlXPathNormalizeFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNotEqualValues */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNotEqualValues */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPathNotEqualValues xmlXPathNotEqualValues__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNotFunction */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNotFunction */
/*-no- .............................................................................................................. */
/*-no- #define xmlXPathNotFunction xmlXPathNotFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNsLookup */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathNsLookup */
/*-no- ........................................................................................................ */
/*-no- #define xmlXPathNsLookup xmlXPathNsLookup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathNumberFunction */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathNumberFunction */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPathNumberFunction xmlXPathNumberFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathObjectCopy */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathObjectCopy */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathObjectCopy xmlXPathObjectCopy__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathOrderDocElems */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathOrderDocElems */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathOrderDocElems xmlXPathOrderDocElems__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathParseNCName */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathParseNCName */
/*-no- .............................................................................................................. */
/*-no- #define xmlXPathParseNCName xmlXPathParseNCName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathParseName */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathParseName */
/*-no- .......................................................................................................... */
/*-no- #define xmlXPathParseName xmlXPathParseName__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathPopBoolean */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathPopBoolean */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathPopBoolean xmlXPathPopBoolean__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathPopExternal */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathPopExternal */
/*-no- .............................................................................................................. */
/*-no- #define xmlXPathPopExternal xmlXPathPopExternal__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathPopNodeSet */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathPopNodeSet */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathPopNodeSet xmlXPathPopNodeSet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathPopNumber */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathPopNumber */
/*-no- .......................................................................................................... */
/*-no- #define xmlXPathPopNumber xmlXPathPopNumber__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathPopString */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathPopString */
/*-no- .......................................................................................................... */
/*-no- #define xmlXPathPopString xmlXPathPopString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathPositionFunction */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathPositionFunction */
/*-no- ........................................................................................................................ */
/*-no- #define xmlXPathPositionFunction xmlXPathPositionFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathRegisterAllFunctions */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathRegisterAllFunctions */
/*-no- ................................................................................................................................ */
/*-no- #define xmlXPathRegisterAllFunctions xmlXPathRegisterAllFunctions__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathRegisterFunc */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathRegisterFunc */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathRegisterFunc xmlXPathRegisterFunc__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathRegisterFuncLookup */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathRegisterFuncLookup */
/*-no- ............................................................................................................................ */
/*-no- #define xmlXPathRegisterFuncLookup xmlXPathRegisterFuncLookup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathRegisterFuncNS */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathRegisterFuncNS */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPathRegisterFuncNS xmlXPathRegisterFuncNS__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathRegisterNs */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathRegisterNs */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathRegisterNs xmlXPathRegisterNs__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathRegisterVariable */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathRegisterVariable */
/*-no- ........................................................................................................................ */
/*-no- #define xmlXPathRegisterVariable xmlXPathRegisterVariable__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathRegisterVariableLookup */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathRegisterVariableLookup */
/*-no- .................................................................................................................................... */
/*-no- #define xmlXPathRegisterVariableLookup xmlXPathRegisterVariableLookup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathRegisterVariableNS */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathRegisterVariableNS */
/*-no- ............................................................................................................................ */
/*-no- #define xmlXPathRegisterVariableNS xmlXPathRegisterVariableNS__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathRegisteredFuncsCleanup */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathRegisteredFuncsCleanup */
/*-no- .................................................................................................................................... */
/*-no- #define xmlXPathRegisteredFuncsCleanup xmlXPathRegisteredFuncsCleanup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathRegisteredNsCleanup */
/*-no- .............................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathRegisteredNsCleanup */
/*-no- .............................................................................................................................. */
/*-no- #define xmlXPathRegisteredNsCleanup xmlXPathRegisteredNsCleanup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathRegisteredVariablesCleanup */
/*-no- ................................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathRegisteredVariablesCleanup */
/*-no- ............................................................................................................................................ */
/*-no- #define xmlXPathRegisteredVariablesCleanup xmlXPathRegisteredVariablesCleanup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathRoot */
/*-no- ................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathRoot */
/*-no- ................................................................................................ */
/*-no- #define xmlXPathRoot xmlXPathRoot__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathRoundFunction */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathRoundFunction */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathRoundFunction xmlXPathRoundFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathStartsWithFunction */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathStartsWithFunction */
/*-no- ............................................................................................................................ */
/*-no- #define xmlXPathStartsWithFunction xmlXPathStartsWithFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathStringEvalNumber */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathStringEvalNumber */
/*-no- ........................................................................................................................ */
/*-no- #define xmlXPathStringEvalNumber xmlXPathStringEvalNumber__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathStringFunction */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathStringFunction */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPathStringFunction xmlXPathStringFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathStringLengthFunction */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathStringLengthFunction */
/*-no- ................................................................................................................................ */
/*-no- #define xmlXPathStringLengthFunction xmlXPathStringLengthFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathSubValues */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathSubValues */
/*-no- .......................................................................................................... */
/*-no- #define xmlXPathSubValues xmlXPathSubValues__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathSubstringAfterFunction */
/*-no- ....................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathSubstringAfterFunction */
/*-no- .................................................................................................................................... */
/*-no- #define xmlXPathSubstringAfterFunction xmlXPathSubstringAfterFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathSubstringBeforeFunction */
/*-no- .......................................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathSubstringBeforeFunction */
/*-no- ...................................................................................................................................... */
/*-no- #define xmlXPathSubstringBeforeFunction xmlXPathSubstringBeforeFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathSubstringFunction */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathSubstringFunction */
/*-no- .......................................................................................................................... */
/*-no- #define xmlXPathSubstringFunction xmlXPathSubstringFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathSumFunction */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathSumFunction */
/*-no- .............................................................................................................. */
/*-no- #define xmlXPathSumFunction xmlXPathSumFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathTrailing */
/*-no- ............................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPathTrailing */
/*-no- ........................................................................................................ */
/*-no- #define xmlXPathTrailing xmlXPathTrailing__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathTrailingSorted */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathTrailingSorted */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPathTrailingSorted xmlXPathTrailingSorted__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathTranslateFunction */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathTranslateFunction */
/*-no- .......................................................................................................................... */
/*-no- #define xmlXPathTranslateFunction xmlXPathTranslateFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathTrueFunction */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathTrueFunction */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathTrueFunction xmlXPathTrueFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathValueFlipSign */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPathValueFlipSign */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPathValueFlipSign xmlXPathValueFlipSign__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathVariableLookup */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathVariableLookup */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPathVariableLookup xmlXPathVariableLookup__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathVariableLookupNS */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathVariableLookupNS */
/*-no- ........................................................................................................................ */
/*-no- #define xmlXPathVariableLookupNS xmlXPathVariableLookupNS__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathWrapCString */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathWrapCString */
/*-no- .............................................................................................................. */
/*-no- #define xmlXPathWrapCString xmlXPathWrapCString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathWrapExternal */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathWrapExternal */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPathWrapExternal xmlXPathWrapExternal__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathWrapNodeSet */
/*-no- ...................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathWrapNodeSet */
/*-no- .............................................................................................................. */
/*-no- #define xmlXPathWrapNodeSet xmlXPathWrapNodeSet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPathWrapString */
/*-no- ................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPathWrapString */
/*-no- ............................................................................................................ */
/*-no- #define xmlXPathWrapString xmlXPathWrapString__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPATH_ENABLED) */
/*-no- #ifdef bottom_xpath */
/*-no- #undef xmlXPatherror */
/*-no- .................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPatherror */
/*-no- .................................................................................................. */
/*-no- #define xmlXPatherror xmlXPatherror__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrBuildNodeList */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPtrBuildNodeList */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPtrBuildNodeList xmlXPtrBuildNodeList__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrEval */
/*-no- .............................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPtrEval */
/*-no- .............................................................................................. */
/*-no- #define xmlXPtrEval xmlXPtrEval__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrEvalRangePredicate */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPtrEvalRangePredicate */
/*-no- .......................................................................................................................... */
/*-no- #define xmlXPtrEvalRangePredicate xmlXPtrEvalRangePredicate__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrFreeLocationSet */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPtrFreeLocationSet */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPtrFreeLocationSet xmlXPtrFreeLocationSet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrLocationSetAdd */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPtrLocationSetAdd */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPtrLocationSetAdd xmlXPtrLocationSetAdd__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrLocationSetCreate */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPtrLocationSetCreate */
/*-no- ........................................................................................................................ */
/*-no- #define xmlXPtrLocationSetCreate xmlXPtrLocationSetCreate__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrLocationSetDel */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPtrLocationSetDel */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPtrLocationSetDel xmlXPtrLocationSetDel__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrLocationSetMerge */
/*-no- .................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPtrLocationSetMerge */
/*-no- ...................................................................................................................... */
/*-no- #define xmlXPtrLocationSetMerge xmlXPtrLocationSetMerge__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrLocationSetRemove */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPtrLocationSetRemove */
/*-no- ........................................................................................................................ */
/*-no- #define xmlXPtrLocationSetRemove xmlXPtrLocationSetRemove__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrNewCollapsedRange */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPtrNewCollapsedRange */
/*-no- ........................................................................................................................ */
/*-no- #define xmlXPtrNewCollapsedRange xmlXPtrNewCollapsedRange__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrNewContext */
/*-no- ................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPtrNewContext */
/*-no- .......................................................................................................... */
/*-no- #define xmlXPtrNewContext xmlXPtrNewContext__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrNewLocationSetNodeSet */
/*-no- ................................................................................................................................................. */
/*-no- #else */
/*-no- #ifndef xmlXPtrNewLocationSetNodeSet */
/*-no- ................................................................................................................................ */
/*-no- #define xmlXPtrNewLocationSetNodeSet xmlXPtrNewLocationSetNodeSet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrNewLocationSetNodes */
/*-no- ........................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPtrNewLocationSetNodes */
/*-no- ............................................................................................................................ */
/*-no- #define xmlXPtrNewLocationSetNodes xmlXPtrNewLocationSetNodes__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrNewRange */
/*-no- .......................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPtrNewRange */
/*-no- ...................................................................................................... */
/*-no- #define xmlXPtrNewRange xmlXPtrNewRange__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrNewRangeNodeObject */
/*-no- ........................................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPtrNewRangeNodeObject */
/*-no- .......................................................................................................................... */
/*-no- #define xmlXPtrNewRangeNodeObject xmlXPtrNewRangeNodeObject__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrNewRangeNodePoint */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPtrNewRangeNodePoint */
/*-no- ........................................................................................................................ */
/*-no- #define xmlXPtrNewRangeNodePoint xmlXPtrNewRangeNodePoint__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrNewRangeNodes */
/*-no- ......................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPtrNewRangeNodes */
/*-no- ................................................................................................................ */
/*-no- #define xmlXPtrNewRangeNodes xmlXPtrNewRangeNodes__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrNewRangePointNode */
/*-no- ..................................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPtrNewRangePointNode */
/*-no- ........................................................................................................................ */
/*-no- #define xmlXPtrNewRangePointNode xmlXPtrNewRangePointNode__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrNewRangePoints */
/*-no- ............................................................................................................................ */
/*-no- #else */
/*-no- #ifndef xmlXPtrNewRangePoints */
/*-no- .................................................................................................................. */
/*-no- #define xmlXPtrNewRangePoints xmlXPtrNewRangePoints__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrRangeToFunction */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPtrRangeToFunction */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPtrRangeToFunction xmlXPtrRangeToFunction__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(LIBXML_XPTR_ENABLED) */
/*-no- #ifdef bottom_xpointer */
/*-no- #undef xmlXPtrWrapLocationSet */
/*-no- ............................................................................................................................... */
/*-no- #else */
/*-no- #ifndef xmlXPtrWrapLocationSet */
/*-no- .................................................................................................................... */
/*-no- #define xmlXPtrWrapLocationSet xmlXPtrWrapLocationSet__internal_alias */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no-  */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no- #endif */
/*-no-  */

#endif
/* CONSBENCH file end */
