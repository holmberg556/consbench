/* CONSBENCH file begin */
#ifndef MAIN_LIBXML2_CPP
#define MAIN_LIBXML2_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_libxml_src_main_libxml2_cpp, "third_party/libxml/src/main_libxml2.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_libxml_src_main_libxml2_cpp, "third_party/libxml/src/main_libxml2.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_libxml_src_c14n_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_catalog_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_chvalid_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_debugXML_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_dict_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_DOCBparser_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_encoding_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_entities_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_error_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_globals_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_hash_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_HTMLparser_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_HTMLtree_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_legacy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_list_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_nanoftp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_nanohttp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_parser_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_parserInternals_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_pattern_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_relaxng_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_SAX_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_SAX2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_schematron_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_threads_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_tree_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_uri_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_valid_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_xinclude_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_xlink_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_xmlIO_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_xmlmemory_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_xmlmodule_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_xmlreader_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_xmlregexp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_xmlsave_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_xmlschemas_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_xmlschemastypes_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_xmlstring_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_xmlunicode_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_xmlwriter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_xpath_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxml_src_xpointer_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_libxml_src_main_libxml2_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_libxml_src_c14n_cpp(it);
  PUBLIC_third_party_libxml_src_catalog_cpp(it);
  PUBLIC_third_party_libxml_src_chvalid_cpp(it);
  PUBLIC_third_party_libxml_src_debugXML_cpp(it);
  PUBLIC_third_party_libxml_src_dict_cpp(it);
  PUBLIC_third_party_libxml_src_DOCBparser_cpp(it);
  PUBLIC_third_party_libxml_src_encoding_cpp(it);
  PUBLIC_third_party_libxml_src_entities_cpp(it);
  PUBLIC_third_party_libxml_src_error_cpp(it);
  PUBLIC_third_party_libxml_src_globals_cpp(it);
  PUBLIC_third_party_libxml_src_hash_cpp(it);
  PUBLIC_third_party_libxml_src_HTMLparser_cpp(it);
  PUBLIC_third_party_libxml_src_HTMLtree_cpp(it);
  PUBLIC_third_party_libxml_src_legacy_cpp(it);
  PUBLIC_third_party_libxml_src_list_cpp(it);
  PUBLIC_third_party_libxml_src_nanoftp_cpp(it);
  PUBLIC_third_party_libxml_src_nanohttp_cpp(it);
  PUBLIC_third_party_libxml_src_parser_cpp(it);
  PUBLIC_third_party_libxml_src_parserInternals_cpp(it);
  PUBLIC_third_party_libxml_src_pattern_cpp(it);
  PUBLIC_third_party_libxml_src_relaxng_cpp(it);
  PUBLIC_third_party_libxml_src_SAX_cpp(it);
  PUBLIC_third_party_libxml_src_SAX2_cpp(it);
  PUBLIC_third_party_libxml_src_schematron_cpp(it);
  PUBLIC_third_party_libxml_src_threads_cpp(it);
  PUBLIC_third_party_libxml_src_tree_cpp(it);
  PUBLIC_third_party_libxml_src_uri_cpp(it);
  PUBLIC_third_party_libxml_src_valid_cpp(it);
  PUBLIC_third_party_libxml_src_xinclude_cpp(it);
  PUBLIC_third_party_libxml_src_xlink_cpp(it);
  PUBLIC_third_party_libxml_src_xmlIO_cpp(it);
  PUBLIC_third_party_libxml_src_xmlmemory_cpp(it);
  PUBLIC_third_party_libxml_src_xmlmodule_cpp(it);
  PUBLIC_third_party_libxml_src_xmlreader_cpp(it);
  PUBLIC_third_party_libxml_src_xmlregexp_cpp(it);
  PUBLIC_third_party_libxml_src_xmlsave_cpp(it);
  PUBLIC_third_party_libxml_src_xmlschemas_cpp(it);
  PUBLIC_third_party_libxml_src_xmlschemastypes_cpp(it);
  PUBLIC_third_party_libxml_src_xmlstring_cpp(it);
  PUBLIC_third_party_libxml_src_xmlunicode_cpp(it);
  PUBLIC_third_party_libxml_src_xmlwriter_cpp(it);
  PUBLIC_third_party_libxml_src_xpath_cpp(it);
  PUBLIC_third_party_libxml_src_xpointer_cpp(it);
}

#endif

