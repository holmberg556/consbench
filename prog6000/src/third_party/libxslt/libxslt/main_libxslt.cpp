/* CONSBENCH file begin */
#ifndef MAIN_LIBXSLT_CPP
#define MAIN_LIBXSLT_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_libxslt_libxslt_main_libxslt_cpp, "third_party/libxslt/libxslt/main_libxslt.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_libxslt_libxslt_main_libxslt_cpp, "third_party/libxslt/libxslt/main_libxslt.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_libxslt_libxslt_attributes_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_attrvt_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_documents_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_extensions_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_extra_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_functions_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_imports_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_keys_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_namespaces_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_numbers_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_pattern_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_preproc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_security_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_templates_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_transform_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_variables_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_xslt_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_xsltlocale_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_libxslt_libxslt_xsltutils_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_libxslt_libxslt_main_libxslt_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_libxslt_libxslt_attributes_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_attrvt_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_documents_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_extensions_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_extra_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_functions_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_imports_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_keys_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_namespaces_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_numbers_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_pattern_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_preproc_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_security_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_templates_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_transform_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_variables_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_xslt_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_xsltlocale_cpp(it);
  PUBLIC_third_party_libxslt_libxslt_xsltutils_cpp(it);
}

#endif

