/* CONSBENCH file begin */
#ifndef GLXEXT_H_2553
#define GLXEXT_H_2553

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_mesa_MesaLib_include_GL_glxext_h, "third_party/mesa/MesaLib/include/GL/glxext.h");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_mesa_MesaLib_include_GL_glxext_h, "third_party/mesa/MesaLib/include/GL/glxext.h");

/*-no- #ifndef __glxext_h_ */
/*-no- #define __glxext_h_ */
/*-no-  */
/*-no- #ifdef __cplusplus */
/*-no- ............ */
/*-no- #endif */
/*-no-  */
/*-no- ... */
/*-no- ................................................. */
/*-no- ... */
/*-no- .......................................................................... */
/*-no- ................................................................... */
/*-no- ........................................................................ */
/*-no- ...................................................................... */
/*-no- ...................................................................... */
/*-no- .......................................................................... */
/*-no- ............................ */
/*-no- ... */
/*-no- .......................................................................... */
/*-no- .......................................................... */
/*-no- ... */
/*-no- .................................................................... */
/*-no- ..................................................................... */
/*-no- ......................................................................... */
/*-no- ....................................................................... */
/*-no- ....................................................................... */
/*-no- .................................................................... */
/*-no- ........................................................... */
/*-no- ... */
/*-no-  */
/*-no- ............................................................... */
/*-no-  */
/*-no- #if defined(_WIN32) && !defined(APIENTRY) && !defined(__CYGWIN__) && !defined(__SCITECH_SNAP__) */
/*-no- #define WIN32_LEAN_AND_MEAN 1 */
/*-no- #include <windows.h> */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef APIENTRY */
/*-no- #define APIENTRY */
/*-no- #endif */
/*-no- #ifndef APIENTRYP */
/*-no- #define APIENTRYP APIENTRY * */
/*-no- #endif */
/*-no- #ifndef GLAPI */
/*-no- #define GLAPI extern */
/*-no- #endif */
/*-no-  */
/*-no- ................................................................. */
/*-no-  */
/*-no- .................................................................... */
/*-no- ........................................ */
/*-no- .......................................................... */
/*-no- #define GLX_GLXEXT_VERSION 32 */
/*-no-  */
/*-no- #ifndef GLX_VERSION_1_3 */
/*-no- #define GLX_WINDOW_BIT                     0x00000001 */
/*-no- #define GLX_PIXMAP_BIT                     0x00000002 */
/*-no- #define GLX_PBUFFER_BIT                    0x00000004 */
/*-no- #define GLX_RGBA_BIT                       0x00000001 */
/*-no- #define GLX_COLOR_INDEX_BIT                0x00000002 */
/*-no- #define GLX_PBUFFER_CLOBBER_MASK           0x08000000 */
/*-no- #define GLX_FRONT_LEFT_BUFFER_BIT          0x00000001 */
/*-no- #define GLX_FRONT_RIGHT_BUFFER_BIT         0x00000002 */
/*-no- #define GLX_BACK_LEFT_BUFFER_BIT           0x00000004 */
/*-no- #define GLX_BACK_RIGHT_BUFFER_BIT          0x00000008 */
/*-no- #define GLX_AUX_BUFFERS_BIT                0x00000010 */
/*-no- #define GLX_DEPTH_BUFFER_BIT               0x00000020 */
/*-no- #define GLX_STENCIL_BUFFER_BIT             0x00000040 */
/*-no- #define GLX_ACCUM_BUFFER_BIT               0x00000080 */
/*-no- #define GLX_CONFIG_CAVEAT                  0x20 */
/*-no- #define GLX_X_VISUAL_TYPE                  0x22 */
/*-no- #define GLX_TRANSPARENT_TYPE               0x23 */
/*-no- #define GLX_TRANSPARENT_INDEX_VALUE        0x24 */
/*-no- #define GLX_TRANSPARENT_RED_VALUE          0x25 */
/*-no- #define GLX_TRANSPARENT_GREEN_VALUE        0x26 */
/*-no- #define GLX_TRANSPARENT_BLUE_VALUE         0x27 */
/*-no- #define GLX_TRANSPARENT_ALPHA_VALUE        0x28 */
/*-no- #define GLX_DONT_CARE                      0xFFFFFFFF */
/*-no- #define GLX_NONE                           0x8000 */
/*-no- #define GLX_SLOW_CONFIG                    0x8001 */
/*-no- #define GLX_TRUE_COLOR                     0x8002 */
/*-no- #define GLX_DIRECT_COLOR                   0x8003 */
/*-no- #define GLX_PSEUDO_COLOR                   0x8004 */
/*-no- #define GLX_STATIC_COLOR                   0x8005 */
/*-no- #define GLX_GRAY_SCALE                     0x8006 */
/*-no- #define GLX_STATIC_GRAY                    0x8007 */
/*-no- #define GLX_TRANSPARENT_RGB                0x8008 */
/*-no- #define GLX_TRANSPARENT_INDEX              0x8009 */
/*-no- #define GLX_VISUAL_ID                      0x800B */
/*-no- #define GLX_SCREEN                         0x800C */
/*-no- #define GLX_NON_CONFORMANT_CONFIG          0x800D */
/*-no- #define GLX_DRAWABLE_TYPE                  0x8010 */
/*-no- #define GLX_RENDER_TYPE                    0x8011 */
/*-no- #define GLX_X_RENDERABLE                   0x8012 */
/*-no- #define GLX_FBCONFIG_ID                    0x8013 */
/*-no- #define GLX_RGBA_TYPE                      0x8014 */
/*-no- #define GLX_COLOR_INDEX_TYPE               0x8015 */
/*-no- #define GLX_MAX_PBUFFER_WIDTH              0x8016 */
/*-no- #define GLX_MAX_PBUFFER_HEIGHT             0x8017 */
/*-no- #define GLX_MAX_PBUFFER_PIXELS             0x8018 */
/*-no- #define GLX_PRESERVED_CONTENTS             0x801B */
/*-no- #define GLX_LARGEST_PBUFFER                0x801C */
/*-no- #define GLX_WIDTH                          0x801D */
/*-no- #define GLX_HEIGHT                         0x801E */
/*-no- #define GLX_EVENT_MASK                     0x801F */
/*-no- #define GLX_DAMAGED                        0x8020 */
/*-no- #define GLX_SAVED                          0x8021 */
/*-no- #define GLX_WINDOW                         0x8022 */
/*-no- #define GLX_PBUFFER                        0x8023 */
/*-no- #define GLX_PBUFFER_HEIGHT                 0x8040 */
/*-no- #define GLX_PBUFFER_WIDTH                  0x8041 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_VERSION_1_4 */
/*-no- #define GLX_SAMPLE_BUFFERS                 100000 */
/*-no- #define GLX_SAMPLES                        100001 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_ARB_get_proc_address */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_ARB_multisample */
/*-no- #define GLX_SAMPLE_BUFFERS_ARB             100000 */
/*-no- #define GLX_SAMPLES_ARB                    100001 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_ARB_vertex_buffer_object */
/*-no- #define GLX_CONTEXT_ALLOW_BUFFER_BYTE_ORDER_MISMATCH_ARB 0x2095 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_ARB_fbconfig_float */
/*-no- #define GLX_RGBA_FLOAT_TYPE_ARB            0x20B9 */
/*-no- #define GLX_RGBA_FLOAT_BIT_ARB             0x00000004 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_ARB_framebuffer_sRGB */
/*-no- #define GLX_FRAMEBUFFER_SRGB_CAPABLE_ARB   0x20B2 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_ARB_create_context */
/*-no- #define GLX_CONTEXT_DEBUG_BIT_ARB          0x00000001 */
/*-no- #define GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB 0x00000002 */
/*-no- #define GLX_CONTEXT_MAJOR_VERSION_ARB      0x2091 */
/*-no- #define GLX_CONTEXT_MINOR_VERSION_ARB      0x2092 */
/*-no- #define GLX_CONTEXT_FLAGS_ARB              0x2094 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_ARB_create_context_profile */
/*-no- #define GLX_CONTEXT_CORE_PROFILE_BIT_ARB   0x00000001 */
/*-no- #define GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB 0x00000002 */
/*-no- #define GLX_CONTEXT_PROFILE_MASK_ARB       0x9126 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_ARB_create_context_robustness */
/*-no- #define GLX_CONTEXT_ROBUST_ACCESS_BIT_ARB  0x00000004 */
/*-no- #define GLX_LOSE_CONTEXT_ON_RESET_ARB      0x8252 */
/*-no- #define GLX_CONTEXT_RESET_NOTIFICATION_STRATEGY_ARB 0x8256 */
/*-no- #define GLX_NO_RESET_NOTIFICATION_ARB      0x8261 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIS_multisample */
/*-no- #define GLX_SAMPLE_BUFFERS_SGIS            100000 */
/*-no- #define GLX_SAMPLES_SGIS                   100001 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_EXT_visual_info */
/*-no- #define GLX_X_VISUAL_TYPE_EXT              0x22 */
/*-no- #define GLX_TRANSPARENT_TYPE_EXT           0x23 */
/*-no- #define GLX_TRANSPARENT_INDEX_VALUE_EXT    0x24 */
/*-no- #define GLX_TRANSPARENT_RED_VALUE_EXT      0x25 */
/*-no- #define GLX_TRANSPARENT_GREEN_VALUE_EXT    0x26 */
/*-no- #define GLX_TRANSPARENT_BLUE_VALUE_EXT     0x27 */
/*-no- #define GLX_TRANSPARENT_ALPHA_VALUE_EXT    0x28 */
/*-no- #define GLX_NONE_EXT                       0x8000 */
/*-no- #define GLX_TRUE_COLOR_EXT                 0x8002 */
/*-no- #define GLX_DIRECT_COLOR_EXT               0x8003 */
/*-no- #define GLX_PSEUDO_COLOR_EXT               0x8004 */
/*-no- #define GLX_STATIC_COLOR_EXT               0x8005 */
/*-no- #define GLX_GRAY_SCALE_EXT                 0x8006 */
/*-no- #define GLX_STATIC_GRAY_EXT                0x8007 */
/*-no- #define GLX_TRANSPARENT_RGB_EXT            0x8008 */
/*-no- #define GLX_TRANSPARENT_INDEX_EXT          0x8009 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGI_swap_control */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGI_video_sync */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGI_make_current_read */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_video_source */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_EXT_visual_rating */
/*-no- #define GLX_VISUAL_CAVEAT_EXT              0x20 */
/*-no- #define GLX_SLOW_VISUAL_EXT                0x8001 */
/*-no- #define GLX_NON_CONFORMANT_VISUAL_EXT      0x800D */
/*-no- .......................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_EXT_import_context */
/*-no- #define GLX_SHARE_CONTEXT_EXT              0x800A */
/*-no- #define GLX_VISUAL_ID_EXT                  0x800B */
/*-no- #define GLX_SCREEN_EXT                     0x800C */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_fbconfig */
/*-no- #define GLX_WINDOW_BIT_SGIX                0x00000001 */
/*-no- #define GLX_PIXMAP_BIT_SGIX                0x00000002 */
/*-no- #define GLX_RGBA_BIT_SGIX                  0x00000001 */
/*-no- #define GLX_COLOR_INDEX_BIT_SGIX           0x00000002 */
/*-no- #define GLX_DRAWABLE_TYPE_SGIX             0x8010 */
/*-no- #define GLX_RENDER_TYPE_SGIX               0x8011 */
/*-no- #define GLX_X_RENDERABLE_SGIX              0x8012 */
/*-no- #define GLX_FBCONFIG_ID_SGIX               0x8013 */
/*-no- #define GLX_RGBA_TYPE_SGIX                 0x8014 */
/*-no- #define GLX_COLOR_INDEX_TYPE_SGIX          0x8015 */
/*-no- ............................ */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_pbuffer */
/*-no- #define GLX_PBUFFER_BIT_SGIX               0x00000004 */
/*-no- #define GLX_BUFFER_CLOBBER_MASK_SGIX       0x08000000 */
/*-no- #define GLX_FRONT_LEFT_BUFFER_BIT_SGIX     0x00000001 */
/*-no- #define GLX_FRONT_RIGHT_BUFFER_BIT_SGIX    0x00000002 */
/*-no- #define GLX_BACK_LEFT_BUFFER_BIT_SGIX      0x00000004 */
/*-no- #define GLX_BACK_RIGHT_BUFFER_BIT_SGIX     0x00000008 */
/*-no- #define GLX_AUX_BUFFERS_BIT_SGIX           0x00000010 */
/*-no- #define GLX_DEPTH_BUFFER_BIT_SGIX          0x00000020 */
/*-no- #define GLX_STENCIL_BUFFER_BIT_SGIX        0x00000040 */
/*-no- #define GLX_ACCUM_BUFFER_BIT_SGIX          0x00000080 */
/*-no- #define GLX_SAMPLE_BUFFERS_BIT_SGIX        0x00000100 */
/*-no- #define GLX_MAX_PBUFFER_WIDTH_SGIX         0x8016 */
/*-no- #define GLX_MAX_PBUFFER_HEIGHT_SGIX        0x8017 */
/*-no- #define GLX_MAX_PBUFFER_PIXELS_SGIX        0x8018 */
/*-no- #define GLX_OPTIMAL_PBUFFER_WIDTH_SGIX     0x8019 */
/*-no- #define GLX_OPTIMAL_PBUFFER_HEIGHT_SGIX    0x801A */
/*-no- #define GLX_PRESERVED_CONTENTS_SGIX        0x801B */
/*-no- #define GLX_LARGEST_PBUFFER_SGIX           0x801C */
/*-no- #define GLX_WIDTH_SGIX                     0x801D */
/*-no- #define GLX_HEIGHT_SGIX                    0x801E */
/*-no- #define GLX_EVENT_MASK_SGIX                0x801F */
/*-no- #define GLX_DAMAGED_SGIX                   0x8020 */
/*-no- #define GLX_SAVED_SGIX                     0x8021 */
/*-no- #define GLX_WINDOW_SGIX                    0x8022 */
/*-no- #define GLX_PBUFFER_SGIX                   0x8023 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGI_cushion */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_video_resize */
/*-no- #define GLX_SYNC_FRAME_SGIX                0x00000000 */
/*-no- #define GLX_SYNC_SWAP_SGIX                 0x00000001 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_dmbuffer */
/*-no- #define GLX_DIGITAL_MEDIA_PBUFFER_SGIX     0x8024 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_swap_group */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_swap_barrier */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIS_blended_overlay */
/*-no- #define GLX_BLENDED_RGBA_SGIS              0x8025 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIS_shared_multisample */
/*-no- #define GLX_MULTISAMPLE_SUB_RECT_WIDTH_SGIS 0x8026 */
/*-no- #define GLX_MULTISAMPLE_SUB_RECT_HEIGHT_SGIS 0x8027 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SUN_get_transparent_index */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_3DFX_multisample */
/*-no- #define GLX_SAMPLE_BUFFERS_3DFX            0x8050 */
/*-no- #define GLX_SAMPLES_3DFX                   0x8051 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_MESA_copy_sub_buffer */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_MESA_pixmap_colormap */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_MESA_release_buffers */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_MESA_set_3dfx_mode */
/*-no- #define GLX_3DFX_WINDOW_MODE_MESA          0x1 */
/*-no- #define GLX_3DFX_FULLSCREEN_MODE_MESA      0x2 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_visual_select_group */
/*-no- #define GLX_VISUAL_SELECT_GROUP_SGIX       0x8028 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_OML_swap_method */
/*-no- #define GLX_SWAP_METHOD_OML                0x8060 */
/*-no- #define GLX_SWAP_EXCHANGE_OML              0x8061 */
/*-no- #define GLX_SWAP_COPY_OML                  0x8062 */
/*-no- #define GLX_SWAP_UNDEFINED_OML             0x8063 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_OML_sync_control */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_float_buffer */
/*-no- #define GLX_FLOAT_COMPONENTS_NV            0x20B0 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_hyperpipe */
/*-no- #define GLX_HYPERPIPE_PIPE_NAME_LENGTH_SGIX 80 */
/*-no- #define GLX_BAD_HYPERPIPE_CONFIG_SGIX      91 */
/*-no- #define GLX_BAD_HYPERPIPE_SGIX             92 */
/*-no- #define GLX_HYPERPIPE_DISPLAY_PIPE_SGIX    0x00000001 */
/*-no- #define GLX_HYPERPIPE_RENDER_PIPE_SGIX     0x00000002 */
/*-no- #define GLX_PIPE_RECT_SGIX                 0x00000001 */
/*-no- #define GLX_PIPE_RECT_LIMITS_SGIX          0x00000002 */
/*-no- #define GLX_HYPERPIPE_STEREO_SGIX          0x00000003 */
/*-no- #define GLX_HYPERPIPE_PIXEL_AVERAGE_SGIX   0x00000004 */
/*-no- #define GLX_HYPERPIPE_ID_SGIX              0x8030 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_MESA_agp_offset */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_EXT_fbconfig_packed_float */
/*-no- #define GLX_RGBA_UNSIGNED_FLOAT_TYPE_EXT   0x20B1 */
/*-no- #define GLX_RGBA_UNSIGNED_FLOAT_BIT_EXT    0x00000008 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_EXT_framebuffer_sRGB */
/*-no- #define GLX_FRAMEBUFFER_SRGB_CAPABLE_EXT   0x20B2 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_EXT_texture_from_pixmap */
/*-no- #define GLX_TEXTURE_1D_BIT_EXT             0x00000001 */
/*-no- #define GLX_TEXTURE_2D_BIT_EXT             0x00000002 */
/*-no- #define GLX_TEXTURE_RECTANGLE_BIT_EXT      0x00000004 */
/*-no- #define GLX_BIND_TO_TEXTURE_RGB_EXT        0x20D0 */
/*-no- #define GLX_BIND_TO_TEXTURE_RGBA_EXT       0x20D1 */
/*-no- #define GLX_BIND_TO_MIPMAP_TEXTURE_EXT     0x20D2 */
/*-no- #define GLX_BIND_TO_TEXTURE_TARGETS_EXT    0x20D3 */
/*-no- #define GLX_Y_INVERTED_EXT                 0x20D4 */
/*-no- #define GLX_TEXTURE_FORMAT_EXT             0x20D5 */
/*-no- #define GLX_TEXTURE_TARGET_EXT             0x20D6 */
/*-no- #define GLX_MIPMAP_TEXTURE_EXT             0x20D7 */
/*-no- #define GLX_TEXTURE_FORMAT_NONE_EXT        0x20D8 */
/*-no- #define GLX_TEXTURE_FORMAT_RGB_EXT         0x20D9 */
/*-no- #define GLX_TEXTURE_FORMAT_RGBA_EXT        0x20DA */
/*-no- #define GLX_TEXTURE_1D_EXT                 0x20DB */
/*-no- #define GLX_TEXTURE_2D_EXT                 0x20DC */
/*-no- #define GLX_TEXTURE_RECTANGLE_EXT          0x20DD */
/*-no- #define GLX_FRONT_LEFT_EXT                 0x20DE */
/*-no- #define GLX_FRONT_RIGHT_EXT                0x20DF */
/*-no- #define GLX_BACK_LEFT_EXT                  0x20E0 */
/*-no- #define GLX_BACK_RIGHT_EXT                 0x20E1 */
/*-no- #define GLX_FRONT_EXT                      GLX_FRONT_LEFT_EXT */
/*-no- #define GLX_BACK_EXT                       GLX_BACK_LEFT_EXT */
/*-no- #define GLX_AUX0_EXT                       0x20E2 */
/*-no- #define GLX_AUX1_EXT                       0x20E3 */
/*-no- #define GLX_AUX2_EXT                       0x20E4 */
/*-no- #define GLX_AUX3_EXT                       0x20E5 */
/*-no- #define GLX_AUX4_EXT                       0x20E6 */
/*-no- #define GLX_AUX5_EXT                       0x20E7 */
/*-no- #define GLX_AUX6_EXT                       0x20E8 */
/*-no- #define GLX_AUX7_EXT                       0x20E9 */
/*-no- #define GLX_AUX8_EXT                       0x20EA */
/*-no- #define GLX_AUX9_EXT                       0x20EB */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_present_video */
/*-no- #define GLX_NUM_VIDEO_SLOTS_NV             0x20F0 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_video_out */
/*-no- #define GLX_VIDEO_OUT_COLOR_NV             0x20C3 */
/*-no- #define GLX_VIDEO_OUT_ALPHA_NV             0x20C4 */
/*-no- #define GLX_VIDEO_OUT_DEPTH_NV             0x20C5 */
/*-no- #define GLX_VIDEO_OUT_COLOR_AND_ALPHA_NV   0x20C6 */
/*-no- #define GLX_VIDEO_OUT_COLOR_AND_DEPTH_NV   0x20C7 */
/*-no- #define GLX_VIDEO_OUT_FRAME_NV             0x20C8 */
/*-no- #define GLX_VIDEO_OUT_FIELD_1_NV           0x20C9 */
/*-no- #define GLX_VIDEO_OUT_FIELD_2_NV           0x20CA */
/*-no- #define GLX_VIDEO_OUT_STACKED_FIELDS_1_2_NV 0x20CB */
/*-no- #define GLX_VIDEO_OUT_STACKED_FIELDS_2_1_NV 0x20CC */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_swap_group */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_video_capture */
/*-no- #define GLX_DEVICE_ID_NV                   0x20CD */
/*-no- #define GLX_UNIQUE_ID_NV                   0x20CE */
/*-no- #define GLX_NUM_VIDEO_CAPTURE_SLOTS_NV     0x20CF */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_EXT_swap_control */
/*-no- #define GLX_SWAP_INTERVAL_EXT              0x20F1 */
/*-no- #define GLX_MAX_SWAP_INTERVAL_EXT          0x20F2 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_copy_image */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_INTEL_swap_event */
/*-no- #define GLX_BUFFER_SWAP_COMPLETE_INTEL_MASK 0x04000000 */
/*-no- #define GLX_EXCHANGE_COMPLETE_INTEL        0x8180 */
/*-no- #define GLX_COPY_COMPLETE_INTEL            0x8181 */
/*-no- #define GLX_FLIP_COMPLETE_INTEL            0x8182 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_multisample_coverage */
/*-no- #define GLX_COVERAGE_SAMPLES_NV            100001 */
/*-no- #define GLX_COLOR_SAMPLES_NV               0x20B3 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_AMD_gpu_association */
/*-no- #define GLX_GPU_VENDOR_AMD                 0x1F00 */
/*-no- #define GLX_GPU_RENDERER_STRING_AMD        0x1F01 */
/*-no- #define GLX_GPU_OPENGL_VERSION_STRING_AMD  0x1F02 */
/*-no- #define GLX_GPU_FASTEST_TARGET_GPUS_AMD    0x21A2 */
/*-no- #define GLX_GPU_RAM_AMD                    0x21A3 */
/*-no- #define GLX_GPU_CLOCK_AMD                  0x21A4 */
/*-no- #define GLX_GPU_NUM_PIPES_AMD              0x21A5 */
/*-no- #define GLX_GPU_NUM_SIMD_AMD               0x21A6 */
/*-no- #define GLX_GPU_NUM_RB_AMD                 0x21A7 */
/*-no- #define GLX_GPU_NUM_SPI_AMD                0x21A8 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_EXT_create_context_es2_profile */
/*-no- #define GLX_CONTEXT_ES2_PROFILE_BIT_EXT    0x00000004 */
/*-no- #endif */
/*-no-  */
/*-no-  */
/*-no- ................................................................. */
/*-no-  */
/*-no- #ifndef GLX_ARB_get_proc_address */
/*-no- ...................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_video_source */
/*-no- ............................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_fbconfig */
/*-no- .............................. */
/*-no- ................................................. */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_pbuffer */
/*-no- ........................... */
/*-no- ................ */
/*-no- ............. */
/*-no- ......................................................................... */
/*-no- ....................................................................... */
/*-no- ................................................................ */
/*-no- .................................................... */
/*-no- ................................................................. */
/*-no- ................................................................. */
/*-no- .......................................................................... */
/*-no- ............. */
/*-no- ...................... */
/*-no- ............................................................. */
/*-no- ............................ */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_video_output */
/*-no- ...................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_video_capture */
/*-no- .................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLEXT_64_TYPES_DEFINED */
/*-no- ...................................................................... */
/*-no- #define GLEXT_64_TYPES_DEFINED */
/*-no- ............................................................... */
/*-no- ........................................................ */
/*-no- #if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L */
/*-no- #include <inttypes.h> */
/*-no- #elif defined(__sun__) || defined(__digital__) */
/*-no- #include <inttypes.h> */
/*-no- #if defined(__STDC__) */
/*-no- #if defined(__arch64__) || defined(_LP64) */
/*-no- ......................... */
/*-no- ................................... */
/*-no- #else */
/*-no- .............................. */
/*-no- ........................................ */
/*-no- #endif /-* __arch64__ *-/ */
/*-no- #endif /-* __STDC__ *-/ */
/*-no- #elif defined( __VMS ) || defined(__sgi) */
/*-no- #include <inttypes.h> */
/*-no- #elif defined(__SCO__) || defined(__USLC__) */
/*-no- #include <stdint.h> */
/*-no- #elif defined(__UNIXOS2__) || defined(__SOL64__) */
/*-no- ......................... */
/*-no- .............................. */
/*-no- ........................................ */
/*-no- #elif defined(_WIN32) && defined(__GNUC__) */
/*-no- #include <stdint.h> */
/*-no- #elif defined(_WIN32) */
/*-no- ........................ */
/*-no- ........................ */
/*-no- .................................. */
/*-no- #else */
/*-no- #include <inttypes.h>     /-* Fallback option *-/ */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_VERSION_1_3 */
/*-no- #define GLX_VERSION_1_3 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ................................................................................ */
/*-no- .......................................................................................................... */
/*-no- .............................................................................................. */
/*-no- ................................................................................. */
/*-no- ........................................................................................................ */
/*-no- ........................................................... */
/*-no- ........................................................................................................... */
/*-no- .............................................................. */
/*-no- .............................................................................................. */
/*-no- .............................................................. */
/*-no- .................................................................................................. */
/*-no- .............................................................................................................................. */
/*-no- ..................................................................................................... */
/*-no- .................................................... */
/*-no- ............................................. */
/*-no- ..................................................................................... */
/*-no- ...................................................................................... */
/*-no- ............................................................................................ */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ............................................................................................. */
/*-no- ....................................................................................................................... */
/*-no- ........................................................................................................... */
/*-no- .............................................................................................. */
/*-no- ..................................................................................................................... */
/*-no- ........................................................................ */
/*-no- ........................................................................................................................ */
/*-no- ........................................................................... */
/*-no- ........................................................................................................... */
/*-no- ........................................................................... */
/*-no- ............................................................................................................... */
/*-no- ........................................................................................................................................... */
/*-no- .................................................................................................................. */
/*-no- ................................................................. */
/*-no- .......................................................... */
/*-no- .................................................................................................. */
/*-no- ................................................................................................... */
/*-no- ......................................................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_VERSION_1_4 */
/*-no- #define GLX_VERSION_1_4 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ................................................................................ */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_ARB_get_proc_address */
/*-no- #define GLX_ARB_get_proc_address 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ...................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ................................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_ARB_multisample */
/*-no- #define GLX_ARB_multisample 1 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_ARB_fbconfig_float */
/*-no- #define GLX_ARB_fbconfig_float 1 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_ARB_framebuffer_sRGB */
/*-no- #define GLX_ARB_framebuffer_sRGB 1 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_ARB_create_context */
/*-no- #define GLX_ARB_create_context 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ............................................................................................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ............................................................................................................................................................ */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_ARB_create_context_profile */
/*-no- #define GLX_ARB_create_context_profile 1 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_ARB_create_context_robustness */
/*-no- #define GLX_ARB_create_context_robustness 1 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIS_multisample */
/*-no- #define GLX_SGIS_multisample 1 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_EXT_visual_info */
/*-no- #define GLX_EXT_visual_info 1 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGI_swap_control */
/*-no- #define GLX_SGI_swap_control 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ............................................. */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- .......................................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGI_video_sync */
/*-no- #define GLX_SGI_video_sync 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- .................................................... */
/*-no- ................................................................................. */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ................................................................. */
/*-no- .............................................................................................. */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGI_make_current_read */
/*-no- #define GLX_SGI_make_current_read 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ..................................................................................................... */
/*-no- ....................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- .................................................................................................................. */
/*-no- .................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_video_source */
/*-no- #define GLX_SGIX_video_source 1 */
/*-no- #ifdef _VL_H */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- .................................................................................................................................................... */
/*-no- ........................................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ................................................................................................................................................................. */
/*-no- ........................................................................................................ */
/*-no- #endif /-* _VL_H *-/ */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_EXT_visual_rating */
/*-no- #define GLX_EXT_visual_rating 1 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_EXT_import_context */
/*-no- #define GLX_EXT_import_context 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ................................................ */
/*-no- ................................................................................................ */
/*-no- .................................................................. */
/*-no- ............................................................................. */
/*-no- ................................................................. */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ............................................................. */
/*-no- ............................................................................................................. */
/*-no- ............................................................................... */
/*-no- .......................................................................................... */
/*-no- .............................................................................. */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_fbconfig */
/*-no- #define GLX_SGIX_fbconfig 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ...................................................................................................... */
/*-no- ............................................................................................................ */
/*-no- ........................................................................................................ */
/*-no- ............................................................................................................................................. */
/*-no- ......................................................................................... */
/*-no- ..................................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ................................................................................................................... */
/*-no- ......................................................................................................................... */
/*-no- ..................................................................................................................... */
/*-no- .......................................................................................................................................................... */
/*-no- ...................................................................................................... */
/*-no- .................................................................................................. */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_pbuffer */
/*-no- #define GLX_SGIX_pbuffer 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ................................................................................................................................................ */
/*-no- ......................................................................... */
/*-no- .......................................................................................................... */
/*-no- ........................................................................................ */
/*-no- .............................................................................................. */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ............................................................................................................................................................. */
/*-no- ...................................................................................... */
/*-no- ....................................................................................................................... */
/*-no- ..................................................................................................... */
/*-no- ........................................................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGI_cushion */
/*-no- #define GLX_SGI_cushion 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ....................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- .................................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_video_resize */
/*-no- #define GLX_SGIX_video_resize 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ................................................................................................. */
/*-no- ...................................................................................................... */
/*-no- ................................................................................................................... */
/*-no- ................................................................................................................. */
/*-no- ............................................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- .............................................................................................................. */
/*-no- ................................................................................................................... */
/*-no- ................................................................................................................................ */
/*-no- .............................................................................................................................. */
/*-no- ............................................................................................................ */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_dmbuffer */
/*-no- #define GLX_SGIX_dmbuffer 1 */
/*-no- #ifdef _DM_BUFFER_H_ */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- .................................................................................................................. */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ............................................................................................................................... */
/*-no- #endif /-* _DM_BUFFER_H_ *-/ */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_swap_group */
/*-no- #define GLX_SGIX_swap_group 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- .......................................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ....................................................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_swap_barrier */
/*-no- #define GLX_SGIX_swap_barrier 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ..................................................................................... */
/*-no- ............................................................................. */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- .................................................................................................. */
/*-no- .......................................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SUN_get_transparent_index */
/*-no- #define GLX_SUN_get_transparent_index 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ................................................................................................................. */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- .............................................................................................................................. */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_MESA_copy_sub_buffer */
/*-no- #define GLX_MESA_copy_sub_buffer 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ........................................................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ........................................................................................................................ */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_MESA_pixmap_colormap */
/*-no- #define GLX_MESA_pixmap_colormap 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- .......................................................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ....................................................................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_MESA_release_buffers */
/*-no- #define GLX_MESA_release_buffers 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ....................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- .................................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_MESA_set_3dfx_mode */
/*-no- #define GLX_MESA_set_3dfx_mode 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- .......................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ....................................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_visual_select_group */
/*-no- #define GLX_SGIX_visual_select_group 1 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_OML_swap_method */
/*-no- #define GLX_OML_swap_method 1 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_OML_sync_control */
/*-no- #define GLX_OML_sync_control 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ............................................................................................................... */
/*-no- ............................................................................................................ */
/*-no- ................................................................................................................................. */
/*-no- .................................................................................................................................................................... */
/*-no- ................................................................................................................................ */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ............................................................................................................................ */
/*-no- ......................................................................................................................... */
/*-no- .............................................................................................................................................. */
/*-no- ................................................................................................................................................................................. */
/*-no- ............................................................................................................................................. */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_float_buffer */
/*-no- #define GLX_NV_float_buffer 1 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_SGIX_hyperpipe */
/*-no- #define GLX_SGIX_hyperpipe 1 */
/*-no-  */
/*-no- ................ */
/*-no- .......................................................... */
/*-no- ...................... */
/*-no- .......................... */
/*-no-  */
/*-no- ................ */
/*-no- .......................................................... */
/*-no- .................... */
/*-no- ................ */
/*-no- ........................ */
/*-no- ...................... */
/*-no- ......................... */
/*-no-  */
/*-no- ................ */
/*-no- ....................................................... */
/*-no- .................................................... */
/*-no- ........................................................ */
/*-no- .............. */
/*-no-  */
/*-no- ................ */
/*-no- ....................................................... */
/*-no- .............................................. */
/*-no- .................... */
/*-no-  */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- .......................................................................................... */
/*-no- .................................................................................................................... */
/*-no- .................................................................................................. */
/*-no- .................................................................. */
/*-no- ......................................................... */
/*-no- ......................................................................................................................................... */
/*-no- ........................................................................................................ */
/*-no- ................................................................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ....................................................................................................... */
/*-no- ................................................................................................................................. */
/*-no- ............................................................................................................... */
/*-no- ............................................................................... */
/*-no- ...................................................................... */
/*-no- ...................................................................................................................................................... */
/*-no- ..................................................................................................................... */
/*-no- ................................................................................................................................ */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_MESA_agp_offset */
/*-no- #define GLX_MESA_agp_offset 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- .............................................................. */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ........................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_EXT_fbconfig_packed_float */
/*-no- #define GLX_EXT_fbconfig_packed_float 1 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_EXT_framebuffer_sRGB */
/*-no- #define GLX_EXT_framebuffer_sRGB 1 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_EXT_texture_from_pixmap */
/*-no- #define GLX_EXT_texture_from_pixmap 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ........................................................................................................ */
/*-no- ................................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ..................................................................................................................... */
/*-no- ................................................................................................ */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_present_video */
/*-no- #define GLX_NV_present_video 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ............................................................................................ */
/*-no- ........................................................................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ......................................................................................................... */
/*-no- ........................................................................................................................................ */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_video_output */
/*-no- #define GLX_NV_video_output 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ............................................................................................................... */
/*-no- ............................................................................................ */
/*-no- ............................................................................................................... */
/*-no- .................................................................. */
/*-no- ........................................................................................................................................ */
/*-no- .................................................................................................................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ............................................................................................................................ */
/*-no- ......................................................................................................... */
/*-no- ............................................................................................................................ */
/*-no- ............................................................................... */
/*-no- ..................................................................................................................................................... */
/*-no- ................................................................................................................................................................................. */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_swap_group */
/*-no- #define GLX_NV_swap_group 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- .................................................................................. */
/*-no- .............................................................................. */
/*-no- ..................................................................................................... */
/*-no- ....................................................................................................... */
/*-no- ........................................................................... */
/*-no- ............................................................ */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- ............................................................................................... */
/*-no- ........................................................................................... */
/*-no- .................................................................................................................. */
/*-no- .................................................................................................................... */
/*-no- ........................................................................................ */
/*-no- ......................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_video_capture */
/*-no- #define GLX_NV_video_capture 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ....................................................................................................................... */
/*-no- .............................................................................................................. */
/*-no- ....................................................................................... */
/*-no- .................................................................................................................. */
/*-no- .......................................................................................... */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- .................................................................................................................................... */
/*-no- ........................................................................................................................... */
/*-no- .................................................................................................... */
/*-no- ............................................................................................................................... */
/*-no- ....................................................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_EXT_swap_control */
/*-no- #define GLX_EXT_swap_control 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ................................................................................. */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- .............................................................................................. */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_copy_image */
/*-no- #define GLX_NV_copy_image 1 */
/*-no- #ifdef GLX_GLXEXT_PROTOTYPES */
/*-no- ................................................................................................................................................................................................................................................................................................................. */
/*-no- #endif /-* GLX_GLXEXT_PROTOTYPES *-/ */
/*-no- .............................................................................................................................................................................................................................................................................................................................. */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_INTEL_swap_event */
/*-no- #define GLX_INTEL_swap_event 1 */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef GLX_NV_multisample_coverage */
/*-no- #define GLX_NV_multisample_coverage 1 */
/*-no- #endif */
/*-no-  */
/*-no-  */
/*-no- #ifdef __cplusplus */
/*-no- . */
/*-no- #endif */
/*-no-  */
/*-no- #endif */

#endif
/* CONSBENCH file end */
