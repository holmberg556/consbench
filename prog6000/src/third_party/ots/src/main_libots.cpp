/* CONSBENCH file begin */
#ifndef MAIN_LIBOTS_CPP
#define MAIN_LIBOTS_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_ots_src_main_libots_cpp, "third_party/ots/src/main_libots.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_ots_src_main_libots_cpp, "third_party/ots/src/main_libots.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_ots_src_cff_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_cff_type2_charstring_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_cmap_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_cvt_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_fpgm_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_gasp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_gdef_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_glyf_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_hdmx_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_head_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_hhea_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_hmtx_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_kern_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_layout_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_loca_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_ltsh_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_maxp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_name_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_os2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_ots_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_post_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_prep_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_vdmx_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_ots_src_vorg_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_ots_src_main_libots_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_ots_src_cff_cpp(it);
  PUBLIC_third_party_ots_src_cff_type2_charstring_cpp(it);
  PUBLIC_third_party_ots_src_cmap_cpp(it);
  PUBLIC_third_party_ots_src_cvt_cpp(it);
  PUBLIC_third_party_ots_src_fpgm_cpp(it);
  PUBLIC_third_party_ots_src_gasp_cpp(it);
  PUBLIC_third_party_ots_src_gdef_cpp(it);
  PUBLIC_third_party_ots_src_glyf_cpp(it);
  PUBLIC_third_party_ots_src_hdmx_cpp(it);
  PUBLIC_third_party_ots_src_head_cpp(it);
  PUBLIC_third_party_ots_src_hhea_cpp(it);
  PUBLIC_third_party_ots_src_hmtx_cpp(it);
  PUBLIC_third_party_ots_src_kern_cpp(it);
  PUBLIC_third_party_ots_src_layout_cpp(it);
  PUBLIC_third_party_ots_src_loca_cpp(it);
  PUBLIC_third_party_ots_src_ltsh_cpp(it);
  PUBLIC_third_party_ots_src_maxp_cpp(it);
  PUBLIC_third_party_ots_src_name_cpp(it);
  PUBLIC_third_party_ots_src_os2_cpp(it);
  PUBLIC_third_party_ots_src_ots_cpp(it);
  PUBLIC_third_party_ots_src_post_cpp(it);
  PUBLIC_third_party_ots_src_prep_cpp(it);
  PUBLIC_third_party_ots_src_vdmx_cpp(it);
  PUBLIC_third_party_ots_src_vorg_cpp(it);
}

#endif

