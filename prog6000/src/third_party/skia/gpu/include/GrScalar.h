/* CONSBENCH file begin */
#ifndef GRSCALAR_H_3894
#define GRSCALAR_H_3894

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_skia_gpu_include_GrScalar_h, "third_party/skia/gpu/include/GrScalar.h");

/* CONSBENCH includes begin */
#include "GrTypes.h"
#include <float.h>
#include <math.h>
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_skia_gpu_include_GrScalar_h, "third_party/skia/gpu/include/GrScalar.h");

/*-no- ... */
/*-no- .............................. */
/*-no-  */
/*-no- ................................................................... */
/*-no- .................................................................... */
/*-no- ........................................... */
/*-no-  */
/*-no- ................................................... */
/*-no-  */
/*-no- ....................................................................... */
/*-no- ..................................................................... */
/*-no- ............................................................................ */
/*-no- ....................................................................... */
/*-no- .................................. */
/*-no- .... */
/*-no-  */
/*-no-  */
/*-no- #ifndef GrScalar_DEFINED */
/*-no- #define GrScalar_DEFINED */
/*-no-  */
/*-no- #include "GrTypes.h" */
/*-no-  */
/*-no- #include <float.h> */
/*-no- #include <math.h> */
/*-no-  */
/*-no- #define GR_Int32Max            (0x7fffffff) */
/*-no- #define GR_Int32Min            (0x80000000) */
/*-no-  */
/*-no- .... */
/*-no- .................................. */
/*-no- .... */
/*-no- #if GR_DEBUG */
/*-no- ........................................ */
/*-no- ................................................................................ */
/*-no- ....................... */
/*-no- ..... */
/*-no- #else */
/*-no-     #define GrIntToFixed(i) (GrFixed)((i) << 16) */
/*-no- #endif */
/*-no-  */
/*-no- #define GR_Fixed1              (1 << 16) */
/*-no- #define GR_FixedHalf           (1 << 15) */
/*-no- #define GR_FixedMax            GR_Int32Max */
/*-no- #define GR_FixedMin            GR_Int32Min */
/*-no-  */
/*-no- #define GrFixedFloorToFixed(x)  ((x) & ~0xFFFF) */
/*-no- #define GrFixedFloorToInt(x)    ((x) >> 16) */
/*-no-  */
/*-no- .... */
/*-no- ......................................... */
/*-no- .... */
/*-no- #define GrFixedToFloat(x)      ((x) * 0.0000152587890625f) */
/*-no-  */
/*-no- .... */
/*-no- ......................................... */
/*-no- .... */
/*-no- #define GrFloatToFixed(x)      ((GrFixed)((x) * GR_Fixed1)) */
/*-no-  */
/*-no- ...................................... */
/*-no- ....................................... */
/*-no- ............................................. */
/*-no- . */
/*-no-  */
/*-no- ............................................................................... */
/*-no-  */
/*-no- #if GR_SCALAR_IS_FIXED */
/*-no- ............................................. */
/*-no-     #define GrIntToScalar(x)        GrIntToFixed(x) */
/*-no-     #define GrFixedToScalar(x)      (x) */
/*-no-     #define GrScalarToFloat(x)      GrFixedToFloat(x) */
/*-no-     #define GrFloatToScalar(x)      GrFloatToFixed(x) */
/*-no-     #define GrScalarHalf(x)         ((x) >> 1) */
/*-no-     #define GrScalarAve(x,y)        (((x)+(y)) >> 1) */
/*-no-     #define GrScalarAbs(x)          GrFixedAbs(x) */
/*-no-     #define GR_Scalar1              GR_Fixed1 */
/*-no-     #define GR_ScalarHalf           GR_FixedHalf */
/*-no-     #define GR_ScalarMax            GR_FixedMax */
/*-no-     #define GR_ScalarMin            GR_FixedMin */
/*-no- #elif GR_SCALAR_IS_FLOAT */
/*-no- ............................................. */
/*-no-     #define GrIntToScalar(x)        ((GrScalar)x) */
/*-no-     #define GrFixedToScalar(x)      GrFixedToFloat(x) */
/*-no-     #define GrScalarToFloat(x)      (x) */
/*-no-     #define GrFloatToScalar(x)      (x) */
/*-no-     #define GrScalarHalf(x)         ((x) * 0.5f) */
/*-no-     #define GrScalarAbs(x)          fabsf(x) */
/*-no-     #define GrScalarAve(x,y)        (((x) + (y)) * 0.5f) */
/*-no-     #define GR_Scalar1              1.f     */
/*-no-     #define GR_ScalarHalf           0.5f */
/*-no-     #define GR_ScalarMax            (FLT_MAX) */
/*-no-     #define GR_ScalarMin            (-FLT_MAX) */
/*-no-  */
/*-no- ....................................................... */
/*-no- .................................... */
/*-no- ..... */
/*-no- ...................................................... */
/*-no- ................................... */
/*-no- ..... */
/*-no- #else */
/*-no-     #error "Scalar type not defined" */
/*-no- #endif */
/*-no-  */
/*-no- .... */
/*-no- ................................ */
/*-no- .... */
/*-no- ...................................................... */
/*-no- #if GR_SCALAR_IS_FLOAT */
/*-no- ................. */
/*-no- #else */
/*-no- ................................. */
/*-no- ...................................... */
/*-no- #endif */
/*-no- . */
/*-no-  */
/*-no- #endif */
/*-no-  */

#endif
/* CONSBENCH file end */
