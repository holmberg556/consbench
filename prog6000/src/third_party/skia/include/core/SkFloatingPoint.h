/* CONSBENCH file begin */
#ifndef SKFLOATINGPOINT_H_3985
#define SKFLOATINGPOINT_H_3985

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_skia_include_core_SkFloatingPoint_h, "third_party/skia/include/core/SkFloatingPoint.h");

/* CONSBENCH includes begin */
#include "SkTypes.h"
#include <math.h>
#include <float.h>
#include "SkFloatBits.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_skia_include_core_SkFloatingPoint_h, "third_party/skia/include/core/SkFloatingPoint.h");

/*-no- ... */
/*-no- ..................................................... */
/*-no- .. */
/*-no- .................................................................. */
/*-no- ................................................................... */
/*-no- .......................................... */
/*-no- .. */
/*-no- .................................................. */
/*-no- .. */
/*-no- ...................................................................... */
/*-no- .................................................................... */
/*-no- ........................................................................... */
/*-no- ...................................................................... */
/*-no- ................................. */
/*-no- .... */
/*-no-  */
/*-no- #ifndef SkFloatingPoint_DEFINED */
/*-no- #define SkFloatingPoint_DEFINED */
/*-no-  */
/*-no- #include "SkTypes.h" */
/*-no-  */
/*-no- #ifdef SK_CAN_USE_FLOAT */
/*-no-  */
/*-no- #include <math.h> */
/*-no- #include <float.h> */
/*-no- #include "SkFloatBits.h" */
/*-no-  */
/*-no- ................................................................ */
/*-no- ......................................................... */
/*-no- ............................................................ */
/*-no- ............................................................. */
/*-no- . */
/*-no-  */
/*-no- ......................................................... */
/*-no- .................................... */
/*-no- .................................... */
/*-no- ..................................................................... */
/*-no- . */
/*-no-  */
/*-no- #ifdef SK_BUILD_FOR_WINCE */
/*-no-     #define sk_float_sqrt(x)        (float)::sqrt(x) */
/*-no-     #define sk_float_sin(x)         (float)::sin(x) */
/*-no-     #define sk_float_cos(x)         (float)::cos(x) */
/*-no-     #define sk_float_tan(x)         (float)::tan(x) */
/*-no-     #define sk_float_acos(x)        (float)::acos(x) */
/*-no-     #define sk_float_asin(x)        (float)::asin(x) */
/*-no-     #define sk_float_atan2(y,x)     (float)::atan2(y,x) */
/*-no-     #define sk_float_abs(x)         (float)::fabs(x) */
/*-no-     #define sk_float_mod(x,y)       (float)::fmod(x,y) */
/*-no-     #define sk_float_exp(x)         (float)::exp(x) */
/*-no-     #define sk_float_log(x)         (float)::log(x) */
/*-no-     #define sk_float_floor(x)       (float)::floor(x) */
/*-no-     #define sk_float_ceil(x)        (float)::ceil(x) */
/*-no- #else */
/*-no-     #define sk_float_sqrt(x)        sqrtf(x) */
/*-no-     #define sk_float_sin(x)         sinf(x) */
/*-no-     #define sk_float_cos(x)         cosf(x) */
/*-no-     #define sk_float_tan(x)         tanf(x) */
/*-no-     #define sk_float_floor(x)       floorf(x) */
/*-no-     #define sk_float_ceil(x)        ceilf(x) */
/*-no- #ifdef SK_BUILD_FOR_MAC */
/*-no-     #define sk_float_acos(x)        static_cast<float>(acos(x)) */
/*-no-     #define sk_float_asin(x)        static_cast<float>(asin(x)) */
/*-no- #else */
/*-no-     #define sk_float_acos(x)        acosf(x) */
/*-no-     #define sk_float_asin(x)        asinf(x) */
/*-no- #endif */
/*-no-     #define sk_float_atan2(y,x) atan2f(y,x) */
/*-no-     #define sk_float_abs(x)         fabsf(x) */
/*-no-     #define sk_float_mod(x,y)       fmodf(x,y) */
/*-no-     #define sk_float_exp(x)         expf(x) */
/*-no-     #define sk_float_log(x)         logf(x) */
/*-no-     #define sk_float_isNaN(x)       _isnan(x) */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef SK_USE_FLOATBITS */
/*-no-     #define sk_float_floor2int(x)   SkFloatToIntFloor(x) */
/*-no-     #define sk_float_round2int(x)   SkFloatToIntRound(x) */
/*-no-     #define sk_float_ceil2int(x)    SkFloatToIntCeil(x) */
/*-no- #else */
/*-no-     #define sk_float_floor2int(x)   (int)sk_float_floor(x) */
/*-no-     #define sk_float_round2int(x)   (int)sk_float_floor((x) + 0.5f) */
/*-no-     #define sk_float_ceil2int(x)    (int)sk_float_ceil(x) */
/*-no- #endif */
/*-no-  */
/*-no- #endif */
/*-no- #endif */

#endif
/* CONSBENCH file end */
