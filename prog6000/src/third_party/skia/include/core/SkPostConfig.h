/* CONSBENCH file begin */
#ifndef SKPOSTCONFIG_H_4007
#define SKPOSTCONFIG_H_4007

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_skia_include_core_SkPostConfig_h, "third_party/skia/include/core/SkPostConfig.h");

/* CONSBENCH includes begin */
        #include <stdio.h>
#include <string.h>
#include <stdlib.h>
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_skia_include_core_SkPostConfig_h, "third_party/skia/include/core/SkPostConfig.h");

/*-no- ... */
/*-no- ..................................................... */
/*-no- .. */
/*-no- .................................................................. */
/*-no- ................................................................... */
/*-no- .......................................... */
/*-no- .. */
/*-no- .................................................. */
/*-no- .. */
/*-no- ...................................................................... */
/*-no- .................................................................... */
/*-no- ........................................................................... */
/*-no- ...................................................................... */
/*-no- ................................. */
/*-no- .... */
/*-no-  */
/*-no- #ifndef SkPostConfig_DEFINED */
/*-no- #define SkPostConfig_DEFINED */
/*-no-  */
/*-no- #if defined(SK_BUILD_FOR_WIN32) || defined(SK_BUILD_FOR_WINCE) */
/*-no-     #define SK_BUILD_FOR_WIN */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(SK_DEBUG) && defined(SK_RELEASE) */
/*-no-     #error "cannot define both SK_DEBUG and SK_RELEASE" */
/*-no- #elif !defined(SK_DEBUG) && !defined(SK_RELEASE) */
/*-no-     #error "must define either SK_DEBUG or SK_RELEASE" */
/*-no- #endif */
/*-no-  */
/*-no- #if defined SK_SUPPORT_UNITTEST && !defined(SK_DEBUG) */
/*-no-     #error "can't have unittests without debug" */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(SK_SCALAR_IS_FIXED) && defined(SK_SCALAR_IS_FLOAT) */
/*-no-     #error "cannot define both SK_SCALAR_IS_FIXED and SK_SCALAR_IS_FLOAT" */
/*-no- #elif !defined(SK_SCALAR_IS_FIXED) && !defined(SK_SCALAR_IS_FLOAT) */
/*-no-     #ifdef SK_CAN_USE_FLOAT */
/*-no-         #define SK_SCALAR_IS_FLOAT */
/*-no-     #else */
/*-no-         #define SK_SCALAR_IS_FIXED */
/*-no-     #endif */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(SK_SCALAR_IS_FLOAT) && !defined(SK_CAN_USE_FLOAT) */
/*-no-     #define SK_CAN_USE_FLOAT */
/*-no- ........................................................................... */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(SK_CPU_LENDIAN) && defined(SK_CPU_BENDIAN) */
/*-no-     #error "cannot define both SK_CPU_LENDIAN and SK_CPU_BENDIAN" */
/*-no- #elif !defined(SK_CPU_LENDIAN) && !defined(SK_CPU_BENDIAN) */
/*-no-     #error "must define either SK_CPU_LENDIAN or SK_CPU_BENDIAN" */
/*-no- #endif */
/*-no-  */
/*-no- ............................................................ */
/*-no- #ifdef SK_A32_SHIFT */
/*-no-     #if !defined(SK_R32_SHIFT) || !defined(SK_G32_SHIFT) || !defined(SK_B32_SHIFT) */
/*-no-         #error "all or none of the 32bit SHIFT amounts must be defined" */
/*-no-     #endif */
/*-no- #else */
/*-no-     #if defined(SK_R32_SHIFT) || defined(SK_G32_SHIFT) || defined(SK_B32_SHIFT) */
/*-no-         #error "all or none of the 32bit SHIFT amounts must be defined" */
/*-no-     #endif */
/*-no- #endif */
/*-no-  */
/*-no- ............................................................................... */
/*-no-  */
/*-no- #ifndef SkNEW */
/*-no-     #define SkNEW(type_name)                new type_name */
/*-no-     #define SkNEW_ARGS(type_name, args)     new type_name args */
/*-no-     #define SkNEW_ARRAY(type_name, count)   new type_name[count] */
/*-no-     #define SkDELETE(obj)                   delete obj */
/*-no-     #define SkDELETE_ARRAY(array)           delete[] array */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef SK_CRASH */
/*-no- #if 1   // set to 0 for infinite loop, which can help connecting gdb */
/*-no-     #define SK_CRASH() *(int *)(uintptr_t)0xbbadbeef = 0 */
/*-no- #else */
/*-no-     #define SK_CRASH()  do {} while (true) */
/*-no- #endif */
/*-no- #endif */
/*-no-  */
/*-no- ............................................................................... */
/*-no-  */
/*-no- #if defined(SK_SOFTWARE_FLOAT) && defined(SK_SCALAR_IS_FLOAT) */
/*-no- .............................................................................. */
/*-no-     #ifndef SK_SCALAR_SLOW_COMPARES */
/*-no-         #define SK_SCALAR_SLOW_COMPARES */
/*-no-     #endif */
/*-no-     #ifndef SK_USE_FLOATBITS */
/*-no-         #define SK_USE_FLOATBITS */
/*-no-     #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef SK_BUILD_FOR_WIN */
/*-no- ...................................................... */
/*-no-     #ifndef WIN32_LEAN_AND_MEAN */
/*-no-         #define WIN32_LEAN_AND_MEAN */
/*-no-         #define WIN32_IS_MEAN_WAS_LOCALLY_DEFINED */
/*-no-     #endif */
/*-no-  */
/*-no-     #include <windows.h> */
/*-no-  */
/*-no-     #ifdef WIN32_IS_MEAN_WAS_LOCALLY_DEFINED */
/*-no-         #undef WIN32_LEAN_AND_MEAN */
/*-no-     #endif */
/*-no-  */
/*-no-     #ifndef SK_DEBUGBREAK */
/*-no-         #define SK_DEBUGBREAK(cond)     do { if (!(cond)) __debugbreak(); } while (false) */
/*-no-     #endif */
/*-no-  */
/*-no-     #ifndef SK_A32_SHIFT */
/*-no-         #define SK_A32_SHIFT 24 */
/*-no-         #define SK_R32_SHIFT 16 */
/*-no-         #define SK_G32_SHIFT 8 */
/*-no-         #define SK_B32_SHIFT 0 */
/*-no-     #endif */
/*-no-  */
/*-no- #elif defined(SK_BUILD_FOR_MAC) */
/*-no-     #ifndef SK_DEBUGBREAK */
/*-no-         #define SK_DEBUGBREAK(cond)     do { if (!(cond)) SK_CRASH(); } while (false) */
/*-no-     #endif */
/*-no- #else */
/*-no-     #ifdef SK_DEBUG */
/*-no-         #include <stdio.h> */
/*-no-         #ifndef SK_DEBUGBREAK */
/*-no-             #define SK_DEBUGBREAK(cond) do { if (cond) break; \ */
/*-no- .............................................................. */
/*-no- ....................................................................... */
/*-no-         #endif */
/*-no-     #endif */
/*-no- #endif */
/*-no-  */
/*-no- ................. */
/*-no-  */
/*-no- #if 0 */
/*-no- #if !defined(strlen) && defined(SK_DEBUG) */
/*-no- ......................................... */
/*-no-     #define strlen(s)   sk_strlen(s) */
/*-no- #endif */
/*-no- #ifndef sk_strcpy */
/*-no-     #define sk_strcpy(dst, src)     strcpy(dst, src) */
/*-no- #endif */
/*-no- #ifndef sk_strchr */
/*-no-     #define sk_strchr(s, c)         strchr(s, c) */
/*-no- #endif */
/*-no- #ifndef sk_strrchr */
/*-no-     #define sk_strrchr(s, c)        strrchr(s, c) */
/*-no- #endif */
/*-no- #ifndef sk_strcmp */
/*-no-     #define sk_strcmp(s, t)         strcmp(s, t) */
/*-no- #endif */
/*-no- #ifndef sk_strncmp */
/*-no-     #define sk_strncmp(s, t, n)     strncmp(s, t, n) */
/*-no- #endif */
/*-no- #ifndef sk_memcpy */
/*-no-     #define sk_memcpy(dst, src, n)  memcpy(dst, src, n) */
/*-no- #endif */
/*-no- #ifndef memmove */
/*-no-     #define memmove(dst, src, n)    memmove(dst, src, n) */
/*-no- #endif */
/*-no- #ifndef sk_memset */
/*-no-     #define sk_memset(dst, val, n)  memset(dst, val, n) */
/*-no- #endif */
/*-no- #ifndef sk_memcmp */
/*-no-     #define sk_memcmp(s, t, n)      memcmp(s, t, n) */
/*-no- #endif */
/*-no-  */
/*-no- #define sk_strequal(s, t)           (!sk_strcmp(s, t)) */
/*-no- #define sk_strnequal(s, t, n)       (!sk_strncmp(s, t, n)) */
/*-no- #endif */
/*-no-  */
/*-no- ...................................................................... */
/*-no-  */
/*-no- #if defined(SK_BUILD_FOR_WIN32) || defined(SK_BUILD_FOR_MAC) */
/*-no-     #ifndef SkLONGLONG */
/*-no-         #ifdef SK_BUILD_FOR_WIN32 */
/*-no-             #define SkLONGLONG  __int64 */
/*-no-         #else */
/*-no-             #define SkLONGLONG  long long */
/*-no-         #endif */
/*-no-     #endif */
/*-no- #endif */
/*-no-  */
/*-no- .............................................................................................. */
/*-no- #ifndef SK_BUILD_FOR_WINCE */
/*-no- #include <string.h> */
/*-no- #include <stdlib.h> */
/*-no- #else */
/*-no- #define _CMNINTRIN_DECLARE_ONLY */
/*-no- #include "cmnintrin.h" */
/*-no- #endif */
/*-no-  */
/*-no- #if defined SK_DEBUG && defined SK_BUILD_FOR_WIN32 */
/*-no- ........................... */
/*-no- #ifdef free */
/*-no- #undef free */
/*-no- #endif */
/*-no- #include <crtdbg.h> */
/*-no- #undef free */
/*-no-  */
/*-no- #ifdef SK_DEBUGx */
/*-no- #if defined(SK_SIMULATE_FAILED_MALLOC) && defined(__cplusplus) */
/*-no- ........................ */
/*-no- .................. */
/*-no- ...................... */
/*-no- ................................ */
/*-no- .................. */
/*-no- ............... */
/*-no- .......... */
/*-no- .......................... */
/*-no- .................. */
/*-no- ...................... */
/*-no- ................................ */
/*-no- .................. */
/*-no- ............... */
/*-no- .......... */
/*-no- ......................... */
/*-no- ........................ */
/*-no- .................................. */
/*-no- .......... */
/*-no- ......................... */
/*-no- ....................... */
/*-no- .......... */
/*-no- ....................................... */
/*-no-     #define DEBUG_CLIENTBLOCK   new( _CLIENT_BLOCK, __FILE__, __LINE__, 0) */
/*-no- #else */
/*-no-     #define DEBUG_CLIENTBLOCK   new( _CLIENT_BLOCK, __FILE__, __LINE__) */
/*-no- #endif */
/*-no-     #define new DEBUG_CLIENTBLOCK */
/*-no- #else */
/*-no- #define DEBUG_CLIENTBLOCK */
/*-no- #endif // _DEBUG */
/*-no-  */
/*-no- #endif */
/*-no-  */
/*-no- #endif */
/*-no-  */

#endif
/* CONSBENCH file end */
