/* CONSBENCH file begin */
#ifndef SKPRECONFIG_H_4008
#define SKPRECONFIG_H_4008

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_skia_include_core_SkPreConfig_h, "third_party/skia/include/core/SkPreConfig.h");

/* CONSBENCH includes begin */
    #include "sk_stdint.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_skia_include_core_SkPreConfig_h, "third_party/skia/include/core/SkPreConfig.h");

/*-no- ... */
/*-no- ..................................................... */
/*-no- .. */
/*-no- .................................................................. */
/*-no- ................................................................... */
/*-no- .......................................... */
/*-no- .. */
/*-no- .................................................. */
/*-no- .. */
/*-no- ...................................................................... */
/*-no- .................................................................... */
/*-no- ........................................................................... */
/*-no- ...................................................................... */
/*-no- ................................. */
/*-no- .... */
/*-no-  */
/*-no- #ifndef SkPreConfig_DEFINED */
/*-no- #define SkPreConfig_DEFINED */
/*-no-  */
/*-no- #ifdef WEBKIT_VERSION_MIN_REQUIRED */
/*-no-     #include "config.h" */
/*-no- #endif */
/*-no-  */
/*-no- ...................................................................... */
/*-no-  */
/*-no- #if !defined(SK_BUILD_FOR_ANDROID_NDK) && !defined(SK_BUILD_FOR_IOS) && !defined(SK_BUILD_FOR_PALM) && !defined(SK_BUILD_FOR_WINCE) && !defined(SK_BUILD_FOR_WIN32) && !defined(SK_BUILD_FOR_SYMBIAN) && !defined(SK_BUILD_FOR_UNIX) && !defined(SK_BUILD_FOR_MAC) && !defined(SK_BUILD_FOR_SDL) && !defined(SK_BUILD_FOR_BREW) */
/*-no-  */
/*-no-     #ifdef __APPLE__ */
/*-no-         #include "TargetConditionals.h" */
/*-no-     #endif */
/*-no-  */
/*-no-     #if defined(PALMOS_SDK_VERSION) */
/*-no-         #define SK_BUILD_FOR_PALM */
/*-no-     #elif defined(UNDER_CE) */
/*-no-         #define SK_BUILD_FOR_WINCE */
/*-no-     #elif defined(WIN32) */
/*-no-         #define SK_BUILD_FOR_WIN32 */
/*-no-     #elif defined(__SYMBIAN32__) */
/*-no-         #define SK_BUILD_FOR_WIN32 */
/*-no-     #elif defined(linux) */
/*-no-         #define SK_BUILD_FOR_UNIX */
/*-no-     #elif TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR */
/*-no-         #define SK_BUILD_FOR_IOS */
/*-no-     #elif defined(ANDROID_NDK) */
/*-no-         #define SK_BUILD_FOR_ANDROID_NDK */
/*-no-     #elif defined(ANROID) */
/*-no-         #define SK_BUILD_FOR_ANDROID */
/*-no-     #else */
/*-no-         #define SK_BUILD_FOR_MAC */
/*-no-     #endif */
/*-no-  */
/*-no- #endif */
/*-no-  */
/*-no- ...................................................................... */
/*-no-  */
/*-no- #if !defined(SK_DEBUG) && !defined(SK_RELEASE) */
/*-no-     #ifdef NDEBUG */
/*-no-         #define SK_RELEASE */
/*-no-     #else */
/*-no-         #define SK_DEBUG */
/*-no-     #endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef SK_BUILD_FOR_WIN32 */
/*-no- 	#define SK_RESTRICT */
/*-no-     #include "sk_stdint.h" */
/*-no- #endif */
/*-no-  */
/*-no- ...................................................................... */
/*-no-  */
/*-no- #if !defined(SK_RESTRICT) */
/*-no-     #define SK_RESTRICT __restrict__ */
/*-no- #endif */
/*-no-  */
/*-no- ...................................................................... */
/*-no-  */
/*-no- #if !defined(SK_SCALAR_IS_FLOAT) && !defined(SK_SCALAR_IS_FIXED) */
/*-no-     #define SK_SCALAR_IS_FLOAT */
/*-no-     #define SK_CAN_USE_FLOAT */
/*-no- #endif */
/*-no-  */
/*-no- ...................................................................... */
/*-no-  */
/*-no- #if !defined(SK_CPU_BENDIAN) && !defined(SK_CPU_LENDIAN) */
/*-no-     #if defined (__ppc__) || defined(__ppc64__) */
/*-no-         #define SK_CPU_BENDIAN */
/*-no-     #else */
/*-no-         #define SK_CPU_LENDIAN */
/*-no-     #endif */
/*-no- #endif */
/*-no-  */
/*-no- ...................................................................... */
/*-no-  */
/*-no- #if (defined(__arm__) && !defined(__thumb__)) || defined(SK_BUILD_FOR_WINCE) || (defined(SK_BUILD_FOR_SYMBIAN) && !defined(__MARM_THUMB__)) */
/*-no- ............................................................................................ */
/*-no-     #define SK_CPU_HAS_CONDITIONAL_INSTR */
/*-no- #endif */
/*-no-  */
/*-no- #endif */
/*-no-  */

#endif
/* CONSBENCH file end */
