/* CONSBENCH file begin */
#ifndef SKSCALAR_H_4017
#define SKSCALAR_H_4017

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_skia_include_core_SkScalar_h, "third_party/skia/include/core/SkScalar.h");

/* CONSBENCH includes begin */
#include "SkFixed.h"
    #include "SkFloatingPoint.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_skia_include_core_SkScalar_h, "third_party/skia/include/core/SkScalar.h");

/*-no- ... */
/*-no- ..................................................... */
/*-no- .. */
/*-no- .................................................................. */
/*-no- ................................................................... */
/*-no- .......................................... */
/*-no- .. */
/*-no- .................................................. */
/*-no- .. */
/*-no- ...................................................................... */
/*-no- .................................................................... */
/*-no- ........................................................................... */
/*-no- ...................................................................... */
/*-no- ................................. */
/*-no- .... */
/*-no-  */
/*-no- #ifndef SkScalar_DEFINED */
/*-no- #define SkScalar_DEFINED */
/*-no-  */
/*-no- #include "SkFixed.h" */
/*-no-  */
/*-no- ..................... */
/*-no-  */
/*-no- .................................................................................... */
/*-no- ................................................................................... */
/*-no- ....................................................................................... */
/*-no- ................................................................................................ */
/*-no- ................. */
/*-no- ... */
/*-no-  */
/*-no- #ifdef SK_SCALAR_IS_FLOAT */
/*-no-     #include "SkFloatingPoint.h" */
/*-no-  */
/*-no- ................................................................................. */
/*-no- ............................................................................. */
/*-no- ....................................... */
/*-no- ....... */
/*-no- ............................. */
/*-no- .......................................... */
/*-no- ........................................ */
/*-no-  */
/*-no- ................................................................... */
/*-no- ....... */
/*-no-     #define SK_Scalar1              (1.0f) */
/*-no- ................................................................... */
/*-no- ....... */
/*-no-     #define SK_ScalarHalf           (0.5f) */
/*-no- ................................................................... */
/*-no- ....... */
/*-no-     #define SK_ScalarInfinity           (*(const float*)&gIEEEInfinity) */
/*-no- ..................................................................................... */
/*-no- ....... */
/*-no-     #define SK_ScalarMax            (3.4028235e+38f) */
/*-no- ...................................................................................... */
/*-no- ....... */
/*-no-     #define SK_ScalarMin            (1.1754944e-38f) */
/*-no- .................................................................... */
/*-no- ....... */
/*-no-     #define SK_ScalarNaN      (*(const float*)(const void*)&gIEEENotANumber) */
/*-no- .................................................................. */
/*-no- ....... */
/*-no- ................................................................ */
/*-no- .......................................................... */
/*-no- .................................................. */
/*-no- ........................................................................... */
/*-no- ....................................... */
/*-no- ................................ */
/*-no- ..... */
/*-no- ..................................................................... */
/*-no- ....... */
/*-no-     #define SkIntToScalar(n)        ((float)(n)) */
/*-no- ....................................................................... */
/*-no- ....... */
/*-no-     #define SkFixedToScalar(x)      SkFixedToFloat(x) */
/*-no- ....................................................................... */
/*-no- ....... */
/*-no-     #define SkScalarToFixed(x)      SkFloatToFixed(x) */
/*-no-  */
/*-no-     #define SkScalarToFloat(n)      (n) */
/*-no-     #define SkFloatToScalar(n)      (n) */
/*-no-  */
/*-no-     #define SkScalarToDouble(n)      (double)(n) */
/*-no-     #define SkDoubleToScalar(n)      (float)(n) */
/*-no-  */
/*-no- ............................................................................... */
/*-no- ....... */
/*-no-     #define SkScalarFraction(x)     sk_float_mod(x, 1.0f) */
/*-no- ......................................................... */
/*-no- ....... */
/*-no-     #define SkScalarRound(x)        sk_float_round2int(x) */
/*-no- ....................................................................... */
/*-no- ....... */
/*-no-     #define SkScalarCeil(x)         sk_float_ceil2int(x) */
/*-no- ...................................................................... */
/*-no- ....... */
/*-no-     #define SkScalarFloor(x)        sk_float_floor2int(x) */
/*-no- ............................................................. */
/*-no- ....... */
/*-no-     #define SkScalarAbs(x)          sk_float_abs(x) */
/*-no- .................................... */
/*-no- ........ */
/*-no-     #define SkScalarCopySign(x, y)  sk_float_copysign(x, y) */
/*-no- ............................................................. */
/*-no- ....... */
/*-no- ................................................................ */
/*-no- ............................................. */
/*-no- ..... */
/*-no- ............................................................... */
/*-no- ....... */
/*-no- ......................................................................... */
/*-no- ................................................. */
/*-no- ..... */
/*-no- ..................................................... */
/*-no- ....... */
/*-no- ................................................................ */
/*-no- ............................................. */
/*-no- ....... */
/*-no-     #define SkScalarMul(a, b)       ((float)(a) * (b)) */
/*-no- ................................................................... */
/*-no- ....... */
/*-no-     #define SkScalarMulAdd(a, b, c) ((float)(a) * (b) + (c)) */
/*-no- .......................................................................................... */
/*-no- ....... */
/*-no-     #define SkScalarMulRound(a, b) SkScalarRound((float)(a) * (b)) */
/*-no- ..................................................................................... */
/*-no- ....... */
/*-no-     #define SkScalarMulCeil(a, b) SkScalarCeil((float)(a) * (b)) */
/*-no- ....................................................................................... */
/*-no- ....... */
/*-no-     #define SkScalarMulFloor(a, b) SkScalarFloor((float)(a) * (b)) */
/*-no- .................................................... */
/*-no- ....... */
/*-no-     #define SkScalarDiv(a, b)       ((float)(a) / (b)) */
/*-no- ................................................... */
/*-no- ....... */
/*-no-     #define SkScalarMod(x,y)        sk_float_mod(x,y) */
/*-no- ...................................................................................... */
/*-no- ....... */
/*-no-     #define SkScalarMulDiv(a, b, c) ((float)(a) * (b) / (c)) */
/*-no- ................................................................. */
/*-no- ....... */
/*-no-     #define SkScalarInvert(x)       (SK_Scalar1 / (x)) */
/*-no-     #define SkScalarFastInvert(x)   (SK_Scalar1 / (x)) */
/*-no- ................................................ */
/*-no- ....... */
/*-no-     #define SkScalarSqrt(x)         sk_float_sqrt(x) */
/*-no- ..................................................... */
/*-no- ....... */
/*-no-     #define SkScalarAve(a, b)       (((a) + (b)) * 0.5f) */
/*-no- .................................................... */
/*-no- ....... */
/*-no-     #define SkScalarMean(a, b)      sk_float_sqrt((float)(a) * (b)) */
/*-no- ................................................... */
/*-no- ....... */
/*-no-     #define SkScalarHalf(a)         ((a) * 0.5f) */
/*-no-  */
/*-no-     #define SK_ScalarSqrt2          1.41421356f */
/*-no-     #define SK_ScalarPI             3.14159265f */
/*-no-     #define SK_ScalarTanPIOver8     0.414213562f */
/*-no-     #define SK_ScalarRoot2Over2     0.707106781f */
/*-no-  */
/*-no-     #define SkDegreesToRadians(degrees) ((degrees) * (SK_ScalarPI / 180)) */
/*-no- ............................................................... */
/*-no-     #define SkScalarSin(radians)    (float)sk_float_sin(radians) */
/*-no-     #define SkScalarCos(radians)    (float)sk_float_cos(radians) */
/*-no-     #define SkScalarTan(radians)    (float)sk_float_tan(radians) */
/*-no-     #define SkScalarASin(val)   (float)sk_float_asin(val) */
/*-no-     #define SkScalarACos(val)   (float)sk_float_acos(val) */
/*-no-     #define SkScalarATan2(y, x) (float)sk_float_atan2(y,x) */
/*-no-     #define SkScalarExp(x)  (float)sk_float_exp(x) */
/*-no-     #define SkScalarLog(x)  (float)sk_float_log(x) */
/*-no-  */
/*-no- ................................................................................. */
/*-no- ................................................................................. */
/*-no-  */
/*-no- #else */
/*-no- ............................. */
/*-no-  */
/*-no-     #define SK_Scalar1              SK_Fixed1 */
/*-no-     #define SK_ScalarHalf           SK_FixedHalf */
/*-no-     #define SK_ScalarInfinity   SK_FixedMax */
/*-no-     #define SK_ScalarMax            SK_FixedMax */
/*-no-     #define SK_ScalarMin            SK_FixedMin */
/*-no-     #define SK_ScalarNaN            SK_FixedNaN */
/*-no-     #define SkScalarIsNaN(x)        ((x) == SK_FixedNaN) */
/*-no-     #define SkScalarIsFinite(x)     ((x) != SK_FixedNaN) */
/*-no-  */
/*-no-     #define SkIntToScalar(n)        SkIntToFixed(n) */
/*-no-     #define SkFixedToScalar(x)      (x) */
/*-no-     #define SkScalarToFixed(x)      (x) */
/*-no-     #ifdef SK_CAN_USE_FLOAT */
/*-no-         #define SkScalarToFloat(n)  SkFixedToFloat(n) */
/*-no-         #define SkFloatToScalar(n)  SkFloatToFixed(n) */
/*-no-  */
/*-no-         #define SkScalarToDouble(n) SkFixedToDouble(n) */
/*-no-         #define SkDoubleToScalar(n) SkDoubleToFixed(n) */
/*-no-     #endif */
/*-no-     #define SkScalarFraction(x)     SkFixedFraction(x) */
/*-no-     #define SkScalarRound(x)        SkFixedRound(x) */
/*-no-     #define SkScalarCeil(x)         SkFixedCeil(x) */
/*-no-     #define SkScalarFloor(x)        SkFixedFloor(x) */
/*-no-     #define SkScalarAbs(x)          SkFixedAbs(x) */
/*-no-     #define SkScalarCopySign(x, y)  SkCopySign32(x, y) */
/*-no-     #define SkScalarClampMax(x, max) SkClampMax(x, max) */
/*-no-     #define SkScalarPin(x, min, max) SkPin32(x, min, max) */
/*-no-     #define SkScalarSquare(x)       SkFixedSquare(x) */
/*-no-     #define SkScalarMul(a, b)       SkFixedMul(a, b) */
/*-no-     #define SkScalarMulAdd(a, b, c) SkFixedMulAdd(a, b, c) */
/*-no-     #define SkScalarMulRound(a, b)  SkFixedMulCommon(a, b, SK_FixedHalf) */
/*-no-     #define SkScalarMulCeil(a, b)   SkFixedMulCommon(a, b, SK_Fixed1 - 1) */
/*-no-     #define SkScalarMulFloor(a, b)  SkFixedMulCommon(a, b, 0) */
/*-no-     #define SkScalarDiv(a, b)       SkFixedDiv(a, b) */
/*-no-     #define SkScalarMod(a, b)       SkFixedMod(a, b) */
/*-no-     #define SkScalarMulDiv(a, b, c) SkMulDiv(a, b, c) */
/*-no-     #define SkScalarInvert(x)       SkFixedInvert(x) */
/*-no-     #define SkScalarFastInvert(x)   SkFixedFastInvert(x) */
/*-no-     #define SkScalarSqrt(x)         SkFixedSqrt(x) */
/*-no-     #define SkScalarAve(a, b)       SkFixedAve(a, b) */
/*-no-     #define SkScalarMean(a, b)      SkFixedMean(a, b) */
/*-no-     #define SkScalarHalf(a)         ((a) >> 1) */
/*-no-  */
/*-no-     #define SK_ScalarSqrt2          SK_FixedSqrt2 */
/*-no-     #define SK_ScalarPI             SK_FixedPI */
/*-no-     #define SK_ScalarTanPIOver8     SK_FixedTanPIOver8 */
/*-no-     #define SK_ScalarRoot2Over2     SK_FixedRoot2Over2 */
/*-no-  */
/*-no-     #define SkDegreesToRadians(degrees)     SkFractMul(degrees, SK_FractPIOver180) */
/*-no-     #define SkScalarSinCos(radians, cosPtr) SkFixedSinCos(radians, cosPtr) */
/*-no-     #define SkScalarSin(radians)    SkFixedSin(radians) */
/*-no-     #define SkScalarCos(radians)    SkFixedCos(radians) */
/*-no-     #define SkScalarTan(val)        SkFixedTan(val) */
/*-no-     #define SkScalarASin(val)       SkFixedASin(val) */
/*-no-     #define SkScalarACos(val)       SkFixedACos(val) */
/*-no-     #define SkScalarATan2(y, x)     SkFixedATan2(y,x) */
/*-no-     #define SkScalarExp(x)          SkFixedExp(x) */
/*-no-     #define SkScalarLog(x)          SkFixedLog(x) */
/*-no-  */
/*-no-     #define SkMaxScalar(a, b)       SkMax32(a, b) */
/*-no-     #define SkMinScalar(a, b)       SkMin32(a, b) */
/*-no- #endif */
/*-no-  */
/*-no- #define SK_ScalarNearlyZero         (SK_Scalar1 / (1 << 12)) */
/*-no-  */
/*-no- ....................................................................... */
/*-no- ... */
/*-no-  */
/*-no- ................................................. */
/*-no- ............................................................................. */
/*-no- ............................ */
/*-no- ...................................... */
/*-no- . */
/*-no-  */
/*-no- ...................................................... */
/*-no- ....................... */
/*-no- ....................... */
/*-no- ..................... */
/*-no- ............................. */
/*-no- ... */
/*-no- ........................................................................... */
/*-no- ........................................ */
/*-no- ..................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................................... */
/*-no- ................................................................................ */
/*-no- .......................................................................... */
/*-no- ............................................................................ */
/*-no- ............................................................................ */
/*-no- ............................................................................. */
/*-no- .............................................................................. */
/*-no- ........................................................................... */
/*-no- ................... */
/*-no- ... */
/*-no- ...................................................................... */
/*-no- ................................................................. */
/*-no-  */
/*-no- #endif */
/*-no-  */

#endif
/* CONSBENCH file end */
