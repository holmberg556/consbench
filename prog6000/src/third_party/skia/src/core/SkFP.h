/* CONSBENCH file begin */
#ifndef SKFP_H_4365
#define SKFP_H_4365

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_skia_src_core_SkFP_h, "third_party/skia/src/core/SkFP.h");

/* CONSBENCH includes begin */
#include "SkMath.h"
    #include "SkFloat.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_skia_src_core_SkFP_h, "third_party/skia/src/core/SkFP.h");

/*-no- ............................ */
/*-no- .. */
/*-no- .................................................. */
/*-no- .. */
/*-no- ................................................................... */
/*-no- .................................................................... */
/*-no- ........................................... */
/*-no- .. */
/*-no- .................................................. */
/*-no- .. */
/*-no- ....................................................................... */
/*-no- ..................................................................... */
/*-no- ............................................................................ */
/*-no- ....................................................................... */
/*-no- ................................. */
/*-no- ... */
/*-no-  */
/*-no- #ifndef SkFP_DEFINED */
/*-no- #define SkFP_DEFINED */
/*-no-  */
/*-no- #include "SkMath.h" */
/*-no-  */
/*-no- #ifdef SK_SCALAR_IS_FLOAT */
/*-no-  */
/*-no- ....................... */
/*-no-  */
/*-no-     #define SkScalarToFP(n)         (n) */
/*-no-     #define SkFPToScalar(n)         (n) */
/*-no-     #define SkIntToFP(n)            SkIntToScalar(n) */
/*-no-     #define SkFPRound(x)            SkScalarRound(n) */
/*-no-     #define SkFPCeil(x)             SkScalarCeil(n) */
/*-no-     #define SkFPFloor(x)            SkScalarFloor(n) */
/*-no-  */
/*-no-     #define SkFPNeg(x)              (-(x)) */
/*-no-     #define SkFPAbs(x)              SkScalarAbs(x) */
/*-no-     #define SkFPAdd(a, b)           ((a) + (b)) */
/*-no-     #define SkFPSub(a, b)           ((a) - (b)) */
/*-no-     #define SkFPMul(a, b)           ((a) * (b)) */
/*-no-     #define SkFPMulInt(a, n)        ((a) * (n)) */
/*-no-     #define SkFPDiv(a, b)           ((a) / (b)) */
/*-no-     #define SkFPDivInt(a, n)        ((a) / (n)) */
/*-no-     #define SkFPInvert(x)           SkScalarInvert(x) */
/*-no-     #define SkFPSqrt(x)             SkScalarSqrt(x) */
/*-no-     #define SkFPCubeRoot(x)         sk_float_pow(x, 0.3333333f) */
/*-no-  */
/*-no-     #define SkFPLT(a, b)            ((a) < (b)) */
/*-no-     #define SkFPLE(a, b)            ((a) <= (b)) */
/*-no-     #define SkFPGT(a, b)            ((a) > (b)) */
/*-no-     #define SkFPGE(a, b)            ((a) >= (b)) */
/*-no-  */
/*-no- #else   // scalar is fixed */
/*-no-  */
/*-no-     #include "SkFloat.h" */
/*-no-  */
/*-no- ......................... */
/*-no-  */
/*-no-     #define SkScalarToFP(n)         SkFloat::SetShift(n, -16) */
/*-no-     #define SkFPToScalar(n)         SkFloat::GetShift(n, -16) */
/*-no-     #define SkIntToFP(n)            SkFloat::SetShift(n, 0) */
/*-no-     #define SkFPRound(x)            SkFloat::Round(x); */
/*-no-     #define SkFPCeil(x)             SkFloat::Ceil(); */
/*-no-     #define SkFPFloor(x)            SkFloat::Floor(); */
/*-no-  */
/*-no-     #define SkFPNeg(x)              SkFloat::Neg(x) */
/*-no-     #define SkFPAbs(x)              SkFloat::Abs(x) */
/*-no-     #define SkFPAdd(a, b)           SkFloat::Add(a, b) */
/*-no-     #define SkFPSub(a, b)           SkFloat::Add(a, SkFloat::Neg(b)) */
/*-no-     #define SkFPMul(a, b)           SkFloat::Mul(a, b) */
/*-no-     #define SkFPMulInt(a, n)        SkFloat::MulInt(a, n) */
/*-no-     #define SkFPDiv(a, b)           SkFloat::Div(a, b) */
/*-no-     #define SkFPDivInt(a, n)        SkFloat::DivInt(a, n) */
/*-no-     #define SkFPInvert(x)           SkFloat::Invert(x) */
/*-no-     #define SkFPSqrt(x)             SkFloat::Sqrt(x) */
/*-no-     #define SkFPCubeRoot(x)         SkFloat::CubeRoot(x) */
/*-no-  */
/*-no-     #define SkFPLT(a, b)            (SkFloat::Cmp(a, b) < 0) */
/*-no-     #define SkFPLE(a, b)            (SkFloat::Cmp(a, b) <= 0) */
/*-no-     #define SkFPGT(a, b)            (SkFloat::Cmp(a, b) > 0) */
/*-no-     #define SkFPGE(a, b)            (SkFloat::Cmp(a, b) >= 0) */
/*-no-  */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef SK_DEBUG */
/*-no- ......................... */
/*-no- #endif */
/*-no-  */
/*-no- #endif */

#endif
/* CONSBENCH file end */
