/* CONSBENCH file begin */
#ifndef SKGRAPHICS_CPP_4378
#define SKGRAPHICS_CPP_4378

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_skia_src_core_SkGraphics_cpp, "third_party/skia/src/core/SkGraphics.cpp");

/* CONSBENCH includes begin */
#include "SkGraphics.h"
#include "Sk64.h"
#include "SkBlitter.h"
#include "SkCanvas.h"
#include "SkFloat.h"
#include "SkGeometry.h"
#include "SkGlobals.h"
#include "SkMath.h"
#include "SkMatrix.h"
#include "SkPath.h"
#include "SkPathEffect.h"
#include "SkRandom.h"
#include "SkRefCnt.h"
#include "SkScalerContext.h"
#include "SkShader.h"
#include "SkStream.h"
#include "SkTSearch.h"
#include "SkTime.h"
#include "SkUtils.h"
#include "SkXfermode.h"
#include "SkGlyphCache.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_skia_src_core_SkGraphics_cpp, "third_party/skia/src/core/SkGraphics.cpp");

/* CONSBENCH public */
CONSBENCH_PUBLIC(third_party_skia_src_core_SkGraphics_cpp);

/*-no- .................................... */
/*-no- .. */
/*-no- .................................................. */
/*-no- .. */
/*-no- ................................................................... */
/*-no- .................................................................... */
/*-no- ........................................... */
/*-no- .. */
/*-no- .................................................. */
/*-no- .. */
/*-no- ....................................................................... */
/*-no- ..................................................................... */
/*-no- ............................................................................ */
/*-no- ....................................................................... */
/*-no- ................................. */
/*-no- ... */
/*-no-  */
/*-no- #include "SkGraphics.h" */
/*-no-  */
/*-no- #include "Sk64.h" */
/*-no- #include "SkBlitter.h" */
/*-no- #include "SkCanvas.h" */
/*-no- #include "SkFloat.h" */
/*-no- #include "SkGeometry.h" */
/*-no- #include "SkGlobals.h" */
/*-no- #include "SkMath.h" */
/*-no- #include "SkMatrix.h" */
/*-no- #include "SkPath.h" */
/*-no- #include "SkPathEffect.h" */
/*-no- #include "SkRandom.h" */
/*-no- #include "SkRefCnt.h" */
/*-no- #include "SkScalerContext.h" */
/*-no- #include "SkShader.h" */
/*-no- #include "SkStream.h" */
/*-no- #include "SkTSearch.h" */
/*-no- #include "SkTime.h" */
/*-no- #include "SkUtils.h" */
/*-no- #include "SkXfermode.h" */
/*-no-  */
/*-no- #if 0 */
/*-no-  */
/*-no- #define SK_SORT_TEMPLATE_TYPE       int */
/*-no- #define SK_SORT_TEMPLATE_NAME       sort_int */
/*-no- #define SK_SORT_TEMPLATE_CMP(a, b)   ((a) - (b)) */
/*-no- #include "SkSortTemplate.h" */
/*-no-  */
/*-no- #define SK_SORT_TEMPLATE_TYPE       int* */
/*-no- #define SK_SORT_TEMPLATE_NAME       sort_intptr */
/*-no- #define SK_SORT_TEMPLATE_CMP(a, b)   (*(a) - *(b)) */
/*-no- #include "SkSortTemplate.h" */
/*-no-  */
/*-no- ....................... */
/*-no- . */
/*-no- ..................................................................... */
/*-no- .................................... */
/*-no- ......................................... */
/*-no-  */
/*-no- ........................... */
/*-no- ................................ */
/*-no- ................. */
/*-no- .... */
/*-no- ........................... */
/*-no- ........................... */
/*-no- ........................ */
/*-no- ........................... */
/*-no- ............................... */
/*-no- ................. */
/*-no-  */
/*-no- ....................... */
/*-no- ........................... */
/*-no- ................................ */
/*-no- ................. */
/*-no-  */
/*-no- . */
/*-no- #endif */
/*-no-  */
/*-no- #define SPEED_TESTx */
/*-no-  */
/*-no- #define typesizeline(type)  { #type , sizeof(type) } */
/*-no-  */
/*-no-  */
/*-no- #ifdef BUILD_EMBOSS_TABLE */
/*-no- .......................................... */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef BUILD_RADIALGRADIENT_TABLE */
/*-no- .............................................. */
/*-no- #endif */
/*-no-  */
/*-no- #define BIG_LOOP_COUNT  1000000 */
/*-no- #define TEXT_LOOP_COUNT 1000 */
/*-no-  */
/*-no- #ifdef SPEED_TEST */
/*-no- .......................... */
/*-no- . */
/*-no- .................... */
/*-no- .... */
/*-no- ............. */
/*-no- ............. */
/*-no- ................... */
/*-no- ............. */
/*-no- ............. */
/*-no- ........................ */
/*-no- . */
/*-no-  */
/*-no- ................................ */
/*-no- . */
/*-no- ....................... */
/*-no- .... */
/*-no- .......... */
/*-no- .......... */
/*-no- ....................... */
/*-no- ........... */
/*-no- ........... */
/*-no- .......................... */
/*-no- . */
/*-no-  */
/*-no- ................................................................. */
/*-no- . */
/*-no- ................... */
/*-no- .... */
/*-no- ................................... */
/*-no- ..................... */
/*-no- .... */
/*-no- ........................ */
/*-no- ................... */
/*-no- .... */
/*-no- ............................. */
/*-no- ......................................... */
/*-no- .......................... */
/*-no- .... */
/*-no- ................................... */
/*-no- .................................... */
/*-no- ...................................... */
/*-no- ................................... */
/*-no-  */
/*-no- ...................................... */
/*-no- ............................................ */
/*-no- .... */
/*-no- .................................... */
/*-no- ............................................. */
/*-no- ................................................ */
/*-no- ......................................................................... */
/*-no- ........................................................................ */
/*-no- . */
/*-no-  */
/*-no- #endif */
/*-no-  */
/*-no- ......................... */
/*-no- ...................... */
/*-no-  */
/*-no- #ifdef BUILD_EMBOSS_TABLE */
/*-no- .............................. */
/*-no- #endif */
/*-no- #ifdef BUILD_RADIALGRADIENT_TABLE */
/*-no- .................................. */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef SK_DEBUGx */
/*-no- .......... */
/*-no-  */
/*-no- ......................... */
/*-no- .............................. */
/*-no- ............................ */
/*-no- ..................... */
/*-no- ........................... */
/*-no- ............................ */
/*-no- .......................... */
/*-no- ........................... */
/*-no- ............................. */
/*-no- ............................ */
/*-no-  */
/*-no- ............................ */
/*-no- ............................ */
/*-no- ............................. */
/*-no- ............................. */
/*-no-  */
/*-no- .............................. */
/*-no- ............................. */
/*-no- ............................... */
/*-no- ............................. */
/*-no- .............................. */
/*-no- ............................... */
/*-no-  */
/*-no- .............................. */
/*-no- ............................... */
/*-no- ................................ */
/*-no- ............................... */
/*-no- ................................. */
/*-no- .................................. */
/*-no- ...... */
/*-no-  */
/*-no- #ifdef SK_CPU_BENDIAN */
/*-no- ......................................... */
/*-no- #else */
/*-no- ............................................ */
/*-no- #endif */
/*-no-  */
/*-no- ..... */
/*-no- ............................ */
/*-no- .......................................................................... */
/*-no- ...................... */
/*-no- ..................................................... */
/*-no- ............ */
/*-no- ....................................................... */
/*-no- ..... */
/*-no- .......................................................... */
/*-no- ................................................. */
/*-no- ............................................................... */
/*-no- ..... */
/*-no-  */
/*-no- #endif */
/*-no-  */
/*-no- .................................. */
/*-no- ..... */
/*-no- .............. */
/*-no- ........................................ */
/*-no- .............................................. */
/*-no- ........................................................ */
/*-no- ......... */
/*-no- ......................................... */
/*-no- .................................................................. */
/*-no-  */
/*-no- .............................................. */
/*-no- ............................................... */
/*-no- ......... */
/*-no- ........................................................................ */
/*-no-  */
/*-no- ...................... */
/*-no- ..................................... */
/*-no- .......................................... */
/*-no- .......................................... */
/*-no- ................................................... */
/*-no- .......................................... */
/*-no- ..................................... */
/*-no- .............................................................................................. */
/*-no- ......... */
/*-no- ..... */
/*-no- .... */
/*-no- .................................... */
/*-no- ..... */
/*-no- .............. */
/*-no- ........................................ */
/*-no- .............................................. */
/*-no- ........................................................... */
/*-no- ......... */
/*-no- ......................................... */
/*-no- .................................................................. */
/*-no-  */
/*-no- .............................................. */
/*-no- .................................................. */
/*-no- ......... */
/*-no- ........................................................................ */
/*-no-  */
/*-no- ...................... */
/*-no- ..................................... */
/*-no- .......................................... */
/*-no- .......................................... */
/*-no- ................................................... */
/*-no- .......................................... */
/*-no- ..................................... */
/*-no- ................................................................................................ */
/*-no- ......... */
/*-no- ..... */
/*-no- .... */
/*-no- ................................ */
/*-no- ..... */
/*-no- .............. */
/*-no- ........................................ */
/*-no- .............................................. */
/*-no- ...................................... */
/*-no- ......... */
/*-no- ......................................... */
/*-no- ............................................................. */
/*-no-  */
/*-no- .............................................. */
/*-no- ............................. */
/*-no- ......... */
/*-no- ................................................................... */
/*-no-  */
/*-no- ...................... */
/*-no- ..................................... */
/*-no- ...................................... */
/*-no- ....................................... */
/*-no- .............................. */
/*-no- ......................... */
/*-no- ..................................................................................... */
/*-no- ......... */
/*-no- ..... */
/*-no- .... */
/*-no- #ifdef SPEED_TEST */
/*-no- ................ */
/*-no- .............. */
/*-no- ......................... */
/*-no-  */
/*-no- ............................. */
/*-no- ............................. */
/*-no- ............................. */
/*-no- ..................... */
/*-no- ................................. */
/*-no- ........................................ */
/*-no- .......... */
/*-no-  */
/*-no- ............................................................ */
/*-no- ............................................ */
/*-no- ................................. */
/*-no- .................................................. */
/*-no- ........................ */
/*-no- ............. */
/*-no- .................................................................................. */
/*-no- ......... */
/*-no- ..... */
/*-no- #endif */
/*-no-  */
/*-no- ................ */
/*-no- ............................. */
/*-no- .................................................... */
/*-no- ............................................... */
/*-no- ............................................... */
/*-no-  */
/*-no- ........................................ */
/*-no- ...................................... */
/*-no- .......................................................... */
/*-no- ......... */
/*-no- ......................................... */
/*-no- ...................................... */
/*-no- ................................................................... */
/*-no- ......... */
/*-no- ......................................... */
/*-no- .......................................................................................... */
/*-no-  */
/*-no- ................................. */
/*-no- ...................................... */
/*-no- ................................................ */
/*-no- ......... */
/*-no- .................................. */
/*-no- ...................................... */
/*-no- ......................................................... */
/*-no- ......... */
/*-no- .................................. */
/*-no- .......................................................................................... */
/*-no- ........ */
/*-no- ........................ */
/*-no- ..... */
/*-no- .... */
/*-no- #ifdef SPEED_TEST */
/*-no- ................ */
/*-no- .................................................................. */
/*-no- ................................................................ */
/*-no- ................................................................ */
/*-no- .............................................................. */
/*-no- ..... */
/*-no- #endif */
/*-no- .... */
/*-no- ................. */
/*-no- ...................... */
/*-no- ....... */
/*-no- . */
/*-no-  */
/*-no- ............................................................................ */
/*-no-  */
/*-no- #include "SkGlyphCache.h" */
/*-no-  */
/*-no- ......................... */
/*-no- .................................... */
/*-no- ...................... */
/*-no- . */
/*-no-  */
/*-no- ....................................... */
/*-no- ........................................ */
/*-no- . */
/*-no-  */
/*-no- ........................................................ */
/*-no- .................................................... */
/*-no- . */
/*-no-  */
/*-no- ............................................................................. */
/*-no- ................ */
/*-no- .................................... */
/*-no- ..... */
/*-no- ................ */
/*-no- .................................... */
/*-no- ..... */
/*-no- ................ */
/*-no- .................................... */
/*-no- ..... */
/*-no- . */
/*-no-  */

#endif
/* CONSBENCH file end */
