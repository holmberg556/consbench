/* CONSBENCH file begin */
#ifndef MAIN_LIBSKIA_OPTS_CPP
#define MAIN_LIBSKIA_OPTS_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_skia_src_opts_main_libskia_opts_cpp, "third_party/skia/src/opts/main_libskia_opts.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_skia_src_opts_main_libskia_opts_cpp, "third_party/skia/src/opts/main_libskia_opts.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_skia_src_opts_SkBitmapProcState_opts_SSE2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_skia_src_opts_SkBlitRow_opts_SSE2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_skia_src_opts_SkUtils_opts_SSE2_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_skia_src_opts_main_libskia_opts_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_skia_src_opts_SkBitmapProcState_opts_SSE2_cpp(it);
  PUBLIC_third_party_skia_src_opts_SkBlitRow_opts_SSE2_cpp(it);
  PUBLIC_third_party_skia_src_opts_SkUtils_opts_SSE2_cpp(it);
}

#endif

