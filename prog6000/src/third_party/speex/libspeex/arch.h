/* CONSBENCH file begin */
#ifndef ARCH_H_4698
#define ARCH_H_4698

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_speex_libspeex_arch_h, "third_party/speex/libspeex/arch.h");

/* CONSBENCH includes begin */
#include "speex/speex_types.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_speex_libspeex_arch_h, "third_party/speex/libspeex/arch.h");

/*-no- .......................................... */
/*-no- .... */
/*-no- ............... */
/*-no- ................................................ */
/*-no- ... */
/*-no- ... */
/*-no- ..................................................................... */
/*-no- ..................................................................... */
/*-no- ........... */
/*-no- ... */
/*-no- ................................................................... */
/*-no- ................................................................ */
/*-no- ... */
/*-no- ...................................................................... */
/*-no- ...................................................................... */
/*-no- ....................................................................... */
/*-no- ... */
/*-no- ..................................................................... */
/*-no- ....................................................................... */
/*-no- ........................................................... */
/*-no- ... */
/*-no- ...................................................................... */
/*-no- ...................................................................... */
/*-no- ........................................................................ */
/*-no- ............................................................................ */
/*-no- ........................................................................ */
/*-no- ...................................................................... */
/*-no- ..................................................................... */
/*-no- ......................................................................... */
/*-no- ....................................................................... */
/*-no- ..................................................................... */
/*-no- ............................................................... */
/*-no- ... */
/*-no-  */
/*-no- #ifndef ARCH_H */
/*-no- #define ARCH_H */
/*-no-  */
/*-no- #ifndef SPEEX_VERSION */
/*-no- #define SPEEX_MAJOR_VERSION 1         /-**< Major Speex version. *-/ */
/*-no- #define SPEEX_MINOR_VERSION 1         /-**< Minor Speex version. *-/ */
/*-no- #define SPEEX_MICRO_VERSION 15        /-**< Micro Speex version. *-/ */
/*-no- #define SPEEX_EXTRA_VERSION ""        /-**< Extra Speex version. *-/ */
/*-no- #define SPEEX_VERSION "speex-1.2beta3"  /-**< Speex version string. *-/ */
/*-no- #endif */
/*-no-  */
/*-no- ......................................................... */
/*-no- #ifdef FIXED_POINT */
/*-no-  */
/*-no- #ifdef FLOATING_POINT */
/*-no- #error You cannot compile as floating point and fixed point at the same time */
/*-no- #endif */
/*-no- #ifdef _USE_SSE */
/*-no- #error SSE is only for floating-point */
/*-no- #endif */
/*-no- #if ((defined (ARM4_ASM)||defined (ARM4_ASM)) && defined(BFIN_ASM)) || (defined (ARM4_ASM)&&defined(ARM5E_ASM)) */
/*-no- #error Make up your mind. What CPU do you have? */
/*-no- #endif */
/*-no- #ifdef VORBIS_PSYCHO */
/*-no- #error Vorbis-psy model currently not implemented in fixed-point */
/*-no- #endif */
/*-no-  */
/*-no- #else */
/*-no-  */
/*-no- #ifndef FLOATING_POINT */
/*-no- #error You now need to define either FIXED_POINT or FLOATING_POINT */
/*-no- #endif */
/*-no- #if defined (ARM4_ASM) || defined(ARM5E_ASM) || defined(BFIN_ASM) */
/*-no- #error I suppose you can have a [ARM4/ARM5E/Blackfin] that has float instructions? */
/*-no- #endif */
/*-no- #ifdef FIXED_POINT_DEBUG */
/*-no- #error "Don't you think enabling fixed-point is a good thing to do if you want to debug that?" */
/*-no- #endif */
/*-no-  */
/*-no-  */
/*-no- #endif */
/*-no-  */
/*-no- #ifndef OUTSIDE_SPEEX */
/*-no- #include "speex/speex_types.h" */
/*-no- #endif */
/*-no-  */
/*-no- #define ABS(x) ((x) < 0 ? (-(x)) : (x))      /-**< Absolute integer value. *-/ */
/*-no- #define ABS16(x) ((x) < 0 ? (-(x)) : (x))    /-**< Absolute 16-bit value.  *-/ */
/*-no- #define MIN16(a,b) ((a) < (b) ? (a) : (b))   /-**< Maximum 16-bit value.   *-/ */
/*-no- #define MAX16(a,b) ((a) > (b) ? (a) : (b))   /-**< Maximum 16-bit value.   *-/ */
/*-no- #define ABS32(x) ((x) < 0 ? (-(x)) : (x))    /-**< Absolute 32-bit value.  *-/ */
/*-no- #define MIN32(a,b) ((a) < (b) ? (a) : (b))   /-**< Maximum 32-bit value.   *-/ */
/*-no- #define MAX32(a,b) ((a) > (b) ? (a) : (b))   /-**< Maximum 32-bit value.   *-/ */
/*-no-  */
/*-no- #ifdef FIXED_POINT */
/*-no-  */
/*-no- ................................. */
/*-no- ................................... */
/*-no- ............................... */
/*-no- ................................ */
/*-no- ............................... */
/*-no- ............................... */
/*-no-  */
/*-no- #define Q15ONE 32767 */
/*-no-  */
/*-no- #define LPC_SCALING  8192 */
/*-no- #define SIG_SCALING  16384 */
/*-no- #define LSP_SCALING  8192. */
/*-no- #define GAMMA_SCALING 32768. */
/*-no- #define GAIN_SCALING 64 */
/*-no- #define GAIN_SCALING_1 0.015625 */
/*-no-  */
/*-no- #define LPC_SHIFT    13 */
/*-no- #define LSP_SHIFT    13 */
/*-no- #define SIG_SHIFT    14 */
/*-no- #define GAIN_SHIFT   6 */
/*-no-  */
/*-no- #define VERY_SMALL 0 */
/*-no- #define VERY_LARGE32 ((spx_word32_t)2147483647) */
/*-no- #define VERY_LARGE16 ((spx_word16_t)32767) */
/*-no- #define Q15_ONE ((spx_word16_t)32767) */
/*-no-  */
/*-no-  */
/*-no- #ifdef FIXED_DEBUG */
/*-no- #include "fixed_debug.h" */
/*-no- #else */
/*-no-  */
/*-no- #include "fixed_generic.h" */
/*-no-  */
/*-no- #ifdef ARM5E_ASM */
/*-no- #include "fixed_arm5e.h" */
/*-no- #elif defined (ARM4_ASM) */
/*-no- #include "fixed_arm4.h" */
/*-no- #elif defined (BFIN_ASM) */
/*-no- #include "fixed_bfin.h" */
/*-no- #endif */
/*-no-  */
/*-no- #endif */
/*-no-  */
/*-no-  */
/*-no- #else */
/*-no-  */
/*-no- ........................ */
/*-no- ......................... */
/*-no- ........................ */
/*-no- ........................ */
/*-no- ........................... */
/*-no- ........................... */
/*-no-  */
/*-no- #define Q15ONE 1.0f */
/*-no- #define LPC_SCALING  1.f */
/*-no- #define SIG_SCALING  1.f */
/*-no- #define LSP_SCALING  1.f */
/*-no- #define GAMMA_SCALING 1.f */
/*-no- #define GAIN_SCALING 1.f */
/*-no- #define GAIN_SCALING_1 1.f */
/*-no-  */
/*-no-  */
/*-no- #define VERY_SMALL 1e-15f */
/*-no- #define VERY_LARGE32 1e15f */
/*-no- #define VERY_LARGE16 1e15f */
/*-no- #define Q15_ONE ((spx_word16_t)1.f) */
/*-no-  */
/*-no- #define QCONST16(x,bits) (x) */
/*-no- #define QCONST32(x,bits) (x) */
/*-no-  */
/*-no- #define NEG16(x) (-(x)) */
/*-no- #define NEG32(x) (-(x)) */
/*-no- #define EXTRACT16(x) (x) */
/*-no- #define EXTEND32(x) (x) */
/*-no- #define SHR16(a,shift) (a) */
/*-no- #define SHL16(a,shift) (a) */
/*-no- #define SHR32(a,shift) (a) */
/*-no- #define SHL32(a,shift) (a) */
/*-no- #define PSHR16(a,shift) (a) */
/*-no- #define PSHR32(a,shift) (a) */
/*-no- #define VSHR32(a,shift) (a) */
/*-no- #define SATURATE16(x,a) (x) */
/*-no- #define SATURATE32(x,a) (x) */
/*-no-  */
/*-no- #define PSHR(a,shift)       (a) */
/*-no- #define SHR(a,shift)       (a) */
/*-no- #define SHL(a,shift)       (a) */
/*-no- #define SATURATE(x,a) (x) */
/*-no-  */
/*-no- #define ADD16(a,b) ((a)+(b)) */
/*-no- #define SUB16(a,b) ((a)-(b)) */
/*-no- #define ADD32(a,b) ((a)+(b)) */
/*-no- #define SUB32(a,b) ((a)-(b)) */
/*-no- #define MULT16_16_16(a,b)     ((a)*(b)) */
/*-no- #define MULT16_16(a,b)     ((spx_word32_t)(a)*(spx_word32_t)(b)) */
/*-no- #define MAC16_16(c,a,b)     ((c)+(spx_word32_t)(a)*(spx_word32_t)(b)) */
/*-no-  */
/*-no- #define MULT16_32_Q11(a,b)     ((a)*(b)) */
/*-no- #define MULT16_32_Q13(a,b)     ((a)*(b)) */
/*-no- #define MULT16_32_Q14(a,b)     ((a)*(b)) */
/*-no- #define MULT16_32_Q15(a,b)     ((a)*(b)) */
/*-no- #define MULT16_32_P15(a,b)     ((a)*(b)) */
/*-no-  */
/*-no- #define MAC16_32_Q11(c,a,b)     ((c)+(a)*(b)) */
/*-no- #define MAC16_32_Q15(c,a,b)     ((c)+(a)*(b)) */
/*-no-  */
/*-no- #define MAC16_16_Q11(c,a,b)     ((c)+(a)*(b)) */
/*-no- #define MAC16_16_Q13(c,a,b)     ((c)+(a)*(b)) */
/*-no- #define MAC16_16_P13(c,a,b)     ((c)+(a)*(b)) */
/*-no- #define MULT16_16_Q11_32(a,b)     ((a)*(b)) */
/*-no- #define MULT16_16_Q13(a,b)     ((a)*(b)) */
/*-no- #define MULT16_16_Q14(a,b)     ((a)*(b)) */
/*-no- #define MULT16_16_Q15(a,b)     ((a)*(b)) */
/*-no- #define MULT16_16_P15(a,b)     ((a)*(b)) */
/*-no- #define MULT16_16_P13(a,b)     ((a)*(b)) */
/*-no- #define MULT16_16_P14(a,b)     ((a)*(b)) */
/*-no-  */
/*-no- #define DIV32_16(a,b)     (((spx_word32_t)(a))/(spx_word16_t)(b)) */
/*-no- #define PDIV32_16(a,b)     (((spx_word32_t)(a))/(spx_word16_t)(b)) */
/*-no- #define DIV32(a,b)     (((spx_word32_t)(a))/(spx_word32_t)(b)) */
/*-no- #define PDIV32(a,b)     (((spx_word32_t)(a))/(spx_word32_t)(b)) */
/*-no-  */
/*-no-  */
/*-no- #endif */
/*-no-  */
/*-no-  */
/*-no- #if defined (CONFIG_TI_C54X) || defined (CONFIG_TI_C55X) */
/*-no-  */
/*-no- ....................... */
/*-no- #define BYTES_PER_CHAR 2  */
/*-no- #define BITS_PER_CHAR 16 */
/*-no- #define LOG2_BITS_PER_CHAR 4 */
/*-no-  */
/*-no- #else  */
/*-no-  */
/*-no- #define BYTES_PER_CHAR 1 */
/*-no- #define BITS_PER_CHAR 8 */
/*-no- #define LOG2_BITS_PER_CHAR 3 */
/*-no-  */
/*-no- #endif */
/*-no-  */
/*-no-  */
/*-no-  */
/*-no- #ifdef FIXED_DEBUG */
/*-no- .......................... */
/*-no- #endif */
/*-no-  */
/*-no-  */
/*-no- #endif */

#endif
/* CONSBENCH file end */
