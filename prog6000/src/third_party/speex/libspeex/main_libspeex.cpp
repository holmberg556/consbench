/* CONSBENCH file begin */
#ifndef MAIN_LIBSPEEX_CPP
#define MAIN_LIBSPEEX_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_speex_libspeex_main_libspeex_cpp, "third_party/speex/libspeex/main_libspeex.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_speex_libspeex_main_libspeex_cpp, "third_party/speex/libspeex/main_libspeex.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_speex_libspeex_bits_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_cb_search_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_exc_10_16_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_exc_10_32_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_exc_20_32_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_exc_5_256_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_exc_5_64_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_exc_8_128_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_filters_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_gain_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_gain_table_lbr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_hexc_10_32_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_hexc_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_high_lsp_tables_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_lpc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_lsp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_lsp_tables_nb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_ltp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_modes_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_modes_wb_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_nb_celp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_quant_lsp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_sb_celp_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_speex_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_speex_callbacks_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_speex_header_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_stereo_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_vbr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_vq_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_speex_libspeex_window_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_speex_libspeex_main_libspeex_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_speex_libspeex_bits_cpp(it);
  PUBLIC_third_party_speex_libspeex_cb_search_cpp(it);
  PUBLIC_third_party_speex_libspeex_exc_10_16_table_cpp(it);
  PUBLIC_third_party_speex_libspeex_exc_10_32_table_cpp(it);
  PUBLIC_third_party_speex_libspeex_exc_20_32_table_cpp(it);
  PUBLIC_third_party_speex_libspeex_exc_5_256_table_cpp(it);
  PUBLIC_third_party_speex_libspeex_exc_5_64_table_cpp(it);
  PUBLIC_third_party_speex_libspeex_exc_8_128_table_cpp(it);
  PUBLIC_third_party_speex_libspeex_filters_cpp(it);
  PUBLIC_third_party_speex_libspeex_gain_table_cpp(it);
  PUBLIC_third_party_speex_libspeex_gain_table_lbr_cpp(it);
  PUBLIC_third_party_speex_libspeex_hexc_10_32_table_cpp(it);
  PUBLIC_third_party_speex_libspeex_hexc_table_cpp(it);
  PUBLIC_third_party_speex_libspeex_high_lsp_tables_cpp(it);
  PUBLIC_third_party_speex_libspeex_lpc_cpp(it);
  PUBLIC_third_party_speex_libspeex_lsp_cpp(it);
  PUBLIC_third_party_speex_libspeex_lsp_tables_nb_cpp(it);
  PUBLIC_third_party_speex_libspeex_ltp_cpp(it);
  PUBLIC_third_party_speex_libspeex_modes_cpp(it);
  PUBLIC_third_party_speex_libspeex_modes_wb_cpp(it);
  PUBLIC_third_party_speex_libspeex_nb_celp_cpp(it);
  PUBLIC_third_party_speex_libspeex_quant_lsp_cpp(it);
  PUBLIC_third_party_speex_libspeex_sb_celp_cpp(it);
  PUBLIC_third_party_speex_libspeex_speex_cpp(it);
  PUBLIC_third_party_speex_libspeex_speex_callbacks_cpp(it);
  PUBLIC_third_party_speex_libspeex_speex_header_cpp(it);
  PUBLIC_third_party_speex_libspeex_stereo_cpp(it);
  PUBLIC_third_party_speex_libspeex_vbr_cpp(it);
  PUBLIC_third_party_speex_libspeex_vq_cpp(it);
  PUBLIC_third_party_speex_libspeex_window_cpp(it);
}

#endif

