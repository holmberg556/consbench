/* CONSBENCH file begin */
#ifndef OPCODES_H_4749
#define OPCODES_H_4749

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_sqlite_preprocessed_opcodes_h, "third_party/sqlite/preprocessed/opcodes.h");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_sqlite_preprocessed_opcodes_h, "third_party/sqlite/preprocessed/opcodes.h");

/*-no- ............................................. */
/*-no- ................................................ */
/*-no- #define OP_NotExists                            1 */
/*-no- #define OP_SeekLe                               2 */
/*-no- #define OP_IncrVacuum                           3 */
/*-no- #define OP_Multiply                            86   /-* same as TK_STAR     *-/ */
/*-no- #define OP_VCreate                              4 */
/*-no- #define OP_BitAnd                              80   /-* same as TK_BITAND   *-/ */
/*-no- #define OP_ResultRow                            5 */
/*-no- #define OP_DropTrigger                          6 */
/*-no- #define OP_OpenPseudo                           7 */
/*-no- #define OP_Affinity                             8 */
/*-no- #define OP_IntegrityCk                          9 */
/*-no- #define OP_RowKey                              10 */
/*-no- #define OP_LoadAnalysis                        11 */
/*-no- #define OP_Last                                12 */
/*-no- #define OP_Subtract                            85   /-* same as TK_MINUS    *-/ */
/*-no- #define OP_Remainder                           88   /-* same as TK_REM      *-/ */
/*-no- #define OP_SetCookie                           13 */
/*-no- #define OP_Sequence                            14 */
/*-no- #define OP_VRename                             15 */
/*-no- #define OP_SeekLt                              16 */
/*-no- #define OP_SCopy                               17 */
/*-no- #define OP_VUpdate                             18 */
/*-no- #define OP_VColumn                             20 */
/*-no- #define OP_DropTable                           21 */
/*-no- #define OP_NotNull                             72   /-* same as TK_NOTNULL  *-/ */
/*-no- #define OP_Rowid                               22 */
/*-no- #define OP_Real                               130   /-* same as TK_FLOAT    *-/ */
/*-no- #define OP_String8                             94   /-* same as TK_STRING   *-/ */
/*-no- #define OP_And                                 67   /-* same as TK_AND      *-/ */
/*-no- #define OP_BitNot                              93   /-* same as TK_BITNOT   *-/ */
/*-no- #define OP_VFilter                             23 */
/*-no- #define OP_NullRow                             24 */
/*-no- #define OP_HaltIfNull                          25 */
/*-no- #define OP_Noop                                26 */
/*-no- #define OP_RowSetRead                          27 */
/*-no- #define OP_Ge                                  78   /-* same as TK_GE       *-/ */
/*-no- #define OP_RowSetAdd                           28 */
/*-no- #define OP_ParseSchema                         29 */
/*-no- #define OP_CollSeq                             30 */
/*-no- #define OP_ToText                             141   /-* same as TK_TO_TEXT  *-/ */
/*-no- #define OP_Eq                                  74   /-* same as TK_EQ       *-/ */
/*-no- #define OP_RowSetTest                          31 */
/*-no- #define OP_ToNumeric                          143   /-* same as TK_TO_NUMERIC*-/ */
/*-no- #define OP_If                                  32 */
/*-no- #define OP_IfNot                               33 */
/*-no- #define OP_ShiftRight                          83   /-* same as TK_RSHIFT   *-/ */
/*-no- #define OP_Destroy                             34 */
/*-no- #define OP_Program                             35 */
/*-no- #define OP_Permutation                         36 */
/*-no- #define OP_CreateIndex                         37 */
/*-no- #define OP_Not                                 19   /-* same as TK_NOT      *-/ */
/*-no- #define OP_Gt                                  75   /-* same as TK_GT       *-/ */
/*-no- #define OP_ResetCount                          38 */
/*-no- #define OP_Goto                                39 */
/*-no- #define OP_IdxDelete                           40 */
/*-no- #define OP_Found                               41 */
/*-no- #define OP_SeekGe                              42 */
/*-no- #define OP_Jump                                43 */
/*-no- #define OP_Pagecount                           44 */
/*-no- #define OP_MustBeInt                           45 */
/*-no- #define OP_Prev                                46 */
/*-no- #define OP_AutoCommit                          47 */
/*-no- #define OP_String                              48 */
/*-no- #define OP_ToInt                              144   /-* same as TK_TO_INT   *-/ */
/*-no- #define OP_Return                              49 */
/*-no- #define OP_Copy                                50 */
/*-no- #define OP_AddImm                              51 */
/*-no- #define OP_Function                            52 */
/*-no- #define OP_Trace                               53 */
/*-no- #define OP_Seek                                54 */
/*-no- #define OP_Concat                              89   /-* same as TK_CONCAT   *-/ */
/*-no- #define OP_NewRowid                            55 */
/*-no- #define OP_SeekGt                              56 */
/*-no- #define OP_Blob                                57 */
/*-no- #define OP_IsNull                              71   /-* same as TK_ISNULL   *-/ */
/*-no- #define OP_Next                                58 */
/*-no- #define OP_ReadCookie                          59 */
/*-no- #define OP_Halt                                60 */
/*-no- #define OP_Expire                              61 */
/*-no- #define OP_Or                                  66   /-* same as TK_OR       *-/ */
/*-no- #define OP_DropIndex                           62 */
/*-no- #define OP_IdxInsert                           63 */
/*-no- #define OP_Savepoint                           64 */
/*-no- #define OP_ShiftLeft                           82   /-* same as TK_LSHIFT   *-/ */
/*-no- #define OP_Column                              65 */
/*-no- #define OP_Int64                               68 */
/*-no- #define OP_Gosub                               69 */
/*-no- #define OP_RowData                             70 */
/*-no- #define OP_Move                                79 */
/*-no- #define OP_BitOr                               81   /-* same as TK_BITOR    *-/ */
/*-no- #define OP_MemMax                              90 */
/*-no- #define OP_Close                               91 */
/*-no- #define OP_ToReal                             145   /-* same as TK_TO_REAL  *-/ */
/*-no- #define OP_VerifyCookie                        92 */
/*-no- #define OP_Null                                95 */
/*-no- #define OP_Integer                             96 */
/*-no- #define OP_Transaction                         97 */
/*-no- #define OP_Divide                              87   /-* same as TK_SLASH    *-/ */
/*-no- #define OP_IdxLT                               98 */
/*-no- #define OP_Delete                              99 */
/*-no- #define OP_IfZero                             100 */
/*-no- #define OP_Rewind                             101 */
/*-no- #define OP_RealAffinity                       102 */
/*-no- #define OP_Clear                              103 */
/*-no- #define OP_Explain                            104 */
/*-no- #define OP_AggStep                            105 */
/*-no- #define OP_Vacuum                             106 */
/*-no- #define OP_VDestroy                           107 */
/*-no- #define OP_IsUnique                           108 */
/*-no- #define OP_Count                              109 */
/*-no- #define OP_VOpen                              110 */
/*-no- #define OP_Yield                              111 */
/*-no- #define OP_AggFinal                           112 */
/*-no- #define OP_OpenWrite                          113 */
/*-no- #define OP_Param                              114 */
/*-no- #define OP_Le                                  76   /-* same as TK_LE       *-/ */
/*-no- #define OP_VNext                              115 */
/*-no- #define OP_Sort                               116 */
/*-no- #define OP_NotFound                           117 */
/*-no- #define OP_MakeRecord                         118 */
/*-no- #define OP_Add                                 84   /-* same as TK_PLUS     *-/ */
/*-no- #define OP_IfNeg                              119 */
/*-no- #define OP_Ne                                  73   /-* same as TK_NE       *-/ */
/*-no- #define OP_Variable                           120 */
/*-no- #define OP_CreateTable                        121 */
/*-no- #define OP_Insert                             122 */
/*-no- #define OP_Compare                            123 */
/*-no- #define OP_IdxGE                              124 */
/*-no- #define OP_OpenRead                           125 */
/*-no- #define OP_IdxRowid                           126 */
/*-no- #define OP_ToBlob                             142   /-* same as TK_TO_BLOB  *-/ */
/*-no- #define OP_VBegin                             127 */
/*-no- #define OP_TableLock                          128 */
/*-no- #define OP_IfPos                              129 */
/*-no- #define OP_OpenEphemeral                      131 */
/*-no- #define OP_Lt                                  77   /-* same as TK_LT       *-/ */
/*-no-  */
/*-no- .................................................. */
/*-no- #define OP_NotUsed_132                        132 */
/*-no- #define OP_NotUsed_133                        133 */
/*-no- #define OP_NotUsed_134                        134 */
/*-no- #define OP_NotUsed_135                        135 */
/*-no- #define OP_NotUsed_136                        136 */
/*-no- #define OP_NotUsed_137                        137 */
/*-no- #define OP_NotUsed_138                        138 */
/*-no- #define OP_NotUsed_139                        139 */
/*-no- #define OP_NotUsed_140                        140 */
/*-no-  */
/*-no-  */
/*-no- ............................................................. */
/*-no- .............................................................. */
/*-no- .......................................... */
/*-no- ... */
/*-no- #define OPFLG_JUMP            0x0001  /-* jump:  P2 holds jmp target *-/ */
/*-no- #define OPFLG_OUT2_PRERELEASE 0x0002  /-* out2-prerelease: *-/ */
/*-no- #define OPFLG_IN1             0x0004  /-* in1:   P1 is an input *-/ */
/*-no- #define OPFLG_IN2             0x0008  /-* in2:   P2 is an input *-/ */
/*-no- #define OPFLG_IN3             0x0010  /-* in3:   P3 is an input *-/ */
/*-no- #define OPFLG_OUT3            0x0020  /-* out3:  P3 is an output *-/ */
/*-no- #define OPFLG_INITIALIZER {\ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ............................................................ */
/*-no- ........................ */

#endif
/* CONSBENCH file end */
