/* CONSBENCH file begin */
#ifndef PARSE_H_4751
#define PARSE_H_4751

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_sqlite_preprocessed_parse_h, "third_party/sqlite/preprocessed/parse.h");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_sqlite_preprocessed_parse_h, "third_party/sqlite/preprocessed/parse.h");

/*-no- #define TK_SEMI                            1 */
/*-no- #define TK_EXPLAIN                         2 */
/*-no- #define TK_QUERY                           3 */
/*-no- #define TK_PLAN                            4 */
/*-no- #define TK_BEGIN                           5 */
/*-no- #define TK_TRANSACTION                     6 */
/*-no- #define TK_DEFERRED                        7 */
/*-no- #define TK_IMMEDIATE                       8 */
/*-no- #define TK_EXCLUSIVE                       9 */
/*-no- #define TK_COMMIT                         10 */
/*-no- #define TK_END                            11 */
/*-no- #define TK_ROLLBACK                       12 */
/*-no- #define TK_SAVEPOINT                      13 */
/*-no- #define TK_RELEASE                        14 */
/*-no- #define TK_TO                             15 */
/*-no- #define TK_TABLE                          16 */
/*-no- #define TK_CREATE                         17 */
/*-no- #define TK_IF                             18 */
/*-no- #define TK_NOT                            19 */
/*-no- #define TK_EXISTS                         20 */
/*-no- #define TK_TEMP                           21 */
/*-no- #define TK_LP                             22 */
/*-no- #define TK_RP                             23 */
/*-no- #define TK_AS                             24 */
/*-no- #define TK_COMMA                          25 */
/*-no- #define TK_ID                             26 */
/*-no- #define TK_INDEXED                        27 */
/*-no- #define TK_ABORT                          28 */
/*-no- #define TK_AFTER                          29 */
/*-no- #define TK_ANALYZE                        30 */
/*-no- #define TK_ASC                            31 */
/*-no- #define TK_ATTACH                         32 */
/*-no- #define TK_BEFORE                         33 */
/*-no- #define TK_BY                             34 */
/*-no- #define TK_CASCADE                        35 */
/*-no- #define TK_CAST                           36 */
/*-no- #define TK_COLUMNKW                       37 */
/*-no- #define TK_CONFLICT                       38 */
/*-no- #define TK_DATABASE                       39 */
/*-no- #define TK_DESC                           40 */
/*-no- #define TK_DETACH                         41 */
/*-no- #define TK_EACH                           42 */
/*-no- #define TK_FAIL                           43 */
/*-no- #define TK_FOR                            44 */
/*-no- #define TK_IGNORE                         45 */
/*-no- #define TK_INITIALLY                      46 */
/*-no- #define TK_INSTEAD                        47 */
/*-no- #define TK_LIKE_KW                        48 */
/*-no- #define TK_MATCH                          49 */
/*-no- #define TK_KEY                            50 */
/*-no- #define TK_OF                             51 */
/*-no- #define TK_OFFSET                         52 */
/*-no- #define TK_PRAGMA                         53 */
/*-no- #define TK_RAISE                          54 */
/*-no- #define TK_REPLACE                        55 */
/*-no- #define TK_RESTRICT                       56 */
/*-no- #define TK_ROW                            57 */
/*-no- #define TK_TRIGGER                        58 */
/*-no- #define TK_VACUUM                         59 */
/*-no- #define TK_VIEW                           60 */
/*-no- #define TK_VIRTUAL                        61 */
/*-no- #define TK_REINDEX                        62 */
/*-no- #define TK_RENAME                         63 */
/*-no- #define TK_CTIME_KW                       64 */
/*-no- #define TK_ANY                            65 */
/*-no- #define TK_OR                             66 */
/*-no- #define TK_AND                            67 */
/*-no- #define TK_IS                             68 */
/*-no- #define TK_BETWEEN                        69 */
/*-no- #define TK_IN                             70 */
/*-no- #define TK_ISNULL                         71 */
/*-no- #define TK_NOTNULL                        72 */
/*-no- #define TK_NE                             73 */
/*-no- #define TK_EQ                             74 */
/*-no- #define TK_GT                             75 */
/*-no- #define TK_LE                             76 */
/*-no- #define TK_LT                             77 */
/*-no- #define TK_GE                             78 */
/*-no- #define TK_ESCAPE                         79 */
/*-no- #define TK_BITAND                         80 */
/*-no- #define TK_BITOR                          81 */
/*-no- #define TK_LSHIFT                         82 */
/*-no- #define TK_RSHIFT                         83 */
/*-no- #define TK_PLUS                           84 */
/*-no- #define TK_MINUS                          85 */
/*-no- #define TK_STAR                           86 */
/*-no- #define TK_SLASH                          87 */
/*-no- #define TK_REM                            88 */
/*-no- #define TK_CONCAT                         89 */
/*-no- #define TK_COLLATE                        90 */
/*-no- #define TK_UMINUS                         91 */
/*-no- #define TK_UPLUS                          92 */
/*-no- #define TK_BITNOT                         93 */
/*-no- #define TK_STRING                         94 */
/*-no- #define TK_JOIN_KW                        95 */
/*-no- #define TK_CONSTRAINT                     96 */
/*-no- #define TK_DEFAULT                        97 */
/*-no- #define TK_NULL                           98 */
/*-no- #define TK_PRIMARY                        99 */
/*-no- #define TK_UNIQUE                         100 */
/*-no- #define TK_CHECK                          101 */
/*-no- #define TK_REFERENCES                     102 */
/*-no- #define TK_AUTOINCR                       103 */
/*-no- #define TK_ON                             104 */
/*-no- #define TK_DELETE                         105 */
/*-no- #define TK_UPDATE                         106 */
/*-no- #define TK_INSERT                         107 */
/*-no- #define TK_SET                            108 */
/*-no- #define TK_DEFERRABLE                     109 */
/*-no- #define TK_FOREIGN                        110 */
/*-no- #define TK_DROP                           111 */
/*-no- #define TK_UNION                          112 */
/*-no- #define TK_ALL                            113 */
/*-no- #define TK_EXCEPT                         114 */
/*-no- #define TK_INTERSECT                      115 */
/*-no- #define TK_SELECT                         116 */
/*-no- #define TK_DISTINCT                       117 */
/*-no- #define TK_DOT                            118 */
/*-no- #define TK_FROM                           119 */
/*-no- #define TK_JOIN                           120 */
/*-no- #define TK_USING                          121 */
/*-no- #define TK_ORDER                          122 */
/*-no- #define TK_GROUP                          123 */
/*-no- #define TK_HAVING                         124 */
/*-no- #define TK_LIMIT                          125 */
/*-no- #define TK_WHERE                          126 */
/*-no- #define TK_INTO                           127 */
/*-no- #define TK_VALUES                         128 */
/*-no- #define TK_INTEGER                        129 */
/*-no- #define TK_FLOAT                          130 */
/*-no- #define TK_BLOB                           131 */
/*-no- #define TK_REGISTER                       132 */
/*-no- #define TK_VARIABLE                       133 */
/*-no- #define TK_CASE                           134 */
/*-no- #define TK_WHEN                           135 */
/*-no- #define TK_THEN                           136 */
/*-no- #define TK_ELSE                           137 */
/*-no- #define TK_INDEX                          138 */
/*-no- #define TK_ALTER                          139 */
/*-no- #define TK_ADD                            140 */
/*-no- #define TK_TO_TEXT                        141 */
/*-no- #define TK_TO_BLOB                        142 */
/*-no- #define TK_TO_NUMERIC                     143 */
/*-no- #define TK_TO_INT                         144 */
/*-no- #define TK_TO_REAL                        145 */
/*-no- #define TK_END_OF_FILE                    146 */
/*-no- #define TK_ILLEGAL                        147 */
/*-no- #define TK_SPACE                          148 */
/*-no- #define TK_UNCLOSED_STRING                149 */
/*-no- #define TK_FUNCTION                       150 */
/*-no- #define TK_COLUMN                         151 */
/*-no- #define TK_AGG_FUNCTION                   152 */
/*-no- #define TK_AGG_COLUMN                     153 */
/*-no- #define TK_CONST_FUNC                     154 */

#endif
/* CONSBENCH file end */
