/* CONSBENCH file begin */
#ifndef MAIN_LIBSQLITE3_CPP
#define MAIN_LIBSQLITE3_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_sqlite_src_src_main_libsqlite3_cpp, "third_party/sqlite/src/src/main_libsqlite3.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_sqlite_src_src_main_libsqlite3_cpp, "third_party/sqlite/src/src/main_libsqlite3.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_sqlite_preprocessed_opcodes_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_preprocessed_parse_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_ext_async_sqlite3async_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_ext_fts2_fts2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_ext_fts2_fts2_hash_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_ext_fts2_fts2_icu_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_ext_fts2_fts2_porter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_ext_fts2_fts2_tokenizer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_ext_fts2_fts2_tokenizer1_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_ext_fts3_fts3_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_ext_fts3_fts3_expr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_ext_fts3_fts3_hash_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_ext_fts3_fts3_icu_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_ext_fts3_fts3_porter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_ext_fts3_fts3_tokenizer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_ext_fts3_fts3_tokenizer1_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_ext_icu_icu_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_alter_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_analyze_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_attach_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_auth_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_backup_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_bitvec_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_btmutex_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_btree_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_build_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_callback_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_complete_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_date_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_delete_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_expr_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_fault_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_func_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_global_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_hash_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_insert_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_legacy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_loadext_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_main_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_malloc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_mem0_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_mem1_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_mem2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_mem3_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_mem5_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_memjournal_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_mutex_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_mutex_unix_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_notify_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_os_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_os_unix_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_pager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_pcache_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_pcache1_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_pragma_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_prepare_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_printf_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_random_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_resolve_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_rowset_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_select_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_status_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_tokenize_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_trigger_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_update_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_utf_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_vacuum_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_vdbe_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_vdbeapi_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_vdbeaux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_vdbeblob_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_vdbemem_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_vtab_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_walker_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_sqlite_src_src_where_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_sqlite_src_src_main_libsqlite3_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_sqlite_preprocessed_opcodes_cpp(it);
  PUBLIC_third_party_sqlite_preprocessed_parse_cpp(it);
  PUBLIC_third_party_sqlite_src_ext_async_sqlite3async_cpp(it);
  PUBLIC_third_party_sqlite_src_ext_fts2_fts2_cpp(it);
  PUBLIC_third_party_sqlite_src_ext_fts2_fts2_hash_cpp(it);
  PUBLIC_third_party_sqlite_src_ext_fts2_fts2_icu_cpp(it);
  PUBLIC_third_party_sqlite_src_ext_fts2_fts2_porter_cpp(it);
  PUBLIC_third_party_sqlite_src_ext_fts2_fts2_tokenizer_cpp(it);
  PUBLIC_third_party_sqlite_src_ext_fts2_fts2_tokenizer1_cpp(it);
  PUBLIC_third_party_sqlite_src_ext_fts3_fts3_cpp(it);
  PUBLIC_third_party_sqlite_src_ext_fts3_fts3_expr_cpp(it);
  PUBLIC_third_party_sqlite_src_ext_fts3_fts3_hash_cpp(it);
  PUBLIC_third_party_sqlite_src_ext_fts3_fts3_icu_cpp(it);
  PUBLIC_third_party_sqlite_src_ext_fts3_fts3_porter_cpp(it);
  PUBLIC_third_party_sqlite_src_ext_fts3_fts3_tokenizer_cpp(it);
  PUBLIC_third_party_sqlite_src_ext_fts3_fts3_tokenizer1_cpp(it);
  PUBLIC_third_party_sqlite_src_ext_icu_icu_cpp(it);
  PUBLIC_third_party_sqlite_src_src_alter_cpp(it);
  PUBLIC_third_party_sqlite_src_src_analyze_cpp(it);
  PUBLIC_third_party_sqlite_src_src_attach_cpp(it);
  PUBLIC_third_party_sqlite_src_src_auth_cpp(it);
  PUBLIC_third_party_sqlite_src_src_backup_cpp(it);
  PUBLIC_third_party_sqlite_src_src_bitvec_cpp(it);
  PUBLIC_third_party_sqlite_src_src_btmutex_cpp(it);
  PUBLIC_third_party_sqlite_src_src_btree_cpp(it);
  PUBLIC_third_party_sqlite_src_src_build_cpp(it);
  PUBLIC_third_party_sqlite_src_src_callback_cpp(it);
  PUBLIC_third_party_sqlite_src_src_complete_cpp(it);
  PUBLIC_third_party_sqlite_src_src_date_cpp(it);
  PUBLIC_third_party_sqlite_src_src_delete_cpp(it);
  PUBLIC_third_party_sqlite_src_src_expr_cpp(it);
  PUBLIC_third_party_sqlite_src_src_fault_cpp(it);
  PUBLIC_third_party_sqlite_src_src_func_cpp(it);
  PUBLIC_third_party_sqlite_src_src_global_cpp(it);
  PUBLIC_third_party_sqlite_src_src_hash_cpp(it);
  PUBLIC_third_party_sqlite_src_src_insert_cpp(it);
  PUBLIC_third_party_sqlite_src_src_legacy_cpp(it);
  PUBLIC_third_party_sqlite_src_src_loadext_cpp(it);
  PUBLIC_third_party_sqlite_src_src_main_cpp(it);
  PUBLIC_third_party_sqlite_src_src_malloc_cpp(it);
  PUBLIC_third_party_sqlite_src_src_mem0_cpp(it);
  PUBLIC_third_party_sqlite_src_src_mem1_cpp(it);
  PUBLIC_third_party_sqlite_src_src_mem2_cpp(it);
  PUBLIC_third_party_sqlite_src_src_mem3_cpp(it);
  PUBLIC_third_party_sqlite_src_src_mem5_cpp(it);
  PUBLIC_third_party_sqlite_src_src_memjournal_cpp(it);
  PUBLIC_third_party_sqlite_src_src_mutex_cpp(it);
  PUBLIC_third_party_sqlite_src_src_mutex_unix_cpp(it);
  PUBLIC_third_party_sqlite_src_src_notify_cpp(it);
  PUBLIC_third_party_sqlite_src_src_os_cpp(it);
  PUBLIC_third_party_sqlite_src_src_os_unix_cpp(it);
  PUBLIC_third_party_sqlite_src_src_pager_cpp(it);
  PUBLIC_third_party_sqlite_src_src_pcache_cpp(it);
  PUBLIC_third_party_sqlite_src_src_pcache1_cpp(it);
  PUBLIC_third_party_sqlite_src_src_pragma_cpp(it);
  PUBLIC_third_party_sqlite_src_src_prepare_cpp(it);
  PUBLIC_third_party_sqlite_src_src_printf_cpp(it);
  PUBLIC_third_party_sqlite_src_src_random_cpp(it);
  PUBLIC_third_party_sqlite_src_src_resolve_cpp(it);
  PUBLIC_third_party_sqlite_src_src_rowset_cpp(it);
  PUBLIC_third_party_sqlite_src_src_select_cpp(it);
  PUBLIC_third_party_sqlite_src_src_status_cpp(it);
  PUBLIC_third_party_sqlite_src_src_table_cpp(it);
  PUBLIC_third_party_sqlite_src_src_tokenize_cpp(it);
  PUBLIC_third_party_sqlite_src_src_trigger_cpp(it);
  PUBLIC_third_party_sqlite_src_src_update_cpp(it);
  PUBLIC_third_party_sqlite_src_src_utf_cpp(it);
  PUBLIC_third_party_sqlite_src_src_util_cpp(it);
  PUBLIC_third_party_sqlite_src_src_vacuum_cpp(it);
  PUBLIC_third_party_sqlite_src_src_vdbe_cpp(it);
  PUBLIC_third_party_sqlite_src_src_vdbeapi_cpp(it);
  PUBLIC_third_party_sqlite_src_src_vdbeaux_cpp(it);
  PUBLIC_third_party_sqlite_src_src_vdbeblob_cpp(it);
  PUBLIC_third_party_sqlite_src_src_vdbemem_cpp(it);
  PUBLIC_third_party_sqlite_src_src_vtab_cpp(it);
  PUBLIC_third_party_sqlite_src_src_walker_cpp(it);
  PUBLIC_third_party_sqlite_src_src_where_cpp(it);
}

#endif

