/* CONSBENCH file begin */
#ifndef MAIN_LIBALLOCATOR_CPP
#define MAIN_LIBALLOCATOR_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_tcmalloc_chromium_src_main_liballocator_cpp, "third_party/tcmalloc/chromium/src/main_liballocator.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_tcmalloc_chromium_src_main_liballocator_cpp, "third_party/tcmalloc/chromium/src/main_liballocator.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_tcmalloc_chromium_src_base_linuxthreads_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_base_logging_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_base_low_level_alloc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_base_spinlock_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_base_sysinfo_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_base_thread_lister_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_base_vdso_support_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_central_freelist_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_common_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_heap_profile_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_heap_profiler_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_internal_logging_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_malloc_extension_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_malloc_hook_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_maybe_threads_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_memory_region_map_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_page_heap_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_profile_handler_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_profiledata_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_profiler_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_raw_printer_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_sampler_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_span_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_stack_trace_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_stacktrace_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_static_vars_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_symbolize_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_system_alloc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_tcmalloc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_tcmalloc_chromium_src_thread_cache_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_tcmalloc_chromium_src_main_liballocator_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_tcmalloc_chromium_src_base_linuxthreads_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_base_logging_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_base_low_level_alloc_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_base_spinlock_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_base_sysinfo_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_base_thread_lister_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_base_vdso_support_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_central_freelist_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_common_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_heap_profile_table_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_heap_profiler_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_internal_logging_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_malloc_extension_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_malloc_hook_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_maybe_threads_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_memory_region_map_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_page_heap_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_profile_handler_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_profiledata_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_profiler_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_raw_printer_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_sampler_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_span_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_stack_trace_table_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_stacktrace_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_static_vars_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_symbolize_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_system_alloc_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_tcmalloc_cpp(it);
  PUBLIC_third_party_tcmalloc_chromium_src_thread_cache_cpp(it);
}

#endif

