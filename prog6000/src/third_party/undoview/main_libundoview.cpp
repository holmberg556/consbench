/* CONSBENCH file begin */
#ifndef MAIN_LIBUNDOVIEW_CPP
#define MAIN_LIBUNDOVIEW_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_undoview_main_libundoview_cpp, "third_party/undoview/main_libundoview.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_undoview_main_libundoview_cpp, "third_party/undoview/main_libundoview.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_undoview_undo_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_undoview_undo_view_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_undoview_main_libundoview_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_undoview_undo_manager_cpp(it);
  PUBLIC_third_party_undoview_undo_view_cpp(it);
}

#endif

