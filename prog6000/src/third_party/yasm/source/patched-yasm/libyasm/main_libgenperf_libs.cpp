/* CONSBENCH file begin */
#ifndef MAIN_LIBGENPERF_LIBS_CPP
#define MAIN_LIBGENPERF_LIBS_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_yasm_source_patched_yasm_libyasm_main_libgenperf_libs_cpp, "third_party/yasm/source/patched-yasm/libyasm/main_libgenperf_libs.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_yasm_source_patched_yasm_libyasm_main_libgenperf_libs_cpp, "third_party/yasm/source/patched-yasm/libyasm/main_libgenperf_libs.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_yasm_source_patched_yasm_libyasm_phash_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_yasm_source_patched_yasm_libyasm_xmalloc_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_yasm_source_patched_yasm_libyasm_xstrdup_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_yasm_source_patched_yasm_libyasm_main_libgenperf_libs_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_yasm_source_patched_yasm_libyasm_phash_cpp(it);
  PUBLIC_third_party_yasm_source_patched_yasm_libyasm_xmalloc_cpp(it);
  PUBLIC_third_party_yasm_source_patched_yasm_libyasm_xstrdup_cpp(it);
}

#endif

