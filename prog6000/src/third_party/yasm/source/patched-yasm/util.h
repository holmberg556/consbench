/* CONSBENCH file begin */
#ifndef UTIL_H_3111
#define UTIL_H_3111

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_yasm_source_patched_yasm_util_h, "third_party/yasm/source/patched-yasm/util.h");

/* CONSBENCH includes begin */
#include <config.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <libyasm-stdint.h>
#include <libyasm/coretype.h>
#  include <locale.h>
#include <libyasm/compat-queue.h>
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_yasm_source_patched_yasm_util_h, "third_party/yasm/source/patched-yasm/util.h");

/*-no- ................................................. */
/*-no- .......................... */
/*-no- .. */
/*-no- ............................................................................. */
/*-no- ............. */
/*-no- .. */
/*-no- .......................................... */
/*-no- .. */
/*-no- ..................................................................... */
/*-no- ..................................................................... */
/*-no- ........... */
/*-no- .................................................................... */
/*-no- ................................................................... */
/*-no- ....................................................................... */
/*-no- ......................................................................... */
/*-no- .......................................................................... */
/*-no- .. */
/*-no- ........................................................................... */
/*-no- ............................................................................ */
/*-no- ............................................................................. */
/*-no- ......................................................................... */
/*-no- ...................................................................... */
/*-no- ....................................................................... */
/*-no- ........................................................................... */
/*-no- .......................................................................... */
/*-no- .......................................................................... */
/*-no- ............................................................................. */
/*-no- .............................. */
/*-no- .... */
/*-no- #ifndef YASM_UTIL_H */
/*-no- #define YASM_UTIL_H */
/*-no-  */
/*-no- #ifdef HAVE_CONFIG_H */
/*-no- #include <config.h> */
/*-no- #endif */
/*-no-  */
/*-no- #if defined(HAVE_GNU_C_LIBRARY) || defined(__MINGW32__) || defined(__DJGPP__) */
/*-no-  */
/*-no- ............................................................................... */
/*-no- # ifdef __STRICT_ANSI__ */
/*-no- #  undef __STRICT_ANSI__ */
/*-no- # endif */
/*-no-  */
/*-no- ........................................................................ */
/*-no- # ifdef NO_STRING_INLINES */
/*-no- #  define __NO_STRING_INLINES */
/*-no- # endif */
/*-no-  */
/*-no- #endif */
/*-no-  */
/*-no- #if !defined(lint) && !defined(NDEBUG) */
/*-no- # define NDEBUG */
/*-no- #endif */
/*-no-  */
/*-no- #include <stdio.h> */
/*-no- #include <stdarg.h> */
/*-no-  */
/*-no- #include <stddef.h> */
/*-no- #include <stdlib.h> */
/*-no- #include <string.h> */
/*-no- #include <assert.h> */
/*-no-  */
/*-no- #ifdef HAVE_STRINGS_H */
/*-no- #include <strings.h> */
/*-no- #endif */
/*-no-  */
/*-no- #include <libyasm-stdint.h> */
/*-no- #include <libyasm/coretype.h> */
/*-no-  */
/*-no- #ifdef lint */
/*-no- # define _(String)      String */
/*-no- #else */
/*-no- # ifdef HAVE_LOCALE_H */
/*-no- #  include <locale.h> */
/*-no- # endif */
/*-no-  */
/*-no- # ifdef ENABLE_NLS */
/*-no- #  include <libintl.h> */
/*-no- #  define _(String)     gettext(String) */
/*-no- # else */
/*-no- #  define gettext(Msgid)                            (Msgid) */
/*-no- #  define dgettext(Domainname, Msgid)               (Msgid) */
/*-no- #  define dcgettext(Domainname, Msgid, Category)    (Msgid) */
/*-no- #  define textdomain(Domainname)                    while (0) /-* nothing *-/ */
/*-no- #  define bindtextdomain(Domainname, Dirname)       while (0) /-* nothing *-/ */
/*-no- #  define _(String)     (String) */
/*-no- # endif */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef gettext_noop */
/*-no- # define N_(String)     gettext_noop(String) */
/*-no- #else */
/*-no- # define N_(String)     (String) */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef HAVE_MERGESORT */
/*-no- #define yasm__mergesort(a, b, c, d)     mergesort(a, b, c, d) */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef HAVE_STRSEP */
/*-no- #define yasm__strsep(a, b)              strsep(a, b) */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef HAVE_STRCASECMP */
/*-no- # define yasm__strcasecmp(x, y)         strcasecmp(x, y) */
/*-no- # define yasm__strncasecmp(x, y, n)     strncasecmp(x, y, n) */
/*-no- #elif HAVE_STRICMP */
/*-no- # define yasm__strcasecmp(x, y)         stricmp(x, y) */
/*-no- # define yasm__strncasecmp(x, y, n)     strnicmp(x, y, n) */
/*-no- #elif HAVE__STRICMP */
/*-no- # define yasm__strcasecmp(x, y)         _stricmp(x, y) */
/*-no- # define yasm__strncasecmp(x, y, n)     _strnicmp(x, y, n) */
/*-no- #elif HAVE_STRCMPI */
/*-no- # define yasm__strcasecmp(x, y)         strcmpi(x, y) */
/*-no- # define yasm__strncasecmp(x, y, n)     strncmpi(x, y, n) */
/*-no- #else */
/*-no- # define USE_OUR_OWN_STRCASECMP */
/*-no- #endif */
/*-no-  */
/*-no- #include <libyasm/compat-queue.h> */
/*-no-  */
/*-no- #ifdef HAVE_SYS_CDEFS_H */
/*-no- # include <sys/cdefs.h> */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef __RCSID */
/*-no- # define RCSID(s)       __RCSID(s) */
/*-no- #elif defined(__GNUC__) && defined(__ELF__) */
/*-no- # define RCSID(s)       __asm__(".ident\t\"" s "\"") */
/*-no- #else */
/*-no- # define RCSID(s)       static const char rcsid[] = s */
/*-no- #endif */
/*-no-  */
/*-no- #ifdef WITH_DMALLOC */
/*-no- # include <dmalloc.h> */
/*-no- # define yasm__xstrdup(str)             xstrdup(str) */
/*-no- # define yasm_xmalloc(size)             xmalloc(size) */
/*-no- # define yasm_xcalloc(count, size)      xcalloc(count, size) */
/*-no- # define yasm_xrealloc(ptr, size)       xrealloc(ptr, size) */
/*-no- # define yasm_xfree(ptr)                xfree(ptr) */
/*-no- #endif */
/*-no-  */
/*-no- ............................................................................ */
/*-no- #define BC_TWO(c)       (0x1ul << (c)) */
/*-no- #define BC_MSK(c)       (((unsigned long)(-1)) / (BC_TWO(BC_TWO(c)) + 1ul)) */
/*-no- #define BC_COUNT(x,c)   ((x) & BC_MSK(c)) + (((x) >> (BC_TWO(c))) & BC_MSK(c)) */
/*-no- #define BitCount(d, s)          do {            \ */
/*-no- ................................................. */
/*-no- ................................................. */
/*-no- ................................................. */
/*-no- ................................................. */
/*-no- ................................................. */
/*-no- ............... */
/*-no-  */
/*-no- .............................................................................. */
/*-no- .......... */
/*-no- ..................... */
/*-no- ........................................ */
/*-no- .... */
/*-no- #define is_exp2(x)  ((x & (x - 1)) == 0) */
/*-no-  */
/*-no- #ifndef NELEMS */
/*-no- ............................................ */
/*-no- ............ */
/*-no- ......................... */
/*-no- .............................. */
/*-no- .... */
/*-no- #define NELEMS(array)   (sizeof(array) / sizeof(array[0])) */
/*-no- #endif */
/*-no-  */
/*-no- #endif */

#endif
/* CONSBENCH file end */
