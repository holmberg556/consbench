/* CONSBENCH file begin */
#ifndef MAIN_LIBZLIB_CPP
#define MAIN_LIBZLIB_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, third_party_zlib_contrib_minizip_main_libzlib_cpp, "third_party/zlib/contrib/minizip/main_libzlib.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(third_party_zlib_contrib_minizip_main_libzlib_cpp, "third_party/zlib/contrib/minizip/main_libzlib.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_third_party_zlib_contrib_minizip_ioapi_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_zlib_contrib_minizip_unzip_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_third_party_zlib_contrib_minizip_zip_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_third_party_zlib_contrib_minizip_main_libzlib_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_third_party_zlib_contrib_minizip_ioapi_cpp(it);
  PUBLIC_third_party_zlib_contrib_minizip_unzip_cpp(it);
  PUBLIC_third_party_zlib_contrib_minizip_zip_cpp(it);
}

#endif

