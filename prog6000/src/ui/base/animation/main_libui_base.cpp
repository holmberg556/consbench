/* CONSBENCH file begin */
#ifndef MAIN_LIBUI_BASE_CPP
#define MAIN_LIBUI_BASE_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, ui_base_animation_main_libui_base_cpp, "ui/base/animation/main_libui_base.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(ui_base_animation_main_libui_base_cpp, "ui/base/animation/main_libui_base.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_ui_base_animation_animation_2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_animation_animation_container_2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_animation_linear_animation_2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_animation_multi_animation_2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_animation_slide_animation_2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_animation_throb_animation_2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_animation_tween_2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_clipboard_clipboard_2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_clipboard_clipboard_linux_2_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_base_clipboard_scoped_clipboard_writer_2_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_ui_base_animation_main_libui_base_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_ui_base_animation_animation_2_cpp(it);
  PUBLIC_ui_base_animation_animation_container_2_cpp(it);
  PUBLIC_ui_base_animation_linear_animation_2_cpp(it);
  PUBLIC_ui_base_animation_multi_animation_2_cpp(it);
  PUBLIC_ui_base_animation_slide_animation_2_cpp(it);
  PUBLIC_ui_base_animation_throb_animation_2_cpp(it);
  PUBLIC_ui_base_animation_tween_2_cpp(it);
  PUBLIC_ui_base_clipboard_clipboard_2_cpp(it);
  PUBLIC_ui_base_clipboard_clipboard_linux_2_cpp(it);
  PUBLIC_ui_base_clipboard_scoped_clipboard_writer_2_cpp(it);
}

#endif

