/* CONSBENCH file begin */
#ifndef MAIN_LIBGFX_CPP
#define MAIN_LIBGFX_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, ui_gfx_main_libgfx_cpp, "ui/gfx/main_libgfx.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(ui_gfx_main_libgfx_cpp, "ui/gfx/main_libgfx.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_ui_gfx_blit_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_canvas_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_canvas_skia_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_canvas_skia_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_codec_jpeg_codec_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_codec_png_codec_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_color_utils_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_font_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_gfx_paths_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_gfx_module_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_image_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_insets_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_path_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_path_gtk_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_platform_font_gtk_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_point_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_rect_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_scrollbar_size_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_size_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_skbitmap_operations_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_skia_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_skia_utils_gtk_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_transform_skia_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_gtk_native_view_id_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_gtk_preserve_window_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_gtk_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_native_theme_linux_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_ui_gfx_native_widget_types_gtk_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_ui_gfx_main_libgfx_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_ui_gfx_blit_cpp(it);
  PUBLIC_ui_gfx_canvas_cpp(it);
  PUBLIC_ui_gfx_canvas_skia_cpp(it);
  PUBLIC_ui_gfx_canvas_skia_linux_cpp(it);
  PUBLIC_ui_gfx_codec_jpeg_codec_cpp(it);
  PUBLIC_ui_gfx_codec_png_codec_cpp(it);
  PUBLIC_ui_gfx_color_utils_cpp(it);
  PUBLIC_ui_gfx_font_cpp(it);
  PUBLIC_ui_gfx_gfx_paths_cpp(it);
  PUBLIC_ui_gfx_gfx_module_cpp(it);
  PUBLIC_ui_gfx_image_cpp(it);
  PUBLIC_ui_gfx_insets_cpp(it);
  PUBLIC_ui_gfx_path_cpp(it);
  PUBLIC_ui_gfx_path_gtk_cpp(it);
  PUBLIC_ui_gfx_platform_font_gtk_cpp(it);
  PUBLIC_ui_gfx_point_cpp(it);
  PUBLIC_ui_gfx_rect_cpp(it);
  PUBLIC_ui_gfx_scrollbar_size_cpp(it);
  PUBLIC_ui_gfx_size_cpp(it);
  PUBLIC_ui_gfx_skbitmap_operations_cpp(it);
  PUBLIC_ui_gfx_skia_util_cpp(it);
  PUBLIC_ui_gfx_skia_utils_gtk_cpp(it);
  PUBLIC_ui_gfx_transform_skia_cpp(it);
  PUBLIC_ui_gfx_gtk_native_view_id_manager_cpp(it);
  PUBLIC_ui_gfx_gtk_preserve_window_cpp(it);
  PUBLIC_ui_gfx_gtk_util_cpp(it);
  PUBLIC_ui_gfx_native_theme_linux_cpp(it);
  PUBLIC_ui_gfx_native_widget_types_gtk_cpp(it);
}

#endif

