/* CONSBENCH file begin */
#ifndef ATK_H_3904
#define ATK_H_3904

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, usr_include_atk_1_0_atk_atk_h, "usr-include/atk-1.0/atk/atk.h");

/* CONSBENCH includes begin */
#include <atk/atkobject.h>
#include <atk/atkaction.h>
#include <atk/atkcomponent.h>
#include <atk/atkdocument.h>
#include <atk/atkeditabletext.h>
#include <atk/atkgobjectaccessible.h>
#include <atk/atkhyperlink.h>
#include <atk/atkhyperlinkimpl.h>
#include <atk/atkhypertext.h>
#include <atk/atkimage.h>
#include <atk/atknoopobject.h>
#include <atk/atknoopobjectfactory.h>
#include <atk/atkobjectfactory.h>
#include <atk/atkplug.h>
#include <atk/atkregistry.h>
#include <atk/atkrelation.h>
#include <atk/atkrelationset.h>
#include <atk/atkrelationtype.h>
#include <atk/atkselection.h>
#include <atk/atksocket.h>
#include <atk/atkstate.h>
#include <atk/atkstateset.h>
#include <atk/atkstreamablecontent.h>
#include <atk/atktable.h>
#include <atk/atktext.h>
#include <atk/atkutil.h>
#include <atk/atkmisc.h>
#include <atk/atkvalue.h>
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(usr_include_atk_1_0_atk_atk_h, "usr-include/atk-1.0/atk/atk.h");

/*-no- ................................ */
/*-no- ....................................... */
/*-no- .. */
/*-no- ................................................................ */
/*-no- .............................................................. */
/*-no- ............................................................... */
/*-no- ................................................................... */
/*-no- .. */
/*-no- .................................................................. */
/*-no- ................................................................. */
/*-no- .................................................................... */
/*-no- ................................................... */
/*-no- .. */
/*-no- .................................................................... */
/*-no- ........................................................ */
/*-no- ............................................................... */
/*-no- .............................. */
/*-no- .... */
/*-no-  */
/*-no- #ifndef __ATK_H__ */
/*-no- #define __ATK_H__ */
/*-no-  */
/*-no- #define __ATK_H_INSIDE__ */
/*-no-  */
/*-no- #include <atk/atkobject.h> */
/*-no- #include <atk/atkaction.h> */
/*-no- #include <atk/atkcomponent.h> */
/*-no- #include <atk/atkdocument.h> */
/*-no- #include <atk/atkeditabletext.h> */
/*-no- #include <atk/atkgobjectaccessible.h> */
/*-no- #include <atk/atkhyperlink.h> */
/*-no- #include <atk/atkhyperlinkimpl.h> */
/*-no- #include <atk/atkhypertext.h> */
/*-no- #include <atk/atkimage.h> */
/*-no- #include <atk/atknoopobject.h> */
/*-no- #include <atk/atknoopobjectfactory.h> */
/*-no- #include <atk/atkobjectfactory.h> */
/*-no- #include <atk/atkplug.h> */
/*-no- #include <atk/atkregistry.h> */
/*-no- #include <atk/atkrelation.h> */
/*-no- #include <atk/atkrelationset.h> */
/*-no- #include <atk/atkrelationtype.h> */
/*-no- #include <atk/atkselection.h> */
/*-no- #include <atk/atksocket.h> */
/*-no- #include <atk/atkstate.h> */
/*-no- #include <atk/atkstateset.h> */
/*-no- #include <atk/atkstreamablecontent.h> */
/*-no- #include <atk/atktable.h> */
/*-no- #include <atk/atktext.h> */
/*-no- #include <atk/atkutil.h> */
/*-no- #include <atk/atkmisc.h> */
/*-no- #include <atk/atkvalue.h> */
/*-no-  */
/*-no- #undef __ATK_H_INSIDE__ */
/*-no-  */
/*-no- #endif /-* __ATK_H__ *-/ */

#endif
/* CONSBENCH file end */
