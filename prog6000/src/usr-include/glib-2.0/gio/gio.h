/* CONSBENCH file begin */
#ifndef GIO_H_44
#define GIO_H_44

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, usr_include_glib_2_0_gio_gio_h, "usr-include/glib-2.0/gio/gio.h");

/* CONSBENCH includes begin */
#include <gio/giotypes.h>
#include <gio/gappinfo.h>
#include <gio/gasyncinitable.h>
#include <gio/gasyncresult.h>
#include <gio/gbufferedinputstream.h>
#include <gio/gbufferedoutputstream.h>
#include <gio/gcancellable.h>
#include <gio/gcharsetconverter.h>
#include <gio/gcontenttype.h>
#include <gio/gconverter.h>
#include <gio/gconverterinputstream.h>
#include <gio/gconverteroutputstream.h>
#include <gio/gcredentials.h>
#include <gio/gdatainputstream.h>
#include <gio/gdataoutputstream.h>
#include <gio/gdbusaddress.h>
#include <gio/gdbusauthobserver.h>
#include <gio/gdbusconnection.h>
#include <gio/gdbuserror.h>
#include <gio/gdbusintrospection.h>
#include <gio/gdbusmessage.h>
#include <gio/gdbusmethodinvocation.h>
#include <gio/gdbusnameowning.h>
#include <gio/gdbusnamewatching.h>
#include <gio/gdbusproxy.h>
#include <gio/gdbusserver.h>
#include <gio/gdbusutils.h>
#include <gio/gdrive.h>
#include <gio/gemblemedicon.h>
#include <gio/gfileattribute.h>
#include <gio/gfileenumerator.h>
#include <gio/gfile.h>
#include <gio/gfileicon.h>
#include <gio/gfileinfo.h>
#include <gio/gfileinputstream.h>
#include <gio/gfileiostream.h>
#include <gio/gfilemonitor.h>
#include <gio/gfilenamecompleter.h>
#include <gio/gfileoutputstream.h>
#include <gio/gfilterinputstream.h>
#include <gio/gfilteroutputstream.h>
#include <gio/gicon.h>
#include <gio/ginetaddress.h>
#include <gio/ginetsocketaddress.h>
#include <gio/ginitable.h>
#include <gio/ginputstream.h>
#include <gio/gioenums.h>
#include <gio/gioenumtypes.h>
#include <gio/gioerror.h>
#include <gio/giomodule.h>
#include <gio/gioscheduler.h>
#include <gio/giostream.h>
#include <gio/gloadableicon.h>
#include <gio/gmemoryinputstream.h>
#include <gio/gmemoryoutputstream.h>
#include <gio/gmount.h>
#include <gio/gmountoperation.h>
#include <gio/gnativevolumemonitor.h>
#include <gio/gnetworkaddress.h>
#include <gio/gnetworkservice.h>
#include <gio/goutputstream.h>
#include <gio/gpermission.h>
#include <gio/gproxy.h>
#include <gio/gproxyaddress.h>
#include <gio/gproxyaddressenumerator.h>
#include <gio/gproxyresolver.h>
#include <gio/gresolver.h>
#include <gio/gseekable.h>
#include <gio/gsettings.h>
#include <gio/gsimpleasyncresult.h>
#include <gio/gsimplepermission.h>
#include <gio/gsocketaddressenumerator.h>
#include <gio/gsocketaddress.h>
#include <gio/gsocketclient.h>
#include <gio/gsocketconnectable.h>
#include <gio/gsocketconnection.h>
#include <gio/gsocketcontrolmessage.h>
#include <gio/gsocket.h>
#include <gio/gsocketlistener.h>
#include <gio/gsocketservice.h>
#include <gio/gsrvtarget.h>
#include <gio/gtcpconnection.h>
#include <gio/gthemedicon.h>
#include <gio/gthreadedsocketservice.h>
#include <gio/gvfs.h>
#include <gio/gvolume.h>
#include <gio/gvolumemonitor.h>
#include <gio/gzlibcompressor.h>
#include <gio/gzlibdecompressor.h>
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(usr_include_glib_2_0_gio_gio_h, "usr-include/glib-2.0/gio/gio.h");

/*-no- .................................................. */
/*-no- .. */
/*-no- ........................................ */
/*-no- .. */
/*-no- ................................................................ */
/*-no- ............................................................. */
/*-no- ............................................................... */
/*-no- ................................................................... */
/*-no- .. */
/*-no- .................................................................. */
/*-no- ................................................................. */
/*-no- .................................................................... */
/*-no- .................................................. */
/*-no- .. */
/*-no- ............................................................ */
/*-no- ............................................................... */
/*-no- .............................................................. */
/*-no- .............................. */
/*-no- .. */
/*-no- ............................................... */
/*-no- .... */
/*-no-  */
/*-no- #ifndef __G_IO_H__ */
/*-no- #define __G_IO_H__ */
/*-no-  */
/*-no- #define __GIO_GIO_H_INSIDE__ */
/*-no-  */
/*-no- #include <gio/giotypes.h> */
/*-no-  */
/*-no- #include <gio/gappinfo.h> */
/*-no- #include <gio/gasyncinitable.h> */
/*-no- #include <gio/gasyncresult.h> */
/*-no- #include <gio/gbufferedinputstream.h> */
/*-no- #include <gio/gbufferedoutputstream.h> */
/*-no- #include <gio/gcancellable.h> */
/*-no- #include <gio/gcharsetconverter.h> */
/*-no- #include <gio/gcontenttype.h> */
/*-no- #include <gio/gconverter.h> */
/*-no- #include <gio/gconverterinputstream.h> */
/*-no- #include <gio/gconverteroutputstream.h> */
/*-no- #include <gio/gcredentials.h> */
/*-no- #include <gio/gdatainputstream.h> */
/*-no- #include <gio/gdataoutputstream.h> */
/*-no- #include <gio/gdbusaddress.h> */
/*-no- #include <gio/gdbusauthobserver.h> */
/*-no- #include <gio/gdbusconnection.h> */
/*-no- #include <gio/gdbuserror.h> */
/*-no- #include <gio/gdbusintrospection.h> */
/*-no- #include <gio/gdbusmessage.h> */
/*-no- #include <gio/gdbusmethodinvocation.h> */
/*-no- #include <gio/gdbusnameowning.h> */
/*-no- #include <gio/gdbusnamewatching.h> */
/*-no- #include <gio/gdbusproxy.h> */
/*-no- #include <gio/gdbusserver.h> */
/*-no- #include <gio/gdbusutils.h> */
/*-no- #include <gio/gdrive.h> */
/*-no- #include <gio/gemblemedicon.h> */
/*-no- #include <gio/gfileattribute.h> */
/*-no- #include <gio/gfileenumerator.h> */
/*-no- #include <gio/gfile.h> */
/*-no- #include <gio/gfileicon.h> */
/*-no- #include <gio/gfileinfo.h> */
/*-no- #include <gio/gfileinputstream.h> */
/*-no- #include <gio/gfileiostream.h> */
/*-no- #include <gio/gfilemonitor.h> */
/*-no- #include <gio/gfilenamecompleter.h> */
/*-no- #include <gio/gfileoutputstream.h> */
/*-no- #include <gio/gfilterinputstream.h> */
/*-no- #include <gio/gfilteroutputstream.h> */
/*-no- #include <gio/gicon.h> */
/*-no- #include <gio/ginetaddress.h> */
/*-no- #include <gio/ginetsocketaddress.h> */
/*-no- #include <gio/ginitable.h> */
/*-no- #include <gio/ginputstream.h> */
/*-no- #include <gio/gioenums.h> */
/*-no- #include <gio/gioenumtypes.h> */
/*-no- #include <gio/gioerror.h> */
/*-no- #include <gio/giomodule.h> */
/*-no- #include <gio/gioscheduler.h> */
/*-no- #include <gio/giostream.h> */
/*-no- #include <gio/gloadableicon.h> */
/*-no- #include <gio/gmemoryinputstream.h> */
/*-no- #include <gio/gmemoryoutputstream.h> */
/*-no- #include <gio/gmount.h> */
/*-no- #include <gio/gmountoperation.h> */
/*-no- #include <gio/gnativevolumemonitor.h> */
/*-no- #include <gio/gnetworkaddress.h> */
/*-no- #include <gio/gnetworkservice.h> */
/*-no- #include <gio/goutputstream.h> */
/*-no- #include <gio/gpermission.h> */
/*-no- #include <gio/gproxy.h> */
/*-no- #include <gio/gproxyaddress.h> */
/*-no- #include <gio/gproxyaddressenumerator.h> */
/*-no- #include <gio/gproxyresolver.h> */
/*-no- #include <gio/gresolver.h> */
/*-no- #include <gio/gseekable.h> */
/*-no- #include <gio/gsettings.h> */
/*-no- #include <gio/gsimpleasyncresult.h> */
/*-no- #include <gio/gsimplepermission.h> */
/*-no- #include <gio/gsocketaddressenumerator.h> */
/*-no- #include <gio/gsocketaddress.h> */
/*-no- #include <gio/gsocketclient.h> */
/*-no- #include <gio/gsocketconnectable.h> */
/*-no- #include <gio/gsocketconnection.h> */
/*-no- #include <gio/gsocketcontrolmessage.h> */
/*-no- #include <gio/gsocket.h> */
/*-no- #include <gio/gsocketlistener.h> */
/*-no- #include <gio/gsocketservice.h> */
/*-no- #include <gio/gsrvtarget.h> */
/*-no- #include <gio/gtcpconnection.h> */
/*-no- #include <gio/gthemedicon.h> */
/*-no- #include <gio/gthreadedsocketservice.h> */
/*-no- #include <gio/gvfs.h> */
/*-no- #include <gio/gvolume.h> */
/*-no- #include <gio/gvolumemonitor.h> */
/*-no- #include <gio/gzlibcompressor.h> */
/*-no- #include <gio/gzlibdecompressor.h> */
/*-no-  */
/*-no- #undef __GIO_GIO_H_INSIDE__ */
/*-no-  */
/*-no- #endif /-* __G_IO_H__ *-/ */
/*-no-  */

#endif
/* CONSBENCH file end */
