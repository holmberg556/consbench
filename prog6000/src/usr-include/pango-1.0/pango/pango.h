/* CONSBENCH file begin */
#ifndef PANGO_H_1502
#define PANGO_H_1502

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, usr_include_pango_1_0_pango_pango_h, "usr-include/pango-1.0/pango/pango.h");

/* CONSBENCH includes begin */
#include <pango/pango-attributes.h>
#include <pango/pango-bidi-type.h>
#include <pango/pango-break.h>
#include <pango/pango-context.h>
#include <pango/pango-coverage.h>
#include <pango/pango-engine.h>
#include <pango/pango-enum-types.h>
#include <pango/pango-features.h>
#include <pango/pango-font.h>
#include <pango/pango-fontmap.h>
#include <pango/pango-fontset.h>
#include <pango/pango-glyph.h>
#include <pango/pango-glyph-item.h>
#include <pango/pango-gravity.h>
#include <pango/pango-item.h>
#include <pango/pango-layout.h>
#include <pango/pango-matrix.h>
#include <pango/pango-renderer.h>
#include <pango/pango-script.h>
#include <pango/pango-tabs.h>
#include <pango/pango-types.h>
#include <pango/pango-utils.h>
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(usr_include_pango_1_0_pango_pango_h, "usr-include/pango-1.0/pango/pango.h");

/*-no- ......... */
/*-no- ........... */
/*-no- .. */
/*-no- ...................................... */
/*-no- .. */
/*-no- ................................................................ */
/*-no- .............................................................. */
/*-no- ............................................................... */
/*-no- ................................................................... */
/*-no- .. */
/*-no- .................................................................. */
/*-no- ................................................................. */
/*-no- .................................................................... */
/*-no- ................................................... */
/*-no- .. */
/*-no- .................................................................... */
/*-no- ........................................................ */
/*-no- ............................................................... */
/*-no- .............................. */
/*-no- .... */
/*-no-  */
/*-no- #ifndef __PANGO_H__ */
/*-no- #define __PANGO_H__ */
/*-no-  */
/*-no- #include <pango/pango-attributes.h> */
/*-no- #include <pango/pango-bidi-type.h> */
/*-no- #include <pango/pango-break.h> */
/*-no- #include <pango/pango-context.h> */
/*-no- #include <pango/pango-coverage.h> */
/*-no- #include <pango/pango-engine.h> */
/*-no- #include <pango/pango-enum-types.h> */
/*-no- #include <pango/pango-features.h> */
/*-no- #include <pango/pango-font.h> */
/*-no- #include <pango/pango-fontmap.h> */
/*-no- #include <pango/pango-fontset.h> */
/*-no- #include <pango/pango-glyph.h> */
/*-no- #include <pango/pango-glyph-item.h> */
/*-no- #include <pango/pango-gravity.h> */
/*-no- #include <pango/pango-item.h> */
/*-no- #include <pango/pango-layout.h> */
/*-no- #include <pango/pango-matrix.h> */
/*-no- #include <pango/pango-renderer.h> */
/*-no- #include <pango/pango-script.h> */
/*-no- #include <pango/pango-tabs.h> */
/*-no- #include <pango/pango-types.h> */
/*-no- #include <pango/pango-utils.h> */
/*-no-  */
/*-no- #endif /-* __PANGO_H__ *-/ */

#endif
/* CONSBENCH file end */
