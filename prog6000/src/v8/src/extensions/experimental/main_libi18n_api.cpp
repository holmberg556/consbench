/* CONSBENCH file begin */
#ifndef MAIN_LIBI18N_API_CPP
#define MAIN_LIBI18N_API_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, v8_src_extensions_experimental_main_libi18n_api_cpp, "v8/src/extensions/experimental/main_libi18n_api.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(v8_src_extensions_experimental_main_libi18n_api_cpp, "v8/src/extensions/experimental/main_libi18n_api.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_v8_src_extensions_experimental_i18n_extension_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_v8_src_extensions_experimental_main_libi18n_api_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_v8_src_extensions_experimental_i18n_extension_cpp(it);
}

#endif

