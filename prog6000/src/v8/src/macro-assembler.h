/* CONSBENCH file begin */
#ifndef MACRO_ASSEMBLER_H_2713
#define MACRO_ASSEMBLER_H_2713

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, v8_src_macro_assembler_h, "v8/src/macro-assembler.h");

/* CONSBENCH includes begin */
#include "assembler.h"
#include "ia32/assembler-ia32.h"
#include "ia32/assembler-ia32-inl.h"
#include "code.h"  // must be after assembler_*.h
#include "ia32/macro-assembler-ia32.h"
#include "assembler.h"
#include "x64/assembler-x64.h"
#include "x64/assembler-x64-inl.h"
#include "code.h"  // must be after assembler_*.h
#include "x64/macro-assembler-x64.h"
#include "arm/constants-arm.h"
#include "assembler.h"
#include "arm/assembler-arm.h"
#include "arm/assembler-arm-inl.h"
#include "code.h"  // must be after assembler_*.h
#include "arm/macro-assembler-arm.h"
#include "mips/constants-mips.h"
#include "assembler.h"
#include "mips/assembler-mips.h"
#include "mips/assembler-mips-inl.h"
#include "code.h"  // must be after assembler_*.h
#include "mips/macro-assembler-mips.h"
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(v8_src_macro_assembler_h, "v8/src/macro-assembler.h");

/*-no- ................................................................... */
/*-no- ..................................................................... */
/*-no- ......................................................................... */
/*-no- ....... */
/*-no- .. */
/*-no- ....................................................................... */
/*-no- ...................................................................... */
/*-no- ................................................................ */
/*-no- .................................................................... */
/*-no- ........................................................................ */
/*-no- ............................... */
/*-no- ............................................................. */
/*-no- ........................................................................ */
/*-no- ...................................................................... */
/*-no- .. */
/*-no- ...................................................................... */
/*-no- .................................................................... */
/*-no- ........................................................................ */
/*-no- ....................................................................... */
/*-no- ........................................................................ */
/*-no- ................................................................... */
/*-no- ........................................................................ */
/*-no- ........................................................................ */
/*-no- ...................................................................... */
/*-no- ........................................................................ */
/*-no- ....................................................................... */
/*-no-  */
/*-no- #ifndef V8_MACRO_ASSEMBLER_H_ */
/*-no- #define V8_MACRO_ASSEMBLER_H_ */
/*-no-  */
/*-no-  */
/*-no- ................................................................. */
/*-no- ................. */
/*-no- ................ */
/*-no- ............... */
/*-no- .. */
/*-no-  */
/*-no-  */
/*-no- ................... */
/*-no- ................ */
/*-no- .............. */
/*-no- ............ */
/*-no- .. */
/*-no-  */
/*-no-  */
/*-no- .................. */
/*-no- .................... */
/*-no- ...................... */
/*-no- .................. */
/*-no- .. */
/*-no-  */
/*-no-  */
/*-no- ................................... */
/*-no- ............................... */
/*-no- ................ */
/*-no- ............. */
/*-no- .. */
/*-no-  */
/*-no-  */
/*-no- .................................... */
/*-no- .................................. */
/*-no-  */
/*-no- #if V8_TARGET_ARCH_IA32 */
/*-no- #include "assembler.h" */
/*-no- #include "ia32/assembler-ia32.h" */
/*-no- #include "ia32/assembler-ia32-inl.h" */
/*-no- #include "code.h"  // must be after assembler_*.h */
/*-no- #include "ia32/macro-assembler-ia32.h" */
/*-no- #elif V8_TARGET_ARCH_X64 */
/*-no- #include "assembler.h" */
/*-no- #include "x64/assembler-x64.h" */
/*-no- #include "x64/assembler-x64-inl.h" */
/*-no- #include "code.h"  // must be after assembler_*.h */
/*-no- #include "x64/macro-assembler-x64.h" */
/*-no- #elif V8_TARGET_ARCH_ARM */
/*-no- #include "arm/constants-arm.h" */
/*-no- #include "assembler.h" */
/*-no- #include "arm/assembler-arm.h" */
/*-no- #include "arm/assembler-arm-inl.h" */
/*-no- #include "code.h"  // must be after assembler_*.h */
/*-no- #include "arm/macro-assembler-arm.h" */
/*-no- #elif V8_TARGET_ARCH_MIPS */
/*-no- #include "mips/constants-mips.h" */
/*-no- #include "assembler.h" */
/*-no- #include "mips/assembler-mips.h" */
/*-no- #include "mips/assembler-mips-inl.h" */
/*-no- #include "code.h"  // must be after assembler_*.h */
/*-no- #include "mips/macro-assembler-mips.h" */
/*-no- #else */
/*-no- #error Unsupported target architecture. */
/*-no- #endif */
/*-no-  */
/*-no- .............. */
/*-no- .................... */
/*-no-  */
/*-no- .......................................... */
/*-no- #ifdef DEBUG */
/*-no-  */
/*-no- ............... */
/*-no- ........ */
/*-no- ................................................. */
/*-no- ............. */
/*-no-  */
/*-no- ......... */
/*-no- ........................ */
/*-no- ................... */
/*-no- .. */
/*-no-  */
/*-no- #else */
/*-no-  */
/*-no- ............... */
/*-no- ........ */
/*-no- ........................................... */
/*-no- .. */
/*-no-  */
/*-no- #endif  // DEBUG */
/*-no-  */
/*-no- .............................. */
/*-no-  */
/*-no- #endif  // V8_MACRO_ASSEMBLER_H_ */

#endif
/* CONSBENCH file end */
