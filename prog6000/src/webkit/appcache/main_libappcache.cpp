/* CONSBENCH file begin */
#ifndef MAIN_LIBAPPCACHE_CPP
#define MAIN_LIBAPPCACHE_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, webkit_appcache_main_libappcache_cpp, "webkit/appcache/main_libappcache.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(webkit_appcache_main_libappcache_cpp, "webkit/appcache/main_libappcache.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_webkit_appcache_appcache_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_backend_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_database_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_disk_cache_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_frontend_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_group_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_histograms_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_host_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_interceptor_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_interfaces_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_request_handler_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_response_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_service_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_storage_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_storage_impl_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_thread_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_working_set_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_update_job_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_appcache_url_request_job_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_manifest_parser_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_view_appcache_internals_job_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_appcache_web_application_cache_host_impl_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_webkit_appcache_main_libappcache_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_webkit_appcache_appcache_cpp(it);
  PUBLIC_webkit_appcache_appcache_backend_impl_cpp(it);
  PUBLIC_webkit_appcache_appcache_database_cpp(it);
  PUBLIC_webkit_appcache_appcache_disk_cache_cpp(it);
  PUBLIC_webkit_appcache_appcache_frontend_impl_cpp(it);
  PUBLIC_webkit_appcache_appcache_group_cpp(it);
  PUBLIC_webkit_appcache_appcache_histograms_cpp(it);
  PUBLIC_webkit_appcache_appcache_host_cpp(it);
  PUBLIC_webkit_appcache_appcache_interceptor_cpp(it);
  PUBLIC_webkit_appcache_appcache_interfaces_cpp(it);
  PUBLIC_webkit_appcache_appcache_request_handler_cpp(it);
  PUBLIC_webkit_appcache_appcache_response_cpp(it);
  PUBLIC_webkit_appcache_appcache_service_cpp(it);
  PUBLIC_webkit_appcache_appcache_storage_cpp(it);
  PUBLIC_webkit_appcache_appcache_storage_impl_cpp(it);
  PUBLIC_webkit_appcache_appcache_thread_cpp(it);
  PUBLIC_webkit_appcache_appcache_working_set_cpp(it);
  PUBLIC_webkit_appcache_appcache_update_job_cpp(it);
  PUBLIC_webkit_appcache_appcache_url_request_job_cpp(it);
  PUBLIC_webkit_appcache_manifest_parser_cpp(it);
  PUBLIC_webkit_appcache_view_appcache_internals_job_cpp(it);
  PUBLIC_webkit_appcache_web_application_cache_host_impl_cpp(it);
}

#endif

