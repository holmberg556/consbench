/* CONSBENCH file begin */
#ifndef MAIN_LIBBLOB_CPP
#define MAIN_LIBBLOB_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, webkit_blob_main_libblob_cpp, "webkit/blob/main_libblob.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(webkit_blob_main_libblob_cpp, "webkit/blob/main_libblob.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_webkit_blob_blob_data_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_blob_blob_storage_controller_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_blob_blob_url_request_job_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_blob_deletable_file_reference_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_blob_view_blob_internals_job_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_webkit_blob_main_libblob_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_webkit_blob_blob_data_cpp(it);
  PUBLIC_webkit_blob_blob_storage_controller_cpp(it);
  PUBLIC_webkit_blob_blob_url_request_job_cpp(it);
  PUBLIC_webkit_blob_deletable_file_reference_cpp(it);
  PUBLIC_webkit_blob_view_blob_internals_job_cpp(it);
}

#endif

