/* CONSBENCH file begin */
#ifndef MAIN_LIBDATABASE_CPP
#define MAIN_LIBDATABASE_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, webkit_database_main_libdatabase_cpp, "webkit/database/main_libdatabase.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(webkit_database_main_libdatabase_cpp, "webkit/database/main_libdatabase.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_webkit_database_databases_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_database_database_connections_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_database_database_tracker_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_database_database_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_database_quota_table_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_database_vfs_backend_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_webkit_database_main_libdatabase_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_webkit_database_databases_table_cpp(it);
  PUBLIC_webkit_database_database_connections_cpp(it);
  PUBLIC_webkit_database_database_tracker_cpp(it);
  PUBLIC_webkit_database_database_util_cpp(it);
  PUBLIC_webkit_database_quota_table_cpp(it);
  PUBLIC_webkit_database_vfs_backend_cpp(it);
}

#endif

