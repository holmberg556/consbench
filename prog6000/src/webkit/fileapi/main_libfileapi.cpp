/* CONSBENCH file begin */
#ifndef MAIN_LIBFILEAPI_CPP
#define MAIN_LIBFILEAPI_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, webkit_fileapi_main_libfileapi_cpp, "webkit/fileapi/main_libfileapi.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(webkit_fileapi_main_libfileapi_cpp, "webkit/fileapi/main_libfileapi.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_webkit_fileapi_file_system_context_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_fileapi_file_system_dir_url_request_job_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_fileapi_file_system_file_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_fileapi_file_system_file_util_proxy_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_fileapi_file_system_operation_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_fileapi_file_system_path_manager_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_fileapi_file_system_url_request_job_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_fileapi_file_system_usage_tracker_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_fileapi_file_system_util_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_fileapi_file_system_usage_cache_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_fileapi_file_writer_delegate_cpp(ConsBenchAcc_iter_t it);
extern void PUBLIC_webkit_fileapi_webfilewriter_base_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_webkit_fileapi_main_libfileapi_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_webkit_fileapi_file_system_context_cpp(it);
  PUBLIC_webkit_fileapi_file_system_dir_url_request_job_cpp(it);
  PUBLIC_webkit_fileapi_file_system_file_util_cpp(it);
  PUBLIC_webkit_fileapi_file_system_file_util_proxy_cpp(it);
  PUBLIC_webkit_fileapi_file_system_operation_cpp(it);
  PUBLIC_webkit_fileapi_file_system_path_manager_cpp(it);
  PUBLIC_webkit_fileapi_file_system_url_request_job_cpp(it);
  PUBLIC_webkit_fileapi_file_system_usage_tracker_cpp(it);
  PUBLIC_webkit_fileapi_file_system_util_cpp(it);
  PUBLIC_webkit_fileapi_file_system_usage_cache_cpp(it);
  PUBLIC_webkit_fileapi_file_writer_delegate_cpp(it);
  PUBLIC_webkit_fileapi_webfilewriter_base_cpp(it);
}

#endif

