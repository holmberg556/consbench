/* CONSBENCH file begin */
#ifndef MAIN_LIBWEBKIT_USER_AGENT_CPP
#define MAIN_LIBWEBKIT_USER_AGENT_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, webkit_glue_main_libwebkit_user_agent_cpp, "webkit/glue/main_libwebkit_user_agent.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(webkit_glue_main_libwebkit_user_agent_cpp, "webkit/glue/main_libwebkit_user_agent.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_webkit_glue_user_agent_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_webkit_glue_main_libwebkit_user_agent_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_webkit_glue_user_agent_cpp(it);
}

#endif

