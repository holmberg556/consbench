/* CONSBENCH file begin */
#ifndef MAIN_LIBWEBKIT_GPU_CPP
#define MAIN_LIBWEBKIT_GPU_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, webkit_gpu_main_libwebkit_gpu_cpp, "webkit/gpu/main_libwebkit_gpu.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(webkit_gpu_main_libwebkit_gpu_cpp, "webkit/gpu/main_libwebkit_gpu.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_webkit_gpu_webgraphicscontext3d_in_process_impl_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_webkit_gpu_main_libwebkit_gpu_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_webkit_gpu_webgraphicscontext3d_in_process_impl_cpp(it);
}

#endif

