/* CONSBENCH file begin */
#ifndef MAIN_LIBQUOTA_CPP
#define MAIN_LIBQUOTA_CPP

/* CONSBENCH enter */
#include "ConsBench.h"
CONSBENCH_ENTER(1, webkit_quota_main_libquota_cpp, "webkit/quota/main_libquota.cpp");

/* CONSBENCH includes begin */
/* CONSBENCH includes end */

/* CONSBENCH leave */
CONSBENCH_LEAVE(webkit_quota_main_libquota_cpp, "webkit/quota/main_libquota.cpp");

/* CONSBENCH libmain */
extern void PUBLIC_webkit_quota_special_storage_policy_cpp(ConsBenchAcc_iter_t it);
void LIB_PUBLIC_webkit_quota_main_libquota_cpp(ConsBenchAcc_iter_t it)
{
  it(sConsBenchAcc());
  PUBLIC_webkit_quota_special_storage_policy_cpp(it);
}

#endif

